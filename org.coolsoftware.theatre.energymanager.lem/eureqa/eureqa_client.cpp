#include <iostream>
#include <time.h>
#include <stdio.h>
#include <direct.h>

#include <eureqa/eureqa.h>

#ifdef WIN32
inline void sleep(int sec) { Sleep(sec*1000); }
#endif

void pause_exit(int code)
{
    std::cout << std::endl;
    #ifdef WIN32
    system("pause");
	#endif
    exit(code);
}

int main(int argc, char *argv[])
{    
	std::string file;
	std::string search_rel;
	//usage: eureqa_client datafile
	if(argc != 3) {
		std::cout << "usage: eureqa_client file search_relationship" << std::endl;
		std::cout << "e.g., eureqa_client benchmark.data f(param)=response_time" << std::endl;
		pause_exit(-1);
	} else {
		file = argv[1];
		search_rel = argv[2];
	}

    // initialize a data set
    eureqa::data_set data; // holds the data
    std::string error_msg; // error information during import

    // import data from a text file
    if (!data.import_ascii(file, error_msg))
    {
        std::cout << error_msg << std::endl;
        std::cout << "Unable to import this file" << std::endl;
        pause_exit(-1);
    }

	/*std::cout << data.summary() << std::endl;
	data.export_ascii(std::cout);
	std::cout << std::endl;*/
    
    // initialize search options
    eureqa::search_options options; // holds the search options
	options.fitness_metric_ = eureqa::fitness_types::root_squared_error;
	options.set_default_building_blocks();
    options.building_blocks_.push_back("x^y");
	options.search_relationship_ = search_rel;

    eureqa::connection conn;

    if (!conn.connect("127.0.0.1"))
    {
        std::cout << "Unable to connect to server" << std::endl; 
		std::cout << "Try running the eureqa_server binary provided with ";
		std::cout << "the Eureqa API (\"server\" sub-directory) first." << std::endl;
        pause_exit(-1);
    }
    else if (!conn.last_result()) 
    { 
        std::cout << "Connection made successfully, but ";
        std::cout << "the server sent back an error message:" << std::endl;
        std::cout << conn.last_result() << std::endl;
        pause_exit(-1); 
    }
    
    if (!conn.send_data_set(data))
    {
        std::cout << "Unable to transfer the data set" << std::endl;
        pause_exit(-1);
    }
    else if (!conn.last_result())
    {
        std::cout << "Data set transferred successfully, but ";
        std::cout << "the server sent back an error message:" << std::endl;
        std::cout << conn.last_result() << std::endl;
        pause_exit(-1);
    }
    
    if (!conn.send_options(options))
    {
        std::cout << "Unable to transfer the search options" << std::endl;
        pause_exit(-1);
    }
    else if (!conn.last_result())
    {
        std::cout << "Search options transferred successfully, but ";
        std::cout << "the server sent back an error message:" << std::endl;
        std::cout << conn.last_result() << std::endl;
        pause_exit(-1);
    }
    
    if (!conn.start_search())
    {
        std::cout << "Unable to send the start command" << std::endl;
        pause_exit(-1);
    }
    else if (!conn.last_result())
    {
        std::cout << "Start command sent successfully, but ";
        std::cout << "the server sent back an error message:" << std::endl;
        std::cout << conn.last_result() << std::endl;
        pause_exit(-1);
    }
    
    eureqa::search_progress progress; // recieves the progress and new solutions
    eureqa::solution_frontier best_solutions; // filters out the best solutions
 

	sleep(5);

	conn.query_frontier(best_solutions);
	
	/*std::cout << best_solutions.to_string() << std::endl;
	std::cout << best_solutions.size() << " solutions found" << std::endl;*/
	
	int bestIdx = 0;
	float bestFit = best_solutions[0].fitness_ * best_solutions[0].complexity_;
	for(int i = 1; i < best_solutions.size(); i++) {
		if(best_solutions[i].fitness_ < bestFit) {
			bestFit = best_solutions[i].fitness_ * best_solutions[i].complexity_;
			bestIdx = i;
		}
	}

	std::cout << best_solutions[bestIdx] << std::endl;
    
    pause_exit(0);
    return 0;
}
