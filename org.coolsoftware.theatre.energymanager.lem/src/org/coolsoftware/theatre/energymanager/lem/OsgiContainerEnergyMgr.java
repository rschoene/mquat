/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.lem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.resource.exp.ParserFacade;
import org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResource;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.nameable.IdentifyableElement;
import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.theatre.Resettable;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.Reconnector;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.haec.theatre.api.App;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.Impl;
import org.haec.theatre.api.Monitor;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.benchmark.IBenchmarkUtil;
import org.haec.theatre.monitor.MonitorRepository;
import org.haec.theatre.monitor.RemoteMonitor;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.task.repository.ITaskRepository;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * OSGi-based implementation of a basic local energy that depends on container
 * providers
 * 
 * @author Sebastian G�tz
 * @author Sebastian Cech
 * @author René Schöne
 */
public class OsgiContainerEnergyMgr implements ILocalEnergyManager, Resettable {

	private static final Logger log = Logger
			.getLogger(OsgiContainerEnergyMgr.class);
	protected static final int GEM_START = 0;
	protected static final int GEM_ALIVE = 1;
	protected static final int GEM_UNAVAILABLE = 2;
	private IGlobalEnergyManager gem; // the gobal EM
	private final URI containerUri; // reference to the ContainerProvider
									// resource Mgr

	private HashMap<String, List<String>> deployedCompVariants; // <K =
																// ComponentTypeID,
																// V=ComponentID>

	private Map<String, String> contracts = new HashMap<String, String>();
	private Map<String, Boolean> contractsReady = new HashMap<String, Boolean>();
	
	private boolean useEureqa = false;
	
	private final Timer timer;

	private class Result {
		final int callId;
		final long jobId;
		final String implName;
		final boolean isConnector;
		final boolean isCopyType;
		boolean finished;
		Serializable result;
		public Result(int callId, long jobId, String implName,
				boolean isConnector, boolean isCopyType) {
			this(callId, jobId, implName, isConnector, isCopyType, false, null);
		}
		protected Result(int callId, long jobId, String implName,
				boolean isConnector, boolean isCopyType,
				boolean finished, Serializable result) {
			this.callId = callId;
			this.jobId = jobId;
			this.implName = implName;
			this.isConnector = isConnector;
			this.isCopyType = isCopyType;
			this.finished = finished;
			this.result = result;
		}
		@Override
		public String toString() {
			return String.format("Result%s(%d#%sfinished, res=%s, con=%b)",
					hashCode(), callId, (finished?"":"un"), StringUtils.cap(result, 100), isConnector);
		}
	}
	private final Result NO_RESULT = new Result(-1, -1, "NO_RESULT",
			false, false, false, null) {
		public String toString() {
			return "NO_RESULT";
		};
	};
	private List<Result> results = new ArrayList<Result>();
	private int callIdCounter = 0;
	/** Locks {@link #apps} */
	private Lock appLock;
	/** { appName - info on this app }. Locked by {@link #appLock} */
	Map<String, AppInfo> apps = new HashMap<>();
	private TheatreSettings settings;

//	public OsgiContainerEnergyMgr(IGlobalEnergyManager gem, URI uri) {
//		this.remote = RemoteOsgiUtil.getRemoteOSGiService(getBundleContext());
//		setGlobalMgr(gem);
//		this.containerUri = uri;
//		this.timer = new Timer();
//		deployedCompVariants = new HashMap<String, List<String>>();
//		log.debug("Initialized LocalEnergyManager. GEM: "
//				+ (gem != null ? "resolved" : "error")
//				+ " URI: " + containerUri);
//		startCheckForNewMaster();
//	}

	public OsgiContainerEnergyMgr() {
		this.containerUri = URI.create(RemoteOsgiUtil.createRemoteOsgiUriString(RemoteOsgiUtil.getLemIp()));
		this.timer = new Timer();
		deployedCompVariants = new HashMap<String, List<String>>();
		log.debug("Initialized LocalEnergyManager.");
		appLock = new ReentrantLock(true);
	}

	private void startCheckForNewMaster(final ComponentContext ctx) {
		TimerTask task = new TimerTask() {
			int phase = GEM_START;
			
			@Override
			public void run() {
				boolean reconnecting = false;
				String gemUri = settings.getGemUri().get();
				try {
					RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(ctx.getBundleContext());
					IGlobalEnergyManager localGem = RemoteOsgiUtil.getRemoteService(
							remote, gemUri, IGlobalEnergyManager.class);
					String retUri = localGem.getUri();
					if(phase != GEM_ALIVE) {
						reconnecting = true;
						setGlobalMgr(localGem);
						BundleContext context = ctx.getBundleContext();
						Reconnector service = context.getService(context.getServiceReference(Reconnector.class));
						// create (new) proxy for task repo
						RemoteOsgiUtil.getRemoteService(remote, gemUri, ITaskRepository.class);
						if(service != null) {
							service.runRegisteredReconnectables();
						}
						if(phase != GEM_START) {
							for(Entry<String, AppInfo> e : apps.entrySet()) {
								log.debug("Re-register app " + e.getKey());
								AppInfo info = e.getValue();
								info.registeredWithGem = false;
								registerApp(info.app);
								info.registeredWithGem = true;
								// register known impls again
								for(Impl impl : info.impls) {
									registerImpl(impl);
								}
							}
						}
						
						log.info("Reconnected to GEM at " + retUri);
						phase = GEM_ALIVE;
					}
				} catch (Exception e) {
					// could not connect to GEM, switch to "Wait for new master"-phase
					if(phase != GEM_UNAVAILABLE) {
						log.warn("Gem at "+gemUri+" not responding. Waiting now.");
						phase = GEM_UNAVAILABLE;
					}
					if(reconnecting) {
						log.debug("Problems with reconnect", e);
					}
				}
			}
		};
		long period = settings.getLemCheckForMasterPeriod().get() * 1000;
		if(period > 0) {
			log.info("Check for master every " + period + "ms.");
			timer.scheduleAtFixedRate(task, 0, period);
		}
		else {
			// run once to get GEM set
			task.run();
			log.info("Will not check for master periodically, because " 
					+ settings.getLemCheckForMasterPeriod().getName() + " is negative"
					+ (settings.getLemCheckForMasterPeriod().isDefault() ? " and default" : ""));
		}
	}

	@Override
	public void replace(String type, String oldComp, String newComp) {
		List<String> variants = getDeployedComponents(type);
		if (variants.contains(oldComp)) {
			deploy(type, newComp);
			undeploy(type, oldComp);
		}
	}

	@Override
	public void replace(String type, String oldComp, String newComp,
			String trgtContainer) {
		ILocalEnergyManager lem = getRemoteLem(trgtContainer);
		lem.deploy(type, newComp);
		undeploy(type, oldComp);

	}

	@Override
	public void migrate(String type, String srcComp, String trgtContainer) {
		ILocalEnergyManager lem = getRemoteLem(trgtContainer);
		lem.deploy(type, srcComp);
		undeploy(type, srcComp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.theatre.energymanager.ILocalEnergyManager#deploy(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public void deploy(String type, String variant) {
		//TODO implement
		log.info("deploying "+variant+" of "+type+" on "+containerUri);
		if(!getDeployedComponents(type).contains(variant)) {
			registerDeployedComponent(type, variant);
		}
		// get the repository
//		IComponentRepository rep = LEMActivator
//				.getRepository(this.gem.getUri());
//		// deploy the type if neccessary
//		if (!isComponentTypeDeployed(type)) {
//			try {
//				URL url = rep.getComponentTypeLocation(type);
//				if(url != null)
//					bmgr.install(url.toString());
//				else log.error("Please implement Repository first."); //TODO impl
//			} catch (MalformedURLException e) {
//				log.error("URL for component type " + type + "is invalid. ", e);
//			}
//		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.theatre.energymanager.ILocalEnergyManager#undeploy(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public void undeploy(String type, String variant) {
		//TODO implement
//		BundleManager bmgr = new BundleManager();
//		List<String> deplVars = getDeployedComponents(type);
//		if (deplVars.contains(variant)) {
//			for (Bundle bundle : (LEMActivator.getContext().getBundles())) {
//				String ccmBundleinfo = bundle.getHeaders().get(
//						CCMBundleListener.CCM_VARIANT);
//				if ((variant != null) && (variant.equals(ccmBundleinfo))) {
//					bmgr.uninstall(bundle.getBundleId());
//					unregisterUndeployedComponent(type, variant);
//					break;
//				}
//			}
//		}
//		if (deplVars.isEmpty()) {
//			for (Bundle bundle : (LEMActivator.getContext().getBundles())) {
//				String ccmBundleinfo = bundle.getHeaders().get(
//						CCMBundleListener.CCM_TYPE);
//				if ((type != null) && (type.equals(ccmBundleinfo))) {
//					bmgr.uninstall(bundle.getBundleId());
//					break;
//				}
//			}
//		}
	}

	/*
	 * Only management of internal LEM infos
	 */
	public void registerDeployedComponent(String type, String variant) {
		if (deployedCompVariants.containsKey(type)) {
			List<String> variants = deployedCompVariants.get(type);
			if (!variants.contains(variant))
				variants.add(variant);
		} else {
			List<String> variants = new ArrayList<String>();
			if (variant != null) {
				variants.add(variant);
			}
			deployedCompVariants.put(type, variants);
		}
		log.debug("Component [" + type + "] " + variant + " registered.");
	}

	/*
	 * Only management of internal LEM infos
	 */
	public void unregisterUndeployedComponent(String type, String variant) {
		// type undeployment (variant is null)
		if ((deployedCompVariants.containsKey(type)) && (variant == null)) {
			deployedCompVariants.remove(type);
			log.debug("Successful undeployment of Type :" + type);
		} else if (deployedCompVariants.containsKey(type)) {
			List<String> variants = deployedCompVariants.get(type);
			if (variants.contains(variant)) {
				variants.remove(variant);
				log.debug("Successful undeployment of Component "
						+ variant
						+ ". There are the following amount of other components: "
						+ variants.size());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.theatre.energymanager.ILocalEnergyManager#getGlobalMgr()
	 */
	@Override
	public IGlobalEnergyManager getGlobalMgr() {
		return gem;
	}
	
	@Override
	public void setGlobalMgr(IGlobalEnergyManager gem) {
		this.gem = gem;
		boolean success = gem.registerLEM(containerUri);
		if(!success) {
			log.warn(String.format("Failed to register at GEM, address.gem = %s, address.lem = %s",
					settings.getGemUri(), containerUri));
		}
	}

	@Override
	public String getContainerId() {
		return containerUri.toString();
	}

	@Override
	public List<String> getDeployedComponentTypes() {
		return new ArrayList<String>(deployedCompVariants.keySet());
	}

	@Override
	public boolean isComponentTypeDeployed(String componentType) {
		return deployedCompVariants.containsKey(componentType);
	}

	@Override
	public List<String> getDeployedComponents(String componentType) {
		List<String> variants = deployedCompVariants.get(componentType);
		log.debug(componentType + "->" + variants);
		if (variants == null) {
			return new ArrayList<String>();
		}
		return variants;
	}

	@Deprecated
	Map<String, List<String>> getdeployedCompVariants() {
		return deployedCompVariants;
	}

	@Override
	public Map<String, Object> getComponentProperties(String componentUri) {
		//TODO
		return null;
	}

	@Override
	public Object getComponentProperty(String component) {
		//TODO
		return null;
	}

	@Override
	public String getComponentModes(String component) {
		//TODO
		return null;
	}

	@Override
	public String getCurrentMode(String activeString) {
		//TODO
		return null;
	}

	@Override
	public Map<String, String> getActiveComponents() {
		//TODO
		return null;
	}

	@Override
	public String getContractByURI(String uri) {
		if (contractsReady.get(uri))
			synchronized(contracts) {
				return contracts.get(uri);
			}
		throw new RuntimeException("contract " + uri + " not ready, yet!");
	}

	private ILocalEnergyManager getRemoteLem(String uri) {
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
		return RemoteOsgiUtil.getRemoteService(remote, uri, ILocalEnergyManager.class);
	}

	/** helper method to get a bundle by its symbolic name **/
	private Bundle getBundle(BundleContext bundleContext, String symbolicName) {
		Bundle result = null;
		for (Bundle candidate : bundleContext.getBundles()) {
			if (candidate.getSymbolicName() != null
					&& candidate.getSymbolicName().equals(symbolicName)) {
				if (result == null
						|| result.getVersion()
								.compareTo(candidate.getVersion()) < 0) {
					result = candidate;
				}
			}
		}
		return result;
	}

	public void retrieveAndStartBundle(String symbolicName)
			throws IOException, BundleException {
		BundleContext bc = getBundleContext();

		String dirName = "plugins";
		File dir = new File(dirName);
		if(!dir.exists()) {
			dir.mkdir();
		}
		String fileName = "plugins/" + symbolicName + ".jar";
		log.debug("retrieving " + fileName + " ...");

		File f = new File(fileName);

		if (!f.exists()) {
			f.createNewFile();
			InputStream is = gem.getBundleBySymbolicName(symbolicName);
//			BufferedOutputStream bos = new BufferedOutputStream(
//					new FileOutputStream(fileName));
			FileOutputStream fos = new FileOutputStream(fileName);
//			int c;
//			while ((c = is.read()) != -1) {
//				bos.write(c);
//			}
//			bos.close();
			byte[] buffer = new byte[1024];
			int bytesRead;
			while((bytesRead = is.read(buffer)) != -1) {
				fos.write(buffer, 0, bytesRead);
			}
			fos.close();
			log.debug("bundle is now here: " + new File(fileName));
		}
		Bundle variantBundle = bc.installBundle(symbolicName,
				new FileInputStream(new File(fileName)));
		log.debug("[LEM] bundle "
						+ variantBundle
						+ " installed? "
						+ ((variantBundle.getState() == Bundle.INSTALLED || variantBundle.getState() == Bundle.ACTIVE) ? "true"
								: "false"));
		variantBundle.start();
		log.debug("[LEM] bundle "
				+ variantBundle
				+ " started? "
				+ ((variantBundle.getState() == Bundle.ACTIVE) ? "true"
						: "false"));
	}

	@Override
	public synchronized String computeContract(String appModelSer,
			String hwModelSer, String appName, String typeName, String contract,
			String implName, String benchName, String pluginName) {

		final File f = getFile(appName, typeName);
		StructuralModel appModel = null;
		StructuralModel hwModel = null;
		if(f.exists() && !settings.getLemForceContracts().get()) {
			log.debug("Found a contract for component " + typeName + ": " + implName);
			String ctr;
			try(FileInputStream fis = new FileInputStream(f)) {
				ctr = FileUtils.readAsString(fis, true);
			} catch (IOException shouldNotHappen) {return null;}

			// let see, whether the contract is ready
			try {
				appModel = CCMUtil.deserializeSWStructModel(appName, appModelSer, false);
				hwModel = CCMUtil.deserializeHWStructModel(hwModelSer, false);
				EclFile eclFile = CCMUtil.deserializeECLContract(appModel, hwModel, appName, typeName, ctr, false);
				boolean ready = contractReady(eclFile, typeName);
				if(ready)
					return ctr;
				log.debug("Found contract not ready yet");
			} catch (IOException e) {
				log.warn("Errors with deserializing", e);
			}
		}
		
		synchronized(contracts) {
		if (contracts.get(typeName) != null) {
			contract = contracts.get(typeName);
		}
		log.info("computing contract for "+implName);
		log.debug("Input Contract:\n"+contract);
		try {
			// run the benchmark...
			// find bundle: appName+"."+implNamesta
			// convention
			BundleContext bc = getBundleContext();
			//old
//			System.out.println("searching for: "+appName.toLowerCase() + "."
//					+ implName.toLowerCase());
//			Bundle variantBundle = getBundle(bc, appName.toLowerCase() + "."
//					+ implName.toLowerCase());
			log.debug("searching for: "+pluginName);
			Bundle variantBundle = getBundle(bc, pluginName);
			
			if (variantBundle == null) {
				// ask GEM for the app bundle and store it here
				String symbolicName = appName.toLowerCase();
				retrieveAndStartBundle(symbolicName);

				// ask GEM for the impl bundle and store it here
				symbolicName = pluginName;
				retrieveAndStartBundle(symbolicName);
				variantBundle = getBundle(bc, pluginName);
			}
			log.debug("found variantBundle " + variantBundle);
			
			if(appModel == null)
				appModel = CCMUtil.deserializeSWStructModel(
					appName, appModelSer, false);
			if(hwModel == null)
				hwModel = CCMUtil.deserializeHWStructModel(
					hwModelSer, false);
			EclFile template = CCMUtil.deserializeECLContract(appModel,
					hwModel, appName, typeName, contract, true);
			
//			if(benchName != null) {
//				log.debug(variantBundle+" benchName: "+benchName);
//				Class<?> benchClass = variantBundle.loadClass(benchName);
				Impl impl = apps.get(appName).getImplByName(implName);
				BenchmarkDriver driver = new BenchmarkDriver();
				Benchmark b = impl.getBenchmark();
				driver.setBenchmark(b);

				boolean foundContract = false;
				boolean didBenchmark  = false;
				for (EclContract c : template.getContracts()) {
					if (c instanceof SWComponentContract
							&& c.getName().replace('_','.').equals(implName)) {
						SWComponentContract sc = (SWComponentContract) c;
						foundContract = true;
						for (ContractMode m : sc.getModes()) {
							establishFixture(m);
							computeFunctions(m, implName, sc.getPort().getName(), driver);
							if(m instanceof SWContractMode) {
								SWContractMode hm = (SWContractMode)m;
								for(SWContractClause xsc : hm.getClauses()) {
									if(xsc instanceof HWComponentRequirementClause) {
										HWComponentRequirementClause hrc = (HWComponentRequirementClause)xsc;
										if(hrc.getRequiredResourceType().getName().equals("CPU")) {
											for(PropertyRequirementClause prc : hrc.getRequiredProperties()) {
												if(prc.getRequiredProperty().getDeclaredVariable().getName().equals("frequency")) {
													updateKnowledgePlane(implName, prc.getMinValue(), hrc.getEnergyRate());
												}
											}
										}
									}
								}
							}
						}
						didBenchmark = true;
					}
				}
				if(!foundContract) {
					log.warn("Could not find benchmark for " + implName);
					log.debug("Possible contracts:");
					for (EclContract c : template.getContracts())
						log.debug(" " + c.getName());
				}
				else if(!didBenchmark)
					log.warn("Could not complete benchmark for " + implName);
//			}

			String ctr = CCMUtil.serializeEObject(template);
			
			contracts.put(typeName, ctr);
			log.debug(ctr);

			// check if the contract is done
			boolean ready = contractReady(template);
			contractsReady.put(typeName, ready);
			
			FileWriter fw = null;
			try {
				fw = new FileWriter(f);
				fw.write(ctr);
			}
			catch(FileNotFoundException shouldNotHappen) {}
			try { if(fw!=null) {fw.close(); } } catch (IOException ignore) { }
			log.info("Contract persisted at " + f.getAbsolutePath());
			if(!ready) {
				log.debug("but not yet done..");
			}

			return ctr;
		} catch (Exception e) {
			log.error("something went terribly wrong for " + implName, e);
//			e.printStackTrace();
		}
		}//synchronized
		return null;

	}
	
	private void updateKnowledgePlane(String implName, Statement frequency, double power) {
		CcmExpressionInterpreter cci = new CcmExpressionInterpreter();
		try {
			gem.updateKnowledePlane(implName, CCMUtil.getLongValue(cci.interpret(frequency)), power);
		} catch(Exception e) {
			log.warn("baaaad ", e);
		}
	}

	/**
	 * @param appName the name of the application
	 * @param typeName the name of the component
	 * @return the filename for the specified contract
	 */
	private File getFile(String appName, String typeName) {
		final String folder = FileUtils.getWorkspaceDir();
		return new File(folder, appName + "_" + typeName + ".ecl");
	}

	private boolean contractReady(EclFile template, String... typeNamesToCheck) {
		//		boolean functionTemplateFound = false;
		final boolean checkNames = (typeNamesToCheck != null && typeNamesToCheck.length > 0);
		for (EclContract c : template.getContracts()) {
			if (c instanceof SWComponentContract) {
				SWComponentContract swc = (SWComponentContract) c;
				if(checkNames) {
					boolean found = false;
					for(String nameToCheck : typeNamesToCheck)
						if(nameToCheck != null && nameToCheck.equals(swc.getComponentType().getName())) {
							found = true;
							break;
						}
					if(!found) {
						continue;
					}
				}
				for (SWContractMode m : swc.getModes()) {
					for (SWContractClause cl : m.getClauses()) {
						if (cl instanceof ProvisionClause) {
							if (((ProvisionClause) cl).getFormula() != null) {
//								functionTemplateFound = true;
								return false;
							}
						} else if (cl instanceof HWComponentRequirementClause) {
							for (PropertyRequirementClause prc : ((HWComponentRequirementClause) cl)
									.getRequiredProperties()) {
								if (prc.getFormula() != null)
									return false;
							}
						} else if (cl instanceof SWComponentRequirementClause) {
							for (PropertyRequirementClause prc : ((SWComponentRequirementClause) cl)
									.getRequiredProperties()) {
								if (prc.getFormula() != null)
									return false;
							}
						}
					}
				}
			} // TODO HW Contracts
		}
		return true;
	}

	private void establishFixture(ContractMode m) {
		// TODO implement
	}

	private File runBenchmark(BenchmarkDriver bench, String implName, String[] nfpNames) {
		for(String nfpName : nfpNames) {
			Monitor m = createNewMonitor(nfpName);
			if(m != null) {
				bench.addMonitor(m);
			}
		}
		File benchData = new File(FileUtils.getWorkspaceDir(), implName + ".bench");
		// System.out.println("BenchData here: " + benchData.getAbsolutePath());
		bench.setBenchData(benchData);
		bench.run();
		return benchData;
	}
	
	/**
	 * Creates a new monitor to observe the nfp with the given name.
	 * @param nfpName the name of the nfp to observe
	 * @return a new or reused monitor
	 */
	private Monitor createNewMonitor(String nfpName) {
		Monitor result = monitorRepo.getMonitor(nfpName);
		if(result instanceof RemoteMonitor) {
			((RemoteMonitor) result).setHostIp(RemoteOsgiUtil.stripRosgiParts(getContainerId()));
		}
		return result;
	}

	private void computeFunctions(ContractMode m, String implName, String portName,
			BenchmarkDriver bench) {
		Set<Variable> metaparams = new HashSet<Variable>();
		Iterator<PortType> it = ((SWComponentContract) m.eContainer()).getComponentType().getPorttypes().iterator();
		while (it.hasNext()) {
			PortType pt = it.next();
			if(pt.getName().equals(portName))
				for(Parameter p : pt.getMetaparameter())
					metaparams.add(p);
		}
		List<TemplateNfpName> templatesTodo = new ArrayList<TemplateNfpName>();

		if (m instanceof SWContractMode) {
			log.debug("Computing functions for mode " + m.getName());
			SWContractMode scm = (SWContractMode) m;
			for (SWContractClause cl : scm.getClauses()) {
				if (cl instanceof ProvisionClause) {
					ProvisionClause pc = (ProvisionClause) cl;
					if(pc.getFormula() != null) {
						Property nfp = pc.getProvidedProperty();
						FormulaTemplate ft = pc.getFormula();
						templatesTodo.add(new TemplateNfpName(ft, nfp, pc));
						pc.setFormula(null);
					} else { log.debug("Ignoring non-formula " + pc); }
				} else if (cl instanceof HWComponentRequirementClause) {
					HWComponentRequirementClause hc = (HWComponentRequirementClause) cl;
					for (PropertyRequirementClause prc : hc
							.getRequiredProperties()) {
						if (prc.getFormula() != null) {
							Property nfp = prc.getRequiredProperty();
							FormulaTemplate ft = prc.getFormula();
							log.debug(nfp);
							templatesTodo.add(new TemplateNfpName(ft, nfp, prc, hc));
							prc.setFormula(null);
						} else { log.debug("Ignoring non-formula " + prc); }
					}
				} else if (cl instanceof SWComponentRequirementClause) {
					SWComponentRequirementClause sc = (SWComponentRequirementClause) cl;
					for (PropertyRequirementClause prc : sc
							.getRequiredProperties()) {
						if (prc.getFormula() != null) {
							Property nfp = prc.getRequiredProperty();
							FormulaTemplate ft = prc.getFormula();
							log.debug(nfp);
							templatesTodo.add(new TemplateNfpName(ft, nfp, prc));
							prc.setFormula(null);
						} else { log.debug("Ignoring non-formula " + prc); }
					}
				}
			}
			FormulaTemplate[] templates = new FormulaTemplate[templatesTodo.size()];
			String[] nfpNames = new String[templatesTodo.size()];
			HWComponentRequirementClause[] hcs = new HWComponentRequirementClause[templatesTodo.size()];
					
			// prepare template and nfpName list
			for (int i = 0; i < templates.length; i++) {
				TemplateNfpName tnn = templatesTodo.get(i);
				templates[i] = tnn.ft;
				nfpNames[i] = tnn.nfpName;
				hcs[i] = tnn.hwcrc;
			}
			Statement[] statements = getExpressionsForFunctionTemplates(implName, bench, templatesTodo, nfpNames, metaparams);
			for (int i = 0; i < statements.length; i++) {
				Statement st = statements[i];
				TemplateNfpName tnn = templatesTodo.get(i);
				if(tnn.nfp.getValOrder().equals(Order.INCREASING)) {
					if(tnn.pc != null) {
						tnn.pc.setMaxValue(st);
					}
					else {
						// use tnn.prc
						tnn.prc.setMinValue(st);
					}
				}
				else if (tnn.nfp.getValOrder().equals(
						Order.DECREASING)) {
					if(tnn.pc != null) {
						tnn.pc.setMinValue(st);
					}
					else {
						// use tnn.prc
						tnn.prc.setMaxValue(st);
					}
				}
			}
		}
		else {
			log.warn("No software mode: " + m.getName());
		}
	}
	
	
	private long setfreq(long newfreq) throws Exception {
		ProcessBuilder pb = new ProcessBuilder("/bin/bash","-c","for x in {0..7}; do echo "+newfreq+" > /sys/devices/system/cpu/cpu$x/cpufreq/scaling_max_freq; done");
		pb.start();
		pb = new ProcessBuilder("/bin/bash","-c","for x in {0..7}; do echo "+newfreq+" > /sys/devices/system/cpu/cpu$x/cpufreq/scaling_min_freq; done");
		pb.start();
		
		Thread.sleep(1000);
		
		pb = new ProcessBuilder("/bin/bash","-c","cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq");
		Process p = pb.start();
		String ret = "";
		BufferedReader isr = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while((line = isr.readLine()) != null) {
			ret+=line;
		}
		long curfreq = Long.parseLong(ret);
		return curfreq;
	}

	public static class TemplateNfpName {
		public FormulaTemplate ft;
		public String nfpName;
		public Property nfp;
		/** May be <code>null</code> */
		public ProvisionClause pc;
		/** May be <code>null</code> */
		public PropertyRequirementClause prc;
		public HWComponentRequirementClause hwcrc;
//		/** May be <code>null</code> */
//		public SWComponentRequirementClause swcrc;
		private TemplateNfpName(FormulaTemplate ft, Property nfp) {
			this.ft = ft;
			this.nfp = nfp;
			this.nfpName = nfp.getDeclaredVariable().getName();
		}
		/** Using ProvisionClause */
		public TemplateNfpName(FormulaTemplate ft, Property nfp, ProvisionClause pc) {
			this(ft, nfp);
			this.pc = pc;
			this.prc = null;
		}
		/** Using PropertyRequirementClause */
		public TemplateNfpName(FormulaTemplate ft, Property nfp, PropertyRequirementClause prc) {
			this(ft, nfp);
			this.pc = null;
			this.prc = prc;
		}
		/** Using PropertyRequirementClause */
		public TemplateNfpName(FormulaTemplate ft, Property nfp, PropertyRequirementClause prc, HWComponentRequirementClause hwcrc) {
			this(ft, nfp);
			this.pc = null;
			this.prc = prc;
			this.hwcrc  = hwcrc;
		}
	}

	private Statement[] getExpressionsForFunctionTemplates(String implName,
			BenchmarkDriver bench, List<TemplateNfpName> templates, String[] nfpNames,
			Set<Variable> metaparams) {
		log.info("Starting benchmark. implName="+implName+", nfs="+Arrays.toString(nfpNames));
		Statement[] result = new Statement[templates.size()];
		File benchData = null;
		int lastfreq = 0;
		IBenchmarkUtil bu = getBenchmarkUtil();
		bu.setUseEureqa(useEureqa);
		List<String> metaparamsStringList= new ArrayList<String>(metaparams.size());
		for(Variable var : metaparams) {
			metaparamsStringList.add(var.getName());
		}
		
		for (int i = 0; i < templates.size(); i++) {
			
			if(templates.get(i).hwcrc != null) {
				for(PropertyRequirementClause prc : templates.get(i).hwcrc.getRequiredProperties()) {
					if(prc.getRequiredProperty().getDeclaredVariable().getName().equals("frequency")) {
						CcmExpressionInterpreter in = new CcmExpressionInterpreter();
						long val = CCMUtil.getLongValue(in.interpret(prc.getMinValue()));
						long newfreq = Math.round(val)*1000;
						if(newfreq != lastfreq) {
							try {
								long r = setfreq(newfreq);
								log.info("Set frequency to "+r+" (goal: "+newfreq+")");
							} catch (Exception e) {
								log.debug("setting frequency did not work - are you root?");
							}
							benchData = runBenchmark(bench, implName, nfpNames);
						} 
					}
				}
			}
			if(benchData == null) {
				benchData = runBenchmark(bench, implName, nfpNames);
			}
			if(log.isDebugEnabled()) {
				FormulaTemplate ft = templates.get(i).ft;
				log.debug("function <" + (ft != null ? ft.getName() : "null") + ">: ");
			}
			FileInputStream fis = null;
			String expression;
			try {
				fis = new FileInputStream(benchData);
			} catch (FileNotFoundException e) {
				log.warn("Could not find file", e);
			}
			if(fis != null) {
				expression = bu.getExpressionsForFunctionTemplate(fis, templates.get(i).nfpName,
						i, nfpNames.length, metaparamsStringList);
				try { fis.close(); } catch(IOException ignore) {}
			}
			else {
				expression = "-1";
			}
			Statement stmt;
			if(expression.equals("-1")) {
				// either FileNotFoundException or error during expression calculation
				IntegerLiteralExpression ile = LiteralsFactory.eINSTANCE.createIntegerLiteralExpression();
				ile.setValue(-1);
				stmt = ile;
			}
			else {
				stmt = parseExpression(expression, metaparams);
			}
			result[i] = stmt;
		}// end for(i : templates.length)
		return result;
	}

	private Statement parseExpression(String exp, Set<Variable> metaparams) {
		Statement e = null;
		ExpResource res = ParserFacade.parseStatement(exp, metaparams);
		if (res.getErrors().size() > 0)
			log.warn("Could not parse expression " + exp);
		else
			e = (Statement) res.getContents().get(0);
		return e;
	}

	private IBenchmarkUtil getBenchmarkUtil() {
		String type;
		if(settings.getLemContractsAtGem().get()) {
//			// fetch bu from gem
			// testing r-osgi-interupt
			RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(getBundleContext());
			IBenchmarkUtil bu = RemoteOsgiUtil.getRemoteService(remote, settings.getGemUri().get(), IBenchmarkUtil.class);
			if(bu != null) {
				log.info("Using remote benchmark util");
				return bu;
			}
			type = IBenchmarkUtil.TYPE_PROXY;
		}
		else {
			// use local. create it, if necessary
			type = IBenchmarkUtil.TYPE_LOCAL;
		}
		Collection<ServiceReference<IBenchmarkUtil>> refs;
		log.debug("Searching for benchmark util with type = " + type);
		try {
			refs = context.getServiceReferences(IBenchmarkUtil.class,
					"(" + IBenchmarkUtil.PROPERTY_TYPE + "=" + type + ")");
		} catch (InvalidSyntaxException e) {
			log.error("Could not get benchmark util service with type = " + type, e);
			return null;
		}
		for(ServiceReference<IBenchmarkUtil> ref : refs) {
			if(ref != null) {
				return context.getService(ref);
			}
		}
		log.error("No suitable benchmark util service found with type = " + type);
		return null;
	}

	@Override
	public Serializable execute(String pluginName, String implName, String portName,
			String appName, Boolean compIsConnector, Boolean compIsCopyType, Long taskId, Long jobId,
			List<String> portTypes, Object... params) {
		int callId = getNextCallId();
		log.debug(appName+" | "+implName+" | "+portName +" | "+jobId);
		printQueue();
		createNewResultObject(callId, jobId, implName, compIsConnector, compIsCopyType);
		try {
			// XXX Could also use service with special properties
			Impl implObj = apps.get(appName).getImplByName(implName);
//			Class<?> implClass = loadClass(pluginName, implName);
//			Object implObj = implClass.newInstance();
			Class<?> implClass = implObj.getClass();
			log.debug("implObject=" + implObj + " : " + Arrays.toString(implClass.getInterfaces()));
			if(implObj instanceof TaskBasedImpl) {
				((TaskBasedImpl) implObj).setTaskId(taskId);
				log.debug("Set task id to " + taskId);
			}
			//TODO fetch interface desc from port type
			log.debug("expected portTypes: "+portTypes);
			Class<?>[] types = new Class<?>[portTypes.size()];
			int i = 0;
			for(String parType : portTypes) {
				String[] splitted = parType.split(":");
				String name = splitted[0].trim();
				String type = splitted[1].trim();
				if(name.startsWith("fileproxy")) {
					types[i] = FileProxy.class;
				} else if(name.startsWith("file")) {
					types[i] = java.io.File.class;
				}
				if(type.toLowerCase().equals("integer")) {
					types[i] = Integer.TYPE; // primitive type
				} else if(type.toLowerCase().equals("file")) {
					types[i] = java.io.File.class;
				} else if(type.toLowerCase().equals("string")) {
					types[i] = String.class;
				} else if(type.toLowerCase().equals("double")) {
					types[i] = Double.TYPE; // primitive type
				} else if(type.toLowerCase().equals("long")) {
					types[i] = Long.TYPE; // primitive type
				} else if(type.toLowerCase().equals("boolean")) {
					types[i] = Boolean.TYPE; // primitive type
				} else if(type.toLowerCase().equals("array")) {
					types[i] = List.class;
				}
				if(types[i] == null) {
					types[i] = Object.class;
				}
				if(types[i].equals(java.io.File.class) && params[i] instanceof FileProxy) {
					FileProxy proxy = (FileProxy) params[i];
					// get the file, possibly copying it first
					params[i] = proxy.getFileFor(containerUri);
				}
				i++;
			}
			log.debug("actual portTypes: " + Arrays.toString(types));
//			for(Class<?> t : types) System.out.println(t.getName());	
			Method port = implClass.getMethod(portName, types);
//			//TO_DO ask for args
//			Object[] args = new Object[types.length]; 
//			for(i = 0; i < types.length; i++) {
//				if(types[i].equals("Integer")) {
//					args[i] = (int) (Math.random()*100);
//				}
//			}
			log.debug("actual portParams: "+Arrays.asList(params));
//			log.debug(params[0].getClass().getName());
			
			waitForQueue(callId);
			
			Object result = port.invoke(implObj, params);
			log.debug("result = " + result);
			// NotSerializableException is caught and returned
			if(result instanceof File) {
				result = new FileProxy((File) result, containerUri);
			}
			updateResult(callId, (Serializable) result);
		} catch (Throwable t) {
			// unwrap Throwable once, if possible
			// to get real exception and not InvocationTargetException
			if(t.getCause() != null) {
				t = t.getCause();
			}
			log.warn("exception on execute", t);
			updateResult(callId, t);
//			System.out.println(e.getMessage());
		}
		return getResult(jobId);
	}

	protected Class<?> loadClass(String pluginName, String implName) throws ClassNotFoundException {
		BundleContext bc = getBundleContext();
		Bundle b = getBundle(bc, pluginName);
		return b.loadClass(implName);
	}

	protected BundleContext getBundleContext() {
		return FrameworkUtil.getBundle(getClass())
				.getBundleContext();
	}
	
	Lock queueLock = new ReentrantLock();
	Condition jobFinished = queueLock.newCondition();
	private ServiceRegistration<ILocalEnergyManager> reg;
	private BundleContext context;
	private MonitorRepository monitorRepo;
	
	/**
	 * Waits until the queue either is empty or only contains jobs scheduled after
	 * myCallId.
	 * @param myCallId the callId of the current job
	 */
	private void waitForQueue(int myCallId) {
		queueLock.lock();
		boolean queueIsBusy = queueIsBusy(myCallId);
		while(queueIsBusy) {
			try {
				// wait 10 seconds than check again if queue is still busy
				// this is to avoid missed jobFinished calls
				jobFinished.await(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) { }
			queueIsBusy = queueIsBusy(myCallId);
		}
		queueLock.unlock();
	}

	private boolean queueIsBusy(int myCallId) {
		log.debug("Checking queueIsBusy " + myCallId);
		Result me = getResultByCallId(myCallId);
		if(me.isCopyType) {
			// copyType does not respect queue
			return false;
		}
		synchronized (results) {
			for(Result r : results) {
				if(!r.finished && !r.isConnector && r.callId < myCallId) {
					// if queue slot is not finished, is no connector and as a smaller callId, then block
					log.debug("Still busy for " + myCallId + ", because of " + r.callId);
					return true;
				}
			}
		}
		return false;
	}

	private void printQueue() {
		synchronized (results) {
			log.debug(results);
		}
	}

	public int getNextCallId() {
		int nextId;
		// use same object for locking
		synchronized (results) {
			nextId = callIdCounter++;
		}
		return nextId;
	}
	
	private void createNewResultObject(int callId, long jobId, String implName,
			boolean isConnector, boolean isCopyType) {
		log.debug("new with id=" + callId);
		Result r = new Result(callId, jobId, implName, isConnector, isCopyType);
		synchronized (results) {
			results.add(r);
		}
	}
	
	private Result getResultByCallId(int callId) {
		Result found = NO_RESULT;
		synchronized (results) {
			for(Result r : results) {
				if(r.callId == callId) {
					found = r;
					break;
				}
			}
		}
		return found;
	}
	
	private Result getResultByJobId(long jobId) {
		Result found = NO_RESULT;
		synchronized (results) {
			for(Result r : results) {
				if(r.jobId == jobId) {
					found = r;
					break;
				}
			}
		}
		return found;
	}
	
	@Override
	public boolean isFinished(long jobId) {
		Result r = getResultByJobId(jobId);
		return r.finished;
	}
	
	private void updateResult(int callId, Serializable result) {
		log.debug(callId + ": new=" + result);
		Result r = getResultByCallId(callId);
		if(r == NO_RESULT)
			return;
		
		synchronized (results) {
			r.finished = true;
			r.result = result;
		}
		
		// informs waiting threads that a job has been finished
		queueLock.lock();
		jobFinished.signalAll();
		queueLock.unlock();
	}
	
	@Override
	public Serializable getResult(long jobId) {
		Result r = getResultByJobId(jobId);
		log.debug("jobId " + jobId + ": " + (r == NO_RESULT ? "NO_RESULT" : r.result));
		return r.result;
	}
	
	@Override
	public void removeResult(long jobId) {
		int index = -1;
		synchronized (results) {
			for (int i = 0; i < results.size(); i++) {
				if(results.get(i).jobId == jobId) {
					index = i;
					break;
				}
			}
			if(index != -1) {
				results.remove(index);
			}
		}
		log.debug(jobId + ": index=" + index);
	}
	
	@Override
	public long[] getJobIdsInQueue() {
		long[] jobIds;
		synchronized (results) {
			jobIds = new long[results.size()];
			for (int i = 0; i < jobIds.length; i++) {
				jobIds[i] = results.get(i).jobId;
			}
		}
		return jobIds;
	}
	
	@Override
	public String getImplName(long jobId) {
		Result r = getResultByJobId(jobId);
		return r.implName;
	}

	@Override
	public void updateContract(String appModelSer, String hwModelSer,
			String appName, String eclURI, String newSerializedContract) {
		log.info("Update contract for " + eclURI + " in " + appName);
		File f = getFile(appName, eclURI);

		// deserialize models and new ecl file
		EclFile newEclFile;
		StructuralModel appModel, hwModel;
		try {
			appModel = CCMUtil.deserializeSWStructModel(appName, appModelSer, false);
			hwModel = CCMUtil.deserializeHWStructModel(hwModelSer, false);
			newEclFile = CCMUtil.deserializeECLContract(appModel , hwModel , appName,
					eclURI, newSerializedContract, false);
		}
		catch(IOException ioe) {
			log.warn("Error during deserialization: ", ioe);
			return;
		}

		synchronized(contracts) {
			if(f.exists()) {
				// some or all values of the formulas have already been calculated
				// --> merge these values into the new contract
				String oldSerializedContract = contracts.get(eclURI);
				if(oldSerializedContract == null) {
					try (FileInputStream fis = new FileInputStream(f)) {
						oldSerializedContract = FileUtils.readAsString(fis, true);
					} catch (IOException ignore) { }
				}
				try {
					EclFile oldEclFile = CCMUtil.deserializeECLContract(appModel , hwModel , appName,
							eclURI, oldSerializedContract, false);
					merge(oldEclFile, newEclFile);
					newSerializedContract = CCMUtil.serializeEObject(newEclFile);
				}
				catch(IOException ioe) {
					log.warn("Error during (de)serialization: ", ioe);
					return;
				}
			}
		
			// update the contract
			contracts.put(eclURI, newSerializedContract);
			log.debug("new:" + newSerializedContract);

			// check if the contract is done
			boolean ready = contractReady(newEclFile);
			contractsReady.put(eclURI, ready);
			
			// rs: always persist eclfile, as it is checked if ready at beginning of the computeContract-method
			Writer fw = null;
			try {
				fw = new FileWriter(f);
				fw.write(newSerializedContract);
			}
			catch(IOException ioe) {
				log.warn("Error while writing: ", ioe);
			}
			finally {
				if(fw != null) try { fw.close(); } catch (IOException ignore) {}
			}
			log.info("Contract persisted at " + f.getAbsolutePath());
			if(!ready) {
				log.debug("but not yet done..");
			}
		}
	}

	/** Copy/move properties from the old to the new ecl file */
	private void merge(EclFile oldEclFile, EclFile newEclFile) {
		for(EclContract oldContract : oldEclFile.getContracts()) {
			String oldContractName = oldContract.getName();
			// find matching new contract
			EclContract newContract = null;
			for(EclContract newContractTest : newEclFile.getContracts()) {
				if(newContractTest.getName().equals(oldContractName)) {
					newContract = newContractTest;
					break;
				}
			}
			if(newContract == null) {
				log.debug("No matching new contract found for " + oldContractName);
				continue;
			}
			if(oldContract instanceof SWComponentContract) {
				SWComponentContract oldSwcc = (SWComponentContract) oldContract;
				SWComponentContract newSwcc = (SWComponentContract) newContract;
				for(SWContractMode oldMode : oldSwcc.getModes()) {
					String oldModeName = oldMode.getName();
					// find matching new mode
					SWContractMode newMode = null;
					for(SWContractMode newModeTest : newSwcc.getModes()) {
						if(newModeTest.getName().equals(oldModeName)) {
							newMode = newModeTest;
							break;
						}
					}
					if(newMode == null) {
						log.warn("No matching new mode found for " + oldModeName);
						break;
					}
					for(SWContractClause oldClause : oldMode.getClauses()) {
						mergeClause(oldClause, newMode.getClauses());
					}
				}
			}
		}
	}

	/** Dispatch class of clause, match with one of the new clauses
	 *  and copy/move properties from the old to the new clause */
	private void mergeClause(SWContractClause oldClause,
			EList<SWContractClause> newClauses) {
		if(oldClause instanceof HWComponentRequirementClause) {
			HWComponentRequirementClause oldhwcrc = (HWComponentRequirementClause) oldClause;
			for(SWContractClause newClause : newClauses) {
				if(newClause instanceof HWComponentRequirementClause) {
					HWComponentRequirementClause newhwcrc = (HWComponentRequirementClause) newClause;
					if(match(oldhwcrc.getRequiredResourceType(), newhwcrc.getRequiredResourceType())) {
						newhwcrc.setEnergyRate(oldhwcrc.getEnergyRate());
						copyList(oldhwcrc.getRequiredProperties(), newhwcrc.getRequiredProperties());
						break;
					}
				}
			}
		}
		else if(oldClause instanceof SWComponentRequirementClause) {
			SWComponentRequirementClause oldswcrc = (SWComponentRequirementClause) oldClause;
			for(SWContractClause newClause : newClauses) {
				if(newClause instanceof SWComponentRequirementClause) {
					SWComponentRequirementClause newswcrc = (SWComponentRequirementClause) newClause;
					if(match(oldswcrc.getRequiredComponentType(), newswcrc.getRequiredComponentType())) {
						copyList(oldswcrc.getRequiredProperties(), newswcrc.getRequiredProperties());
					}
				}
			}
		}
		else if(oldClause instanceof ProvisionClause) {
			ProvisionClause oldPc = (ProvisionClause) oldClause;
			for(SWContractClause newClause : newClauses) {
				if(newClause instanceof ProvisionClause) {
					ProvisionClause newPc = (ProvisionClause) newClause;
					if(matchString(oldPc.getProvidedProperty().getDeclaredVariable().getName(),
							newPc.getProvidedProperty().getDeclaredVariable().getName())) {
						newPc.setFormula(oldPc.getFormula());
						newPc.setMaxValue(oldPc.getMaxValue());
						newPc.setMinValue(oldPc.getMinValue());
					}
				}
			}
		}
	}
	
	/** Copy/Move elements from old list to new list. Clears new list beforehand. */
	private <T> void copyList(EList<T> oldList, EList<T> newList) {
		newList.clear();
		for(T oldElement : wrapList(oldList)) {
			newList.add(oldElement);
		}
	}
	
	/** @return new ArrayList<T>(oldList) */
	private <T> Iterable<T> wrapList(EList<T> oldList) {
		return new ArrayList<T>(oldList);
	}

	/** @return matchString(o1.getId(), o2.getId()) || matchString(o1.getName(), o2.getName()) */
	private <T extends IdentifyableElement & NamedElement> boolean match(T o1, T o2) {
		return matchString(o1.getId(), o2.getId()) || matchString(o1.getName(), o2.getName());
	}

	/** <code>null</code>-save string matching. */
	private boolean matchString(String o1, String o2) {
		if(o1 == null)
			return false;
		return o1.equals(o2);
	}

	@Override
	public boolean reset(boolean full) {
		log.info("Resetting now.");
		synchronized (results) {
			results.clear();
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return "LEM@"+PlatformUtils.getHostName();
	}
	
	class AppInfo {
		App app;
		boolean registeredWithGem = false;
		boolean removed = false;
		List<Impl> impls = new ArrayList<>();
		
		public Impl getImplByName(String implName) {
			for(Impl i : impls) {
				if(i.getVariantName().equals(implName)) {
					return i;
				}
			}
			return null;
		}
	}

	protected void addApp(final App app) {
		log.debug("Add app: " + app);
		new Thread(new InitApp(app)).start();
	}
	
	class InitApp implements Runnable {
		private App app;
		public InitApp(App app) {
			this.app = app;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			int implsTodo = 0;
			try {
				appLock.tryLock(10, TimeUnit.SECONDS);
				AppInfo info = apps.get(app.getName());
				log.debug("Fetched info: " + info);
				if(info != null) {
					if(info.app == null) {
						// an impl was added first, move on with registering
						info.app = app;
					} else {
						// app known and already registered (or in progress of being registered)
						log.info("App already known to lem: " + app.getName());
						return;
					}
				} else {
					info = new AppInfo();
					info.app = app;
					apps.put(app.getName(), info);
				}
				log.info("Begin register app " + app.getName());
				implsTodo = info.impls.size();
				registerApp(app);
				info.registeredWithGem = true;
				// if some impls were already added, register them now
				for(Impl impl : info.impls) {
					registerImpl(impl);
				}
			} catch (Exception e) {
				log.error("Could not register app " + app.getName() + " with " + implsTodo + " impls", e);
			} finally {
				appLock.unlock();
			}
		}
	}

	/**
	 * @param app
	 * @throws IOException 
	 */
	private void registerApp(App app) throws Exception {
		log.debug("entry");

		//read model and pass as String
		String appName = app.getName();
		BundleContext context = Platform.getBundle(app.getBundleSymbolicName()).getBundleContext();
		String modelPath = app.getApplicationModelPath();
		log.debug(modelPath+" "+context);
		log.debug(context.getBundle().getEntry(modelPath));
		String model;
		/*try(*/InputStream in = context.getBundle().getEntry(modelPath).openStream();
//				) {
			model = FileUtils.readAsString(in, true);
//		} catch(Exception e) {
//			log.warn("Exception reading the model at " + modelPath, e);
//		}
		
		StructuralModel m = CCMUtil.deserializeSWStructModel(appName, model, false);
		SWComponentType r = (SWComponentType)m.getRoot();
		Map<String,String> appContracts = new HashMap<String, String>();
		for(SWComponentType sub : r.getSubtypes()) {
			String compName = //fileName.substring(fileName.lastIndexOf("/")+1,fileName.lastIndexOf("."));
					sub.getName();
			String eclUri = sub.getEclUri();
			if(eclUri == null || eclUri.isEmpty()) {
				log.warn("Ecl uri of " + compName + " is null or empty. Ignoring it.");
				continue;
			}
			int pos = eclUri.lastIndexOf("/");
			String fileName = eclUri.substring(pos+1);
			File contractDir = new File(app.getContractsPath());
			String contractPath = new File(contractDir,fileName).getPath();
			log.debug("Loading " + contractPath);
			InputStream eclIn = context.getBundle().getEntry(contractPath).openStream();
			String eclModel = FileUtils.readAsString(eclIn, true);
			try { eclIn.close(); } catch (IOException ignore) {}
			if(compName.contains("_")) compName = compName.substring(compName.lastIndexOf("_")+1);
			appContracts.put(compName,eclModel);
			log.info(compName+" :: "+StringUtils.cap(eclModel, 400));
		}
		
		log.info("Call gem.registerApp for " + app.getName());
		gem.registerApp(appName, containerUri, model, appContracts);
	}

	protected void removeApp(App app) {
		AppInfo info = apps.get(app.getName());
		if(info == null) {
			log.warn("Removing unknown app: " + app.getName());
		} else {
			info.removed = true;
		}
	}
	
	protected void addImpl(final Impl impl) {
		log.debug("Add impl: " + impl);
		new Thread(new InitImpl(impl)).start();
	}
	
	class InitImpl implements Runnable {
		private Impl impl;
		public InitImpl(Impl impl) {
			this.impl = impl;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			String appName = impl.getApplicationName();
			try {
				appLock.tryLock(10, TimeUnit.SECONDS);
				AppInfo info = apps.get(appName);
				log.debug("Fetched info: " + info);
				if(info == null) {
					// create new info, store impl for later (will be picked up by at app registration)
					info = new AppInfo();
					info.impls.add(impl);
					apps.put(appName, info);
					log.debug("Created new info.");
				} else {
					info.impls.add(impl);
					// app already known, start registering
					registerImpl(impl);
				}
			} catch (Exception e) {
				log.error("Could not register impl " + impl.getVariantName(), e);
			} finally {
				appLock.unlock();
			}
		}
	}

	/**
	 * @param impl
	 */
	private void registerImpl(Impl impl) {
		log.info("Call gem.registerVariant at " + gem.getUri() + " for " + impl.getVariantName());
		// XXX remove benchmark class name?
		gem.registerVariant(impl.getApplicationName(), impl.getComponentType(), impl.getVariantName(),
				null, getContainerId(), impl.getPluginName());
	}

	protected void removeImpl(Impl impl) {
		// empty by now
	}
	
	protected void setTheatreSettings(TheatreSettings s) {
		this.settings = s;
	}
	
	protected void unsetTheatreSettings(TheatreSettings s) {
		this.settings = null;
	}
	
	protected void setMonitorRepo(MonitorRepository mr) {
		this.monitorRepo = mr;
	}
	
	protected void unsetMonitorRepo(MonitorRepository mr) {
		this.monitorRepo = null;
	}
	
	protected void activate(ComponentContext ctx) {
		log.info("LEM activating");
		context = ctx.getBundleContext();
		setupRemotePresence(ctx);
		startCheckForNewMaster(ctx);
		sentReadyIn(5, TimeUnit.SECONDS);
	}

	/**
	 * Call {@link #gem}.setReady({@link #containerUri}) after the specified time span
	 * @param the maximum time to wait
	 * @param unit the time unit of the time argument
	 */
	private void sentReadyIn(long time, TimeUnit unit) {
		new Thread(new SentDelayedReady(time, unit)).start();
	}
	
	class SentDelayedReady implements Runnable {
		private long time;
		private TimeUnit unit;

		public SentDelayedReady(long time, TimeUnit unit) {
			this.time = time;
			this.unit = unit;
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			long millis = TimeUnit.MILLISECONDS.convert(time, unit);
			try {
				Thread.sleep(millis);
			} catch(InterruptedException ignore) {}
			if(gem != null) {
				gem.setReady(containerUri);
			}
		}
		
	}

	/**
	 * Publish remote interface and connect to GEM
	 * @param ctx the component context to get the bundleContext
	 */
	private void setupRemotePresence(ComponentContext ctx) {
		log.info("Remote registering using uri = "+ containerUri);
		reg = RemoteOsgiUtil.register(ctx.getBundleContext(), ILocalEnergyManager.class, this);
		// add a shutdown hook to unregister LEM
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				if(gem != null) {
					gem.unregisterLem(containerUri);
				}
			}
		}));
	}
	
	protected void deactivate(ComponentContext ctx) {
		log.info("Deactivating LEM");
		if(gem != null) {
			gem.unregisterLem(containerUri);
		}
		if(reg != null) {
			reg.unregister();
		}
	}
}
