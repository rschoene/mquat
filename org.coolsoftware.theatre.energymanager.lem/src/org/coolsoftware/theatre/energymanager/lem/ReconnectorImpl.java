/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.lem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.util.Reconnectable;
import org.coolsoftware.theatre.util.Reconnector;

/**
 * Simple reconnector with special order.
 * XXX Currently not supported.
 * <ol>
 * <li>{@link TaskRepositoryActivator}</li>
 * <li>{@link LEMActivator}</li>
 * <li>{@link AppActivator}</li>
 * <li>{@link ImplActivator}</li>
 * <li>The rest</li>
 * <li>The ready signal for the gem</li>
 * </ol>
 * @author René Schöne
 */
public class ReconnectorImpl implements Reconnector {

	private List<Reconnectable> reconnectables = new ArrayList<>();
	private Logger log = Logger.getLogger(ReconnectorImpl.class);

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Reconnector#registerForReconnect(org.coolsoftware.theatre.util.Reconnectable)
	 */
	@Override
	public void registerForReconnect(Reconnectable r) {
		reconnectables.add(r);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Reconnector#unregisterForReconnect(org.coolsoftware.theatre.util.Reconnectable)
	 */
	@Override
	public void runRegisteredReconnectables() {
		List<Reconnectable> todo = new ArrayList<>(reconnectables);
//		run(todo, TaskRepositoryActivator.class);
//		run(todo, LEMActivator.class);
//		run(todo, AppActivator.class);
//		run(todo, ImplActivator.class);
		// run the rest
		if(!todo.isEmpty()) {
			run(todo, null);
		}
		// make signal to gem
//		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
//		IGlobalEnergyManager gem = RemoteOsgiUtil.getRemoteService(remote,
//				AppUtil.getGEMUri(), IGlobalEnergyManager.class);
//		gem.setReady(URI.create(lem.getContainerId()));
	}
	
	private void run(List<Reconnectable> todo,
			Class<? extends Reconnectable> clazz) {
		for (Iterator<Reconnectable> iter = todo.iterator(); iter.hasNext(); ) {
		    Reconnectable r = iter.next();
		    if(clazz == null || clazz.isInstance(r)) {
		    	try {
					r.reconnect();
				} catch (Exception e) {
					log.debug("Got " + e.getClass() + ":" + e.getMessage());
				}
		    	iter.remove();
		    }
		}
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Reconnector#unregisterForReconnect(org.coolsoftware.theatre.util.Reconnectable)
	 */
	@Override
	public void unregisterForReconnect(Reconnectable r) {
		reconnectables.remove(r);
	}


}
