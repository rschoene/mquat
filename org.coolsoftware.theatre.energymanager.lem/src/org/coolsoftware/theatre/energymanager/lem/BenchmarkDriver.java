/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.lem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.api.Monitor;

/**
 * A driver to execute a benchmark. Iterates over a number of input values and runs and iteration for each of them.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class BenchmarkDriver implements Runnable {
	
	private static final int STATE_CONFIGURE = 1;
	private static final int STATE_RUNNING = 2;
	private static final int STATE_FINISHED = 3;
	
	protected int iterations = 5;
	
	List<Monitor> monitors = new ArrayList<Monitor>();
	private File benchLog;
	private Benchmark bench;
	private int state;
	private Logger log = Logger.getLogger(BenchmarkDriver.class);
	
	public BenchmarkDriver() {
		state = STATE_CONFIGURE;
	}
	
	public void setBenchmark(Benchmark b) {
		if(state != STATE_CONFIGURE) {
			throw new IllegalStateException("Not in configuration state.");
		}
		this.bench = b;
	}
	
	public void setBenchData(File f) {
		if(state != STATE_CONFIGURE) {
			throw new IllegalStateException("Not in configuration state.");
		}
		this.benchLog = f;
	}
	
	public void addMonitor(Monitor m) {
		if(state != STATE_CONFIGURE) {
			throw new IllegalStateException("Not in configuration state.");
		}
		this.monitors.add(m);
	}

	@Override
	public void run() {
		if(benchLog == null) { log.warn("Logfile not set yet."); }
		if(bench == null) { log.warn("Benchmark not set yet."); }
		state = STATE_RUNNING;
		// fix monitors
		Monitor[] localMonitors = monitors.toArray(new Monitor[monitors.size()]);
		List<BenchmarkData> inputValues = bench.getData();
		try(BufferedWriter logWriter = new BufferedWriter(new FileWriter(benchLog))) {
			for(BenchmarkData data : inputValues) {
				log.debug("Start with benchmark data: " + data);
				for(Monitor m : localMonitors) {
					m.collectBefore();
				}
				Object output = bench.iteration(data);
				for(Monitor m : localMonitors) {
					m.collectAfter(data, output);
				}
				for(Monitor m : localMonitors) {
					m.compute();
					logWriter.write(m.propertyAndValue()+";");
					log.debug(m.propertyAndValue());
				}
				// XXX Multi-Metaparam not supported
				logWriter.write(data.getMetaparamValue()+";");
				logWriter.write("\n");
				logWriter.flush();
			}
		} catch (IOException e) {
			log.error("Could not complete benchmark", e);
		} finally {
			state = STATE_FINISHED;
		}
		log.debug("Benchmark finished with " + inputValues.size() + " data items written to " + benchLog.getAbsolutePath());
	}
	
//	public void clearMonitors() {
//		this.monitors = new ArrayList<Monitor>();
//	}
}
