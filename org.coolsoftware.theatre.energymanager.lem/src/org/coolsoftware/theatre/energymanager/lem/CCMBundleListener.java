/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.lem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.FrameworkUtil;

/**
 * BundleListener (un-)deploying (un-)installed (respectively) component bundles.
 * @author Sebastian Götz
 */
public class CCMBundleListener implements BundleListener {
	
	public static final String CCM_TYPE = "CCM-SWComponentType";
	public static final String CCM_VARIANT = "CCM-Variant";
	public static final String CCM_VARIANT_TYPE = "CCM-VariantType";
	public static final String CCM_BENCHMARK = "CCM-Benchmark";
	
	private Map<String, List<String>> unresolvedVariants;
	
	private final OsgiContainerEnergyMgr lem;

	public CCMBundleListener(ILocalEnergyManager lem) {
		this.lem = (OsgiContainerEnergyMgr) lem;
		unresolvedVariants = new HashMap<String, List<String>>();
		BundleContext context = FrameworkUtil.getBundle(CCMBundleListener.class).getBundleContext();
		Bundle [] bundles = context.getBundles();
		for (Bundle bundle : bundles) {
			BundleEvent event = new BundleEvent(BundleEvent.INSTALLED, bundle);
			bundleChanged(event);
		}
	}

	@Override
	public void bundleChanged(BundleEvent event) {
		if((event.getBundle().getSymbolicName()!=null) && (event.getBundle().getSymbolicName().contains("org.coolsoftware"))){
			int i = event.getType();
			switch (i) {
			case BundleEvent.INSTALLED:
				handleInstall(event.getBundle());
//				System.out.println("Bundle installed: " + event.getBundle().getSymbolicName() );
				break;
			case BundleEvent.STARTED:
				handleStart(event.getBundle());
//				System.out.println("Bundle Started: " + event.getBundle().getSymbolicName() );
				break;
			case BundleEvent.STOPPED:
				handleStop(event.getBundle());
//				System.out.println("Bundle Stopped: " + event.getBundle().getSymbolicName() );
				break;
			case BundleEvent.UNINSTALLED:
				handleUninstall(event.getBundle());
//				System.out.println("Bundle unistalled: " + event.getBundle().getSymbolicName() );
				break;
			default:
				break;
			}
		}
		// handle unresolved variants
		Map<String, List<String>> tmp = new HashMap<String, List<String>>();
		tmp.putAll(unresolvedVariants);
		for (String ccmType : tmp.keySet()) {
			if(lem.isComponentTypeDeployed(ccmType)){
				for (String var : tmp.get(ccmType)) {
					lem.registerDeployedComponent(ccmType, var);
				}
				unresolvedVariants.remove(ccmType);
//				System.out.println("dependency resolved for SWComponentType " + ccmType);
			}
		}
		
	}

	private void handleUninstall(Bundle bundle) {
		if(bundle.getHeaders().get(CCM_TYPE)!=null)
			lem.unregisterUndeployedComponent(bundle.getHeaders().get(CCM_TYPE), null);
		else if((bundle.getHeaders().get(CCM_VARIANT_TYPE)!=null) && (bundle.getHeaders().get(CCM_VARIANT)!=null)){
			String type = bundle.getHeaders().get(CCM_VARIANT_TYPE);
			String variant = bundle.getHeaders().get(CCM_VARIANT);
			lem.unregisterUndeployedComponent(type, variant);
		}			
	}

	private void handleStop(Bundle bundle) {
		// TODO to be implemented
//		handleUninstall(bundle);
		
	}

	private void handleStart(Bundle bundle) {
		// TODO to be implemented
//		handleInstall(bundle);
		
	}

	private void handleInstall(Bundle bundle){
		// is it a type bundle
		if(bundle.getHeaders().get(CCM_TYPE) != null){
			String ccmType = bundle.getHeaders().get(CCM_TYPE);
			if(!lem.isComponentTypeDeployed(ccmType)){
				lem.registerDeployedComponent(ccmType, null);
			} 
//			else {
//				throw new LemException("Error during LEM initialization: Container is inconsistent due to a duplicated SWComponentType identified." +
//						"CCMType " + ccmType + " (" + bundle.getSymbolicName() + ")"+ " is already deployed in this container.");
//			}
		} else if ((bundle.getHeaders().get(CCM_VARIANT) != null) && (bundle.getHeaders().get(CCM_VARIANT_TYPE) != null)){
			String ccmVariant = bundle.getHeaders().get(CCM_VARIANT);
			String ccmVariantType = bundle.getHeaders().get(CCM_VARIANT_TYPE);
			if(lem.isComponentTypeDeployed(ccmVariantType))
				lem.registerDeployedComponent(ccmVariantType, ccmVariant);
			else {
				if(!unresolvedVariants.containsKey(ccmVariantType))
					unresolvedVariants.put(ccmVariantType, new ArrayList<String>());
				unresolvedVariants.get(ccmVariantType).add(ccmVariant);
			}
			
		}
		
		
	}

}
