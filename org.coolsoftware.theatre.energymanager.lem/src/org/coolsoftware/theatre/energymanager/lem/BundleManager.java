/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.lem;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.LemException;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

/**
 * Manager taking care of correct bundle status of (un-)deployed component bundles by
 * installing or uninstalling them.
 * @author Sebastian Götz
 */
public class BundleManager {
	private static final Logger log = Logger.getLogger(BundleManager.class);
	private BundleContext context;
	
	public BundleManager(BundleContext context) {
		this.context = context;
	}
	
	/**
	 * @param context the context to set
	 */
	public void setContext(BundleContext context) {
		this.context = context;
	}
	
	/**
	 * @return the context
	 */
	public BundleContext getContext() {
		return context;
	}
	
	public void install(String bundleLocation){
		try {
			Bundle newBundle = context.installBundle(bundleLocation);
			newBundle.start();
		} catch (BundleException e) {
			log.error("[BundleManager]: Can not install the Bundle from: " + bundleLocation, e);	
		} catch (SecurityException e){
			log.error(e.getMessage(), e);
		} catch (IllegalStateException e){
			log.error(e.getMessage(), e);
		}
	}
	
	public void uninstall(long bundleIdentifier){
		try {
			Bundle bundleToUninstall = context.getBundle(bundleIdentifier);
			if(bundleToUninstall==null)
				throw new LemException("Not able to uninstall the Bundle with id " +  bundleIdentifier + " because the bundle was not found");
			bundleToUninstall.uninstall();
		} catch (BundleException e) {
			log.error("[BundleManager]: Can not uninstall the Bundle: " + bundleIdentifier, e);	
		} catch (SecurityException e){
			log.error(e.getMessage(), e);
		} catch (IllegalStateException e){
			log.error(e.getMessage(), e);
		} catch (LemException e) {
			log.error(e.getMessage(), e);
		}
	}
	
	/**
	 * Iterates all registered {@link Bundle}s and uninstall CCM App related bundles to achieve a consistent initial container state.
	 * A container is initially consistent in cases where no CCM {@link Bundle} is available before the {@link IGlobalEnergyManager#registerLEM(URI)}
	 * as well as after the {@link IGlobalEnergyManager#unregisterLem(URI)}.
	 * The method is called 
	 * @return <code>true</code> if all the container is consistent and <code>false</code> otherwise
	 */
	public boolean cleanup(){
		List<Bundle> uninstalled_bundles = new ArrayList<Bundle>();
		for (Bundle bundle : context.getBundles()) {
			if((bundle.getHeaders().get(CCMBundleListener.CCM_TYPE) != null) ||	
					(bundle.getHeaders().get(CCMBundleListener.CCM_VARIANT) != null)){
				try {
					bundle.uninstall();
					if(bundle.getState() != Bundle.UNINSTALLED)
						uninstalled_bundles.add(bundle);
						
				} catch (BundleException e) {
					uninstalled_bundles.add(bundle);
					System.err.println("unable to uninstall Bundle  " + bundle.getBundleId() + ": " +e.getMessage());		
				}
			}
		}
		if (uninstalled_bundles.isEmpty())
			return true;
		return false;
	}

}
