/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.transcoder.noop;

import java.io.File;

import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.utils.FileProxy;
import org.haec.videoprovider.IVideoProviderFactory;

/**
 * Implementation of org.haec.app.transcoder.noop.TranscoderNoop
 * @author René Schöne
 */
public class TranscoderNoop extends TaskBasedImpl implements TranscoderTranscode {

	private IVideoProviderFactory fac;

	/* (non-Javadoc)
	 * @see org.haec.app.videotranscodingserver.TranscoderTranscode#transcode(java.lang.Object, java.lang.String, java.lang.String)
	 */
	@Override
	public FileProxy transcode(Object in, String outputVideoCodec,
			String outputAudioCodec) {
		// in is of type FileProxy
		FileProxy fp1 = (FileProxy) in;
		File f = fp1.getFileFor(thisUri);
		return wrap(f);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return TranscodingServerUtil.APP_NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "Transcoder";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.transcoder.noop.TranscoderNoop";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new TranscoderNoopBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.transcoder.noop";
	}
	
	protected void setVideoProviderFactory(IVideoProviderFactory fac) {
		this.fac = fac;
	}

}
