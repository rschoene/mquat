/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.scaling.noop;

import java.util.ArrayList;
import java.util.List;

import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;
import org.haec.videoprovider.IVideoProviderFactory;
import org.haec.videoprovider.VideoProvider;
import org.haec.videoprovider.VideoProviderFactory;
import org.haec.videos.Video;

class NoBenchmarkData implements BenchmarkData {

	int mpv;
	
	public NoBenchmarkData(int mpv) {
		super();
		this.mpv = mpv;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mpv;
	}
	
}
	

/**
 * Benchmark for org.haec.app.scaling.noop.ScalingNoop
 * @author René Schöne
 */
public class ScalingNoopBenchmark implements Benchmark {
	
	private IVideoProviderFactory fac;
	
	public ScalingNoopBenchmark(IVideoProviderFactory fac) {
		this.fac = fac;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>(); 
		VideoProvider prov = fac.getCurrentVideoProvider();
		for(Video video : prov.getOneVideoOfEachLength()) {
			int mpValue = TranscodingServerUtil.getMetaparameterValue(video.length, video.resX, video.resY);
			result.add(new NoBenchmarkData(mpValue));
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		// do nothing
		return null;
	}

}
