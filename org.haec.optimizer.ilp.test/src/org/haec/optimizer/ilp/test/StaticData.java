/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.ilp.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.ecl.EclFile;
import org.haec.optimizer.testcase.TestCases;

/**
 * Utility class storing immutable data in the {@link GLPK_ILP_Test}.
 * @author René Schöne
 */
public class StaticData {

	public static final String appName = "VideoTranscodingServer";
	public static final String ecl_prefix = "models/contracts/org.haec.app.videoTranscodingServer_";
	public static final String scaling = "Scaling";
	public static final String controller = "Controller";
	public static final String copyScaling1 = "CopyScalingFile1";
	public static final String response_time = "response_time";
	public static final Pattern containerPattern = Pattern.compile("r-osgi://([^:]*?):(.*?)");
	public static final String[] ecl_files = new String[] {"Censor.ecl", "Controller.ecl", "PiP.ecl", "Scaling.ecl", "Transcoder.ecl", "cpScaling1.ecl"};
	public static final String[] ecl_contractIds = new String[] {
		"org_haec_app_censor_ffmpeg_CensorFfmpeg",
		"org_haec_app_controller_delegating_DelegatingController",
		"org_haec_app_pip_ffmpeg_PipFfmpeg",
		"org_haec_app_scaling_ffmpeg_ScalingFfmpeg",
		//org_haec_app_scaling_mencoder_ScalingMencoder | org_haec_app_scaling_handbrake_ScalingHandbrake
		"org_haec_app_transcoder_ffmpeg_TranscoderFfmpeg",
		"org_haec_app_cpScaling1"};
	public static final String[] ecl_contract_compNames = new String[] {"Censor", controller, "PiP", scaling, "Transcoder", copyScaling1};
	public static Map<String, String> ecl_strings;
	public static Map<String, EclFile> ecls;
	public static final String sw_sm_file = "models/Software.structure";
	public static String sw_sm_string;
	public static StructuralModel sw_sm;
	public static final String sw_vm_file = "models/currentSoftware.variant";
	public static String sw_vm_string;
	public static VariantModel sw_vm;
	//	private static SWComponent scalingImpl;
	public static final String hw_sm_file = "models/Hardware.structure";
	public static String hw_sm_string;
	public static StructuralModel hw_sm;
	public static final String hw_vm_file_default = "models/currentHardware.variant";
	public static VariantModel hw_vm;
	public static final String testcases_file = "models/t1.testcase";
	public static final Map<String, List<String>> resourceMap =
			new HashMap<String, List<String>>(){
		private static final long serialVersionUID = 1L;
			{put("*", Arrays.asList("123.avi"));}};
	public static String testcases_string;
	public static TestCases testcases;
	public static boolean initialized = false;
	public static String logLayout = "%d{HH:mm:ss.SSS/zzz}%p%M(%F:%L): %m%n";
}