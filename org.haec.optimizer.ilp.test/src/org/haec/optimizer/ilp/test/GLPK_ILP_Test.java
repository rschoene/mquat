/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.ilp.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LevelChanger;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.Job;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;
import org.coolsoftware.coolcomponents.ccm.variant.Schedule;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.ecl.EclFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.requests.Import;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.RequestsFactory;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.Optimizer;
import org.coolsoftware.theatre.energymanager.Optimizer.Mapping;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.haec.optimizer.ilp.GLPK_ILPGenerator;
import org.haec.optimizer.testcase.ContainerSchedule;
import org.haec.optimizer.testcase.CopyFilePattern;
import org.haec.optimizer.testcase.ExpectedOutcome;
import org.haec.optimizer.testcase.ImplDontCare;
import org.haec.optimizer.testcase.ImplMapping;
import org.haec.optimizer.testcase.ImplToContainer;
import org.haec.optimizer.testcase.JobDescription;
import org.haec.optimizer.testcase.Situation;
import org.haec.optimizer.testcase.TestCase;
import org.haec.optimizer.testcase.TestCases;
import org.haec.optimizer.testcase.impl.TestcasePackageImpl;
import org.haec.optimizer.testcase.resource.testcase.mopp.TestcaseResource;
import org.haec.test.utils.GEMAdapter;
import org.haec.test.utils.GRMAdapter;
import org.haec.test.utils.TestUtils;
import org.haec.test.utils.TheatreSettingsAdapter;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * A test class to test {@link GLPK_ILPGenerator}.
 * @author René Schöne
 */
@SuppressWarnings("serial")
@RunWith(Parameterized.class)
public class GLPK_ILP_Test implements Runnable {
	
	@Rule
	public ErrorCollector collector = new ErrorCollector();
	
	private TestCase tc;
	private Request request;
	private final Map<String,String> implToCompName;
	private final List<String> servernames;
	protected Map<String, String> hw_vm_strings;
	private static Logger log = Logger.getLogger(GLPK_ILP_Test.class);

	private static TheatreSettings s;
	
	public GLPK_ILP_Test(TestCase tc) {
		this.tc = tc;
		implToCompName = new HashMap<>();
		servernames = new ArrayList<>();
		hw_vm_strings = new HashMap<>();
	}

	@BeforeClass
	public synchronized static void init() {
		if(StaticData.initialized) {
			return;
		}
//		ConsoleAppender appender = new ConsoleAppender(
//				new PatternLayout(StaticData.logLayout));
//		Logger rootLogger = Logger.getRootLogger();
		LevelChanger.changeLevels();
//		rootLogger.addAppender(appender);

		pluginsSetup();
		try {
			StaticData.sw_sm_string = FileUtils.readAsString(StaticData.sw_sm_file);
			StaticData.sw_vm_string = FileUtils.readAsString(StaticData.sw_vm_file);
			StaticData.hw_sm_string = FileUtils.readAsString(StaticData.hw_sm_file);
//			ecl_string = FileUtils.readAsString(ecl_file);
			StaticData.testcases_string = FileUtils.readAsString(StaticData.testcases_file, true);

			StaticData.sw_sm = CCMUtil.deserializeHWStructModel(StaticData.sw_sm_string, false);
			StaticData.hw_sm = CCMUtil.deserializeHWStructModel(StaticData.hw_sm_string, false);
			StaticData.ecl_strings = new HashMap<>(StaticData.ecl_files.length);
			StaticData.ecls = new HashMap<>(StaticData.ecl_files.length);
			for (int i = 0; i < StaticData.ecl_files.length; i++) {
				String ecl_file = StaticData.ecl_prefix + StaticData.ecl_files[i];
				File f = new File(ecl_file);
				if(!f.exists()) {
					log.fatal(String.format("%s does not exist. Exiting.", f));
					exit("File not existing");
				}
				String ecl_string = FileUtils.readAsString(f.getAbsolutePath());
				StaticData.ecl_strings.put(StaticData.ecl_contract_compNames[i], ecl_string);
				EclFile ecl = CCMUtil.deserializeECLContract(StaticData.sw_sm, StaticData.hw_sm, StaticData.appName, StaticData.ecl_contractIds[i], ecl_string, false);
				StaticData.ecls.put(StaticData.ecl_contract_compNames[i], ecl);
			}
			StaticData.testcases = deserializeTestcase(StaticData.testcases_string);
		} catch (Exception e) {
			log.warn("Problems parsing models.", e);
			exit("Parsing problems");
		}
		checkModels();
		StaticData.initialized = true;
	}
	
	/**
	 * Set ecore links to <code>theatre</code> and <code>models</code>.
	 * Reload properties of Activator, if any.
	 */
	protected static void pluginsSetup() {
//		TestUtils.createEcoreLink("theatre", "theatre/", null);
		TestUtils.createEcoreLink("models", "models/", null);
		s = new TheatreSettingsAdapter()
				.getILPfeatureFuzzy().set(true)
				.getILPfeatureOnlyUpdate().set(false);
	}

	private static TestCases deserializeTestcase(String serializedTestcase) throws IOException {
		//create an inputstream of the serialized model
		ByteArrayInputStream bTestcase = new ByteArrayInputStream(serializedTestcase.getBytes());
		//load it using emf + emftext factory
		EObject result = null;
		ResourceSet rs = CCMUtil.createResourceSet();
		rs.getPackageRegistry().put("testcase", TestcasePackageImpl.eINSTANCE);
		
		URI targetURI = URI.createPlatformResourceURI(CCMUtil.APP_STRUCTURE_PREFIX_PATH + "dummy.testcase", false);
		org.eclipse.emf.ecore.resource.Resource resource = rs.getResource(targetURI , false);
		/* Check if the resource has already been created. */
		if (resource == null) {
//			resource = rs.createResource(targetURI);
			resource = new TestcaseResource(targetURI);
		}
		// no else.
	
		if (!resource.isLoaded())
			resource.load(bTestcase, null);
		// no else.
	
		if (resource.getContents().size() > 0) {
			result = resource.getContents().get(0);
		}
		
//		System.out.println(result);
		return (TestCases)result;
	}

	private static void checkModels() {
		class FileAndModel {
			String fname;
			Object model;
			public FileAndModel(String fname, Object model) {
				this.fname = fname;
				this.model = model;
			}
		}
		List<FileAndModel> models = new ArrayList<FileAndModel>() {{
			add(StaticData.hw_sm_file, StaticData.hw_sm);
			add(StaticData.sw_sm_file, StaticData.sw_sm);
			for (int i = 0; i < StaticData.ecl_files.length; i++) {
				add(StaticData.ecl_files[i], StaticData.ecls.get(StaticData.ecl_contract_compNames[i]));
			}
			add(StaticData.testcases_file, StaticData.testcases);
		}
		boolean add(String fname, Object model) {
			return add(new FileAndModel(fname, model));
		}};
		for(FileAndModel fam : models) {
			assertNotNull(String.format("Model loaded from '%s' is null", fam.fname), fam.model);
		}
		ResourceSet rs = CCMUtil.createResourceSet();
		rs.getPackageRegistry().put("testcase", TestcasePackageImpl.eINSTANCE);
		org.eclipse.emf.ecore.resource.Resource r = 
				rs.createResource(URI.createPlatformResourceURI("models/read.testcase", false));
		r.getContents().add(StaticData.testcases);
		try {
			r.save(null);
		} catch (IOException e) {
			log.error("Could not save testcases", e);
		}
	}

	@Before
	public void addFileAppender() {
		Logger.getRootLogger().removeAllAppenders();
		FileAppender appender = new FileAppender();
		appender.setFile(String.format("logs/%s.log", tc.getName()));
		appender.setAppend(false);
		appender.setThreshold(Level.DEBUG);
		appender.setName(tc.getName());
		appender.setLayout(new PatternLayout(StaticData.logLayout));
		appender.activateOptions();
	
		Logger logger = Logger.getRootLogger();
		logger.addAppender(appender);
	}

	@Before
	public void resetVariantModels() {
		ResourceSet rs = CCMUtil.createResourceSet();
		String hw_vm_file = tc.getRequest().getHardwareVariantmodelFile();
		if(hw_vm_file == null) {
			// default value in testcase is null. Use default path, if this default is found.
			hw_vm_file = StaticData.hw_vm_file_default;
		}
		try {
			StaticData.sw_vm = TestUtils.loadVariantModel(rs, StaticData.sw_vm_string, StaticData.sw_vm_file);
			assertNotNull(String.format("Model loaded from '%s' is null", StaticData.sw_vm_file), StaticData.sw_vm);

			String hw_vm_string = hw_vm_strings.get(hw_vm_file);
			if(hw_vm_string == null) {
				hw_vm_string = FileUtils.readAsString(hw_vm_file);
				hw_vm_strings.put(hw_vm_file, hw_vm_string);
			}
			StaticData.hw_vm = TestUtils.loadVariantModel(StaticData.hw_sm.eResource().getResourceSet(), hw_vm_string, hw_vm_file);
			assertNotNull(String.format("Model loaded from '%s' is null", hw_vm_file), StaticData.hw_vm);
			EcoreUtil.resolveAll(StaticData.hw_vm.getRoot());
			updateImplToCompName();
			updateServernames();
		} catch (IOException e) {
			log.warn("Problem on serializing variant models", e);
		}
	}
	
	private void updateImplToCompName() {
		implToCompName.clear();
		List<SWComponent> toSearch = new ArrayList<>();
		toSearch.add((SWComponent) StaticData.sw_vm.getRoot());
		while(!toSearch.isEmpty()) {
			SWComponent impl = toSearch.remove(0);
			EcoreUtil.resolveAll(impl);
			String implName = impl.getName();
			String compName = impl.getSpecification().getName();
			implToCompName.put(implName, compName);
			toSearch.addAll(impl.getSubtypes());
		}
	}
	
	private void updateServernames() {
		servernames.clear();
		for(Resource r : ((Resource) StaticData.hw_vm.getRoot()).getSubresources()) {
			servernames.add(r.getName());
		}
	}

	@Parameters(name = "{0}")
	public static Collection<Object[]> testcases() {
		init();
		return new ArrayList<Object[]>() {{
			String pattern = StaticData.testcases.getSettings().getPattern();
			for(TestCase testcase : StaticData.testcases.getTestcases()) {
				if(pattern != null && !testcase.getName().contains(pattern)) {
					System.out.println("Skipping testcase " + testcase);
					continue;
				}
				add(new Object[]{testcase});
				if(StaticData.testcases.getSettings().isOnlyFirst()) { break; }
			}
		}};
	}

	@Override
	@Test
	public void run() {
		init();
		Object[] args = createArgs();
		applyOn(tc.getSituation(), request);
		Mapping mapping = getOptimizer().optimize(args);
		log.info(mapping);
		check(tc.getExpectedOutcome(), mapping);
	}
	
	protected Optimizer getOptimizer() {
		GLPK_ILPGenerator result = new GLPK_ILPGenerator(){
			{
				setTheatreSettings(s);
			}
			protected long currentTimeMillis() {
				return 0;
			};
		};
		return result;
	}

	/** @return Object[]{Request, Map&lt;String, EclFile&gt;, Boolean, gem}
	 * @see GLPK_ILPGenerator#optimize(Object[]) */
	protected Object[] createArgs() {
		request = createRequest();
		Map<String, EclFile> eclFiles = createEclFiles();
		IGlobalEnergyManager gem = getGem();
		Map<String, Object> options = new HashMap<>();
		options.put(Optimizer.OPTION_VERBOSE, Boolean.TRUE);
		options.put(Optimizer.OPTION_RESOURCENAMES, StaticData.resourceMap);
		options.put(Optimizer.OPTION_APPNAME, StaticData.appName);
		options.put(Optimizer.OPTION_PROBLEMNAME, tc.getName());
		options.put(Optimizer.OPTION_PROFILE, profileToString(tc.getRequest().getProfile()));
		return new Object[]{ request, eclFiles, gem, options };
	}

	/**
	 * @return a string as declared in {@link Optimizer}.
	 */
	private String profileToString(String profile) {
		final String ENERGY_EFFICIENT = "energy",
					 NORMAL = "normal",
					 UTILITY = "utility";
		switch(profile) {
		case ENERGY_EFFICIENT: return Optimizer.PROFILE_ENERGY_SAVING;
		case NORMAL: return Optimizer.PROFILE_NORMAL;
		case UTILITY: return Optimizer.PROFILE_UTILITY;
		}
		throw new IllegalArgumentException("Unknown profile " + profile);
	}

	/** Creates the request.
	 * Calls Controller (first port), response_time <= 80
	 * max_video_length (first metaparameter) = 10 */
	protected Request createRequest() {
		RequestsFactory fac = RequestsFactory.eINSTANCE;
		EclFactory eclFac = EclFactory.eINSTANCE;
		Request r = fac.createRequest();
		Import im = fac.createImport();
		im.setModel(StaticData.sw_sm);
		r.setImport(im);
		Platform p = fac.createPlatform();
		p.setHwmodel(StaticData.hw_vm);
		r.setHardware(p);
		
		org.haec.optimizer.testcase.Request tcRequest = tc.getRequest();
		log.info("Using description of request: " + tcRequest);
		String compName = d(tcRequest.getComponentName(), StaticData.controller);
		log.debug("Using component " + compName);
		SWComponentContract contract = (SWComponentContract) StaticData.ecls.get(compName).getContracts().get(0);
		SWComponentType compType = contract.getComponentType();
		PortType portType = contract.getPort();
		if(tcRequest.getPortName() != null) {
			log.warn("PortName of testcase.request is ignored, using port of first mode of contract of "
					+ compName +", that is " + portType.getName());
		}
		r.setComponent(compType);
		r.setTarget(portType);
		
		PropertyRequirementClause prc = eclFac.createPropertyRequirementClause();
		String propName = d(tcRequest.getReqPropertyName(), StaticData.response_time);
		Property property = findPropertyInSW_SM(propName, compName);
		if(property == null) {
			log.error(String.format("Property %s not found. Exiting.", propName));
			exit("required property not found");
		}
		prc.setRequiredProperty(property);
		RealLiteralExpression stmt = LiteralsFactory.eINSTANCE
				.createRealLiteralExpression();
		stmt.setValue(d(tcRequest.getReqPropertyValue(), Double.valueOf(80)));
		prc.setMaxValue(stmt);
		r.getReqs().add(prc);
		
		MetaParamValue mpv = fac.createMetaParamValue();
		//first metaparameter = max_video_length
		String metaParamName = d(tcRequest.getMetaparamName(), "max_video_length");
		Parameter param = findMetaparamInPort(metaParamName, portType);
		if(param == null) {
			log.warn(String.format("Metaparam %s not found in portType %s", metaParamName, portType));
			exit("metaparam not found");
		}
		mpv.setMetaparam(param);
		mpv.setValue(d(tcRequest.getMetaparamValue(), Integer.valueOf(10)));
		r.getMetaParamValues().add(mpv);
		log.debug("Request: " + r);
		return r;
	}

	/** Helper method for default values */
	private <S> S d(S value, S defaultValue) {
		if(value == null) {
			return defaultValue;
		}
		return value;
	}

	private Property findPropertyInSW_SM(String propertyName, String compName) {
		for(SWComponentType sub : ((SWComponentType) StaticData.sw_sm.getRoot()).getSubtypes()) {
			if(sub.getName().equals(compName)) {
				for(Property prop : sub.getProperties()) {
					if(prop.getDeclaredVariable().getName().equals(propertyName)) {
						return prop;
					}
				}
			}
		}
		return null;
	}

	private Parameter findMetaparamInPort(String metaParamName,
			PortType portType) {
		for(Parameter p : portType.getMetaparameter()) {
			if(p.getName().equals(metaParamName)) {
				return p;
			}
		}
		return null;
	}

	/** contract-controller for each server */
	protected Map<String, EclFile> createEclFiles() {
		Map<String, EclFile> m = new HashMap<>(servernames.size());
		EclFile f = StaticData.ecls.get(d(tc.getRequest().getComponentName(), StaticData.controller));
		for(String sName : servernames) {
			m.put(sName, f);
		}
		return m;
	}

	/** GEMAdapter with relevant methods implemented */
	private IGlobalEnergyManager getGem() {
		return new GEMAdapter(getGrm()) {
			@Override
			public Map<String, String> getContractsForComponent(String appName,
					final String compName) {
				// name == name of sw-component-type
				// return { serverName -> ecl_string }
				assert appName == StaticData.appName;
				Map<String, String> m = new HashMap<>(servernames.size());
				for(String sName : servernames) {
					String reqCompName = tc.getRequest().getComponentName();
					if(reqCompName != null && reqCompName.equals(StaticData.scaling)) {
						// only include scaling and copyScaling1
						if(!compName.equals(StaticData.scaling) && !compName.equals(StaticData.copyScaling1)) {
							log.info("Skipping component " + sName + " for " + compName);
							continue;
						}
					}
					m.put(sName, StaticData.ecl_strings.get(compName));
				}
				return m;
			}
			@Override
			public Map<String, Map<String, String>> getContractsForApp(
					String appName, Map<String, List<String>> nameMap) {
				Map<String, Map<String, String>> m = new HashMap<>(servernames.size() * StaticData.ecl_strings.size());
				for(Entry<String, String> e : StaticData.ecl_strings.entrySet()) {
					String compName = e.getKey();
					String serializedContract = e.getValue();
					String pattern = null;
					if(compName == StaticData.copyScaling1) {
						List<CopyFilePattern> patterns = tc.getSituation().getCopyFilePatterns();
						if(patterns.size() == 0) {
							// simulate, that every server hosts the given file.
							pattern = null;
						}
						else {
							if(patterns.size() > 1) {
								log.warn("Ignoring all but first pattern in situation of request" + tc.getName() + ".");
							}
							CopyFilePattern cfp = patterns.get(0);
							if(cfp.getContainsString() != null) {
								pattern = "*"+cfp.getContainsString()+"*";
							}
							if(cfp.getExactString() != null) {
								pattern = cfp.getExactString();
							}
						}
					}
					for(String sName : servernames) {
						String reqCompName = tc.getRequest().getComponentName();
						if(reqCompName != null && reqCompName.equals(StaticData.scaling)) {
							// only include scaling and copyScaling1
							if(!compName.equals(StaticData.scaling) && !compName.equals(StaticData.copyScaling1)) {
								log.info("Skipping component " + sName + " for " + compName);
								continue;
							}
						}
						if(pattern == null || cleanContainerName(sName).matches(pattern)) {
							CollectionsUtil.ensureHashMap(m, compName).put(sName, serializedContract);
						}
						else { // pattern did not match. dirty way of setting provided response time of resource.
							String changedContract = serializedContract.replaceFirst("1\\.0", "99.9");
//							CollectionsUtil.ensureHashMap(m, compName).put(sName, changedContract);
						}
					}
				}
				return m;
			}
			
			@Override
			public String getApplicationModel(String appName) {
				return StaticData.sw_sm_string;
			}
	
			@Override
			public String getCurrentSystemConfiguration(String appUri) {
				assertEquals(appUri, StaticData.appName);
				return StaticData.sw_vm_string;
			}
		};
	}

	/** GRMAdapter with relevant method implemented */
	IGlobalResourceManager getGrm() {
		return new GRMAdapter() {
			@Override
			public String getInfrastructureTypes() {
				return StaticData.hw_sm_string;
			}
			
		};
	}

	protected void applyOn(Situation situation, Request request) {
		Resource root = (Resource) request.getHardware().getHwmodel().getRoot();
		Map<String, ContainerProvider> mapOfServers = new HashMap<>();
		for(Resource r : root.getSubresources()) {
			if(!(r instanceof ContainerProvider)) continue;
			mapOfServers.put(uncleanContainerName(r.getName()), (ContainerProvider) r);
		}
		// loop over containerNames in variable "jobs"
		// add the found jobs to schedule of container
		for(ContainerSchedule s : situation.getSchedules()) {
			String containerName = uncleanContainerName(s.getContainerName());
			List<JobDescription> jobList = s.getJobs();
			ContainerProvider server = mapOfServers.get(containerName);
			if(server == null) {
				log.error("Could not find server with name " + containerName);
				continue;
			}
			Schedule schedule = server.getSchedule();
			if(schedule == null) {
				schedule = VariantFactory.eINSTANCE.createSchedule();
				server.setSchedule(schedule);
			}
			schedule.getJobs().clear();
			for(JobDescription jobDesc : jobList) {
				Job job = VariantFactory.eINSTANCE.createJob();
				job.setImpl(findComponent(StaticData.sw_vm, jobDesc.getImplName()));
				job.setPredictedRuntime(jobDesc.getPredictedRuntime()/1000.0);
				job.setStartTime(jobDesc.getStartTime());
				schedule.getJobs().add(job);
			}
		}
	}
	
	private String uncleanContainerName(String containerName) {
		if(containerName.startsWith("r-osgi")) {
			// already unclean
			return containerName;
		}
		return String.format("r-osgi://%s:%d", containerName, 9278);
	}

	/** Breadth-first search in components of model for component with given name. */
	private static SWComponent findComponent(VariantModel vm, String compName) {
		if(compName == null) {
			log.warn("CompName is null");
			return null;
		}
		List<SWComponent> toSearch = new ArrayList<>();
		toSearch.add((SWComponent) vm.getRoot());
		while(!toSearch.isEmpty()) {
			SWComponent current = toSearch.remove(0);
			if(compName.equals(current.getName())) {
				return current;
			}
			toSearch.addAll(current.getSubtypes());
		}
		return null;
	}
	
	protected void check(ExpectedOutcome expectedOutcome, Mapping actualMapping) {
		assertNotNull("No mapping returned", actualMapping);
		final List<ImplMapping> expectedMappings = expectedOutcome.getMappings();
		Set<String> implsToMatch = new HashSet<String>(expectedMappings.size()) {{
			for(ImplMapping itc : expectedMappings) {
				add(itc.getImplName());
			}
		}};
		for(String implName : actualMapping.keySet()) {
			String containerName = cleanContainerName(actualMapping.get(implName));
			ImplMapping expectedMapping = findExpectedMapping(expectedMappings, implName);
			collector.checkThat("Unexpected impl: " + implName,
					expectedMapping, is(notNullValue()));
			if(expectedMapping == null) {
//				result.addError("Unexpected impl", implName);
				continue;
			}
			implsToMatch.remove(implName);
			if(expectedMapping instanceof ImplDontCare) {
				// don't care
				log.debug(String.format("Don't care for %s", implName));
			}
			else {
				String expectedContainer = ((ImplToContainer) expectedMapping).getContainerName();
				if(expectedContainer.equals(containerName)) {
					// everything fine so far, check startTime if present
					Long expectedStartTime = ((ImplToContainer) expectedMapping).getStartAtMillis();
					if(expectedStartTime == null) {
						// no expectations, move on
					}
					else {
						String compName = implToCompName.get(implName);
						Long actualStartTime = actualMapping.getStartTime(compName);
						if(actualStartTime == null) {
//							result.addError("No startTime found", String.format("for %s expecting %s",
//									compName, expectedStartTime));
							collector.checkThat("No startTime found for "+implName,
									actualStartTime, is(notNullValue()));
						}
						if(actualStartTime != expectedStartTime) {
//							result.addError("StartTime mismatch", String.format("for %s: Expected %d, but was %d",
//									compName, expectedStartTime, actualStartTime));
							collector.checkThat("StartTime mismatch for "+implName,
									actualStartTime, equalTo(expectedStartTime));
						}
					}
				}
				else {
//					result.addError("Container mismatch", String.format("for %s: Expected '%s', but was '%s'",
//							implName, expectedContainer, containerName));
					collector.checkThat("Container mismatch for "+implName,
							containerName, equalTo(expectedContainer));
				}
			}
		}
		collector.checkThat("Expected impls without match",
				implsToMatch, equalTo(Collections.<String>emptySet()));
	}

	private String cleanContainerName(String actualContainerName) {
		// extract r-osgi://<containerName>:<port> from actualContainerName
		Matcher m = StaticData.containerPattern.matcher(actualContainerName);
		if(m.matches()) {
			actualContainerName = m.group(1);
		}
		return actualContainerName;
	}

	private ImplMapping findExpectedMapping(List<ImplMapping> expectedMappings, String implName) {
		for(ImplMapping itc : expectedMappings) {
			if(itc.getImplName().equals(implName)) {
				return itc;
			}
		}
		return null;
	}
	
	@After
	public void removeFileAppender() {
		Logger.getRootLogger().removeAppender(tc.getName());
	}

	/** Shortcut for exiting and possibly other clean-up code
	 * @throws RuntimeException always */
	private static void exit(String reason) {
//		System.exit(status);
		throw new RuntimeException("Exiting, because " + reason);
	}

}
