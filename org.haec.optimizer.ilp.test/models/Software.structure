<?xml version="1.0" encoding="UTF-8"?>
<type:StructuralModel xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:type="http://cool-software.org/ccm/1.0/typesystem">
  <root xsi:type="type:SWComponentType" name="VideoTranscodingServer" description="top level component" id="videotranscodingserver">
    <subtypes name="Transcoder" description="Change format" id="transcoder" eclUri="contracts/transcoder.ecl">
      <porttypes name="transcode" visibility="public" out="//@root/@subtypes.0/@connectors.0" direction="required" in="//@root/@subtypes.6/@connectors.0">
        <parameter name="fp1" direction="in"/>
        <parameter name="outputVideoCodec" type="String" direction="in"/>
        <parameter name="outputAudioCodec" type="String" direction="in"/>
        <parameter name="video_out" direction="out"/>
        <metaparameter name="max_video_length" type="Integer"/>
      </porttypes>
      <connectors name="transcoderConnector" source="//@root/@subtypes.0/@porttypes.0" target="//@root/@subtypes.4/@porttypes.1"/>
      <properties valOrder="decreasing">
        <declaredVariable name="run_time" type="Real"/>
      </properties>
      <properties valOrder="decreasing">
        <declaredVariable name="response_time" type="Real"/>
      </properties>
      <properties valOrder="increasing">
        <declaredVariable name="utility" type="Real"/>
      </properties>
    </subtypes>
    <subtypes name="PiP" description="Merge two videos" id="pip" eclUri="contracts/pip.ecl">
      <porttypes name="doPip" visibility="public" out="//@root/@subtypes.1/@connectors.0">
        <parameter name="fileVideo1" direction="in"/>
        <parameter name="fileVideo2" direction="in"/>
        <parameter name="pos" type="Integer" direction="in"/>
        <parameter name="video_out" direction="out"/>
        <metaparameter name="max_video_length" type="Integer"/>
      </porttypes>
      <connectors name="pipConnector" source="//@root/@subtypes.1/@porttypes.0" target="//@root/@subtypes.4/@porttypes.2"/>
      <properties valOrder="decreasing">
        <declaredVariable name="run_time" type="Real"/>
      </properties>
       <properties valOrder="decreasing">
        <declaredVariable name="response_time" type="Real"/>
      </properties>
    </subtypes>
    <subtypes name="Scaling" description="Change resolution" id="scaling" eclUri="contracts/scaling.ecl">
      <porttypes name="scale" visibility="public" out="//@root/@subtypes.2/@connectors.0" direction="required" in="//@root/@subtypes.5/@connectors.0">
        <parameter name="fp1" direction="in"/>
        <parameter name="w" type="Integer" direction="in"/>
        <parameter name="h" type="Integer" direction="in"/>
        <parameter name="mult" type="Boolean" direction="in"/>
        <parameter name="fileproxy_out" direction="out"/>
        <metaparameter name="max_video_length" type="Integer"/>
      </porttypes>
      <connectors name="scaleConnector" source="//@root/@subtypes.2/@porttypes.0" target="//@root/@subtypes.4/@porttypes.3"/>
      <properties valOrder="decreasing">
        <declaredVariable name="run_time" type="Real"/>
      </properties>
      <properties valOrder="decreasing">
        <declaredVariable name="response_time" type="Real"/>
      </properties>
      <properties valOrder="increasing">
        <declaredVariable name="utility" type="Real"/>
      </properties>
    </subtypes>
    <subtypes name="Censor" description="Introduce censored areas" id="censor" eclUri="contracts/censor.ecl">
      <porttypes name="censor" out="//@root/@subtypes.3/@connectors.0">
        <parameter name="fileVideo" direction="in"/>
        <parameter name="video_out" direction="out"/>
        <metaparameter name="max_video_length" type="Integer"/>
      </porttypes>
      <connectors name="censorConnector" source="//@root/@subtypes.3/@porttypes.0" target="//@root/@subtypes.4/@porttypes.0"/>
      <properties valOrder="decreasing">
        <declaredVariable name="run_time" type="Real"/>
      </properties>
    </subtypes>
    <subtypes xsi:type="type:SWConnectorType" name="Controller" description="Facade for video transformation" id="controller" eclUri="contracts/controller.ecl">
      <porttypes name="getCensored" direction="required" in="//@root/@subtypes.3/@connectors.0"/>
      <porttypes name="getTranscoded" direction="required" in="//@root/@subtypes.0/@connectors.0"/>
      <porttypes name="getPiP" direction="required" in="//@root/@subtypes.1/@connectors.0">
        <parameter name="fileVideo1" direction="in"/>
        <parameter name="fileVideo2" direction="in"/>
        <parameter name="pos" type="Integer" direction="in"/>
        <parameter name="video_out" direction="out"/>
        <metaparameter name="max_video_length" type="Integer"/>
      </porttypes>
      <porttypes name="getScaled" direction="required" in="//@root/@subtypes.2/@connectors.0">
        <parameter name="fileVideo" direction="in"/>
        <parameter name="w" type="Integer" direction="in"/>
        <parameter name="h" type="Integer" direction="in"/>
        <parameter name="mult" type="Boolean" direction="in"/>
        <parameter name="video_out" direction="out"/>
        <metaparameter name="max_video_length" type="Integer"/>
      </porttypes>
      <porttypes name="pipScaled" visibility="public">
        <parameter name="fileVideo1" direction="in"/>
        <parameter name="fileVideo2" direction="in"/>
        <parameter name="w2" type="Integer" direction="in"/>
        <parameter name="h2" type="Integer" direction="in"/>
        <parameter name="mult" type="Boolean" direction="in"/>
        <parameter name="pos" type="Integer" direction="in"/>
        <parameter name="video_out" direction="out"/>
        <metaparameter name="max_video_length" type="Integer"/>
      </porttypes>
      <properties valOrder="decreasing">
        <declaredVariable name="run_time" type="Real"/>
      </properties>
      <properties valOrder="decreasing">
        <declaredVariable name="response_time" type="Real"/>
      </properties>
    </subtypes>
	<subtypes xsi:type="type:CopyFileType" name="CopyScalingFile1" description="Copy fileVideo1 to Scaling" id="cpScaling1" eclUri="contracts/cpScaling1.ecl" parameter="//@root/@subtypes.2/@porttypes.0/@parameter.0">
      <porttypes name="getFile" visibility="public" out="//@root/@subtypes.5/@connectors.0">
        <parameter name="fp1" direction="in"/>
        <parameter name="fileproxy_out" direction="out"/>
      </porttypes>
      <connectors name="cpScalingConnector1" source="//@root/@subtypes.5/@porttypes.0" target="//@root/@subtypes.2/@porttypes.0"/>
      <properties valOrder="decreasing">
        <declaredVariable name="run_time" type="Real"/>
      </properties>
      <properties valOrder="decreasing">
        <declaredVariable name="response_time" type="Real"/>
      </properties>
    </subtypes>
	<subtypes xsi:type="type:CopyFileType" name="CopyTranscoderFile1" description="Copy fileVideo1 to Transcoder" id="cpTranscoder1" eclUri="contracts/cpTranscoder1.ecl" parameter="//@root/@subtypes.0/@porttypes.0/@parameter.0">
      <porttypes name="getFile" visibility="public" out="//@root/@subtypes.6/@connectors.0">
        <parameter name="fp1" direction="in"/>
        <parameter name="fileproxy_out" direction="out"/>
      </porttypes>
      <connectors name="cpTranscodingConnector1" source="//@root/@subtypes.6/@porttypes.0" target="//@root/@subtypes.0/@porttypes.0"/>
      <properties valOrder="decreasing">
        <declaredVariable name="run_time" type="Real"/>
      </properties>
      <properties valOrder="decreasing">
        <declaredVariable name="response_time" type="Real"/>
      </properties>
    </subtypes>
  </root>
</type:StructuralModel>
