import ccm [platform:/resource/models/Hardware.structure]
import ccm [platform:/resource/models/Software.structure]

contract org_haec_app_pip_ffmpeg_PipFfmpeg implements software PiP . doPip { 
	
		mode pipFfmpeg1 { 
			requires resource CPU { 
				frequency min: 250  
				cpu_time max: f( max_video_length  )  = 5.0  + 0.5  * ( max_video_length  )  
				
					energyRate:  5.2 
						} 
			requires resource RAM { 
				free min: 52.0
				
					energyRate:  5.2 
						}
			provides run_time min: f( max_video_length  )  = 5.0  + 0.5  * ( max_video_length  )  
			} 
		} 
		