import ccm [platform:/resource/models/Hardware.structure]
import ccm [platform:/resource/models/Software.structure]

contract org_haec_app_scaling_ffmpeg_ScalingFfmpeg implements software Scaling . scale { 
	
		mode scaleFfmpeg1 { 
			requires resource CPU { 
				frequency min: 253.3  
				cpu_time max: f( max_video_length  )  = 10.0  + 1.0 * ( max_video_length  )  
				
					energyRate:  5.0
						} 
			requires resource RAM { 
				free min: 5.0
				
					energyRate:  5.33 
						} 
			requires component CopyScalingFile1 { }
		provides utility max: 1.0
			provides run_time min: f( max_video_length  )  = 13.0  + 1.0  * ( max_video_length  )  
			} 
		} 
		contract org_haec_app_scaling_mencoder_ScalingMencoder implements software Scaling . scale { 
	
		mode scaleMencoder1 { 
			requires resource CPU { 
				frequency min: 253.2  
				cpu_time max: f( max_video_length  )  = 10.0  + 1.1  * ( max_video_length  )  
				
					energyRate:  5.0
						} 
			requires resource RAM { 
				free min: 5.0
				
					energyRate:  5.32 
						} 
			requires component CopyScalingFile1 { }
		provides utility max: 1.0
			provides run_time min: f( max_video_length  )  = 13.0  + 1.1  * ( max_video_length  )  
			} 
		} 
		contract org_haec_app_scaling_handbrake_ScalingHandbrake implements software Scaling . scale { 
	
		mode scaleHandbrake1 { 
			requires resource CPU { 
				frequency min: 253.1  
				cpu_time max: f( max_video_length  )  = 10.0  + 1.2  * ( max_video_length  )  
				
					energyRate:  5.0
						} 
			requires resource RAM { 
				free min: 5.0
				
					energyRate:  5.31 
						} 
			requires component CopyScalingFile1 { }
		provides utility max: 1.0
			provides run_time min: f( max_video_length  )  = 10.0  + 1.2  * ( max_video_length  )  
			} 
		} 
		
contract org_haec_app_scaling_noop_ScalingNoop implements software Scaling.scale {
	mode scaleNoop1 {
		requires resource CPU { 
			frequency min: 253.0
			energyRate:  0.1
		}
		requires component CopyScalingFile1 { }
		provides utility max: 0.1
		provides run_time max: 0.01
	}
}
		