import ccm [platform:/resource/models/Hardware.structure]
import ccm [platform:/resource/models/Software.structure]

contract org_haec_app_censor_ffmpeg_CensorFfmpeg implements software Censor . censor { 
	
		mode censorFfmpeg1 { 
			requires resource CPU { 
				frequency min: 501  
				cpu_time max: f( max_video_length  )  = 10.0000091552702  + 6.05468746274711E-4  * ( max_video_length  )  
				
					energyRate:  5.1 
						} 
			requires resource RAM { 
				free min: 51  
				
					energyRate:  5.1 
						} 
			provides run_time min: f( max_video_length  )  = 0.0  + 8.18684895846757E-4  * ( max_video_length  )  
			} 
		} 
		
