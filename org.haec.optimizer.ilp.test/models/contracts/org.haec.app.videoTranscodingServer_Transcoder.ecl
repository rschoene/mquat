import ccm [platform:/resource/models/Hardware.structure]
import ccm [platform:/resource/models/Software.structure]

contract org_haec_app_transcoder_ffmpeg_TranscoderFfmpeg implements software Transcoder . transcode { 
	
		mode transcoderFfmpeg1 { 
			requires resource CPU { 
				frequency min: 500  
				cpu_time max: f( max_video_length  )  = 0.0  + 4.74117242507475E-4  * ( max_video_length  )  
				
					energyRate:  5.0 
						} 
			requires resource RAM { 
				free min: 50  
				
					energyRate:  5.0 
						} 
			provides run_time min: f( max_video_length  )  = 0.0  + 4.74074001944311E-4  * ( max_video_length  )  
			} 
		} 
		
