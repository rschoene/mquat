import ccm [platform:/resource/models/Hardware.structure]
import ccm [platform:/resource/models/Software.structure]

contract org_haec_app_controller_delegating_DelegatingController implements software Controller . pipScaled { 
	
		mode pipScaledModeDelegating { 
			requires component Scaling { }
			requires component PiP { }
			provides run_time min: f( max_video_length  )  = 0.0  + 0.001  * ( max_video_length  )
			} 
		} 
		
