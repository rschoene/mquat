/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.pbo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorFactory;
import org.coolsoftware.coolcomponents.ccm.behavior.CostParameter;
import org.coolsoftware.coolcomponents.ccm.behavior.Occurrence;
import org.coolsoftware.coolcomponents.ccm.behavior.Pin;
import org.coolsoftware.coolcomponents.ccm.behavior.State;
import org.coolsoftware.coolcomponents.ccm.behavior.StateMachine;
import org.coolsoftware.coolcomponents.ccm.behavior.Workload;
import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Behavior;
import org.coolsoftware.coolcomponents.ccm.variant.CostParameterBinding;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
//import org.coolsoftware.simulation.workload.resource.workload.Simulator;
import org.coolsoftware.theatre.energymanager.Optimizer;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.GeneratorUtil;
import org.haec.theatre.utils.CollectionsUtil;
import org.sat4j.pb.SolverFactory;
import org.sat4j.pb.core.PBSolver;
import org.sat4j.pb.reader.OPBReader2010;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;

/**
 * Pseudo boolean optimizer.
 * @author Sebastian Götz
 */
public class PBOGenerator implements Optimizer {
	private static Set<String> binaryVars = new HashSet<String>();
	private static Map<String, String> binaryVarMapping = new HashMap<String, String>();
	private static Map<String, Set<HWComponentRequirementClause>> resourceReqs = new HashMap<String, Set<HWComponentRequirementClause>>();
	private static Map<String, Map<String, CcmValue>> providedProperties = new HashMap<String, Map<String, CcmValue>>();
	private static Map<String, Map<String, CcmValue>> requiredProperties = new HashMap<String, Map<String, CcmValue>>();

	private static String opbdpLinux = "/home/sebastian/dev/opbdp-1.1.3/opbdp";
	private static String opbdpWin = /*"C:/dev/opbdp1.1.3.exe"*/ "D:/Programme/opbdp/opbdp-1.1.4.exe";

	private static int numConstraints = 0;
	private static int numVars = 0;

	private static boolean useSAT4J = false;

	public static Mapping runPBO(Request req, Map<String,EclFile> lemContracts,
			String pbo) {
		File reqFile = new File("pbotmp");
		return runPBO(req, lemContracts, reqFile, pbo);
	}

	public static Mapping runPBO(Request req, Map<String,EclFile> lemContracts,
			File reqFile, String pbo) {
		Mapping ret = new Mapping();
		File opbFile = new File(reqFile.getAbsolutePath() + ".opb");
		System.out.println("opbFile: " + opbFile.getAbsolutePath());
		try {
			if (!opbFile.exists())
				if (!opbFile.createNewFile())
					System.out.println("cannot create opb file");

			/* only for debugging */
			FileWriter fw = new FileWriter(opbFile, false);
			fw.write(pbo);
			fw.close();
			System.out.println("written PBO into file: "
					+ opbFile.getAbsolutePath() + File.separator
					+ opbFile.getName());

			if (useSAT4J)
				useSAT4J(ret, opbFile);
			else
				useOPBDP(ret, opbFile);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	private static void useOPBDP(Map<String, String> ret, File opbFile) {
		try {
			File here = new File(".");
			System.out.println("cwd: " + here.getAbsolutePath());
			if (opbFile.getAbsolutePath().length() > 32) {
				// move it to where the solver is
				File copy = new File("copy.opb");
				copy.createNewFile();
				FileReader r = new FileReader(opbFile);
				FileWriter w = new FileWriter(copy);
				int i;
				while ((i = r.read()) != -1) {
					w.write(i);
				}
				w.flush();
				w.close();
				r.close();
				opbFile = copy;
				System.out.println("copied pbo file");
			}
			long start = System.nanoTime();

			String opbdp = "";
			if (System.getProperty("os.name").equals("Linux")) {
				opbdp = opbdpLinux;
			} else {
				opbdp = opbdpWin;
			}
			ProcessBuilder pb = new ProcessBuilder(opbdp, "-f"
					+ opbFile.getAbsolutePath(), "-s");
			pb.directory(here);
			pb.redirectErrorStream(true);

			final Process p = pb.start();
			
			Scanner s = new Scanner(p.getInputStream()).useDelimiter("\\Z");

			File copy = new File("copy.opb");
			if (copy.exists())
				copy.deleteOnExit();

			String msg = "Optimal Mapping:\n\n";
			String sol = "";
			sol = s.next();
			long duration = System.nanoTime() - start;
			System.out.println("solving took: " + duration / 1000 / 1000
					+ " ms");

			String[] selects = sol.substring(
					sol.indexOf("0-1 Variables fixed to 1 :")
							+ "0-1 Variables fixed to 1 :".length() + 1).split(
					" ");
			for (String x : selects) {
				if (x.length() == 0 || x.equals(" "))
					break;
				String mappingVar = getKeyForValue(x);
				if (mappingVar == null)
					break;
				String[] parts = mappingVar.split("#");
				String impl = parts[1];
				String mode = parts[2];
				String server = parts[3];
				msg += impl + "(" + mode + ") => " + server + "\n";
				ret.put(impl, server);
			}

			System.out.println(msg);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} 
	}

	private static String getKeyForValue(String x) {
		for (String key : binaryVarMapping.keySet()) {
			if (binaryVarMapping.get(key).equals(x))
				return key;
		}
		return null;
	}

	private static void useSAT4J(Map<String, String> ret, File opbFile)
			throws IOException, FileNotFoundException {
		try {
			PBSolver solver = SolverFactory
					.newCompetPBCPMixedConstraintsMinObjective();
			solver.setTimeout(10); // 10 sec timeout
			OPBReader2010 reader = new OPBReader2010(solver);
			IProblem problem = reader
					.parseInstance(new FileInputStream(opbFile));

			System.out.println("\n==========================");

			String msg = "Optimal Mapping:\n\n";
			String solution = reader.decode(problem.model());
			for (String str : solution.split(" ")) {
				String longVersion = binaryVarMapping.get(str);
				String[] parts = longVersion.split("#");
				String impl = parts[1];
				String mode = parts[2];
				String server = parts[3];
				msg += impl + "(" + mode + ") => " + server + "\n";
				ret.put(impl, server);
			}

			System.out.println(msg);

		} catch (ParseFormatException e) {
			System.out.println("Cannot parse OPB!");
		} catch (ContradictionException e) {
			System.out.println("UNSAT (trivial - contradiction)");
		}
	}

	public static String generatePBOforRequest(Request req, Map<String,EclFile> lemContracts,
			IGlobalEnergyManager gem, String appName) {
		long start = System.nanoTime();
		binaryVars = new HashSet<String>();
		resourceReqs = new HashMap<String, Set<HWComponentRequirementClause>>();
		providedProperties = new HashMap<String, Map<String, CcmValue>>();
		requiredProperties = new HashMap<String, Map<String, CcmValue>>();

		CcmExpressionInterpreter interpreter = new CcmExpressionInterpreter();

		numConstraints = 0;
		numVars = 0;

		// Set<String> usageVars = new HashSet<String>();

		String pbo = "";

		Platform platform = req.getHardware();
		VariantModel vm = platform.getHwmodel();
		Resource root = (Resource) vm.getRoot();
		Map<String, Resource> containers = new HashMap<String, Resource>();
		// get all servers
		for (Resource r : root.getSubresources()) {
			containers.put(r.getName(), r);
		}
		pbo += generateDecisionVarsAndCache(lemContracts, containers, req, gem, appName, null);
		pbo += "\n";
		pbo = eliminateDuplicates(pbo);

		// now go for resource constraints
		// for each subresource of containers introduce rules of the following
		// kind sum(b#impl#mode#container*usage#container#resource#property) <=
		// resource-capacity
		pbo += "* resource usage boundary constraints\n";
		for (Resource r : root.getSubresources()) {
			String containerName = GeneratorUtil.cleanString(r.getName());
			for (Resource sr : r.getSubresources()) {
				//pbo += "*  resource: "+sr.getName()+"\n";
				String srname = GeneratorUtil.cleanString(sr.getName());
				String tname = sr.getSpecification().getName();
				for (VariantPropertyBinding vpb : sr.getPropertyBinding()) {
					String pname = GeneratorUtil.cleanString(vpb.getProperty()
							.getDeclaredVariable().getName());
					int numEffects = 0;
					
					//pbo += "*   property: "+pname+"\n";
					CcmValue val = interpreter.interpret(vpb
							.getValueExpression());

					if (val instanceof CcmString)
						continue;
					
					for (String implModeServer : resourceReqs.keySet()) {
						if(!implModeServer.endsWith(containerName)) continue;
						Set<HWComponentRequirementClause> hrcs = resourceReqs
								.get(implModeServer);
						for (HWComponentRequirementClause hrc : hrcs) {
							for (PropertyRequirementClause prc : hrc
									.getRequiredProperties()) {
								if (hrc.getRequiredResourceType().getName()
										.equals(tname) && prc.getRequiredProperty().getDeclaredVariable().getName().equals(pname)) {
									long value = 0;
									if(prc.getMinValue() != null) {
										value = GeneratorUtil.getCcmValueAsInt(interpreter
												.interpret(prc.getMinValue()));
									} else {
										value = GeneratorUtil.getCcmValueAsInt(interpreter
												.interpret(prc.getMaxValue()));
									}
									if(value != 0) { // TODO check
										pbo += value+" "
												+ binaryVarMapping.get("b#"
														+ implModeServer) + " +";
										numEffects++;
									}
								}
							}
						}
					}
					if(numEffects > 0) {
						pbo = pbo.substring(0, pbo.length() - 2);
						if(vpb.getProperty().getValOrder().equals(Order.DECREASING))
							pbo += " >= ";
						else 
							pbo += " <= ";
						pbo += GeneratorUtil.getCcmValueAsInt(val) + ";\n";
						numConstraints++;
					}
				}
			}
		}

		Map<String, Long> reqNFP = new HashMap<String, Long>();

		// add request requirements on NFPs into map to use them as upper bound
		// for following NFP constraints
		for (PropertyRequirementClause prc : req.getReqs()) {

			String reqPropName = prc.getRequiredProperty()
					.getDeclaredVariable().getName();
			long val;
			if(prc.getMinValue() != null)
				val = GeneratorUtil.getCcmValueAsInt(new CcmExpressionInterpreter()
					.interpret(prc.getMinValue()));
			else
				val = GeneratorUtil.getCcmValueAsInt(new CcmExpressionInterpreter()
				.interpret(prc.getMaxValue()));
			reqNFP.put(reqPropName, val);

		}

		pbo += "\n* provided/required property constraints\n";
		// provided properties constraints
		for (String propName : providedProperties.keySet()) {
			String strProv = "* property: " + propName + "\n";
			Map<String, CcmValue> provProp = providedProperties.get(propName);
			boolean asc = true;
			for (String binaryVar : provProp.keySet()) {
				CcmValue val = provProp.get(binaryVar);
				asc = val.isAscending();
				long v = GeneratorUtil.getCcmValueAsInt(val);
				if(v != 0)
				strProv += v+ " "
						+ binaryVarMapping.get(binaryVar) + " +";
			}
			strProv = strProv.substring(0, strProv.length() - 2);

			for (String reqPropName : requiredProperties.keySet()) {
				if (!reqPropName.equals(propName))
					continue;
				Map<String, CcmValue> reqProp = requiredProperties
						.get(propName);
				for (String binaryVar : reqProp.keySet()) {
					CcmValue val = reqProp.get(binaryVar);
					long v = GeneratorUtil.getCcmValueAsInt(val); //TODO consider int vals..
					if(!val.isAscending()) asc = false;
					if(v != 0)
					strProv += " -" +  v + " "
							+ binaryVarMapping.get(binaryVar);
				}
			}


			if (reqNFP.get(propName.substring(propName.indexOf("#")+1)) != null && req.getComponent().getName().equals(propName.substring(0,propName.indexOf("#")))) {
				if(!asc)
					strProv += " <= ";
				else
					strProv += " >= ";
				strProv += reqNFP.get(propName.substring(propName.indexOf("#")+1));
			} else {
				if(!asc)
					strProv += " >= ";
				else
					strProv += " <= ";
				strProv += "0";
			}

			pbo += strProv + ";\n";
			numConstraints++;
		}

		// pbo += "* required property constraints\n";
		// // required properties constraints
		// for (String propName : requiredProperties.keySet()) {
		// String strReq = "";
		// Map<String, CcmValue> reqProp = requiredProperties.get(propName);
		// for (String binaryVar : reqProp.keySet()) {
		// CcmValue val = reqProp.get(binaryVar);
		// strReq += getCcmValueAsInt(val) + " " +
		// binaryVarMapping.get(binaryVar) + " + ";
		// }
		// strReq = strReq.substring(0, strReq.length() - 3);
		// strReq = " <= "+propName;
		// pbo += strReq + ";\n";
		// numConstraints++;
		// }

		String objective = "min: ";
		for (String bv : binaryVars) {
			objective += (int)getWeightForMappingVar(bv, interpreter, req) + " "
					+ binaryVarMapping.get(bv) + " + ";
		}
		objective = objective.substring(0, objective.length() - 3);
		objective += ";\n\n";

		pbo = objective + pbo;

		pbo = "* #variable= " + numVars + " #constraint= " + numConstraints
				+ "\n" + pbo;

		System.out.println(pbo);

		long duration = System.nanoTime() - start;

		System.out.println("Generation took: " + (duration / 1000 / 1000)
				+ " ms");
		return pbo;
	}
	
	/**This method drops duplicate syntactic/selection constraints, which occur due to the double nature of {@link #generateDecisionVarsAndCache(Map, Map, Request, OsgiGem, String, Set)} (i.e., selection constraints + filling cache).
	 * 
	 * @param pbo The generated PBO including duplicates
	 * @return The PBO without duplicates
	 */
	private static String eliminateDuplicates(String pbo) {
		String[] lines = pbo.split("\n");
		Set<String> noDuplicates = new HashSet<String>();
		for(String line : lines) {
			if(!noDuplicates.add(line)) {
				numConstraints--;
			}
		}
		String ret = "";
		for(String line : noDuplicates) {
			ret += line+"\n";
		}
		return ret;
	}

	/**
	 * Computes the weight of a mapping variable (b#VLC#highQ#Server1) by
	 * interpreting its resource requirements. Currently, the sum of all NFRs is
	 * used. Later, energy should be used to normalize the values.
	 * 
	 * @param bv
	 *            name of the mapping variable
	 * @return int - weight of variable in terms of resource requirements
	 */
	private static double getWeightForMappingVar(String bv,
			CcmExpressionInterpreter interpreter, Request req) {
		// read from resourceReqs (bv without b# and #container)
		double weight = 0;

		String reqsByImplMode = bv.substring(2);
		Set<HWComponentRequirementClause> reqs = resourceReqs
				.get(reqsByImplMode);
		for (HWComponentRequirementClause hrc : reqs) {
			double energyRate = hrc.getEnergyRate();
			ResourceType resType = hrc.getRequiredResourceType();
			for (PropertyRequirementClause prc : hrc.getRequiredProperties()) {
				
				if(prc.getMinValue() != null) {
					weight = ((CcmReal)interpreter.interpret(prc
							.getMinValue())).getRealValue();
				} else {
					weight = ((CcmReal)interpreter.interpret(prc
							.getMaxValue())).getRealValue();
				}
				//the time is to be multiplied with the power implied by the hardware setup specified in the quality mode
				//-> simulate the resource in that mode using the computed workload (e.g., cpu_time)
				//TODO overcome assumption: only one resource of a type per server
				String sName = bv.substring(bv.lastIndexOf("#")+1);
				
				//now access variant model of infrastructure...
				Resource root = (Resource)req.getHardware().getHwmodel().getRoot();
				for(Resource server : root.getSubresources()) {
					if(server.getName().equals(sName)) {
						for(Resource res : server.getSubresources()) {
							if(res.getSpecification().getName().equals(resType.getName())) {
								Behavior behavior = res.getBehavior();
								if(behavior != null) {
									if(behavior.getTemplate() instanceof StateMachine) {
										StateMachine sm = (StateMachine)behavior.getTemplate();
										//now set the mode for the resource and evaluate the behavior using the workload from above
										Workload w = BehaviorFactory.eINSTANCE.createWorkload();
										Occurrence o = BehaviorFactory.eINSTANCE.createOccurrence();
										o.setTime(0);
										o.setLoad((int)weight);
										//TODO get correct pin (only works if there's just one pin)
										Pin pin = sm.getPins().iterator().next();
										o.setPin(pin);
										w.getItems().add(o);
										w.setResource(res);
//										Simulator sim = new Simulator();
										List<CostParameterBinding> bindings = new ArrayList<CostParameterBinding>();
										for(State state : sm.getStates()) {
											CostParameter cp = state.getParameter().iterator().next(); //there should be always only one
											CostParameterBinding cpb = VariantFactory.eINSTANCE.createCostParameterBinding();
											cpb.setParameter(cp);
											Variable v = VariablesFactory.eINSTANCE.createVariable();
											v.setName(cp.getName());
											RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
											rle.setValue(energyRate);
											v.setInitialExpression(rle);
											cpb.setDeclaredVariable(v);
											bindings.add(cpb);
										}
//										sm = sim.simulate(sm, w, bindings);
										CcmReal energy = ((CcmReal)sm.getPower().getDeclaredVariable().getValue());
										if(energy != null) {
											weight = energy.getRealValue();
											System.out.println("simulated: "+weight+" in "+((CcmInteger)sm.getTimer().getDeclaredVariable().getValue()).getIntegerValue());
										}
										sm.reset();
									}
								} else {
									System.out.println("Behavior of "+res.getName()+" should be set to compute the induced energy consumption.");
								}
							}
						}
					}
				}
			}
		}
		
		return weight;
	}

	/**
	 * Fills global sets/maps: binaryVars, binaryVarMapping, providedProperties,
	 * requiredProperties and resourceReqs. Also builds up selection constraint 
	 * (at least on implementation per type).
	 * 
	 * @param lemContracts {@link Map}[String,{@link EclFile}]
	 * @param containers {@link Map}[String,{@link Resource}]
	 * @param req {@link Request}
	 * @return
	 */
	private static String generateDecisionVarsAndCache(Map<String,EclFile> lemContracts,
			Map<String, Resource> containers, Request req, IGlobalEnergyManager gem, String appName, Set<String> depBvn) {
		
		String ret = "";
		//deptype,reftype,container,contract
		Map<String, Map<String, Map<String, EclFile>>> deps = new HashMap<String, Map<String, Map<String, EclFile>>>();
				
		for(String server : lemContracts.keySet()) {
			EclFile ecl = lemContracts.get(server);
			for (EclContract ctr : ecl.getContracts()) {
				if (ctr instanceof SWComponentContract) {
					SWComponentContract swc = (SWComponentContract) ctr;
					setMetaparameters(req, ctr);
					
					String implName = GeneratorUtil.cleanString(swc.getName());
					for (SWContractMode swm : swc.getModes()) {
	
						String modeName = GeneratorUtil.cleanString(swm.getName());
						// generate binary vars (impl/mode mapped to server =
						// true/false)
						
						String bvn = "b#" + implName + "#" + modeName + "#"
								+ GeneratorUtil.cleanString(server);

						if(binaryVars.add(bvn))
							binaryVarMapping.put(bvn, "x" + (++numVars));

						ret += binaryVarMapping.get(bvn) + " +";

						// provided property per mode
						for (SWContractClause cl : swm.getClauses()) {
							if (cl instanceof ProvisionClause) {
								handleProvisions(req, bvn, cl, swc.getComponentType());
							} else if (cl instanceof SWComponentRequirementClause) {
								handleSWRequirements(req, deps, bvn, cl, gem, appName);
							} else if (cl instanceof HWComponentRequirementClause) {
								handleHWRequirements(req, implName, modeName, cl, server);
							}
						}						
					}
				}
			}
		}
		ret = ret.substring(0, ret.length() - 2) + " ";
		if(depBvn != null && depBvn.size() > 0) {
			for(String db : depBvn) {
				ret += "-"+binaryVarMapping.get(db)+" ";
			}
			//ret = ret.substring(0,ret.length()-3);
			ret += "= 0;\n";
		} else {
			ret += "= 1;\n";
		}
		
		//recursively follow dependencies
		for (String newDepBvn : deps.keySet()) {
			Map<String, Map<String, EclFile>> depFilesPerContainerByDependentType = deps.get(newDepBvn);
			if(depFilesPerContainerByDependentType == null) {
				throw new RuntimeException("Unresolved ECL contract dependency.");
			} else {
				for(String refType : depFilesPerContainerByDependentType.keySet()) {
					Map<String, EclFile> depFilesPerContainer = depFilesPerContainerByDependentType.get(refType);
					String depRet = generateDecisionVarsAndCache(depFilesPerContainer, containers, req, gem, appName, deps.keySet());
					ret += depRet;
				}
			}
		}
		numConstraints++;

		return ret;
	}

	/**
	 * @param req
	 * @param ctr
	 */
	private static void setMetaparameters(Request req, EclContract ctr) {
		//set metaparameter values from request 
		for(Parameter mp : ((SWComponentContract) ctr).getPort().getMetaparameter()) {
			for(MetaParamValue mpv : req.getMetaParamValues()) {
				if(mpv.getMetaparam().getName().equals(mp.getName())) {
					RealLiteralExpression ile = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
					ile.setValue(mpv.getValue());
					mp.setValue(new CcmExpressionInterpreter().interpret(ile));
				}
			}
		}
	}

	/**
	 * @param req
	 * @param implName
	 * @param modeName
	 * @param clause
	 */
	private static void handleHWRequirements(Request req, String implName,
			String modeName, SWContractClause clause, String server) {
		
			HWComponentRequirementClause hrc = (HWComponentRequirementClause) clause;
			Set<HWComponentRequirementClause> reqs = resourceReqs
					.get(implName + "#" + modeName + "#" + server);
			if (reqs == null) {
				reqs = new HashSet<HWComponentRequirementClause>();
			} else {
				for(HWComponentRequirementClause existing : reqs) {
					if(existing.getRequiredResourceType().getName().equals(hrc.getRequiredResourceType().getName())) return;
				}
			}
			reqs.add(hrc);
			resourceReqs.put(implName + "#" + modeName + "#" + server, reqs);
			
			for(PropertyRequirementClause prc : ((HWComponentRequirementClause) clause).getRequiredProperties()) {
				CcmReal val;
				boolean min = true;
				Statement minmaxval = prc.getMinValue();
				if(minmaxval == null) {
					minmaxval = prc.getMaxValue();
					min = false;
				}
				if(minmaxval instanceof FunctionExpression) {
					val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
					RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
					rle.setValue(val.getRealValue());
					if(min)
						prc.setMinValue(rle);
					else
						prc.setMaxValue(rle);
				} 
			}
		
	}

	/**
	 * @param req
	 * @param deps
	 * @param bvn
	 * @param cl
	 */
	private static void handleSWRequirements(Request req, Map<String, Map<String, Map<String, EclFile>>> deps,
			String bvn, SWContractClause cl, IGlobalEnergyManager gem, String appName) {
		SWComponentType t = ((SWComponentRequirementClause) cl)
				.getRequiredComponentType();
//		String eclURI;
//		if(req.eResource() != null) {
//			URI reqURI = req.getComponent().eResource().getURI();
//			String reqStr = reqURI.toFileString();
//			if(reqStr.indexOf(File.separator) != -1) {
//				reqStr = reqStr.substring(0,reqStr.lastIndexOf(File.separator));
//			}
//			eclURI = reqStr+File.separator+t.getEclUri();
//		} else {
//			eclURI = t.getEclUri();
//		}
//		EclFile dep = ResolverUtil.loadEclFile(eclURI);
		
		//load EclFiles of dependent component type per container
		try {
			Map<String,String> serializedContractByContainer = gem.getContractsForComponent(appName, t.getName());
			Map<String, EclFile> referrencedContractByContainer = new HashMap<String, EclFile>();
			StructuralModel appModel = CCMUtil.deserializeSWStructModel(appName, gem.getApplicationModel(appName), false);
			StructuralModel hwModel = CCMUtil.deserializeHWStructModel(
					((IGlobalResourceManager) gem.getGlobalResourceManager()).getInfrastructureTypes(), false);
			for(String container : serializedContractByContainer.keySet()) {
				String serialContract = serializedContractByContainer.get(container);
				EclFile f = CCMUtil.deserializeECLContract(appModel, hwModel, appName, t.getName(), serialContract, false);
				referrencedContractByContainer.put(container, f);
			}
			Map<String, Map<String, EclFile>> dep = new HashMap<String, Map<String,EclFile>>();
			dep.put(t.getName(), referrencedContractByContainer);
			deps.put(bvn,dep);
		} catch(IOException ioe) {
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
		
		for(PropertyRequirementClause prc : ((SWComponentRequirementClause) cl).getRequiredProperties()) {
			boolean min = true;
			Statement minmaxval = prc.getMinValue();
			if(minmaxval == null) {
				minmaxval = prc.getMaxValue();
				min = false;
			}
			CcmReal val;
			if(minmaxval instanceof FunctionExpression) {
				val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
				rle.setValue(val.getRealValue());
				if(min)	prc.setMinValue(rle);
				else prc.setMaxValue(rle);
			} else if (prc.getFormula() instanceof FormulaTemplate) {
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
				rle.setValue(0);
				val = (CcmReal)new CcmExpressionInterpreter().interpret(rle);
			}	else {
				val = (CcmReal)new CcmExpressionInterpreter().interpret(minmaxval);
			}
			if(min) val.setAscending(false); else val.setAscending(true);
			String propName = ((SWComponentRequirementClause) cl).getRequiredComponentType().getName() + "#" + prc.getRequiredProperty().getDeclaredVariable().getName();
			Map<String,CcmValue> reqVal = requiredProperties.get(propName);
			if(reqVal == null) reqVal = new HashMap<String,CcmValue>();
			reqVal.put(bvn, val);
			requiredProperties.put(propName, reqVal);
		}
	}

	/**
	 * @param req
	 * @param bvn
	 * @param cl
	 */
	private static void handleProvisions(Request req, String bvn,
			SWContractClause cl, SWComponentType t) {
		CcmReal val = null;
		boolean min = true;
		Statement minmaxval = ((ProvisionClause)cl).getMinValue();
		if(minmaxval == null) {
			minmaxval = ((ProvisionClause)cl).getMaxValue();
			min = false;
		}
		if(minmaxval instanceof FunctionExpression) {
			val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min)
				((ProvisionClause)cl).setMinValue(rle);
			else 
				((ProvisionClause)cl).setMaxValue(rle);
		} else if (((ProvisionClause)cl).getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmReal)new CcmExpressionInterpreter().interpret(rle);
		} else {
			val = (CcmReal)new CcmExpressionInterpreter().interpret(minmaxval);
		}
		if(((ProvisionClause)cl).getProvidedProperty().getValOrder().equals(Order.INCREASING))
			val.setAscending(true); else 
			val.setAscending(false);
		
		String propName = t.getName()+"#"+((ProvisionClause)cl).getProvidedProperty().getDeclaredVariable().getName();
		Map<String,CcmValue> propVal = providedProperties.get(propName);
		if(propVal == null) propVal = new HashMap<String,CcmValue>();
		propVal.put(bvn, val);
		providedProperties.put(propName, propVal);
	}

	@Override
	public Mapping optimize(Object[] args) {
		try {
			Request req = (Request)args[0];
			@SuppressWarnings("unchecked")
			Map<String, EclFile> eclFiles = (Map<String, EclFile>)args[1];
			IGlobalEnergyManager gem = (IGlobalEnergyManager)args[2];
			@SuppressWarnings("unchecked")
			Map<String, Object> options = (Map<String, Object>) args[3];
			Boolean verbose = CollectionsUtil.get(options, OPTION_VERBOSE, false);
			String appName = (String) options.get(OPTION_APPNAME);
			Map<String, List<String>> fileNameMap = CollectionsUtil.get(options, OPTION_RESOURCENAMES, new HashMap<String, List<String>>());
//			problemName = CollectionsUtil.get(options, OPTION_PROBLEMNAME, appName.replace(',', '_'));
			String pbo = generatePBOforRequest(req, eclFiles, gem, appName);
			return runPBO(req, eclFiles, pbo);
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.Optimizer#getId()
	 */
	@Override
	public String getId() {
		return "org.haec.optimizer.pbo";
	}
}
