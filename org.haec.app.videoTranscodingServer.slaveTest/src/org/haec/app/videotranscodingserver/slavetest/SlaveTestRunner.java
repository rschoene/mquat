/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.slavetest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.haec.app.pip.ffmpeg.PipFfmpeg;
import org.haec.app.scaling.ffmpeg.ScalingFfmpeg;
import org.haec.app.scaling.handbrake.ScalingHandbrake;
import org.haec.app.scaling.mencoder.ScalingMencoder;
import org.haec.app.transcoder.ffmpeg.TranscoderFfmpeg;
import org.haec.app.transcoder.handbrake.TranscoderHandbrake;
import org.haec.app.transcoder.mencoder.TranscoderMencoder;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.ScalingScale;
import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.apps.util.SharedMethods;
import org.haec.apps.util.VideoResolution;
import org.haec.apps.util.VideoSettings;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.StringUtils;
import org.haec.theatre.utils.settings.Setting;

/**
 * A simple application to test most implementations of org.haec.app.videoTranscodingServer
 * Uses the config file in <code>theatre/slavetest.properties</code>.
 * @author René Schöne
 */
public class SlaveTestRunner implements Callable<List<Object[]>> {
	
	private Logger log = Logger.getLogger(SlaveTestRunner.class);
	private BufferedWriter bw;
	private List<Object[]> values = new ArrayList<>();
	private VideoSettings settings;
	
	public SlaveTestRunner(VideoSettings settings) {
		this.settings = settings;
		try {
			bw = new BufferedWriter(new FileWriter("slavetest.csv", true));
		} catch (IOException e) {
			log.warn("No csv file will be written", e);
			bw = null;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public List<Object[]> call() {
		values.clear();
		runScaling(settings.runScalingFfmepg(), new ScalingFfmpeg() {
			protected <S> S invoke(Class<S> clazz, String compName,
					String portName, Object... params) {
				return clazz.cast(params[0]);
			}
		});
		runScaling(settings.runScalingMencoder(), new ScalingMencoder() {
			protected <S> S invoke(Class<S> clazz, String compName,
					String portName, Object... params) {
				return clazz.cast(params[0]);
			}
		});
		runScaling(settings.runScalingHandbrake(), new ScalingHandbrake() {
			protected <S> S invoke(Class<S> clazz, String compName,
					String portName, Object... params) {
				return clazz.cast(params[0]);
			}
		});

		runTranscoder(settings.runTranscoderFfmepg(), new TranscoderFfmpeg() {
			protected <S> S invoke(Class<S> clazz, String compName,
					String portName, Object... params) {
				return clazz.cast(params[0]);
			}
		});
		runTranscoder(settings.runTranscoderMencoder(), new TranscoderMencoder() {
			protected <S> S invoke(Class<S> clazz, String compName,
					String portName, Object... params) {
				return clazz.cast(params[0]);
			}
		});
		runTranscoder(settings.runTranscoderHandbrake(), new TranscoderHandbrake() {
			protected <S> S invoke(Class<S> clazz, String compName,
					String portName, Object... params) {
				return clazz.cast(params[0]);
			}
		});
		
		runPip(settings.runPipFfmepg(), new PipFfmpeg());
		
		if(bw != null) {
			try { bw.flush(); } catch (IOException e) { e.printStackTrace(); }
		}
		return values;
	}

	private void runScaling(Setting<Boolean> runScaling, ScalingScale instance) {
		boolean shouldRun = runScaling.get();
		String name = runScaling.getName();
		if(!shouldRun) { log.info("Skipping " + name); return; }
		log.info("Running " + name);
		Integer width = settings.scalingWidth().get();
		Integer height = settings.scalingHeight().get();
		String fname = firstFilePath();
		FileProxy fp = instance.scale(wrap(fname), width, height, false);
		analyzeScaling(name, fp.getFile());
	}

	private void runTranscoder(Setting<Boolean> runTranscoder, TranscoderTranscode instance) {
		boolean shouldRun = runTranscoder.get();
		String name = runTranscoder.getName();
		if(!shouldRun) { log.info("Skipping " + name); return; }
		log.info("Running " + name);
		String vCodecString = settings.transcoderVCodec().get();
		String aCodecString = settings.transcoderACodec().get();
		String fname = firstFilePath();
		
		Iterable<String> vCodecs = StringUtils.tokenize(vCodecString, ",");
		Iterable<String> aCodecsIterable = StringUtils.tokenize(aCodecString, ",");
		List<String> aCodecs = new ArrayList<>();
		for(String s : aCodecsIterable) { aCodecs.add(s); }

		for(String vCodec : vCodecs) {
			for(String aCodec : aCodecs) {
				FileProxy fp = instance.transcode(wrap(fname), vCodec.trim(), aCodec.trim());
				analyzeTranscoder(name, fp.getFile(), vCodec, aCodec);
			}
		}
	}

	protected String firstFilePath() {
		return settings.firstVideoName().get().trim();
	}

	private void runPip(Setting<Boolean> runPip, PipDoPip instance) {
		boolean shouldRun = runPip.get();
		String name = runPip.getName();
		if(!shouldRun) { log.info("Skipping " + name); return; }
		log.info("Running " + name);
		Integer pos = settings.pipPos().get();
		String fname1 = firstFilePath();
		String fname2 = settings.secondVideoName().get().trim();
		File f = instance.doPip(new File(fname1), new File(fname2), pos);
		List<String> list = analyze(f);
		if(list == null || list.isEmpty()) { log.warn("Got empty list."); return; }
	}

	private void analyzeScaling(String name, File f) {
		AtomicBoolean pass = new AtomicBoolean(true);
		List<String> list = analyze(f);
		if(list == null || list.isEmpty()) { log.warn("Got empty list."); return; }
		VideoResolution res = SharedMethods.getVideoResolutionFfmpegHandbrake(list);
		values.add(assertEquals(name, pass, "Scaling.height differs", res.height, settings.scalingHeight().get()));
		values.add(assertEquals(name, pass, "Scaling.width differs", res.width, settings.scalingWidth().get()));
		if(pass.get()) { log.info("Everything fine for " + name); }
	}

	private void analyzeTranscoder(String name, File f, String vCodec, String aCodec) {
		AtomicBoolean pass = new AtomicBoolean(true);
		List<String> list = analyze(f);
		if(list == null || list.isEmpty()) { log.warn("Got empty list."); return; }
		String vcodecString = SharedMethods.getVideoCodec(list);
		String acodecString = SharedMethods.getAudioCodec(list);
		values.add(assertCodecEquals(name, pass, f, "Transcoder vcodec differs", vcodecString, vCodec));
		values.add(assertCodecEquals(name, pass, f, "Transcoder acodec differs", acodecString, aCodec));
		if(pass.get()) { log.info("Everything fine for " + name); }
	}
	
	private List<String> analyze(File f) {
		if(f != null && f.exists()) { try {
			Process p = Runtime.getRuntime().exec(new String[]{"ffprobe", f.getAbsolutePath()});
			String stderr = FileUtils.readAsString(p.getErrorStream(), true);
			System.err.println(stderr);
			String stdout = FileUtils.readAsString(p.getInputStream(), true);
			System.out.println(stdout);
			Iterable<String> iterable = StringUtils.tokenize(stderr, "\n");
			List<String> list = new ArrayList<>();
			for(String s : iterable) { list.add(s); }
			return list;
		} catch (IOException e) {
			log.warn("Problems with executing ffprobe on " + f, e);
		}}
		else {
			log.warn("File does not exist: " + f);
		}
		return Collections.emptyList();
	}

	/** Self-made assertEquals
	 * @param pass 
	 * @return values written to csv */
	private Object[] assertEquals(String name, AtomicBoolean pass, String message, long actual, long expected) {
		boolean match = (expected == actual);
		if(!match) { log.warn(message + ": Expected " + expected +", but was " + actual); }
		pass.compareAndSet(true, match);
		return writeToCsvAndReturn(currentTime(), name, expected, actual, match);
	}

	/** Self-made assertEquals for codecs
	 * @return values written to csv */
	private Object[] assertCodecEquals(String name, AtomicBoolean pass, File f, String message, String actualCodec, String expectedCodec) {
		boolean match;
		switch(expectedCodec) {
		case "theora":
			match = actualCodec.startsWith("theora") || actualCodec.startsWith("libt");
			break;
		default: match = actualCodec.startsWith(expectedCodec);
		}
		if(!match) { log.warn(message + ": Expected " + expectedCodec +", but was " + actualCodec); }
		pass.compareAndSet(true, match);
		return writeToCsvAndReturn(currentTime(), name, f.getAbsolutePath(), expectedCodec, actualCodec, match);
	}

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	private String currentTime() {
		return sdf.format(GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @param fname the path to a local file
	 * @return a resolved file proxy
	 */
	private FileProxy wrap(String fname) {
		return new FileProxy(new File(fname));
	}
	
	private Object[] writeToCsvAndReturn(Object... values) {
		if(bw != null) { try { FileUtils.writeToCsv(bw, values); }
		catch (IOException e) { } }
		return values;
	}
	
}
