/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.slavetest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.TheatreCommandProvider;
import org.haec.apps.util.VideoSettings;
import org.haec.theatre.utils.AbstractTheatreCommandProvider;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.haec.theatre.utils.settings.PropertiesSetting;
import org.osgi.service.component.ComponentContext;

/**
 * A TheatreCommandProvider providing the command {@value #COMMAND_NAME}.
 * @author René Schöne
 */
public class SlaveTestCommandProvider extends AbstractTheatreCommandProvider {
	
	private static final String COMMAND_NAME = "slaveTest";
	private static final String DESCRIPTION  = "Tests encoders with various settings.";
	private VideoSettings settings;
	private Logger log = Logger.getLogger(SlaveTestCommandProvider.class);
	
	public SlaveTestCommandProvider() {
		super(COMMAND_NAME);
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.AbstractTheatreCommandProvider#execute0(java.lang.String[])
	 */
	@Override
	protected String execute0(String[] params) {
		try {
			SlaveTestRunner str = new SlaveTestRunner(settings);
			List<Object[]> result = str.call();
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for(Object[] values : result) {
				if(first) { first = false; }
				else { sb.append(","); }
				sb.append(Arrays.toString(values));
			}
			return sb.toString();
		} catch(Exception e) {
			return e.getMessage();
		}
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.TheatreCommandProvider#getDescription()
	 */
	@Override
	public String getDescription() {
		return DESCRIPTION;
	}
	
	protected void setVideoSettings(VideoSettings s) {
		this.settings = s;
		if(settings.firstVideoName().get() == null) {
			try {
				// search for files with the given regex, sort them (getting smallest first)
				String[] cmdarray = new String[]{"/bin/sh", "-c", "find "+settings.getResourceDir().get()+
						" -name "+settings.videoRegexName().get()+
						" | xargs ls -Sr | head -n 1"};
				log.debug("Running " + StringUtils.join(" ", cmdarray));
				InputStream is = Runtime.getRuntime().exec(cmdarray)
						.getInputStream();
				((PropertiesSetting<String>) settings.firstVideoName()).set(FileUtils.readAsString(is).trim());
			} catch (IOException e) {
				log.error("Problems with getting default first file", e);
			}
		}
		if(settings.secondVideoName().get() == null) {
			try {
				// search for files with the given regex, sort them and getting second smallest
				String[] cmdarray = new String[]{"/bin/sh", "-c", "find "+settings.getResourceDir().get()+
						" -name "+settings.videoRegexName().get()+
						" | xargs ls -Sr | head -n 2 | tac | head -n 1"};
				log.debug("Running " + StringUtils.join(" ", cmdarray));
				InputStream is = Runtime.getRuntime().exec(cmdarray)
						.getInputStream();
				((PropertiesSetting<String>) settings.secondVideoName()).set(FileUtils.readAsString(is).trim());
			} catch (IOException e) {
				log.warn("Problems with getting default second file", e);
			}
		}
	}
	
	protected void activate(ComponentContext ctx) {
		RemoteOsgiUtil.register(ctx.getBundleContext(), TheatreCommandProvider.class, this);
	}

}
