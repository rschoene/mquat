/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.slavetest;

import java.util.Arrays;
import java.util.Map;

import org.coolsoftware.theatre.TheatreCommandProvider;
import org.haec.theatre.utils.AbstractTheatreCommandProvider;
import org.haec.theatre.utils.BundleUtils;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;

/**
 * A TheatreCommandProvider providing the command {@value #COMMAND_NAME}.
 * @author René Schöne
 */
public class ActivateBundleProvider extends AbstractTheatreCommandProvider {

	private static final String COMMAND_NAME = "startBundle";
	private static final String DESCRIPTION  = "Starts the given bundle(s).";
	private BundleContext context;
	
	public ActivateBundleProvider() {
		super(COMMAND_NAME);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.TheatreCommandProvider#getDescription()
	 */
	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.AbstractTheatreCommandProvider#execute0(java.lang.String[])
	 */
	@Override
	protected String execute0(String[] params) {
		Map<String, Integer> bundleStates = BundleUtils.startBundles(context, Arrays.asList(params));
		Function<Integer, String> getStateName = new Function<Integer, String>() {
			
			@Override
			public String apply(Integer s) {
				switch(s) {
				case Bundle.ACTIVE: return "active";
				case Bundle.INSTALLED: return "installed";
				case Bundle.RESOLVED: return "resolved";
				case Bundle.STARTING: return "starting";
				case Bundle.STOPPING: return "stopping";
				case Bundle.UNINSTALLED: return "uninstalled";
				default: return "unknown";
				}
			}
		};
		return CollectionsUtil.map(bundleStates, getStateName).toString();
	}
	
	protected void activate(ComponentContext ctx) {
		this.context = ctx.getBundleContext();
		RemoteOsgiUtil.register(context, TheatreCommandProvider.class, this);
	}

}
