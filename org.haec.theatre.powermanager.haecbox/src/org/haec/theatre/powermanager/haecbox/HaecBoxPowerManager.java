/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.powermanager.haecbox;

import java.net.URI;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.PowerManager;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;

/**
 * Simple powermanager relying on external entity to switch on and off hosts.
 * It just checks the connected LEMs upon blocking calls.
 * @author René Schöne
 */
public class HaecBoxPowerManager implements PowerManager {

	private IGlobalEnergyManager gem;
	private Logger log = Logger.getLogger(HaecBoxPowerManager.class);
	private boolean hostnameEqualIp;
	private TheatreSettings settings;

	public HaecBoxPowerManager() {
//		BundleContext context = settings.getContext();
//		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
//		gem = RemoteOsgiUtil.getRemoteService(
//				remote, AppUtil.getGEMUri(), IGlobalEnergyManager.class);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#powerOnBlocking(java.lang.String)
	 */
	@Override
	public boolean powerOnBlocking(String hostname) {
		log.debug(hostname);
		return checkLocalMgrsBlocking(hostname, true);
	}

	/**
	 * @param hostname the hostname to search for
	 * @param containing if true: wait until mgrs contain hostname.
	 *  if false: wait until mgrs do not contain hostname.
	 * @return if containing: found hostname. if not containing: does not contain hostname.
	 */
	protected boolean checkLocalMgrsBlocking(String hostname, boolean containing) {
		settings.reload();
		String ip = hostnameEqualIp ? hostname : PlatformUtils.getIpOf(hostname);
		URI uri = URI.create(RemoteOsgiUtil.createRemoteOsgiUriString(ip));
		long waitUntil = System.currentTimeMillis() + settings.getPowermanagerMaxWait().get();
		boolean found = false;
		while(System.currentTimeMillis() < waitUntil &&
				containing ^ (found = getLocalMgrs().contains(uri))) {
			// check if uri is in local manager list
			// continue, if containing and is in, or not containing and is not in
			if(settings.getPowermanagerVerbose().get()) {
				log.debug("Waiting for " + hostname + " to " + 
						(containing ? "boot" : "shut down"));
			}
			try { Thread.sleep(settings.getPowermanagerUpdateRate().get());
			} catch (InterruptedException e) { break; }
		}
		return containing ^ !found;
	}

	protected List<java.net.URI> getLocalMgrs() {
		return gem.getLocalMgrs();
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#powerOnAsynchronous(java.lang.String)
	 */
	@Override
	public boolean powerOnAsynchronous(String hostname) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#shutdownBlocking(java.lang.String)
	 */
	@Override
	public boolean shutdownBlocking(String hostname) {
		log.debug(hostname);
		return checkLocalMgrsBlocking(hostname, false);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#shutdownAsynchronous(java.lang.String)
	 */
	@Override
	public boolean shutdownAsynchronous(String hostname) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getState(java.lang.String)
	 */
	@Override
	public int getState(String hostname) {
		return PowerManager.UNKNOWN;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getDrawnCurrent(java.lang.String)
	 */
	@Override
	public int getDrawnCurrent(String hostname) {
		return PowerManager.ERROR_VALUE;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getTotalConsumedEnergy(java.lang.String)
	 */
	@Override
	public float getTotalConsumedEnergy(String hostname) {
		return PowerManager.ERROR_VALUE;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getUptime(java.lang.String)
	 */
	@Override
	public int getUptime(String hostname) {
		return PowerManager.ERROR_VALUE;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#setHostnameEqualIp()
	 */
	@Override
	public void setHostnameEqualIp(boolean hostnameEqualIp) {
		this.hostnameEqualIp = hostnameEqualIp;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#isHostnameEqualIp()
	 */
	@Override
	public boolean isHostnameEqualIp() {
		return this.hostnameEqualIp;
	}
	
	protected void setSettings(TheatreSettings s) {
		this.settings = s;
	}
	
	protected void unsetSettings(TheatreSettings s) {
		this.settings = null;
	}
	
	protected void setGem(IGlobalEnergyManager gem) {
		if(settings.getPowermanagerVerbose().get()) {
			log.debug("Gem set to " + gem);
		}
		this.gem = gem;
	}
	
	protected void unsetGem(IGlobalEnergyManager gem) {
		this.gem = null;
	}

}
