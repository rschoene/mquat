/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map.Entry;
import java.util.Arrays;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.haec.theatre.api.Monitor;
import org.haec.theatre.monitor.MonitorRepository;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * A simple GUI creating a button for every method starting with {@value #TASK_PREFIX}. 
 * @author René Schöne
 */
public class UtilsGui {
	
	private static final String TASK_PREFIX = "task";
	private JFrame frame;
	
	public UtilsGui() {
		createFrame();
		setVisible(false);
	}
	
	private void createFrame() {
		frame = new JFrame("UtilsGui");
		frame.setLayout(new GridLayout(0, 4, 10, 10)); // 4 columns
		for(Method m : UtilsGui.class.getMethods()) {
//			System.out.println(m.getName());
			if(accept(m)) {
//				System.out.println(" included");
				addButton(m);
			}
//			System.out.println(String.format("Name: %s, annotations: %s", m.getName(), Arrays.toString(m.getAnnotations())));
		}
		addCloseButton();
		frame.pack();
		frame.setLocationRelativeTo(null);
	}

	private boolean accept(Method m) {
//		return m.isAnnotationPresent(UtilsGuiTask.class);
		return m.getName().startsWith(TASK_PREFIX);
	}

	private void addCloseButton() {
		JButton button = new JButton("Close");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		frame.add(button);
	}

	private void addButton(final Method m) {
		String name = m.getName().substring(TASK_PREFIX.length());
		JButton button = new JButton(name);
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					m.invoke(UtilsGui.this);
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				}
			}
		});
		frame.add(button);
	}

	public void setVisible(boolean b) {
		frame.setVisible(b);
	}
	
	public boolean isVisible() {
		return frame.isVisible();
	}
	
	public void taskPrintSystemProperties() {
		final int MAX_LINE_LENGTH = 80;
		final String delimiter = ", ";
		Properties props = System.getProperties();
		StringBuilder sb = new StringBuilder();
		int lineLenght = 0;
		for(Entry<Object, Object> entry : props.entrySet()) {
			String line = entry.getKey() + "=" + entry.getValue();
			lineLenght += line.length();
			if(lineLenght > MAX_LINE_LENGTH) {
				// break line
				sb.append("\n").append(line);
				lineLenght = 0;
			}
			else {
				sb.append(delimiter).append(line);
			}
		}
		System.out.println(sb.substring(delimiter.length()));
	}
	
	public void taskHello() {
		System.out.println("Hello World!");
	}
	
	public void taskPrintMonitors() {
		ServiceReference<MonitorRepository> ref = UtilsGuiActivator.getContext().getServiceReference(MonitorRepository.class);
		if(ref == null) {
			System.out.println("Monitor-Repository unavailable");
		}
		MonitorRepository repo = UtilsGuiActivator.getContext().getService(ref);
		List<Monitor> list = repo.getAllMonitors(true);
		System.out.println("all available: " + list);
	}
	
	public void taskServices() {
		BundleContext context = UtilsGuiActivator.getContext();
		ServiceReference<?>[] refs;
		try {
			refs = context.getServiceReferences((String) null, null);
		} catch (InvalidSyntaxException e) {
			e.printStackTrace();
			return;
		}
		for(ServiceReference<?> ref : refs) {
			System.out.println(ref);
			String[] keys = ref.getPropertyKeys();
			for(String key : keys) {
				String value;
				if(key.equals("objectClass")) {
					value = Arrays.toString((String[]) ref.getProperty(key));
				}
				else {
					value = ref.getProperty(key).toString();
				}
				System.out.println(String.format(" %s: %s", key, value));
			}
		}
	}
	
	public void taskStartPip() throws BundleException {
		BundleContext context = UtilsGuiActivator.getContext();
		String pipffmpeg = "org.haec.app.pip.ffmpeg";
		for(Bundle b : context.getBundles()) {
			if(b.getSymbolicName().contains(pipffmpeg)) {
				b.start();
			}
		}
	}

}
