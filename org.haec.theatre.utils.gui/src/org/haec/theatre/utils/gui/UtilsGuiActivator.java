/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.gui;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Creates and shows the {@link UtilsGui}.
 * @author René Schöne
 */
public class UtilsGuiActivator implements BundleActivator {

	private static BundleContext context;
	private static UtilsGui ugui;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		UtilsGuiActivator.context = bundleContext;
		if(ugui == null) {
			ugui = new UtilsGui();
		}
		if(ugui.isVisible()) {
			// do nothing
			System.out.println("[UtilsGui] Starting, but already visible.");
		}
		else {
			ugui.setVisible(true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		UtilsGuiActivator.context = null;
		if(ugui == null) {
			System.out.println("[UtilsGui] Stopping, but no Gui found.");
		}
		else {
			if(!ugui.isVisible()) {
				// do nothing
				System.out.println("[UtilsGui] Stopping, but already invisible.");
			}
			else {
				ugui.setVisible(false);
			}
		}
	}

}
