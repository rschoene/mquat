/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.powermanager.cambrionix;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.PowerManager;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.settings.BooleanSetting;
import org.haec.theatre.utils.settings.IntegerSetting;
import org.haec.theatre.utils.settings.SettingHolder;
import org.haec.theatre.utils.settings.StringSetting;
import org.osgi.service.component.ComponentContext;

import cambrionix.api.Cambrionix;
import cambrionix.api.CambrionixException;
import cambrionix.api.CambrionixPort;

/**
 * Powermanager using a cambrionix API to power on and off hosts itself.
 * @author René Schöne
 */
public class CambrionixPowerManager extends SettingHolder implements PowerManager {
	
	private Logger log = Logger.getLogger(CambrionixPowerManager.class);
	private Cambrionix c;
	private boolean hostnameEqualsIp;

	public static final String fname = "theatre/hostmapping.properties";
	public static final int HOSTNAME_NOT_FOUND = -1;
	
	/** manager.device (default: "/dev/ttyUSB1") */
	public StringSetting DEVICE =
			new StringSetting("manager.device", "/dev/ttyUSB1");
	/** manager.failBlockCalls (default: true) */
	public BooleanSetting FAIL_BLOCKING =
			new BooleanSetting("manager.failBlockCalls", true);
	/** manager.maxBlockingTries (default: 5) */
	public IntegerSetting MAX_TRIES_BLOCKING =
			new IntegerSetting("manager.maxBlockingTries", 5);
	/** manager.waitInterval (default: 1000) */
	public IntegerSetting WAIT_INTERVAL = 
			new IntegerSetting("manager.waitInterval", 1000);
	private Properties p;

	/**
	 * @throws CambrionixException 
	 * 
	 */
	public CambrionixPowerManager() throws CambrionixException {
		c = new Cambrionix(DEVICE.get());
		c.disableAllProfiles();
		c.enableProfile("4");
		c.startPoller();
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#powerOnBlocking(java.lang.String)
	 */
	@Override
	public boolean powerOnBlocking(String hostname) {
		checkBlocking();
		boolean success = powerOnAsynchronous(hostname);
		if(!success) { log.warn("No success power on " + hostname); }
		return waitForPort(hostname, false);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#powerOnAsynchronous(java.lang.String)
	 */
	@Override
	public boolean powerOnAsynchronous(String hostname) {
		CambrionixPort p = getPort(hostname);
		if(p == null) {
			log.debug("Cambrionix port is null for " + hostname + ". exit.");
			return false;
		}
		if(p.isOff()) { try {
			p.turnOn();
		} catch (CambrionixException e) {
			log.error("Error while turning on " + hostname, e);
			return false;
		} }
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#shutdownBlocking(java.lang.String)
	 */
	@Override
	public boolean shutdownBlocking(String hostname) {
		checkBlocking();
		boolean success = shutdownAsynchronous(hostname);
		if(!success) { log.warn("No success shutdown " + hostname); }
		return waitForPort(hostname, true);
	}

	private void checkBlocking() {
		if(FAIL_BLOCKING.isTrue()) {
			throw new UnsupportedOperationException("Blocking calls not directly supported.");
		}
	}

	private boolean waitForPort(String hostname, boolean waitForOff) {
		CambrionixPort port = getPort(hostname);
		try {
			for (int i = 0; i < MAX_TRIES_BLOCKING.get(); i++) {
				if(waitForOff ? port.isOff() : port.isOn()) { return true; }
				log.debug("Waiting for " + hostname);
				Thread.sleep(WAIT_INTERVAL.get());
			}
		} catch (InterruptedException e) { log.debug("Got interupted", e); }
		return waitForOff ? port.isOff() : port.isOn();
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#shutdownAsynchronous(java.lang.String)
	 */
	@Override
	public boolean shutdownAsynchronous(String hostname) {
		CambrionixPort p = getPort(hostname);
		if(p == null) { return false; }
		if(p.isOn()) { try {
			p.turnOff();
		} catch (CambrionixException e) {
			log.error("Error while shutting down " + hostname, e);
			return false;
		} }
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getState(java.lang.String)
	 */
	@Override
	public int getState(String hostname) {
		CambrionixPort p = getPort(hostname);
		if(p == null) { return UNKNOWN; }
		if(p.isOff()) {
			return OFFLINE;
		} else if(p.isOn()) {
			return ONLINE;
		} else {
			return UNKNOWN;
		}
	}
	
	public CambrionixPort getPort(String hostname) {
		if(hostnameEqualsIp) {
			hostname = PlatformUtils.getHostNameOf(hostname);
		}
		int portId = getPortId(hostname);
		if(portId == HOSTNAME_NOT_FOUND) {
			log.error("Hostname not found");
			return null;
		}
		try {
			return c.getPort(portId);
		} catch (CambrionixException e) {
			log.error("Error for " + hostname, e);
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getDrawnCurrent(java.lang.String)
	 */
	@Override
	public int getDrawnCurrent(String hostname) {
		CambrionixPort p = getPort(hostname);
		if(p == null) { return ERROR_VALUE; }
		return p.getDrawnCurrent();
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getTotalConsumedEnergy(java.lang.String)
	 */
	@Override
	public float getTotalConsumedEnergy(String hostname) {
		CambrionixPort p = getPort(hostname);
		if(p == null) { return ERROR_VALUE; }
		return p.getEnergy();
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#getUptime(java.lang.String)
	 */
	@Override
	public int getUptime(String hostname) {
		CambrionixPort p = getPort(hostname);
		if(p == null) { return ERROR_VALUE; }
		return p.getUptime();
	}
	
	public CambrionixPort getPort(int portId) throws CambrionixException {
		return c.getPort(portId);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#setHostnameEqualIp(boolean)
	 */
	@Override
	public void setHostnameEqualIp(boolean hostnameEqualIp) {
		this.hostnameEqualsIp = hostnameEqualIp;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.PowerManager#isHostnameEqualIp()
	 */
	@Override
	public boolean isHostnameEqualIp() {
		return this.hostnameEqualsIp;
	}
	
	protected void activate(ComponentContext ctx) {
		p = loadProperties(fname);
		reload(p, this);
	}
	
	public int getPortId(String hostname) throws NumberFormatException {
		String property = p.getProperty(hostname);
		if(property == null) {
			return HOSTNAME_NOT_FOUND;
		}
		return Integer.parseInt(property);
	}

}
