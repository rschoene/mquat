/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.copyFile.transcoder;

import org.haec.app.copyFile.AbstractCopyFile;
import org.haec.app.videotranscodingserver.CopyTranscoderGetFile;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.utils.FileProxy;

/**
 * Implementation of org.haec.app.copyFile.transcoder.CopyFileTranscoderImpl
 * @author René Schöne
 */
public class CopyFileTranscoderImpl extends AbstractCopyFile implements CopyTranscoderGetFile {

	public CopyFileTranscoderImpl() {
		super();
	}

	@Override
	public FileProxy getFile(Object fp1) {
		return super.getFile(fp1);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "CopyTranscoderFile1";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.copyFile.transcoder.CopyFileTranscoderImpl";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new CopyFileTranscoderBenchmark();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.copyFile.transcoder";
	}

}
