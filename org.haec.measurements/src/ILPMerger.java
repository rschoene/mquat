/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore.Builder;

/**
 * Measurement class for ILP.
 * @author Sebastian Götz
 */
public class ILPMerger {
	private static String resultFile = "ilp-result-max-2.txt";
	
	public static void main(String[] args) {
		StringBuffer folder = new StringBuffer();
		StringBuffer ret = new StringBuffer();
		String base = "C:/dev/workspaces/runtime-CoolSoftware-Workbench.product/GeneratedExamples/ex/";
		folder.append(base);
		
		for(int a = 2; a <= 100; a++) {
			for(int b = 2; b <= 100; b++) {
				String subfolder = "gen_"+a+"x"+b+"-1x1/";
				folder.append(subfolder);
				folder.append(resultFile);
				StringBuffer add = work(a,b,folder);
				if(add == null) break;
				ret.append(add);
				folder.delete(base.length(), folder.length());
			}
		}
//		ret.append(a+";"+b+";"+tgen+";"+tarch+";"+tres+";"+tnfp+";"+tobj+";"+tsolve+";"+vars+";"+constraints+";"+hasSol+";"+obj+"\n");
		String toPrint = "Components;Server;Tgen;Tarch;Tres;Tnfp;Tobj;Tsolve;vars;constraints;hasSol;obj\n";
		toPrint += ret.toString();
		File f = new File("ilp-max-2.Csv");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			bw.write(toPrint);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(toPrint);
	}
	
	private static int getValueForString(String line, String key, String delimiter) {
		if(line.contains(key)) {
			int start = line.indexOf(key)+key.length()+1;
			int stop = line.indexOf(delimiter,start);
			try {
				int ret = Integer.parseInt(line.substring(start,stop));
				return ret;
			}
			catch (Exception e) {
				System.err.println(key+" "+delimiter);
				System.err.println(line);
			}
		} else {
			System.err.println(key+" not in "+line);
		}
		return -1;
	}
	
	private static double getDoubleValueForString(String line, String key, String delimiter) {
		if(line.contains(key)) {
			int start = line.indexOf(key)+key.length()+1;
			int stop = line.indexOf(delimiter,start);
			try {
				double ret = Double.parseDouble(line.substring(start,stop));
				return ret;
			}
			catch (Exception e) {
				System.err.println(key+" "+delimiter);
				System.err.println(line);
			}
		} else {
			System.err.println(key+" not in "+line);
		}
		return -1;
	}
	
	private static StringBuffer work(int a, int b, StringBuffer uri) {
		StringBuffer ret = new StringBuffer();
		int tsolve = 0, vars = 0, constraints = 0, tgen = 0, tarch = 0,tres = 0,tnfp = 0,tobj = 0, hasSol = 0;
		double obj = 0.0;
		try {
			InputStream is = new FileInputStream(new File(uri.toString()));
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer contents = new StringBuffer();
			while((line = br.readLine()) != null) {
				contents.append(line+"\n");
			}
			line = contents.toString();
			
			tsolve = getValueForString(line, "Solver Time:", " ms");
			vars = getValueForString(line, "Cols:", "\n");
			constraints = getValueForString(line, "Rows:", "\n");
			obj = getDoubleValueForString(line, "Obj:", "\n");
			
			int start = line.indexOf("Optimal Mapping:")+"Optimal Mapping:".length();
			int stop = line.indexOf("Solver Time:");
			String sol = line.substring(start,stop).trim();
			
			if(sol.length() > 0) hasSol = 1;
			
			br.close();
			is.close();
		
			uri.delete(uri.length()-resultFile.length(), uri.length());
			uri.append("ilp-gen-result-2.txt");
			InputStream is2 = new FileInputStream(new File(uri.toString()));
			BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
			
			StringBuffer contents2 = new StringBuffer();
			String line2;
			while((line2 = br2.readLine()) != null) {
				contents2.append(line2+"\n");
			}
			line2 = contents2.toString();
			
			tarch = getValueForString(line2, "Architectural Constraints:", " ms");
			tres = getValueForString(line2, "Resource Negotiation     :", " ms");
			tnfp = getValueForString(line2, "Software NFP Negotiation :", " ms");
			tobj = getValueForString(line2, "Objective Function       :", " ms");
			tgen = tarch+tres+tnfp+tobj;
			
			br2.close();
			is2.close();
		} catch (IOException e) {
			return null;
		}
		ret.append(a+";"+b+";"+tgen+";"+tarch+";"+tres+";"+tnfp+";"+tobj+";"+tsolve+";"+vars+";"+constraints+";"+hasSol+";"+obj+"\n");
		//System.out.print(ret);
		return ret;
	}
}
