/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.util.Set;
import java.util.TreeSet;

/**
 * Playground class.
 * @author Sebastian Götz
 */
public class Combiner {
	public static void main(String[] args) {
		String[][] x = new String[][] {{"a","b"},{"c","d"},{"e","f"},{"x","y"}};
		System.out.println(combos(x,"",0));
	}
	
	private static Set<String> combos(String[][] in, String soFar, int start) {
		Set<String> ret = new TreeSet<String>();
		for (int i = start; i < in.length; i++) {
			String[] block = in[i];
			for (int x = 0; x < block.length; x++) {
				// get all combinations possible for this selection
				if (i < in.length - 1) {
					ret.addAll(combos(in, soFar + "" + block[x], start + 1));
				} else {
					ret.add(soFar + "" + block[x]);
				}
			}
			return ret;
		}
		return ret;
	}
}
