/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.scaling.ffmpeg;

import java.io.File;
import java.util.List;

import org.haec.app.scaling.AbstractScaling;
import org.haec.app.videotranscodingserver.ScalingScale;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.apps.util.VideoUtil;
import org.haec.ffmpeg.FfmpegUtil;
import org.haec.theatre.api.Benchmark;

/**
 * Implementation of org.haec.app.scaling.ffmpeg.ScalingFfmpeg
 * @author René Schöne
 */
public class ScalingFfmpeg extends AbstractScaling implements ScalingScale {
	
	public ScalingFfmpeg() {
		super(FfmpegUtil.sharedInstance);
	}

	@Override
	protected void addArguments(List<String> params, File fileVideo, int w,
			int h, boolean mult, File out, ErrorState ignore) {
		params.add("-y");
		params.add("-i");
		params.add(fileVideo.getAbsolutePath());
		addScaleArguments(params, w, h, mult);
		addOtherArguments(params);
		params.add(out.getAbsolutePath());
	}

	private void addScaleArguments(List<String> params, int w, int h, boolean mult) {
		params.add("-vf");
		StringBuilder sb = new StringBuilder("scale=");
		if(mult) {
			sb.append("in_w");
			if(w < 0) {
				sb.append('/');
				w = -w;
			}
			else
				sb.append('*');
		}
		sb.append(w);
		sb.append(':');
		if(mult) {
			sb.append("in_h");
			if(h < 0) {
				sb.append('/');
				h = -h;
			}
			else
				sb.append('*');
		}
		sb.append(h);
		params.add(sb.toString());
	}

	private void addOtherArguments(List<String> params) {
		// support for aac is experimental, allow anyway
		params.add("-strict");
		params.add("experimental");
		// maybe disable audio
		if(util.shouldEncodeNoSound()) {
			params.add("-an");
		}
	}

	@Override
	protected int handleProcess(Process startedProcess, VideoUtil util)
			throws InterruptedException {
		return util.waitForever(startedProcess, true);
	}
	
	@Override
	protected VideoResolutionCalculator getResolutionCalculator() {
		// use ffmpeg directly
		return FfmpegUtil.sharedInstance;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.scaling.ffmpeg.ScalingFfmpeg";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new ScalingFfmpegBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.scaling.ffmpeg";
	}

}