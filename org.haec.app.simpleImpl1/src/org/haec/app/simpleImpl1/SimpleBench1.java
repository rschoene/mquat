/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.simpleImpl1;

import java.util.ArrayList;
import java.util.List;

import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;

/**
 * 
 * @author René Schöne
 */
public class SimpleBench1 implements Benchmark {

	SimpleImpl1 comp = new SimpleImpl1();
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration()
	 */
	@Override
	public Object iteration(BenchmarkData d) {
		return comp.doWork(((Data) d).input);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>();
		for(int particle_count = 100; particle_count <= 700; particle_count += 200) {
			Data d = new Data();
			d.particle_count = particle_count;
			d.input = particle_count + (int) (Math.random() * 10);
			result.add(d);
		}
		return result;
	}
}
