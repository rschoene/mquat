/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.simpleImpl1;

import org.haec.app.simpleApp.SimpleApp;
import org.haec.app.simpleApp.SimpleComponent;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.TaskBasedImpl;

/**
 * A simple impl to test THEATRE refactoring into OSGi-DS.
 * @author René Schöne
 */
public class SimpleImpl1 extends TaskBasedImpl implements SimpleComponent {

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return SimpleApp.APP_NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "SimpleComponent";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "SimpleImpl1";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new SimpleBench1();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.simpleImpl1";
	}

	/* (non-Javadoc)
	 * @see org.haec.app.simpleApp.SimpleComponent#doWork(java.util.List)
	 */
	@Override
	public int doWork(int input) {
		System.out.println("Doing some hard work for " + input + " ...");
		try {
			Thread.sleep(input);
		} catch (Exception ignore) {}
		return 2 * input;
	}

}
