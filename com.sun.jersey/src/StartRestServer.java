

import java.awt.HeadlessException;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;

public class StartRestServer {

	public static void main(String[] args) throws IllegalArgumentException, IOException {
		String serverAddress;
		switch(args.length) {
		case 1:
			if(args[0].equals("-h") || args[0].equals("--help")) {
				printUsage();
				return;
			}
			// use as server address
			serverAddress = args[0];
			break;
		case 2:
			serverAddress = String.format("http://%s:%s/rest", args[0], args[1]);
			break;
		default:
			System.out.println("Using default.");
			serverAddress = "http://localhost:8080/rest";
		}
		System.out.println("Starting server at " + serverAddress);
		HttpServer server = HttpServerFactory.create( serverAddress );
		server.start();
		try {
			JOptionPane.showMessageDialog( null, "Ende" );
		} catch (HeadlessException e) {
			System.out.println("Press any key to close server...");
			System.in.read();
		}
		server.stop( 0 );
	}

	private static void printUsage() {
		System.out.println("Synopsis:\n StartRestServer [ServerAddress]");
		System.out.println("StartRestServer [Host] [Port]");
		System.out.println("Description:\n Starts the rest server either at the given [ServerAddress], or at"
				+ "http://[Host]:[Port]/rest, or at the default address if no parameter is given");
	}

}
