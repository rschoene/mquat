/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videoTranscodingServer.cui;

import java.io.File;
import java.io.Serializable;
import java.net.URI;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator starting the ConsoleStarter.
 * @author René Schöne
 */
public class CuiActivator implements BundleActivator {
	
	Logger log = Logger.getLogger(ConsoleStarter.class);

	@Override
	public void start(BundleContext context) throws Exception {
		try {
			String version = context.getBundle().getVersion().toString();
			log.info("cui " + version + " started");
			ConsoleStarter cs = new ConsoleStarter(context);
			ExecuteResult ee = cs.startRequest();
			Serializable result = ee.getResult();
			log.debug("result="+result);
			// maybe copy it
			if(result instanceof FileProxy) {
				URI thisUri = URI.create(RemoteOsgiUtil.getLemIp());
				File localFile = ((FileProxy) result).getFileFor(thisUri);
				log.info("result after unproxy as a file: " + localFile);
			}
			else if(result instanceof File) {
				log.info("result as a file: " + (File) result);
			}
		} catch (Throwable e) {
			log.warn("Got exception", e);
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		log.info("cui stopped");
	}

}
