/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videoTranscodingServer.cui;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.ui.BundleNames;
import org.haec.app.videotranscodingserver.ui.RequestStarter;
import org.haec.theatre.utils.BundleUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * Example class to start a fixed scenario (Controller.pipScaled() for two videos and relative scaling with factor 0.5)
 * @author René Schöne
 */
public class ConsoleStarter extends RequestStarter {
	
	private static final String KEY_FIRST_VIDEO  = "org.haec.app.videoTranscodingServer.cui.video1";
	private static final String KEY_SECOND_VIDEO = "org.haec.app.videoTranscodingServer.cui.video2";
	private final List<MetaparameterLine> metaParams;
	private final List<NfrLine> nfrs;
	private final List<ParameterLine> params;

	public ConsoleStarter(BundleContext context) {
		super(context);
		debug("new metaparameter line");
		MetaparameterLine mpl = new MetaparameterLine("max_video_length");
		debug("set value");
		mpl.setValue(3686400);
		metaParams = Collections.singletonList(mpl);
		
		debug("new nfr line");
		NfrLine nfrl = new NfrLine("response_time", 80000);
		nfrs = Collections.singletonList(nfrl);
		
		debug("get videos");
		File video1 = getFirstVideoFile();
		File video2 = getSecondVideoFile();
		debug("new parameter lines");
		params = Arrays.asList(new ParameterLine("fileVideo1", "Value", video1),
				new ParameterLine("fileVideo2", "Value", video2),
				new ParameterLine("w2", "Integer", -2),
				new ParameterLine("h2", "Integer", -2),
				new ParameterLine("mult", "Boolean", Boolean.TRUE),
				new ParameterLine("pos", "Integer", Integer.valueOf(PipDoPip.POS_CENTER)));
		debug("start bundles");
		BundleUtils.startBundles(context, BundleNames.getAllBundleNames());
	}

	private File getFirstVideoFile() {
//		Video v3 = ExampleVideoProvider.getVideos(3).get(0);
//		return v3.file;
		return new File(System.getProperty(KEY_FIRST_VIDEO));
	}

	private File getSecondVideoFile() {
//		Video v4 = ExampleVideoProvider.getVideos(4).get(0);
//		return v4.file;
		return new File(System.getProperty(KEY_SECOND_VIDEO));
	}

	@Override
	protected List<MetaparameterLine> getMetaparams() {
		return metaParams;
	}

	@Override
	protected List<NfrLine> getNfrs() {
		return nfrs;
	}

	@Override
	protected List<ParameterLine> getParams() {
		return params;
	}

	@Override
	protected String getSelectedPortName() {
		return "pipScaled";
	}

	@Override
	protected String getCompName() {
		return "Controller";
	}

	@Override
	protected String getAppName() {
		return "org.haec.app.videoTranscodingServer";
	}

	@Override
	protected String getSelectedOptimizer() {
		return "ilp";
	}

	@Override
	protected IGlobalEnergyManager getGEM() {
		String ip = "127.0.0.1";
		RemoteOSGiService remote = getRemoteOSGiService();
		return RemoteOsgiUtil.getRemoteService(remote, ip, IGlobalEnergyManager.class);
	}
	
	public RemoteOSGiService getRemoteOSGiService() {
		BundleContext context = FrameworkUtil.getBundle(this.getClass())
				.getBundleContext();
		return RemoteOsgiUtil.getRemoteOSGiService(context);
	}
	
	public static void main(String[] args) {
	}

}
