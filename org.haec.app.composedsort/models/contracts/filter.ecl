import ccm [../Server.structure]
import ccm [../composedsort.structure]

contract org_haec_app_filter_unsortedfilter_UnsortedFilter implements software Filter.filter { 
	mode fast {
    requires component Sort {
      response_time max: 50
    }
		requires resource CPU {			
			frequency min: 300			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 55.0		
		}		
		provides response_time <f_wtime_fast(list_size)> 	
	}	
	mode slow {		
	  requires component Sort;
		requires resource CPU {			
			frequency min: 100			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 45.0		
		}		
		provides response_time <f_wtime_slow(list_size)>
	}
}
