import ccm [../Server.structure]
import ccm [../composedsort.structure]

contract org_haec_app_listgen_random_Random implements software ListGen.generate { 
	mode fast {		
		requires resource CPU {			
			frequency min: 300			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 55.0		
		}		
		provides run_time <f_wtime_fast(list_size)> 	
	}	
	mode slow {		
		requires resource CPU {			
			frequency min: 100			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 45.0
		}		
		provides run_time <f_wtime_slow(list_size)>
	}
}