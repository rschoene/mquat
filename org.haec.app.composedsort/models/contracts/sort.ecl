import ccm [../Server.structure]
import ccm [../composedsort.structure]

contract org_haec_app_sort_Quicksort implements software Sort.sort { 
	mode immediate {	
    requires component ListGen;
		requires resource CPU {			
			frequency min: 300			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 55.0		
		}		
		provides run_time <f_wtime_fast(list_size)> 	
	}	
	mode delayed {
    requires component ListGen {
			response_time max: 50
		}			
		requires resource CPU {			
			frequency min: 100			
			cpu_time <f_cpu_time(list_size)>		
			energyRate: 30.0
		}		
		provides run_time <f_wtime_slow(list_size)>
	}
}

contract org_haec_app_sort_Javasort implements software Sort.sort { 
	mode immediate {
    requires component ListGen {
			response_time max: 50
		}			
		requires resource CPU {			
			frequency min: 300			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 55.0
		}		
		provides run_time <f_wtime_fast(list_size)> 	
	}	
	mode delayed {		
	  requires component ListGen {
			response_time max: 50
		}	
		requires resource CPU {			
			frequency min: 100			
			cpu_time <f_cpu_time(list_size)>	
			energyRate: 30.0
		}		
		provides run_time <f_wtime_slow(list_size)>
	}
}
