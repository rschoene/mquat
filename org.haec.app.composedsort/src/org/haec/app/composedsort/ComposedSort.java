package org.haec.app.composedsort;

import org.haec.theatre.api.App;

/**
 * Composed sort; creating, sorting and sorting of lists.
 * @author René Schöne
 */
public class ComposedSort implements App {

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getName()
	 */
	@Override
	public String getName() {
		return "org.haec.app.composedsort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getApplicationModelPath()
	 */
	@Override
	public String getApplicationModelPath() {
		return "models/composedsort.structure";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getContractsPath()
	 */
	@Override
	public String getContractsPath() {
		return "models/contracts";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getBundleSymbolicName()
	 */
	@Override
	public String getBundleSymbolicName() {
		return "org.haec.app.composedsort";
	}

}
