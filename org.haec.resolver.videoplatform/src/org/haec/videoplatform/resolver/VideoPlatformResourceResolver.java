/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoplatform.resolver;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.haec.apps.util.VideoSettings;
import org.haec.theatre.rest.util.RestUtils;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.ResourceResolver;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;

/**
 * Resource resolver using the information of the videoplatform in the HAEC-Box prototype.
 * @author René Schöne
 */
public class VideoPlatformResourceResolver implements ResourceResolver, IVideoPlatformResolver {

	private static final Logger log = Logger.getLogger(VideoPlatformResourceResolver.class);
	private WebTarget root;
	private VideoSettings settings;
	private ServiceRegistration<?> reg;
	private Client client;
	
	public VideoPlatformResourceResolver() {
		this.client = ClientBuilder.newClient();
		this.root = client.target(UriBuilder.fromUri("http://" + settings.getVideoplatformHost().get())
				.port(settings.getVideoPlatformPort().get()).build())
				.path(settings.getVideoPlatformPathVideoByName().get());
	}

	@Override
	public List<FileProxy> resolve(final String name) {
		if(settings.getVideoplatformResolverEnabled().get()) {
			// config will be updated in getVideoList
			List<Video> videoList = getVideoList(name);
			return parse(videoList);
		}
		else {
			File vidDir = new File(settings.getResourceDir().get());
			FilenameFilter filter = new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String fileName) {
					return fileName.contains(name);
				}
			};
			List<FileProxy> result = new ArrayList<>();
			// just search in local directory, if it is in
			for(File file: vidDir.listFiles(filter)) {
				result.add(new FileProxy(file));
			}
			return result;
		}
	}

	@Override
	public List<Video> getVideoList(String name) {
		log.debug("Parsing: " + name);
		Builder b = root.queryParam("name", name).request(MediaType.APPLICATION_JSON_TYPE);
		return RestUtils.getResponse(b, new GenericType<List<Video>>(){});
	}

	private List<FileProxy> parse(List<Video> videoList) {
		File vidDir = new File(settings.getResourceDir().get());
		List<FileProxy> result = new ArrayList<>();
		int ignoredVideos = 0;
		if(videoList != null) {
			for(Video video : videoList) {
				if(video.fname == null) { continue; }
				File file = new File(vidDir, video.fname);
//				for(String hostname : StringUtils.tokenize(video.hosts, ",")) {
				for(String hostname : video.hosts) {
					if(hostname.trim().isEmpty()) { ignoredVideos++; continue; }
					String ip = PlatformUtils.getIpOf(hostname);
					result.add(new FileProxy(file, ip == null ? null : URI.create(ip)));
				}
			}
		}
		if(log.isDebugEnabled()) {
			log.debug("Resolved videos: " + result.size() + ", ignored videos: " + ignoredVideos);
		}
		return result;
	}

	protected void setVideoSettings(VideoSettings vs) {
		this.settings = vs;
	}

	protected void unsetVideoSettings(VideoSettings vs) {
		this.settings = null;
	}
	
	protected void activate(ComponentContext ctx) {
		reg = RemoteOsgiUtil.register(ctx.getBundleContext(), this, ResourceResolver.class, IVideoPlatformResolver.class);
	}
	
	protected void deactivate(ComponentContext ctx) {
		if(reg != null) {
			reg.unregister();
		}
		client.close();
	}


}
