/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoplatform.resolver;

import java.io.Serializable;
import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Representation of a video.
 * @author René Schöne
 */
@XmlRootElement
public class Video implements Serializable{
	private static final long serialVersionUID = -6956550502643618288L;

	@XmlElement(name="uploadtime") public String uploadtime;
	@XmlElement public long popularity;
	@XmlElement public long user_id;
	@XmlElement public String name;
	@XmlElement public long fps;
	@XmlElement public long fsize;
	@XmlElement public String format;
	@XmlElement public String variant;
	@XmlElement public int height;
	@XmlElement public int width;
	@XmlElement public String audio_codec;
	@XmlElement public String fname;
	@XmlElement public String[] hosts;
	@XmlElement public double duration;
	@XmlElement public String video_codec;
	@XmlElement public double bitrate;
	@XmlElement public long id;
	@XmlElement
//	(name="total_hits")
	public int totalHits;

	@Override
	public String toString() {
		return "Video [uploadtime=" + uploadtime + ", popularity=" + popularity
				+ ", user_id=" + user_id + ", name=" + name + ", fps=" + fps
				+ ", fsize=" + fsize + ", format=" + format + ", variant="
				+ variant + ", height=" + height + ", width=" + width
				+ ", audio_codec=" + audio_codec + ", fname=" + fname
				+ ", hosts=" + Arrays.toString(hosts) + ", duration=" + duration
				+ ", video_codec=" + video_codec + ", bitrate=" + bitrate
				+ ", id=" + id + ", totalHits=" + totalHits + "]";
	}
	
}