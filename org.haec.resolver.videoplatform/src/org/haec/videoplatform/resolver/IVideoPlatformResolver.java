/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoplatform.resolver;

import java.util.List;

/**
 * Special resolver using the VideoPlatform in the HAEC-Box prototype.
 * @author René Schöne
 */
public interface IVideoPlatformResolver {

	/**
	 * Search for videos with the given name.
	 * @param name the name of the video to search
	 * @return the list of found videos
	 */
	public abstract List<Video> getVideoList(String name);

}
