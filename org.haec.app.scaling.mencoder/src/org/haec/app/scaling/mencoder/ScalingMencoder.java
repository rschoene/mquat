/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.scaling.mencoder;

import java.io.File;
import java.util.List;

import org.haec.app.scaling.AbstractScaling;
import org.haec.app.videotranscodingserver.ScalingScale;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.VideoResolution;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.apps.util.VideoUtil;
import org.haec.mencoder.MencoderUtil;
import org.haec.theatre.api.Benchmark;

/**
 * Implementation of org.haec.app.scaling.mencoder.ScalingMencoder
 * @author René Schöne
 */
public class ScalingMencoder extends AbstractScaling implements ScalingScale {

	public ScalingMencoder() {
		super(MencoderUtil.sharedInstance);
	}

	private void addScalingArgument(List<String> params, File fileVideo, int width, int height,
			boolean mult) {
		if(mult) {
			VideoResolutionCalculator calc = getResolutionCalculator();
			if(calc == null) {
				log.warn("No resolution can be calculated, option mult ignored.");
				return;
			}
			VideoResolution res = calc.getVideoResolution(fileVideo);
			if(width < 0)
				width = res.width / (-width);
			else
				width = res.width * width;

			if(height < 0)
				height = res.height / (-height);
			else
				height = res.height * height;
		}
		params.add("-vf");
		params.add("scale="+width+":"+height);
	}

	@Override
	protected boolean handleExitValue(int exitValue, ErrorState state) {
		if(exitValue == 0 || exitValue == 3) {
			// 0 means no error
			// 3 means assertion failure -> acceptable probably
			return false;
		}
		else {
			state.nextState();
			if(state.shouldGiveUp()) {
				log.error(" Giving up.");
				return false;
			}
			return true;
		}
	}

	@Override
	protected int handleProcess(Process startedProcess, VideoUtil util) throws InterruptedException {
		util.printStream(startedProcess.getInputStream());
		return startedProcess.waitFor();
	}

	@Override
	protected void addArguments(List<String> params, File fileVideo, int w,
			int h, boolean mult, File out, ErrorState state) {
		params.add(fileVideo.getAbsolutePath());
		addScalingArgument(params, fileVideo, w, h, mult);
		state.addHandlingParams(params);
		params.add("-o");
		params.add(out.getAbsolutePath());
		if(!params.contains("-ovc")) {
			params.add("-ovc"); params.add("lavc"); params.add("-of"); params.add("lavf");
		}
		if(!params.contains("-oac")) {
			params.add("-oac"); params.add("copy");
		}
	}

	/* (non-Javadoc)
	 * @see org.haec.app.scaling.AbstractScaling#getDefaultExtension(java.io.File)
	 */
	@Override
	protected String getDefaultExtension(File fileVideo) {
		return "mp4";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.scaling.mencoder.ScalingMencoder";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new ScalingMencoderBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.scaling.mencoder";
	}
}