/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.util.Map;
import java.util.Set;

import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.resource.request.ContractChecker;
import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author Sebastian Götz
 */
public class FindHWvariantAction implements IObjectActionDelegate {
	
	private Request req = null;

	public FindHWvariantAction() {
		
	}

	@Override
	public void run(IAction action) {
		Map<Set<ContractMode>, Boolean> validPathes = ContractChecker.findValidMappings(req);
		
		String msg = "";
		
		for(Set<ContractMode> path : validPathes.keySet()) {
			if(validPathes.get(path)) {
				msg += "valid: "+path+"\n";
			} else {
				//msg += "INVALID: "+path+"\n";
			}
		}
		
		MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Result", msg);
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		req = ResolverUtil.getSelectRequest(selection);
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		
	}

}
