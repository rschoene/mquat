/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.File;
import java.util.Set;

import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.resource.request.ContractChecker;
import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author Sebastian Götz
 */
public class DetermineModesAction implements IObjectActionDelegate {
	
	private Request req = null;

	public DetermineModesAction() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run(IAction action) {
		if(req == null) {
			System.err.println("Request must not be null!");
		} else {
			String eclURI = req.getComponent().getEclUri();
			URI uri = req.eResource().getURI();
			String reqStr = uri.toFileString();
			if(reqStr.indexOf(File.separator) != -1) {
				reqStr = reqStr.substring(0,reqStr.lastIndexOf(File.separator));
			}
			eclURI = reqStr+File.separator+eclURI;
			EclFile resolvedEclFile = ResolverUtil.loadEclFile(eclURI);
			
			Set<ContractMode> found = ContractChecker.searchValidModesForRequest(req, resolvedEclFile);
			String msg = "The following modes match:\n";
			for(ContractMode m : found) {
				msg += "- "+m.getName()+"\n";
			}
			MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Result", msg);
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		req = ResolverUtil.getSelectRequest(selection);
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}

}
