/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.Request;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.haec.theatre.utils.FileUtils;

/**
 * 
 * @author Sebastian Götz
 */
public class MOILPGenerator {

	private Map<String, String> resourceShortCuts;
	private Set<String> binaryVars = new HashSet<String>();
	private Map<String,String> binaryVarMapping = new HashMap<String,String>();
	private URI baseURI;
	
	private Map<String,Map<String,EclFile>> allContracts;

	private StructuralModel swModel, hwModel;
	private VariantModel infra;
	
	private CcmExpressionInterpreter interpreter;
	
	private long genArch = 0, genRes = 0, genNFP = 0, genObj = 0;
	
	private int numConstraints = 0, numVars = 0;
	
	private int TIMEOUT_IN_SECS = 120;
	
	public MOILPGenerator() {
		allContracts = new HashMap<String,Map<String,EclFile>>();
		interpreter = new CcmExpressionInterpreter();
		resourceShortCuts = new HashMap<String, String>();
		binaryVars = new HashSet<String>();
		binaryVarMapping = new HashMap<String, String>();
		numConstraints = 0;
		numVars = 0;
	}

	public String generateMOILPforRequest(Request req,
			Map<String, EclFile> eclFiles, boolean verbose, StructuralModel smodel, StructuralModel hwModel,
			VariantModel infra, int numObj) {

		this.swModel = smodel;
		this.hwModel = hwModel;
		this.infra = infra;
		
		baseURI = req.eResource().getURI();
		baseURI = baseURI.trimSegments(1);
		
		int numRes = 0;
		for (String res : eclFiles.keySet()) {
			numRes++;
			resourceShortCuts.put(res,"R" + numRes);
			if(verbose) {
				System.out.println(res);
			}
		}
		
		allContracts.put(req.getComponent().getName(), eclFiles);

		StringBuilder sb = new StringBuilder();
		
		// architectural constraints
		long start = System.currentTimeMillis();
		sb.append(generateArchitecturalConstraints(req, eclFiles, verbose));
		sb.append("\n");
		long stop = System.currentTimeMillis();
		genArch = stop - start;
		// resource negotiation
		start = System.currentTimeMillis();
		sb.append(generateResourceConstraints(req, verbose));
		sb.append("\n");
		stop = System.currentTimeMillis();
		genRes = stop - start;
		// software NFP negotiation
		start = System.currentTimeMillis();
		sb.append(generateNFPConstraints(req, verbose));
		sb.append("\n");
		stop = System.currentTimeMillis();
		genNFP = stop - start;
		
		sb.append("\nbin ");
		for(String bv : binaryVars) {
			sb.append(bv+", ");
		}
		sb.delete(sb.length()-2, sb.length());
		sb.append(";");
		
		int numVars = binaryVars.size();
		StringBuffer header = new StringBuffer();
		//header.append("* #variable= " + numVars + " #constraint= " + numConstraints +"\n");
		
		// objective function
		start = System.currentTimeMillis();
		for(int i = 0; i < numObj; i++) {
			StringBuffer ret = generateObjectiveFunction(req,eclFiles,verbose);
			header.append(ret+"\n");
		}
		stop = System.currentTimeMillis();
		genObj = stop - start;
		header.append("\n");
		header.append(sb);
		
		String msg = "Generation Summary:\n ";
		msg += "Architectural Constraints: "+genArch+" ms\n";
		msg += "Resource Negotiation     : "+genRes +" ms\n";
		msg += "Software NFP Negotiation : "+genNFP +" ms\n";
		msg += "Objective Function       : "+genObj +" ms\n";
		
		URI resultFile = req.eResource().getURI().trimSegments(1).appendSegment("moilp-gen-result-5.txt");
		OutputStream os;
		try {
			os = URIConverter.INSTANCE.createOutputStream(resultFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.write(msg);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return header.toString();
	}

	private StringBuffer generateObjectiveFunction(Request req,
			Map<String, EclFile> eclFiles, boolean verbose) {
		StringBuffer b = new StringBuffer();
		b.append("min: ");
		for(String bv : binaryVars) {
			b.append(((int)(Math.random()*100)+1)+" "+bv+" + ");
		}
		b.delete(b.length()-3, b.length());
		b.append(";\n");
		return b;
	}

	/**
	 * Generate architectural constraints of ILP.
	 * 
	 * @param req
	 *            the request
	 * @param eclFiles
	 *            Map<String,EclFile> Map of container URIs to contract file of
	 *            directly requested component type
	 * @param verbose
	 *            show debug info or not
	 * @return architectural constraints as String
	 */
	private StringBuilder generateArchitecturalConstraints(Request req,
			Map<String, EclFile> eclFiles, boolean verbose) {

		// at least one impl per required type has to be chosen
		StringBuilder archStr = new StringBuilder();

		// get all impls of this type, use the first container's contract
		String firstContainer = eclFiles.keySet().iterator().next();
		EclFile contractOfFirstComponent = eclFiles.get(firstContainer);

		try {
			archStr.append(generateArchitecturalConstraintForComponentType(contractOfFirstComponent));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return archStr;
	}

	private StringBuilder generateArchitecturalConstraintForComponentType(
			EclFile file) throws IOException {
		StringBuilder ret = new StringBuilder();

		Set<SWComponentType> furtherRequiredTypes = new HashSet<SWComponentType>();
		boolean nonEmptyConstraint = false;
		
		for (EclContract c : file.getContracts()) {
			SWComponentContract sc = (SWComponentContract) c;
			String implName = sc.getName();
			for (SWContractMode m : sc.getModes()) {
				String modeName = m.getName();
				for (SWContractClause cl : m.getClauses()) {
					if (cl instanceof SWComponentRequirementClause) {
						furtherRequiredTypes
								.add(((SWComponentRequirementClause) cl)
										.getRequiredComponentType());
					}
				}
				for (String r : resourceShortCuts.values()) {
					String binaryVar = "b#" + implName + "#" + modeName + "#"
							+ r;
					String s = "x"+(numVars++);
					binaryVarMapping.put(binaryVar,s);
					binaryVars.add(s);
					ret.append(s);
					ret.append(" + ");
					nonEmptyConstraint = true;
				}
			}
		}
		
		int end = ret.length();
		int start = end - 2;
		if(start > 0)
		ret.delete(start, end);
		
		if(furtherRequiredTypes.size() > 0) {
			ret.append(" = 1;\n");
			numConstraints++;
			for(SWComponentType t : furtherRequiredTypes) {
				EclFile f = getContractForComponentType(t);
				Map<String,EclFile> ctrs = new HashMap<String, EclFile>();
				for(String res : resourceShortCuts.keySet()) {
					ctrs.put(res,f);
				}
				allContracts.put(t.getName(),ctrs);
				ret.append(generateArchitecturalConstraintForComponentType(f));
				//TODO handle implication constraint
			}
		} else if(nonEmptyConstraint){
			ret.append(" = 1;\n");
			numConstraints++;
		}

		return ret;
	}

	private EclFile getContractForComponentType(SWComponentType t) throws IOException {
		String uri = t.getEclUri();
		URI fileURI = baseURI.appendSegment(uri);
		String serializedContract = FileUtils.readContractAsText(fileURI);
		
		EclFile ret = readContract(fileURI);
		return ret;
	}
	
	private EclFile readContract(URI fileURI) throws IOException {
		Set<StructuralModel> visibleStructuralModels = new HashSet<StructuralModel>();
		visibleStructuralModels.add(swModel);
		visibleStructuralModels.add(hwModel);
		
		ResourceSet rs = new ResourceSetImpl();
		Resource res = rs.createResource(fileURI);
		res.load(null);
		EObject root = res.getAllContents().next();
		EcoreUtil.resolveAll(root);
		return (EclFile)root;
	}

	private StringBuffer generateResourceConstraints(Request req,
			 boolean verbose) {
		
		StringBuffer ret = new StringBuffer();
		
		//property => binaryVar => val
		Map<String, Map<String, Long>> reqs = new HashMap<String, Map<String,Long>>();
		
		for(String typeName : allContracts.keySet()) {
			Map<String,EclFile> eclFiles = allContracts.get(typeName);
			//collect resource requirements in contracts per server			
			for(String res : eclFiles.keySet()) {
				EclFile f = eclFiles.get(res);
				for(EclContract c : f.getContracts()) {
					String implName = c.getName();
					for(SWContractMode m : ((SWComponentContract)c).getModes()) {
						String modeName = m.getName();
						for(SWContractClause cl : m.getClauses()) {
							if(cl instanceof HWComponentRequirementClause) {
								HWComponentRequirementClause hrc = (HWComponentRequirementClause)cl;
								String hwName = hrc.getRequiredResourceType().getName();
								for(PropertyRequirementClause prc : hrc.getRequiredProperties()) {
									String propertyName = prc.getRequiredProperty().getDeclaredVariable().getName();
									
									propertyName = resourceShortCuts.get(res)+"#"+hwName+"#"+propertyName;
									Statement stmt = prc.getMinValue();
									CcmValue v = interpreter.interpret(stmt);
									long val = ((CcmInteger)v).getIntegerValue();
									String bv = "b#"+implName+"#"+modeName+"#"+resourceShortCuts.get(res);
									bv = binaryVarMapping.get(bv);
									Map<String,Long> r = reqs.get(propertyName);
									if(r == null) r = new HashMap<String, Long>();
									r.put(bv, val);
									reqs.put(propertyName, r);
								}
							}
						}
					}
				}
			}
		}
		
		for(String propertyName : reqs.keySet()) {
			Map<String, Long> r = reqs.get(propertyName);
			for(String bv : r.keySet()) {
				ret.append(r.get(bv)+" "+bv+" + ");
			}
			ret.delete(ret.length()-3, ret.length());
			ret.append(" <= "+getUpperBoundForResource(propertyName)+";\n");
			numConstraints++;
		}
		
		return ret;
	}

	private String getUpperBoundForResource(String propertyName) {
		String[] idx = propertyName.split("#"); //resource#type#property
		org.coolsoftware.coolcomponents.ccm.variant.Resource root = (org.coolsoftware.coolcomponents.ccm.variant.Resource)infra.getRoot();
		for(org.coolsoftware.coolcomponents.ccm.variant.Resource x : root.getSubresources()) {
			if(idx[0].equals(resourceShortCuts.get(x.getName())))
			for(org.coolsoftware.coolcomponents.ccm.variant.Resource y : x.getSubresources()) {
				if(y.getSpecification().getName().equals(idx[1]))
				for(VariantPropertyBinding vpb : y.getPropertyBinding()) {
					if(vpb.getProperty().getDeclaredVariable().getName().equals(idx[2])) {
						CcmInteger val = (CcmInteger)interpreter.interpret(vpb.getValueExpression());
						return val.getIntegerValue()+"";
					}
				}
			}
		}
		return "42"; //if nothing found -> 42
	}

	private StringBuffer generateNFPConstraints(Request req,
			boolean verbose) {
		
		StringBuffer ret = new StringBuffer();
		
		//property => binaryVar => val
		Map<String, Map<String, Long>> reqs = new HashMap<String, Map<String,Long>>();
		Map<String, Map<String, Long>> provs = new HashMap<String, Map<String,Long>>();
		
		for(String typeName : allContracts.keySet()) {
			Map<String,EclFile> eclFiles = allContracts.get(typeName);
			//collect resource requirements in contracts per server			
			for(String res : eclFiles.keySet()) {
				EclFile f = eclFiles.get(res);
				for(EclContract c : f.getContracts()) {
					String implName = c.getName();
					for(SWContractMode m : ((SWComponentContract)c).getModes()) {
						String modeName = m.getName();
						for(SWContractClause cl : m.getClauses()) {
							if(cl instanceof SWComponentRequirementClause) {
								SWComponentRequirementClause src = (SWComponentRequirementClause)cl;
								String swName = src.getRequiredComponentType().getName();
								for(PropertyRequirementClause prc : src.getRequiredProperties()) {
									String propertyName = prc.getRequiredProperty().getDeclaredVariable().getName();
									
									propertyName = swName+"#"+propertyName;
									Statement stmt = prc.getMinValue();
									CcmValue v = interpreter.interpret(stmt);
									long val = ((CcmInteger)v).getIntegerValue();
									String bv = "b#"+implName+"#"+modeName+"#"+resourceShortCuts.get(res);
									bv = binaryVarMapping.get(bv);
									Map<String,Long> r = reqs.get(propertyName);
									if(r == null) r = new HashMap<String, Long>();
									r.put(bv, val);
									reqs.put(propertyName, r);
								}
							} else if(cl instanceof ProvisionClause) {
								ProvisionClause pc = (ProvisionClause)cl;
								String propertyName = pc.getProvidedProperty().getDeclaredVariable().getName();
								propertyName = typeName+"#"+propertyName;
								Statement stmt = pc.getMinValue();
								CcmValue v = interpreter.interpret(stmt);
								long val = ((CcmInteger)v).getIntegerValue();
								String bv = "b#"+implName+"#"+modeName+"#"+resourceShortCuts.get(res);
								bv = binaryVarMapping.get(bv);
								Map<String,Long> r = provs.get(propertyName);
								if(r == null) r = new HashMap<String, Long>();
								r.put(bv, val);
								provs.put(propertyName, r);
							}
						}
					}
				}
			}
		}
		
		for(String propertyName : reqs.keySet()) {
			Map<String, Long> p = provs.get(propertyName);
			if(p != null) {
				for(String bv : p.keySet()) {
					ret.append(p.get(bv)+" "+bv+" + ");
				}
				ret.delete(ret.length()-3, ret.length());
			} else {
				ret.append("0");
			}
			ret.append(" >= ");
			Map<String, Long> r = reqs.get(propertyName);
			for(String bv : r.keySet()) {
				ret.append(r.get(bv)+" "+bv+" + ");
			}
			ret.delete(ret.length()-3, ret.length());
			ret.append(";\n");
			numConstraints++;
		}
		
		return ret;
	}

	public Map<String, String> runMOILP(Request req,
			Map<String, EclFile> eclFiles, String moilp) {
		File reqFile = new File("tmp");
		return runMOILP(req, eclFiles, reqFile, moilp);
	}

	public Map<String, String> runMOILP(Request req, Map<String,EclFile> lemContracts,
			File reqFile, String moilp) {
		Map<String, String> ret = new HashMap<String, String>();
		File ilpFile = new File(reqFile.getAbsolutePath() + ".moilp");
		System.out.println("moilpFile: " + ilpFile.getAbsolutePath());
		try {
			if (!ilpFile.exists())
				if (!ilpFile.createNewFile())
					System.out.println("cannot create moilp file");

			/* only for debugging */
			FileWriter fw = new FileWriter(ilpFile, false);
			fw.write(moilp);
			fw.close();
			System.out.println("written MOILP into file: "
					+ ilpFile.getAbsolutePath() + File.separator
					+ ilpFile.getName());

			runMOILP(ret, ilpFile, req, moilp);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	private void runMOILP(Map<String, String> ret, File ilpFile, Request req, final String moilp) {
		try {
			long start = System.nanoTime();
			boolean timedout = false;
			MOILP_KH82 task = new MOILP_KH82(moilp, false);
			task.start();
			for(int i = 0; i < TIMEOUT_IN_SECS*100; i++) {
				if(task.isAlive()) {
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else break;
			}
			if(task.isAlive()) {
				task.terminate();
				task.interrupt();
				timedout = true;
			}
			boolean oom = task.isOutofmemory();
			int numSol = task.getNumSolutions();
			
			System.out.println("done");
			
			String msg = "Optimal Mapping:\n\n";
		
			long duration = System.nanoTime() - start;
			System.out.println("solving took: " + task.getSolverTime()
					+ " ms");

//			if(sol.contains("0-1 Variables fixed to 1 :")) {
//				String[] selects = sol.substring(
//						sol.indexOf("0-1 Variables fixed to 1 :")
//								+ "0-1 Variables fixed to 1 :".length() + 1).split(
//						" ");
//				for (String x : selects) {
//					if (x.length() == 0 || x.equals(" "))
//						break;
//					String mappingVar = getKeyForValue(x);
//					if (mappingVar == null)
//						break;
//					String[] parts = mappingVar.split("#");
//					String impl = parts[1];
//					String mode = parts[2];
//					String server = parts[3];
//					for(String key : resourceShortCuts.keySet()) {
//						if(resourceShortCuts.get(key).equals(server)) {
//							server = key;
//							break;
//						}
//					}
//					msg += impl + "(" + mode + ") => " + server + "\n";
//					ret.put(impl, server);
//				}
//			}
			
			msg += "\n\nSolver Time: "+task.getSolverTime()+" ms\n";
			msg += "\nSolved ILPs: "+task.getSolvedILPs()+"\n";
			msg += "Timed out: "+timedout+"\n";
			msg += "OutOfMemory: "+(oom?"1":"0")+"\n";
			msg += "Num Solutions: "+numSol+"\n";
			msg += "Cols: "+numVars+"\n";//vars
			msg += "Rows: "+numConstraints+"\n";//constraints

			System.out.println(msg);
			
			URI resultFile = req.eResource().getURI().trimSegments(1).appendSegment("moilp-result-5.txt");
			OutputStream os = URIConverter.INSTANCE.createOutputStream(resultFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.write(msg);
			bw.flush();
			bw.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} 
	}

	private String getKeyForValue(String x) {
		for (String key : binaryVarMapping.keySet()) {
			if (binaryVarMapping.get(key).equals(x))
				return key;
		}
		return null;
	}
}
