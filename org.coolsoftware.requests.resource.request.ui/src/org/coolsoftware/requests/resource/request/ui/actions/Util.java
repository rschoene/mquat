/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.File;

import org.coolsoftware.requests.Request;
import org.eclipse.emf.common.util.URI;

/**
 * 
 * @author Sebastian Götz
 */
public class Util {

	public static String getEclUriFromRequest(Request req) {
		String eclURI = req.getComponent().getEclUri();
		URI uri = req.getComponent().eResource().getURI();
		uri = uri.trimSegments(1);
		/*String reqStr = uri.toString();
		if(reqStr.indexOf(File.separator) != -1) {
			reqStr = reqStr.substring(0,reqStr.lastIndexOf(File.separator));
		}
		*/
		eclURI = uri.appendSegments(eclURI.split("/")).toString();
		return eclURI;
	}

}
