/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.gnu.glpk.glp_prob;
import org.haec.optimizer.ilp.GLPK_ILPGenerator;
import org.haec.optimizer.ilp.ILPGenerator;
import org.haec.test.utils.GEMAdapter;
import org.haec.test.utils.GRMAdapter;
import org.haec.test.utils.TheatreSettingsAdapter;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.Function;

/**
 * Testing Generation and Solving of different ILP-Generators.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class GenerateILPAction implements IObjectActionDelegate {

	private Request req = null;
	private File reqFile = null;
	private boolean single = true;
	private File rootDir;
	
	private final static String toy = "toy";
	private final static String lp = "lp";
	private final static String glpk = "glpk";

	public GenerateILPAction() {

	}

	@Override
	public void run(IAction action) {
		if (req == null && single == true) {
			System.err.println("Request must not be null!");
		} else {
			List<URI> requests = new ArrayList<URI>();
			if (!single) {
				// fetch each req
				List<File> todos = new LinkedList<File>();
				todos.add(rootDir);
				while(!todos.isEmpty()) {
					File dir = todos.remove(0);
					System.out.println("Searching " + dir);
					for(File f : dir.listFiles()) {
						if(f.isDirectory()) {
							todos.add(f);
						}
						else if(f.getName().endsWith(".request")) {
							requests.add(URI.createFileURI(f.getAbsolutePath()));
						}
					}
				}
				System.out.println("Got " + requests.size() + " requests to process.");
			}
			Collections.sort(requests, new Comparator<URI>() {

				@Override
				public int compare(URI arg0, URI arg1) {
					return arg0.path().compareTo(arg1.path());
				}
			});
			Display display = Display.getCurrent();
			if(display == null) { display = Display.getDefault(); }
			Shell shell = new Shell(display);
			ILPDialog dialog = new ILPDialog(shell, new String[]{toy, glpk, lp}, requests);
			int kind = dialog.open();
			System.out.println(kind);
			if(kind == Window.CANCEL) {
				System.out.println("User Abort.");
				return;
			}

			String solver = dialog.getSolver().toLowerCase();
			if(single) {
				work(kind, false, solver);
			} else {
				shell = new Shell(display);
				shell.setText("Progress");
				Composite c = new Composite(shell, SWT.NULL);
				c.setLayout(new GridLayout());
				try {
					final Label currentReq = new Label(c, SWT.NULL);
					currentReq.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
					currentReq.setText("Preparing ...");
					final ProgressBar pb = new ProgressBar(c, SWT.HORIZONTAL);
					pb.setMinimum(0);
					pb.setMaximum(requests.size());
					pb.setSelection(0);
					pb.setBounds(10, 10, 200, 20);
					c.setSize(500, 50);
					c.pack();
					shell.pack();
					shell.open();

					boolean first = true;
					for (int i = 0; i < requests.size(); i++) {
						URI target = requests.get(i);
						if(!dialog.getCheckedURIs().get(i)) {
							System.out.println("Skipping " + target);
							pb.setSelection(pb.getSelection() + 1);
							continue;
						}
						System.out.println("Processing " + target);
						ResourceSet rs = new ResourceSetImpl();
						org.eclipse.emf.ecore.resource.Resource res = rs
								.getResource(target, true);
						req = (Request) res.getContents().iterator()
								.next();
						currentReq.setText("Current: " + req.eResource().getURI().path());
						if(first) {
							work(kind, true, solver);
							first = false;
						}
						work(kind, false, solver);
						System.gc();
						pb.setSelection(pb.getSelection() + 1);
						System.out.println("next");
					}
				}
				finally {
					shell.dispose();
				}
			}
		}
	}

	public class ILPDialog extends TitleAreaDialog {
		// IDs for ILPDialog buttons
		// We use large integers because we don't want
		// to conflict with system constants
		public static final int GENERATE = 9999;
		public static final int SOLVE = 9998;
		public static final int BOTH = 9997;

		private org.eclipse.swt.widgets.List list;
		private Table table;
		private String[] solvers;
		private String solver;
		private List<URI> requests;
		private List<Boolean> checkedURIs;
		private Function<TableItem, Boolean> getChecked;
		private PreferenceStore preferenceStore;

		/**
		 * Constructor for MailDialog.
		 * 
		 * @param shell -
		 *            Containing shell
		 * @param solvers -
		 *            ILP solvers passed to the dialog
		 * @param requests 
		 */
		public ILPDialog(Shell shell, String[] solvers, List<URI> requests) {
			super(shell);
			this.solvers = solvers;
			this.requests = requests;
			preferenceStore = new PreferenceStore("mquat_ilp_action.properties");
			try { preferenceStore.load(); }
			catch (IOException e) { e.printStackTrace(); }
			preferenceStore.setDefault("solver", glpk);
		}

		/**
		 * @see org.eclipse.jface.window.Window#create() We complete the dialog with
		 *      a title and a message
		 */
		public void create() {
			super.create();
			setTitle("Choose ILP solver and action");
			setMessage("First choose ILP solver, next choose action to execute with it.");
		}

		@Override
		public int open() {
			int result = super.open();
			try { preferenceStore.save(); }
			catch (IOException e) { e.printStackTrace(); }
			return result;
		}
		
		/**
		 * @see org.eclipse.jface.dialogs.Dialog#
		 *      createDialogArea(org.eclipse.swt.widgets.Composite) Here we fill the
		 *      center area of the dialog
		 */
		protected Control createDialogArea(Composite parent) {
			final Composite area = new Composite(parent, SWT.NULL);
			area.setLayout(new RowLayout());
			list = new org.eclipse.swt.widgets.List(area, SWT.BORDER | SWT.MULTI);
			list.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					validate();
					for(String s : list.getSelection()) {
						System.out.println(s);
					}
					preferenceStore.setValue("solver", list.getSelection()[0]);
				}
			});
			for (int i = 0; i < solvers.length; i++) {
				list.add(solvers[i]);
			}
			String lastSolver = preferenceStore.getString("solver");
			if(lastSolver != null) {
				list.setSelection(new String[]{lastSolver});
			}
			table = new Table(area, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
			table.setLayoutData(new RowData(-1, 200));
			for (URI req : requests) {
				preferenceStore.setDefault(preferenceName(req.path()), true);
				TableItem item = new TableItem(table, SWT.NONE);
				item.setChecked(preferenceStore.getBoolean(preferenceName(req.path())));
				item.setText(req.path());
			}
			getChecked = new Function<TableItem, Boolean>() {
				public Boolean apply(TableItem s) {
					preferenceStore.setValue(preferenceName(s.getText(0)), s.getChecked());
					return s.getChecked();
				}
			};
			table.addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event event) {
					String string = event.detail == SWT.CHECK ? "Checked"
							: "Selected";
					System.out.println(event.item + " : " + string);
					validate();
				}
			});
			table.setSize(100, 100);
			table.pack();
			updateCheckedRequests();

			return area;
		}
		
		private String preferenceName(String name) {
			return "req"+name;
		}

		private void updateCheckedRequests() {
			checkedURIs = CollectionsUtil.map(table.getItems(), getChecked);
		}

		protected void validate() {
			updateCheckedRequests();
			boolean atLeastOneChecked = false;
			for(boolean checked : checkedURIs) {
				if(checked) { atLeastOneChecked = true; break; }
			}
			boolean correct = list.getSelectionCount() >= 1 && atLeastOneChecked;
			getButton(GENERATE).setEnabled(correct);
			getButton(SOLVE).setEnabled(correct);
			getButton(BOTH).setEnabled(correct);
			setErrorMessage(correct ? null : "Please select at least one solver and at least one request!");
		}

		protected void createButtonsForButtonBar(Composite parent) {
			class SelectionAdapter2 extends SelectionAdapter {
				int return_code;
				public SelectionAdapter2(int return_code) {
					this.return_code = return_code;
				}
				@Override
				public void widgetSelected(SelectionEvent e) {
					solver = list.getSelection()[0];
					setReturnCode(return_code);
					close();
				}
			}
			// Create Generate button
			Button generateButton = createButton(parent, GENERATE, "Generate", false);
			generateButton.addSelectionListener(new SelectionAdapter2(GENERATE));

			// Create Solve button
			Button solveButton = createButton(parent, SOLVE, "Solve", false);
			solveButton.addSelectionListener(new SelectionAdapter2(SOLVE));

			// Create Both button
			Button bothButton = createButton(parent, BOTH, "Generate + Solve", true);
			bothButton.addSelectionListener(new SelectionAdapter2(BOTH));

			// Create Cancel button
			Button cancelButton = createButton(parent, IDialogConstants.CANCEL_ID, "Cancel", false);
			cancelButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					setReturnCode(CANCEL);
					close();
				}
			});
			validate();
		}

		public String getSolver() {
			return solver;
		}
		
		public List<Boolean> getCheckedURIs() {
			return checkedURIs;
		}
	}
	
	/**
	 * @param kind 0: only generate, 1: only solve, 2: do both
	 * @param dryrun true: save times in csv, false: just run and don't save times in csv
	 * @param solver solver to use, one of (toy, glpk, lp)
	 */
	private void work(int kind, boolean dryrun, final String solver) {
		if(dryrun) {
			System.out.println("::: Dry-Run :::");
		}
		String ilp = null;
		GLPK_ILPGenerator glpk_gen = null;
		glp_prob glpk_ilp = null; IGlobalEnergyManager gem = null; final String appName = "name";
		final StructuralModel smodel = req.getImport().getModel();
		Map<String, EclFile> eclFiles = new HashMap<String, EclFile>();
		
		String eclURI = Util.getEclUriFromRequest(req);
		EclFile resolvedEclFile = ResolverUtil.loadEclFile(eclURI);

		final Resource root = (Resource) req.getHardware().getHwmodel().getRoot();
		for (Resource server : root.getSubresources()) {
			eclFiles.put(server.getName(), resolvedEclFile);
		}
		ILPGenerator2 toy_gen = null;
		
		long total_start = System.currentTimeMillis();
		IGlobalResourceManager grm = new GRMAdapter() {
			String hw_sm_ser = null;
			
			@Override
			public String getInfrastructureTypes() {
				if(hw_sm_ser == null) {
					try { hw_sm_ser = CCMUtil.serializeEObject(req.getImport().getModel());
					} catch (IOException e) { e.printStackTrace(); }
				}
				return hw_sm_ser;
			}
		};
		// for hw-struct: rely on fallback of glpk-ilp-generator
		gem = new GEMAdapter(grm) {
			URI baseURI;
			Map<String, Map<String, String>> contractsForApp = null;
			String smodel_ser;

			{
				baseURI = req.eResource().getURI();
				baseURI = baseURI.trimSegments(1);

			}
			public String getApplicationModel(String appName) {
				if(smodel_ser == null) {
					try { smodel_ser = CCMUtil.serializeEObject(smodel);
					} catch (IOException e) { e.printStackTrace(); }
					System.out.println("Model-Size:" + smodel_ser.length());
				}
				return smodel_ser;
			};
			@Override
			public Map<String, VariantModel> getCurrentSystemConfigurations() {
				Map<String, VariantModel> result = new HashMap<String, VariantModel>();
				result.put(appName, null);
				return result;
			}
			@Override
			public Map<String, Map<String, String>> getContractsForApp(
					String appName, Map<String, List<String>> names) {
				ensureContractsForApp();
				return contractsForApp;
			}
			@Override
			public Map<String, String> getContractsForComponent(
					String appName, String compName) {
				ensureContractsForApp();
				return contractsForApp.get(compName);
			}
			private void ensureContractsForApp() {
				if(contractsForApp == null) {
					contractsForApp = new HashMap<String, Map<String,String>>();
					// result: compName -> containerUri -> ECLFile
					for(SWComponentType swType : iterswtypes(smodel)) {
						String compName = swType.getName();
						String contract;
						try {
							EclFile eclfile = getContractForComponentType(swType);
							if(eclfile == null) {
								System.err.println("No eclfile found for " + compName);
								continue;
							}
							contract = CCMUtil.serializeEObject(eclfile);
						} catch (IOException e) {
							e.printStackTrace();
							continue;
						}
						Map<String, String> subMap = new HashMap<String, String>();
						for (Resource server : root.getSubresources()) {
							subMap.put(server.getName(), contract);
						}
						contractsForApp.put(compName, subMap);
					}
				}
			}
			private EclFile getContractForComponentType(SWComponentType t) throws IOException {
				String uri = t.getEclUri();
				if(uri == null) {
					return null;
				}
				URI fileURI = baseURI.appendSegment(uri);
//					String serializedContract = FileUtils.readContractAsText(fileURI);
				
				EclFile ret = readContract(fileURI);
				return ret;
			}
			
			private EclFile readContract(URI fileURI) throws IOException {
				Set<StructuralModel> visibleStructuralModels = new HashSet<StructuralModel>();
				visibleStructuralModels.add(smodel);
				visibleStructuralModels.add(req.getImport().getModel());
				
				ResourceSet rs = new ResourceSetImpl();
				org.eclipse.emf.ecore.resource.Resource res = rs.createResource(fileURI);
				res.load(null);
				EObject root = res.getAllContents().next();
				EcoreUtil.resolveAll(root);
				return (EclFile)root;
			}

			private Iterable<SWComponentType> iterswtypes(StructuralModel smodel) {
				List<SWComponentType> result = new ArrayList<SWComponentType>();
				List<SWComponentType> todo = new ArrayList<SWComponentType>();
				SWComponentType root = (SWComponentType) smodel.getRoot();
				todo.add(root);
				while(!todo.isEmpty()) {
					SWComponentType current = todo.remove(0);
					result.add(current);
					todo.addAll(current.getSubtypes());
				}
				return result;
			}
		};
		switch(solver) {
		case glpk:
			TheatreSettings settings = new TheatreSettingsAdapter() {{
				getILPfeatureSequentialConstraint().set(true);
				getILPfeatureZeroIsUnknown().set(true);
				getILPobjectiveSimpleWeight().set(true);
				getILPsetupInitTimeout().set(240);
				getILPsolverExact().set(true);
			}};
			glpk_gen = new GLPK_ILPGenerator() {{
				profile = PROFILE_NORMAL;
//				problemName = reqUri.segment(reqUri.segmentCount() - 2);
				timeout = 40;
			}};
			glpk_gen.setTheatreSettings(settings);
			URI reqUri = req.eResource().getURI();
			glpk_gen.problemName = reqUri.segment(reqUri.segmentCount() - 2);
			break;
		case toy:
			toy_gen = new ILPGenerator2();
			break;
		}

		if(shouldGenerate(kind)) {
			System.out.println("** Generate ILP **");
	
			long start = System.currentTimeMillis();
			switch(solver) {
			case glpk:
				try {
					glpk_ilp = glpk_gen.generateILPforRequest(req, eclFiles, false, gem, appName,
							Collections.<String,List<String>>emptyMap());
				} catch (TimeoutException e1) {
					e1.printStackTrace();
				}
				break;
			case toy:
				ilp = toy_gen.generateILPforRequest(req, eclFiles, false, smodel, req
						.getImport().getModel(), req.getHardware().getHwmodel(), dryrun);
				break;
			case lp:
				ILPGenerator.reset();
				ilp = ILPGenerator.generateILPforRequest(req, eclFiles, false, gem, appName);
				break;
			default:
				throw new RuntimeException("Unknown solver " + solver);
			}
			long stop = System.currentTimeMillis();
			System.out.println("Generation-time: " + (stop - start) + "ms.");
			
			if(!solver.equals(glpk)) {
				URI ilpRes = req.eResource().getURI().trimSegments(1)
						.appendSegment("generated-3.lp");
				try {
					OutputStream os = URIConverter.INSTANCE.createOutputStream(ilpRes);
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
					bw.write(ilp.toString());
					bw.flush();
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		else {
			URI ilpRes = req.eResource().getURI().trimSegments(1).appendSegment("generated-3.lp");
			String fileName = ilpRes.toFileString();
			try {
				ilp = FileUtils.readAsString(fileName, true);
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}

		if(shouldSolve(kind)) {
			System.out.println("** Solve ILP **");
	
			Map<String, String> result;
			switch(solver) {
			case glpk:
				result = glpk_gen.runILP(req, eclFiles, glpk_ilp);
				break;
			case toy:
				result = toy_gen.runILP(req, eclFiles, ilp, dryrun);
				break;
			case lp:
				result = ILPGenerator.runILP(req, eclFiles, ilp);
				break;
			default:
				throw new RuntimeException("Unknown solver " + solver);
			}
	
			System.out.println(result);
		}
		
		long total_stop = System.currentTimeMillis();
		System.out.println("Total-time: " + (total_stop - total_start) + "ms.");
	}

	private boolean shouldGenerate(int kind) {
		return kind != ILPDialog.SOLVE; // only generate and both left
	}

	private boolean shouldSolve(int kind) {
		return kind != ILPDialog.GENERATE; // only solve and both left
	}

	// public RemoteOSGiService getRemoteOSGiService() {
	// BundleContext context = FrameworkUtil.getBundle(this.getClass())
	// .getBundleContext();
	// ServiceReference remoteRef = context
	// .getServiceReference(RemoteOSGiService.class.getName());
	// if (remoteRef == null) {
	// System.err.println("Error: R-OSGi not found!");
	// }
	// RemoteOSGiService remote = (RemoteOSGiService) context
	// .getService(remoteRef);
	// return remote;
	// }

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof TreeSelection) {
			TreeSelection ts = (TreeSelection) selection;
			if (ts.getFirstElement() instanceof org.eclipse.core.internal.resources.File) {
				req = ResolverUtil.getSelectRequest(selection);
				reqFile = ResolverUtil.getSelectedRequestFile(selection);
				single = true;
			} else {
				rootDir = ResolverUtil.getSelectedFolder(selection);
				single = false;
			}
		}
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {

	}

}
