/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

/**
 * 
 * @author Sebastian Götz
 */
public class ILPSolver implements Callable<Map<String,Integer>>{

	private String ilp;
	private boolean verbose;
	
	public ILPSolver(String ilp, boolean verbose) {
		this.ilp = ilp;
		this.verbose = verbose;
	}
	
	private Map<String,Integer> runILP(String ilp, boolean verbose) {
		Map<String,Integer> ret = new TreeMap<String, Integer>();
		File lpFile = new File("temp.lp");
		if(verbose) System.out.println("lpFile: " + lpFile.getAbsolutePath());
		try {
			if (!lpFile.exists())
				if (!lpFile.createNewFile())
					System.out.println("cannot create lp file");

			FileWriter fw = new FileWriter(lpFile, false);
			fw.write(ilp);
			fw.close();
						
			if(verbose)
			System.out.println("written ilp into file: "
					+ lpFile.getAbsolutePath() + "/" + lpFile.getName());

			try {
				String jlp = System.getProperty("java.library.path"); // Windows
				String workspaceLocation = "";

				if(verbose)
				System.out.println("JLP: "
						+ System.getProperty("java.library.path"));
				LpSolve.lpSolveVersion();

				long start = System.nanoTime();
				LpSolve solver = LpSolve.readLp(lpFile.getAbsolutePath(), 1,
						lpFile.getName());
				solver.setBbRule(LpSolve.NODE_PSEUDONONINTSELECT
						+ LpSolve.NODE_RESTARTMODE + LpSolve.NODE_DYNAMICMODE
						+ LpSolve.NODE_RCOSTFIXING);
				solver.setTimeout(120);
				solver.solve();
				long duration = System.nanoTime() - start;
				if(verbose)
				System.out.println("\n====took: " + duration / 1000 / 1000
						+ " ms====");

				Set<String> optimalQualityPath = new HashSet<String>();
				if(verbose)
				System.out.println("Value of objective function: "
						+ solver.getObjective());
				double[] var = solver.getPtrVariables();
				for (int i = 0; i < var.length; i++) {
					if (solver.getColName(i + 1) != null) {
							//&& solver.getColName(i + 1).startsWith("b#")) {
						//if (var[i] == 1) {
						if(verbose)
							System.out.println("Value of var[" + i + "] ("
									+ solver.getColName(i + 1) + ") = "
									+ var[i]);
							ret.put(solver.getColName(i+1),(int)var[i]);
							//optimalQualityPath.add(solver.getColName(i + 1));
						//}
					}
				}

				int cols = solver.getNcolumns();
				int rows = solver.getNrows();
				double obj = solver.getObjective();
				// to free memory
				solver.deleteLp();

				String msg = "Optimal Mapping:\n\n";
				for (String str : optimalQualityPath) {
					String[] parts = str.split("#");
					String impl = parts[1];
					String mode = parts[2];
					String server = parts[3];
					
					msg += impl + "(" + mode + ") => " + server + "\n";
					//ret.put(impl.replaceAll("_", "."), server);
				}
				msg += "\n\nSolver Time: "+(duration/1000000)+" ms\n";
				msg += "Cols: "+cols+"\n";//vars
				msg += "Rows: "+rows+"\n";//constraints
				msg += "Obj: "+obj+"\n";

				if(verbose)
				System.out.println(msg);			
				
				return ret;
			} catch (LpSolveException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Map<String, Integer> call() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
