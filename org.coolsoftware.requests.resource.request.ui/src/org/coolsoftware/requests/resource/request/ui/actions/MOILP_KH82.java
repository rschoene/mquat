/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

/**
 * <p>
 * Implementation of Multi-objective Integer Linear Programming approach as
 * described by Klein and Hannan 1982.
 * </p>
 * 
 * <p>
 * <i>D. Klein and E. Hannan. An algorithm for the multiple objective integer
 * linear programming problem. In European Journal of Operational Research 9, p.
 * 378-385, North-Holland Publishing Company, 1982.</i>
 * </p>
 * 
 * Using lp_solve to solve the single objective ILPs.
 * 
 * To avoid complex parsing the variable names are restricted to x<sub>i</sub>
 * where i is any number (e.g., x24).
 * 
 * @author Sebastian Götz
 * @date 29.01.2013
 */
public class MOILP_KH82 extends Thread {
	private static final int f = 1;

	private String moilp;
	private boolean verbose;
	private volatile boolean terminate = false;
	private boolean outofmemory = false;
	private int numSolutions = 0;
	private long solverTime = 0;

	public int solvedILPs = 0;
	
	private List<Map<String, Double>> objectives;
	private List<Map<String, Double>> allObjectives;

	public static void main(String[] args) {
		String testILP = "";

		// Example from Klein & Hannan '82
		// testILP += "min: 10x1 + 15x2 + 10x3 +5x4;\n";
		// testILP += "min:  5x1 -  8x2 -  5x3 + x4;\n";
		// testILP += "min: - x1 +  2x2 +   x3 -4x4;\n";
		// testILP += "min: -2x1 +  3x2 -  4x3 -2x4;\n";
		// testILP += "min: 12x1 -  3x2 +  4x3 -5x4;\n";
		//
		// testILP += "2x1+3x2+5x3+x4 >= 4;\n";
		// testILP += "4x1+2x2+x3+x4 >= 4;\n";
		//
		// testILP += "bin x1,x2,x3,x4;\n";

		// Example from Deckro/Winkofsky '83
		// testILP += "min:  2x1 +  6x2 -  2x3 + 3x4 + 2x5;\n";
		// testILP += "min:  3x1 +  5x2 +  2x3 + 2x4 + 3x5;\n";
		// testILP += "min:  2x1 +  3x2 +  6x3 - 1x4 + 1x5;\n";
		//
		// testILP += "8x1 + 4x2 + 1x3 + 1x4 + 5x5 >= 4;\n";
		// testILP += "6x1 + 3x2 + 2x3 + 1x5 >= 3;\n";
		// testILP += "2x1 + 9x2 + 3x3 + 2x4 + 3x5 >= 4;\n";
		//
		// testILP += "bin x1,x2,x3,x4,x5;\n";

		//read from file
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("C:\\dev\\workspaces\\runtime-CoolSoftware-Workbench.product\\GeneratedExamples\\ex\\gen_14x5-1x1\\generated.moilp")));
			String l = "";
			StringBuilder content = new StringBuilder();
			while((l = br.readLine()) != null) {
				content.append(l+"\n");
			}
			testILP = content.toString();
			//System.out.println(testILP);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}

		MOILP_KH82 solver = new MOILP_KH82(testILP, false);

		StringBuilder sb = new StringBuilder();
		sb.append("n;o1;o2\n");
		try {
			Set<Map<String, Integer>> pareto = solver.runMOILP(testILP,false);
			int n = 0;
			for(Map<String,Integer> sol : pareto) {
				n++;
				System.out.println("Solution #"+n+": "+sol);
				int o = 0;
				sb.append(n);
				for(Map<String,Double> obj : solver.allObjectives) {
					o++;
					double objValue = 0;
					for(String var : sol.keySet()) {
						objValue += sol.get(var) * obj.get(var);
					}
					sb.append(";"+objValue);
				}
				sb.append("\n");
			}
			System.out.println(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		//System.out.println("Number of solved ILPs: " + solver.solvedILPs);
	}

	public MOILP_KH82(String moilp, boolean verbose) {
		this.moilp = moilp;
		this.verbose = verbose;
		numSolutions = 0;
		outofmemory = false;
	}

	public Set<Map<String, Integer>> runMOILP(String moilp, boolean verbose)
			throws InterruptedException {
		solvedILPs = 0;
		// identify and collect objective functions + remove all objectives
		// after the first
		objectives = new ArrayList<Map<String, Double>>();
		allObjectives = new ArrayList<Map<String, Double>>();
		int oidx = 0;
		String soilp = "";
		for (String clause : moilp.split("\n")) {
			if (clause.startsWith("min: ")) {
				if (oidx == 0) {
					soilp += clause + "\n";
				} // else ommit
				String fi = clause.substring("min: ".length(),
						clause.length() - 1);
				fi = fi.replaceAll("-", "+-");
				fi = fi.replaceAll(" ", "");
				if (fi.startsWith("+"))
					fi = fi.substring(1);
				if (verbose)
					System.out.println(fi);
				Map<String, Double> weights = new TreeMap<String, Double>();
				for (String entry : fi.split("\\+")) {
					String[] weightAndVar = entry.split("x");
					// ugly case -x1
					double w = 0;
					if (weightAndVar[0].equals("-")) {
						w = -1;
					} else if (weightAndVar[0].equals("")) {
						w = 1;
					} else {
						w = Double.parseDouble(weightAndVar[0]);
					}
					weights.put("x" + weightAndVar[1], w);
				}
				if (verbose)
					System.out.println(weights);
				if (oidx > 0)
					objectives.add(weights);
				allObjectives.add(weights);
				oidx++;
			} else {
				soilp += clause + "\n";
			}
			// NOTE: objective weights of 0 are not collected!
		}
		if (verbose)
			System.out.println(soilp);

		// first step according to KH82 is to solve the single-objective ILP for
		// a random objective function
		Map<String, Integer> solution = runILP(soilp, verbose);

		Set<Map<String, Integer>> allSolutions = new HashSet<Map<String, Integer>>();
		allSolutions.add(solution);

		System.out.println("Solutions: " + allSolutions);
		// second step: add constraints to the single-objective ILP
		// for each found solution (i) and other objective function (k) add
		// c^k*x <= c^k*y^i -1
		// "and" connect per solution, "or" connect by objective function
		// "or" connection realized as separate SOILPs!

		long start = System.currentTimeMillis();
		long now = 0;
		int addSolutions = 0;
		do {
			int oldsolcount = allSolutions.size();
			allSolutions = process(allSolutions, objectives, soilp, verbose);
			addSolutions = allSolutions.size() - oldsolcount;
			System.out.println("new solutions: " + addSolutions);
			System.out.println("Solutions: " + allSolutions);
			now = System.currentTimeMillis();
		} while (addSolutions > 0 && (now - start) < (60000));
		long stop = System.currentTimeMillis();
		numSolutions = allSolutions.size();
		solverTime = stop - start;
		System.out.println("took " + (stop - start) + " ms");
		
		return allSolutions;
	}

	private Set<Map<String, Integer>> process(
			Set<Map<String, Integer>> allSolutions,
			List<Map<String, Double>> objectives, String soilp, boolean verbose)
			throws InterruptedException {
		Set<Map<String, Integer>> retSolutions = new HashSet<Map<String, Integer>>();
		retSolutions.addAll(allSolutions);

		String nextSOILP = soilp;
		int andIdx = 0;
		for (Map<String, Integer> sol : allSolutions) { // and connection

			for (Map<String, Double> obj : objectives) { // or connection
			// nextSOILP = addNewConstraint(nextSOILP, "//OR\n");
				// compute c^k*y^i - 1
				int sum = 0;
				for (String v : sol.keySet()) {
					sum += obj.get(v) * sol.get(v);
				}
				sum = sum - f;

				// generate c^k*x <= c^k*y^i -1 constraint
				String newCstr = "";
				for (String v : obj.keySet()) {
					newCstr += obj.get(v) + v + "+";
				}
				newCstr = newCstr.substring(0, newCstr.length() - 1) + " ";
				newCstr = newCstr.replaceAll("\\+-", "-");
				newCstr += "<= " + sum + "; //OR" + andIdx + "\n";

				nextSOILP = addNewConstraint(nextSOILP, newCstr);
			}

			andIdx++;
		}

		if (verbose)
			System.out.println("\nExtended ILP:\n" + nextSOILP);

		int orCount = objectives.size();

		Set<Map<String, Integer>> newSolutions = runExILP(nextSOILP, verbose,
				andIdx, orCount);
		retSolutions.addAll(newSolutions);

		return retSolutions;
	}

	/**
	 * Process extended ILPs, which include ORx comments.
	 * 
	 * Select all combinations for each set of ORx (x=1,..,n).
	 * **/
	private Set<Map<String, Integer>> runExILP(String exILP, boolean verbose,
			int blocks, int blockSize) throws InterruptedException {
		Set<Map<String, Integer>> ret = new HashSet<Map<String, Integer>>();

		String[][] conditionalClauses = new String[blocks][blockSize]; // reference
																		// e.g.
																		// [1][2]
																		// -
																		// first
																		// OR
																		// block,
																		// second
																		// clause

		String plainILP = "";
		int clauseIdx = 0;
		int lastBlockIdx = 0;
		for (String clause : exILP.split("\n")) {
			if (clause.contains("//OR")) {
				int blockIdx = Integer.parseInt(clause.substring(clause
						.indexOf("//OR") + 4));
				if (lastBlockIdx != blockIdx) {
					clauseIdx = 0;
					lastBlockIdx = blockIdx;
				}
				conditionalClauses[blockIdx][clauseIdx++] = clause + "\n";
			} else {
				plainILP += clause + "\n";
			}
		}

		Set<String> allCombos = combos(conditionalClauses, "", 0);

		for (String combo : allCombos) {
			String ilpVariant = addNewConstraint(plainILP, combo);
			if (verbose)
				System.out.println(ilpVariant);
			ret.add(runILP(ilpVariant, verbose));
		}

		return ret;
	}

	/** Add constraint to single-objective ILP <i>before</i> bin constraint! **/
	private String addNewConstraint(String soilp, String newCstr) {
		StringBuffer ret = new StringBuffer();
		for (String clause : soilp.split("\n")) {
			if (!clause.startsWith("bin ")) {
				ret.append(clause + "\n");
			} else {
				ret.append(newCstr);
				ret.append(clause + "\n");
			}
		}
		return ret.toString();
	}

	private Map<String, Integer> runILP(String ilp, boolean verbose)
			throws InterruptedException {
		if (terminate)
			throw new InterruptedException();
		solvedILPs++;
		Map<String, Integer> ret = new TreeMap<String, Integer>();
		File lpFile = new File("temp.lp");
		if (verbose)
			System.out.println("lpFile: " + lpFile.getAbsolutePath());
		try {
			if (!lpFile.exists())
				if (!lpFile.createNewFile())
					System.out.println("cannot create lp file");

			FileWriter fw = new FileWriter(lpFile, false);
			fw.write(ilp);
			fw.close();

			if (verbose)
				System.out.println("written ilp into file: "
						+ lpFile.getAbsolutePath() + "/" + lpFile.getName());

			try {
				String jlp = System.getProperty("java.library.path"); // Windows
				String workspaceLocation = "";

				if (verbose)
					System.out.println("JLP: "
							+ System.getProperty("java.library.path"));
				LpSolve.lpSolveVersion();

				long start = System.nanoTime();
				LpSolve solver = LpSolve.readLp(lpFile.getAbsolutePath(), 1,
						lpFile.getName());
				solver.setBbRule(LpSolve.NODE_PSEUDONONINTSELECT
						+ LpSolve.NODE_RESTARTMODE + LpSolve.NODE_DYNAMICMODE
						+ LpSolve.NODE_RCOSTFIXING);
				solver.setTimeout(120);
				solver.solve();
				long duration = System.nanoTime() - start;
				if (verbose)
					System.out.println("\n====took: " + duration / 1000 / 1000
							+ " ms====");

				Set<String> optimalQualityPath = new HashSet<String>();
				if (verbose)
					System.out.println("Value of objective function: "
							+ solver.getObjective());
				double[] var = solver.getPtrVariables();
				for (int i = 0; i < var.length; i++) {
					if (solver.getColName(i + 1) != null) {
						// && solver.getColName(i + 1).startsWith("b#")) {
						// if (var[i] == 1) {
						if (verbose)
							System.out.println("Value of var[" + i + "] ("
									+ solver.getColName(i + 1) + ") = "
									+ var[i]);
						ret.put(solver.getColName(i + 1), (int) var[i]);
						// optimalQualityPath.add(solver.getColName(i + 1));
						// }
					}
				}

				int cols = solver.getNcolumns();
				int rows = solver.getNrows();
				double obj = solver.getObjective();
				// to free memory
				solver.deleteLp();

				String msg = "Optimal Mapping:\n\n";
				for (String str : optimalQualityPath) {
					String[] parts = str.split("#");
					String impl = parts[1];
					String mode = parts[2];
					String server = parts[3];

					msg += impl + "(" + mode + ") => " + server + "\n";
					// ret.put(impl.replaceAll("_", "."), server);
				}
				msg += "\n\nSolver Time: " + (duration / 1000000) + " ms\n";
				msg += "Cols: " + cols + "\n";// vars
				msg += "Rows: " + rows + "\n";// constraints
				msg += "Obj: " + obj + "\n";

				if (verbose)
					System.out.println(msg);

				return ret;
			} catch (LpSolveException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	private Set<String> combos(String[][] in, String soFar, int start) {
		Set<String> ret = new TreeSet<String>();
		for (int i = start; i < in.length; i++) {
			String[] block = in[i];
			for (int x = 0; x < block.length; x++) {
				// get all combinations possible for this selection
				if (i < in.length - 1) {
					ret.addAll(combos(in, soFar + "" + block[x], start + 1));
				} else {
					ret.add(soFar + "" + block[x]);
				}
			}
			return ret;
		}
		return ret;
	}

	public void run() {
		try {
			runMOILP(moilp, verbose);
		} catch (InterruptedException e) {
			System.out.flush();
			System.out.println("Solver canceled");
		} catch (OutOfMemoryError oome) {
			System.out.println("Out Of Memory");
			outofmemory = true;
		}
	}

	public int getSolvedILPs() {
		return solvedILPs;
	}

	public void terminate() {
		terminate = true;
	}

	public int getNumSolutions() {
		return numSolutions;
	}

	public long getSolverTime() {
		return solverTime;
	}

	public boolean isOutofmemory() {
		return outofmemory;
	}

}
