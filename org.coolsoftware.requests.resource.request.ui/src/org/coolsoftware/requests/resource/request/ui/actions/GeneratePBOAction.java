/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 * 
 * @author Sebastian Götz
 */
public class GeneratePBOAction implements IObjectActionDelegate {
	
	private Request req = null;
	private File reqFile = null;
	private boolean single = true;
	private File rootDir;

	public GeneratePBOAction() {

	}

	@Override
	public void run(IAction action) {
		if (req == null && single == true) {
			System.err.println("Request must not be null!");
		} else {
			if (single) {
				work();
			} else {
				// fetch each req
				URI targetDir = URI.createFileURI(rootDir.getAbsolutePath());
				for (int a = 24; a <= 100; a++) {
					for (int b = 2; b <= 100; b++) {
						if(a==24 && b < 51) b=51; 
						targetDir = targetDir.appendSegment("gen_" + a + "x"
								+ b + "-1x1");
						targetDir = targetDir
								.appendSegment("generated.request");
						System.out.println(targetDir);
						ResourceSet rs = new ResourceSetImpl();
						org.eclipse.emf.ecore.resource.Resource res = rs
								.getResource(targetDir, true);
						req = (Request) res.getContents().iterator()
								.next();
						work();

						targetDir = targetDir.trimSegments(2);
						System.gc();
					}
				}
			}
		}
	}
	
	private void work() {
		String eclURI = Util.getEclUriFromRequest(req);
		EclFile resolvedEclFile = ResolverUtil.loadEclFile(eclURI);

		Map<String, EclFile> eclFiles = new HashMap<String, EclFile>();

		Resource root = (Resource) req.getHardware().getHwmodel().getRoot();
		for (Resource server : root.getSubresources()) {
			eclFiles.put(server.getName(), resolvedEclFile);
		}

		StructuralModel smodel = req.getImport().getModel();

		PBOGenerator2 gen = new PBOGenerator2();
		String pbo;

		pbo = gen.generatePBOforRequest(req, eclFiles, false, smodel, req
				.getImport().getModel(), req.getHardware().getHwmodel());

		//System.out.println(ilp);

		URI ilpRes = req.eResource().getURI().trimSegments(1)
				.appendSegment("generated.pbo");
		try {
			OutputStream os = URIConverter.INSTANCE.createOutputStream(ilpRes);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.write(pbo);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("###\n"+pbo+"\n###");

		Map<String, String> result = gen.runPBO(req, eclFiles, pbo);

		System.out.println(result);
		
		resolvedEclFile = null;
		eclFiles = null;
		root = null;
		smodel = null;
		gen = null;
		ilpRes = null;
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof TreeSelection) {
			TreeSelection ts = (TreeSelection) selection;
			if (ts.getFirstElement() instanceof org.eclipse.core.internal.resources.File) {
				req = ResolverUtil.getSelectRequest(selection);
				reqFile = ResolverUtil.getSelectedRequestFile(selection);
				single = true;
			} else {
				rootDir = ResolverUtil.getSelectedFolder(selection);
				single = false;
			}
		}
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		
	}

}
