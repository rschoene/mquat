/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.requests.resource.request.SystemGenerator;
import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 * 
 * @author Sebastian Götz
 */
public class GenerateSystemAction  implements IObjectActionDelegate {

	private URI targetDir;
	
	private class ConfigParameters {
		private int numServer, numTypes, numImpls, numModes;

		public int getNumServer() {
			return numServer;
		}

		public void setNumServer(int numServer) {
			this.numServer = numServer;
		}

		public int getNumTypes() {
			return numTypes;
		}

		public void setNumTypes(int numTypes) {
			this.numTypes = numTypes;
		}

		public int getNumImpls() {
			return numImpls;
		}

		public void setNumImpls(int numImpls) {
			this.numImpls = numImpls;
		}

		public int getNumModes() {
			return numModes;
		}

		public void setNumModes(int numModes) {
			this.numModes = numModes;
		}
	}
	
	private class ConfigDialog extends Dialog {
		
		private Text t1, t2, t3, t4;
		private ConfigParameters params;

		protected ConfigDialog(Shell parentShell) {
			super(parentShell);
			params = new ConfigParameters();
			//default values
			params.setNumImpls(1);
			params.setNumModes(1);
			params.setNumServer(30);
			params.setNumTypes(30);
		}
		
		protected void configureShell(Shell newShell) {
			super.configureShell(newShell);
			newShell.setText("System Generation Parameters");
			newShell.setSize(300, 200);
		}	
		
		@Override
		protected Control createDialogArea(Composite composite) {
			
			GridLayout gl = new GridLayout(2, false);
			composite.setLayout(gl);
			
			GridData gridData = new GridData();
		    gridData.grabExcessHorizontalSpace = true;
		    gridData.horizontalAlignment = GridData.FILL;
			
			Label l1 = new Label(composite, SWT.NONE);
			l1.setText("Number of Components: ");
			
			t1 = new Text(composite, SWT.BORDER);
			t1.setText("30");
			t1.setLayoutData(gridData);
			
			Label l2 = new Label(composite, SWT.NONE);
			l2.setText("Number of Servers: ");
			
			t2 = new Text(composite, SWT.BORDER);
			t2.setText("30");
			t2.setLayoutData(gridData);
			
			Label l3 = new Label(composite, SWT.NONE);
			l3.setText("Number of Impls per Type: ");
			
			t3 = new Text(composite, SWT.BORDER);
			t3.setText("1");
			t3.setLayoutData(gridData);
			
			Label l4 = new Label(composite, SWT.NONE);
			l4.setText("Number of Modes per Impl: ");
			
			t4 = new Text(composite, SWT.BORDER);
			t4.setText("1");
			t4.setLayoutData(gridData);
			
			composite.pack();
			
			return composite;
		}
		
		@Override
		protected void buttonPressed(int buttonId) {
			params.setNumTypes(Integer.parseInt(t1.getText()));
			params.setNumServer(Integer.parseInt(t2.getText()));
			params.setNumImpls(Integer.parseInt(t3.getText()));
			params.setNumModes(Integer.parseInt(t4.getText()));
			super.buttonPressed(buttonId);
		}
		
		public ConfigParameters getParams() {
			return params;
		}
	}

	@Override
	public void run(IAction action) {
		
		
		ConfigDialog d = new ConfigDialog(null);
		int ret = d.open();
		if(ret == Dialog.CANCEL) return;
		
		ConfigParameters p = d.getParams();
		int numComponents = p.getNumTypes();
		int numRes = p.getNumServer();
		int numImpls = p.getNumImpls();
		int numModes = p.getNumModes();
		
		System.out.println("Generating: "+numComponents+" x "+numRes+" System in "+targetDir);
		
		for(int a = 83; a <= 100; a++) {
			for(int b = 2; b <= 100; b++) {
				targetDir = targetDir.appendSegment("gen_"+a+"x"+b+"-"+numImpls+"x"+numModes);
				SystemGenerator.generateProblem("Generated", a-1, a-1, 1, 1, numImpls, numModes, 1, 1, 1, b, 1, 0,targetDir);
				targetDir = targetDir.trimSegments(1);
			}
		}
		
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		File folder = ResolverUtil.getSelectedFolder(selection);
		if(folder != null) {
			targetDir = URI.createFileURI(folder.getAbsolutePath());
		}
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}
}
