/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.Request;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.haec.theatre.utils.FileUtils;

/**
 * 
 * @author Sebastian Götz
 */
public class ILPGenerator2 {

	private Map<String, String> resourceShortCuts;
	private Set<String> binaryVars = new HashSet<String>();
	private URI baseURI;
	
	private Map<String,Map<String,EclFile>> allContracts;

	private StructuralModel swModel, hwModel;
	private VariantModel infra;
	
	private CcmExpressionInterpreter interpreter;
	
	private long genArch = 0, genRes = 0, genNFP = 0, genObj = 0;
	
	public ILPGenerator2() {
		allContracts = new HashMap<String,Map<String,EclFile>>();
		interpreter = new CcmExpressionInterpreter();
		resourceShortCuts = new HashMap<String, String>();
		binaryVars = new HashSet<String>();
	}

	public String generateILPforRequest(Request req,
			Map<String, EclFile> eclFiles, boolean verbose, StructuralModel smodel, StructuralModel hwModel,
			VariantModel infra, boolean dryrun) {

		this.swModel = smodel;
		this.hwModel = hwModel;
		this.infra = infra;
		
		baseURI = req.eResource().getURI();
		baseURI = baseURI.trimSegments(1);
		
		int numRes = 0;
		for (String res : eclFiles.keySet()) {
			numRes++;
			resourceShortCuts.put(res,"R" + res.substring("Server".length()));
			if(verbose) {
				System.out.println(res);
			}
		}
		
		allContracts.put(req.getComponent().getName(), eclFiles);

		StringBuilder sb = new StringBuilder();
		
		// architectural constraints
		long start = System.currentTimeMillis();
		sb.append(generateArchitecturalConstraints(req, eclFiles, verbose));
		sb.append("\n");
		long stop = System.currentTimeMillis();
		genArch = stop - start;
		// resource negotiation
		start = System.currentTimeMillis();
		sb.append(generateResourceConstraints(req, verbose));
		sb.append("\n");
		stop = System.currentTimeMillis();
		genRes = stop - start;
		// software NFP negotiation
		start = System.currentTimeMillis();
		sb.append(generateNFPConstraints(req, verbose));
		sb.append("\n");
		stop = System.currentTimeMillis();
		genNFP = stop - start;
		
		sb.append("\nint ");
		for(String bv : binaryVars) {
			sb.append(bv+", ");
		}
		sb.delete(sb.length()-2, sb.length());
		sb.append(";");
		
		// objective function
		start = System.currentTimeMillis();
		StringBuffer ret = generateObjectiveFunction(req,eclFiles,verbose);
		stop = System.currentTimeMillis();
		genObj = stop - start;
		ret.append("\n");
		ret.append(sb);
		
//		String msg = "Generation Summary:\n ";
//		msg += "Architectural Constraints: "+genArch+" ms\n";
//		msg += "Resource Negotiation     : "+genRes +" ms\n";
//		msg += "Software NFP Negotiation : "+genNFP +" ms\n";
//		msg += "Objective Function       : "+genObj +" ms\n";
//		
//		URI resultFile = req.eResource().getURI().trimSegments(1).appendSegment("ilp-gen-result-3.txt");
//		OutputStream os;
//		try {
//			os = URIConverter.INSTANCE.createOutputStream(resultFile);
//			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
//			bw.write(msg);
//			bw.flush();
//			bw.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		if(!dryrun) {
			// schema: solver, timestamp, arch[itectural constraints], res[ource negotation], nfp [negotiation], obj[ective function]
			String fileName = req.eResource().getURI().trimSegments(1).appendSegment("gen.csv").toFileString();
			FileUtils.writeToCsv(fileName, "Toy", timestamp(), genArch, genRes, genNFP, genObj);
		}
		
		return ret.toString();
	}

	private String timestamp() {
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
		return dateFormat.format(currentTime);
	}

	private StringBuffer generateObjectiveFunction(Request req,
			Map<String, EclFile> eclFiles, boolean verbose) {
		StringBuffer b = new StringBuffer();
		b.append("min: ");
		for(String bv : binaryVars) {
			b.append(((int)(Math.random()*100))+bv+" + ");
		}
		b.delete(b.length()-3, b.length());
		b.append(";\n");
		return b;
	}

	/**
	 * Generate architectural constraints of ILP.
	 * 
	 * @param req
	 *            the request
	 * @param eclFiles
	 *            Map<String,EclFile> Map of container URIs to contract file of
	 *            directly requested component type
	 * @param verbose
	 *            show debug info or not
	 * @return architectural constraints as String
	 */
	private StringBuilder generateArchitecturalConstraints(Request req,
			Map<String, EclFile> eclFiles, boolean verbose) {

		// at least one impl per required type has to be chosen
		StringBuilder archStr = new StringBuilder();

		// get all impls of this type, use the first container's contract
		String firstContainer = eclFiles.keySet().iterator().next();
		EclFile contractOfFirstComponent = eclFiles.get(firstContainer);

		try {
			archStr.append(generateArchitecturalConstraintForComponentType(contractOfFirstComponent));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return archStr;
	}

	private StringBuilder generateArchitecturalConstraintForComponentType(
			EclFile file) throws IOException {
		StringBuilder ret = new StringBuilder();

		Set<SWComponentType> furtherRequiredTypes = new HashSet<SWComponentType>();
		boolean nonEmptyConstraint = false;
		
		for (EclContract c : file.getContracts()) {
			SWComponentContract sc = (SWComponentContract) c;
			String implName = sc.getName();
			for (SWContractMode m : sc.getModes()) {
				String modeName = m.getName();
				for (SWContractClause cl : m.getClauses()) {
					if (cl instanceof SWComponentRequirementClause) {
						furtherRequiredTypes
								.add(((SWComponentRequirementClause) cl)
										.getRequiredComponentType());
					}
				}
				for (String r : resourceShortCuts.values()) {
					String binaryVar = "b#" + implName + "#" + modeName + "#"
							+ r;
					binaryVars.add(binaryVar);
					ret.append(binaryVar);
					ret.append(" + ");
					nonEmptyConstraint = true;
				}
			}
		}
		
		int end = ret.length();
		int start = end - 2;
		if(start > 0)
		ret.delete(start, end);
		
		if(furtherRequiredTypes.size() > 0) {
			ret.append(" = 1;\n");
			for(SWComponentType t : furtherRequiredTypes) {
				EclFile f = getContractForComponentType(t);
				Map<String,EclFile> ctrs = new HashMap<String, EclFile>();
				for(String res : resourceShortCuts.keySet()) {
					ctrs.put(res,f);
				}
				allContracts.put(t.getName(),ctrs);
				ret.append(generateArchitecturalConstraintForComponentType(f));
				//TODO handle implication constraint
			}
		} else if(nonEmptyConstraint){
			ret.append(" = 1;\n");
		}

		return ret;
	}

	private EclFile getContractForComponentType(SWComponentType t) throws IOException {
		String uri = t.getEclUri();
		URI fileURI = baseURI.appendSegment(uri);
		String serializedContract = FileUtils.readContractAsText(fileURI);
		
		EclFile ret = readContract(fileURI);
		return ret;
	}
	
	private EclFile readContract(URI fileURI) throws IOException {
		Set<StructuralModel> visibleStructuralModels = new HashSet<StructuralModel>();
		visibleStructuralModels.add(swModel);
		visibleStructuralModels.add(hwModel);
		
		ResourceSet rs = new ResourceSetImpl();
		Resource res = rs.createResource(fileURI);
		res.load(null);
		EObject root = res.getAllContents().next();
		EcoreUtil.resolveAll(root);
		return (EclFile)root;
	}

	private StringBuffer generateResourceConstraints(Request req,
			 boolean verbose) {
		
		StringBuffer ret = new StringBuffer();
		
		//property => binaryVar => val
		Map<String, Map<String, Long>> reqs = new HashMap<String, Map<String,Long>>();
		
		for(String typeName : allContracts.keySet()) {
			Map<String,EclFile> eclFiles = allContracts.get(typeName);
			//collect resource requirements in contracts per server			
			for(String res : eclFiles.keySet()) {
				EclFile f = eclFiles.get(res);
				for(EclContract c : f.getContracts()) {
					String implName = c.getName();
					for(SWContractMode m : ((SWComponentContract)c).getModes()) {
						String modeName = m.getName();
						for(SWContractClause cl : m.getClauses()) {
							if(cl instanceof HWComponentRequirementClause) {
								HWComponentRequirementClause hrc = (HWComponentRequirementClause)cl;
								String hwName = hrc.getRequiredResourceType().getName();
								for(PropertyRequirementClause prc : hrc.getRequiredProperties()) {
									String propertyName = prc.getRequiredProperty().getDeclaredVariable().getName();
									
									propertyName = resourceShortCuts.get(res)+"#"+hwName+"#"+propertyName;
									Statement stmt = prc.getMinValue();
									CcmValue v = interpreter.interpret(stmt);
									long val = ((CcmInteger)v).getIntegerValue();
									String bv = "b#"+implName+"#"+modeName+"#"+resourceShortCuts.get(res);
									Map<String,Long> r = reqs.get(propertyName);
									if(r == null) r = new HashMap<String, Long>();
									r.put(bv, val);
									reqs.put(propertyName, r);
								}
							}
						}
					}
				}
			}
		}
		
		for(String propertyName : reqs.keySet()) {
			ret.append(propertyName+" >= 0;\n");
			ret.append(propertyName+" <= "+getUpperBoundForResource(propertyName)+";\n");
			ret.append(propertyName+" = ");
			Map<String, Long> r = reqs.get(propertyName);
			for(String bv : r.keySet()) {
				ret.append(r.get(bv)+" "+bv+" + ");
			}
			ret.delete(ret.length()-3, ret.length());
			ret.append(";\n");
		}
		
		return ret;
	}

	private String getUpperBoundForResource(String propertyName) {
		String[] idx = propertyName.split("#"); //resource#type#property
		org.coolsoftware.coolcomponents.ccm.variant.Resource root = (org.coolsoftware.coolcomponents.ccm.variant.Resource)infra.getRoot();
		for(org.coolsoftware.coolcomponents.ccm.variant.Resource x : root.getSubresources()) {
			if(idx[0].equals(resourceShortCuts.get(x.getName())))
			for(org.coolsoftware.coolcomponents.ccm.variant.Resource y : x.getSubresources()) {
				if(y.getSpecification().getName().equals(idx[1]))
				for(VariantPropertyBinding vpb : y.getPropertyBinding()) {
					if(vpb.getProperty().getDeclaredVariable().getName().equals(idx[2])) {
						CcmInteger val = (CcmInteger)interpreter.interpret(vpb.getValueExpression());
						return val.getIntegerValue()+"";
					}
				}
			}
		}
		return "42"; //if nothing found -> 42
	}

	private StringBuffer generateNFPConstraints(Request req,
			boolean verbose) {
		
		StringBuffer ret = new StringBuffer();
		
		//property => binaryVar => val
		Map<String, Map<String, Long>> reqs = new HashMap<String, Map<String,Long>>();
		Map<String, Map<String, Long>> provs = new HashMap<String, Map<String,Long>>();
		
		for(String typeName : allContracts.keySet()) {
			Map<String,EclFile> eclFiles = allContracts.get(typeName);
			//collect resource requirements in contracts per server			
			for(String res : eclFiles.keySet()) {
				EclFile f = eclFiles.get(res);
				for(EclContract c : f.getContracts()) {
					String implName = c.getName();
					for(SWContractMode m : ((SWComponentContract)c).getModes()) {
						String modeName = m.getName();
						for(SWContractClause cl : m.getClauses()) {
							if(cl instanceof SWComponentRequirementClause) {
								SWComponentRequirementClause src = (SWComponentRequirementClause)cl;
								String swName = src.getRequiredComponentType().getName();
								for(PropertyRequirementClause prc : src.getRequiredProperties()) {
									String propertyName = prc.getRequiredProperty().getDeclaredVariable().getName();
									
									propertyName = swName+"#"+propertyName;
									Statement stmt = prc.getMinValue();
									CcmValue v = interpreter.interpret(stmt);
									long val = ((CcmInteger)v).getIntegerValue();
									String bv = "b#"+implName+"#"+modeName+"#"+resourceShortCuts.get(res);
									Map<String,Long> r = reqs.get(propertyName);
									if(r == null) r = new HashMap<String, Long>();
									r.put(bv, val);
									reqs.put(propertyName, r);
								}
							} else if(cl instanceof ProvisionClause) {
								ProvisionClause pc = (ProvisionClause)cl;
								String propertyName = pc.getProvidedProperty().getDeclaredVariable().getName();
								propertyName = typeName+"#"+propertyName;
								Statement stmt = pc.getMinValue();
								CcmValue v = interpreter.interpret(stmt);
								long val = ((CcmInteger)v).getIntegerValue();
								String bv = "b#"+implName+"#"+modeName+"#"+resourceShortCuts.get(res);
								Map<String,Long> r = provs.get(propertyName);
								if(r == null) r = new HashMap<String, Long>();
								r.put(bv, val);
								provs.put(propertyName, r);
							}
						}
					}
				}
			}
		}
		
		for(String propertyName : reqs.keySet()) {
			ret.append(propertyName+" >= ");
			Map<String, Long> r = reqs.get(propertyName);
			for(String bv : r.keySet()) {
				ret.append(r.get(bv)+" "+bv+" + ");
			}
			ret.delete(ret.length()-3, ret.length());
			ret.append(";\n");
		}
		
		for(String propertyName : provs.keySet()) {
			ret.append(propertyName+" = ");
			Map<String, Long> r = provs.get(propertyName);
			for(String bv : r.keySet()) {
				ret.append(r.get(bv)+" "+bv+" + ");
			}
			ret.delete(ret.length()-3, ret.length());
			ret.append(";\n");
		}
		
		return ret;
	}

	public Map<String, String> runILP(Request req,
			Map<String, EclFile> eclFiles, String ilp, boolean dryrun) {
		File reqFile = new File("tmp");
		return runILP(req, eclFiles, reqFile, ilp, dryrun);
	}

	private Map<String, String> runILP(Request req,
			Map<String, EclFile> eclFiles, File reqFile, String ilp, boolean dryrun) {

		Map<String, String> ret = new HashMap<String, String>();
		
		File lpFile = new File(reqFile.getAbsolutePath() + ".lp");
		System.out.println("lpFile: " + lpFile.getAbsolutePath());
		try {
			if (!lpFile.exists())
				if (!lpFile.createNewFile())
					System.out.println("cannot create lp file");

			FileWriter fw = new FileWriter(lpFile, false);
			fw.write(ilp);
			fw.close();
			// persist contract for debugging
//			EclFile ecl = eclFiles.values().iterator().next();
//			File cinst = new File(reqFile.getAbsolutePath() + "-instance.ecl");
//			FileOutputStream fos = new FileOutputStream(cinst);
//			ecl.eResource().save(fos,
//					new HashMap<Object, Object>());
//			fos.close();

			System.out.println("written ilp into file: "
					+ lpFile.getAbsolutePath() + "/" + lpFile.getName());
//			System.out.println("written contract instance into file: "
//					+ cinst.getAbsolutePath() + "/" + cinst.getName());

			try {
				String jlp = System.getProperty("java.library.path"); // Windows
				String workspaceLocation = ResourcesPlugin.getWorkspace()
						.getRoot().getLocationURI().getPath();
				if (workspaceLocation.length() > 0) {
					workspaceLocation = workspaceLocation.replaceAll("/",
							"\\\\");
					if (workspaceLocation.startsWith("\\"))
						workspaceLocation = workspaceLocation.substring(1);
					if (!jlp.endsWith(File.pathSeparator))
						jlp += File.pathSeparator;
					jlp += workspaceLocation + File.separator;
				}
				System.setProperty("java.library.path", jlp);
				System.out.println("JLP: "
						+ System.getProperty("java.library.path"));
				LpSolve.lpSolveVersion();

				long start = System.nanoTime();
				LpSolve solver = LpSolve.readLp(lpFile.getAbsolutePath(), 1,
						lpFile.getName());
				solver.setBbRule(LpSolve.NODE_PSEUDONONINTSELECT
						+ LpSolve.NODE_RESTARTMODE + LpSolve.NODE_DYNAMICMODE
						+ LpSolve.NODE_RCOSTFIXING);
				solver.setTimeout(120);
				solver.solve();
				long duration = (System.nanoTime() - start) / 1000 / 1000;
				System.out.println("\n====took: " + duration + " ms====");

				Set<String> optimalQualityPath = new HashSet<String>();
				System.out.println("Value of objective function: "
						+ solver.getObjective());
				double[] var = solver.getPtrVariables();
				for (int i = 0; i < var.length; i++) {
					if (solver.getColName(i + 1) != null
							&& solver.getColName(i + 1).startsWith("b#")) {
						if (var[i] == 1) {
							System.out.println("Value of var[" + i + "] ("
									+ solver.getColName(i + 1) + ") = "
									+ var[i]);
							optimalQualityPath.add(solver.getColName(i + 1));
						}
					}
				}

				int cols = solver.getNcolumns();
				int rows = solver.getNrows();
				double obj = solver.getObjective();
				
				if(!dryrun) {
					// schema: solver, timestamp, rows, cols, duration
					String fileName = req.eResource().getURI().trimSegments(1).appendSegment("sol.csv").toFileString();
					FileUtils.writeToCsv(fileName, "Toy", timestamp(), rows, cols, duration);
				}

				// to free memory
				solver.deleteLp();

				String msg = "Optimal Mapping:\n\n";
				for (String str : optimalQualityPath) {
					String[] parts = str.split("#");
					String impl = parts[1];
					String mode = parts[2];
					String server = parts[3];
					for (String x : resourceShortCuts.keySet()) {
						if (resourceShortCuts.get(x).equals(server)) {
							server = x;
							break;
						}
					}
					msg += impl + "(" + mode + ") => " + server + "\n";
					ret.put(impl.replaceAll("_", "."), server);
				}
				msg += "\n\nSolver Time: "+ duration + " ms\n";
				msg += "Cols: "+cols+"\n";//vars
				msg += "Rows: "+rows+"\n";//constraints
				msg += "Obj: "+obj+"\n";

				System.out.println(msg);
				
				URI resultFile = req.eResource().getURI().trimSegments(1).appendSegment("ilp-result-max-3.txt");
				OutputStream os = URIConverter.INSTANCE.createOutputStream(resultFile);
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
				bw.write(msg);
				bw.flush();
				bw.close();
				
			} catch (LpSolveException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		return ret;
		
	}

}
