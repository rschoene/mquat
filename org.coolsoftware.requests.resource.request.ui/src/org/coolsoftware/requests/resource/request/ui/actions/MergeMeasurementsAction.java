/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 * 
 * @author Sebastian Götz
 */
public class MergeMeasurementsAction implements IObjectActionDelegate {
	
	private File selectedFolder;
	private URI targetDir;

	@Override
	public void run(IAction action) {
		StringBuffer all = new StringBuffer();
		if(selectedFolder != null) {
			for(int a = 2; a <= 100; a++) {
				for(int b = 2; b <= 100; b++) {
					if(a == 96 && b == 71) break;
					targetDir = targetDir.appendSegment("gen_"+a+"x"+b+"-1x1");
					all.append(processDir(targetDir,a,b));
					targetDir = targetDir.trimSegments(1);
				}
			}
		}
		//ret.append(a+";"+b+";"+tgen+";"+tarch+";"+tres+";"+tnfp+";"+tobj+";"+tsolve+";"+vars+";"+constraints+"\n");
		System.out.println("Components;Server;Tgen;Tarch;Tres;Tnfp;Tobj;Tsolve;vars;constraints;hasSolution");
		System.out.println(all.toString());
	}

	private StringBuffer processDir(URI dir,int a, int b) {
		StringBuffer ret = new StringBuffer();
		dir = dir.appendSegment("ilp-result.txt");
		int tsolve = 0, vars = 0, constraints = 0, tgen = 0, tarch = 0,tres = 0,tnfp = 0,tobj = 0, hasSol = 0;
		try {
			InputStream is = URIConverter.INSTANCE.createInputStream(dir);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuffer contents = new StringBuffer();
			while((line = br.readLine()) != null) {
				contents.append(line+"\n");
			}
			line = contents.toString();
			
			tsolve = getValueForString(line, "Solver Time:", " ms");
			vars = getValueForString(line, "Cols:", "\n");
			constraints = getValueForString(line, "Rows:", "\n");
			
			int start = line.indexOf("Optimal Mapping:")+"Optimal Mapping:".length();
			int stop = line.indexOf("Solver Time:");
			String sol = line.substring(start,stop).trim();
			System.out.println(sol);
			
			if(sol.length() > 0) hasSol = 1;
			
			br.close();
			is.close();
		
			dir = dir.trimSegments(1);
			dir = dir.appendSegment("ilp-gen-result.txt");
			InputStream is2 = URIConverter.INSTANCE.createInputStream(dir);
			BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
			
			StringBuffer contents2 = new StringBuffer();
			String line2;
			while((line2 = br2.readLine()) != null) {
				contents2.append(line2+"\n");
			}
			line2 = contents2.toString();
			
			tarch = getValueForString(line2, "Architectural Constraints:", " ms");
			tres = getValueForString(line2, "Resource Negotiation     :", " ms");
			tnfp = getValueForString(line2, "Software NFP Negotiation :", " ms");
			tobj = getValueForString(line2, "Objective Function       :", " ms");
			tgen = tarch+tres+tnfp+tobj;
			
			br2.close();
			is2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ret.append(a+";"+b+";"+tgen+";"+tarch+";"+tres+";"+tnfp+";"+tobj+";"+tsolve+";"+vars+";"+constraints+";"+hasSol+"\n");		
		
		dir.trimSegments(1);
		return ret;
	}
	
	private int getValueForString(String line, String key, String delimiter) {
		if(line.contains(key)) {
			int start = line.indexOf(key)+key.length()+1;
			int stop = line.indexOf(delimiter,start);
			try {
				int ret = Integer.parseInt(line.substring(start,stop));
				return ret;
			}
			catch (Exception e) {
				System.err.println(key+" "+delimiter);
				System.err.println(line);
			}
		} else {
			System.err.println(key+" not in "+line);
		}
		return -1;
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		selectedFolder = ResolverUtil.getSelectedFolder(selection);
		if(selectedFolder != null) {
			targetDir = URI.createFileURI(selectedFolder.getAbsolutePath());
		}
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		
	}

}
