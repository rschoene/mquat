/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 * 
 * @author Sebastian Götz
 */
public class GenerateACOAction implements IObjectActionDelegate {
	
	private Request req = null;
	private File reqFile = null;
	private boolean single = true;
	private File rootDir;

	public GenerateACOAction() {

	}

	@Override
	public void run(IAction action) {
		if (req == null && single == true) {
			System.err.println("Request must not be null!");
		} else {
			if (single) {
				for(int iters = 10; iters <= 10; iters += 10)
					for(int ants = 10; ants <= 10; ants += 10) {
						System.out.println("ants: "+ants+" iters: "+iters);
//						double totacc = 0;
						for(int i = 1; i < 10; i++) {
							double acc = work(iters,ants,0);
//							totacc += acc;
						}
//						System.out.println("d/acc: "+totacc/10);
					}
			} else {
				// fetch each req
				URI targetDir = URI.createFileURI(rootDir.getAbsolutePath());
				for (int a = 2; a <= 30; a++) {
					for (int b = 2; b <= 100; b++) {
						if((a >= 27 && b >= 60) || a > 27) continue;
						targetDir = targetDir.appendSegment("gen_" + a + "x"
								+ b + "-1x1");
						targetDir = targetDir
								.appendSegment("generated.request");
						System.out.println(targetDir);
						ResourceSet rs = new ResourceSetImpl();
						org.eclipse.emf.ecore.resource.Resource res = rs
								.getResource(targetDir, true);
						req = (Request) res.getContents().iterator()
								.next();
						//for(int i = 1; i < 10; i++) {
							work(100,100,0);
						//}

						targetDir = targetDir.trimSegments(2);
						System.gc();
					}
				}
			}
		}
	}

	private double work(int numIters, int numAnts, double lastAcc) {
		long start = System.currentTimeMillis();
		String eclURI = Util.getEclUriFromRequest(req);
		EclFile resolvedEclFile = ResolverUtil.loadEclFile(eclURI);

		Map<String, EclFile> eclFiles = new HashMap<String, EclFile>();

		Resource root = (Resource) req.getHardware().getHwmodel().getRoot();
		for (Resource server : root.getSubresources()) {
			eclFiles.put(server.getName(), resolvedEclFile);
		}


		ACOGenerator2 gen = new ACOGenerator2();

		Map<String,String> result = gen.generateAndRunACOForRequest(req, eclFiles, numAnts, numIters, false);
		
		System.out.print("Optimal mapping: ");
		System.out.println(result);
		
		//now assess the mapping
		//1. get objective value for the mapping
		double objValue = gen.getObjValueOfOptimalMapping();
		boolean valid = gen.isOptimalMappingValid();
		//2. get min/max values from ILP results
		String minFileName = "ilp-result-min-3.txt";
		String maxFileName = "ilp-result-max-3.txt";
		double minObj = getObjFromResultFile(minFileName);
		double maxObj = getObjFromResultFile(maxFileName);
		NumberFormat nf = NumberFormat.getPercentInstance();
		nf.setMaximumFractionDigits(2);
		double accuracy = (1-(objValue-minObj)/(maxObj-minObj));
		
		//if(accuracy < lastAcc) return accuracy;
		
		System.out.println("Objective value range: "+minObj+" < "+objValue+" < "+maxObj+" ==> "+accuracy+" ("+valid+")");
		long stop = System.currentTimeMillis();
		
		String msg = "Optimal mapping: "+result+"\nAccuracy: "+accuracy+"\nants: "+numAnts+"\niters: "+numIters+"\nTime: "+(stop-start)+" ms\nvalid: "+(valid?"1":"0")+"\n";
		try {
			URI resultFile = req.eResource().getURI().trimSegments(1).appendSegment("aco-result-3.txt");
			OutputStream os = URIConverter.INSTANCE.createOutputStream(resultFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.write(msg);
			bw.flush();
			bw.close();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		gen.clear();
		resolvedEclFile = null;
		eclFiles = null;
		root = null;
		gen = null;
		
		System.out.println("Took: "+(stop-start)+" ms");
		
		return accuracy;
	}

	private double getObjFromResultFile(String minFileName) {
		URI reqURI = req.eResource().getURI();
		reqURI = reqURI.trimSegments(1);
		reqURI = reqURI.appendSegment(minFileName);
		try {
			InputStream is = URIConverter.INSTANCE.createInputStream(reqURI);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = "";
			while((line = br.readLine()) != null) {
				if(line.startsWith("Obj: ")) {
					String strObj = line.substring("Obj: ".length()).trim();
					return Double.parseDouble(strObj);
				}
			}
			br.close();
			is.close();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		return -1;
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof TreeSelection) {
			TreeSelection ts = (TreeSelection) selection;
			if (ts.getFirstElement() instanceof org.eclipse.core.internal.resources.File) {
				req = ResolverUtil.getSelectRequest(selection);
				reqFile = ResolverUtil.getSelectedRequestFile(selection);
				single = true;
			} else {
				rootDir = ResolverUtil.getSelectedFolder(selection);
				single = false;
			}
		}
	}
	
	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {

	}

}
