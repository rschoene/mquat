/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.ui.actions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.GeneratorUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.haec.optimizer.aco.AntColony;
import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.mquat.MQuATAnt;
import org.haec.optimizer.aco.mquat.MQuATGraph;
import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.HardwareComponent;
import org.haec.optimizer.aco.mquat.bo.Implementation;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.QualityMode.Comparator;
import org.haec.optimizer.aco.mquat.bo.Requirement;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.haec.theatre.utils.FileUtils;

/**
 * 
 * @author Sebastian Götz
 */
public class ACOGenerator2 {
	
	private Map<String, Map<String,CcmValue>> providedProperties = new HashMap<String, Map<String,CcmValue>>();
	private Map<String, Map<String,CcmValue>> requiredProperties = new HashMap<String, Map<String,CcmValue>>();
	private Map<String, Resource> containers;
	
	//contains the weights extracted from ILP solution - key is the binary var name 
	private Map<String, Integer> objectiveWeights; 
	
	private double objValue;
	private boolean optValid;
	
	private URI baseURI;

	public Map<String, String> generateAndRunACOForRequest(Request req,  Map<String,EclFile> eclFiles, int numAnts, int iterations, boolean verbose) {
		double initPheromone = 100;
		objValue = 0.0;
		
		baseURI = req.eResource().getURI();
		baseURI = baseURI.trimSegments(1);
		
		try {
			objectiveWeights = extractWeightsFromILP();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
		Map<String, String> ret = new HashMap<String, String>();
		
		long start = System.currentTimeMillis();
		
		providedProperties = new HashMap<String, Map<String,CcmValue>>();
		requiredProperties = new HashMap<String, Map<String,CcmValue>>();

		Platform platform = req.getHardware();
		VariantModel vm = platform.getHwmodel();
		Resource root = (Resource) vm.getRoot();
		
		MQuATGraph graph = new MQuATGraph();
		ACOComponent groot = new ACOComponent("root", 0);
		graph.addComponent(groot);
		
		// get all servers
		containers = new HashMap<String, Resource>();
		int nServer = 0;
		//TODO how to associate these shortcuts with ILP shortcuts?!
		for (Resource r : root.getSubresources()) {
			nServer++;
			containers.put("R"+r.getName().substring("Server".length()), r);
		}		
		
		Map<String,SoftwareComponent> compTypes = new HashMap<String,SoftwareComponent>();
		Map<String,HardwareComponent> hwTypes = new HashMap<String,HardwareComponent>();
		Map<String,Container> servers = new HashMap<String,Container>();
		
		//add all servers
		for(String sName : containers.keySet()) {
			Container server = addServerToGraph(initPheromone, graph, groot,
					containers, servers, sName);
			servers.put(sName, server);
		}

		//add all SW components
		StructuralModel smodel = req.getImport().getModel();
		SWComponentType rootType = (SWComponentType)smodel.getRoot();
		for(SWComponentType t : rootType.getSubtypes()) {			
			EclFile e;
			try {
				e = getContractForComponentType(t);
				for(EclContract c : e.getContracts()) {
					addImplementationToGraph(req, initPheromone,
							graph, groot, compTypes, hwTypes, servers, c);
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		//TODO persist graph
//		System.out.println(graph);
		
		//make some Ants and let things role :D
		Requirement req1 = new Requirement(compTypes.get(req.getComponent().getName()));
		for(PropertyRequirementClause prc : req.getReqs()) {
			String propName = prc.getRequiredProperty().getDeclaredVariable().getName();
			CcmReal val = evaluateRequirementClause(req, prc);
			req1.addRequirementClause(propName, val.getRealValue(), (val.isAscending() ? Comparator.GET : Comparator.LET));
		}
		 
		AntColony colony = new AntColony(graph);
		
		for(int a = 0; a < numAnts; a++) {
			MQuATAnt ant = new MQuATAnt("ant"+(a+1), graph, groot, 200, iterations);
			ant.addInitRequirement(req1);
			colony.addAnt(ant);
		}
		
		long stop = System.currentTimeMillis();
		
		String msg = "Generation Time: "+(stop-start)+" ms\n";
		       msg += "Graph Size: "+graph.getComponents().size()+"\n";
		URI resultFile = req.eResource().getURI().trimSegments(1).appendSegment("aco-gen-result-3.txt");
		OutputStream os;
		try {
			os = URIConverter.INSTANCE.createOutputStream(resultFile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.write(msg);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Generation Time: "+(stop-start)+" ms");
		
		start = System.currentTimeMillis();
		colony.setVerbose(verbose);
//		System.out.println("###");
		colony.scheduleActivities(iterations);
		
		stop = System.currentTimeMillis();
		
		System.out.println("Processing time: "+(stop-start)+" ms");
//		
//		System.out.println("Final Graph:");
//		System.out.println("============");
//		System.out.println(colony.getGraph());
		
//		System.out.println("Optimal Configuration:");
//		System.out.println("======================");		
		MQuATGraph g = (MQuATGraph)colony.getGraph();
//		for(String str : g.getOptimalMapping()) {
		for(QualityMode qm : g.getValidOptimalMapping(req)) {
//			System.out.println(str);
			String server = qm.getOutgoingLinks().iterator().next().getTarget().getName();
			String impl = qm.getName();
			ret.put(impl, server);
		}
		objValue = g.getObjValueOfOptimalMapping();
		optValid = g.isOptimalMappingValid();
		
		return ret;
	}
	
	public void clear() {
		providedProperties = new HashMap<String, Map<String,CcmValue>>();
		requiredProperties = new HashMap<String, Map<String,CcmValue>>();
		containers = new HashMap<String, Resource>();
		objValue = 0.0;
	}
	
	public double getObjValueOfOptimalMapping() {
		return objValue;
	}
	
	public boolean isOptimalMappingValid() {
		return optValid;
	}

	private Map<String, Integer> extractWeightsFromILP() throws Exception {
		Map<String, Integer> ret = new HashMap<String, Integer>();
		//open generated ILP
		URI f = baseURI.appendSegment("generated-3.lp");
		 
		String ilp = "";
		InputStream is = URIConverter.INSTANCE.createInputStream(f);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
//		String line;
//		while((line = br.readLine()) != null) {
//			ilp += line;
//			if(line.contains("\n")) break;
//		}
		ilp = br.readLine();
		
		//min: 51b#SC13_Impl0#SC13_Impl0_mode0#R45 + 39b#SC13_Impl0#SC13_Impl0_mode0#R46 + ...;
		ilp = ilp.substring(5); //remove min:
		ilp = ilp.substring(0,ilp.length()-1); //remove ;
		String[] parts = ilp.split(" \\+ ");
		for(String p : parts) {
			int idx = p.indexOf("b#");
			String strWeight = p.substring(0,idx);
			String binVar = p.substring(idx);
			int weight = Integer.parseInt(strWeight);
			ret.put(binVar, weight);
		}
		
		baseURI.trimSegments(1);
		return ret;
	}

	private void addImplementationToGraph(Request req, double initPheromone, MQuATGraph graph,
			ACOComponent groot, Map<String, SoftwareComponent> compTypes,
			Map<String, HardwareComponent> hwTypes, Map<String, Container> servers, EclContract c) {
		//deptype,reftype,container,contract
		Map<String, Map<String, Map<String, EclFile>>> deps = new HashMap<String, Map<String, Map<String, EclFile>>>();
		
		if(c instanceof SWComponentContract) {
			setMetaparameters(req, c);
			
			SWComponentContract swc = (SWComponentContract)c;
			SWComponentType swcomp = swc.getComponentType();
			//get or create node for component type
			SoftwareComponent sc = compTypes.get(swcomp.getName());
			if(sc == null) {
				sc = new SoftwareComponent(graph, swcomp.getName(), initPheromone);
				graph.addComponent(sc);
				graph.connect(groot, sc, initPheromone);
				compTypes.put(swcomp.getName(), sc);
			}
			//get or create impl (i.e., this contract)
			Implementation impl = sc.getImplementationByName(c.getName());
			int implPheromone = (int)initPheromone;
			if(impl == null) {
				impl = sc.addImplementation(c.getName());
			}
			for(String sName : servers.keySet()) {
				for(ContractMode cm : swc.getModes()) {
					if(cm instanceof SWContractMode) {
						SWContractMode scm = (SWContractMode)cm;
						//Modes have different requirements/provisions for different containers! -> always create a new mode 						
						//set pheromone based on ILP weights
						//TODO resource naming problem - identify correct sName
						String bvn = "b#"+swc.getName()+"#"+scm.getName()+"#"+sName; 
						int pheromone = objectiveWeights.get(bvn);
						implPheromone += pheromone;
						QualityMode qm = impl.addQualityMode(scm.getName()+"@"+sName,objectiveWeights.get(bvn)); 
						qm.setWeight(objectiveWeights.get(bvn));
						
						//graph.addComponent(qm);
						graph.connect(impl, qm, pheromone);
						
						for(SWContractClause cl : ((SWContractMode)scm).getClauses()) {
							if(cl instanceof ProvisionClause) {
								handleProvisionClause(req, sName, sc, qm,
										cl);
							} else if (cl instanceof SWComponentRequirementClause) {
								handleSWRequirementClause(req, initPheromone, graph,
										compTypes, sName, sc, qm, cl, deps);
							} else if (cl instanceof HWComponentRequirementClause) {
								handleHWRequirementClause(req,
										initPheromone, graph, hwTypes,
										sName, sc, qm, cl);
							}
						}
						//connect mode and server
						graph.connect(qm, servers.get(sName), initPheromone);
					}
				}
			}
			impl.setPheromone(implPheromone);
		}
	}

	private void setMetaparameters(Request req, EclContract c) {
		for(Parameter mp : ((SWComponentContract) c).getPort().getMetaparameter()) {
			for(MetaParamValue mpv : req.getMetaParamValues()) {
				if(mpv.getMetaparam().getName().equals(mp.getName())) {
					RealLiteralExpression ile = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
					ile.setValue(mpv.getValue());
					mp.setValue(new CcmExpressionInterpreter().interpret(ile));
				}
			}
		}
	}

	private Container addServerToGraph(double initPheromone,
			MQuATGraph graph, ACOComponent groot,
			Map<String, Resource> containers, Map<String, Container> servers,
			String sName) {
		Container server = servers.get(sName);
		if(server == null) {
			server = new Container(graph, sName, initPheromone);
			graph.addComponent(server);
			graph.connect(server.getOutPort(), groot, initPheromone);

			Resource res = containers.get(sName);
			for(Resource sres : res.getSubresources()) {
				HardwareComponent hc = new HardwareComponent(graph, sres.getSpecification().getName(), initPheromone);
				//set provisions
				for(VariantPropertyBinding vpb : sres.getPropertyBinding()) {
					String pname = vpb.getProperty()
							.getDeclaredVariable().getName();
					CcmExpressionInterpreter interpreter = new CcmExpressionInterpreter();
					CcmValue val = interpreter.interpret(vpb
							.getValueExpression());
					
					if(val instanceof CcmString) continue;
					if(val instanceof CcmInteger)
						hc.addProvision(pname, (double)((CcmInteger)val).getIntegerValue());
					else
					if(val instanceof CcmReal)
						hc.addProvision(pname, ((CcmReal)val).getRealValue());
				}
				server.addResource(hc, true);
			}
			servers.put(sName, server);
		}
		return server;
	}

	private void handleHWRequirementClause(Request req,
			double initPheromone, MQuATGraph graph,
			Map<String, HardwareComponent> hwTypes, String sName,
			SoftwareComponent sc, QualityMode qm, SWContractClause cl) {
		ResourceType t = ((HWComponentRequirementClause) cl)
				.getRequiredResourceType();
		HardwareComponent dc = hwTypes.get(t.getName());
		if(dc == null) {
			dc = new HardwareComponent(graph, t.getName(), initPheromone);
			hwTypes.put(t.getName(), dc);
		}

		for(PropertyRequirementClause prc : ((HWComponentRequirementClause) cl).getRequiredProperties()) {
			CcmInteger val = (CcmInteger)evaluateRequirementClause(
					req, prc);
			String propName = prc.getRequiredProperty().getDeclaredVariable().getName();
			Map<String,CcmValue> reqVal = requiredProperties.get(propName);
			if(reqVal == null) reqVal = new HashMap<String,CcmValue>();
			reqVal.put(sc.getName()+"#"+sName, val);
			requiredProperties.put(propName, reqVal);
			
			qm.addHWRequirementClause(dc.getName(), propName, (double)val.getIntegerValue(), (val.isAscending() ? Comparator.GET : Comparator.LET));
		}
	}

	private void handleSWRequirementClause(Request req, double initPheromone, MQuATGraph graph,
			Map<String, SoftwareComponent> compTypes, String sName,
			SoftwareComponent sc, QualityMode qm, SWContractClause cl, Map<String, Map<String, Map<String, EclFile>>> deps) {
		SWComponentType reftype = ((SWComponentRequirementClause) cl)
				.getRequiredComponentType();
		SoftwareComponent dc = compTypes.get(reftype.getName());
		if(dc == null) dc = new SoftwareComponent(graph, reftype.getName(), initPheromone);
		
		try {			
			Map<String, EclFile> referrencedContractByContainer = new HashMap<String, EclFile>();
			
			for(String container : containers.keySet()) {
				EclFile f = getContractForComponentType(reftype);
				referrencedContractByContainer.put(container, f);
			}
			Map<String, Map<String, EclFile>> dep = new HashMap<String, Map<String,EclFile>>();
			dep.put(reftype.getName(), referrencedContractByContainer);
			/** will be interpreted in addServerToGraph */
			deps.put(sc.getName(), dep);
		} catch(IOException ioe) {
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
		
		for(PropertyRequirementClause prc : ((SWComponentRequirementClause) cl).getRequiredProperties()) {
			CcmReal val = evaluateRequirementClause(
					req, prc);
			String propName = prc.getRequiredProperty().getDeclaredVariable().getName();
			Map<String,CcmValue> reqVal = requiredProperties.get(propName);
			if(reqVal == null) reqVal = new HashMap<String,CcmValue>();
			reqVal.put(sc.getName()+"#"+sName, val);
			requiredProperties.put(propName, reqVal);
			
			qm.addSWRequirementClause(dc, propName, val.getRealValue(), (val.isAscending() ? Comparator.GET : Comparator.LET));
		}
	}
	
	private EclFile getContractForComponentType(SWComponentType t) throws IOException {
		String uri = t.getEclUri();
		URI fileURI = baseURI.appendSegment(uri);
		String serializedContract = FileUtils.readContractAsText(fileURI);
		
		EclFile ret = readContract(fileURI);
		return ret;
	}
	
	private EclFile readContract(URI fileURI) throws IOException {		
		ResourceSet rs = new ResourceSetImpl();
		org.eclipse.emf.ecore.resource.Resource res = rs.createResource(fileURI);
		res.load(null);
		EObject root = res.getAllContents().next();
		EcoreUtil.resolveAll(root);
		return (EclFile)root;
	}

	private void handleProvisionClause(Request req, String sName,
			SoftwareComponent sc, QualityMode qm, SWContractClause cl) {
		CcmInteger val = null;
		boolean min = true;
		Statement minmaxval = ((ProvisionClause)cl).getMinValue();
		if(minmaxval == null) {
			minmaxval = ((ProvisionClause)cl).getMaxValue();
			min = false;
		}
		if(minmaxval instanceof FunctionExpression) {
			val = (CcmInteger)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min)
				((ProvisionClause)cl).setMinValue(rle);
			else 
				((ProvisionClause)cl).setMaxValue(rle);
		} else if (((ProvisionClause)cl).getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmInteger)new CcmExpressionInterpreter().interpret(rle);
		} else {
			val = (CcmInteger)new CcmExpressionInterpreter().interpret(minmaxval);
		}
		if(((ProvisionClause)cl).getProvidedProperty().getValOrder() == Order.INCREASING) {
			if(min) { val.setAscending(true); } else { val.setAscending(false); }
		} else {
			if(min) { val.setAscending(false); } else { val.setAscending(true); }
		}
		
		
		String propName = ((ProvisionClause)cl).getProvidedProperty().getDeclaredVariable().getName();
		Map<String,CcmValue> propVal = providedProperties.get(propName);
		if(propVal == null) propVal = new HashMap<String,CcmValue>();
		propVal.put(sc.getName()+"#"+sName, val);
		providedProperties.put(propName, propVal);
		qm.addProvision(propName, (double)val.getIntegerValue(), (min ? Comparator.GET : Comparator.LET));
	}

	/**
	 * @param req
	 * @param prc
	 * @return
	 */
	private CcmReal evaluateRequirementClause(Request req,
			PropertyRequirementClause prc) {
		boolean min = true;
		Statement minmaxval = prc.getMinValue();
		if(minmaxval == null) {
			minmaxval = prc.getMaxValue();
			min = false;
		}
		CcmReal val;
		if(minmaxval instanceof FunctionExpression) {
			val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min)	prc.setMinValue(rle);
			else prc.setMaxValue(rle);
		} else if (prc.getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmReal)new CcmExpressionInterpreter().interpret(rle);
		}	else {
			val = (CcmReal)new CcmExpressionInterpreter().interpret(minmaxval);
		}
		if(prc.getRequiredProperty().getValOrder() == Order.INCREASING) {
			if(min) { val.setAscending(true); } else { val.setAscending(false); }
		} else {
			if(min) { val.setAscending(false); } else { val.setAscending(true); }
		}
		return val;
	}

}
