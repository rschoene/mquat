/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.ui;

public class RequestUIMetaInformation extends org.coolsoftware.requests.resource.request.mopp.RequestMetaInformation {
	
	public org.coolsoftware.requests.resource.request.IRequestHoverTextProvider getHoverTextProvider() {
		return new org.coolsoftware.requests.resource.request.ui.RequestHoverTextProvider();
	}
	
	public org.coolsoftware.requests.resource.request.ui.RequestImageProvider getImageProvider() {
		return org.coolsoftware.requests.resource.request.ui.RequestImageProvider.INSTANCE;
	}
	
	public org.coolsoftware.requests.resource.request.ui.RequestColorManager createColorManager() {
		return new org.coolsoftware.requests.resource.request.ui.RequestColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(org.coolsoftware.requests.resource.request.IRequestTextResour
	 * ce, org.coolsoftware.requests.resource.request.ui.RequestColorManager) instead.
	 */
	public org.coolsoftware.requests.resource.request.ui.RequestTokenScanner createTokenScanner(org.coolsoftware.requests.resource.request.ui.RequestColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public org.coolsoftware.requests.resource.request.ui.RequestTokenScanner createTokenScanner(org.coolsoftware.requests.resource.request.IRequestTextResource resource, org.coolsoftware.requests.resource.request.ui.RequestColorManager colorManager) {
		return new org.coolsoftware.requests.resource.request.ui.RequestTokenScanner(resource, colorManager);
	}
	
	public org.coolsoftware.requests.resource.request.ui.RequestCodeCompletionHelper createCodeCompletionHelper() {
		return new org.coolsoftware.requests.resource.request.ui.RequestCodeCompletionHelper();
	}
	
}
