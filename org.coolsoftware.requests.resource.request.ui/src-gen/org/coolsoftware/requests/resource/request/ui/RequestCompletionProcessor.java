/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.ui;

public class RequestCompletionProcessor implements org.eclipse.jface.text.contentassist.IContentAssistProcessor {
	
	private org.coolsoftware.requests.resource.request.IRequestResourceProvider resourceProvider;
	private org.coolsoftware.requests.resource.request.ui.IRequestBracketHandlerProvider bracketHandlerProvider;
	
	public RequestCompletionProcessor(org.coolsoftware.requests.resource.request.IRequestResourceProvider resourceProvider, org.coolsoftware.requests.resource.request.ui.IRequestBracketHandlerProvider bracketHandlerProvider) {
		this.resourceProvider = resourceProvider;
		this.bracketHandlerProvider = bracketHandlerProvider;
	}
	
	public org.eclipse.jface.text.contentassist.ICompletionProposal[] computeCompletionProposals(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		org.coolsoftware.requests.resource.request.IRequestTextResource textResource = resourceProvider.getResource();
		String content = viewer.getDocument().get();
		org.coolsoftware.requests.resource.request.ui.RequestCodeCompletionHelper helper = new org.coolsoftware.requests.resource.request.ui.RequestCodeCompletionHelper();
		org.coolsoftware.requests.resource.request.ui.RequestCompletionProposal[] computedProposals = helper.computeCompletionProposals(textResource, content, offset);
		
		// call completion proposal post processor to allow for customizing the proposals
		org.coolsoftware.requests.resource.request.ui.RequestProposalPostProcessor proposalPostProcessor = new org.coolsoftware.requests.resource.request.ui.RequestProposalPostProcessor();
		java.util.List<org.coolsoftware.requests.resource.request.ui.RequestCompletionProposal> computedProposalList = java.util.Arrays.asList(computedProposals);
		java.util.List<org.coolsoftware.requests.resource.request.ui.RequestCompletionProposal> extendedProposalList = proposalPostProcessor.process(computedProposalList);
		if (extendedProposalList == null) {
			extendedProposalList = java.util.Collections.emptyList();
		}
		java.util.List<org.coolsoftware.requests.resource.request.ui.RequestCompletionProposal> finalProposalList = new java.util.ArrayList<org.coolsoftware.requests.resource.request.ui.RequestCompletionProposal>();
		for (org.coolsoftware.requests.resource.request.ui.RequestCompletionProposal proposal : extendedProposalList) {
			if (proposal.getMatchesPrefix()) {
				finalProposalList.add(proposal);
			}
		}
		org.eclipse.jface.text.contentassist.ICompletionProposal[] result = new org.eclipse.jface.text.contentassist.ICompletionProposal[finalProposalList.size()];
		int i = 0;
		for (org.coolsoftware.requests.resource.request.ui.RequestCompletionProposal proposal : finalProposalList) {
			String proposalString = proposal.getInsertString();
			String displayString = proposal.getDisplayString();
			String prefix = proposal.getPrefix();
			org.eclipse.swt.graphics.Image image = proposal.getImage();
			org.eclipse.jface.text.contentassist.IContextInformation info;
			info = new org.eclipse.jface.text.contentassist.ContextInformation(image, proposalString, proposalString);
			int begin = offset - prefix.length();
			int replacementLength = prefix.length();
			// if a closing bracket was automatically inserted right before, we enlarge the
			// replacement length in order to overwrite the bracket.
			org.coolsoftware.requests.resource.request.ui.IRequestBracketHandler bracketHandler = bracketHandlerProvider.getBracketHandler();
			String closingBracket = bracketHandler.getClosingBracket();
			if (bracketHandler.addedClosingBracket() && proposalString.endsWith(closingBracket)) {
				replacementLength += closingBracket.length();
			}
			result[i++] = new org.eclipse.jface.text.contentassist.CompletionProposal(proposalString, begin, replacementLength, proposalString.length(), image, displayString, info, proposalString);
		}
		return result;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformation[] computeContextInformation(org.eclipse.jface.text.ITextViewer viewer, int offset) {
		return null;
	}
	
	public char[] getCompletionProposalAutoActivationCharacters() {
		return null;
	}
	
	public char[] getContextInformationAutoActivationCharacters() {
		return null;
	}
	
	public org.eclipse.jface.text.contentassist.IContextInformationValidator getContextInformationValidator() {
		return null;
	}
	
	public String getErrorMessage() {
		return null;
	}
}
