/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videos;

import java.io.File;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representation of a video used by the {@link VideoProvider}.
 * @author René Schöne
 */
public class Video implements Serializable {
	private static final long serialVersionUID = 7080964538206433080L;

	/** <b>&lt;name&gt;</b>_<b>&lt;resX&gt</b>x<b>&lt;resY&gt;</b>.<b>&lt;extension&gt;</b>*/
	public static transient final Pattern pattern = Pattern.compile("(.*)_(\\d*)x(\\d*)\\.(.*)");
	
	/** Full file name */
	public final String fileName;
	/** Short video name */
	public final String videoName;
	/** Wdith in pixel */
	public final int resX;
	/** Height in pixel */
	public final int resY;
	/** Length in seconds */
	public final int length;
	/** Extension of the reference video*/
	public final String extension;
	/** File object to get video content */
	public final File file;
	
	public Video(File src, int length) {
		this.file = src.getAbsoluteFile();
		this.fileName = file.getPath();
		this.length = length;
		Matcher m = pattern.matcher(file.getName());
		if(m.matches()) {
			this.videoName = m.group(1);
			this.resX = Integer.parseInt(m.group(2));
			this.resY = Integer.parseInt(m.group(3));
			this.extension = m.group(4);
		}
		else {
			throw new Error(fileName + " did not match the pattern.");
		}
	}
	
	public Video(File src, int length, int resX, int resY, String extension) {
		this.file = src.getAbsoluteFile();
		this.fileName =  file.getPath();
		this.videoName = file.getName();
		this.length = length;
		this.resX = resX;
		this.resY = resY;
		this.extension = extension;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Video [videoName=").append(videoName)
				.append(", resX=").append(resX).append(", resY=")
				.append(resY).append(", length=").append(length)
				.append(", extension=").append(extension).append("]");
		return builder.toString();
	}
}