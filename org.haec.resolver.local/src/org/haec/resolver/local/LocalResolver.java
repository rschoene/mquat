/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.resolver.local;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.ResourceResolver;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;

/**
 * Simple resolver using standard linux commands (find, xargs, head) to find the smallest matching file.
 * @author René Schöne
 */
public class LocalResolver implements ResourceResolver {
	
	private TheatreSettings settings;
	private ServiceRegistration<?> reg;

	@Override
	public List<FileProxy> resolve(String name) {
		try {
			return Arrays.asList(new FileProxy(getFile(name), URI.create(RemoteOsgiUtil.getLemIp())));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private File getFile(String regex) throws IOException {
		String[] cmdarray = new String[]{"/bin/sh", "-c", "find " + settings.getResourceDir().get() +
				" -name '*" + regex + "*'" +
				" | xargs ls -Sr | head -n 1"};
		System.out.println("Running " + StringUtils.join(" ", cmdarray));
		InputStream is = Runtime.getRuntime().exec(cmdarray)
				.getInputStream();
		return new File(FileUtils.readAsString(is).trim());
	}
	
	protected void setTheatreSettings(TheatreSettings vs) {
		this.settings = vs;
	}

	protected void unsetTheatreSettings(TheatreSettings vs) {
		this.settings = null;
	}
	
	protected void activate(ComponentContext ctx) {
		reg = RemoteOsgiUtil.register(ctx.getBundleContext(), this, ResourceResolver.class);
	}
	
	protected void deactivate(ComponentContext ctx) {
		if(reg != null) {
			reg.unregister();
		}
	}
	
}

