/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.transcoder.ffmpeg;

import java.io.File;
import java.util.List;

import org.haec.app.transcoder.AbstractTranscoder;
import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.apps.util.ErrorState;
import org.haec.ffmpeg.FfmpegUtil;
import org.haec.theatre.api.Benchmark;

/**
 * Implementation of org.haec.app.transcoder.ffmpeg.TranscoderFfmpeg
 * @author René Schöne
 */
public class TranscoderFfmpeg extends AbstractTranscoder implements TranscoderTranscode {
	
	public TranscoderFfmpeg() {
		super(FfmpegUtil.sharedInstance);
	}

	@Override
	protected int handleProcess(Process startedProcess)
			throws InterruptedException {
		return util.waitForever(startedProcess, true);
	}

	@Override
	protected void addArguments(List<String> params, File fileVideo,
			String outputVideoCodec, String outputAudioCodec, File out, ErrorState ignore) {
		params.add("-y");
		params.add("-i");
		params.add(fileVideo.getAbsolutePath());
		
		// additional parameters. start with index 3.
		if(outputVideoCodec != null) {
			handleVideoCodec(params, outputVideoCodec);
		}
		if(outputAudioCodec != null) {
			handleAudioCodec(params, outputAudioCodec);
		}
//		if(outputFormat.equalsIgnoreCase("flv")) {
//			params.add("-ar");
//			params.add("44100");
//		}
		params.add(out.getAbsolutePath());
	}

	private void handleVideoCodec(List<String> params, String outputVideoCodec) {
		switch(outputVideoCodec) {
		case "h264": outputVideoCodec = "libx264"; break;
		case "theora": outputVideoCodec = "libtheora"; break;
		}
		params.add("-vcodec");
		params.add(outputVideoCodec);
	}

	private void handleAudioCodec(List<String> params, String outputAudioCodec) {
		switch(outputAudioCodec) {
		case "mp3":
			outputAudioCodec = "libmp3lame";
		case "aac":
			params.add("-strict");
			params.add("-2");
			break;
		case "ac3":
			params.add("-ab");
			params.add(Integer.toString(defaultAudioBitrate));
			break;
		}
		params.add("-acodec");
		params.add(outputAudioCodec);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.transcoder.ffmpeg.TranscoderFfmpeg";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new TranscoderFfmpegBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.transcoder.ffmpeg";
	}

}
