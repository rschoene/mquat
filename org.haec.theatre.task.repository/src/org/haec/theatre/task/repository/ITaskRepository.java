/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.task.repository;

import org.coolsoftware.theatre.Resettable;
import org.haec.theatre.task.ITask;

/**
 * The repository storing information for tasks and their jobs to be run.
 * @author René Schöne
 */
public interface ITaskRepository extends Resettable {

	/**
	 * @param taskId the identifier of the task
	 * @return the task with the given identifier, or <code>null</code> of none
	 */
	public abstract ITask getTask(long taskId);

	/**
	 * Creates a new task with the next available id.
	 * @return the task in serialized form
	 */
	public abstract ITask createNewTask();
	
	/**
	 * Creates a new task with the given id.
	 * @return the task in serialized form
	 */
	public abstract ITask createNewTask(long taskId);
	
	/**
	 * Overrides the task with the given id with the task described by its serialized form.
	 * The new task should have the same identifier as the old task.
	 * @param taskId the identifier of the task to override
	 * @param serializedTaskDescription the serialized form of the new task
	 */
	public abstract void updateTask(long taskId, ITask serializedTaskDescription);

}
