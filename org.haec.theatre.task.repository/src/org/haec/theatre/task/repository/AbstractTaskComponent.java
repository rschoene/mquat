/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.task.repository;

import java.io.File;
import java.net.URI;

import org.apache.log4j.Logger;
import org.haec.theatre.task.ITask;
import org.haec.theatre.task.TaskImpl;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

/**
 * Superclass for all components requiring other components.
 * Allows the LEM to set the taskId and allows the component to fetch the Task from
 * the repository.
 * @author René Schöne
 */
public class AbstractTaskComponent {
	
	protected long taskId;
	protected String appName;

	private static Logger log = Logger.getLogger(AbstractTaskComponent.class);;
	private transient boolean useStub;
	//TODO get real thisUri from caller
	protected URI thisUri;

	public AbstractTaskComponent(String appName) {
		this.appName = appName;
		this.useStub = false;
		try {
			thisUri = URI.create(RemoteOsgiUtil.getLemIp());
		} catch (Exception e) {
			log.warn("Can not initialize thisUri. Using localhost fallback.", e);
			thisUri = URI.create("127.0.0.1");
		}
	}
	
	/** Sets the task id for this component. */
	public void setTaskId(long taskId) {
		this.taskId = taskId;
		log.debug(taskId);
	}
	
	/** Gets the task id of this component. */
	public long getTaskId() {
		return this.taskId;
	}
	
	public String getAppName() {
		return appName;
	}

	protected ITask getTask() {
		if(this.useStub) {
			log.debug("Returning stub task.");
			// create a stub task-descriptor and return it
			ITask task = new TaskImpl() {
				@Override
				public <S> S execute(Class<S> clazz, String appName, String compName,
						String portName, Object... params) {
					return getStubResultFor(clazz, appName, compName, portName, params);
				}
			};
			return task;
		}
		// connect to task repository
		BundleContext bundleContext = FrameworkUtil.getBundle(AbstractTaskComponent.class).getBundleContext();
		ServiceReference<ITaskRepository> ref = bundleContext.getServiceReference(ITaskRepository.class);
		if(ref == null) {
			log.warn("TaskRepository was not found. Returning null.");
			return null;
		}
		ITaskRepository repo = bundleContext.getService(ref);
		// fetch task in serialized form with this' id
		log.debug("TaskRepo fetched");
		ITask task = repo.getTask(getTaskId());
		log.debug("Task fetched, id=" + task.getTaskId());
		return task;
	}
	
	
	/**
	 * @see #getStubResultFor(String, String, String, Object...)
	 */
	public void setUseStub(boolean useStub) {
		this.useStub = useStub;
	}
	
	/**
	 * This method is intended to be overriden within Benchmarks to provide suitable stubs.
	 * <b>Remark:</b> {@link #setUseStub(boolean) setUseStub(true)} has to be called before.
	 * Every call from {@link #getTask()}.execute() will be redirected to this method then.
	 * @param appName the name of the app
	 * @param compName the name of the called component
	 * @param portName the name of the called port
	 * @param params the params passed to the port
	 * @return the result. Defaults to <code>null</code>
	 */
	protected <S> S getStubResultFor(Class<S> clazz, String appName, String compName, String portName, Object... params) {
		return null;
	}

	protected <S> S invoke(Class<S> clazz, String compName, String portName,
			Object... params) {
		log.debug("compName="+compName+", portName="+portName);
		ITask task = getTask();
		printMappings(task);
		return task.execute(clazz, appName, compName, portName, params);
	}

	private ITask printMappings(ITask task) {
		if(task == null) {
			log.warn("Task is null");
			return task;
		}
		long id = task.getTaskId();
		log.debug("Task with id="+id);
		for(String appName : task.getAppNames()) {
			for(String compName : task.getComponentNames(appName)) {
				log.debug(String.format(" App: %s | Comp: %s | Impl: %s | Container: %s",
						appName, compName, task.getImplName(appName, compName),
						task.getContainer(appName, compName)));
			}
		}
		return task;
	}
	
	/**
	 * Wraps the given file into a proxy using {@link #thisUri} as URI.
	 * @param f the given file to wrap
	 * @return the new FileProxy
	 */
	protected FileProxy wrap(File f) {
		return new FileProxy(f, thisUri);
	}

}
