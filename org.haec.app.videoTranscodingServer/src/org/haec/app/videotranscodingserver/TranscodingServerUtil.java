/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver;

import java.util.Arrays;
import java.util.List;

/**
 * Utility class for org.haec.app.videoTranscodingServer
 * @author René Schöne
 */
public class TranscodingServerUtil {
	
	public static final String APP_NAME = "org.haec.app.videoTranscodingServer";
	private static final List<String> vCodecs = Arrays.asList("h264", "theora", "mpeg4");
	private static final List<String> aCodecs = Arrays.asList("mp3", "ac3", "aac");
	/** Should components throw exceptions if return-code is not zero? */
	public static final String FAIL_FAST = "vts.failfast";
	public static final String MAX_BENCH_SCALING = "vts.benchmark.max.scaling";
	public static final String MAX_BENCH_TRANSCODING = "vts.benchmark.max.transcoding";
	
	public static int getMetaparameterValue(int duration, int resX, int resY) {
		return duration * resX * resY;
	}
	
	public static int getMetaparameterValue(int duration1, int duration2, int resX1, int resY1, int resX2, int resY2) {
		return Math.max(duration1, duration2) * Math.max(resX1 * resY1, resX2 * resY2);
	}

	/** @return ["theora", "h264", "mpeg4"] */
	public static List<String> getSupportedVideoCodecs() {
		return vCodecs;
	}

	/** @return ["mp3", "ac3", "aac"] */
	public static List<String> getSupportedAudioCodecs() {
		return aCodecs;
	}

}
