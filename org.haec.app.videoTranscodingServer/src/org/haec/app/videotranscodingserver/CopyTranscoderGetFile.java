/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver;

import org.haec.theatre.utils.FileProxy;

/**
 * AppInterface of CopyTranscoderFile.getFile
 * @author René Schöne
 */
public interface CopyTranscoderGetFile {
	
	/**
	 * Takes an unresolved FileProxy and returns a resolved one. 
	 * @param fp1 the unresolved FileProxy with only {@link FileProxy#getName() name} set.
	 * @return the resolved FileProxy whose {@link FileProxy#getFileFor(java.net.URI) getFileFor} can be invoked
	 */
	public FileProxy getFile(Object fp1);

}
