/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver;

import java.io.File;

/**
 * AppInterface of Controller.pipScaled
 * @author René Schöne
 */
public interface ControllerPipScaled {

	/**
	 * Scale the second video down using the parameters w2, h2 and mult.
	 * After that, run picture-in-picture using the first video and the scaled second video
	 * by using the parameter pos.
	 * @param fileVideo1 the first video, used as a basis
	 * @param fileVideo2 the second video, which will be scaled and overlayed
	 * @param w2 the width parameter (absolute or factor)
	 * @param h2 the height parameter (absolute or factor)
	 * @param mult if <code>true</code>, w2 and h2 are factors (positive to multiply, negative to divide),
	 *  otherwise they are absolute values
	 * @param pos the position argument, see the constants in {@link PipDoPip}
	 * @return the resulting file
	 */
	public File pipScaled(File fileVideo1, File fileVideo2, int w2, int h2, boolean mult,
			int pos);
}
