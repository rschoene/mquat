/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver;

import org.haec.theatre.api.App;

/**
 * Video scaling and transcoding application.
 * @author René Schöne
 */
public class VideoServerApp implements App {

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getName()
	 */
	@Override
	public String getName() {
		return TranscodingServerUtil.APP_NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getApplicationModelPath()
	 */
	@Override
	public String getApplicationModelPath() {
		return "models/VideoTranscoding.structure";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getContractsPath()
	 */
	@Override
	public String getContractsPath() {
		return "models/contracts";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getBundleSymbolicName()
	 */
	@Override
	public String getBundleSymbolicName() {
		return "org.haec.app.videoTranscodingServer";
	}

}
