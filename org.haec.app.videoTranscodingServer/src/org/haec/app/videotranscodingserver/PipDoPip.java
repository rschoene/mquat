/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver;

import java.io.File;

/**
 * AppInterface of PiP.doPip
 * @author René Schöne
 */
public interface PipDoPip { //TODO maybe copy it for controller interface (as controller is using pip)
	
	public static final int POS_TOP_LEFT_CORNER  =		1;
	public static final int POS_TOP_RIGHT_CORNER =		2;
	public static final int POS_BOTTOM_RIGHT_CORNER =	3;
	public static final int POS_BOTTOM_LEFT_CORNER =	4;
	public static final int POS_CENTER =				5;

	/**
	 * Execute the picture-in-picture command
	 * @param in1 the base video
	 * @param in2 the video to overlay
	 * @param pos the position of the video (use constants of pip)
	 * @return the output file
	 */
	public File doPip(File in1, File in2, int pos);

}
