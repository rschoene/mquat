/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver;

import org.haec.theatre.utils.FileProxy;

/**
 * AppInterface of Transcoder.transcode
 * @author René Schöne
 */
public interface TranscoderTranscode {

	/**
	 * Transcodes the given input video into an video with the given output format.
	 * @param in the video to use as input
	 * @param outputVideoCodec the new video codec, may be <code>null</code> if outputAudioCodec is not
	 * @param outputAudioCodec the new audio codec, may be <code>null</code> if outputVideoCodec is not
	 * @return the output video in the given format
	 */
	public FileProxy transcode(Object in, String outputVideoCodec, String outputAudioCodec);

}
