/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver;

import org.haec.theatre.utils.FileProxy;

/**
 * AppInterface of Scaling.scale
 * @author René Schöne
 */
public interface ScalingScale {

	/**
	 * Scales the video either to the given resolution w:h, if mult is false
	 *  or scales by the factors w and h, if mult is true. Relative downscaling
	 *  can be achieved setting w and/or h to negative numbers. For example,
	 *  the new width will then be <code>out_width = input_width / w</code>.
	 * @param in the input video
	 * @param w either absolute width or scaling factor for width
	 * @param h either absolute height or scaling factor for height
	 * @param mult determines whether to interpret values as absolute or relative
	 * @return the scaled video
	 */
	FileProxy scale(Object in, int w, int h, boolean mult);

}