import ccm [../Server.structure]
import ccm [../VideoTranscoding.structure]

contract org_haec_app_pip_ffmpeg_PipFfmpeg implements software PiP.doPip {
	mode pipFfmpeg1 {
		requires resource CPU {
			frequency min: 252
			cpu_time <f_cpu_time(max_video_length)>
			energyRate: 5.2}
		requires resource RAM {free min: 52 energyRate: 5.2}
		provides run_time <f_rtime_fast(max_video_length)>
	}
}
