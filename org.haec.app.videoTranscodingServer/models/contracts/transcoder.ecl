import ccm [../Server.structure]
import ccm [../VideoTranscoding.structure]

contract org_haec_app_transcoder_ffmpeg_TranscoderFfmpeg implements software Transcoder.transcode {
	mode transcoderFfmpeg1 {
		requires resource CPU {
			frequency min: 500
			cpu_time <f_cpu_time(video_length)>
			energyRate: 5.0}
		requires resource RAM {free min: 50 energyRate: 5.0}
		requires component CopyTranscoderFile1 { }
		provides run_time <f(max_video_length)>
	}
}

contract org_haec_app_transcoder_mencoder_TranscoderMencoder implements software Transcoder.transcode {
	mode transcoderMencoder1 {
		requires resource CPU {
			frequency min: 500
			cpu_time <f_cpu_time(video_length)>
			energyRate: 5.0}
		requires resource RAM {free min: 50 energyRate: 5.0}
		requires component CopyTranscoderFile1 { }
		provides run_time <f(max_video_length)>
	}
}

contract org_haec_app_transcoder_handbrake_TranscoderHandbrake implements software Transcoder.transcode {
	mode transcoderHandbrake1 {
		requires resource CPU {
			frequency min: 500
			cpu_time <f_cpu_time(video_length)>
			energyRate: 5.0}
		requires resource RAM {free min: 50 energyRate: 5.0}
		requires component CopyTranscoderFile1 { }
		provides run_time <f(max_video_length)>
	}
}

contract org_haec_app_transcoder_noop_TranscoderNoop implements software Transcoder.transcode {
	mode transcoderNoop1 {
		requires resource CPU { 
			frequency min: 500
			energyRate:  0.1
		}
		requires component CopyTranscoderFile1 { }
		provides utility max: 0.1
		provides run_time max: 0.01
	}
}
