import ccm [../Server.structure]
import ccm [../VideoTranscoding.structure]

contract org_haec_app_censor_ffmpeg_CensorFfmpeg implements software Censor.censor {
	mode censorFfmpeg1 {
		requires resource CPU {
			frequency min: 501
			cpu_time <f_cpu_time(max_video_length)>
			energyRate: 5.1}
		requires resource RAM {free min: 51 energyRate: 5.1}
		provides run_time <f_rtime(max_video_length)>
	}
}
