import ccm [../Server.structure]
import ccm [../VideoTranscoding.structure]

contract org_haec_app_controller_delegating_DelegatingController implements software Controller.pipScaled {
	mode pipScaledModeDelegating {
		requires component Scaling { }
		requires component PiP { }
		provides run_time <f_rtime(max_video_length)>
	}
}
