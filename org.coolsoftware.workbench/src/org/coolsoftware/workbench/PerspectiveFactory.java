package org.coolsoftware.workbench;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class PerspectiveFactory implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		
		String editorArea = layout.getEditorArea();
		
		 IFolderLayout left =
			 layout.createFolder("left", IPageLayout.LEFT, (float) 0.26, editorArea);
		 
		 IFolderLayout lefttop =
			 layout.createFolder("lefttop", IPageLayout.TOP, 0.6f, "left");
		 lefttop.addView(IPageLayout.ID_RES_NAV);
		 
		 IFolderLayout leftbottom =
			 layout.createFolder("leftbottom", IPageLayout.BOTTOM, 0f, "left");
		 //leftbottom.addView(IPageLayout.ID_OUTLINE);
		 leftbottom.addView(IPageLayout.ID_PROP_SHEET);
		 
		 IFolderLayout right =
			 layout.createFolder("right", IPageLayout.RIGHT, (float) 0.66, editorArea);
//		 right.addView("org.cool_software.simulation.workload.resource.workload.views.SimulationResultView");
		 right.addView("org.coolsoftware.simulation.workload.resource.workload.views.SimulationResultView");

		 IFolderLayout bottom =
			 layout.createFolder("bottom", IPageLayout.BOTTOM, (float) 0.48, editorArea);
//		 bottom.addView("org.cool_software.simulation.workload.resource.workload.views.EnergyView");
		 bottom.addView("org.coolsoftware.simulation.workload.resource.workload.views.EnergyView");
	}

}
