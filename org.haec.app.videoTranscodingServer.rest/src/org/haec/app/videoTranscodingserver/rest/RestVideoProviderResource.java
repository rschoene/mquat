package org.haec.app.videoTranscodingserver.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.haec.videoprovider.IVideoProviderFactory;
import org.haec.videoprovider.VideoProvider;
import org.haec.videos.Video;

public class RestVideoProviderResource {

	private IVideoProviderFactory fac;
	
	@Path("videos")
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public Response getVideoNames(@QueryParam("length") int length, @QueryParam("hostname") String hostname) {
		VideoProvider vp = fac.getCurrentVideoProvider();
		if(vp == null) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("no provider found!").build();
		}
		List<Video> list = vp.getVideos(length, hostname);
		if(list == null) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("list is null!").build();
		}
		StringBuilder sb = new StringBuilder();
		for(Video v : list) {
			sb.append(v.fileName).append(",");
		}
		return Response.ok(sb.toString()).build();
	}

	protected void setVideoProviderFactory(IVideoProviderFactory fac) {
		this.fac = fac;
	}

}
