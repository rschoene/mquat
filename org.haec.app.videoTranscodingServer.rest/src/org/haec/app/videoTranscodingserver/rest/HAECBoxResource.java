/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videoTranscodingserver.rest;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.rest.GemCallOptimizeResource;
import org.haec.theatre.rest.constants.PathConstants;
import org.haec.theatre.rest.util.TaskIdInfo;
import org.haec.theatre.utils.CollectionsUtil;

/**
 * Special resource for the HAEC-Box prototype.
 * @author René Schöne
 */
@Path("haec-box")
public class HAECBoxResource extends GemCallOptimizeResource {
	
	private Logger log = Logger.getLogger(HAECBoxResource.class);
	
	static final String TRANSCODER_PORT = "transcode";
	static final String TRANSCODER_COMPONENT = "Transcoder";
	static final String SCALING_PORT = "scale";
	static final String SCALING_COMPONENT = "Scaling";
	public static final String appName = TranscodingServerUtil.APP_NAME;
	public static final String KEY_MAX_RESPONSE_TIME = "maxResponseTime";
	public static final String KEY_OLD_WIDTH = "oldWidth";
	public static final String KEY_OLD_HEIGHT = "oldHeight";
	public static final String KEY_OLD_LENGTH = "oldLength";
	public static final String KEY_OLD_WIDTH2 = "oldWidth2";
	public static final String KEY_OLD_HEIGHT2 = "oldHeight2";
	public static final String KEY_OLD_LENGTH2 = "oldLength2";
	public static final String KEY_NEW_WIDTH = "newWidth";
	public static final String KEY_NEW_HEIGHT = "newHeight";
	public static final String KEY_NEW_VIDEO_CODEC = "newVideoCodec";
	public static final String KEY_NEW_AUDIO_CODEC = "newAudioCodec";
	public static final String KEY_NEW_CONTAINER = "newContainer";
	public static final String KEY_POS = "pos";
	public static final String KEY_NAME = "name";
	public static final String KEY_NAME2 = "name2";
	/** @see IGlobalEnergyManager#SETTINGS_PROFILE */
	public static final String KEY_PROFILE = "profile";
	
	private static final String NO_VALUE = "-1";
	public static final int INT_NO_VALUE = -1;
	
	static final List<String> responseTime = Arrays.asList("response_time");
	static final List<String> max = Arrays.asList("max");
	static final List<String> mvl = Arrays.asList("max_video_length");
	public static final String DEFER_TS_REQUEST = "haec-box-transcode-scale-defer";
	
	private Response fire(String request, Object[] userParams) {
		log.debug("request=" + request + ", params=" + Arrays.toString(userParams));
		IGlobalEnergyManager gem = getGem();
	
		// call optimize method
		Serializable s;
		try {
			// only optimize first pass
			s = gem.run(request, true, true, userParams);
		}
		catch(Exception e) {
			s = null;
		}
		if(s == null) {
			Response r = Response.status(Status.INTERNAL_SERVER_ERROR).entity("Got null as result.").build();
			log.debug(loggingContent(r));
			return r;
		}
		long taskId = (Long) s;
		return Response.ok(new TaskIdInfo(taskId),
				MediaType.APPLICATION_JSON).build();
	}

	static List<String> makeMvl(int oldLength, int oldWidth, int oldHeight) {
		return Arrays.asList(Integer.toString(TranscodingServerUtil.getMetaparameterValue(
				oldLength, oldWidth, oldHeight)));
	}

	private List<String> makeMvl(int oldLength, int oldWidth, int oldHeight,
			int oldLength2, int oldWidth2, int oldHeight2) {
		return Arrays.asList(Integer.toString(TranscodingServerUtil.getMetaparameterValue(
				oldLength, oldLength2, oldWidth, oldHeight, oldWidth2, oldHeight2)));
	}

	private int posInt(String pos) {
		int posInt;
		switch(pos) {
		case "top-left": posInt = PipDoPip.POS_TOP_LEFT_CORNER; break;
		case "top-right": posInt = PipDoPip.POS_TOP_RIGHT_CORNER; break;
		case "bottom-left": posInt = PipDoPip.POS_BOTTOM_LEFT_CORNER; break;
		case "bottom-right": posInt = PipDoPip.POS_BOTTOM_RIGHT_CORNER; break;
		case "center": //$FALL-THROUGH$
		default: posInt = PipDoPip.POS_CENTER; break;
		}
		return posInt;
	}
	
	static String resource(String copyFileTypeName, String name) {
		return IGlobalEnergyManager.RESOURCE_IDENTIFIER + copyFileTypeName
				+ IGlobalEnergyManager.RESOURCE_TYPE_DELIMITER + name;
	}

	@Path("scale")
	@POST
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response s(
			@DefaultValue("ilp") @QueryParam(PathConstants.KEY_SOLVER) String solver,
			@DefaultValue("80000") @QueryParam(KEY_MAX_RESPONSE_TIME) int maxResponseTime,
			@QueryParam(KEY_OLD_WIDTH) int oldWidth,
			@QueryParam(KEY_OLD_HEIGHT) int oldHeight,
			@QueryParam(KEY_OLD_LENGTH) int oldLength,
			@QueryParam(KEY_NEW_WIDTH) int newWidth,
			@QueryParam(KEY_NEW_HEIGHT) int newHeight,
			@QueryParam(KEY_NAME) String name,
			@Context UriInfo uriInfo
			) {
		String compName = SCALING_COMPONENT;
		String portName = SCALING_PORT;
		MultivaluedMap<String, String> options = uriInfo.getQueryParameters();
		CollectionsUtil.removeAll(options, KEY_OLD_WIDTH, KEY_OLD_HEIGHT, KEY_OLD_LENGTH,
				PathConstants.KEY_SOLVER, KEY_NEW_WIDTH, KEY_NEW_HEIGHT, KEY_NAME);
		log.debug("Remaining options: " + options);
		String request = makeRequest(solver, appName, compName, portName,
				responseTime, max, Arrays.asList(Integer.toString(maxResponseTime)),
				mvl, makeMvl(oldLength, oldWidth, oldHeight),
				options);
		Object[] userParams = new Object[]{resource("CopyScalingFile1",name), newWidth, newHeight, false};
		return fire(request, userParams);
	}

	@Path("transcode")
	@POST
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response t(
			@DefaultValue("ilp") @QueryParam(PathConstants.KEY_SOLVER) String solver,
			@DefaultValue("80000") @QueryParam(KEY_MAX_RESPONSE_TIME) int maxResponseTime,
			@QueryParam(KEY_OLD_WIDTH) int oldWidth,
			@QueryParam(KEY_OLD_HEIGHT) int oldHeight,
			@QueryParam(KEY_OLD_LENGTH) int oldLength,
			@QueryParam(KEY_NEW_VIDEO_CODEC) String outputVideoCodec,
			@QueryParam(KEY_NEW_AUDIO_CODEC) String outputAudioCodec,
			@QueryParam(KEY_NAME) String name,
			@Context UriInfo uriInfo
			) {
		String compName = TRANSCODER_COMPONENT;
		String portName = TRANSCODER_PORT;
		MultivaluedMap<String, String> options = uriInfo.getQueryParameters();
		CollectionsUtil.removeAll(options, KEY_OLD_WIDTH, KEY_OLD_HEIGHT, KEY_OLD_LENGTH,
				PathConstants.KEY_SOLVER, KEY_NEW_VIDEO_CODEC, KEY_NEW_AUDIO_CODEC, KEY_NAME);
		log.debug("Remaining options: " + options);
		String request = makeRequest(solver, appName, compName, portName,
				responseTime, max, Arrays.asList(Integer.toString(maxResponseTime)),
				mvl, makeMvl(oldLength, oldWidth, oldHeight),
				options);
		Object[] userParams = new Object[]{resource("CopyTranscoderFile1", name),
				outputVideoCodec, outputAudioCodec, false};
		return fire(request, userParams);
	}
	
	@Path("pip")
	@POST
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response p(
			@DefaultValue("ilp") @QueryParam(PathConstants.KEY_SOLVER) String solver,
			@DefaultValue("80000") @QueryParam(KEY_MAX_RESPONSE_TIME) int maxResponseTime,
			@QueryParam(KEY_OLD_WIDTH) int oldWidth,
			@QueryParam(KEY_OLD_HEIGHT) int oldHeight,
			@QueryParam(KEY_OLD_LENGTH) int oldLength,
			@QueryParam(KEY_OLD_WIDTH2) int oldWidth2,
			@QueryParam(KEY_OLD_HEIGHT2) int oldHeight2,
			@QueryParam(KEY_OLD_LENGTH2) int oldLength2,
			@QueryParam(KEY_POS) String pos,
			@QueryParam(KEY_NAME) String name,
			@QueryParam(KEY_NAME2) String name2,
			@Context UriInfo uriInfo
			) {
		String compName = "PiP";
		String portName = "doPip";
		MultivaluedMap<String, String> options = uriInfo.getQueryParameters();
		CollectionsUtil.removeAll(options, KEY_OLD_WIDTH, KEY_OLD_HEIGHT, KEY_OLD_LENGTH,
				KEY_OLD_WIDTH2, KEY_OLD_HEIGHT2, KEY_OLD_LENGTH2,
				PathConstants.KEY_SOLVER, KEY_POS, KEY_NAME, KEY_NAME2);
		log.debug("Remaining options: " + options);
		String request = makeRequest(solver, appName, compName, portName,
				responseTime, max, Arrays.asList(Integer.toString(maxResponseTime)),
				mvl, makeMvl(oldLength, oldWidth, oldHeight, oldLength2, oldWidth2, oldHeight2),
				options);
		int posInt = posInt(pos);
		Object[] userParams = new Object[]{resource("CopyPiPFile1", name),
				resource("CopyPiPFile2",name2), posInt, false};
		return fire(request, userParams);
	}

	@Path("pip-scaled")
	@POST
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response pb(
			@DefaultValue("ilp") @QueryParam(PathConstants.KEY_SOLVER) String solver,
			@DefaultValue("80000") @QueryParam(KEY_MAX_RESPONSE_TIME) int maxResponseTime,
			@QueryParam(KEY_OLD_WIDTH) int oldWidth,
			@QueryParam(KEY_OLD_HEIGHT) int oldHeight,
			@QueryParam(KEY_OLD_LENGTH) int oldLength,
			@QueryParam(KEY_OLD_WIDTH2) int oldWidth2,
			@QueryParam(KEY_OLD_HEIGHT2) int oldHeight2,
			@QueryParam(KEY_OLD_LENGTH2) int oldLength2,
			@QueryParam(KEY_NEW_WIDTH) int newWidth,
			@QueryParam(KEY_NEW_HEIGHT) int newHeight,
			@QueryParam(KEY_POS) String pos,
			@QueryParam(KEY_NAME) String name,
			@QueryParam(KEY_NAME2) String name2,
			@Context UriInfo uriInfo
			) {
		String compName = "Controller";
		String portName = "pipScaled";
		MultivaluedMap<String, String> options = uriInfo.getQueryParameters();
		CollectionsUtil.removeAll(options, KEY_OLD_WIDTH, KEY_OLD_HEIGHT, KEY_OLD_LENGTH,
				PathConstants.KEY_SOLVER, KEY_OLD_WIDTH2, KEY_OLD_HEIGHT2, KEY_OLD_LENGTH2,
				KEY_NEW_WIDTH, KEY_NEW_HEIGHT, KEY_POS, KEY_NAME, KEY_NAME2);
		log.debug("Remaining options: " + options);
		String request = makeRequest(solver, appName, compName, portName,
				responseTime, max, Arrays.asList(Integer.toString(maxResponseTime)),
				mvl, makeMvl(oldLength, oldWidth, oldHeight, oldLength2, oldWidth2, oldHeight2),
				options);
		int posInt = posInt(pos);
		Object[] userParams = new Object[]{resource("CopyScalingFile1", name), resource("CopyPiPFile2", name2),
				newWidth, newHeight, false, posInt};
		return fire(request, userParams);
	}
	
	@Path("ts")
	@POST
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response ts(
			@DefaultValue("ilp") @QueryParam(PathConstants.KEY_SOLVER) String solver,
			@DefaultValue("80000") @QueryParam(KEY_MAX_RESPONSE_TIME) int maxResponseTime,
//			@QueryParam(KEY_OLD_WIDTH) int oldWidth,
//			@QueryParam(KEY_OLD_HEIGHT) int oldHeight,
//			@QueryParam(KEY_OLD_LENGTH) int oldLength,
			@DefaultValue(NO_VALUE) @QueryParam(KEY_NEW_WIDTH) int newWidth,
			@DefaultValue(NO_VALUE) @QueryParam(KEY_NEW_HEIGHT) int newHeight,
			@QueryParam(KEY_NEW_VIDEO_CODEC) String outputVideoCodec,
			@QueryParam(KEY_NEW_AUDIO_CODEC) String outputAudioCodec,
			@QueryParam(KEY_NAME) String name,
			@Context UriInfo uriInfo
			) {
		log.debug("entry");
		// we will find need to store the REST-params in the userParams
		Object[] userParams = HAECBoxPreOptimizeRunner.makeUserParams(solver, maxResponseTime, newWidth, newHeight,
				outputVideoCodec, outputAudioCodec, name, uriInfo.getQueryParameters());
		return fire(null, userParams);
	}

}
