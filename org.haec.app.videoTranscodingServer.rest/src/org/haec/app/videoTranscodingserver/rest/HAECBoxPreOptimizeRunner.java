/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videoTranscodingserver.rest;

import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.INT_NO_VALUE;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.KEY_NAME;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.KEY_NEW_AUDIO_CODEC;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.KEY_NEW_HEIGHT;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.KEY_NEW_VIDEO_CODEC;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.KEY_NEW_WIDTH;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.SCALING_COMPONENT;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.SCALING_PORT;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.TRANSCODER_COMPONENT;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.TRANSCODER_PORT;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.appName;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.makeMvl;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.max;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.mvl;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.resource;
import static org.haec.app.videoTranscodingserver.rest.HAECBoxResource.responseTime;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.util.PreOptimizeRunner;
import org.haec.theatre.rest.GemCallOptimizeResource;
import org.haec.theatre.rest.constants.PathConstants;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.videoplatform.resolver.IVideoPlatformResolver;
import org.haec.videoplatform.resolver.Video;

/**
 * PreOptimizeRunner to be run before transcode-scale requests (to decide which component to execute)
 * @author René Schöne
 */
public class HAECBoxPreOptimizeRunner extends GemCallOptimizeResource implements PreOptimizeRunner {
	
	private static Logger log = Logger.getLogger(HAECBoxPreOptimizeRunner.class);
	private IVideoPlatformResolver vpResolver;

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.util.PreOptimizeRunner#update(org.coolsoftware.theatre.energymanager.util.PreOptimizeRunner.RequestAndParams)
	 */
	@Override
	public RequestAndParams update(RequestAndParams rap) {
		if(rap.userRequest != null && !rap.userRequest.equals(HAECBoxResource.DEFER_TS_REQUEST)) { return rap; }
		// looks like a good candidate
		// we will find the REST-params in the userParams
		String solver = getSolver(rap.params);
		int maxResponseTime = getMaxResponseTime(rap.params);
		int newWidth = getNewWidth(rap.params);
		int newHeight = getNewHeight(rap.params);
		String outputVideoCodec = getOutputVideoCodec(rap.params);
		String outputAudioCodec = getOutputAudioCodec(rap.params);
		String name = getName(rap.params);
		MultivaluedMap<String, String> options = getOptions(rap.params);
		
		// VERY simple. If resolution matches, do transcoding, else do scaling
		String compName;
		String portName;
		List<Video> vList = vpResolver.getVideoList(name);
		// check for existing transcodates
		Video videoToUse = null;
		Boolean transcode = null;
		for(Video v : vList) {
			if(v.hosts == null || v.hosts.length == 0) { continue; }
			if(!v.variant.isEmpty()) {
				if(matchingRes(v, newWidth, newHeight)) {
					videoToUse = v;
					transcode = Boolean.TRUE;
					break;
				} else if(matchingCodec(v, outputVideoCodec, outputAudioCodec)) {
					videoToUse = v;
					transcode = Boolean.FALSE;
					break;
				}
			}
			else if(videoToUse == null) {
				 /*default: use first non-variant video*/
				videoToUse = v;
				if(matchingRes(v, newWidth, newHeight)) {
					transcode = Boolean.TRUE;
				} else if(matchingCodec(v, outputVideoCodec, outputAudioCodec)) {
					transcode = Boolean.FALSE;
				}
			}
		}
		if(videoToUse == null) {
			rap.errorMessage = "No video for name " + name;
			return rap;
		}
		if(transcode == null) {
			/*default: do just scaling*/
			log.warn("No choice for Scaling vs. Transcoding. Using Scaling.");
			transcode = Boolean.FALSE;
		}
		log.info("Using video " + videoToUse + " and " + (transcode ? "Transcoding" : "Scaling"));
		if(transcode) {
			compName = TRANSCODER_COMPONENT;
			portName = TRANSCODER_PORT;
			rap.params = new Object[]{resource("CopyTranscoderFile1", name),
					outputVideoCodec, outputAudioCodec};
		}
		else {
			compName = SCALING_COMPONENT;
			portName = SCALING_PORT;
			rap.params = new Object[]{resource("CopyScalingFile1",name), newWidth, newHeight, false};
		}
		CollectionsUtil.removeAll(options, //KEY_OLD_WIDTH, KEY_OLD_HEIGHT, KEY_OLD_LENGTH,
				PathConstants.KEY_SOLVER, KEY_NEW_WIDTH, KEY_NEW_HEIGHT,
				KEY_NEW_VIDEO_CODEC, KEY_NEW_AUDIO_CODEC, KEY_NAME);
		log.debug("Remaining options: " + options);
		rap.userRequest = makeRequest(solver, appName, compName, portName,
				responseTime, max, Arrays.asList(Integer.toString(maxResponseTime)),
				mvl, makeMvl((int) videoToUse.duration, videoToUse.width, videoToUse.height),
				options);
		return rap;
	}

	/** @return the user params as an object array */
	public static Object[] makeUserParams(String solver, int maxResponseTime, int newWidth, int newHeight,
			String outputVideoCodec, String outputAudioCodec, String name, MultivaluedMap<String, String> options) {
		return new Object[]{solver, maxResponseTime, newWidth, newHeight,
				outputVideoCodec, outputAudioCodec, name, options};
	}
	
	private static String getSolver(Object[] params) { return (String) params[0]; }
	private static int getMaxResponseTime(Object[] params) { return (int) params[1]; }
	private static int getNewWidth(Object[] params) { return (int) params[2]; }
	private static int getNewHeight(Object[] params) { return (int) params[3]; }
	private static String getOutputAudioCodec(Object[] params) { return (String) params[4]; }
	private static String getOutputVideoCodec(Object[] params) { return (String) params[5]; }
	private static String getName(Object[] params) { return (String) params[6]; }
	@SuppressWarnings("unchecked")
	private static MultivaluedMap<String, String> getOptions(Object[] params) {
		return (MultivaluedMap<String, String>) params[7];
	}

	private boolean matchingRes(Video v, int newWidth, int newHeight) {
		return (newWidth == INT_NO_VALUE || newWidth == v.width) &&
				(newHeight == INT_NO_VALUE || newHeight == v.height);
	}

	private boolean matchingCodec(Video v, String outputVideoCodec,
			String outputAudioCodec) {
		return (outputVideoCodec == null || v.video_codec.equals(outputVideoCodec))
				&& (outputAudioCodec == null || v.audio_codec.equals(outputAudioCodec));
	}
	
	protected void setVideoPlatformResolver(IVideoPlatformResolver vpr) {
		this.vpResolver = vpr;
	}

}
