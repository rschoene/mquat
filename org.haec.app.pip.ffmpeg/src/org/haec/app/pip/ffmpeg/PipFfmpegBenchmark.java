/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.pip.ffmpeg;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;
import org.haec.videos.Video;
import org.haec.videoprovider.IVideoProviderFactory;

class PipData implements BenchmarkData {
	File in1;
	File in2;
	int mvl;
	
	public PipData(File in1, File in2, int mvl) {
		super();
		this.in1 = in1;
		this.in2 = in2;
		this.mvl = mvl;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mvl;
	}
	
}

/**
 * Benchmark of org.haec.app.pip.ffmpeg.PipFfmpeg
 * @author René Schöne
 */
public class PipFfmpegBenchmark implements Benchmark {
	
	private PipFfmpeg comp = new PipFfmpeg();
	private Logger log = Logger.getLogger(PipFfmpegBenchmark.class);
	private IVideoProviderFactory fac;
	public PipFfmpegBenchmark(IVideoProviderFactory fac) {
		super();
		this.fac = fac;
	}

	int pos;

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		log.info("[PIP Bench] Starting");
		List<BenchmarkData> result = new ArrayList<BenchmarkData>();
		pos = PipDoPip.POS_CENTER;
		for(Video[] vids : fac.getCurrentVideoProvider().getTwoVideos(true, true, false, true)) {
			Video video1 = vids[0], video2 = vids[1];
			// metaparameters: maxVideoLength
			int mpValue = TranscodingServerUtil.getMetaparameterValue(video1.length, video2.length,
					video1.resX, video1.resY, video2.resX, video2.resY);
			result.add(new PipData(video1.file, video2.file, mpValue));
		}
		log.info("[PIP Bench] Finished");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		PipData d = (PipData) data;
		log.debug("in1 = " + d.in1.getName() + ", in2 = " + d.in2.getName());
		return comp.doPip(d.in1, d.in2, pos);
	}

}
