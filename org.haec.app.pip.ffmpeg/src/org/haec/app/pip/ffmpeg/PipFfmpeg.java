/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.pip.ffmpeg;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.ffmpeg.FfmpegUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.utils.FileUtils;
import org.haec.videoprovider.IVideoProviderFactory;

/**
 * Implementation of org.haec.app.pip.ffmpeg.PipFfmpeg
 * @author René Schöne
 */
public class PipFfmpeg extends TaskBasedImpl implements PipDoPip {

	private static final Object FLV = "flv";
	private static final boolean isWindows = System.getProperty("os.name").contains("Windows");
	private static Logger log = Logger.getLogger(PipFfmpeg.class);
	private IVideoProviderFactory fac;
	
	public File doPip(File fileVideo1, File fileVideo2, int pos) {
		if(FfmpegUtil.sharedInstance.shouldPass()) {
			log.debug("Pass. Returning " + fileVideo1);
			return fileVideo1;
		}
		// TODO check Implementation of doPip
		// use "-benchmark" argument to generate benchmark output (probably invisible or self-created through monitors)
		File out;
		try {
			// use same extension as base video
			out = FfmpegUtil.sharedInstance.getTemporaryFile(fileVideo1);
		} catch (IOException e) {
			log.warn("Error. Could not temporary ffmpeg output. Exiting.", e);
			return null;
		}
		try {
			String overlay = getOverlayArgument(pos);
			log.debug("overlay-argument: " + overlay);
			List<String> params = new ArrayList<String>(20);
			params.add("-y"); // override without asking
			params.add("-i");
			params.add(fileVideo1.getAbsolutePath());
			if(!isWindows) {
				handleAAC(params);
			}
			params.add("-i");
			params.add(fileVideo2.getAbsolutePath());
			if(!isWindows) {
				handleAAC(params);
			}
			params.add("-filter_complex");
			params.add(overlay);
			handleExtension(params, out);
			params.add(out.getAbsolutePath());
			int exitValue = FfmpegUtil.sharedInstance.waitForever(
					FfmpegUtil.sharedInstance.createProcess(params), true);
			if(exitValue != 0)
				log.debug("Warning. Got exitValue of " + exitValue);
			else
				log.debug("Done");
		} catch (IOException e) {
			log.warn("Error. Could not create ffmpeg process. Exiting.", e);
		}
		// return result of temporary output
		log.debug("Return value: " + out);
		return out;
	}

	private void handleAAC(List<String> params) {
		params.add("-strict");
		params.add("experimental");
	}

	private void handleExtension(List<String> params, File fileVideo) {
		String extension = FileUtils.splitExt(fileVideo).extension;
		if(extension.equals(FLV)) {
			// set specific sample rate
			params.add("-ar");
			params.add("44100");
		}
	}

	private final static String left = "0";
	private final static String right = "main_w-overlay_w";
	private final static String top = "0";
	private final static String bottom = "main_h-overlay_h";
	private String getOverlayArgument(int pos) {
		String x, y;
		switch(pos) {
		case PipDoPip.POS_TOP_LEFT_CORNER:
			y = top;
			x = left;
			break;
		case PipDoPip.POS_TOP_RIGHT_CORNER:
			y = top;
			x = right;
			break;
		case PipDoPip.POS_BOTTOM_LEFT_CORNER:
			y = bottom;
			x = left;
			break;
		case PipDoPip.POS_BOTTOM_RIGHT_CORNER:
			y = bottom;
			x = right;
			break;
		default:
		case PipDoPip.POS_CENTER:
			y = "main_h/2-overlay_h/2";
			x = "main_w/2-overlay_w/2";
			break;
		}
		StringBuilder sb = new StringBuilder();
		if(isWindows) {
			sb.append('"');
		}
		sb.append("overlay");
		sb.append("=x=");
		sb.append(x);
		sb.append(":y=");
		sb.append(y);
		if(isWindows) {
			sb.append('"');
		}
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return TranscodingServerUtil.APP_NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "PiP";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.pip.ffmpeg.PipFfmpeg";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new PipFfmpegBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.pip.ffmpeg";
	}
	
	protected void setVideoProviderFactory(IVideoProviderFactory fac) {
		this.fac = fac;
	}
}
