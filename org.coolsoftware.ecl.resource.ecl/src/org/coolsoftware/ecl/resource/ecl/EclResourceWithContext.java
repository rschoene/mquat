/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.resource.ecl;

import java.util.Collections;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.ecl.resource.ecl.mopp.EclResource;
import org.eclipse.emf.common.util.URI;

/**
 * An {@link EclResourceWithContext} is an {@link EclResource} that further can
 * have a context containing a {@link Set} of {@link StructuralModel}s that are
 * visible within the {@link EclResource}s context.
 * 
 * @author Claas Wilke
 */
public class EclResourceWithContext extends EclResource {

	/** {@link StructuralModel}s visible within this resource's context. */
	protected Set<StructuralModel> visibleStructuralModels = null;

	/**
	 * Creates an instance with the given URI.
	 * 
	 * @param uri
	 *            the URI.
	 */
	public EclResourceWithContext(URI uri) {
		super(uri);
	}

	/**
	 * Returns the visible {@link StructuralModel} set as context of this
	 * {@link EclResource}.
	 * 
	 * @return The visible {@link StructuralModel} set as context of this
	 *         {@link EclResource}.
	 */
	public Set<StructuralModel> getVisibleStructuralModels() {
		if (visibleStructuralModels == null)
			return Collections.emptySet();
		else
			return visibleStructuralModels;
	}

	/**
	 * Sets the visible {@link StructuralModel} set as context of this
	 * {@link EclResource}.
	 * 
	 * @param visibleStructuralModels
	 *            The visible {@link StructuralModel} set as context of this
	 *            {@link EclResource}.
	 */
	public void setVisibleVariables(Set<StructuralModel> visibleStructuralModels) {
		this.visibleStructuralModels = visibleStructuralModels;
	}
}
