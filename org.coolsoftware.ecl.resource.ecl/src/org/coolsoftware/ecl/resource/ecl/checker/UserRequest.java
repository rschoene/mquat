/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.resource.ecl.checker;

import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;

/**
 * Simple user request to check contracts against.
 * @author Sebastian Götz
 */
public class UserRequest {
	private String requestedProperty;
	private CcmValue value;
	private String comparator;		
	
	public UserRequest(String requestedProperty, CcmValue value,
			String comparator) {
		super();
		this.requestedProperty = requestedProperty;
		this.value = value;
		this.comparator = comparator;
	}
	
	//provided: m1(min 20 fps) m2(min 10 fps, max 19 fps) m3(max 9 fps), requested min 15 fps -> m1,m2
	//provided: m1(max 9s) m2(min 10s, max 14s) m3(min 15s), requested max 15s 
	public boolean fulfils(CcmValue minProv, CcmValue maxProv) {
		//is value inbetween min provided and max provided
		double val = ((CcmReal)value).getRealValue();
		if(comparator.equals("min")) { //min-request-case (e.g. framerate)
			if(maxProv != null) {
				//min requirement
				double max = ((CcmReal)maxProv).getRealValue();
				return max > val;
			} else if(minProv != null){
				double min = ((CcmReal)minProv).getRealValue();
				return min >= val;
			}
		} else if(comparator.equals("max")) { //max-request-case (e.g. time)
			if(maxProv != null) {
				double max = ((CcmReal)maxProv).getRealValue();
				return max < val;
			} else if(minProv != null) {
				//could always be more than val (upper bound required)
			}
		}
		return false;
	}
	
}
