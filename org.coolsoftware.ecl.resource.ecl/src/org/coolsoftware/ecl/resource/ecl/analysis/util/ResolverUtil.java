/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.resource.ecl.analysis.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.Import;
import org.coolsoftware.ecl.resource.ecl.EclResourceWithContext;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

/**
 * Utility class providing methodes for loading and finding resources used in contracts.
 * @author Sebastian Götz
 */
public class ResolverUtil {

	/**
	 * Postfix of the {@link URL} of a {@link EResource} to be imported into an
	 * ECL contract.
	 */
	public static final String IMPORT_PATH_POSTFIX = "]";

	/**
	 * Prefix of the {@link URL} of a {@link EResource} to be imported into an
	 * ECL contract.
	 */
	public static final String IMPORT_PATH_PREFIX = "[";

	public static synchronized EObject loadResource(String identifier,
			Resource containerResource) throws FileNotFoundException {

		EObject result = null;

		try {
			// URI ccmURI = URI.createFileURI(ccmFile.getAbsolutePath());
			URI ccmURI = getURI(identifier, containerResource);
			
			ResourceSet rs = new ResourceSetImpl();
			Resource resource = rs.getResource(ccmURI, false);

			/* Check if the resource has already been created. */
			if (resource == null) {
				resource = rs.createResource(ccmURI);
			} 
			if(resource == null) {
				ccmURI = URI.createFileURI(identifier);
				resource = rs.createResource(ccmURI);
			}
			// no else.

			if(resource != null) {
				if (!resource.isLoaded())
					resource.load(null);
				// no else.
	
				if (resource.getContents().size() > 0) {
					result = resource.getContents().get(0);
				}
			}

			else {
				throw new FileNotFoundException("The CCM '" + identifier
						+ "' is empty.");
			}
		}

		catch (IllegalArgumentException e) {
			throw new FileNotFoundException("The URI '" + identifier
					+ "' is invalid.");
		}

		catch (IOException e) {
			throw new FileNotFoundException(
					"IOException during opening the file '" + identifier
							+ "': " + e.getMessage() + " || " + e.getCause());
		}

		return result;
	}

	/**
	 * Helper method to find {@link ResourceType} in an {@link EclFile}.
	 * 
	 * @param identifier
	 *            The id of the {@link ResourceType}s to be found.
	 * @param resolveFuzzy
	 *            If to resolve fuzzy or not.
	 * @param file
	 *            The {@link EclFile} used for resolving.
	 * @return A Map containing the resolved identifiers as keys and their
	 *         {@link ResourceType} as value.
	 */
	public static Map<String, ResourceType> findResourceTypes(
			String identifier, boolean resolveFuzzy, EclFile file) {

		Map<String, ResourceType> result = new HashMap<String, ResourceType>();

		for (Import impord : file.getImports()) {

			if (impord instanceof CcmImport) {
				CcmImport ccmImport = (CcmImport) impord;

				ComponentType root = ccmImport.getCcmStructuralModel()
						.getRoot();

				if (root instanceof ResourceType) {
					ResourceType curType = (ResourceType) root;
					result = findResourceTypesInner(identifier, resolveFuzzy,
							curType);
				}
				// no else (no SWComponentType).
			}
			// no else.
		}
		// end for (iteration on imports).

		if (file.eResource() instanceof EclResourceWithContext) {
			for (StructuralModel model : ((EclResourceWithContext) file
					.eResource()).getVisibleStructuralModels()) {

				ComponentType root = model.getRoot();

				if (root instanceof ResourceType) {
					ResourceType curType = (ResourceType) root;
					result = findResourceTypesInner(identifier, resolveFuzzy,
							curType);
				}
				// no else (no SWComponentType).
			}
			// no else.
		}
		// no else.

		return result;
	}

	private static Map<String, ResourceType> findResourceTypesInner(
			String identifier, boolean resolveFuzzy, ResourceType curType) {

		Map<String, ResourceType> result = new HashMap<String, ResourceType>();

		if (resolveFuzzy && curType.getName().startsWith(identifier))
			result.put(curType.getName(), (ResourceType) curType);
		else if (!resolveFuzzy && curType.getName().equals(identifier))
			result.put(curType.getName(), (ResourceType) curType);
		// no else.

		if (curType.getSubtypes() != null) {
			for (ResourceType sub : curType.getSubtypes()) {
				result.putAll(findResourceTypesInner(identifier, resolveFuzzy,
						sub));
			}
		}

		return result;
	}

	/**
	 * Helper method to find {@link SWComponentType} in an {@link EclFile}.
	 * 
	 * @param identifier
	 *            The id of the {@link SWComponentType}s to be found.
	 * @param resolveFuzzy
	 *            If to resolve fuzzy or not.
	 * @param file
	 *            The {@link EclFile} used for resolving.
	 * @return A Map containing the resolved identifiers as keys and their
	 *         {@link SWComponentType} as value.
	 */
	public static Map<String, SWComponentType> findSWComponentTypes(
			String identifier, boolean resolveFuzzy, EclFile file) {

		Map<String, SWComponentType> result = new HashMap<String, SWComponentType>();

		for (Import impord : file.getImports()) {

			if (impord instanceof CcmImport) {
				CcmImport ccmImport = (CcmImport) impord;

				ComponentType root = ccmImport.getCcmStructuralModel()
						.getRoot();

				if (root instanceof SWComponentType) {
					SWComponentType curType = (SWComponentType) root;
					result = findSWComponentTypesInner(identifier,
							resolveFuzzy, curType);
				}
				// no else (no SWComponentType).
			}
			// no else.
		}
		// end for (iteration on imports).

		if (file.eResource() instanceof EclResourceWithContext) {
			for (StructuralModel model : ((EclResourceWithContext) file
					.eResource()).getVisibleStructuralModels()) {

				ComponentType root = model.getRoot();

				if (root instanceof SWComponentType) {
					SWComponentType curType = (SWComponentType) root;
					result = findSWComponentTypesInner(identifier,
							resolveFuzzy, curType);
				}
				// no else (no SWComponentType).
			}
			// no else.
		}
		// no else.

		return result;
	}

	private static Map<String, SWComponentType> findSWComponentTypesInner(
			String identifier, boolean resolveFuzzy, SWComponentType curType) {

		Map<String, SWComponentType> result = new HashMap<String, SWComponentType>();

		if (resolveFuzzy && curType.getName().startsWith(identifier))
			result.put(curType.getName(), (SWComponentType) curType);
		else if (!resolveFuzzy && curType.getName().equals(identifier))
			result.put(curType.getName(), (SWComponentType) curType);
		// no else.

		if (curType.getSubtypes() != null) {
			for (SWComponentType sub : curType.getSubtypes()) {
				result.putAll(findSWComponentTypesInner(identifier,
						resolveFuzzy, sub));
			}
		}

		return result;
	}

	/**
	 * Helper method to resolve a given relative path and a given
	 * {@link Resource} which refers to the relative path as input.
	 * 
	 * @param relativePath
	 *            The relative path.
	 * @param containerResource
	 *            The {@link Resource} containing the relative path.
	 * @return The resolve absolute {@link URI}.
	 */
	protected static URI getURI(String relativePath, Resource containerResource) {
		URI hintURI;

		if (relativePath.contains(":")) {
			// relativePath is an absolute path - we can use it as it is
			hintURI = URI.createFileURI(relativePath);
		}

		else if (relativePath.startsWith("./")) {
			// relativePath is an relative path that we can use it as it is
			hintURI = URI.createURI(relativePath);
		}

		else {
			// relativePath is an relative path - we must resolve it
			URI containerURI = containerResource.getURI();
			hintURI = URI.createURI(relativePath).resolve(containerURI);
		}

		return hintURI;
	}
}
