/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.analysis;

import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.eclipse.emf.ecore.EObject;

public class FormulaTemplateMetaparameterReferenceResolver implements org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter> {
	
	private org.coolsoftware.ecl.resource.ecl.analysis.EclDefaultResolverDelegate<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter> delegate = new org.coolsoftware.ecl.resource.ecl.analysis.EclDefaultResolverDelegate<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter>();
	
	public void resolve(String identifier, org.coolsoftware.ecl.FormulaTemplate container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Parameter> result) {
		//delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
				if(container.eContainer() instanceof PropertyRequirementClause) {
					EObject ctr = container.eContainer().eContainer().eContainer().eContainer();
					if(ctr instanceof SWComponentContract) {
						for(Parameter p : ((SWComponentContract)ctr).getPort().getMetaparameter()) {
							if(p.getName().equals(identifier)) result.addMapping(identifier, p);
						}
					}
				} else if(container.eContainer() instanceof ProvisionClause) {
					EObject ctr = container.eContainer().eContainer().eContainer();
					if(ctr instanceof SWComponentContract) {
						for(Parameter p : ((SWComponentContract)ctr).getPort().getMetaparameter()) {
							if(p.getName().equals(identifier)) result.addMapping(identifier, p);
						}
					}
				}
	}
	
	public String deResolve(org.coolsoftware.coolcomponents.ccm.structure.Parameter element, org.coolsoftware.ecl.FormulaTemplate container, org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
