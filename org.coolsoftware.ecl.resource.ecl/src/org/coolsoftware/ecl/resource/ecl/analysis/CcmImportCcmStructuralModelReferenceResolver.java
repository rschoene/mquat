/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.analysis;

import java.io.FileNotFoundException;

import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.ecl.resource.ecl.analysis.util.ResolverUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

public class CcmImportCcmStructuralModelReferenceResolver
		implements
		org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.CcmImport, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver#resolve(java.
	 * lang.String, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EReference, int, boolean,
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult)
	 */
	public void resolve(
			String identifier,
			org.coolsoftware.ecl.CcmImport container,
			org.eclipse.emf.ecore.EReference reference,
			int position,
			boolean resolveFuzzy,
			final org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> result) {

		try {
			EObject canidate = ResolverUtil.loadResource(identifier,
					container.eResource());

			if (canidate instanceof StructuralModel) {
				StructuralModel sm = (StructuralModel) canidate;
				result.addMapping(identifier, sm);
			}

			else {
				result.setErrorMessage("Invalid type of EObject. Expected StructuralModel but was "
						+ canidate.eClass().getName() + ".");
			}
		}

		catch (FileNotFoundException e) {
			result.setErrorMessage(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver#deResolve(org
	 * .eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EReference)
	 */
	public String deResolve(
			org.coolsoftware.coolcomponents.ccm.structure.StructuralModel element,
			org.coolsoftware.ecl.CcmImport container,
			org.eclipse.emf.ecore.EReference reference) {
		URI uri = element.eResource().getURI();
		if(uri.isFile())
			return uri.toFileString();
		else if (uri.isPlatform())
			return uri.toString();
		
		return element.eResource().getURI().toFileString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclConfigurable#setOptions(java.util
	 * .Map)
	 */
	public void setOptions(java.util.Map<?, ?> options) {
		// save options in a field or leave method empty if this resolver does
		// not depend
		// on any option
	}
}
