/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.analysis;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.ecl.HWComponentContract;
import org.coolsoftware.ecl.SWComponentContract;

public class ProvisionClauseProvidedPropertyReferenceResolver
		implements
		org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver#resolve(java.
	 * lang.String, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EReference, int, boolean,
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult)
	 */
	public void resolve(
			String identifier,
			org.coolsoftware.ecl.ProvisionClause container,
			org.eclipse.emf.ecore.EReference reference,
			int position,
			boolean resolveFuzzy,
			final org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property> result) {

		result.setErrorMessage("Cannot resolve Property '" + identifier + "'.");

		ComponentType compType;

		if (container.eContainer().eContainer() instanceof SWComponentContract)
			compType = ((SWComponentContract) container.eContainer()
					.eContainer()).getComponentType();

		else if (container.eContainer().eContainer() instanceof HWComponentContract)
			compType = ((HWComponentContract) container.eContainer()
					.eContainer()).getComponentType();

		else
			return;

		for (Property property : compType.getProperties()) {

			if (resolveFuzzy && property.getDeclaredVariable().getName().startsWith(identifier))
				result.addMapping(property.getDeclaredVariable().getName(), property);
			else if (!resolveFuzzy && property.getDeclaredVariable().getName().equals(identifier))
				result.addMapping(identifier, property);
			// no else.
		}
		// end for.
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver#deResolve(org
	 * .eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EReference)
	 */
	public String deResolve(
			org.coolsoftware.coolcomponents.ccm.structure.Property element,
			org.coolsoftware.ecl.ProvisionClause container,
			org.eclipse.emf.ecore.EReference reference) {
		return element.getDeclaredVariable().getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclConfigurable#setOptions(java.util
	 * .Map)
	 */
	public void setOptions(java.util.Map<?, ?> options) {
		// save options in a field or leave method empty if this resolver does
		// not depend
		// on any option
	}
}
