/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.analysis;

import org.coolsoftware.coolcomponents.ccm.structure.PortType;


public class SWComponentContractPortReferenceResolver implements org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType> {
	
	private org.coolsoftware.ecl.resource.ecl.analysis.EclDefaultResolverDelegate<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType> delegate = new org.coolsoftware.ecl.resource.ecl.analysis.EclDefaultResolverDelegate<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType>();
	
	public void resolve(String identifier, org.coolsoftware.ecl.SWComponentContract container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.PortType> result) {
		for(PortType pt : container.getComponentType().getPorttypes()) {
			if(resolveFuzzy) {
				if(pt.getName().startsWith(identifier)) result.addMapping(identifier, pt);
			} else {
				if(pt.getName().equals(identifier)) result.addMapping(identifier, pt);
			}
		}
	}
	
	public String deResolve(org.coolsoftware.coolcomponents.ccm.structure.PortType element, org.coolsoftware.ecl.SWComponentContract container, org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
