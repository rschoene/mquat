/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.analysis;

import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.resource.ecl.analysis.util.ResolverUtil;

public class SWComponentRequirementClauseRequiredComponentTypeReferenceResolver
		implements
		org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.SWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver#resolve(java.
	 * lang.String, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EReference, int, boolean,
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult)
	 */
	public void resolve(
			String identifier,
			org.coolsoftware.ecl.SWComponentRequirementClause container,
			org.eclipse.emf.ecore.EReference reference,
			int position,
			boolean resolveFuzzy,
			final org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> result) {
		result.setErrorMessage("Cannot find SWComponentType '" + identifier
				+ "'.");

		EclFile file = (EclFile) container.eResource().getContents().get(0);

		Map<String, SWComponentType> resolvedTypes = ResolverUtil
				.findSWComponentTypes(identifier, resolveFuzzy, file);

		for (String key : resolvedTypes.keySet())
			result.addMapping(key, resolvedTypes.get(key));
		// end for.
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver#deResolve(org
	 * .eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EReference)
	 */
	public String deResolve(
			org.coolsoftware.coolcomponents.ccm.structure.SWComponentType element,
			org.coolsoftware.ecl.SWComponentRequirementClause container,
			org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.ecl.resource.ecl.IEclConfigurable#setOptions(java.util
	 * .Map)
	 */
	public void setOptions(java.util.Map<?, ?> options) {
		// save options in a field or leave method empty if this resolver does
		// not depend
		// on any option
	}
}
