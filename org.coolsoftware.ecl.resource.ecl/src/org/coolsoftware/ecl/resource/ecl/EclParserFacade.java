/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.resource.ecl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.resource.ecl.mopp.EclPrinter2;
import org.coolsoftware.ecl.resource.ecl.mopp.EclResource;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Facade class to provide methods to parse {@link EclFile}s with a given
 * context.
 * 
 * @author Claas Wilke
 */
public class EclParserFacade {

	/**
	 * Parses a given {@link String} into an {@link EclResource}. The
	 * {@link EclResource} might contain errors if the given {@link String} does
	 * not represent a valid {@link EclFile}.
	 * 
	 * <p>
	 * <strong>Note:</strong> Well-formedness rule checks are integrated via
	 * depency injection, thus, they will only be checked when executing this
	 * class as an Eclipse application.
	 * <p>
	 * 
	 * @param source
	 *            The {@link String} to be parsed.
	 * @param visibleStructuralModels
	 *            A {@link Set} of {@link StructuralModel}s that are visible
	 *            within the context of the {@link EclFile} to be parsed.
	 * @return The parsed {@link EclResource}.
	 */
	public static EclResource parseEclFile(String source,
			Set<StructuralModel> visibleStructuralModels) {

		if (source == null)
			throw new IllegalArgumentException(
					"The source String to be parsed cannot be null");
		// no else.

		ResourceSet rs = new ResourceSetImpl();
		EclResourceWithContext resource = new EclResourceWithContext(
				URI.createFileURI("temp.ecl"));

		if (visibleStructuralModels != null)
			resource.setVisibleVariables(visibleStructuralModels);
		// no else.

		rs.getResources().add(resource);

		try {
			resource.load(new ByteArrayInputStream(source.getBytes()), null);
			EcoreUtil.resolveAll(resource);
		}

		catch (IOException e) {
			throw new IllegalStateException(
					"Unexpected IOException during parsing of source '"
							+ source + "'.", e);
		}

		return resource;
	}
	public static EclResource parseEclFile(InputStream source,
			Set<StructuralModel> visibleStructuralModels) {

		if (source == null)
			throw new IllegalArgumentException(
					"The source String to be parsed cannot be null");
		// no else.

		ResourceSet rs = new ResourceSetImpl();
		EclResourceWithContext resource = new EclResourceWithContext(
				URI.createFileURI("temp.ecl"));

		if (visibleStructuralModels != null)
			resource.setVisibleVariables(visibleStructuralModels);
		// no else.

		rs.getResources().add(resource);

		try {
			resource.load(source, null);
			EcoreUtil.resolveAll(resource);
		}

		catch (IOException e) {
			throw new IllegalStateException(
					"Unexpected IOException during parsing of source '"
							+ source + "'.", e);
		}
		
		finally {
			try {
			source.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}

		return resource;
	}

	/**
	 * Returns the printed {@link String} representation for a given
	 * {@link EclFile}.
	 * 
	 * @param contract
	 *            The {@link EclContract} to be printed.
	 * @return The printed {@link EclFile}.
	 */
	public static String printContract(EclContract contract) {

		OutputStream os = new ByteArrayOutputStream();
		EclPrinter2 printer;

		if (contract.eResource() instanceof IEclTextResource)
			printer = new EclPrinter2(os, (EclResource) contract.eResource());
		else
			printer = new EclPrinter2(os, null);

		try {
			printer.print(contract);
		}

		catch (IOException e) {
			throw new IllegalStateException(
					"Unexpected IOException during printing of contract '"
							+ contract.toString() + "'.", e);
		}

		return os.toString();
	}
	
	public static String printEclFile(EclFile f) {

		OutputStream os = new ByteArrayOutputStream();
		EclPrinter2 printer;

		if (f.eResource() instanceof IEclTextResource)
			printer = new EclPrinter2(os, (EclResource) f.eResource());
		else
			printer = new EclPrinter2(os, null);

		try {
			printer.print(f);
		}

		catch (IOException e) {
			throw new IllegalStateException(
					"Unexpected IOException during printing of contract '"
							+ f.toString() + "'.", e);
		}

		return os.toString();
	}
}
