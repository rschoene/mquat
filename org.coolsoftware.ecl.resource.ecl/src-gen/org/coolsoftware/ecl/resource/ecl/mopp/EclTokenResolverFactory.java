/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.mopp;

/**
 * The EclTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class EclTokenResolverFactory implements org.coolsoftware.ecl.resource.ecl.IEclTokenResolverFactory {
	
	private java.util.Map<String, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver> featureName2CollectInTokenResolver;
	private static org.coolsoftware.ecl.resource.ecl.IEclTokenResolver defaultResolver = new org.coolsoftware.ecl.resource.ecl.analysis.EclDefaultTokenResolver();
	
	public EclTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver>();
		registerTokenResolver("ILT", new org.coolsoftware.ecl.resource.ecl.analysis.ExpILTTokenResolver());
		registerTokenResolver("QUOTED_91_93", new org.coolsoftware.ecl.resource.ecl.analysis.EclQUOTED_91_93TokenResolver());
		registerTokenResolver("TYPE_LITERAL", new org.coolsoftware.ecl.resource.ecl.analysis.ExpTYPE_LITERALTokenResolver());
		registerTokenResolver("REAL_LITERAL", new org.coolsoftware.ecl.resource.ecl.analysis.ExpREAL_LITERALTokenResolver());
		registerTokenResolver("TEXT", new org.coolsoftware.ecl.resource.ecl.analysis.EclTEXTTokenResolver());
		registerTokenResolver("QUOTED_34_34", new org.coolsoftware.ecl.resource.ecl.analysis.ExpQUOTED_34_34TokenResolver());
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected org.coolsoftware.ecl.resource.ecl.IEclTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private org.coolsoftware.ecl.resource.ecl.IEclTokenResolver internalCreateResolver(java.util.Map<String, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver> resolverMap, String key, org.coolsoftware.ecl.resource.ecl.IEclTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
