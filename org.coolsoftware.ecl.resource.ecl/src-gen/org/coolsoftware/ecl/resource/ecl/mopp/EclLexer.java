/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 3.4

	package org.coolsoftware.ecl.resource.ecl.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class EclLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int ILT=4;
    public static final int LINEBREAK=5;
    public static final int ML_COMMENT=6;
    public static final int QUOTED_34_34=7;
    public static final int QUOTED_91_93=8;
    public static final int REAL_LITERAL=9;
    public static final int SL_COMMENT=10;
    public static final int TEXT=11;
    public static final int TYPE_LITERAL=12;
    public static final int WHITESPACE=13;

    	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
    	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
    	
    	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
    		lexerExceptions.add(e);
    		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
    	}


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public EclLexer() {} 
    public EclLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public EclLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "Ecl.g"; }

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:15:7: ( '!' )
            // Ecl.g:15:9: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:16:7: ( '!=' )
            // Ecl.g:16:9: '!='
            {
            match("!="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:17:7: ( '(' )
            // Ecl.g:17:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:18:7: ( ')' )
            // Ecl.g:18:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:19:7: ( ')>' )
            // Ecl.g:19:9: ')>'
            {
            match(")>"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:20:7: ( '*' )
            // Ecl.g:20:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:21:7: ( '+' )
            // Ecl.g:21:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:22:7: ( '++' )
            // Ecl.g:22:9: '++'
            {
            match("++"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:23:7: ( '+=' )
            // Ecl.g:23:9: '+='
            {
            match("+="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:24:7: ( '-' )
            // Ecl.g:24:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:25:7: ( '--' )
            // Ecl.g:25:9: '--'
            {
            match("--"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:26:7: ( '-=' )
            // Ecl.g:26:9: '-='
            {
            match("-="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:27:7: ( '.' )
            // Ecl.g:27:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:28:7: ( '/' )
            // Ecl.g:28:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:29:7: ( ':' )
            // Ecl.g:29:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:30:7: ( ';' )
            // Ecl.g:30:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:31:7: ( '<' )
            // Ecl.g:31:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:32:7: ( '<=' )
            // Ecl.g:32:9: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:33:7: ( '=' )
            // Ecl.g:33:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:34:7: ( '==' )
            // Ecl.g:34:9: '=='
            {
            match("=="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:35:7: ( '>' )
            // Ecl.g:35:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:36:7: ( '>=' )
            // Ecl.g:36:9: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:37:7: ( '^' )
            // Ecl.g:37:9: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:38:7: ( 'and' )
            // Ecl.g:38:9: 'and'
            {
            match("and"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:39:7: ( 'ccm' )
            // Ecl.g:39:9: 'ccm'
            {
            match("ccm"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:40:7: ( 'component' )
            // Ecl.g:40:9: 'component'
            {
            match("component"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:41:7: ( 'contract' )
            // Ecl.g:41:9: 'contract'
            {
            match("contract"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:42:7: ( 'cos' )
            // Ecl.g:42:9: 'cos'
            {
            match("cos"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:43:7: ( 'energyRate: ' )
            // Ecl.g:43:9: 'energyRate: '
            {
            match("energyRate: "); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:44:7: ( 'f' )
            // Ecl.g:44:9: 'f'
            {
            match('f'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:45:7: ( 'false' )
            // Ecl.g:45:9: 'false'
            {
            match("false"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:46:7: ( 'hardware' )
            // Ecl.g:46:9: 'hardware'
            {
            match("hardware"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:47:7: ( 'implements' )
            // Ecl.g:47:9: 'implements'
            {
            match("implements"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:48:7: ( 'implies' )
            // Ecl.g:48:9: 'implies'
            {
            match("implies"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:49:7: ( 'import' )
            // Ecl.g:49:9: 'import'
            {
            match("import"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:50:7: ( 'max:' )
            // Ecl.g:50:9: 'max:'
            {
            match("max:"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:51:7: ( 'metaparameter:' )
            // Ecl.g:51:9: 'metaparameter:'
            {
            match("metaparameter:"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:52:7: ( 'min:' )
            // Ecl.g:52:9: 'min:'
            {
            match("min:"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:53:7: ( 'mode' )
            // Ecl.g:53:9: 'mode'
            {
            match("mode"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:54:7: ( 'not' )
            // Ecl.g:54:9: 'not'
            {
            match("not"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:55:7: ( 'or' )
            // Ecl.g:55:9: 'or'
            {
            match("or"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:56:7: ( 'provides' )
            // Ecl.g:56:9: 'provides'
            {
            match("provides"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:57:7: ( 'requires' )
            // Ecl.g:57:9: 'requires'
            {
            match("requires"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:58:7: ( 'resource' )
            // Ecl.g:58:9: 'resource'
            {
            match("resource"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:59:7: ( 'sin' )
            // Ecl.g:59:9: 'sin'
            {
            match("sin"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:60:7: ( 'software' )
            // Ecl.g:60:9: 'software'
            {
            match("software"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "T__60"
    public final void mT__60() throws RecognitionException {
        try {
            int _type = T__60;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:61:7: ( 'true' )
            // Ecl.g:61:9: 'true'
            {
            match("true"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__60"

    // $ANTLR start "T__61"
    public final void mT__61() throws RecognitionException {
        try {
            int _type = T__61;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:62:7: ( 'var' )
            // Ecl.g:62:9: 'var'
            {
            match("var"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__61"

    // $ANTLR start "T__62"
    public final void mT__62() throws RecognitionException {
        try {
            int _type = T__62;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:63:7: ( '{' )
            // Ecl.g:63:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__62"

    // $ANTLR start "T__63"
    public final void mT__63() throws RecognitionException {
        try {
            int _type = T__63;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:64:7: ( '}' )
            // Ecl.g:64:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__63"

    // $ANTLR start "ILT"
    public final void mILT() throws RecognitionException {
        try {
            int _type = ILT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5347:4: ( ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? ) )
            // Ecl.g:5348:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            {
            // Ecl.g:5348:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            // Ecl.g:5348:2: ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            {
            // Ecl.g:5348:2: ( '-' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='-') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // Ecl.g:5348:2: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Ecl.g:5348:6: ( '0' .. '9' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Ecl.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            // Ecl.g:5348:17: ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='E'||LA5_0=='e') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // Ecl.g:5348:18: ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Ecl.g:5348:27: ( '-' )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0=='-') ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // Ecl.g:5348:28: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // Ecl.g:5348:33: ( '0' .. '9' )+
                    int cnt4=0;
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // Ecl.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt4 >= 1 ) break loop4;
                                EarlyExitException eee =
                                    new EarlyExitException(4, input);
                                throw eee;
                        }
                        cnt4++;
                    } while (true);


                    }
                    break;

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ILT"

    // $ANTLR start "WHITESPACE"
    public final void mWHITESPACE() throws RecognitionException {
        try {
            int _type = WHITESPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5350:11: ( ( ( ' ' | '\\t' | '\\f' ) ) )
            // Ecl.g:5351:2: ( ( ' ' | '\\t' | '\\f' ) )
            {
            if ( input.LA(1)=='\t'||input.LA(1)=='\f'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "LINEBREAK"
    public final void mLINEBREAK() throws RecognitionException {
        try {
            int _type = LINEBREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5354:10: ( ( ( '\\r\\n' | '\\r' | '\\n' ) ) )
            // Ecl.g:5355:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            {
            // Ecl.g:5355:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            // Ecl.g:5355:2: ( '\\r\\n' | '\\r' | '\\n' )
            {
            // Ecl.g:5355:2: ( '\\r\\n' | '\\r' | '\\n' )
            int alt6=3;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='\r') ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1=='\n') ) {
                    alt6=1;
                }
                else {
                    alt6=2;
                }
            }
            else if ( (LA6_0=='\n') ) {
                alt6=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }
            switch (alt6) {
                case 1 :
                    // Ecl.g:5355:3: '\\r\\n'
                    {
                    match("\r\n"); 



                    }
                    break;
                case 2 :
                    // Ecl.g:5355:12: '\\r'
                    {
                    match('\r'); 

                    }
                    break;
                case 3 :
                    // Ecl.g:5355:19: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINEBREAK"

    // $ANTLR start "QUOTED_91_93"
    public final void mQUOTED_91_93() throws RecognitionException {
        try {
            int _type = QUOTED_91_93;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5358:13: ( ( ( '[' ) (~ ( ']' ) )* ( ']' ) ) )
            // Ecl.g:5359:2: ( ( '[' ) (~ ( ']' ) )* ( ']' ) )
            {
            // Ecl.g:5359:2: ( ( '[' ) (~ ( ']' ) )* ( ']' ) )
            // Ecl.g:5359:2: ( '[' ) (~ ( ']' ) )* ( ']' )
            {
            // Ecl.g:5359:2: ( '[' )
            // Ecl.g:5359:3: '['
            {
            match('['); 

            }


            // Ecl.g:5359:7: (~ ( ']' ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\\')||(LA7_0 >= '^' && LA7_0 <= '\uFFFF')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // Ecl.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\\')||(input.LA(1) >= '^' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            // Ecl.g:5359:16: ( ']' )
            // Ecl.g:5359:17: ']'
            {
            match(']'); 

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTED_91_93"

    // $ANTLR start "SL_COMMENT"
    public final void mSL_COMMENT() throws RecognitionException {
        try {
            int _type = SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5361:11: ( ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* ) )
            // Ecl.g:5362:3: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            {
            // Ecl.g:5362:3: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            // Ecl.g:5362:3: '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            {
            match("//"); 



            // Ecl.g:5362:7: (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0 >= '\u0000' && LA8_0 <= '\t')||(LA8_0 >= '\u000B' && LA8_0 <= '\f')||(LA8_0 >= '\u000E' && LA8_0 <= '\uFFFE')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // Ecl.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFE') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SL_COMMENT"

    // $ANTLR start "ML_COMMENT"
    public final void mML_COMMENT() throws RecognitionException {
        try {
            int _type = ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5365:11: ( ( '/*' ( . )* '*/' ) )
            // Ecl.g:5366:3: ( '/*' ( . )* '*/' )
            {
            // Ecl.g:5366:3: ( '/*' ( . )* '*/' )
            // Ecl.g:5366:3: '/*' ( . )* '*/'
            {
            match("/*"); 



            // Ecl.g:5366:7: ( . )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='*') ) {
                    int LA9_1 = input.LA(2);

                    if ( (LA9_1=='/') ) {
                        alt9=2;
                    }
                    else if ( ((LA9_1 >= '\u0000' && LA9_1 <= '.')||(LA9_1 >= '0' && LA9_1 <= '\uFFFF')) ) {
                        alt9=1;
                    }


                }
                else if ( ((LA9_0 >= '\u0000' && LA9_0 <= ')')||(LA9_0 >= '+' && LA9_0 <= '\uFFFF')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // Ecl.g:5366:7: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            match("*/"); 



            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ML_COMMENT"

    // $ANTLR start "TYPE_LITERAL"
    public final void mTYPE_LITERAL() throws RecognitionException {
        try {
            int _type = TYPE_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5369:13: ( ( ( 'Boolean' | 'Integer' | 'Real' | 'String' ) ) )
            // Ecl.g:5370:2: ( ( 'Boolean' | 'Integer' | 'Real' | 'String' ) )
            {
            // Ecl.g:5370:2: ( ( 'Boolean' | 'Integer' | 'Real' | 'String' ) )
            // Ecl.g:5370:2: ( 'Boolean' | 'Integer' | 'Real' | 'String' )
            {
            // Ecl.g:5370:2: ( 'Boolean' | 'Integer' | 'Real' | 'String' )
            int alt10=4;
            switch ( input.LA(1) ) {
            case 'B':
                {
                alt10=1;
                }
                break;
            case 'I':
                {
                alt10=2;
                }
                break;
            case 'R':
                {
                alt10=3;
                }
                break;
            case 'S':
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }

            switch (alt10) {
                case 1 :
                    // Ecl.g:5370:3: 'Boolean'
                    {
                    match("Boolean"); 



                    }
                    break;
                case 2 :
                    // Ecl.g:5370:15: 'Integer'
                    {
                    match("Integer"); 



                    }
                    break;
                case 3 :
                    // Ecl.g:5370:27: 'Real'
                    {
                    match("Real"); 



                    }
                    break;
                case 4 :
                    // Ecl.g:5370:36: 'String'
                    {
                    match("String"); 



                    }
                    break;

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TYPE_LITERAL"

    // $ANTLR start "REAL_LITERAL"
    public final void mREAL_LITERAL() throws RecognitionException {
        try {
            int _type = REAL_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5372:13: ( ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? ) )
            // Ecl.g:5373:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            {
            // Ecl.g:5373:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            // Ecl.g:5373:2: ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            {
            // Ecl.g:5373:2: ( '-' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='-') ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // Ecl.g:5373:2: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Ecl.g:5373:6: ( '0' .. '9' )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0 >= '0' && LA12_0 <= '9')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // Ecl.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            // Ecl.g:5373:17: ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='E'||LA15_0=='e') ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // Ecl.g:5373:18: ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Ecl.g:5373:27: ( '-' )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0=='-') ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // Ecl.g:5373:28: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // Ecl.g:5373:33: ( '0' .. '9' )+
                    int cnt14=0;
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( ((LA14_0 >= '0' && LA14_0 <= '9')) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // Ecl.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt14 >= 1 ) break loop14;
                                EarlyExitException eee =
                                    new EarlyExitException(14, input);
                                throw eee;
                        }
                        cnt14++;
                    } while (true);


                    }
                    break;

            }


            match('.'); 

            // Ecl.g:5373:49: ( '-' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0=='-') ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // Ecl.g:5373:49: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Ecl.g:5373:53: ( '0' .. '9' )+
            int cnt17=0;
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0 >= '0' && LA17_0 <= '9')) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // Ecl.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt17 >= 1 ) break loop17;
                        EarlyExitException eee =
                            new EarlyExitException(17, input);
                        throw eee;
                }
                cnt17++;
            } while (true);


            // Ecl.g:5373:64: ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0=='E'||LA20_0=='e') ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // Ecl.g:5373:65: ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Ecl.g:5373:74: ( '-' )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0=='-') ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // Ecl.g:5373:75: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // Ecl.g:5373:80: ( '0' .. '9' )+
                    int cnt19=0;
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( ((LA19_0 >= '0' && LA19_0 <= '9')) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // Ecl.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt19 >= 1 ) break loop19;
                                EarlyExitException eee =
                                    new EarlyExitException(19, input);
                                throw eee;
                        }
                        cnt19++;
                    } while (true);


                    }
                    break;

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REAL_LITERAL"

    // $ANTLR start "TEXT"
    public final void mTEXT() throws RecognitionException {
        try {
            int _type = TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5375:5: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ ) )
            // Ecl.g:5376:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ )
            {
            // Ecl.g:5376:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ )
            // Ecl.g:5376:2: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+
            {
            // Ecl.g:5376:2: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+
            int cnt21=0;
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0=='-'||(LA21_0 >= '0' && LA21_0 <= '9')||(LA21_0 >= 'A' && LA21_0 <= 'Z')||LA21_0=='_'||(LA21_0 >= 'a' && LA21_0 <= 'z')) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // Ecl.g:
            	    {
            	    if ( input.LA(1)=='-'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt21 >= 1 ) break loop21;
                        EarlyExitException eee =
                            new EarlyExitException(21, input);
                        throw eee;
                }
                cnt21++;
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TEXT"

    // $ANTLR start "QUOTED_34_34"
    public final void mQUOTED_34_34() throws RecognitionException {
        try {
            int _type = QUOTED_34_34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Ecl.g:5378:13: ( ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) ) )
            // Ecl.g:5379:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            {
            // Ecl.g:5379:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            // Ecl.g:5379:2: ( '\"' ) (~ ( '\"' ) )* ( '\"' )
            {
            // Ecl.g:5379:2: ( '\"' )
            // Ecl.g:5379:3: '\"'
            {
            match('\"'); 

            }


            // Ecl.g:5379:7: (~ ( '\"' ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0 >= '\u0000' && LA22_0 <= '!')||(LA22_0 >= '#' && LA22_0 <= '\uFFFF')) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // Ecl.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            // Ecl.g:5379:16: ( '\"' )
            // Ecl.g:5379:17: '\"'
            {
            match('\"'); 

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTED_34_34"

    public void mTokens() throws RecognitionException {
        // Ecl.g:1:8: ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | ILT | WHITESPACE | LINEBREAK | QUOTED_91_93 | SL_COMMENT | ML_COMMENT | TYPE_LITERAL | REAL_LITERAL | TEXT | QUOTED_34_34 )
        int alt23=60;
        alt23 = dfa23.predict(input);
        switch (alt23) {
            case 1 :
                // Ecl.g:1:10: T__14
                {
                mT__14(); 


                }
                break;
            case 2 :
                // Ecl.g:1:16: T__15
                {
                mT__15(); 


                }
                break;
            case 3 :
                // Ecl.g:1:22: T__16
                {
                mT__16(); 


                }
                break;
            case 4 :
                // Ecl.g:1:28: T__17
                {
                mT__17(); 


                }
                break;
            case 5 :
                // Ecl.g:1:34: T__18
                {
                mT__18(); 


                }
                break;
            case 6 :
                // Ecl.g:1:40: T__19
                {
                mT__19(); 


                }
                break;
            case 7 :
                // Ecl.g:1:46: T__20
                {
                mT__20(); 


                }
                break;
            case 8 :
                // Ecl.g:1:52: T__21
                {
                mT__21(); 


                }
                break;
            case 9 :
                // Ecl.g:1:58: T__22
                {
                mT__22(); 


                }
                break;
            case 10 :
                // Ecl.g:1:64: T__23
                {
                mT__23(); 


                }
                break;
            case 11 :
                // Ecl.g:1:70: T__24
                {
                mT__24(); 


                }
                break;
            case 12 :
                // Ecl.g:1:76: T__25
                {
                mT__25(); 


                }
                break;
            case 13 :
                // Ecl.g:1:82: T__26
                {
                mT__26(); 


                }
                break;
            case 14 :
                // Ecl.g:1:88: T__27
                {
                mT__27(); 


                }
                break;
            case 15 :
                // Ecl.g:1:94: T__28
                {
                mT__28(); 


                }
                break;
            case 16 :
                // Ecl.g:1:100: T__29
                {
                mT__29(); 


                }
                break;
            case 17 :
                // Ecl.g:1:106: T__30
                {
                mT__30(); 


                }
                break;
            case 18 :
                // Ecl.g:1:112: T__31
                {
                mT__31(); 


                }
                break;
            case 19 :
                // Ecl.g:1:118: T__32
                {
                mT__32(); 


                }
                break;
            case 20 :
                // Ecl.g:1:124: T__33
                {
                mT__33(); 


                }
                break;
            case 21 :
                // Ecl.g:1:130: T__34
                {
                mT__34(); 


                }
                break;
            case 22 :
                // Ecl.g:1:136: T__35
                {
                mT__35(); 


                }
                break;
            case 23 :
                // Ecl.g:1:142: T__36
                {
                mT__36(); 


                }
                break;
            case 24 :
                // Ecl.g:1:148: T__37
                {
                mT__37(); 


                }
                break;
            case 25 :
                // Ecl.g:1:154: T__38
                {
                mT__38(); 


                }
                break;
            case 26 :
                // Ecl.g:1:160: T__39
                {
                mT__39(); 


                }
                break;
            case 27 :
                // Ecl.g:1:166: T__40
                {
                mT__40(); 


                }
                break;
            case 28 :
                // Ecl.g:1:172: T__41
                {
                mT__41(); 


                }
                break;
            case 29 :
                // Ecl.g:1:178: T__42
                {
                mT__42(); 


                }
                break;
            case 30 :
                // Ecl.g:1:184: T__43
                {
                mT__43(); 


                }
                break;
            case 31 :
                // Ecl.g:1:190: T__44
                {
                mT__44(); 


                }
                break;
            case 32 :
                // Ecl.g:1:196: T__45
                {
                mT__45(); 


                }
                break;
            case 33 :
                // Ecl.g:1:202: T__46
                {
                mT__46(); 


                }
                break;
            case 34 :
                // Ecl.g:1:208: T__47
                {
                mT__47(); 


                }
                break;
            case 35 :
                // Ecl.g:1:214: T__48
                {
                mT__48(); 


                }
                break;
            case 36 :
                // Ecl.g:1:220: T__49
                {
                mT__49(); 


                }
                break;
            case 37 :
                // Ecl.g:1:226: T__50
                {
                mT__50(); 


                }
                break;
            case 38 :
                // Ecl.g:1:232: T__51
                {
                mT__51(); 


                }
                break;
            case 39 :
                // Ecl.g:1:238: T__52
                {
                mT__52(); 


                }
                break;
            case 40 :
                // Ecl.g:1:244: T__53
                {
                mT__53(); 


                }
                break;
            case 41 :
                // Ecl.g:1:250: T__54
                {
                mT__54(); 


                }
                break;
            case 42 :
                // Ecl.g:1:256: T__55
                {
                mT__55(); 


                }
                break;
            case 43 :
                // Ecl.g:1:262: T__56
                {
                mT__56(); 


                }
                break;
            case 44 :
                // Ecl.g:1:268: T__57
                {
                mT__57(); 


                }
                break;
            case 45 :
                // Ecl.g:1:274: T__58
                {
                mT__58(); 


                }
                break;
            case 46 :
                // Ecl.g:1:280: T__59
                {
                mT__59(); 


                }
                break;
            case 47 :
                // Ecl.g:1:286: T__60
                {
                mT__60(); 


                }
                break;
            case 48 :
                // Ecl.g:1:292: T__61
                {
                mT__61(); 


                }
                break;
            case 49 :
                // Ecl.g:1:298: T__62
                {
                mT__62(); 


                }
                break;
            case 50 :
                // Ecl.g:1:304: T__63
                {
                mT__63(); 


                }
                break;
            case 51 :
                // Ecl.g:1:310: ILT
                {
                mILT(); 


                }
                break;
            case 52 :
                // Ecl.g:1:314: WHITESPACE
                {
                mWHITESPACE(); 


                }
                break;
            case 53 :
                // Ecl.g:1:325: LINEBREAK
                {
                mLINEBREAK(); 


                }
                break;
            case 54 :
                // Ecl.g:1:335: QUOTED_91_93
                {
                mQUOTED_91_93(); 


                }
                break;
            case 55 :
                // Ecl.g:1:348: SL_COMMENT
                {
                mSL_COMMENT(); 


                }
                break;
            case 56 :
                // Ecl.g:1:359: ML_COMMENT
                {
                mML_COMMENT(); 


                }
                break;
            case 57 :
                // Ecl.g:1:370: TYPE_LITERAL
                {
                mTYPE_LITERAL(); 


                }
                break;
            case 58 :
                // Ecl.g:1:383: REAL_LITERAL
                {
                mREAL_LITERAL(); 


                }
                break;
            case 59 :
                // Ecl.g:1:396: TEXT
                {
                mTEXT(); 


                }
                break;
            case 60 :
                // Ecl.g:1:401: QUOTED_34_34
                {
                mQUOTED_34_34(); 


                }
                break;

        }

    }


    protected DFA23 dfa23 = new DFA23(this);
    static final String DFA23_eotS =
        "\1\uffff\1\52\1\uffff\1\54\1\uffff\1\57\1\62\1\uffff\1\65\2\uffff"+
        "\1\67\1\71\1\73\1\uffff\3\47\1\101\12\47\2\uffff\1\121\3\uffff\4"+
        "\47\11\uffff\1\127\13\uffff\5\47\1\uffff\7\47\1\146\7\47\2\uffff"+
        "\4\47\1\uffff\1\164\1\165\2\47\1\170\10\47\1\u0082\1\uffff\3\47"+
        "\1\u0086\2\47\1\u0089\1\47\1\121\4\47\2\uffff\2\47\1\uffff\5\47"+
        "\1\uffff\1\47\1\uffff\1\u0097\1\uffff\3\47\1\uffff\1\47\1\u009c"+
        "\1\uffff\2\47\1\u009f\4\47\1\u00a4\5\47\1\uffff\4\47\1\uffff\2\47"+
        "\1\uffff\4\47\1\uffff\3\47\1\u00b7\7\47\1\u009f\5\47\1\u00c4\1\uffff"+
        "\5\47\2\u009f\1\47\1\u00cb\1\47\1\u00cd\1\47\1\uffff\1\47\1\u00d0"+
        "\1\u00d1\1\u00d2\1\u00d3\1\u00d4\1\uffff\1\47\1\uffff\2\47\5\uffff"+
        "\1\47\1\u00d9\1\47\2\uffff\3\47\1\uffff";
    static final String DFA23_eofS =
        "\u00de\uffff";
    static final String DFA23_minS =
        "\1\11\1\75\1\uffff\1\76\1\uffff\1\53\1\55\1\uffff\1\52\2\uffff\3"+
        "\75\1\uffff\1\156\1\143\1\156\1\55\1\141\1\155\1\141\1\157\2\162"+
        "\1\145\1\151\1\162\1\141\2\uffff\1\55\3\uffff\1\157\1\156\1\145"+
        "\1\164\11\uffff\1\55\13\uffff\1\144\2\155\1\145\1\154\1\uffff\1"+
        "\162\1\160\1\170\1\164\1\156\1\144\1\164\1\55\1\157\1\161\1\156"+
        "\1\146\1\165\1\162\1\55\2\uffff\1\157\1\164\1\141\1\162\1\uffff"+
        "\2\55\1\160\1\164\1\55\1\162\1\163\1\144\1\154\1\72\1\141\1\72\1"+
        "\145\1\55\1\uffff\1\166\1\165\1\157\1\55\1\164\1\145\1\55\1\60\1"+
        "\55\1\154\1\145\1\154\1\151\2\uffff\1\157\1\162\1\uffff\1\147\1"+
        "\145\1\167\1\145\1\162\1\uffff\1\160\1\uffff\1\55\1\uffff\2\151"+
        "\1\165\1\uffff\1\167\1\55\1\uffff\1\145\1\147\1\55\2\156\1\141\1"+
        "\171\1\55\1\141\1\155\1\145\1\164\1\141\1\uffff\1\144\2\162\1\141"+
        "\1\uffff\1\141\1\145\1\uffff\1\147\1\145\1\143\1\122\1\uffff\1\162"+
        "\1\145\1\163\1\55\1\162\2\145\1\143\1\162\1\156\1\162\1\55\1\156"+
        "\1\164\1\141\1\145\1\156\1\55\1\uffff\1\141\2\163\2\145\2\55\1\164"+
        "\1\55\1\164\1\55\1\164\1\uffff\1\155\5\55\1\uffff\1\145\1\uffff"+
        "\1\163\1\145\5\uffff\1\72\1\55\1\164\2\uffff\1\145\1\162\1\72\1"+
        "\uffff";
    static final String DFA23_maxS =
        "\1\175\1\75\1\uffff\1\76\1\uffff\1\75\1\172\1\uffff\1\57\2\uffff"+
        "\3\75\1\uffff\1\156\1\157\1\156\1\172\1\141\1\155\2\157\2\162\1"+
        "\145\1\157\1\162\1\141\2\uffff\1\172\3\uffff\1\157\1\156\1\145\1"+
        "\164\11\uffff\1\172\13\uffff\1\144\1\155\1\163\1\145\1\154\1\uffff"+
        "\1\162\1\160\1\170\1\164\1\156\1\144\1\164\1\172\1\157\1\163\1\156"+
        "\1\146\1\165\1\162\1\71\2\uffff\1\157\1\164\1\141\1\162\1\uffff"+
        "\2\172\1\160\1\164\1\172\1\162\1\163\1\144\1\157\1\72\1\141\1\72"+
        "\1\145\1\172\1\uffff\1\166\1\165\1\157\1\172\1\164\1\145\1\172\1"+
        "\71\1\172\1\154\1\145\1\154\1\151\2\uffff\1\157\1\162\1\uffff\1"+
        "\147\1\145\1\167\1\151\1\162\1\uffff\1\160\1\uffff\1\172\1\uffff"+
        "\2\151\1\165\1\uffff\1\167\1\172\1\uffff\1\145\1\147\1\172\2\156"+
        "\1\141\1\171\1\172\1\141\1\155\1\145\1\164\1\141\1\uffff\1\144\2"+
        "\162\1\141\1\uffff\1\141\1\145\1\uffff\1\147\1\145\1\143\1\122\1"+
        "\uffff\1\162\1\145\1\163\1\172\1\162\2\145\1\143\1\162\1\156\1\162"+
        "\1\172\1\156\1\164\1\141\1\145\1\156\1\172\1\uffff\1\141\2\163\2"+
        "\145\2\172\1\164\1\172\1\164\1\172\1\164\1\uffff\1\155\5\172\1\uffff"+
        "\1\145\1\uffff\1\163\1\145\5\uffff\1\72\1\172\1\164\2\uffff\1\145"+
        "\1\162\1\72\1\uffff";
    static final String DFA23_acceptS =
        "\2\uffff\1\3\1\uffff\1\6\2\uffff\1\15\1\uffff\1\17\1\20\3\uffff"+
        "\1\27\16\uffff\1\61\1\62\1\uffff\1\64\1\65\1\66\4\uffff\1\73\1\74"+
        "\1\2\1\1\1\5\1\4\1\10\1\11\1\7\1\uffff\1\14\1\12\1\67\1\70\1\16"+
        "\1\22\1\21\1\24\1\23\1\26\1\25\5\uffff\1\36\17\uffff\1\63\1\72\4"+
        "\uffff\1\13\16\uffff\1\51\15\uffff\1\30\1\31\2\uffff\1\34\5\uffff"+
        "\1\44\1\uffff\1\46\1\uffff\1\50\3\uffff\1\55\2\uffff\1\60\15\uffff"+
        "\1\47\4\uffff\1\57\2\uffff\1\71\4\uffff\1\37\22\uffff\1\43\14\uffff"+
        "\1\42\6\uffff\1\33\1\uffff\1\40\2\uffff\1\52\1\53\1\54\1\56\1\32"+
        "\3\uffff\1\35\1\41\3\uffff\1\45";
    static final String DFA23_specialS =
        "\u00de\uffff}>";
    static final String[] DFA23_transitionS = {
            "\1\40\1\41\1\uffff\1\40\1\41\22\uffff\1\40\1\1\1\50\5\uffff"+
            "\1\2\1\3\1\4\1\5\1\uffff\1\6\1\7\1\10\12\37\1\11\1\12\1\13\1"+
            "\14\1\15\2\uffff\1\47\1\43\6\47\1\44\10\47\1\45\1\46\7\47\1"+
            "\42\2\uffff\1\16\1\47\1\uffff\1\17\1\47\1\20\1\47\1\21\1\22"+
            "\1\47\1\23\1\24\3\47\1\25\1\26\1\27\1\30\1\47\1\31\1\32\1\33"+
            "\1\47\1\34\4\47\1\35\1\uffff\1\36",
            "\1\51",
            "",
            "\1\53",
            "",
            "\1\55\21\uffff\1\56",
            "\1\60\2\uffff\12\37\3\uffff\1\61\3\uffff\32\47\4\uffff\1\47"+
            "\1\uffff\32\47",
            "",
            "\1\64\4\uffff\1\63",
            "",
            "",
            "\1\66",
            "\1\70",
            "\1\72",
            "",
            "\1\74",
            "\1\75\13\uffff\1\76",
            "\1\77",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\1\100"+
            "\31\47",
            "\1\102",
            "\1\103",
            "\1\104\3\uffff\1\105\3\uffff\1\106\5\uffff\1\107",
            "\1\110",
            "\1\111",
            "\1\112",
            "\1\113",
            "\1\114\5\uffff\1\115",
            "\1\116",
            "\1\117",
            "",
            "",
            "\1\47\1\122\1\uffff\12\37\7\uffff\4\47\1\120\25\47\4\uffff"+
            "\1\47\1\uffff\4\47\1\120\25\47",
            "",
            "",
            "",
            "\1\123",
            "\1\124",
            "\1\125",
            "\1\126",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\130",
            "\1\131",
            "\1\132\1\133\4\uffff\1\134",
            "\1\135",
            "\1\136",
            "",
            "\1\137",
            "\1\140",
            "\1\141",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\147",
            "\1\150\1\uffff\1\151",
            "\1\152",
            "\1\153",
            "\1\154",
            "\1\155",
            "\1\156\2\uffff\12\157",
            "",
            "",
            "\1\160",
            "\1\161",
            "\1\162",
            "\1\163",
            "",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\166",
            "\1\167",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\171",
            "\1\172",
            "\1\173",
            "\1\174\2\uffff\1\175",
            "\1\176",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u0087",
            "\1\u0088",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\12\157",
            "\1\47\1\122\1\uffff\12\157\7\uffff\32\47\4\uffff\1\47\1\uffff"+
            "\32\47",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "\1\u008d",
            "",
            "",
            "\1\u008e",
            "\1\u008f",
            "",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093\3\uffff\1\u0094",
            "\1\u0095",
            "",
            "\1\u0096",
            "",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u0098",
            "\1\u0099",
            "\1\u009a",
            "",
            "\1\u009b",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u009d",
            "\1\u009e",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "\1\u00a8",
            "\1\u00a9",
            "",
            "\1\u00aa",
            "\1\u00ab",
            "\1\u00ac",
            "\1\u00ad",
            "",
            "\1\u00ae",
            "\1\u00af",
            "",
            "\1\u00b0",
            "\1\u00b1",
            "\1\u00b2",
            "\1\u00b3",
            "",
            "\1\u00b4",
            "\1\u00b5",
            "\1\u00b6",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00ba",
            "\1\u00bb",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\1\u00c2",
            "\1\u00c3",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u00c5",
            "\1\u00c6",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00ca",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00cc",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00ce",
            "",
            "\1\u00cf",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "",
            "\1\u00d5",
            "",
            "\1\u00d6",
            "\1\u00d7",
            "",
            "",
            "",
            "",
            "",
            "\1\u00d8",
            "\1\47\2\uffff\12\47\7\uffff\32\47\4\uffff\1\47\1\uffff\32\47",
            "\1\u00da",
            "",
            "",
            "\1\u00db",
            "\1\u00dc",
            "\1\u00dd",
            ""
    };

    static final short[] DFA23_eot = DFA.unpackEncodedString(DFA23_eotS);
    static final short[] DFA23_eof = DFA.unpackEncodedString(DFA23_eofS);
    static final char[] DFA23_min = DFA.unpackEncodedStringToUnsignedChars(DFA23_minS);
    static final char[] DFA23_max = DFA.unpackEncodedStringToUnsignedChars(DFA23_maxS);
    static final short[] DFA23_accept = DFA.unpackEncodedString(DFA23_acceptS);
    static final short[] DFA23_special = DFA.unpackEncodedString(DFA23_specialS);
    static final short[][] DFA23_transition;

    static {
        int numStates = DFA23_transitionS.length;
        DFA23_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA23_transition[i] = DFA.unpackEncodedString(DFA23_transitionS[i]);
        }
    }

    class DFA23 extends DFA {

        public DFA23(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 23;
            this.eot = DFA23_eot;
            this.eof = DFA23_eof;
            this.min = DFA23_min;
            this.max = DFA23_max;
            this.accept = DFA23_accept;
            this.special = DFA23_special;
            this.transition = DFA23_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | ILT | WHITESPACE | LINEBREAK | QUOTED_91_93 | SL_COMMENT | ML_COMMENT | TYPE_LITERAL | REAL_LITERAL | TEXT | QUOTED_34_34 );";
        }
    }
 

}