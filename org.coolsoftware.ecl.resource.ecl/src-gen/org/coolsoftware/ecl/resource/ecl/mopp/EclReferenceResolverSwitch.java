/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.mopp;

public class EclReferenceResolverSwitch implements org.coolsoftware.ecl.resource.ecl.IEclReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected org.coolsoftware.ecl.resource.ecl.analysis.CcmImportCcmStructuralModelReferenceResolver ccmImportCcmStructuralModelReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.CcmImportCcmStructuralModelReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.SWComponentContractComponentTypeReferenceResolver sWComponentContractComponentTypeReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.SWComponentContractComponentTypeReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.SWComponentContractPortReferenceResolver sWComponentContractPortReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.SWComponentContractPortReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.HWComponentContractComponentTypeReferenceResolver hWComponentContractComponentTypeReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.HWComponentContractComponentTypeReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.ProvisionClauseProvidedPropertyReferenceResolver provisionClauseProvidedPropertyReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.ProvisionClauseProvidedPropertyReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.SWComponentRequirementClauseRequiredComponentTypeReferenceResolver sWComponentRequirementClauseRequiredComponentTypeReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.SWComponentRequirementClauseRequiredComponentTypeReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.HWComponentRequirementClauseRequiredResourceTypeReferenceResolver hWComponentRequirementClauseRequiredResourceTypeReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.HWComponentRequirementClauseRequiredResourceTypeReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.PropertyRequirementClauseRequiredPropertyReferenceResolver propertyRequirementClauseRequiredPropertyReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.PropertyRequirementClauseRequiredPropertyReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.FormulaTemplateMetaparameterReferenceResolver formulaTemplateMetaparameterReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.FormulaTemplateMetaparameterReferenceResolver();
	protected org.coolsoftware.ecl.resource.ecl.analysis.VariableCallExpressionReferredVariableReferenceResolver variableCallExpressionReferredVariableReferenceResolver = new org.coolsoftware.ecl.resource.ecl.analysis.VariableCallExpressionReferredVariableReferenceResolver();
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.CcmImport, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> getCcmImportCcmStructuralModelReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getCcmImport_CcmStructuralModel(), ccmImportCcmStructuralModelReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> getSWComponentContractComponentTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_ComponentType(), sWComponentContractComponentTypeReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType> getSWComponentContractPortReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_Port(), sWComponentContractPortReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.HWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.ResourceType> getHWComponentContractComponentTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract_ComponentType(), hWComponentContractComponentTypeReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property> getProvisionClauseProvidedPropertyReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause_ProvidedProperty(), provisionClauseProvidedPropertyReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.SWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> getSWComponentRequirementClauseRequiredComponentTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause_RequiredComponentType(), sWComponentRequirementClauseRequiredComponentTypeReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.HWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.ResourceType> getHWComponentRequirementClauseRequiredResourceTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause_RequiredResourceType(), hWComponentRequirementClauseRequiredResourceTypeReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.PropertyRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.Property> getPropertyRequirementClauseRequiredPropertyReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause_RequiredProperty(), propertyRequirementClauseRequiredPropertyReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter> getFormulaTemplateMetaparameterReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getFormulaTemplate_Metaparameter(), formulaTemplateMetaparameterReferenceResolver);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable> getVariableCallExpressionReferredVariableReferenceResolver() {
		return getResolverChain(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression_ReferredVariable(), variableCallExpressionReferredVariableReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		ccmImportCcmStructuralModelReferenceResolver.setOptions(options);
		sWComponentContractComponentTypeReferenceResolver.setOptions(options);
		sWComponentContractPortReferenceResolver.setOptions(options);
		hWComponentContractComponentTypeReferenceResolver.setOptions(options);
		provisionClauseProvidedPropertyReferenceResolver.setOptions(options);
		sWComponentRequirementClauseRequiredComponentTypeReferenceResolver.setOptions(options);
		hWComponentRequirementClauseRequiredResourceTypeReferenceResolver.setOptions(options);
		propertyRequirementClauseRequiredPropertyReferenceResolver.setOptions(options);
		formulaTemplateMetaparameterReferenceResolver.setOptions(options);
		variableCallExpressionReferredVariableReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getCcmImport().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("ccmStructuralModel")) {
				ccmImportCcmStructuralModelReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.CcmImport) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("componentType")) {
				sWComponentContractComponentTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.SWComponentContract) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.PortType> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.PortType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("port")) {
				sWComponentContractPortReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.SWComponentContract) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("componentType")) {
				hWComponentContractComponentTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.HWComponentContract) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("providedProperty")) {
				provisionClauseProvidedPropertyReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.ProvisionClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("requiredComponentType")) {
				sWComponentRequirementClauseRequiredComponentTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.SWComponentRequirementClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("requiredResourceType")) {
				hWComponentRequirementClauseRequiredResourceTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.HWComponentRequirementClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("requiredProperty")) {
				propertyRequirementClauseRequiredPropertyReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.PropertyRequirementClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getFormulaTemplate().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Parameter> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Parameter>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("metaparameter")) {
				formulaTemplateMetaparameterReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.FormulaTemplate) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression().isInstance(container)) {
			EclFuzzyResolveResult<org.coolsoftware.coolcomponents.expressions.variables.Variable> frr = new EclFuzzyResolveResult<org.coolsoftware.coolcomponents.expressions.variables.Variable>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("referredVariable")) {
				variableCallExpressionReferredVariableReferenceResolver.resolve(identifier, (org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getCcmImport_CcmStructuralModel()) {
			return getResolverChain(reference, ccmImportCcmStructuralModelReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_ComponentType()) {
			return getResolverChain(reference, sWComponentContractComponentTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_Port()) {
			return getResolverChain(reference, sWComponentContractPortReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract_ComponentType()) {
			return getResolverChain(reference, hWComponentContractComponentTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause_ProvidedProperty()) {
			return getResolverChain(reference, provisionClauseProvidedPropertyReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause_RequiredComponentType()) {
			return getResolverChain(reference, sWComponentRequirementClauseRequiredComponentTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause_RequiredResourceType()) {
			return getResolverChain(reference, hWComponentRequirementClauseRequiredResourceTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause_RequiredProperty()) {
			return getResolverChain(reference, propertyRequirementClauseRequiredPropertyReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getFormulaTemplate_Metaparameter()) {
			return getResolverChain(reference, formulaTemplateMetaparameterReferenceResolver);
		}
		if (reference == org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression_ReferredVariable()) {
			return getResolverChain(reference, variableCallExpressionReferredVariableReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(org.coolsoftware.ecl.resource.ecl.IEclOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new org.coolsoftware.ecl.resource.ecl.util.EclRuntimeUtil().logWarning("Found value with invalid type for option " + org.coolsoftware.ecl.resource.ecl.IEclOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver) {
			org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver replacingResolver = (org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver) resolverValue;
			if (replacingResolver instanceof org.coolsoftware.ecl.resource.ecl.IEclDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((org.coolsoftware.ecl.resource.ecl.IEclDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof org.coolsoftware.ecl.resource.ecl.IEclReferenceCache) {
					org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver nextResolver = (org.coolsoftware.ecl.resource.ecl.IEclReferenceResolver) next;
					if (nextResolver instanceof org.coolsoftware.ecl.resource.ecl.IEclDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((org.coolsoftware.ecl.resource.ecl.IEclDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new org.coolsoftware.ecl.resource.ecl.util.EclRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.ecl.resource.ecl.IEclOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.ecl.resource.ecl.IEclDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new org.coolsoftware.ecl.resource.ecl.util.EclRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.ecl.resource.ecl.IEclOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.ecl.resource.ecl.IEclDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
