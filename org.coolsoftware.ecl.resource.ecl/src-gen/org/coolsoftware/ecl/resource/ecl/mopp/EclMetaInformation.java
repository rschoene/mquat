/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.mopp;

public class EclMetaInformation implements org.coolsoftware.ecl.resource.ecl.IEclMetaInformation {
	
	public String getSyntaxName() {
		return "ecl";
	}
	
	public String getURI() {
		return "http://www.cool-software.org/ecl";
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTextScanner createLexer() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclAntlrScanner(new org.coolsoftware.ecl.resource.ecl.mopp.EclLexer());
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclParser().createInstance(inputStream, encoding);
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTextPrinter createPrinter(java.io.OutputStream outputStream, org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclPrinter(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclReferenceResolverSwitch getReferenceResolverSwitch() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclReferenceResolverSwitch();
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTokenResolverFactory getTokenResolverFactory() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "org.coolsoftware.ecl/metamodel/ecl.cs";
	}
	
	public String[] getTokenNames() {
		return org.coolsoftware.ecl.resource.ecl.mopp.EclParser.tokenNames;
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTokenStyle getDefaultTokenStyle(String tokenName) {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclBracketPair> getBracketPairs() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclResourceFactory();
	}
	
	public org.coolsoftware.ecl.resource.ecl.mopp.EclNewFileContentProvider getNewFileContentProvider() {
		return new org.coolsoftware.ecl.resource.ecl.mopp.EclNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new org.coolsoftware.ecl.resource.ecl.mopp.EclResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "org.coolsoftware.ecl.resource.ecl.ui.launchConfigurationType";
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclNameProvider createNameProvider() {
		return new org.coolsoftware.ecl.resource.ecl.analysis.EclDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		org.coolsoftware.ecl.resource.ecl.mopp.EclAntlrTokenHelper tokenHelper = new org.coolsoftware.ecl.resource.ecl.mopp.EclAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
