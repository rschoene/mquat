grammar Ecl;

options {
	superClass = EclANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package org.coolsoftware.ecl.resource.ecl.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package org.coolsoftware.ecl.resource.ecl.mopp;
}

@members{
	private org.coolsoftware.ecl.resource.ecl.IEclTokenResolverFactory tokenResolverFactory = new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new org.coolsoftware.ecl.resource.ecl.IEclProblem() {
					public org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity getSeverity() {
						return org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity.ERROR;
					}
					public org.coolsoftware.ecl.resource.ecl.EclEProblemType getType() {
						return org.coolsoftware.ecl.resource.ecl.EclEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		org.coolsoftware.ecl.resource.ecl.IEclExpectedElement terminal = org.coolsoftware.ecl.resource.ecl.grammar.EclFollowSetProvider.TERMINALS[terminalID];
		org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[] containmentFeatures = new org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = org.coolsoftware.ecl.resource.ecl.grammar.EclFollowSetProvider.LINKS[ids[i]];
		}
		org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace containmentTrace = new org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace(eClass, containmentFeatures);
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal expectedElement = new org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
				org.coolsoftware.ecl.resource.ecl.IEclLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
				org.coolsoftware.ecl.resource.ecl.IEclLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
				org.coolsoftware.ecl.resource.ecl.IEclLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new EclParser(new org.antlr.runtime3_4_0.CommonTokenStream(new EclLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new EclParser(new org.antlr.runtime3_4_0.CommonTokenStream(new EclLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new org.coolsoftware.ecl.resource.ecl.util.EclRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public EclParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((EclLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((EclLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == org.coolsoftware.ecl.EclFile.class) {
				return parse_org_coolsoftware_ecl_EclFile();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.CcmImport.class) {
				return parse_org_coolsoftware_ecl_CcmImport();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentContract.class) {
				return parse_org_coolsoftware_ecl_SWComponentContract();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentContract.class) {
				return parse_org_coolsoftware_ecl_HWComponentContract();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SWContractMode.class) {
				return parse_org_coolsoftware_ecl_SWContractMode();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.HWContractMode.class) {
				return parse_org_coolsoftware_ecl_HWContractMode();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.ProvisionClause.class) {
				return parse_org_coolsoftware_ecl_ProvisionClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentRequirementClause.class) {
				return parse_org_coolsoftware_ecl_SWComponentRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentRequirementClause.class) {
				return parse_org_coolsoftware_ecl_HWComponentRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SelfRequirementClause.class) {
				return parse_org_coolsoftware_ecl_SelfRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.PropertyRequirementClause.class) {
				return parse_org_coolsoftware_ecl_PropertyRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.FormulaTemplate.class) {
				return parse_org_coolsoftware_ecl_FormulaTemplate();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.Metaparameter.class) {
				return parse_org_coolsoftware_ecl_Metaparameter();
			}
			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.Block.class) {
				return parse_org_coolsoftware_coolcomponents_expressions_Block();
			}
			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.variables.Variable.class) {
				return parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();
			}
		}
		throw new org.coolsoftware.ecl.resource.ecl.mopp.EclUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(org.coolsoftware.ecl.resource.ecl.IEclOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public org.coolsoftware.ecl.resource.ecl.IEclParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>>();
		org.coolsoftware.ecl.resource.ecl.mopp.EclParseResult parseResult = new org.coolsoftware.ecl.resource.ecl.mopp.EclParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.ecl.resource.ecl.IEclTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		org.coolsoftware.ecl.resource.ecl.IEclParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal>();
		java.util.List<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 138;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<org.coolsoftware.ecl.resource.ecl.util.EclPair<org.coolsoftware.ecl.resource.ecl.IEclExpectedElement, org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (org.coolsoftware.ecl.resource.ecl.util.EclPair<org.coolsoftware.ecl.resource.ecl.IEclExpectedElement, org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[]> newFollowerPair : newFollowers) {
							org.coolsoftware.ecl.resource.ecl.IEclExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace containmentTrace = new org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace(null, newFollowerPair.getRight());
							org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal newFollowTerminal = new org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[0]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[2]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_org_coolsoftware_ecl_EclFile{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_org_coolsoftware_ecl_EclFile returns [org.coolsoftware.ecl.EclFile element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_coolsoftware_ecl_Import			{
				if (terminateParsing) {
					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__IMPORTS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_0_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[3]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[4]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[5]);
	}
	
	(
		(
			a1_0 = parse_org_coolsoftware_ecl_EclContract			{
				if (terminateParsing) {
					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
					startIncompleteElement(element);
				}
				if (a1_0 != null) {
					if (a1_0 != null) {
						Object value = a1_0;
						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__CONTRACTS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_0_0_0_2, a1_0, true);
					copyLocalizationInfos(a1_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[6]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[7]);
	}
	
;

parse_org_coolsoftware_ecl_CcmImport returns [org.coolsoftware.ecl.CcmImport element = null]
@init{
}
:
	a0 = 'import' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_1_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[8]);
	}
	
	a1 = 'ccm' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_1_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[9]);
	}
	
	(
		a2 = QUOTED_91_93		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_91_93");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.StructuralModel proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createStructuralModel();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.CcmImport, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getCcmImportCcmStructuralModelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_1_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[10]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[11]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[12]);
	}
	
;

parse_org_coolsoftware_ecl_SWComponentContract returns [org.coolsoftware.ecl.SWComponentContract element = null]
@init{
}
:
	a0 = 'contract' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[13]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[14]);
	}
	
	a2 = 'implements' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[15]);
	}
	
	a3 = 'software' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[16]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_8, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[17]);
	}
	
	a5 = '.' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[18]);
	}
	
	(
		a6 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
				startIncompleteElement(element);
			}
			if (a6 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.PortType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createPortType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractPortReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_10, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[19]);
	}
	
	a7 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_11, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[20]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[21]);
	}
	
	(
		(
			a8 = 'metaparameter:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_13_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[22]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[23]);
			}
			
			(
				(
					(
						a9_0 = parse_org_coolsoftware_ecl_Metaparameter						{
							if (terminateParsing) {
								throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
							}
							if (element == null) {
								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
								startIncompleteElement(element);
							}
							if (a9_0 != null) {
								if (a9_0 != null) {
									Object value = a9_0;
									addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__METAPARAMS, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_13_0_0_1_0_0_0, a9_0, true);
								copyLocalizationInfos(a9_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[24]);
						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[25]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[26]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[27]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[28]);
	}
	
	(
		(
			(
				a10_0 = parse_org_coolsoftware_ecl_SWContractMode				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
						startIncompleteElement(element);
					}
					if (a10_0 != null) {
						if (a10_0 != null) {
							Object value = a10_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__MODES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_15_0_0_0, a10_0, true);
						copyLocalizationInfos(a10_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[29]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[30]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[31]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[32]);
	}
	
	a11 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_16, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[33]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[34]);
	}
	
;

parse_org_coolsoftware_ecl_HWComponentContract returns [org.coolsoftware.ecl.HWComponentContract element = null]
@init{
}
:
	a0 = 'contract' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[35]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[36]);
	}
	
	a2 = 'implements' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[37]);
	}
	
	a3 = 'hardware' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[38]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_8, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[39]);
	}
	
	a5 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[40]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[41]);
	}
	
	(
		(
			a6 = 'metaparameter:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_12_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[42]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[43]);
			}
			
			(
				(
					(
						a7_0 = parse_org_coolsoftware_ecl_Metaparameter						{
							if (terminateParsing) {
								throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
							}
							if (element == null) {
								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
								startIncompleteElement(element);
							}
							if (a7_0 != null) {
								if (a7_0 != null) {
									Object value = a7_0;
									addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__METAPARAMS, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_12_0_0_1_0_0_0, a7_0, true);
								copyLocalizationInfos(a7_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[44]);
						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[45]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[46]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[47]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[48]);
	}
	
	(
		(
			(
				a8_0 = parse_org_coolsoftware_ecl_HWContractMode				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
						startIncompleteElement(element);
					}
					if (a8_0 != null) {
						if (a8_0 != null) {
							Object value = a8_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__MODES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_14_0_0_0, a8_0, true);
						copyLocalizationInfos(a8_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[49]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[50]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[51]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[52]);
	}
	
	a9 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_15, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[53]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[54]);
	}
	
;

parse_org_coolsoftware_ecl_SWContractMode returns [org.coolsoftware.ecl.SWContractMode element = null]
@init{
}
:
	a0 = 'mode' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[55]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[56]);
	}
	
	a2 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[57]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[58]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[59]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[60]);
	}
	
	(
		(
			(
				a3_0 = parse_org_coolsoftware_ecl_SWContractClause				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__CLAUSES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_6_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[61]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[62]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[63]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[64]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[65]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[66]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[67]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[68]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[69]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[70]);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[71]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[72]);
	}
	
;

parse_org_coolsoftware_ecl_HWContractMode returns [org.coolsoftware.ecl.HWContractMode element = null]
@init{
}
:
	a0 = 'mode' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[73]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[74]);
	}
	
	a2 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[75]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[76]);
	}
	
	(
		(
			(
				a3_0 = parse_org_coolsoftware_ecl_HWContractClause				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__CLAUSES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_6_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[77]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[78]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[79]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[80]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[81]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[82]);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[83]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[84]);
	}
	
;

parse_org_coolsoftware_ecl_ProvisionClause returns [org.coolsoftware.ecl.ProvisionClause element = null]
@init{
}
:
	a0 = 'provides' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[85]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getProvisionClauseProvidedPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[86]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[87]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[88]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[89]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[90]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[91]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[92]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[93]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[94]);
	}
	
	(
		(
			a2 = 'min:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_4_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[95]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[96]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[97]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[98]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[99]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[100]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[101]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[102]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[103]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[104]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[105]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[106]);
			}
			
			(
				a3_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MIN_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_4_0_0_2, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[107]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[108]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[109]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[110]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[111]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[112]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[113]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[114]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[115]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[116]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[117]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[118]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[119]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[120]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[121]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[122]);
	}
	
	(
		(
			a4 = 'max:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[123]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[124]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[125]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[126]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[127]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[128]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[129]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[130]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[131]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[132]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[133]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[134]);
			}
			
			(
				a5_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
						startIncompleteElement(element);
					}
					if (a5_0 != null) {
						if (a5_0 != null) {
							Object value = a5_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MAX_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_5_0_0_2, a5_0, true);
						copyLocalizationInfos(a5_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[135]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[136]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[137]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[138]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[139]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[140]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[141]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[142]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[143]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[144]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[145]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[146]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[147]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[148]);
	}
	
	(
		(
			a6_0 = parse_org_coolsoftware_ecl_FormulaTemplate			{
				if (terminateParsing) {
					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
					startIncompleteElement(element);
				}
				if (a6_0 != null) {
					if (a6_0 != null) {
						Object value = a6_0;
						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__FORMULA), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_6, a6_0, true);
					copyLocalizationInfos(a6_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[149]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[150]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[151]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[152]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[153]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[154]);
	}
	
;

parse_org_coolsoftware_ecl_SWComponentRequirementClause returns [org.coolsoftware.ecl.SWComponentRequirementClause element = null]
@init{
}
:
	a0 = 'requires' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[155]);
	}
	
	a1 = 'component' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[156]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentRequirementClauseRequiredComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[157]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[158]);
	}
	
	(
		(
			a3 = '{' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_0_0_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[159]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[160]);
			}
			
			(
				(
					(
						a4_0 = parse_org_coolsoftware_ecl_PropertyRequirementClause						{
							if (terminateParsing) {
								throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
							}
							if (element == null) {
								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
								startIncompleteElement(element);
							}
							if (a4_0 != null) {
								if (a4_0 != null) {
									Object value = a4_0;
									addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_0_0_0_0_2_0_0_0, a4_0, true);
								copyLocalizationInfos(a4_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[161]);
						addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[162]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[163]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[164]);
			}
			
			a5 = '}' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_0_0_0_0_3, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[165]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[166]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[167]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[168]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[169]);
			}
			
		)
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[170]);
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[171]);
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[172]);
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[173]);
			addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[174]);
		}
		
		
		|		a6 = ';' {
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_1_0, null, true);
			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[175]);
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[176]);
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[177]);
			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[178]);
			addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[179]);
		}
		
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[180]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[181]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[182]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[183]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[184]);
	}
	
;

parse_org_coolsoftware_ecl_HWComponentRequirementClause returns [org.coolsoftware.ecl.HWComponentRequirementClause element = null]
@init{
}
:
	a0 = 'requires' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[185]);
	}
	
	a1 = 'resource' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[186]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentRequirementClauseRequiredResourceTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[187]);
	}
	
	a3 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[188]);
	}
	
	(
		(
			(
				a4_0 = parse_org_coolsoftware_ecl_PropertyRequirementClause				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_8_0_0_0, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[189]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[190]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[191]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[192]);
	}
	
	a5 = 'energyRate: ' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[193]);
	}
	
	(
		a6 = REAL_LITERAL		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
				startIncompleteElement(element);
			}
			if (a6 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
				}
				java.lang.Double resolved = (java.lang.Double) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_11, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[194]);
	}
	
	a7 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_13, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[195]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[196]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[197]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[198]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[199]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[200]);
	}
	
;

parse_org_coolsoftware_ecl_SelfRequirementClause returns [org.coolsoftware.ecl.SelfRequirementClause element = null]
@init{
}
:
	a0 = 'requires' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_9_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSelfRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[201]);
	}
	
	(
		a1_0 = parse_org_coolsoftware_ecl_PropertyRequirementClause		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_9_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[202]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[203]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[204]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[205]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[206]);
	}
	
;

parse_org_coolsoftware_ecl_PropertyRequirementClause returns [org.coolsoftware.ecl.PropertyRequirementClause element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.PropertyRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPropertyRequirementClauseRequiredPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_0, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[207]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[208]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[209]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[210]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[211]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[212]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[213]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[214]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[215]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[216]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[217]);
	}
	
	(
		(
			a1 = 'min:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[218]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[219]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[220]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[221]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[222]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[223]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[224]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[225]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[226]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[227]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[228]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[229]);
			}
			
			(
				a2_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
						startIncompleteElement(element);
					}
					if (a2_0 != null) {
						if (a2_0 != null) {
							Object value = a2_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_2_0_0_2, a2_0, true);
						copyLocalizationInfos(a2_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[230]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[231]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[232]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[233]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[234]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[235]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[236]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[237]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[238]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[239]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[240]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[241]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[242]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[243]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[244]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[245]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[246]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[247]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[248]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[249]);
	}
	
	(
		(
			a3 = 'max:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[250]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[251]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[252]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[253]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[254]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[255]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[256]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[257]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[258]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[259]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[260]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[261]);
			}
			
			(
				a4_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_3_0_0_2, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[262]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[263]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[264]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[265]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[266]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[267]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[268]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[269]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[270]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[271]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[272]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[273]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[274]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[275]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[276]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[277]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[278]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[279]);
	}
	
	(
		(
			a5_0 = parse_org_coolsoftware_ecl_FormulaTemplate			{
				if (terminateParsing) {
					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
					startIncompleteElement(element);
				}
				if (a5_0 != null) {
					if (a5_0 != null) {
						Object value = a5_0;
						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_4, a5_0, true);
					copyLocalizationInfos(a5_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[280]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[281]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[282]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[283]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[284]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[285]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[286]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[287]);
	}
	
;

parse_org_coolsoftware_ecl_FormulaTemplate returns [org.coolsoftware.ecl.FormulaTemplate element = null]
@init{
}
:
	a0 = '<' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[288]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[289]);
	}
	
	a2 = '(' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[290]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[291]);
	}
	
	(
		(
			(
				a3 = TEXT				
				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.coolsoftware.coolcomponents.ccm.structure.Parameter proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createParameter();
						collectHiddenTokens(element);
						registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFormulaTemplateMetaparameterReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_3_0_0_0, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[292]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[293]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[294]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[295]);
	}
	
	a4 = ')>' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[296]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[297]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[298]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[299]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[300]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[301]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[302]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[303]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[304]);
	}
	
;

parse_org_coolsoftware_ecl_Metaparameter returns [org.coolsoftware.ecl.Metaparameter element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createMetaparameter();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_12_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[305]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[306]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[307]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[308]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[309]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[310]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[311]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[312]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[313]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[314]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[315]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[316]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[317]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[318]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[319]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[320]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[321]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[322]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[323]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[324]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[325]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[326]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[327]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[328]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[329]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[330]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[331]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[332]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[333]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[334]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[335]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[336]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[337]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[338]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[339]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[340]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[341]);
	}
	
;

parse_org_coolsoftware_coolcomponents_expressions_Block returns [org.coolsoftware.coolcomponents.expressions.Block element = null]
@init{
}
:
	a0 = '{' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[342]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[343]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[344]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[345]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[346]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[347]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[348]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[349]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[350]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[351]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[352]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[353]);
	}
	
	(
		a1_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[354]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[355]);
	}
	
	(
		(
			a2 = ';' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_4_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[356]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[357]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[358]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[359]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[360]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[361]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[362]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[363]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[364]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[365]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[366]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[367]);
			}
			
			(
				a3_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_4_0_0_2, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[368]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[369]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[370]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[371]);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
	}
	
;

parse_org_coolsoftware_coolcomponents_expressions_variables_Variable returns [org.coolsoftware.coolcomponents.expressions.variables.Variable element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[372]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[373]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[374]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[375]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[376]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[377]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[378]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[379]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[380]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[381]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[382]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[383]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[384]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[385]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[386]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[387]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[388]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[389]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[390]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[391]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[392]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[393]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[394]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[395]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[396]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[397]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[398]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[399]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[400]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[401]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[402]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[403]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[404]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[405]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[406]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[407]);
	}
	
	(
		(
			a1 = ':' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_1_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[408]);
			}
			
			(
				a2 = TYPE_LITERAL				
				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
						startIncompleteElement(element);
					}
					if (a2 != null) {
						org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TYPE_LITERAL");
						tokenResolver.setOptions(getOptions());
						org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
						}
						org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral resolved = (org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_1_0_0_3, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[409]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[410]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[411]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[412]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[413]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[414]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[415]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[416]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[417]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[418]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[419]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[420]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[421]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[422]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[423]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[424]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[425]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[426]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[427]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[428]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[429]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[430]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[431]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[432]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[433]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[434]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[435]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[436]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[437]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[438]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[439]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[440]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[441]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[442]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[443]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[444]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[445]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[446]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[447]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[448]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[449]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[450]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[451]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[452]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[453]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[454]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[455]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[456]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[457]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[458]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[459]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[460]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[461]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[462]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[463]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[464]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[465]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[466]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[467]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[468]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[469]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[470]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[471]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[472]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[473]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[474]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[475]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[476]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[477]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[478]);
	}
	
	(
		(
			a3 = '=' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[479]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[480]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[481]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[482]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[483]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[484]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[485]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[486]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[487]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[488]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[489]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[490]);
			}
			
			(
				a4_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_2_0_0_3, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[491]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[492]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[493]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[494]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[495]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[496]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[497]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[498]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[499]);
				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[500]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[501]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[502]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[503]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[504]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[505]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[506]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[507]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[508]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[509]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[510]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[511]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[512]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[513]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[514]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[515]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[516]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[517]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[518]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[519]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[520]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[521]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[522]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[523]);
				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[524]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[525]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[526]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[527]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[528]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[529]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[530]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[531]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[532]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[533]);
		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[534]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[535]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[536]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[537]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[538]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[539]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[540]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[541]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[542]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[543]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[544]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[545]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[546]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[547]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[548]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[549]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[550]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[551]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[552]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[553]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[554]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[555]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[556]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[557]);
		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[558]);
	}
	
	|//derived choice rules for sub-classes: 
	
	c0 = parse_org_coolsoftware_ecl_Metaparameter{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parseop_Expression_level_03 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
	leftArg = parseop_Expression_level_04	((
		()
		{ element = null; }
		a0 = 'or' {
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_25_0_0_2, null, true);
			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[559]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[560]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[561]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[562]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[563]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[564]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[565]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[566]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[567]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[568]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[569]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[570]);
		}
		
		rightArg = parseop_Expression_level_04		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_25_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_25_0_0_4, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
		|		
		()
		{ element = null; }
		a0 = 'and' {
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_26_0_0_2, null, true);
			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[571]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[572]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[573]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[574]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[575]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[576]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[577]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[578]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[579]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[580]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[581]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[582]);
		}
		
		rightArg = parseop_Expression_level_04		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_26_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_26_0_0_4, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
	)+ | /* epsilon */ { element = leftArg; }
	
)
;

parseop_Expression_level_04 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_5((
	()
	{ element = null; }
	a0 = 'implies' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_27_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[583]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[584]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[585]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[586]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[587]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[588]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[589]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[590]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[591]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[592]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[593]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[594]);
	}
	
	rightArg = parseop_Expression_level_5	{
		if (terminateParsing) {
			throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
		}
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		if (leftArg != null) {
			if (leftArg != null) {
				Object value = leftArg;
				element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_27_0_0_0, leftArg, true);
			copyLocalizationInfos(leftArg, element);
		}
	}
	{
		if (terminateParsing) {
			throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
		}
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		if (rightArg != null) {
			if (rightArg != null) {
				Object value = rightArg;
				addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_27_0_0_4, rightArg, true);
			copyLocalizationInfos(rightArg, element);
		}
	}
	{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_5 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'f' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_11_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[595]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[596]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[597]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[598]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[599]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[600]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[601]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[602]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[603]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[604]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[605]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[606]);
}

arg = parseop_Expression_level_6{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
	startIncompleteElement(element);
}
if (arg != null) {
	if (arg != null) {
		Object value = arg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_11_0_0_2, arg, true);
	copyLocalizationInfos(arg, element);
}
}
|
a0 = '!' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_28_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[607]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[608]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[609]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[610]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[611]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[612]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[613]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[614]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[615]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[616]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[617]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[618]);
}

arg = parseop_Expression_level_6{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
	startIncompleteElement(element);
}
if (arg != null) {
	if (arg != null) {
		Object value = arg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_28_0_0_2, arg, true);
	copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_6{ element = arg; }
;

parseop_Expression_level_6 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_11((
()
{ element = null; }
a0 = '=' {
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_32_0_0_2, null, true);
	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[619]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[620]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[621]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[622]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[623]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[624]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[625]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[626]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[627]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[628]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[629]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[630]);
}

rightArg = parseop_Expression_level_11{
	if (terminateParsing) {
		throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_32_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_32_0_0_4, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_11 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_12((
()
{ element = null; }
a0 = '>' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_19_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[631]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[632]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[633]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[634]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[635]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[636]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[637]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[638]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[639]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[640]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[641]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[642]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_19_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_19_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '>=' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_20_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[643]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[644]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[645]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[646]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[647]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[648]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[649]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[650]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[651]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[652]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[653]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[654]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_20_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_20_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '<' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_21_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[655]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[656]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[657]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[658]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[659]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[660]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[661]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[662]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[663]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[664]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[665]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[666]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_21_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_21_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '<=' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_22_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[667]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[668]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[669]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[670]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[671]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[672]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[673]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[674]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[675]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[676]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[677]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[678]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_22_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_22_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_12 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_19((
()
{ element = null; }
a0 = '==' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_23_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[679]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[680]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[681]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[682]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[683]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[684]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[685]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[686]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[687]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[688]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[689]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[690]);
}

rightArg = parseop_Expression_level_19{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
	Object value = leftArg;
	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_23_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
	Object value = rightArg;
	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_23_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '!=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_24_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[691]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[692]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[693]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[694]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[695]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[696]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[697]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[698]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[699]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[700]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[701]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[702]);
}

rightArg = parseop_Expression_level_19{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
	Object value = leftArg;
	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_24_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
	Object value = rightArg;
	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_24_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_19 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_21((
()
{ element = null; }
a0 = '^' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_18_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[703]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[704]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[705]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[706]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[707]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[708]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[709]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[710]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[711]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[712]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[713]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[714]);
}

rightArg = parseop_Expression_level_21{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_18_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_18_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_21 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_22((
()
{ element = null; }
a0 = '+' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_12_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[715]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[716]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[717]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[718]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[719]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[720]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[721]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[722]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[723]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[724]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[725]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[726]);
}

rightArg = parseop_Expression_level_22{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_12_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_12_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '-' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_13_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[727]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[728]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[729]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[730]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[731]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[732]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[733]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[734]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[735]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[736]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[737]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[738]);
}

rightArg = parseop_Expression_level_22{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_13_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_13_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_22 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_80((
()
{ element = null; }
a0 = '*' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_16_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[739]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[740]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[741]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[742]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[743]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[744]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[745]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[746]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[747]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[748]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[749]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[750]);
}

rightArg = parseop_Expression_level_80{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_16_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_16_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '/' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_17_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[751]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[752]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[753]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[754]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[755]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[756]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[757]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[758]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[759]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[760]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[761]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[762]);
}

rightArg = parseop_Expression_level_80{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_17_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_17_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_80 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'sin' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_9_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[763]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[764]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[765]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[766]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[767]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[768]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[769]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[770]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[771]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[772]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[773]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[774]);
}

arg = parseop_Expression_level_87{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_9_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
a0 = 'cos' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_10_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[775]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[776]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[777]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[778]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[779]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[780]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[781]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[782]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[783]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[784]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[785]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[786]);
}

arg = parseop_Expression_level_87{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_10_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_87{ element = arg; }
;

parseop_Expression_level_87 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'not' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_8_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[787]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[788]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[789]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[790]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[791]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[792]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[793]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[794]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[795]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[796]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[797]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[798]);
}

arg = parseop_Expression_level_88{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_8_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_88{ element = arg; }
;

parseop_Expression_level_88 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
arg = parseop_Expression_level_90(
a0 = '++' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_6_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[799]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[800]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[801]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[802]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[803]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[804]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[805]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[806]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[807]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[808]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[809]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[810]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[811]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[812]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[813]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[814]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[815]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[816]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[817]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[818]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[819]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[820]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[821]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[822]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[823]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[824]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[825]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[826]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[827]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[828]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[829]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[830]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[831]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[832]);
}

{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_6_0_0_0, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
a0 = '--' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_7_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[833]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[834]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[835]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[836]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[837]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[838]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[839]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[840]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[841]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[842]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[843]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[844]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[845]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[846]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[847]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[848]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[849]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[850]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[851]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[852]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[853]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[854]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[855]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[856]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[857]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[858]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[859]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[860]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[861]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[862]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[863]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[864]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[865]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[866]);
}

{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_7_0_0_0, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
/* epsilon */ { element = arg; } 
)
;

parseop_Expression_level_90 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_91((
()
{ element = null; }
a0 = '+=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_14_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[867]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[868]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[869]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[870]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[871]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[872]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[873]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[874]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[875]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[876]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[877]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[878]);
}

rightArg = parseop_Expression_level_91{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_14_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_14_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '-=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_15_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[879]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[880]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[881]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[882]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[883]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[884]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[885]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[886]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[887]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[888]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[889]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[890]);
}

rightArg = parseop_Expression_level_91{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_15_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_15_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_91 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
c0 = parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression{ element = c1; /* this is a subclass or primitive expression choice */ }
|c2 = parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression{ element = c2; /* this is a subclass or primitive expression choice */ }
|c3 = parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression{ element = c3; /* this is a subclass or primitive expression choice */ }
|c4 = parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression{ element = c4; /* this is a subclass or primitive expression choice */ }
|c5 = parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration{ element = c5; /* this is a subclass or primitive expression choice */ }
|c6 = parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression{ element = c6; /* this is a subclass or primitive expression choice */ }
;

parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element = null]
@init{
}
:
(
a0 = ILT
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createIntegerLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("ILT");
tokenResolver.setOptions(getOptions());
org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.Long resolved = (java.lang.Long) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_1_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[891]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[892]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[893]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[894]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[895]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[896]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[897]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[898]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[899]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[900]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[901]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[902]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[903]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[904]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[905]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[906]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[907]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[908]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[909]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[910]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[911]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[912]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[913]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[914]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[915]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[916]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[917]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[918]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[919]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[920]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[921]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[922]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[923]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[924]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element = null]
@init{
}
:
(
a0 = REAL_LITERAL
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createRealLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
tokenResolver.setOptions(getOptions());
org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.Double resolved = (java.lang.Double) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_2_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[925]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[926]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[927]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[928]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[929]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[930]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[931]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[932]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[933]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[934]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[935]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[936]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[937]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[938]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[939]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[940]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[941]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[942]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[943]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[944]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[945]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[946]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[947]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[948]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[949]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[950]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[951]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[952]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[953]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[954]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[955]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[956]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[957]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[958]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element = null]
@init{
}
:
(
(
a0 = 'true' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_3_0_0_0, true, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
// set value of boolean attribute
Object value = true;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
|a1 = 'false' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_3_0_0_0, false, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
// set value of boolean attribute
Object value = false;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
)
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[959]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[960]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[961]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[962]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[963]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[964]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[965]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[966]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[967]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[968]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[969]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[970]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[971]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[972]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[973]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[974]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[975]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[976]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[977]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[978]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[979]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[980]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[981]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[982]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[983]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[984]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[985]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[986]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[987]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[988]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[989]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[990]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[991]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[992]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element = null]
@init{
}
:
(
a0 = QUOTED_34_34
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createStringLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
tokenResolver.setOptions(getOptions());
org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.String resolved = (java.lang.String) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_4_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[993]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[994]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[995]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[996]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[997]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[998]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[999]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1000]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1001]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1002]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1003]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1004]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1005]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1006]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1007]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1008]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1009]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1010]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1011]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1012]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1013]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1014]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1015]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1016]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1017]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1018]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1019]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1020]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1021]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1022]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1023]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1024]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1025]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1026]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression returns [org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element = null]
@init{
}
:
a0 = '(' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_5_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1027]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1028]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1029]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1030]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1031]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1032]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1033]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1034]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1035]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1036]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1037]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1038]);
}

(
a1_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
Object value = a1_0;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_5_0_0_2, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1039]);
}

a2 = ')' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_5_0_0_4, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
}
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1040]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1041]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1042]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1043]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1044]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1045]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1046]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1047]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1048]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1049]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1050]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1051]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1052]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1053]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1054]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1055]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1056]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1057]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1058]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1059]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1060]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1061]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1062]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1063]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1064]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1065]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1066]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1067]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1068]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1069]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1070]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1071]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1072]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1073]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration returns [org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element = null]
@init{
}
:
a0 = 'var' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_29_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableDeclaration(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1074]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableDeclaration(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1075]);
}

(
a1_0 = parse_org_coolsoftware_coolcomponents_expressions_variables_Variable{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
Object value = a1_0;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_29_0_0_2, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1076]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1077]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1078]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1079]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1080]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1081]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1082]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1083]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1084]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1085]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1086]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1087]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1088]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1089]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1090]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1091]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1092]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1093]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1094]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1095]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1096]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1097]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1098]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1099]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1100]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1101]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1102]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1103]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1104]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1105]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1106]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1107]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1108]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1109]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression returns [org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element = null]
@init{
}
:
(
a0 = TEXT
{
if (terminateParsing) {
throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableCallExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
tokenResolver.setOptions(getOptions());
org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
String resolved = (String) resolvedObject;
org.coolsoftware.coolcomponents.expressions.variables.Variable proxy = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
collectHiddenTokens(element);
registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableCallExpressionReferredVariableReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), resolved, proxy);
if (proxy != null) {
Object value = proxy;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_31_0_0_0, proxy, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1110]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1111]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1112]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1113]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1114]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1115]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1116]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1117]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1118]);
addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1119]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1120]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1121]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1122]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1123]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1124]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1125]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1126]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1127]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1128]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1129]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1130]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1131]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1132]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1133]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1134]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1135]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1136]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1137]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1138]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1139]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1140]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1141]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1142]);
addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1143]);
}

;

parse_org_coolsoftware_ecl_Import returns [org.coolsoftware.ecl.Import element = null]
:
c0 = parse_org_coolsoftware_ecl_CcmImport{ element = c0; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_ecl_EclContract returns [org.coolsoftware.ecl.EclContract element = null]
:
c0 = parse_org_coolsoftware_ecl_SWComponentContract{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_ecl_HWComponentContract{ element = c1; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_ecl_SWContractClause returns [org.coolsoftware.ecl.SWContractClause element = null]
:
c0 = parse_org_coolsoftware_ecl_ProvisionClause{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_ecl_SWComponentRequirementClause{ element = c1; /* this is a subclass or primitive expression choice */ }
|c2 = parse_org_coolsoftware_ecl_HWComponentRequirementClause{ element = c2; /* this is a subclass or primitive expression choice */ }
|c3 = parse_org_coolsoftware_ecl_SelfRequirementClause{ element = c3; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_ecl_HWContractClause returns [org.coolsoftware.ecl.HWContractClause element = null]
:
c0 = parse_org_coolsoftware_ecl_ProvisionClause{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_ecl_HWComponentRequirementClause{ element = c1; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_coolcomponents_expressions_Expression returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
:
c = parseop_Expression_level_03{ element = c; /* this rule is an expression root */ }

;

ILT:
('-'?('0'..'9')+(('e'|'E')('-')?('0'..'9')+)?)
;
WHITESPACE:
((' ' | '\t' | '\f'))
{ _channel = 99; }
;
LINEBREAK:
(('\r\n' | '\r' | '\n'))
{ _channel = 99; }
;
QUOTED_91_93:
(('[')(~(']'))*(']'))
;
SL_COMMENT:
( '//'(~('\n'|'\r'|'\uffff'))* )
{ _channel = 99; }
;
ML_COMMENT:
( '/*'.*'*/')
{ _channel = 99; }
;
TYPE_LITERAL:
(('Boolean' | 'Integer' | 'Real' | 'String'))
;
REAL_LITERAL:
('-'?('0'..'9')+(('e'|'E')('-')?('0'..'9')+)?'.''-'?('0'..'9')+(('e'|'E')('-')?('0'..'9')+)?)
;
TEXT:
(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
;
QUOTED_34_34:
(('"')(~('"'))*('"'))
;

