/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.mopp;

public class EclTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public org.coolsoftware.ecl.resource.ecl.IEclTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("SL_COMMENT".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x00, 0x80, 0x00}, null, false, true, false, false);
		}
		if ("ML_COMMENT".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x00, 0x80, 0x00}, null, false, true, false, false);
		}
		if ("true".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("false".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("not".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("sin".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("cos".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("f".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("or".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("and".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("implies".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("var".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("QUOTED_34_34".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x2A, 0x00, 0xFF}, null, false, false, false, false);
		}
		if ("TASK_ITEM".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		if ("import".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("ccm".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("contract".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("implements".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("software".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("metaparameter:".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("hardware".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("mode".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("provides".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("min:".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("max:".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("requires".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("component".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("resource".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("energyRate: ".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("QUOTED_91_93".equals(tokenName)) {
			return new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(new int[] {0x2A, 0x00, 0xFF}, null, false, false, false, false);
		}
		return null;
	}
	
}
