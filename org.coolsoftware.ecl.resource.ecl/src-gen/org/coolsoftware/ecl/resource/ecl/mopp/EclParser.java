/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 3.4

	package org.coolsoftware.ecl.resource.ecl.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class EclParser extends EclANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ILT", "LINEBREAK", "ML_COMMENT", "QUOTED_34_34", "QUOTED_91_93", "REAL_LITERAL", "SL_COMMENT", "TEXT", "TYPE_LITERAL", "WHITESPACE", "'!'", "'!='", "'('", "')'", "')>'", "'*'", "'+'", "'++'", "'+='", "'-'", "'--'", "'-='", "'.'", "'/'", "':'", "';'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'^'", "'and'", "'ccm'", "'component'", "'contract'", "'cos'", "'energyRate: '", "'f'", "'false'", "'hardware'", "'implements'", "'implies'", "'import'", "'max:'", "'metaparameter:'", "'min:'", "'mode'", "'not'", "'or'", "'provides'", "'requires'", "'resource'", "'sin'", "'software'", "'true'", "'var'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int ILT=4;
    public static final int LINEBREAK=5;
    public static final int ML_COMMENT=6;
    public static final int QUOTED_34_34=7;
    public static final int QUOTED_91_93=8;
    public static final int REAL_LITERAL=9;
    public static final int SL_COMMENT=10;
    public static final int TEXT=11;
    public static final int TYPE_LITERAL=12;
    public static final int WHITESPACE=13;

    // delegates
    public EclANTLRParserBase[] getDelegates() {
        return new EclANTLRParserBase[] {};
    }

    // delegators


    public EclParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public EclParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(111 + 1);
         

    }

    public String[] getTokenNames() { return EclParser.tokenNames; }
    public String getGrammarFileName() { return "Ecl.g"; }


    	private org.coolsoftware.ecl.resource.ecl.IEclTokenResolverFactory tokenResolverFactory = new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
    			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new org.coolsoftware.ecl.resource.ecl.IEclProblem() {
    					public org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity getSeverity() {
    						return org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity.ERROR;
    					}
    					public org.coolsoftware.ecl.resource.ecl.EclEProblemType getType() {
    						return org.coolsoftware.ecl.resource.ecl.EclEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		org.coolsoftware.ecl.resource.ecl.IEclExpectedElement terminal = org.coolsoftware.ecl.resource.ecl.grammar.EclFollowSetProvider.TERMINALS[terminalID];
    		org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[] containmentFeatures = new org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = org.coolsoftware.ecl.resource.ecl.grammar.EclFollowSetProvider.LINKS[ids[i]];
    		}
    		org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace containmentTrace = new org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace(eClass, containmentFeatures);
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal expectedElement = new org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
    			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
    				org.coolsoftware.ecl.resource.ecl.IEclLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
    			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
    				org.coolsoftware.ecl.resource.ecl.IEclLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>() {
    			public boolean execute(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource) {
    				org.coolsoftware.ecl.resource.ecl.IEclLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public org.coolsoftware.ecl.resource.ecl.IEclTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new EclParser(new org.antlr.runtime3_4_0.CommonTokenStream(new EclLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new EclParser(new org.antlr.runtime3_4_0.CommonTokenStream(new EclLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new org.coolsoftware.ecl.resource.ecl.util.EclRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public EclParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((EclLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((EclLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == org.coolsoftware.ecl.EclFile.class) {
    				return parse_org_coolsoftware_ecl_EclFile();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.CcmImport.class) {
    				return parse_org_coolsoftware_ecl_CcmImport();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentContract.class) {
    				return parse_org_coolsoftware_ecl_SWComponentContract();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentContract.class) {
    				return parse_org_coolsoftware_ecl_HWComponentContract();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SWContractMode.class) {
    				return parse_org_coolsoftware_ecl_SWContractMode();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.HWContractMode.class) {
    				return parse_org_coolsoftware_ecl_HWContractMode();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.ProvisionClause.class) {
    				return parse_org_coolsoftware_ecl_ProvisionClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_SWComponentRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_HWComponentRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SelfRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_SelfRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.PropertyRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_PropertyRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.FormulaTemplate.class) {
    				return parse_org_coolsoftware_ecl_FormulaTemplate();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.Metaparameter.class) {
    				return parse_org_coolsoftware_ecl_Metaparameter();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.Block.class) {
    				return parse_org_coolsoftware_coolcomponents_expressions_Block();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.variables.Variable.class) {
    				return parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();
    			}
    		}
    		throw new org.coolsoftware.ecl.resource.ecl.mopp.EclUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(org.coolsoftware.ecl.resource.ecl.IEclOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public org.coolsoftware.ecl.resource.ecl.IEclParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource>>();
    		org.coolsoftware.ecl.resource.ecl.mopp.EclParseResult parseResult = new org.coolsoftware.ecl.resource.ecl.mopp.EclParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.ecl.resource.ecl.IEclTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		org.coolsoftware.ecl.resource.ecl.IEclParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (org.coolsoftware.ecl.resource.ecl.IEclCommand<org.coolsoftware.ecl.resource.ecl.IEclTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal>();
    		java.util.List<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 138;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<org.coolsoftware.ecl.resource.ecl.util.EclPair<org.coolsoftware.ecl.resource.ecl.IEclExpectedElement, org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (org.coolsoftware.ecl.resource.ecl.util.EclPair<org.coolsoftware.ecl.resource.ecl.IEclExpectedElement, org.coolsoftware.ecl.resource.ecl.mopp.EclContainedFeature[]> newFollowerPair : newFollowers) {
    							org.coolsoftware.ecl.resource.ecl.IEclExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace containmentTrace = new org.coolsoftware.ecl.resource.ecl.grammar.EclContainmentTrace(null, newFollowerPair.getRight());
    							org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal newFollowTerminal = new org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(org.coolsoftware.ecl.resource.ecl.mopp.EclExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Ecl.g:541:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_org_coolsoftware_ecl_EclFile ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        org.coolsoftware.ecl.EclFile c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Ecl.g:542:2: ( (c0= parse_org_coolsoftware_ecl_EclFile ) EOF )
            // Ecl.g:543:2: (c0= parse_org_coolsoftware_ecl_EclFile ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[0]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[2]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Ecl.g:550:2: (c0= parse_org_coolsoftware_ecl_EclFile )
            // Ecl.g:551:3: c0= parse_org_coolsoftware_ecl_EclFile
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_ecl_EclFile_in_start82);
            c0=parse_org_coolsoftware_ecl_EclFile();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_org_coolsoftware_ecl_EclFile"
    // Ecl.g:559:1: parse_org_coolsoftware_ecl_EclFile returns [org.coolsoftware.ecl.EclFile element = null] : ( (a0_0= parse_org_coolsoftware_ecl_Import ) )* ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )* ;
    public final org.coolsoftware.ecl.EclFile parse_org_coolsoftware_ecl_EclFile() throws RecognitionException {
        org.coolsoftware.ecl.EclFile element =  null;

        int parse_org_coolsoftware_ecl_EclFile_StartIndex = input.index();

        org.coolsoftware.ecl.Import a0_0 =null;

        org.coolsoftware.ecl.EclContract a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Ecl.g:562:2: ( ( (a0_0= parse_org_coolsoftware_ecl_Import ) )* ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )* )
            // Ecl.g:563:2: ( (a0_0= parse_org_coolsoftware_ecl_Import ) )* ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )*
            {
            // Ecl.g:563:2: ( (a0_0= parse_org_coolsoftware_ecl_Import ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==48) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Ecl.g:564:3: (a0_0= parse_org_coolsoftware_ecl_Import )
            	    {
            	    // Ecl.g:564:3: (a0_0= parse_org_coolsoftware_ecl_Import )
            	    // Ecl.g:565:4: a0_0= parse_org_coolsoftware_ecl_Import
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Import_in_parse_org_coolsoftware_ecl_EclFile124);
            	    a0_0=parse_org_coolsoftware_ecl_Import();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a0_0 != null) {
            	    					if (a0_0 != null) {
            	    						Object value = a0_0;
            	    						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__IMPORTS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_0_0_0_0, a0_0, true);
            	    					copyLocalizationInfos(a0_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[3]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[4]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[5]);
            	}

            // Ecl.g:593:2: ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==40) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Ecl.g:594:3: (a1_0= parse_org_coolsoftware_ecl_EclContract )
            	    {
            	    // Ecl.g:594:3: (a1_0= parse_org_coolsoftware_ecl_EclContract )
            	    // Ecl.g:595:4: a1_0= parse_org_coolsoftware_ecl_EclContract
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_EclContract_in_parse_org_coolsoftware_ecl_EclFile159);
            	    a1_0=parse_org_coolsoftware_ecl_EclContract();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a1_0 != null) {
            	    					if (a1_0 != null) {
            	    						Object value = a1_0;
            	    						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__CONTRACTS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_0_0_0_2, a1_0, true);
            	    					copyLocalizationInfos(a1_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[6]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[7]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_org_coolsoftware_ecl_EclFile_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_EclFile"



    // $ANTLR start "parse_org_coolsoftware_ecl_CcmImport"
    // Ecl.g:624:1: parse_org_coolsoftware_ecl_CcmImport returns [org.coolsoftware.ecl.CcmImport element = null] : a0= 'import' a1= 'ccm' (a2= QUOTED_91_93 ) ;
    public final org.coolsoftware.ecl.CcmImport parse_org_coolsoftware_ecl_CcmImport() throws RecognitionException {
        org.coolsoftware.ecl.CcmImport element =  null;

        int parse_org_coolsoftware_ecl_CcmImport_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Ecl.g:627:2: (a0= 'import' a1= 'ccm' (a2= QUOTED_91_93 ) )
            // Ecl.g:628:2: a0= 'import' a1= 'ccm' (a2= QUOTED_91_93 )
            {
            a0=(Token)match(input,48,FOLLOW_48_in_parse_org_coolsoftware_ecl_CcmImport200); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_1_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[8]);
            	}

            a1=(Token)match(input,38,FOLLOW_38_in_parse_org_coolsoftware_ecl_CcmImport214); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_1_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[9]);
            	}

            // Ecl.g:656:2: (a2= QUOTED_91_93 )
            // Ecl.g:657:3: a2= QUOTED_91_93
            {
            a2=(Token)match(input,QUOTED_91_93,FOLLOW_QUOTED_91_93_in_parse_org_coolsoftware_ecl_CcmImport232); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_91_93");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.StructuralModel proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createStructuralModel();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.CcmImport, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getCcmImportCcmStructuralModelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_1_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[10]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[11]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[12]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_org_coolsoftware_ecl_CcmImport_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_CcmImport"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWComponentContract"
    // Ecl.g:700:1: parse_org_coolsoftware_ecl_SWComponentContract returns [org.coolsoftware.ecl.SWComponentContract element = null] : a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'software' (a4= TEXT ) a5= '.' (a6= TEXT ) a7= '{' ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+ a11= '}' ;
    public final org.coolsoftware.ecl.SWComponentContract parse_org_coolsoftware_ecl_SWComponentContract() throws RecognitionException {
        org.coolsoftware.ecl.SWComponentContract element =  null;

        int parse_org_coolsoftware_ecl_SWComponentContract_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a11=null;
        org.coolsoftware.ecl.Metaparameter a9_0 =null;

        org.coolsoftware.ecl.SWContractMode a10_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Ecl.g:703:2: (a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'software' (a4= TEXT ) a5= '.' (a6= TEXT ) a7= '{' ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+ a11= '}' )
            // Ecl.g:704:2: a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'software' (a4= TEXT ) a5= '.' (a6= TEXT ) a7= '{' ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+ a11= '}'
            {
            a0=(Token)match(input,40,FOLLOW_40_in_parse_org_coolsoftware_ecl_SWComponentContract268); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[13]);
            	}

            // Ecl.g:718:2: (a1= TEXT )
            // Ecl.g:719:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract286); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[14]);
            	}

            a2=(Token)match(input,46,FOLLOW_46_in_parse_org_coolsoftware_ecl_SWComponentContract307); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[15]);
            	}

            a3=(Token)match(input,59,FOLLOW_59_in_parse_org_coolsoftware_ecl_SWComponentContract321); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[16]);
            	}

            // Ecl.g:782:2: (a4= TEXT )
            // Ecl.g:783:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract339); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_8, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[17]);
            	}

            a5=(Token)match(input,26,FOLLOW_26_in_parse_org_coolsoftware_ecl_SWComponentContract360); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[18]);
            	}

            // Ecl.g:836:2: (a6= TEXT )
            // Ecl.g:837:3: a6= TEXT
            {
            a6=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract378); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a6 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.PortType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createPortType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractPortReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_10, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[19]);
            	}

            a7=(Token)match(input,62,FOLLOW_62_in_parse_org_coolsoftware_ecl_SWComponentContract399); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_11, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[20]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[21]);
            	}

            // Ecl.g:891:2: ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==50) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // Ecl.g:892:3: (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    {
                    // Ecl.g:892:3: (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    // Ecl.g:893:4: a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    {
                    a8=(Token)match(input,50,FOLLOW_50_in_parse_org_coolsoftware_ecl_SWComponentContract422); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_13_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[22]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[23]);
                    			}

                    // Ecl.g:908:4: ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==TEXT) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // Ecl.g:909:5: ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    {
                    	    // Ecl.g:909:5: ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    // Ecl.g:910:6: (a9_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    {
                    	    // Ecl.g:910:6: (a9_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    // Ecl.g:911:7: a9_0= parse_org_coolsoftware_ecl_Metaparameter
                    	    {
                    	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_SWComponentContract463);
                    	    a9_0=parse_org_coolsoftware_ecl_Metaparameter();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a9_0 != null) {
                    	    								if (a9_0 != null) {
                    	    									Object value = a9_0;
                    	    									addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__METAPARAMS, value);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_13_0_0_1_0_0_0, a9_0, true);
                    	    								copyLocalizationInfos(a9_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[24]);
                    	    						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[25]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[26]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[27]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[28]);
            	}

            // Ecl.g:952:2: ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==52) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // Ecl.g:953:3: ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) )
            	    {
            	    // Ecl.g:953:3: ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) )
            	    // Ecl.g:954:4: (a10_0= parse_org_coolsoftware_ecl_SWContractMode )
            	    {
            	    // Ecl.g:954:4: (a10_0= parse_org_coolsoftware_ecl_SWContractMode )
            	    // Ecl.g:955:5: a10_0= parse_org_coolsoftware_ecl_SWContractMode
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWContractMode_in_parse_org_coolsoftware_ecl_SWComponentContract552);
            	    a10_0=parse_org_coolsoftware_ecl_SWContractMode();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a10_0 != null) {
            	    						if (a10_0 != null) {
            	    							Object value = a10_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__MODES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_15_0_0_0, a10_0, true);
            	    						copyLocalizationInfos(a10_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[29]);
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[30]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[31]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[32]);
            	}

            a11=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_ecl_SWComponentContract593); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_2_0_0_16, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[33]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[34]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_org_coolsoftware_ecl_SWComponentContract_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWComponentContract"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWComponentContract"
    // Ecl.g:1006:1: parse_org_coolsoftware_ecl_HWComponentContract returns [org.coolsoftware.ecl.HWComponentContract element = null] : a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'hardware' (a4= TEXT ) a5= '{' ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+ a9= '}' ;
    public final org.coolsoftware.ecl.HWComponentContract parse_org_coolsoftware_ecl_HWComponentContract() throws RecognitionException {
        org.coolsoftware.ecl.HWComponentContract element =  null;

        int parse_org_coolsoftware_ecl_HWComponentContract_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a9=null;
        org.coolsoftware.ecl.Metaparameter a7_0 =null;

        org.coolsoftware.ecl.HWContractMode a8_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Ecl.g:1009:2: (a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'hardware' (a4= TEXT ) a5= '{' ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+ a9= '}' )
            // Ecl.g:1010:2: a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'hardware' (a4= TEXT ) a5= '{' ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+ a9= '}'
            {
            a0=(Token)match(input,40,FOLLOW_40_in_parse_org_coolsoftware_ecl_HWComponentContract622); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[35]);
            	}

            // Ecl.g:1024:2: (a1= TEXT )
            // Ecl.g:1025:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract640); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[36]);
            	}

            a2=(Token)match(input,46,FOLLOW_46_in_parse_org_coolsoftware_ecl_HWComponentContract661); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[37]);
            	}

            a3=(Token)match(input,45,FOLLOW_45_in_parse_org_coolsoftware_ecl_HWComponentContract675); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[38]);
            	}

            // Ecl.g:1088:2: (a4= TEXT )
            // Ecl.g:1089:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract693); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_8, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[39]);
            	}

            a5=(Token)match(input,62,FOLLOW_62_in_parse_org_coolsoftware_ecl_HWComponentContract714); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[40]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[41]);
            	}

            // Ecl.g:1143:2: ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==50) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // Ecl.g:1144:3: (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    {
                    // Ecl.g:1144:3: (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    // Ecl.g:1145:4: a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    {
                    a6=(Token)match(input,50,FOLLOW_50_in_parse_org_coolsoftware_ecl_HWComponentContract737); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_12_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[42]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[43]);
                    			}

                    // Ecl.g:1160:4: ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==TEXT) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // Ecl.g:1161:5: ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    {
                    	    // Ecl.g:1161:5: ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    // Ecl.g:1162:6: (a7_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    {
                    	    // Ecl.g:1162:6: (a7_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    // Ecl.g:1163:7: a7_0= parse_org_coolsoftware_ecl_Metaparameter
                    	    {
                    	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_HWComponentContract778);
                    	    a7_0=parse_org_coolsoftware_ecl_Metaparameter();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a7_0 != null) {
                    	    								if (a7_0 != null) {
                    	    									Object value = a7_0;
                    	    									addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__METAPARAMS, value);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_12_0_0_1_0_0_0, a7_0, true);
                    	    								copyLocalizationInfos(a7_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[44]);
                    	    						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[45]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[46]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[47]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[48]);
            	}

            // Ecl.g:1204:2: ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==52) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // Ecl.g:1205:3: ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) )
            	    {
            	    // Ecl.g:1205:3: ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) )
            	    // Ecl.g:1206:4: (a8_0= parse_org_coolsoftware_ecl_HWContractMode )
            	    {
            	    // Ecl.g:1206:4: (a8_0= parse_org_coolsoftware_ecl_HWContractMode )
            	    // Ecl.g:1207:5: a8_0= parse_org_coolsoftware_ecl_HWContractMode
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWContractMode_in_parse_org_coolsoftware_ecl_HWComponentContract867);
            	    a8_0=parse_org_coolsoftware_ecl_HWContractMode();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a8_0 != null) {
            	    						if (a8_0 != null) {
            	    							Object value = a8_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__MODES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_14_0_0_0, a8_0, true);
            	    						copyLocalizationInfos(a8_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[49]);
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[50]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[51]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[52]);
            	}

            a9=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_ecl_HWComponentContract908); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_3_0_0_15, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[53]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getEclFile(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[54]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_org_coolsoftware_ecl_HWComponentContract_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWComponentContract"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWContractMode"
    // Ecl.g:1258:1: parse_org_coolsoftware_ecl_SWContractMode returns [org.coolsoftware.ecl.SWContractMode element = null] : a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+ a4= '}' ;
    public final org.coolsoftware.ecl.SWContractMode parse_org_coolsoftware_ecl_SWContractMode() throws RecognitionException {
        org.coolsoftware.ecl.SWContractMode element =  null;

        int parse_org_coolsoftware_ecl_SWContractMode_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.ecl.SWContractClause a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Ecl.g:1261:2: (a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+ a4= '}' )
            // Ecl.g:1262:2: a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+ a4= '}'
            {
            a0=(Token)match(input,52,FOLLOW_52_in_parse_org_coolsoftware_ecl_SWContractMode937); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[55]);
            	}

            // Ecl.g:1276:2: (a1= TEXT )
            // Ecl.g:1277:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWContractMode955); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[56]);
            	}

            a2=(Token)match(input,62,FOLLOW_62_in_parse_org_coolsoftware_ecl_SWContractMode976); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[57]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[58]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[59]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[60]);
            	}

            // Ecl.g:1329:2: ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0 >= 55 && LA9_0 <= 56)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // Ecl.g:1330:3: ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) )
            	    {
            	    // Ecl.g:1330:3: ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) )
            	    // Ecl.g:1331:4: (a3_0= parse_org_coolsoftware_ecl_SWContractClause )
            	    {
            	    // Ecl.g:1331:4: (a3_0= parse_org_coolsoftware_ecl_SWContractClause )
            	    // Ecl.g:1332:5: a3_0= parse_org_coolsoftware_ecl_SWContractClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWContractClause_in_parse_org_coolsoftware_ecl_SWContractMode1005);
            	    a3_0=parse_org_coolsoftware_ecl_SWContractClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__CLAUSES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_6_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[61]);
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[62]);
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[63]);
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[64]);
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[65]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[66]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[67]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[68]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[69]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[70]);
            	}

            a4=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_ecl_SWContractMode1046); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_4_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[71]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[72]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_org_coolsoftware_ecl_SWContractMode_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWContractMode"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWContractMode"
    // Ecl.g:1389:1: parse_org_coolsoftware_ecl_HWContractMode returns [org.coolsoftware.ecl.HWContractMode element = null] : a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+ a4= '}' ;
    public final org.coolsoftware.ecl.HWContractMode parse_org_coolsoftware_ecl_HWContractMode() throws RecognitionException {
        org.coolsoftware.ecl.HWContractMode element =  null;

        int parse_org_coolsoftware_ecl_HWContractMode_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.ecl.HWContractClause a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Ecl.g:1392:2: (a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+ a4= '}' )
            // Ecl.g:1393:2: a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+ a4= '}'
            {
            a0=(Token)match(input,52,FOLLOW_52_in_parse_org_coolsoftware_ecl_HWContractMode1075); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[73]);
            	}

            // Ecl.g:1407:2: (a1= TEXT )
            // Ecl.g:1408:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWContractMode1093); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[74]);
            	}

            a2=(Token)match(input,62,FOLLOW_62_in_parse_org_coolsoftware_ecl_HWContractMode1114); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[75]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[76]);
            	}

            // Ecl.g:1458:2: ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0 >= 55 && LA10_0 <= 56)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // Ecl.g:1459:3: ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) )
            	    {
            	    // Ecl.g:1459:3: ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) )
            	    // Ecl.g:1460:4: (a3_0= parse_org_coolsoftware_ecl_HWContractClause )
            	    {
            	    // Ecl.g:1460:4: (a3_0= parse_org_coolsoftware_ecl_HWContractClause )
            	    // Ecl.g:1461:5: a3_0= parse_org_coolsoftware_ecl_HWContractClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWContractClause_in_parse_org_coolsoftware_ecl_HWContractMode1143);
            	    a3_0=parse_org_coolsoftware_ecl_HWContractClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__CLAUSES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_6_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[77]);
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[78]);
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[79]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[80]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[81]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[82]);
            	}

            a4=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_ecl_HWContractMode1184); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_5_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[83]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[84]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_org_coolsoftware_ecl_HWContractMode_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWContractMode"



    // $ANTLR start "parse_org_coolsoftware_ecl_ProvisionClause"
    // Ecl.g:1514:1: parse_org_coolsoftware_ecl_ProvisionClause returns [org.coolsoftware.ecl.ProvisionClause element = null] : a0= 'provides' (a1= TEXT ) ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? ;
    public final org.coolsoftware.ecl.ProvisionClause parse_org_coolsoftware_ecl_ProvisionClause() throws RecognitionException {
        org.coolsoftware.ecl.ProvisionClause element =  null;

        int parse_org_coolsoftware_ecl_ProvisionClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.coolcomponents.expressions.Expression a3_0 =null;

        org.coolsoftware.coolcomponents.expressions.Expression a5_0 =null;

        org.coolsoftware.ecl.FormulaTemplate a6_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Ecl.g:1517:2: (a0= 'provides' (a1= TEXT ) ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? )
            // Ecl.g:1518:2: a0= 'provides' (a1= TEXT ) ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            {
            a0=(Token)match(input,55,FOLLOW_55_in_parse_org_coolsoftware_ecl_ProvisionClause1213); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[85]);
            	}

            // Ecl.g:1532:2: (a1= TEXT )
            // Ecl.g:1533:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_ProvisionClause1231); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getProvisionClauseProvidedPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[86]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[87]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[88]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[89]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[90]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[91]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[92]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[93]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[94]);
            	}

            // Ecl.g:1580:2: ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==51) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // Ecl.g:1581:3: (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Ecl.g:1581:3: (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Ecl.g:1582:4: a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a2=(Token)match(input,51,FOLLOW_51_in_parse_org_coolsoftware_ecl_ProvisionClause1261); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_4_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[95]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[96]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[97]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[98]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[99]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[100]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[101]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[102]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[103]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[104]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[105]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[106]);
                    			}

                    // Ecl.g:1607:4: (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Ecl.g:1608:5: a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1287);
                    a3_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a3_0 != null) {
                    						if (a3_0 != null) {
                    							Object value = a3_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MIN_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_4_0_0_2, a3_0, true);
                    						copyLocalizationInfos(a3_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[107]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[108]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[109]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[110]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[111]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[112]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[113]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[114]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[115]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[116]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[117]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[118]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[119]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[120]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[121]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[122]);
            	}

            // Ecl.g:1654:2: ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==49) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // Ecl.g:1655:3: (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Ecl.g:1655:3: (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Ecl.g:1656:4: a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a4=(Token)match(input,49,FOLLOW_49_in_parse_org_coolsoftware_ecl_ProvisionClause1337); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_5_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[123]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[124]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[125]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[126]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[127]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[128]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[129]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[130]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[131]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[132]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[133]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[134]);
                    			}

                    // Ecl.g:1681:4: (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Ecl.g:1682:5: a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1363);
                    a5_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a5_0 != null) {
                    						if (a5_0 != null) {
                    							Object value = a5_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MAX_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_5_0_0_2, a5_0, true);
                    						copyLocalizationInfos(a5_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[135]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[136]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[137]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[138]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[139]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[140]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[141]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[142]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[143]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[144]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[145]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[146]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[147]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[148]);
            	}

            // Ecl.g:1726:2: ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==30) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // Ecl.g:1727:3: (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    {
                    // Ecl.g:1727:3: (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    // Ecl.g:1728:4: a6_0= parse_org_coolsoftware_ecl_FormulaTemplate
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_ProvisionClause1413);
                    a6_0=parse_org_coolsoftware_ecl_FormulaTemplate();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    					startIncompleteElement(element);
                    				}
                    				if (a6_0 != null) {
                    					if (a6_0 != null) {
                    						Object value = a6_0;
                    						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__FORMULA), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_6_0_0_6, a6_0, true);
                    					copyLocalizationInfos(a6_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[149]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[150]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[151]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[152]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[153]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[154]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_org_coolsoftware_ecl_ProvisionClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_ProvisionClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWComponentRequirementClause"
    // Ecl.g:1761:1: parse_org_coolsoftware_ecl_SWComponentRequirementClause returns [org.coolsoftware.ecl.SWComponentRequirementClause element = null] : a0= 'requires' a1= 'component' (a2= TEXT ) ( (a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )* a5= '}' ) |a6= ';' ) ;
    public final org.coolsoftware.ecl.SWComponentRequirementClause parse_org_coolsoftware_ecl_SWComponentRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.SWComponentRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_SWComponentRequirementClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a5=null;
        Token a6=null;
        org.coolsoftware.ecl.PropertyRequirementClause a4_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Ecl.g:1764:2: (a0= 'requires' a1= 'component' (a2= TEXT ) ( (a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )* a5= '}' ) |a6= ';' ) )
            // Ecl.g:1765:2: a0= 'requires' a1= 'component' (a2= TEXT ) ( (a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )* a5= '}' ) |a6= ';' )
            {
            a0=(Token)match(input,56,FOLLOW_56_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1454); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[155]);
            	}

            a1=(Token)match(input,39,FOLLOW_39_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1468); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[156]);
            	}

            // Ecl.g:1793:2: (a2= TEXT )
            // Ecl.g:1794:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1486); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentRequirementClauseRequiredComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[157]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[158]);
            	}

            // Ecl.g:1834:2: ( (a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )* a5= '}' ) |a6= ';' )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==62) ) {
                alt15=1;
            }
            else if ( (LA15_0==29) ) {
                alt15=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;

            }
            switch (alt15) {
                case 1 :
                    // Ecl.g:1835:3: (a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )* a5= '}' )
                    {
                    // Ecl.g:1835:3: (a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )* a5= '}' )
                    // Ecl.g:1836:4: a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )* a5= '}'
                    {
                    a3=(Token)match(input,62,FOLLOW_62_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1516); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_0_0_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[159]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[160]);
                    			}

                    // Ecl.g:1851:4: ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==TEXT) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // Ecl.g:1852:5: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
                    	    {
                    	    // Ecl.g:1852:5: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
                    	    // Ecl.g:1853:6: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
                    	    {
                    	    // Ecl.g:1853:6: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
                    	    // Ecl.g:1854:7: a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause
                    	    {
                    	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1557);
                    	    a4_0=parse_org_coolsoftware_ecl_PropertyRequirementClause();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a4_0 != null) {
                    	    								if (a4_0 != null) {
                    	    									Object value = a4_0;
                    	    									addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_0_0_0_0_2_0_0_0, a4_0, true);
                    	    								copyLocalizationInfos(a4_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[161]);
                    	    						addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[162]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[163]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[164]);
                    			}

                    a5=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1618); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_0_0_0_0_3, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[165]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[166]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[167]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[168]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[169]);
                    			}

                    }


                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[170]);
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[171]);
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[172]);
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[173]);
                    			addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[174]);
                    		}

                    }
                    break;
                case 2 :
                    // Ecl.g:1917:6: a6= ';'
                    {
                    a6=(Token)match(input,29,FOLLOW_29_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1654); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (element == null) {
                    				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
                    				startIncompleteElement(element);
                    			}
                    			collectHiddenTokens(element);
                    			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_7_0_0_6_0_1_0, null, true);
                    			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    		}

                    if ( state.backtracking==0 ) {
                    			// expected elements (follow set)
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[175]);
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[176]);
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[177]);
                    			addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[178]);
                    			addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[179]);
                    		}

                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[180]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[181]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[182]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[183]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[184]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_org_coolsoftware_ecl_SWComponentRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWComponentRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWComponentRequirementClause"
    // Ecl.g:1947:1: parse_org_coolsoftware_ecl_HWComponentRequirementClause returns [org.coolsoftware.ecl.HWComponentRequirementClause element = null] : a0= 'requires' a1= 'resource' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= 'energyRate: ' (a6= REAL_LITERAL ) a7= '}' ;
    public final org.coolsoftware.ecl.HWComponentRequirementClause parse_org_coolsoftware_ecl_HWComponentRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.HWComponentRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_HWComponentRequirementClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        org.coolsoftware.ecl.PropertyRequirementClause a4_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Ecl.g:1950:2: (a0= 'requires' a1= 'resource' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= 'energyRate: ' (a6= REAL_LITERAL ) a7= '}' )
            // Ecl.g:1951:2: a0= 'requires' a1= 'resource' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= 'energyRate: ' (a6= REAL_LITERAL ) a7= '}'
            {
            a0=(Token)match(input,56,FOLLOW_56_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1693); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[185]);
            	}

            a1=(Token)match(input,57,FOLLOW_57_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1707); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[186]);
            	}

            // Ecl.g:1979:2: (a2= TEXT )
            // Ecl.g:1980:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1725); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentRequirementClauseRequiredResourceTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[187]);
            	}

            a3=(Token)match(input,62,FOLLOW_62_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1746); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[188]);
            	}

            // Ecl.g:2033:2: ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==TEXT) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // Ecl.g:2034:3: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            	    {
            	    // Ecl.g:2034:3: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            	    // Ecl.g:2035:4: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    {
            	    // Ecl.g:2035:4: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    // Ecl.g:2036:5: a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1775);
            	    a4_0=parse_org_coolsoftware_ecl_PropertyRequirementClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a4_0 != null) {
            	    						if (a4_0 != null) {
            	    							Object value = a4_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_8_0_0_0, a4_0, true);
            	    						copyLocalizationInfos(a4_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[189]);
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[190]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[191]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[192]);
            	}

            a5=(Token)match(input,42,FOLLOW_42_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1816); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[193]);
            	}

            // Ecl.g:2084:2: (a6= REAL_LITERAL )
            // Ecl.g:2085:3: a6= REAL_LITERAL
            {
            a6=(Token)match(input,REAL_LITERAL,FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1834); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a6 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
            				}
            				java.lang.Double resolved = (java.lang.Double) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_11, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[194]);
            	}

            a7=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1855); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_8_0_0_13, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[195]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[196]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[197]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[198]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[199]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[200]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_org_coolsoftware_ecl_HWComponentRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWComponentRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_SelfRequirementClause"
    // Ecl.g:2141:1: parse_org_coolsoftware_ecl_SelfRequirementClause returns [org.coolsoftware.ecl.SelfRequirementClause element = null] : a0= 'requires' (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ;
    public final org.coolsoftware.ecl.SelfRequirementClause parse_org_coolsoftware_ecl_SelfRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.SelfRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_SelfRequirementClause_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.ecl.PropertyRequirementClause a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Ecl.g:2144:2: (a0= 'requires' (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            // Ecl.g:2145:2: a0= 'requires' (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            {
            a0=(Token)match(input,56,FOLLOW_56_in_parse_org_coolsoftware_ecl_SelfRequirementClause1884); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_9_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSelfRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[201]);
            	}

            // Ecl.g:2159:2: (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            // Ecl.g:2160:3: a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SelfRequirementClause1902);
            a1_0=parse_org_coolsoftware_ecl_PropertyRequirementClause();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_9_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[202]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[203]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[204]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[205]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[206]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_org_coolsoftware_ecl_SelfRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SelfRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_PropertyRequirementClause"
    // Ecl.g:2191:1: parse_org_coolsoftware_ecl_PropertyRequirementClause returns [org.coolsoftware.ecl.PropertyRequirementClause element = null] : (a0= TEXT ) ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? ;
    public final org.coolsoftware.ecl.PropertyRequirementClause parse_org_coolsoftware_ecl_PropertyRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.PropertyRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_PropertyRequirementClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a2_0 =null;

        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;

        org.coolsoftware.ecl.FormulaTemplate a5_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Ecl.g:2194:2: ( (a0= TEXT ) ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? )
            // Ecl.g:2195:2: (a0= TEXT ) ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            {
            // Ecl.g:2195:2: (a0= TEXT )
            // Ecl.g:2196:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_PropertyRequirementClause1939); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.PropertyRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPropertyRequirementClauseRequiredPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_0, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[207]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[208]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[209]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[210]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[211]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[212]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[213]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[214]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[215]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[216]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[217]);
            	}

            // Ecl.g:2245:2: ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==51) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // Ecl.g:2246:3: (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Ecl.g:2246:3: (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Ecl.g:2247:4: a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a1=(Token)match(input,51,FOLLOW_51_in_parse_org_coolsoftware_ecl_PropertyRequirementClause1969); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_2_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[218]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[219]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[220]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[221]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[222]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[223]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[224]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[225]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[226]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[227]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[228]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[229]);
                    			}

                    // Ecl.g:2272:4: (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Ecl.g:2273:5: a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause1995);
                    a2_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a2_0 != null) {
                    						if (a2_0 != null) {
                    							Object value = a2_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_2_0_0_2, a2_0, true);
                    						copyLocalizationInfos(a2_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[230]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[231]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[232]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[233]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[234]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[235]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[236]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[237]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[238]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[239]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[240]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[241]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[242]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[243]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[244]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[245]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[246]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[247]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[248]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[249]);
            	}

            // Ecl.g:2323:2: ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==49) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // Ecl.g:2324:3: (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Ecl.g:2324:3: (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Ecl.g:2325:4: a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a3=(Token)match(input,49,FOLLOW_49_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2045); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[250]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[251]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[252]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[253]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[254]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[255]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[256]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[257]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[258]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[259]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[260]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[261]);
                    			}

                    // Ecl.g:2350:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Ecl.g:2351:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2071);
                    a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a4_0 != null) {
                    						if (a4_0 != null) {
                    							Object value = a4_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_3_0_0_2, a4_0, true);
                    						copyLocalizationInfos(a4_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[262]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[263]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[264]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[265]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[266]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[267]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[268]);
                    				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[269]);
                    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[270]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[271]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[272]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[273]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[274]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[275]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[276]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[277]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[278]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[279]);
            	}

            // Ecl.g:2399:2: ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==30) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // Ecl.g:2400:3: (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    {
                    // Ecl.g:2400:3: (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    // Ecl.g:2401:4: a5_0= parse_org_coolsoftware_ecl_FormulaTemplate
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2121);
                    a5_0=parse_org_coolsoftware_ecl_FormulaTemplate();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				if (a5_0 != null) {
                    					if (a5_0 != null) {
                    						Object value = a5_0;
                    						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_10_0_0_4, a5_0, true);
                    					copyLocalizationInfos(a5_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[280]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[281]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[282]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[283]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[284]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[285]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[286]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[287]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_org_coolsoftware_ecl_PropertyRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_PropertyRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_FormulaTemplate"
    // Ecl.g:2436:1: parse_org_coolsoftware_ecl_FormulaTemplate returns [org.coolsoftware.ecl.FormulaTemplate element = null] : a0= '<' (a1= TEXT ) a2= '(' ( ( (a3= TEXT ) ) )* a4= ')>' ;
    public final org.coolsoftware.ecl.FormulaTemplate parse_org_coolsoftware_ecl_FormulaTemplate() throws RecognitionException {
        org.coolsoftware.ecl.FormulaTemplate element =  null;

        int parse_org_coolsoftware_ecl_FormulaTemplate_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Ecl.g:2439:2: (a0= '<' (a1= TEXT ) a2= '(' ( ( (a3= TEXT ) ) )* a4= ')>' )
            // Ecl.g:2440:2: a0= '<' (a1= TEXT ) a2= '(' ( ( (a3= TEXT ) ) )* a4= ')>'
            {
            a0=(Token)match(input,30,FOLLOW_30_in_parse_org_coolsoftware_ecl_FormulaTemplate2162); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[288]);
            	}

            // Ecl.g:2454:2: (a1= TEXT )
            // Ecl.g:2455:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2180); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[289]);
            	}

            a2=(Token)match(input,16,FOLLOW_16_in_parse_org_coolsoftware_ecl_FormulaTemplate2201); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[290]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[291]);
            	}

            // Ecl.g:2505:2: ( ( (a3= TEXT ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==TEXT) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // Ecl.g:2506:3: ( (a3= TEXT ) )
            	    {
            	    // Ecl.g:2506:3: ( (a3= TEXT ) )
            	    // Ecl.g:2507:4: (a3= TEXT )
            	    {
            	    // Ecl.g:2507:4: (a3= TEXT )
            	    // Ecl.g:2508:5: a3= TEXT
            	    {
            	    a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2230); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3 != null) {
            	    						org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	    						tokenResolver.setOptions(getOptions());
            	    						org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            	    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), result);
            	    						Object resolvedObject = result.getResolvedToken();
            	    						if (resolvedObject == null) {
            	    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            	    						}
            	    						String resolved = (String) resolvedObject;
            	    						org.coolsoftware.coolcomponents.ccm.structure.Parameter proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createParameter();
            	    						collectHiddenTokens(element);
            	    						registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFormulaTemplateMetaparameterReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), resolved, proxy);
            	    						if (proxy != null) {
            	    							Object value = proxy;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER, value);
            	    							completedElement(value, false);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_3_0_0_0, proxy, true);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[292]);
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[293]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[294]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[295]);
            	}

            a4=(Token)match(input,18,FOLLOW_18_in_parse_org_coolsoftware_ecl_FormulaTemplate2276); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_11_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[296]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[297]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[298]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[299]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[300]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[301]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[302]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[303]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[304]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_org_coolsoftware_ecl_FormulaTemplate_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_FormulaTemplate"



    // $ANTLR start "parse_org_coolsoftware_ecl_Metaparameter"
    // Ecl.g:2580:1: parse_org_coolsoftware_ecl_Metaparameter returns [org.coolsoftware.ecl.Metaparameter element = null] : (a0= TEXT ) ;
    public final org.coolsoftware.ecl.Metaparameter parse_org_coolsoftware_ecl_Metaparameter() throws RecognitionException {
        org.coolsoftware.ecl.Metaparameter element =  null;

        int parse_org_coolsoftware_ecl_Metaparameter_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Ecl.g:2583:2: ( (a0= TEXT ) )
            // Ecl.g:2584:2: (a0= TEXT )
            {
            // Ecl.g:2584:2: (a0= TEXT )
            // Ecl.g:2585:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_Metaparameter2309); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createMetaparameter();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.ECL_12_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[305]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[306]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[307]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[308]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[309]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[310]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[311]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[312]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[313]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[314]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[315]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[316]);
            		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[317]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[318]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[319]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[320]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[321]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[322]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[323]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[324]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[325]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[326]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[327]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[328]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[329]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[330]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[331]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[332]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[333]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[334]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[335]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[336]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[337]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[338]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[339]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[340]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[341]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_org_coolsoftware_ecl_Metaparameter_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_Metaparameter"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_Block"
    // Ecl.g:2658:1: parse_org_coolsoftware_coolcomponents_expressions_Block returns [org.coolsoftware.coolcomponents.expressions.Block element = null] : a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}' ;
    public final org.coolsoftware.coolcomponents.expressions.Block parse_org_coolsoftware_coolcomponents_expressions_Block() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Block element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_Block_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;

        org.coolsoftware.coolcomponents.expressions.Expression a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return element; }

            // Ecl.g:2661:2: (a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}' )
            // Ecl.g:2662:2: a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}'
            {
            a0=(Token)match(input,62,FOLLOW_62_in_parse_org_coolsoftware_coolcomponents_expressions_Block2345); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[342]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[343]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[344]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[345]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[346]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[347]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[348]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[349]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[350]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[351]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[352]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[353]);
            	}

            // Ecl.g:2687:2: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            // Ecl.g:2688:3: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2363);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[354]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[355]);
            	}

            // Ecl.g:2714:2: ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==29) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // Ecl.g:2715:3: (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
            	    {
            	    // Ecl.g:2715:3: (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
            	    // Ecl.g:2716:4: a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            	    {
            	    a2=(Token)match(input,29,FOLLOW_29_in_parse_org_coolsoftware_coolcomponents_expressions_Block2390); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_4_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[356]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[357]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[358]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[359]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[360]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[361]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[362]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[363]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[364]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[365]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[366]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[367]);
            	    			}

            	    // Ecl.g:2741:4: (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            	    // Ecl.g:2742:5: a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2416);
            	    a3_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_4_0_0_2, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[368]);
            	    				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[369]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[370]);
            		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[371]);
            	}

            a4=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_coolcomponents_expressions_Block2457); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_0_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 15, parse_org_coolsoftware_coolcomponents_expressions_Block_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_Block"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_Variable"
    // Ecl.g:2791:1: parse_org_coolsoftware_coolcomponents_expressions_variables_Variable returns [org.coolsoftware.coolcomponents.expressions.variables.Variable element = null] : ( (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? |c0= parse_org_coolsoftware_ecl_Metaparameter );
    public final org.coolsoftware.coolcomponents.expressions.variables.Variable parse_org_coolsoftware_coolcomponents_expressions_variables_Variable() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.Variable element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;

        org.coolsoftware.ecl.Metaparameter c0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return element; }

            // Ecl.g:2794:2: ( (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? |c0= parse_org_coolsoftware_ecl_Metaparameter )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==TEXT) ) {
                int LA24_1 = input.LA(2);

                if ( (synpred24_Ecl()) ) {
                    alt24=1;
                }
                else if ( (true) ) {
                    alt24=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 24, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;

            }
            switch (alt24) {
                case 1 :
                    // Ecl.g:2795:2: (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
                    {
                    // Ecl.g:2795:2: (a0= TEXT )
                    // Ecl.g:2796:3: a0= TEXT
                    {
                    a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2490); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (terminateParsing) {
                    				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    			}
                    			if (element == null) {
                    				element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                    				startIncompleteElement(element);
                    			}
                    			if (a0 != null) {
                    				org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    				tokenResolver.setOptions(getOptions());
                    				org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
                    				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), result);
                    				Object resolvedObject = result.getResolvedToken();
                    				if (resolvedObject == null) {
                    					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
                    				}
                    				java.lang.String resolved = (java.lang.String) resolvedObject;
                    				if (resolved != null) {
                    					Object value = resolved;
                    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), value);
                    					completedElement(value, false);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_0, resolved, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
                    			}
                    		}

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[372]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[373]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[374]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[375]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[376]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[377]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[378]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[379]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[380]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[381]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[382]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[383]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[384]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[385]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[386]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[387]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[388]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[389]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[390]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[391]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[392]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[393]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[394]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[395]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[396]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[397]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[398]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[399]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[400]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[401]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[402]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[403]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[404]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[405]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[406]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[407]);
                    	}

                    // Ecl.g:2866:2: ( (a1= ':' (a2= TYPE_LITERAL ) ) )?
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==28) ) {
                        alt22=1;
                    }
                    switch (alt22) {
                        case 1 :
                            // Ecl.g:2867:3: (a1= ':' (a2= TYPE_LITERAL ) )
                            {
                            // Ecl.g:2867:3: (a1= ':' (a2= TYPE_LITERAL ) )
                            // Ecl.g:2868:4: a1= ':' (a2= TYPE_LITERAL )
                            {
                            a1=(Token)match(input,28,FOLLOW_28_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2520); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            				if (element == null) {
                            					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            					startIncompleteElement(element);
                            				}
                            				collectHiddenTokens(element);
                            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_1_0_0_1, null, true);
                            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                            			}

                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[408]);
                            			}

                            // Ecl.g:2882:4: (a2= TYPE_LITERAL )
                            // Ecl.g:2883:5: a2= TYPE_LITERAL
                            {
                            a2=(Token)match(input,TYPE_LITERAL,FOLLOW_TYPE_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2546); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            					if (terminateParsing) {
                            						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                            					}
                            					if (element == null) {
                            						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            						startIncompleteElement(element);
                            					}
                            					if (a2 != null) {
                            						org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TYPE_LITERAL");
                            						tokenResolver.setOptions(getOptions());
                            						org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
                            						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), result);
                            						Object resolvedObject = result.getResolvedToken();
                            						if (resolvedObject == null) {
                            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
                            						}
                            						org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral resolved = (org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral) resolvedObject;
                            						if (resolved != null) {
                            							Object value = resolved;
                            							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), value);
                            							completedElement(value, false);
                            						}
                            						collectHiddenTokens(element);
                            						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_1_0_0_3, resolved, true);
                            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
                            					}
                            				}

                            }


                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[409]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[410]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[411]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[412]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[413]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[414]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[415]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[416]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[417]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[418]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[419]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[420]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[421]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[422]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[423]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[424]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[425]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[426]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[427]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[428]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[429]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[430]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[431]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[432]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[433]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[434]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[435]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[436]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[437]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[438]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[439]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[440]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[441]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[442]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[443]);
                            			}

                            }


                            }
                            break;

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[444]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[445]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[446]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[447]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[448]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[449]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[450]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[451]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[452]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[453]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[454]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[455]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[456]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[457]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[458]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[459]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[460]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[461]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[462]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[463]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[464]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[465]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[466]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[467]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[468]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[469]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[470]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[471]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[472]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[473]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[474]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[475]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[476]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[477]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[478]);
                    	}

                    // Ecl.g:2993:2: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==32) ) {
                        int LA23_1 = input.LA(2);

                        if ( (synpred23_Ecl()) ) {
                            alt23=1;
                        }
                    }
                    switch (alt23) {
                        case 1 :
                            // Ecl.g:2994:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                            {
                            // Ecl.g:2994:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                            // Ecl.g:2995:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                            {
                            a3=(Token)match(input,32,FOLLOW_32_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2601); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            				if (element == null) {
                            					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            					startIncompleteElement(element);
                            				}
                            				collectHiddenTokens(element);
                            				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_2_0_0_1, null, true);
                            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                            			}

                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[479]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[480]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[481]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[482]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[483]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[484]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[485]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[486]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[487]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[488]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[489]);
                            				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[490]);
                            			}

                            // Ecl.g:3020:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                            // Ecl.g:3021:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                            {
                            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2627);
                            a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                            state._fsp--;
                            if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            					if (terminateParsing) {
                            						throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                            					}
                            					if (element == null) {
                            						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            						startIncompleteElement(element);
                            					}
                            					if (a4_0 != null) {
                            						if (a4_0 != null) {
                            							Object value = a4_0;
                            							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION), value);
                            							completedElement(value, true);
                            						}
                            						collectHiddenTokens(element);
                            						retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_30_0_0_2_0_0_3, a4_0, true);
                            						copyLocalizationInfos(a4_0, element);
                            					}
                            				}

                            }


                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[491]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[492]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[493]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[494]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[495]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[496]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[497]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[498]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[499]);
                            				addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[500]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[501]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[502]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[503]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[504]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[505]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[506]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[507]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[508]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[509]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[510]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[511]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[512]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[513]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[514]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[515]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[516]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[517]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[518]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[519]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[520]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[521]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[522]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[523]);
                            				addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[524]);
                            			}

                            }


                            }
                            break;

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[525]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[526]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[527]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[528]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[529]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[530]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[531]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[532]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[533]);
                    		addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[534]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[535]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[536]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[537]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[538]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[539]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[540]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[541]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[542]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[543]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[544]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[545]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[546]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[547]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[548]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[549]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[550]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[551]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[552]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[553]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[554]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[555]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[556]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[557]);
                    		addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[558]);
                    	}

                    }
                    break;
                case 2 :
                    // Ecl.g:3121:2: c0= parse_org_coolsoftware_ecl_Metaparameter
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2673);
                    c0=parse_org_coolsoftware_ecl_Metaparameter();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 16, parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_Variable"



    // $ANTLR start "parseop_Expression_level_03"
    // Ecl.g:3125:1: parseop_Expression_level_03 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_03() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_03_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return element; }

            // Ecl.g:3128:2: (leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |) )
            // Ecl.g:3129:2: leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_032698);
            leftArg=parseop_Expression_level_04();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:3129:40: ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |)
            int alt26=2;
            switch ( input.LA(1) ) {
            case 54:
                {
                int LA26_1 = input.LA(2);

                if ( (synpred27_Ecl()) ) {
                    alt26=1;
                }
                else if ( (true) ) {
                    alt26=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 26, 1, input);

                    throw nvae;

                }
                }
                break;
            case 37:
                {
                int LA26_2 = input.LA(2);

                if ( (synpred27_Ecl()) ) {
                    alt26=1;
                }
                else if ( (true) ) {
                    alt26=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 26, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 15:
            case 17:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 27:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 42:
            case 47:
            case 49:
            case 55:
            case 56:
            case 63:
                {
                alt26=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;

            }

            switch (alt26) {
                case 1 :
                    // Ecl.g:3129:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
                    {
                    // Ecl.g:3129:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
                    int cnt25=0;
                    loop25:
                    do {
                        int alt25=3;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==54) ) {
                            int LA25_2 = input.LA(2);

                            if ( (synpred25_Ecl()) ) {
                                alt25=1;
                            }


                        }
                        else if ( (LA25_0==37) ) {
                            int LA25_3 = input.LA(2);

                            if ( (synpred26_Ecl()) ) {
                                alt25=2;
                            }


                        }


                        switch (alt25) {
                    	case 1 :
                    	    // Ecl.g:3130:3: () a0= 'or' rightArg= parseop_Expression_level_04
                    	    {
                    	    // Ecl.g:3130:3: ()
                    	    // Ecl.g:3130:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,54,FOLLOW_54_in_parseop_Expression_level_032718); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_25_0_0_2, null, true);
                    	    			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[559]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[560]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[561]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[562]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[563]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[564]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[565]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[566]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[567]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[568]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[569]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[570]);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_032735);
                    	    rightArg=parseop_Expression_level_04();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_25_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_25_0_0_4, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Ecl.g:3197:3: () a0= 'and' rightArg= parseop_Expression_level_04
                    	    {
                    	    // Ecl.g:3197:3: ()
                    	    // Ecl.g:3197:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,37,FOLLOW_37_in_parseop_Expression_level_032769); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_26_0_0_2, null, true);
                    	    			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[571]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[572]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[573]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[574]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[575]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[576]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[577]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[578]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[579]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[580]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[581]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[582]);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_032786);
                    	    rightArg=parseop_Expression_level_04();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_26_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_26_0_0_4, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt25 >= 1 ) break loop25;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(25, input);
                                throw eee;
                        }
                        cnt25++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:3263:21: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 17, parseop_Expression_level_03_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_03"



    // $ANTLR start "parseop_Expression_level_04"
    // Ecl.g:3268:1: parseop_Expression_level_04 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_04() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_04_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return element; }

            // Ecl.g:3271:9: (leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |) )
            // Ecl.g:3272:9: leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_042832);
            leftArg=parseop_Expression_level_5();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:3272:37: ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |)
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==47) ) {
                int LA28_1 = input.LA(2);

                if ( (synpred29_Ecl()) ) {
                    alt28=1;
                }
                else if ( (true) ) {
                    alt28=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 28, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA28_0==EOF||LA28_0==TEXT||LA28_0==15||LA28_0==17||(LA28_0 >= 19 && LA28_0 <= 25)||LA28_0==27||(LA28_0 >= 29 && LA28_0 <= 37)||LA28_0==42||LA28_0==49||(LA28_0 >= 54 && LA28_0 <= 56)||LA28_0==63) ) {
                alt28=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;

            }
            switch (alt28) {
                case 1 :
                    // Ecl.g:3272:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
                    {
                    // Ecl.g:3272:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
                    int cnt27=0;
                    loop27:
                    do {
                        int alt27=2;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0==47) ) {
                            int LA27_2 = input.LA(2);

                            if ( (synpred28_Ecl()) ) {
                                alt27=1;
                            }


                        }


                        switch (alt27) {
                    	case 1 :
                    	    // Ecl.g:3273:2: () a0= 'implies' rightArg= parseop_Expression_level_5
                    	    {
                    	    // Ecl.g:3273:2: ()
                    	    // Ecl.g:3273:3: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,47,FOLLOW_47_in_parseop_Expression_level_042848); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_27_0_0_2, null, true);
                    	    		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		// expected elements (follow set)
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[583]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[584]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[585]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[586]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[587]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[588]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[589]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[590]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[591]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[592]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[593]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[594]);
                    	    	}

                    	    pushFollow(FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_042862);
                    	    rightArg=parseop_Expression_level_5();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (leftArg != null) {
                    	    			if (leftArg != null) {
                    	    				Object value = leftArg;
                    	    				element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_27_0_0_0, leftArg, true);
                    	    			copyLocalizationInfos(leftArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (rightArg != null) {
                    	    			if (rightArg != null) {
                    	    				Object value = rightArg;
                    	    				addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_27_0_0_4, rightArg, true);
                    	    			copyLocalizationInfos(rightArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt27 >= 1 ) break loop27;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(27, input);
                                throw eee;
                        }
                        cnt27++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:3339:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 18, parseop_Expression_level_04_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_04"



    // $ANTLR start "parseop_Expression_level_5"
    // Ecl.g:3344:1: parseop_Expression_level_5 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'f' arg= parseop_Expression_level_6 |a0= '!' arg= parseop_Expression_level_6 |arg= parseop_Expression_level_6 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_5() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_5_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return element; }

            // Ecl.g:3347:0: (a0= 'f' arg= parseop_Expression_level_6 |a0= '!' arg= parseop_Expression_level_6 |arg= parseop_Expression_level_6 )
            int alt29=3;
            switch ( input.LA(1) ) {
            case 43:
                {
                alt29=1;
                }
                break;
            case 14:
                {
                alt29=2;
                }
                break;
            case ILT:
            case QUOTED_34_34:
            case REAL_LITERAL:
            case TEXT:
            case 16:
            case 41:
            case 44:
            case 53:
            case 58:
            case 60:
            case 61:
                {
                alt29=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;

            }

            switch (alt29) {
                case 1 :
                    // Ecl.g:3348:0: a0= 'f' arg= parseop_Expression_level_6
                    {
                    a0=(Token)match(input,43,FOLLOW_43_in_parseop_Expression_level_52903); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_11_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[595]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[596]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[597]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[598]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[599]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[600]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[601]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[602]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[603]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[604]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[605]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[606]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_52914);
                    arg=parseop_Expression_level_6();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    }
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
                    	startIncompleteElement(element);
                    }
                    if (arg != null) {
                    	if (arg != null) {
                    		Object value = arg;
                    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP), value);
                    		completedElement(value, true);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_11_0_0_2, arg, true);
                    	copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Ecl.g:3393:0: a0= '!' arg= parseop_Expression_level_6
                    {
                    a0=(Token)match(input,14,FOLLOW_14_in_parseop_Expression_level_52923); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_28_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[607]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[608]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[609]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[610]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[611]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[612]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[613]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[614]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[615]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[616]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[617]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[618]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_52934);
                    arg=parseop_Expression_level_6();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    }
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
                    	startIncompleteElement(element);
                    }
                    if (arg != null) {
                    	if (arg != null) {
                    		Object value = arg;
                    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    		completedElement(value, true);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_28_0_0_2, arg, true);
                    	copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Ecl.g:3439:5: arg= parseop_Expression_level_6
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_52944);
                    arg=parseop_Expression_level_6();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 19, parseop_Expression_level_5_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_5"



    // $ANTLR start "parseop_Expression_level_6"
    // Ecl.g:3442:1: parseop_Expression_level_6 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_11 ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_6() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_6_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return element; }

            // Ecl.g:3445:9: (leftArg= parseop_Expression_level_11 ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |) )
            // Ecl.g:3446:9: leftArg= parseop_Expression_level_11 ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_62966);
            leftArg=parseop_Expression_level_11();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:3446:38: ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |)
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==32) ) {
                int LA31_1 = input.LA(2);

                if ( (synpred33_Ecl()) ) {
                    alt31=1;
                }
                else if ( (true) ) {
                    alt31=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 31, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA31_0==EOF||LA31_0==TEXT||LA31_0==15||LA31_0==17||(LA31_0 >= 19 && LA31_0 <= 25)||LA31_0==27||(LA31_0 >= 29 && LA31_0 <= 31)||(LA31_0 >= 33 && LA31_0 <= 37)||LA31_0==42||LA31_0==47||LA31_0==49||(LA31_0 >= 54 && LA31_0 <= 56)||LA31_0==63) ) {
                alt31=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;

            }
            switch (alt31) {
                case 1 :
                    // Ecl.g:3446:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
                    {
                    // Ecl.g:3446:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
                    int cnt30=0;
                    loop30:
                    do {
                        int alt30=2;
                        int LA30_0 = input.LA(1);

                        if ( (LA30_0==32) ) {
                            int LA30_2 = input.LA(2);

                            if ( (synpred32_Ecl()) ) {
                                alt30=1;
                            }


                        }


                        switch (alt30) {
                    	case 1 :
                    	    // Ecl.g:3447:0: () a0= '=' rightArg= parseop_Expression_level_11
                    	    {
                    	    // Ecl.g:3447:2: ()
                    	    // Ecl.g:3447:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,32,FOLLOW_32_in_parseop_Expression_level_62979); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_32_0_0_2, null, true);
                    	    	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[619]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[620]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[621]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[622]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[623]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[624]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[625]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[626]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[627]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[628]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[629]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[630]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_62990);
                    	    rightArg=parseop_Expression_level_11();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_32_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_32_0_0_4, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt30 >= 1 ) break loop30;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(30, input);
                                throw eee;
                        }
                        cnt30++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:3513:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 20, parseop_Expression_level_6_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_6"



    // $ANTLR start "parseop_Expression_level_11"
    // Ecl.g:3518:1: parseop_Expression_level_11 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_11() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_11_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return element; }

            // Ecl.g:3521:9: (leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |) )
            // Ecl.g:3522:9: leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113028);
            leftArg=parseop_Expression_level_12();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:3522:38: ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |)
            int alt33=2;
            switch ( input.LA(1) ) {
            case 34:
                {
                int LA33_1 = input.LA(2);

                if ( (synpred38_Ecl()) ) {
                    alt33=1;
                }
                else if ( (true) ) {
                    alt33=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 1, input);

                    throw nvae;

                }
                }
                break;
            case 35:
                {
                int LA33_2 = input.LA(2);

                if ( (synpred38_Ecl()) ) {
                    alt33=1;
                }
                else if ( (true) ) {
                    alt33=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 2, input);

                    throw nvae;

                }
                }
                break;
            case 30:
                {
                int LA33_3 = input.LA(2);

                if ( (synpred38_Ecl()) ) {
                    alt33=1;
                }
                else if ( (true) ) {
                    alt33=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 3, input);

                    throw nvae;

                }
                }
                break;
            case 31:
                {
                int LA33_4 = input.LA(2);

                if ( (synpred38_Ecl()) ) {
                    alt33=1;
                }
                else if ( (true) ) {
                    alt33=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 4, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 15:
            case 17:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 27:
            case 29:
            case 32:
            case 33:
            case 36:
            case 37:
            case 42:
            case 47:
            case 49:
            case 54:
            case 55:
            case 56:
            case 63:
                {
                alt33=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;

            }

            switch (alt33) {
                case 1 :
                    // Ecl.g:3522:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
                    {
                    // Ecl.g:3522:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
                    int cnt32=0;
                    loop32:
                    do {
                        int alt32=5;
                        switch ( input.LA(1) ) {
                        case 30:
                            {
                            int LA32_2 = input.LA(2);

                            if ( (synpred36_Ecl()) ) {
                                alt32=3;
                            }


                            }
                            break;
                        case 34:
                            {
                            int LA32_3 = input.LA(2);

                            if ( (synpred34_Ecl()) ) {
                                alt32=1;
                            }


                            }
                            break;
                        case 35:
                            {
                            int LA32_4 = input.LA(2);

                            if ( (synpred35_Ecl()) ) {
                                alt32=2;
                            }


                            }
                            break;
                        case 31:
                            {
                            int LA32_5 = input.LA(2);

                            if ( (synpred37_Ecl()) ) {
                                alt32=4;
                            }


                            }
                            break;

                        }

                        switch (alt32) {
                    	case 1 :
                    	    // Ecl.g:3523:0: () a0= '>' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Ecl.g:3523:2: ()
                    	    // Ecl.g:3523:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,34,FOLLOW_34_in_parseop_Expression_level_113041); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_19_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[631]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[632]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[633]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[634]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[635]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[636]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[637]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[638]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[639]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[640]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[641]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[642]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113052);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_19_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_19_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Ecl.g:3590:0: () a0= '>=' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Ecl.g:3590:2: ()
                    	    // Ecl.g:3590:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,35,FOLLOW_35_in_parseop_Expression_level_113070); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_20_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[643]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[644]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[645]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[646]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[647]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[648]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[649]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[650]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[651]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[652]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[653]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[654]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113081);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_20_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_20_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 3 :
                    	    // Ecl.g:3657:0: () a0= '<' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Ecl.g:3657:2: ()
                    	    // Ecl.g:3657:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,30,FOLLOW_30_in_parseop_Expression_level_113099); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_21_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[655]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[656]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[657]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[658]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[659]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[660]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[661]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[662]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[663]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[664]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[665]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[666]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113110);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_21_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_21_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 4 :
                    	    // Ecl.g:3724:0: () a0= '<=' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Ecl.g:3724:2: ()
                    	    // Ecl.g:3724:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,31,FOLLOW_31_in_parseop_Expression_level_113128); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_22_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[667]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[668]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[669]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[670]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[671]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[672]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[673]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[674]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[675]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[676]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[677]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[678]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113139);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_22_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_22_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt32 >= 1 ) break loop32;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(32, input);
                                throw eee;
                        }
                        cnt32++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:3790:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 21, parseop_Expression_level_11_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_11"



    // $ANTLR start "parseop_Expression_level_12"
    // Ecl.g:3795:1: parseop_Expression_level_12 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_12() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_12_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return element; }

            // Ecl.g:3798:9: (leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |) )
            // Ecl.g:3799:9: leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123177);
            leftArg=parseop_Expression_level_19();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:3799:38: ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |)
            int alt35=2;
            switch ( input.LA(1) ) {
            case 33:
                {
                int LA35_1 = input.LA(2);

                if ( (synpred41_Ecl()) ) {
                    alt35=1;
                }
                else if ( (true) ) {
                    alt35=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 35, 1, input);

                    throw nvae;

                }
                }
                break;
            case 15:
                {
                int LA35_2 = input.LA(2);

                if ( (synpred41_Ecl()) ) {
                    alt35=1;
                }
                else if ( (true) ) {
                    alt35=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 35, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 17:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 27:
            case 29:
            case 30:
            case 31:
            case 32:
            case 34:
            case 35:
            case 36:
            case 37:
            case 42:
            case 47:
            case 49:
            case 54:
            case 55:
            case 56:
            case 63:
                {
                alt35=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;

            }

            switch (alt35) {
                case 1 :
                    // Ecl.g:3799:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
                    {
                    // Ecl.g:3799:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
                    int cnt34=0;
                    loop34:
                    do {
                        int alt34=3;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==33) ) {
                            int LA34_2 = input.LA(2);

                            if ( (synpred39_Ecl()) ) {
                                alt34=1;
                            }


                        }
                        else if ( (LA34_0==15) ) {
                            int LA34_3 = input.LA(2);

                            if ( (synpred40_Ecl()) ) {
                                alt34=2;
                            }


                        }


                        switch (alt34) {
                    	case 1 :
                    	    // Ecl.g:3800:0: () a0= '==' rightArg= parseop_Expression_level_19
                    	    {
                    	    // Ecl.g:3800:2: ()
                    	    // Ecl.g:3800:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,33,FOLLOW_33_in_parseop_Expression_level_123190); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_23_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[679]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[680]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[681]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[682]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[683]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[684]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[685]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[686]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[687]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[688]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[689]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[690]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123201);
                    	    rightArg=parseop_Expression_level_19();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    	Object value = leftArg;
                    	    	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_23_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    	Object value = rightArg;
                    	    	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_23_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Ecl.g:3867:0: () a0= '!=' rightArg= parseop_Expression_level_19
                    	    {
                    	    // Ecl.g:3867:2: ()
                    	    // Ecl.g:3867:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,15,FOLLOW_15_in_parseop_Expression_level_123219); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_24_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[691]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[692]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[693]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[694]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[695]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[696]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[697]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[698]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[699]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[700]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[701]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[702]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123230);
                    	    rightArg=parseop_Expression_level_19();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    	Object value = leftArg;
                    	    	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_24_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    	Object value = rightArg;
                    	    	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_24_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt34 >= 1 ) break loop34;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(34, input);
                                throw eee;
                        }
                        cnt34++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:3933:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 22, parseop_Expression_level_12_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_12"



    // $ANTLR start "parseop_Expression_level_19"
    // Ecl.g:3938:1: parseop_Expression_level_19 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_19() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_19_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return element; }

            // Ecl.g:3941:9: (leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |) )
            // Ecl.g:3942:9: leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193268);
            leftArg=parseop_Expression_level_21();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:3942:38: ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |)
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==36) ) {
                int LA37_1 = input.LA(2);

                if ( (synpred43_Ecl()) ) {
                    alt37=1;
                }
                else if ( (true) ) {
                    alt37=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 37, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA37_0==EOF||LA37_0==TEXT||LA37_0==15||LA37_0==17||(LA37_0 >= 19 && LA37_0 <= 25)||LA37_0==27||(LA37_0 >= 29 && LA37_0 <= 35)||LA37_0==37||LA37_0==42||LA37_0==47||LA37_0==49||(LA37_0 >= 54 && LA37_0 <= 56)||LA37_0==63) ) {
                alt37=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;

            }
            switch (alt37) {
                case 1 :
                    // Ecl.g:3942:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
                    {
                    // Ecl.g:3942:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
                    int cnt36=0;
                    loop36:
                    do {
                        int alt36=2;
                        int LA36_0 = input.LA(1);

                        if ( (LA36_0==36) ) {
                            int LA36_2 = input.LA(2);

                            if ( (synpred42_Ecl()) ) {
                                alt36=1;
                            }


                        }


                        switch (alt36) {
                    	case 1 :
                    	    // Ecl.g:3943:0: () a0= '^' rightArg= parseop_Expression_level_21
                    	    {
                    	    // Ecl.g:3943:2: ()
                    	    // Ecl.g:3943:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,36,FOLLOW_36_in_parseop_Expression_level_193281); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_18_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[703]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[704]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[705]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[706]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[707]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[708]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[709]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[710]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[711]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[712]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[713]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[714]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193292);
                    	    rightArg=parseop_Expression_level_21();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_18_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_18_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt36 >= 1 ) break loop36;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(36, input);
                                throw eee;
                        }
                        cnt36++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:4009:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 23, parseop_Expression_level_19_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_19"



    // $ANTLR start "parseop_Expression_level_21"
    // Ecl.g:4014:1: parseop_Expression_level_21 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_21() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_21_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return element; }

            // Ecl.g:4017:9: (leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |) )
            // Ecl.g:4018:9: leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213330);
            leftArg=parseop_Expression_level_22();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:4018:38: ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |)
            int alt39=2;
            switch ( input.LA(1) ) {
            case 20:
                {
                int LA39_1 = input.LA(2);

                if ( (synpred46_Ecl()) ) {
                    alt39=1;
                }
                else if ( (true) ) {
                    alt39=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 39, 1, input);

                    throw nvae;

                }
                }
                break;
            case 23:
                {
                int LA39_2 = input.LA(2);

                if ( (synpred46_Ecl()) ) {
                    alt39=1;
                }
                else if ( (true) ) {
                    alt39=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 39, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 15:
            case 17:
            case 19:
            case 21:
            case 22:
            case 24:
            case 25:
            case 27:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 42:
            case 47:
            case 49:
            case 54:
            case 55:
            case 56:
            case 63:
                {
                alt39=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;

            }

            switch (alt39) {
                case 1 :
                    // Ecl.g:4018:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
                    {
                    // Ecl.g:4018:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
                    int cnt38=0;
                    loop38:
                    do {
                        int alt38=3;
                        int LA38_0 = input.LA(1);

                        if ( (LA38_0==20) ) {
                            int LA38_2 = input.LA(2);

                            if ( (synpred44_Ecl()) ) {
                                alt38=1;
                            }


                        }
                        else if ( (LA38_0==23) ) {
                            int LA38_3 = input.LA(2);

                            if ( (synpred45_Ecl()) ) {
                                alt38=2;
                            }


                        }


                        switch (alt38) {
                    	case 1 :
                    	    // Ecl.g:4019:0: () a0= '+' rightArg= parseop_Expression_level_22
                    	    {
                    	    // Ecl.g:4019:2: ()
                    	    // Ecl.g:4019:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,20,FOLLOW_20_in_parseop_Expression_level_213343); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_12_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[715]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[716]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[717]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[718]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[719]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[720]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[721]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[722]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[723]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[724]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[725]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[726]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213354);
                    	    rightArg=parseop_Expression_level_22();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_12_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_12_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Ecl.g:4086:0: () a0= '-' rightArg= parseop_Expression_level_22
                    	    {
                    	    // Ecl.g:4086:2: ()
                    	    // Ecl.g:4086:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,23,FOLLOW_23_in_parseop_Expression_level_213372); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_13_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[727]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[728]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[729]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[730]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[731]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[732]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[733]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[734]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[735]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[736]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[737]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[738]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213383);
                    	    rightArg=parseop_Expression_level_22();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_13_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_13_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt38 >= 1 ) break loop38;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(38, input);
                                throw eee;
                        }
                        cnt38++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:4152:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 24, parseop_Expression_level_21_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_21"



    // $ANTLR start "parseop_Expression_level_22"
    // Ecl.g:4157:1: parseop_Expression_level_22 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_22() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_22_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return element; }

            // Ecl.g:4160:9: (leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |) )
            // Ecl.g:4161:9: leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223421);
            leftArg=parseop_Expression_level_80();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:4161:38: ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |)
            int alt41=2;
            switch ( input.LA(1) ) {
            case 19:
                {
                int LA41_1 = input.LA(2);

                if ( (synpred49_Ecl()) ) {
                    alt41=1;
                }
                else if ( (true) ) {
                    alt41=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 41, 1, input);

                    throw nvae;

                }
                }
                break;
            case 27:
                {
                int LA41_2 = input.LA(2);

                if ( (synpred49_Ecl()) ) {
                    alt41=1;
                }
                else if ( (true) ) {
                    alt41=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 41, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 15:
            case 17:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 42:
            case 47:
            case 49:
            case 54:
            case 55:
            case 56:
            case 63:
                {
                alt41=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;

            }

            switch (alt41) {
                case 1 :
                    // Ecl.g:4161:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
                    {
                    // Ecl.g:4161:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
                    int cnt40=0;
                    loop40:
                    do {
                        int alt40=3;
                        int LA40_0 = input.LA(1);

                        if ( (LA40_0==19) ) {
                            int LA40_2 = input.LA(2);

                            if ( (synpred47_Ecl()) ) {
                                alt40=1;
                            }


                        }
                        else if ( (LA40_0==27) ) {
                            int LA40_3 = input.LA(2);

                            if ( (synpred48_Ecl()) ) {
                                alt40=2;
                            }


                        }


                        switch (alt40) {
                    	case 1 :
                    	    // Ecl.g:4162:0: () a0= '*' rightArg= parseop_Expression_level_80
                    	    {
                    	    // Ecl.g:4162:2: ()
                    	    // Ecl.g:4162:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,19,FOLLOW_19_in_parseop_Expression_level_223434); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_16_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[739]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[740]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[741]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[742]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[743]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[744]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[745]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[746]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[747]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[748]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[749]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[750]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223445);
                    	    rightArg=parseop_Expression_level_80();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_16_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_16_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Ecl.g:4229:0: () a0= '/' rightArg= parseop_Expression_level_80
                    	    {
                    	    // Ecl.g:4229:2: ()
                    	    // Ecl.g:4229:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,27,FOLLOW_27_in_parseop_Expression_level_223463); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_17_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[751]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[752]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[753]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[754]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[755]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[756]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[757]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[758]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[759]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[760]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[761]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[762]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223474);
                    	    rightArg=parseop_Expression_level_80();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_17_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_17_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt40 >= 1 ) break loop40;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(40, input);
                                throw eee;
                        }
                        cnt40++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:4295:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 25, parseop_Expression_level_22_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_22"



    // $ANTLR start "parseop_Expression_level_80"
    // Ecl.g:4300:1: parseop_Expression_level_80 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'sin' arg= parseop_Expression_level_87 |a0= 'cos' arg= parseop_Expression_level_87 |arg= parseop_Expression_level_87 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_80() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_80_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return element; }

            // Ecl.g:4303:0: (a0= 'sin' arg= parseop_Expression_level_87 |a0= 'cos' arg= parseop_Expression_level_87 |arg= parseop_Expression_level_87 )
            int alt42=3;
            switch ( input.LA(1) ) {
            case 58:
                {
                alt42=1;
                }
                break;
            case 41:
                {
                alt42=2;
                }
                break;
            case ILT:
            case QUOTED_34_34:
            case REAL_LITERAL:
            case TEXT:
            case 16:
            case 44:
            case 53:
            case 60:
            case 61:
                {
                alt42=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;

            }

            switch (alt42) {
                case 1 :
                    // Ecl.g:4304:0: a0= 'sin' arg= parseop_Expression_level_87
                    {
                    a0=(Token)match(input,58,FOLLOW_58_in_parseop_Expression_level_803512); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_9_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[763]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[764]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[765]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[766]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[767]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[768]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[769]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[770]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[771]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[772]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[773]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[774]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803523);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_9_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Ecl.g:4349:0: a0= 'cos' arg= parseop_Expression_level_87
                    {
                    a0=(Token)match(input,41,FOLLOW_41_in_parseop_Expression_level_803532); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_10_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[775]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[776]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[777]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[778]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[779]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[780]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[781]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[782]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[783]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[784]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[785]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[786]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803543);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_10_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Ecl.g:4395:5: arg= parseop_Expression_level_87
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803553);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 26, parseop_Expression_level_80_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_80"



    // $ANTLR start "parseop_Expression_level_87"
    // Ecl.g:4398:1: parseop_Expression_level_87 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'not' arg= parseop_Expression_level_88 |arg= parseop_Expression_level_88 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_87() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_87_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return element; }

            // Ecl.g:4401:0: (a0= 'not' arg= parseop_Expression_level_88 |arg= parseop_Expression_level_88 )
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==53) ) {
                alt43=1;
            }
            else if ( (LA43_0==ILT||LA43_0==QUOTED_34_34||LA43_0==REAL_LITERAL||LA43_0==TEXT||LA43_0==16||LA43_0==44||(LA43_0 >= 60 && LA43_0 <= 61)) ) {
                alt43=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;

            }
            switch (alt43) {
                case 1 :
                    // Ecl.g:4402:0: a0= 'not' arg= parseop_Expression_level_88
                    {
                    a0=(Token)match(input,53,FOLLOW_53_in_parseop_Expression_level_873575); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_8_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[787]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[788]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[789]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[790]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[791]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[792]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[793]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[794]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[795]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[796]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[797]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[798]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873586);
                    arg=parseop_Expression_level_88();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_8_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Ecl.g:4448:5: arg= parseop_Expression_level_88
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873596);
                    arg=parseop_Expression_level_88();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 27, parseop_Expression_level_87_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_87"



    // $ANTLR start "parseop_Expression_level_88"
    // Ecl.g:4451:1: parseop_Expression_level_88 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_88() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_88_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return element; }

            // Ecl.g:4454:5: (arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |) )
            // Ecl.g:4455:5: arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_90_in_parseop_Expression_level_883618);
            arg=parseop_Expression_level_90();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:4455:34: (a0= '++' |a0= '--' |)
            int alt44=3;
            switch ( input.LA(1) ) {
            case 21:
                {
                int LA44_1 = input.LA(2);

                if ( (synpred53_Ecl()) ) {
                    alt44=1;
                }
                else if ( (true) ) {
                    alt44=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 44, 1, input);

                    throw nvae;

                }
                }
                break;
            case 24:
                {
                int LA44_2 = input.LA(2);

                if ( (synpred54_Ecl()) ) {
                    alt44=2;
                }
                else if ( (true) ) {
                    alt44=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 44, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 15:
            case 17:
            case 19:
            case 20:
            case 22:
            case 23:
            case 25:
            case 27:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 42:
            case 47:
            case 49:
            case 54:
            case 55:
            case 56:
            case 63:
                {
                alt44=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;

            }

            switch (alt44) {
                case 1 :
                    // Ecl.g:4456:0: a0= '++'
                    {
                    a0=(Token)match(input,21,FOLLOW_21_in_parseop_Expression_level_883625); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_6_0_0_2, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[799]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[800]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[801]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[802]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[803]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[804]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[805]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[806]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[807]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[808]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[809]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[810]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[811]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[812]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[813]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[814]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[815]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[816]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[817]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[818]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[819]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[820]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[821]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[822]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[823]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[824]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[825]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[826]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[827]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[828]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[829]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[830]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[831]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[832]);
                    }

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_6_0_0_0, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Ecl.g:4523:0: a0= '--'
                    {
                    a0=(Token)match(input,24,FOLLOW_24_in_parseop_Expression_level_883640); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_7_0_0_2, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[833]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[834]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[835]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[836]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[837]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[838]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[839]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[840]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[841]);
                    addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[842]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[843]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[844]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[845]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[846]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[847]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[848]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[849]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[850]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[851]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[852]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[853]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[854]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[855]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[856]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[857]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[858]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[859]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[860]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[861]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[862]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[863]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[864]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[865]);
                    addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[866]);
                    }

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_7_0_0_0, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Ecl.g:4590:15: 
                    {
                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 28, parseop_Expression_level_88_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_88"



    // $ANTLR start "parseop_Expression_level_90"
    // Ecl.g:4594:1: parseop_Expression_level_90 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_90() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_90_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return element; }

            // Ecl.g:4597:9: (leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |) )
            // Ecl.g:4598:9: leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903677);
            leftArg=parseop_Expression_level_91();

            state._fsp--;
            if (state.failed) return element;

            // Ecl.g:4598:38: ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |)
            int alt46=2;
            switch ( input.LA(1) ) {
            case 22:
                {
                int LA46_1 = input.LA(2);

                if ( (synpred57_Ecl()) ) {
                    alt46=1;
                }
                else if ( (true) ) {
                    alt46=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 46, 1, input);

                    throw nvae;

                }
                }
                break;
            case 25:
                {
                int LA46_2 = input.LA(2);

                if ( (synpred57_Ecl()) ) {
                    alt46=1;
                }
                else if ( (true) ) {
                    alt46=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 46, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 15:
            case 17:
            case 19:
            case 20:
            case 21:
            case 23:
            case 24:
            case 27:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 42:
            case 47:
            case 49:
            case 54:
            case 55:
            case 56:
            case 63:
                {
                alt46=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;

            }

            switch (alt46) {
                case 1 :
                    // Ecl.g:4598:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
                    {
                    // Ecl.g:4598:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
                    int cnt45=0;
                    loop45:
                    do {
                        int alt45=3;
                        int LA45_0 = input.LA(1);

                        if ( (LA45_0==22) ) {
                            int LA45_2 = input.LA(2);

                            if ( (synpred55_Ecl()) ) {
                                alt45=1;
                            }


                        }
                        else if ( (LA45_0==25) ) {
                            int LA45_3 = input.LA(2);

                            if ( (synpred56_Ecl()) ) {
                                alt45=2;
                            }


                        }


                        switch (alt45) {
                    	case 1 :
                    	    // Ecl.g:4599:0: () a0= '+=' rightArg= parseop_Expression_level_91
                    	    {
                    	    // Ecl.g:4599:2: ()
                    	    // Ecl.g:4599:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,22,FOLLOW_22_in_parseop_Expression_level_903690); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_14_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[867]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[868]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[869]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[870]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[871]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[872]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[873]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[874]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[875]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[876]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[877]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[878]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903701);
                    	    rightArg=parseop_Expression_level_91();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_14_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_14_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Ecl.g:4666:0: () a0= '-=' rightArg= parseop_Expression_level_91
                    	    {
                    	    // Ecl.g:4666:2: ()
                    	    // Ecl.g:4666:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,25,FOLLOW_25_in_parseop_Expression_level_903719); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_15_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[879]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[880]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[881]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[882]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[883]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[884]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[885]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[886]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[887]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[888]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[889]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[890]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903730);
                    	    rightArg=parseop_Expression_level_91();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_15_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_15_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt45 >= 1 ) break loop45;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(45, input);
                                throw eee;
                        }
                        cnt45++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Ecl.g:4732:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 29, parseop_Expression_level_90_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_90"



    // $ANTLR start "parseop_Expression_level_91"
    // Ecl.g:4737:1: parseop_Expression_level_91 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression |c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression |c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression |c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression |c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression |c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration |c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_91() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_91_StartIndex = input.index();

        org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression c0 =null;

        org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression c1 =null;

        org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression c2 =null;

        org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression c3 =null;

        org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression c4 =null;

        org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration c5 =null;

        org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression c6 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return element; }

            // Ecl.g:4740:0: (c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression |c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression |c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression |c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression |c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression |c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration |c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression )
            int alt47=7;
            switch ( input.LA(1) ) {
            case ILT:
                {
                alt47=1;
                }
                break;
            case REAL_LITERAL:
                {
                alt47=2;
                }
                break;
            case 44:
            case 60:
                {
                alt47=3;
                }
                break;
            case QUOTED_34_34:
                {
                alt47=4;
                }
                break;
            case 16:
                {
                alt47=5;
                }
                break;
            case 61:
                {
                alt47=6;
                }
                break;
            case TEXT:
                {
                alt47=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;

            }

            switch (alt47) {
                case 1 :
                    // Ecl.g:4741:0: c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_in_parseop_Expression_level_913768);
                    c0=parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Ecl.g:4742:2: c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_in_parseop_Expression_level_913776);
                    c1=parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Ecl.g:4743:2: c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_in_parseop_Expression_level_913784);
                    c2=parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Ecl.g:4744:2: c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_in_parseop_Expression_level_913792);
                    c3=parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Ecl.g:4745:2: c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_in_parseop_Expression_level_913800);
                    c4=parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 6 :
                    // Ecl.g:4746:2: c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_in_parseop_Expression_level_913808);
                    c5=parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c5; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 7 :
                    // Ecl.g:4747:2: c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_in_parseop_Expression_level_913816);
                    c6=parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c6; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 30, parseop_Expression_level_91_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_91"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression"
    // Ecl.g:4750:1: parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element = null] : (a0= ILT ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return element; }

            // Ecl.g:4753:4: ( (a0= ILT ) )
            // Ecl.g:4754:4: (a0= ILT )
            {
            // Ecl.g:4754:4: (a0= ILT )
            // Ecl.g:4755:4: a0= ILT
            {
            a0=(Token)match(input,ILT,FOLLOW_ILT_in_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression3840); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createIntegerLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("ILT");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.Long resolved = (java.lang.Long) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_1_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[891]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[892]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[893]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[894]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[895]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[896]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[897]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[898]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[899]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[900]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[901]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[902]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[903]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[904]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[905]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[906]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[907]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[908]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[909]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[910]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[911]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[912]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[913]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[914]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[915]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[916]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[917]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[918]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[919]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[920]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[921]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[922]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[923]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[924]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 31, parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression"
    // Ecl.g:4825:1: parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element = null] : (a0= REAL_LITERAL ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return element; }

            // Ecl.g:4828:4: ( (a0= REAL_LITERAL ) )
            // Ecl.g:4829:4: (a0= REAL_LITERAL )
            {
            // Ecl.g:4829:4: (a0= REAL_LITERAL )
            // Ecl.g:4830:4: a0= REAL_LITERAL
            {
            a0=(Token)match(input,REAL_LITERAL,FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression3870); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createRealLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.Double resolved = (java.lang.Double) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_2_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[925]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[926]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[927]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[928]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[929]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[930]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[931]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[932]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[933]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[934]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[935]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[936]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[937]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[938]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[939]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[940]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[941]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[942]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[943]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[944]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[945]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[946]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[947]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[948]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[949]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[950]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[951]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[952]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[953]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[954]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[955]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[956]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[957]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[958]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 32, parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression"
    // Ecl.g:4900:1: parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element = null] : ( (a0= 'true' |a1= 'false' ) ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return element; }

            // Ecl.g:4903:0: ( ( (a0= 'true' |a1= 'false' ) ) )
            // Ecl.g:4904:0: ( (a0= 'true' |a1= 'false' ) )
            {
            // Ecl.g:4904:0: ( (a0= 'true' |a1= 'false' ) )
            // Ecl.g:4905:0: (a0= 'true' |a1= 'false' )
            {
            // Ecl.g:4905:0: (a0= 'true' |a1= 'false' )
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==60) ) {
                alt48=1;
            }
            else if ( (LA48_0==44) ) {
                alt48=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;

            }
            switch (alt48) {
                case 1 :
                    // Ecl.g:4906:0: a0= 'true'
                    {
                    a0=(Token)match(input,60,FOLLOW_60_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression3902); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_3_0_0_0, true, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    // set value of boolean attribute
                    Object value = true;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;
                case 2 :
                    // Ecl.g:4919:2: a1= 'false'
                    {
                    a1=(Token)match(input,44,FOLLOW_44_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression3911); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_3_0_0_0, false, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    // set value of boolean attribute
                    Object value = false;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[959]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[960]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[961]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[962]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[963]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[964]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[965]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[966]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[967]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[968]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[969]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[970]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[971]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[972]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[973]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[974]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[975]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[976]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[977]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[978]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[979]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[980]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[981]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[982]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[983]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[984]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[985]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[986]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[987]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[988]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[989]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[990]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[991]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[992]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 33, parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression"
    // Ecl.g:4974:1: parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element = null] : (a0= QUOTED_34_34 ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return element; }

            // Ecl.g:4977:4: ( (a0= QUOTED_34_34 ) )
            // Ecl.g:4978:4: (a0= QUOTED_34_34 )
            {
            // Ecl.g:4978:4: (a0= QUOTED_34_34 )
            // Ecl.g:4979:4: a0= QUOTED_34_34
            {
            a0=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression3943); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createStringLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.String resolved = (java.lang.String) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_4_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[993]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[994]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[995]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[996]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[997]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[998]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[999]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1000]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1001]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1002]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1003]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1004]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1005]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1006]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1007]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1008]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1009]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1010]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1011]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1012]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1013]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1014]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1015]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1016]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1017]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1018]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1019]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1020]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1021]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1022]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1023]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1024]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1025]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1026]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 34, parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression"
    // Ecl.g:5049:1: parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression returns [org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element = null] : a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')' ;
    public final org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return element; }

            // Ecl.g:5052:4: (a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')' )
            // Ecl.g:5053:4: a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')'
            {
            a0=(Token)match(input,16,FOLLOW_16_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression3971); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_5_0_0_0, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1027]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1028]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1029]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1030]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1031]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1032]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1033]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1034]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1035]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1036]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1037]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1038]);
            }

            // Ecl.g:5078:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            // Ecl.g:5079:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression3984);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            Object value = a1_0;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP), value);
            completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_5_0_0_2, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1039]);
            }

            a2=(Token)match(input,17,FOLLOW_17_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression3996); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_5_0_0_4, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1040]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1041]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1042]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1043]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1044]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1045]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1046]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1047]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1048]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1049]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1050]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1051]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1052]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1053]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1054]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1055]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1056]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1057]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1058]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1059]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1060]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1061]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1062]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1063]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1064]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1065]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1066]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1067]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1068]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1069]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1070]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1071]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1072]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1073]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 35, parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration"
    // Ecl.g:5153:1: parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration returns [org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element = null] : a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable ) ;
    public final org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.variables.Variable a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return element; }

            // Ecl.g:5156:4: (a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable ) )
            // Ecl.g:5157:4: a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable )
            {
            a0=(Token)match(input,61,FOLLOW_61_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4022); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_29_0_0_0, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableDeclaration(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1074]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableDeclaration(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1075]);
            }

            // Ecl.g:5172:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable )
            // Ecl.g:5173:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4035);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            Object value = a1_0;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE), value);
            completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_29_0_0_2, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1076]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1077]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1078]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1079]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1080]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1081]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1082]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1083]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1084]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1085]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1086]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1087]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1088]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1089]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1090]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1091]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1092]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1093]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1094]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1095]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1096]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1097]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1098]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1099]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1100]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1101]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1102]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1103]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1104]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1105]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1106]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1107]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1108]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1109]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 36, parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression"
    // Ecl.g:5233:1: parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression returns [org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element = null] : (a0= TEXT ) ;
    public final org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return element; }

            // Ecl.g:5236:4: ( (a0= TEXT ) )
            // Ecl.g:5237:4: (a0= TEXT )
            {
            // Ecl.g:5237:4: (a0= TEXT )
            // Ecl.g:5238:4: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression4064); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.ecl.resource.ecl.mopp.EclTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableCallExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.ecl.resource.ecl.IEclTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            String resolved = (String) resolvedObject;
            org.coolsoftware.coolcomponents.expressions.variables.Variable proxy = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
            collectHiddenTokens(element);
            registerContextDependentProxy(new org.coolsoftware.ecl.resource.ecl.mopp.EclContextDependentURIFragmentFactory<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableCallExpressionReferredVariableReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), resolved, proxy);
            if (proxy != null) {
            Object value = proxy;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.ecl.resource.ecl.grammar.EclGrammarInformationProvider.EXP_31_0_0_0, proxy, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1110]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1111]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1112]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1113]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1114]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWContractMode(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1115]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1116]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1117]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1118]);
            addExpectedElement(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause(), org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1119]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1120]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1121]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1122]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1123]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1124]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1125]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1126]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1127]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1128]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1129]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1130]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1131]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1132]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1133]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1134]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1135]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1136]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1137]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1138]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1139]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1140]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1141]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1142]);
            addExpectedElement(null, org.coolsoftware.ecl.resource.ecl.mopp.EclExpectationConstants.EXPECTATIONS[1143]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 37, parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression"



    // $ANTLR start "parse_org_coolsoftware_ecl_Import"
    // Ecl.g:5312:1: parse_org_coolsoftware_ecl_Import returns [org.coolsoftware.ecl.Import element = null] : c0= parse_org_coolsoftware_ecl_CcmImport ;
    public final org.coolsoftware.ecl.Import parse_org_coolsoftware_ecl_Import() throws RecognitionException {
        org.coolsoftware.ecl.Import element =  null;

        int parse_org_coolsoftware_ecl_Import_StartIndex = input.index();

        org.coolsoftware.ecl.CcmImport c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return element; }

            // Ecl.g:5313:4: (c0= parse_org_coolsoftware_ecl_CcmImport )
            // Ecl.g:5314:4: c0= parse_org_coolsoftware_ecl_CcmImport
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_ecl_CcmImport_in_parse_org_coolsoftware_ecl_Import4088);
            c0=parse_org_coolsoftware_ecl_CcmImport();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 38, parse_org_coolsoftware_ecl_Import_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_Import"



    // $ANTLR start "parse_org_coolsoftware_ecl_EclContract"
    // Ecl.g:5318:1: parse_org_coolsoftware_ecl_EclContract returns [org.coolsoftware.ecl.EclContract element = null] : (c0= parse_org_coolsoftware_ecl_SWComponentContract |c1= parse_org_coolsoftware_ecl_HWComponentContract );
    public final org.coolsoftware.ecl.EclContract parse_org_coolsoftware_ecl_EclContract() throws RecognitionException {
        org.coolsoftware.ecl.EclContract element =  null;

        int parse_org_coolsoftware_ecl_EclContract_StartIndex = input.index();

        org.coolsoftware.ecl.SWComponentContract c0 =null;

        org.coolsoftware.ecl.HWComponentContract c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return element; }

            // Ecl.g:5319:0: (c0= parse_org_coolsoftware_ecl_SWComponentContract |c1= parse_org_coolsoftware_ecl_HWComponentContract )
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==40) ) {
                int LA49_1 = input.LA(2);

                if ( (LA49_1==TEXT) ) {
                    int LA49_2 = input.LA(3);

                    if ( (LA49_2==46) ) {
                        int LA49_3 = input.LA(4);

                        if ( (LA49_3==59) ) {
                            alt49=1;
                        }
                        else if ( (LA49_3==45) ) {
                            alt49=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 49, 3, input);

                            throw nvae;

                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 49, 2, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 49, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;

            }
            switch (alt49) {
                case 1 :
                    // Ecl.g:5320:0: c0= parse_org_coolsoftware_ecl_SWComponentContract
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4107);
                    c0=parse_org_coolsoftware_ecl_SWComponentContract();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Ecl.g:5321:2: c1= parse_org_coolsoftware_ecl_HWComponentContract
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4115);
                    c1=parse_org_coolsoftware_ecl_HWComponentContract();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 39, parse_org_coolsoftware_ecl_EclContract_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_EclContract"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWContractClause"
    // Ecl.g:5325:1: parse_org_coolsoftware_ecl_SWContractClause returns [org.coolsoftware.ecl.SWContractClause element = null] : (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_SWComponentRequirementClause |c2= parse_org_coolsoftware_ecl_HWComponentRequirementClause |c3= parse_org_coolsoftware_ecl_SelfRequirementClause );
    public final org.coolsoftware.ecl.SWContractClause parse_org_coolsoftware_ecl_SWContractClause() throws RecognitionException {
        org.coolsoftware.ecl.SWContractClause element =  null;

        int parse_org_coolsoftware_ecl_SWContractClause_StartIndex = input.index();

        org.coolsoftware.ecl.ProvisionClause c0 =null;

        org.coolsoftware.ecl.SWComponentRequirementClause c1 =null;

        org.coolsoftware.ecl.HWComponentRequirementClause c2 =null;

        org.coolsoftware.ecl.SelfRequirementClause c3 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return element; }

            // Ecl.g:5326:0: (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_SWComponentRequirementClause |c2= parse_org_coolsoftware_ecl_HWComponentRequirementClause |c3= parse_org_coolsoftware_ecl_SelfRequirementClause )
            int alt50=4;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==55) ) {
                alt50=1;
            }
            else if ( (LA50_0==56) ) {
                switch ( input.LA(2) ) {
                case 39:
                    {
                    alt50=2;
                    }
                    break;
                case 57:
                    {
                    alt50=3;
                    }
                    break;
                case TEXT:
                    {
                    alt50=4;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 50, 2, input);

                    throw nvae;

                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;

            }
            switch (alt50) {
                case 1 :
                    // Ecl.g:5327:0: c0= parse_org_coolsoftware_ecl_ProvisionClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_SWContractClause4134);
                    c0=parse_org_coolsoftware_ecl_ProvisionClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Ecl.g:5328:2: c1= parse_org_coolsoftware_ecl_SWComponentRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4142);
                    c1=parse_org_coolsoftware_ecl_SWComponentRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Ecl.g:5329:2: c2= parse_org_coolsoftware_ecl_HWComponentRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4150);
                    c2=parse_org_coolsoftware_ecl_HWComponentRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Ecl.g:5330:2: c3= parse_org_coolsoftware_ecl_SelfRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SelfRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4158);
                    c3=parse_org_coolsoftware_ecl_SelfRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 40, parse_org_coolsoftware_ecl_SWContractClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWContractClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWContractClause"
    // Ecl.g:5334:1: parse_org_coolsoftware_ecl_HWContractClause returns [org.coolsoftware.ecl.HWContractClause element = null] : (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_HWComponentRequirementClause );
    public final org.coolsoftware.ecl.HWContractClause parse_org_coolsoftware_ecl_HWContractClause() throws RecognitionException {
        org.coolsoftware.ecl.HWContractClause element =  null;

        int parse_org_coolsoftware_ecl_HWContractClause_StartIndex = input.index();

        org.coolsoftware.ecl.ProvisionClause c0 =null;

        org.coolsoftware.ecl.HWComponentRequirementClause c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return element; }

            // Ecl.g:5335:0: (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_HWComponentRequirementClause )
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==55) ) {
                alt51=1;
            }
            else if ( (LA51_0==56) ) {
                alt51=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 51, 0, input);

                throw nvae;

            }
            switch (alt51) {
                case 1 :
                    // Ecl.g:5336:0: c0= parse_org_coolsoftware_ecl_ProvisionClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_HWContractClause4177);
                    c0=parse_org_coolsoftware_ecl_ProvisionClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Ecl.g:5337:2: c1= parse_org_coolsoftware_ecl_HWComponentRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_HWContractClause4185);
                    c1=parse_org_coolsoftware_ecl_HWComponentRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 41, parse_org_coolsoftware_ecl_HWContractClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWContractClause"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_Expression"
    // Ecl.g:5341:1: parse_org_coolsoftware_coolcomponents_expressions_Expression returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : c= parseop_Expression_level_03 ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parse_org_coolsoftware_coolcomponents_expressions_Expression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_Expression_StartIndex = input.index();

        org.coolsoftware.coolcomponents.expressions.Expression c =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 42) ) { return element; }

            // Ecl.g:5342:3: (c= parseop_Expression_level_03 )
            // Ecl.g:5343:3: c= parseop_Expression_level_03
            {
            pushFollow(FOLLOW_parseop_Expression_level_03_in_parse_org_coolsoftware_coolcomponents_expressions_Expression4204);
            c=parseop_Expression_level_03();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c; /* this rule is an expression root */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 42, parse_org_coolsoftware_coolcomponents_expressions_Expression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_Expression"

    // $ANTLR start synpred23_Ecl
    public final void synpred23_Ecl_fragment() throws RecognitionException {
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;


        // Ecl.g:2994:3: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )
        // Ecl.g:2994:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
        {
        // Ecl.g:2994:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
        // Ecl.g:2995:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        {
        a3=(Token)match(input,32,FOLLOW_32_in_synpred23_Ecl2601); if (state.failed) return ;

        // Ecl.g:3020:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        // Ecl.g:3021:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
        {
        pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred23_Ecl2627);
        a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }

    }
    // $ANTLR end synpred23_Ecl

    // $ANTLR start synpred24_Ecl
    public final void synpred24_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;


        // Ecl.g:2795:2: ( (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? )
        // Ecl.g:2795:2: (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
        {
        // Ecl.g:2795:2: (a0= TEXT )
        // Ecl.g:2796:3: a0= TEXT
        {
        a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_synpred24_Ecl2490); if (state.failed) return ;

        }


        // Ecl.g:2866:2: ( (a1= ':' (a2= TYPE_LITERAL ) ) )?
        int alt55=2;
        int LA55_0 = input.LA(1);

        if ( (LA55_0==28) ) {
            alt55=1;
        }
        switch (alt55) {
            case 1 :
                // Ecl.g:2867:3: (a1= ':' (a2= TYPE_LITERAL ) )
                {
                // Ecl.g:2867:3: (a1= ':' (a2= TYPE_LITERAL ) )
                // Ecl.g:2868:4: a1= ':' (a2= TYPE_LITERAL )
                {
                a1=(Token)match(input,28,FOLLOW_28_in_synpred24_Ecl2520); if (state.failed) return ;

                // Ecl.g:2882:4: (a2= TYPE_LITERAL )
                // Ecl.g:2883:5: a2= TYPE_LITERAL
                {
                a2=(Token)match(input,TYPE_LITERAL,FOLLOW_TYPE_LITERAL_in_synpred24_Ecl2546); if (state.failed) return ;

                }


                }


                }
                break;

        }


        // Ecl.g:2993:2: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
        int alt56=2;
        int LA56_0 = input.LA(1);

        if ( (LA56_0==32) ) {
            alt56=1;
        }
        switch (alt56) {
            case 1 :
                // Ecl.g:2994:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                {
                // Ecl.g:2994:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                // Ecl.g:2995:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                {
                a3=(Token)match(input,32,FOLLOW_32_in_synpred24_Ecl2601); if (state.failed) return ;

                // Ecl.g:3020:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                // Ecl.g:3021:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                {
                pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred24_Ecl2627);
                a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                state._fsp--;
                if (state.failed) return ;

                }


                }


                }
                break;

        }


        }

    }
    // $ANTLR end synpred24_Ecl

    // $ANTLR start synpred25_Ecl
    public final void synpred25_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3130:3: ( () a0= 'or' rightArg= parseop_Expression_level_04 )
        // Ecl.g:3130:3: () a0= 'or' rightArg= parseop_Expression_level_04
        {
        // Ecl.g:3130:3: ()
        // Ecl.g:3130:4: 
        {
        }


        a0=(Token)match(input,54,FOLLOW_54_in_synpred25_Ecl2718); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred25_Ecl2735);
        rightArg=parseop_Expression_level_04();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred25_Ecl

    // $ANTLR start synpred26_Ecl
    public final void synpred26_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3197:3: ( () a0= 'and' rightArg= parseop_Expression_level_04 )
        // Ecl.g:3197:3: () a0= 'and' rightArg= parseop_Expression_level_04
        {
        // Ecl.g:3197:3: ()
        // Ecl.g:3197:4: 
        {
        }


        a0=(Token)match(input,37,FOLLOW_37_in_synpred26_Ecl2769); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred26_Ecl2786);
        rightArg=parseop_Expression_level_04();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred26_Ecl

    // $ANTLR start synpred27_Ecl
    public final void synpred27_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3129:41: ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ )
        // Ecl.g:3129:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
        {
        // Ecl.g:3129:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
        int cnt57=0;
        loop57:
        do {
            int alt57=3;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==54) ) {
                alt57=1;
            }
            else if ( (LA57_0==37) ) {
                alt57=2;
            }


            switch (alt57) {
        	case 1 :
        	    // Ecl.g:3130:3: () a0= 'or' rightArg= parseop_Expression_level_04
        	    {
        	    // Ecl.g:3130:3: ()
        	    // Ecl.g:3130:4: 
        	    {
        	    }


        	    a0=(Token)match(input,54,FOLLOW_54_in_synpred27_Ecl2718); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred27_Ecl2735);
        	    rightArg=parseop_Expression_level_04();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Ecl.g:3197:3: () a0= 'and' rightArg= parseop_Expression_level_04
        	    {
        	    // Ecl.g:3197:3: ()
        	    // Ecl.g:3197:4: 
        	    {
        	    }


        	    a0=(Token)match(input,37,FOLLOW_37_in_synpred27_Ecl2769); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred27_Ecl2786);
        	    rightArg=parseop_Expression_level_04();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt57 >= 1 ) break loop57;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(57, input);
                    throw eee;
            }
            cnt57++;
        } while (true);


        }

    }
    // $ANTLR end synpred27_Ecl

    // $ANTLR start synpred28_Ecl
    public final void synpred28_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3273:2: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )
        // Ecl.g:3273:2: () a0= 'implies' rightArg= parseop_Expression_level_5
        {
        // Ecl.g:3273:2: ()
        // Ecl.g:3273:3: 
        {
        }


        a0=(Token)match(input,47,FOLLOW_47_in_synpred28_Ecl2848); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_5_in_synpred28_Ecl2862);
        rightArg=parseop_Expression_level_5();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred28_Ecl

    // $ANTLR start synpred29_Ecl
    public final void synpred29_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3272:38: ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ )
        // Ecl.g:3272:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
        {
        // Ecl.g:3272:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
        int cnt58=0;
        loop58:
        do {
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==47) ) {
                alt58=1;
            }


            switch (alt58) {
        	case 1 :
        	    // Ecl.g:3273:2: () a0= 'implies' rightArg= parseop_Expression_level_5
        	    {
        	    // Ecl.g:3273:2: ()
        	    // Ecl.g:3273:3: 
        	    {
        	    }


        	    a0=(Token)match(input,47,FOLLOW_47_in_synpred29_Ecl2848); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_5_in_synpred29_Ecl2862);
        	    rightArg=parseop_Expression_level_5();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt58 >= 1 ) break loop58;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(58, input);
                    throw eee;
            }
            cnt58++;
        } while (true);


        }

    }
    // $ANTLR end synpred29_Ecl

    // $ANTLR start synpred32_Ecl
    public final void synpred32_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3447:2: ( () a0= '=' rightArg= parseop_Expression_level_11 )
        // Ecl.g:3447:2: () a0= '=' rightArg= parseop_Expression_level_11
        {
        // Ecl.g:3447:2: ()
        // Ecl.g:3447:2: 
        {
        }


        a0=(Token)match(input,32,FOLLOW_32_in_synpred32_Ecl2979); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_11_in_synpred32_Ecl2990);
        rightArg=parseop_Expression_level_11();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred32_Ecl

    // $ANTLR start synpred33_Ecl
    public final void synpred33_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3446:39: ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ )
        // Ecl.g:3446:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
        {
        // Ecl.g:3446:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
        int cnt59=0;
        loop59:
        do {
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==32) ) {
                alt59=1;
            }


            switch (alt59) {
        	case 1 :
        	    // Ecl.g:3447:0: () a0= '=' rightArg= parseop_Expression_level_11
        	    {
        	    // Ecl.g:3447:2: ()
        	    // Ecl.g:3447:2: 
        	    {
        	    }


        	    a0=(Token)match(input,32,FOLLOW_32_in_synpred33_Ecl2979); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_11_in_synpred33_Ecl2990);
        	    rightArg=parseop_Expression_level_11();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt59 >= 1 ) break loop59;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(59, input);
                    throw eee;
            }
            cnt59++;
        } while (true);


        }

    }
    // $ANTLR end synpred33_Ecl

    // $ANTLR start synpred34_Ecl
    public final void synpred34_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3523:2: ( () a0= '>' rightArg= parseop_Expression_level_12 )
        // Ecl.g:3523:2: () a0= '>' rightArg= parseop_Expression_level_12
        {
        // Ecl.g:3523:2: ()
        // Ecl.g:3523:2: 
        {
        }


        a0=(Token)match(input,34,FOLLOW_34_in_synpred34_Ecl3041); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred34_Ecl3052);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred34_Ecl

    // $ANTLR start synpred35_Ecl
    public final void synpred35_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3590:2: ( () a0= '>=' rightArg= parseop_Expression_level_12 )
        // Ecl.g:3590:2: () a0= '>=' rightArg= parseop_Expression_level_12
        {
        // Ecl.g:3590:2: ()
        // Ecl.g:3590:2: 
        {
        }


        a0=(Token)match(input,35,FOLLOW_35_in_synpred35_Ecl3070); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred35_Ecl3081);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred35_Ecl

    // $ANTLR start synpred36_Ecl
    public final void synpred36_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3657:2: ( () a0= '<' rightArg= parseop_Expression_level_12 )
        // Ecl.g:3657:2: () a0= '<' rightArg= parseop_Expression_level_12
        {
        // Ecl.g:3657:2: ()
        // Ecl.g:3657:2: 
        {
        }


        a0=(Token)match(input,30,FOLLOW_30_in_synpred36_Ecl3099); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred36_Ecl3110);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred36_Ecl

    // $ANTLR start synpred37_Ecl
    public final void synpred37_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3724:2: ( () a0= '<=' rightArg= parseop_Expression_level_12 )
        // Ecl.g:3724:2: () a0= '<=' rightArg= parseop_Expression_level_12
        {
        // Ecl.g:3724:2: ()
        // Ecl.g:3724:2: 
        {
        }


        a0=(Token)match(input,31,FOLLOW_31_in_synpred37_Ecl3128); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred37_Ecl3139);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred37_Ecl

    // $ANTLR start synpred38_Ecl
    public final void synpred38_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3522:39: ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ )
        // Ecl.g:3522:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
        {
        // Ecl.g:3522:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
        int cnt60=0;
        loop60:
        do {
            int alt60=5;
            switch ( input.LA(1) ) {
            case 34:
                {
                alt60=1;
                }
                break;
            case 35:
                {
                alt60=2;
                }
                break;
            case 30:
                {
                alt60=3;
                }
                break;
            case 31:
                {
                alt60=4;
                }
                break;

            }

            switch (alt60) {
        	case 1 :
        	    // Ecl.g:3523:0: () a0= '>' rightArg= parseop_Expression_level_12
        	    {
        	    // Ecl.g:3523:2: ()
        	    // Ecl.g:3523:2: 
        	    {
        	    }


        	    a0=(Token)match(input,34,FOLLOW_34_in_synpred38_Ecl3041); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3052);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Ecl.g:3590:0: () a0= '>=' rightArg= parseop_Expression_level_12
        	    {
        	    // Ecl.g:3590:2: ()
        	    // Ecl.g:3590:2: 
        	    {
        	    }


        	    a0=(Token)match(input,35,FOLLOW_35_in_synpred38_Ecl3070); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3081);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 3 :
        	    // Ecl.g:3657:0: () a0= '<' rightArg= parseop_Expression_level_12
        	    {
        	    // Ecl.g:3657:2: ()
        	    // Ecl.g:3657:2: 
        	    {
        	    }


        	    a0=(Token)match(input,30,FOLLOW_30_in_synpred38_Ecl3099); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3110);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 4 :
        	    // Ecl.g:3724:0: () a0= '<=' rightArg= parseop_Expression_level_12
        	    {
        	    // Ecl.g:3724:2: ()
        	    // Ecl.g:3724:2: 
        	    {
        	    }


        	    a0=(Token)match(input,31,FOLLOW_31_in_synpred38_Ecl3128); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3139);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt60 >= 1 ) break loop60;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(60, input);
                    throw eee;
            }
            cnt60++;
        } while (true);


        }

    }
    // $ANTLR end synpred38_Ecl

    // $ANTLR start synpred39_Ecl
    public final void synpred39_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3800:2: ( () a0= '==' rightArg= parseop_Expression_level_19 )
        // Ecl.g:3800:2: () a0= '==' rightArg= parseop_Expression_level_19
        {
        // Ecl.g:3800:2: ()
        // Ecl.g:3800:2: 
        {
        }


        a0=(Token)match(input,33,FOLLOW_33_in_synpred39_Ecl3190); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred39_Ecl3201);
        rightArg=parseop_Expression_level_19();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred39_Ecl

    // $ANTLR start synpred40_Ecl
    public final void synpred40_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3867:2: ( () a0= '!=' rightArg= parseop_Expression_level_19 )
        // Ecl.g:3867:2: () a0= '!=' rightArg= parseop_Expression_level_19
        {
        // Ecl.g:3867:2: ()
        // Ecl.g:3867:2: 
        {
        }


        a0=(Token)match(input,15,FOLLOW_15_in_synpred40_Ecl3219); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred40_Ecl3230);
        rightArg=parseop_Expression_level_19();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred40_Ecl

    // $ANTLR start synpred41_Ecl
    public final void synpred41_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3799:39: ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ )
        // Ecl.g:3799:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
        {
        // Ecl.g:3799:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
        int cnt61=0;
        loop61:
        do {
            int alt61=3;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==33) ) {
                alt61=1;
            }
            else if ( (LA61_0==15) ) {
                alt61=2;
            }


            switch (alt61) {
        	case 1 :
        	    // Ecl.g:3800:0: () a0= '==' rightArg= parseop_Expression_level_19
        	    {
        	    // Ecl.g:3800:2: ()
        	    // Ecl.g:3800:2: 
        	    {
        	    }


        	    a0=(Token)match(input,33,FOLLOW_33_in_synpred41_Ecl3190); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred41_Ecl3201);
        	    rightArg=parseop_Expression_level_19();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Ecl.g:3867:0: () a0= '!=' rightArg= parseop_Expression_level_19
        	    {
        	    // Ecl.g:3867:2: ()
        	    // Ecl.g:3867:2: 
        	    {
        	    }


        	    a0=(Token)match(input,15,FOLLOW_15_in_synpred41_Ecl3219); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred41_Ecl3230);
        	    rightArg=parseop_Expression_level_19();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt61 >= 1 ) break loop61;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(61, input);
                    throw eee;
            }
            cnt61++;
        } while (true);


        }

    }
    // $ANTLR end synpred41_Ecl

    // $ANTLR start synpred42_Ecl
    public final void synpred42_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3943:2: ( () a0= '^' rightArg= parseop_Expression_level_21 )
        // Ecl.g:3943:2: () a0= '^' rightArg= parseop_Expression_level_21
        {
        // Ecl.g:3943:2: ()
        // Ecl.g:3943:2: 
        {
        }


        a0=(Token)match(input,36,FOLLOW_36_in_synpred42_Ecl3281); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_21_in_synpred42_Ecl3292);
        rightArg=parseop_Expression_level_21();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred42_Ecl

    // $ANTLR start synpred43_Ecl
    public final void synpred43_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:3942:39: ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ )
        // Ecl.g:3942:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
        {
        // Ecl.g:3942:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
        int cnt62=0;
        loop62:
        do {
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==36) ) {
                alt62=1;
            }


            switch (alt62) {
        	case 1 :
        	    // Ecl.g:3943:0: () a0= '^' rightArg= parseop_Expression_level_21
        	    {
        	    // Ecl.g:3943:2: ()
        	    // Ecl.g:3943:2: 
        	    {
        	    }


        	    a0=(Token)match(input,36,FOLLOW_36_in_synpred43_Ecl3281); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_21_in_synpred43_Ecl3292);
        	    rightArg=parseop_Expression_level_21();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt62 >= 1 ) break loop62;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(62, input);
                    throw eee;
            }
            cnt62++;
        } while (true);


        }

    }
    // $ANTLR end synpred43_Ecl

    // $ANTLR start synpred44_Ecl
    public final void synpred44_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4019:2: ( () a0= '+' rightArg= parseop_Expression_level_22 )
        // Ecl.g:4019:2: () a0= '+' rightArg= parseop_Expression_level_22
        {
        // Ecl.g:4019:2: ()
        // Ecl.g:4019:2: 
        {
        }


        a0=(Token)match(input,20,FOLLOW_20_in_synpred44_Ecl3343); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred44_Ecl3354);
        rightArg=parseop_Expression_level_22();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred44_Ecl

    // $ANTLR start synpred45_Ecl
    public final void synpred45_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4086:2: ( () a0= '-' rightArg= parseop_Expression_level_22 )
        // Ecl.g:4086:2: () a0= '-' rightArg= parseop_Expression_level_22
        {
        // Ecl.g:4086:2: ()
        // Ecl.g:4086:2: 
        {
        }


        a0=(Token)match(input,23,FOLLOW_23_in_synpred45_Ecl3372); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred45_Ecl3383);
        rightArg=parseop_Expression_level_22();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred45_Ecl

    // $ANTLR start synpred46_Ecl
    public final void synpred46_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4018:39: ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ )
        // Ecl.g:4018:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
        {
        // Ecl.g:4018:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
        int cnt63=0;
        loop63:
        do {
            int alt63=3;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==20) ) {
                alt63=1;
            }
            else if ( (LA63_0==23) ) {
                alt63=2;
            }


            switch (alt63) {
        	case 1 :
        	    // Ecl.g:4019:0: () a0= '+' rightArg= parseop_Expression_level_22
        	    {
        	    // Ecl.g:4019:2: ()
        	    // Ecl.g:4019:2: 
        	    {
        	    }


        	    a0=(Token)match(input,20,FOLLOW_20_in_synpred46_Ecl3343); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred46_Ecl3354);
        	    rightArg=parseop_Expression_level_22();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Ecl.g:4086:0: () a0= '-' rightArg= parseop_Expression_level_22
        	    {
        	    // Ecl.g:4086:2: ()
        	    // Ecl.g:4086:2: 
        	    {
        	    }


        	    a0=(Token)match(input,23,FOLLOW_23_in_synpred46_Ecl3372); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred46_Ecl3383);
        	    rightArg=parseop_Expression_level_22();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt63 >= 1 ) break loop63;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(63, input);
                    throw eee;
            }
            cnt63++;
        } while (true);


        }

    }
    // $ANTLR end synpred46_Ecl

    // $ANTLR start synpred47_Ecl
    public final void synpred47_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4162:2: ( () a0= '*' rightArg= parseop_Expression_level_80 )
        // Ecl.g:4162:2: () a0= '*' rightArg= parseop_Expression_level_80
        {
        // Ecl.g:4162:2: ()
        // Ecl.g:4162:2: 
        {
        }


        a0=(Token)match(input,19,FOLLOW_19_in_synpred47_Ecl3434); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred47_Ecl3445);
        rightArg=parseop_Expression_level_80();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred47_Ecl

    // $ANTLR start synpred48_Ecl
    public final void synpred48_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4229:2: ( () a0= '/' rightArg= parseop_Expression_level_80 )
        // Ecl.g:4229:2: () a0= '/' rightArg= parseop_Expression_level_80
        {
        // Ecl.g:4229:2: ()
        // Ecl.g:4229:2: 
        {
        }


        a0=(Token)match(input,27,FOLLOW_27_in_synpred48_Ecl3463); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred48_Ecl3474);
        rightArg=parseop_Expression_level_80();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred48_Ecl

    // $ANTLR start synpred49_Ecl
    public final void synpred49_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4161:39: ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ )
        // Ecl.g:4161:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
        {
        // Ecl.g:4161:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
        int cnt64=0;
        loop64:
        do {
            int alt64=3;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==19) ) {
                alt64=1;
            }
            else if ( (LA64_0==27) ) {
                alt64=2;
            }


            switch (alt64) {
        	case 1 :
        	    // Ecl.g:4162:0: () a0= '*' rightArg= parseop_Expression_level_80
        	    {
        	    // Ecl.g:4162:2: ()
        	    // Ecl.g:4162:2: 
        	    {
        	    }


        	    a0=(Token)match(input,19,FOLLOW_19_in_synpred49_Ecl3434); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred49_Ecl3445);
        	    rightArg=parseop_Expression_level_80();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Ecl.g:4229:0: () a0= '/' rightArg= parseop_Expression_level_80
        	    {
        	    // Ecl.g:4229:2: ()
        	    // Ecl.g:4229:2: 
        	    {
        	    }


        	    a0=(Token)match(input,27,FOLLOW_27_in_synpred49_Ecl3463); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred49_Ecl3474);
        	    rightArg=parseop_Expression_level_80();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt64 >= 1 ) break loop64;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(64, input);
                    throw eee;
            }
            cnt64++;
        } while (true);


        }

    }
    // $ANTLR end synpred49_Ecl

    // $ANTLR start synpred53_Ecl
    public final void synpred53_Ecl_fragment() throws RecognitionException {
        Token a0=null;

        // Ecl.g:4456:4: (a0= '++' )
        // Ecl.g:4456:4: a0= '++'
        {
        a0=(Token)match(input,21,FOLLOW_21_in_synpred53_Ecl3625); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred53_Ecl

    // $ANTLR start synpred54_Ecl
    public final void synpred54_Ecl_fragment() throws RecognitionException {
        Token a0=null;

        // Ecl.g:4523:4: (a0= '--' )
        // Ecl.g:4523:4: a0= '--'
        {
        a0=(Token)match(input,24,FOLLOW_24_in_synpred54_Ecl3640); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred54_Ecl

    // $ANTLR start synpred55_Ecl
    public final void synpred55_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4599:2: ( () a0= '+=' rightArg= parseop_Expression_level_91 )
        // Ecl.g:4599:2: () a0= '+=' rightArg= parseop_Expression_level_91
        {
        // Ecl.g:4599:2: ()
        // Ecl.g:4599:2: 
        {
        }


        a0=(Token)match(input,22,FOLLOW_22_in_synpred55_Ecl3690); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred55_Ecl3701);
        rightArg=parseop_Expression_level_91();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred55_Ecl

    // $ANTLR start synpred56_Ecl
    public final void synpred56_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4666:2: ( () a0= '-=' rightArg= parseop_Expression_level_91 )
        // Ecl.g:4666:2: () a0= '-=' rightArg= parseop_Expression_level_91
        {
        // Ecl.g:4666:2: ()
        // Ecl.g:4666:2: 
        {
        }


        a0=(Token)match(input,25,FOLLOW_25_in_synpred56_Ecl3719); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred56_Ecl3730);
        rightArg=parseop_Expression_level_91();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred56_Ecl

    // $ANTLR start synpred57_Ecl
    public final void synpred57_Ecl_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Ecl.g:4598:39: ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ )
        // Ecl.g:4598:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
        {
        // Ecl.g:4598:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
        int cnt65=0;
        loop65:
        do {
            int alt65=3;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==22) ) {
                alt65=1;
            }
            else if ( (LA65_0==25) ) {
                alt65=2;
            }


            switch (alt65) {
        	case 1 :
        	    // Ecl.g:4599:0: () a0= '+=' rightArg= parseop_Expression_level_91
        	    {
        	    // Ecl.g:4599:2: ()
        	    // Ecl.g:4599:2: 
        	    {
        	    }


        	    a0=(Token)match(input,22,FOLLOW_22_in_synpred57_Ecl3690); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred57_Ecl3701);
        	    rightArg=parseop_Expression_level_91();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Ecl.g:4666:0: () a0= '-=' rightArg= parseop_Expression_level_91
        	    {
        	    // Ecl.g:4666:2: ()
        	    // Ecl.g:4666:2: 
        	    {
        	    }


        	    a0=(Token)match(input,25,FOLLOW_25_in_synpred57_Ecl3719); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred57_Ecl3730);
        	    rightArg=parseop_Expression_level_91();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt65 >= 1 ) break loop65;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(65, input);
                    throw eee;
            }
            cnt65++;
        } while (true);


        }

    }
    // $ANTLR end synpred57_Ecl

    // Delegated rules

    public final boolean synpred55_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred55_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred27_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred27_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred43_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred43_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred28_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred28_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred53_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred53_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred44_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred44_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred33_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred33_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred32_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred32_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred48_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred48_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred46_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred46_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred49_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred49_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred35_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred35_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred45_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred45_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred29_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred29_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred34_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred34_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred23_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred23_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred47_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred47_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred54_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred54_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred38_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred38_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred56_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred56_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred26_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred26_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred57_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred57_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred25_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred25_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred40_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred40_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred24_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred24_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred41_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred41_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred42_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred42_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred39_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred39_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred37_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred37_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred36_Ecl() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred36_Ecl_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_EclFile_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Import_in_parse_org_coolsoftware_ecl_EclFile124 = new BitSet(new long[]{0x0001010000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_EclContract_in_parse_org_coolsoftware_ecl_EclFile159 = new BitSet(new long[]{0x0000010000000002L});
    public static final BitSet FOLLOW_48_in_parse_org_coolsoftware_ecl_CcmImport200 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_parse_org_coolsoftware_ecl_CcmImport214 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_QUOTED_91_93_in_parse_org_coolsoftware_ecl_CcmImport232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_parse_org_coolsoftware_ecl_SWComponentContract268 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract286 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_parse_org_coolsoftware_ecl_SWComponentContract307 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_59_in_parse_org_coolsoftware_ecl_SWComponentContract321 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract339 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_26_in_parse_org_coolsoftware_ecl_SWComponentContract360 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract378 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_62_in_parse_org_coolsoftware_ecl_SWComponentContract399 = new BitSet(new long[]{0x0014000000000000L});
    public static final BitSet FOLLOW_50_in_parse_org_coolsoftware_ecl_SWComponentContract422 = new BitSet(new long[]{0x0010000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_SWComponentContract463 = new BitSet(new long[]{0x0010000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWContractMode_in_parse_org_coolsoftware_ecl_SWComponentContract552 = new BitSet(new long[]{0x8010000000000000L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_ecl_SWComponentContract593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_parse_org_coolsoftware_ecl_HWComponentContract622 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract640 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_parse_org_coolsoftware_ecl_HWComponentContract661 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_parse_org_coolsoftware_ecl_HWComponentContract675 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract693 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_62_in_parse_org_coolsoftware_ecl_HWComponentContract714 = new BitSet(new long[]{0x0014000000000000L});
    public static final BitSet FOLLOW_50_in_parse_org_coolsoftware_ecl_HWComponentContract737 = new BitSet(new long[]{0x0010000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_HWComponentContract778 = new BitSet(new long[]{0x0010000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWContractMode_in_parse_org_coolsoftware_ecl_HWComponentContract867 = new BitSet(new long[]{0x8010000000000000L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_ecl_HWComponentContract908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_parse_org_coolsoftware_ecl_SWContractMode937 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWContractMode955 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_62_in_parse_org_coolsoftware_ecl_SWContractMode976 = new BitSet(new long[]{0x0180000000000000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWContractClause_in_parse_org_coolsoftware_ecl_SWContractMode1005 = new BitSet(new long[]{0x8180000000000000L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_ecl_SWContractMode1046 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_parse_org_coolsoftware_ecl_HWContractMode1075 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWContractMode1093 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_62_in_parse_org_coolsoftware_ecl_HWContractMode1114 = new BitSet(new long[]{0x0180000000000000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWContractClause_in_parse_org_coolsoftware_ecl_HWContractMode1143 = new BitSet(new long[]{0x8180000000000000L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_ecl_HWContractMode1184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_parse_org_coolsoftware_ecl_ProvisionClause1213 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_ProvisionClause1231 = new BitSet(new long[]{0x000A000040000002L});
    public static final BitSet FOLLOW_51_in_parse_org_coolsoftware_ecl_ProvisionClause1261 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1287 = new BitSet(new long[]{0x0002000040000002L});
    public static final BitSet FOLLOW_49_in_parse_org_coolsoftware_ecl_ProvisionClause1337 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1363 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_ProvisionClause1413 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1454 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1468 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1486 = new BitSet(new long[]{0x4000000020000000L});
    public static final BitSet FOLLOW_62_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1516 = new BitSet(new long[]{0x8000000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1557 = new BitSet(new long[]{0x8000000000000800L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1618 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1654 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1693 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_57_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1707 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1725 = new BitSet(new long[]{0x4000000000000000L});
    public static final BitSet FOLLOW_62_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1746 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1775 = new BitSet(new long[]{0x0000040000000800L});
    public static final BitSet FOLLOW_42_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1816 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1834 = new BitSet(new long[]{0x8000000000000000L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause1855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_parse_org_coolsoftware_ecl_SelfRequirementClause1884 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SelfRequirementClause1902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_PropertyRequirementClause1939 = new BitSet(new long[]{0x000A000040000002L});
    public static final BitSet FOLLOW_51_in_parse_org_coolsoftware_ecl_PropertyRequirementClause1969 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause1995 = new BitSet(new long[]{0x0002000040000002L});
    public static final BitSet FOLLOW_49_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2045 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2071 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_parse_org_coolsoftware_ecl_FormulaTemplate2162 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2180 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_org_coolsoftware_ecl_FormulaTemplate2201 = new BitSet(new long[]{0x0000000000040800L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2230 = new BitSet(new long[]{0x0000000000040800L});
    public static final BitSet FOLLOW_18_in_parse_org_coolsoftware_ecl_FormulaTemplate2276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_Metaparameter2309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_parse_org_coolsoftware_coolcomponents_expressions_Block2345 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2363 = new BitSet(new long[]{0x8000000020000000L});
    public static final BitSet FOLLOW_29_in_parse_org_coolsoftware_coolcomponents_expressions_Block2390 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2416 = new BitSet(new long[]{0x8000000020000000L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_coolcomponents_expressions_Block2457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2490 = new BitSet(new long[]{0x0000000110000002L});
    public static final BitSet FOLLOW_28_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2520 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_TYPE_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2546 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_32_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2601 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2673 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_032698 = new BitSet(new long[]{0x0040002000000002L});
    public static final BitSet FOLLOW_54_in_parseop_Expression_level_032718 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_032735 = new BitSet(new long[]{0x0040002000000002L});
    public static final BitSet FOLLOW_37_in_parseop_Expression_level_032769 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_032786 = new BitSet(new long[]{0x0040002000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_042832 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_47_in_parseop_Expression_level_042848 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_042862 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_43_in_parseop_Expression_level_52903 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_52914 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_parseop_Expression_level_52923 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_52934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_52944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_62966 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_32_in_parseop_Expression_level_62979 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_62990 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113028 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_34_in_parseop_Expression_level_113041 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113052 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_35_in_parseop_Expression_level_113070 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113081 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_30_in_parseop_Expression_level_113099 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113110 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_31_in_parseop_Expression_level_113128 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113139 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123177 = new BitSet(new long[]{0x0000000200008002L});
    public static final BitSet FOLLOW_33_in_parseop_Expression_level_123190 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123201 = new BitSet(new long[]{0x0000000200008002L});
    public static final BitSet FOLLOW_15_in_parseop_Expression_level_123219 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123230 = new BitSet(new long[]{0x0000000200008002L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193268 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_36_in_parseop_Expression_level_193281 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193292 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213330 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_20_in_parseop_Expression_level_213343 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213354 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_23_in_parseop_Expression_level_213372 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213383 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223421 = new BitSet(new long[]{0x0000000008080002L});
    public static final BitSet FOLLOW_19_in_parseop_Expression_level_223434 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223445 = new BitSet(new long[]{0x0000000008080002L});
    public static final BitSet FOLLOW_27_in_parseop_Expression_level_223463 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223474 = new BitSet(new long[]{0x0000000008080002L});
    public static final BitSet FOLLOW_58_in_parseop_Expression_level_803512 = new BitSet(new long[]{0x3020100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_parseop_Expression_level_803532 = new BitSet(new long[]{0x3020100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_parseop_Expression_level_873575 = new BitSet(new long[]{0x3000100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873586 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_90_in_parseop_Expression_level_883618 = new BitSet(new long[]{0x0000000001200002L});
    public static final BitSet FOLLOW_21_in_parseop_Expression_level_883625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_parseop_Expression_level_883640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903677 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_22_in_parseop_Expression_level_903690 = new BitSet(new long[]{0x3000100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903701 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_25_in_parseop_Expression_level_903719 = new BitSet(new long[]{0x3000100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903730 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_in_parseop_Expression_level_913768 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_in_parseop_Expression_level_913776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_in_parseop_Expression_level_913784 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_in_parseop_Expression_level_913792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_in_parseop_Expression_level_913800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_in_parseop_Expression_level_913808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_in_parseop_Expression_level_913816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ILT_in_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression3840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression3870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression3902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression3911 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression3943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression3971 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression3984 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression3996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4022 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression4064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_CcmImport_in_parse_org_coolsoftware_ecl_Import4088 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_SWContractClause4134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SelfRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_HWContractClause4177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_HWContractClause4185 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_03_in_parse_org_coolsoftware_coolcomponents_expressions_Expression4204 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_synpred23_Ecl2601 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred23_Ecl2627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_synpred24_Ecl2490 = new BitSet(new long[]{0x0000000110000002L});
    public static final BitSet FOLLOW_28_in_synpred24_Ecl2520 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_TYPE_LITERAL_in_synpred24_Ecl2546 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_32_in_synpred24_Ecl2601 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred24_Ecl2627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_synpred25_Ecl2718 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred25_Ecl2735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_synpred26_Ecl2769 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred26_Ecl2786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_synpred27_Ecl2718 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred27_Ecl2735 = new BitSet(new long[]{0x0040002000000002L});
    public static final BitSet FOLLOW_37_in_synpred27_Ecl2769 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred27_Ecl2786 = new BitSet(new long[]{0x0040002000000002L});
    public static final BitSet FOLLOW_47_in_synpred28_Ecl2848 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_synpred28_Ecl2862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_synpred29_Ecl2848 = new BitSet(new long[]{0x34201A0000014A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_synpred29_Ecl2862 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_32_in_synpred32_Ecl2979 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_synpred32_Ecl2990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_synpred33_Ecl2979 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_synpred33_Ecl2990 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_34_in_synpred34_Ecl3041 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred34_Ecl3052 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_synpred35_Ecl3070 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred35_Ecl3081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred36_Ecl3099 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred36_Ecl3110 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_synpred37_Ecl3128 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred37_Ecl3139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_synpred38_Ecl3041 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3052 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_35_in_synpred38_Ecl3070 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3081 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_30_in_synpred38_Ecl3099 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3110 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_31_in_synpred38_Ecl3128 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred38_Ecl3139 = new BitSet(new long[]{0x0000000CC0000002L});
    public static final BitSet FOLLOW_33_in_synpred39_Ecl3190 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred39_Ecl3201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_synpred40_Ecl3219 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred40_Ecl3230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_synpred41_Ecl3190 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred41_Ecl3201 = new BitSet(new long[]{0x0000000200008002L});
    public static final BitSet FOLLOW_15_in_synpred41_Ecl3219 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred41_Ecl3230 = new BitSet(new long[]{0x0000000200008002L});
    public static final BitSet FOLLOW_36_in_synpred42_Ecl3281 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_synpred42_Ecl3292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_synpred43_Ecl3281 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_synpred43_Ecl3292 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_20_in_synpred44_Ecl3343 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred44_Ecl3354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_synpred45_Ecl3372 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred45_Ecl3383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_synpred46_Ecl3343 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred46_Ecl3354 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_23_in_synpred46_Ecl3372 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred46_Ecl3383 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_19_in_synpred47_Ecl3434 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred47_Ecl3445 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_synpred48_Ecl3463 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred48_Ecl3474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_synpred49_Ecl3434 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred49_Ecl3445 = new BitSet(new long[]{0x0000000008080002L});
    public static final BitSet FOLLOW_27_in_synpred49_Ecl3463 = new BitSet(new long[]{0x3420120000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred49_Ecl3474 = new BitSet(new long[]{0x0000000008080002L});
    public static final BitSet FOLLOW_21_in_synpred53_Ecl3625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_synpred54_Ecl3640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_synpred55_Ecl3690 = new BitSet(new long[]{0x3000100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred55_Ecl3701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_synpred56_Ecl3719 = new BitSet(new long[]{0x3000100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred56_Ecl3730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_synpred57_Ecl3690 = new BitSet(new long[]{0x3000100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred57_Ecl3701 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_25_in_synpred57_Ecl3719 = new BitSet(new long[]{0x3000100000010A90L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred57_Ecl3730 = new BitSet(new long[]{0x0000000002400002L});

}