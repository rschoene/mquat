/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.mopp;

public class EclProblem implements org.coolsoftware.ecl.resource.ecl.IEclProblem {
	
	private String message;
	private org.coolsoftware.ecl.resource.ecl.EclEProblemType type;
	private org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity severity;
	private java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclQuickFix> quickFixes;
	
	public EclProblem(String message, org.coolsoftware.ecl.resource.ecl.EclEProblemType type, org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<org.coolsoftware.ecl.resource.ecl.IEclQuickFix>emptySet());
	}
	
	public EclProblem(String message, org.coolsoftware.ecl.resource.ecl.EclEProblemType type, org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity severity, org.coolsoftware.ecl.resource.ecl.IEclQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public EclProblem(String message, org.coolsoftware.ecl.resource.ecl.EclEProblemType type, org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity severity, java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<org.coolsoftware.ecl.resource.ecl.IEclQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public org.coolsoftware.ecl.resource.ecl.EclEProblemType getType() {
		return type;
	}
	
	public org.coolsoftware.ecl.resource.ecl.EclEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
