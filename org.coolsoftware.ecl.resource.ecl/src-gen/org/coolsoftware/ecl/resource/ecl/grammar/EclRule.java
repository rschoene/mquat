/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class EclRule extends org.coolsoftware.ecl.resource.ecl.grammar.EclSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public EclRule(org.eclipse.emf.ecore.EClass metaclass, org.coolsoftware.ecl.resource.ecl.grammar.EclChoice choice, org.coolsoftware.ecl.resource.ecl.grammar.EclCardinality cardinality) {
		super(cardinality, new org.coolsoftware.ecl.resource.ecl.grammar.EclSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public org.coolsoftware.ecl.resource.ecl.grammar.EclChoice getDefinition() {
		return (org.coolsoftware.ecl.resource.ecl.grammar.EclChoice) getChildren()[0];
	}
	
}

