/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.util;

/**
 * A utility class to resolve proxy objects that allows to terminate resolving.
 */
public class EclInterruptibleEcoreResolver {
	
	private boolean terminate = false;
	
	public void terminate() {
		terminate = true;
	}
	
	/**
	 * Visits all proxies in the resource set and tries to resolve them.
	 * 
	 * @param resourceSet the objects to visit.
	 */
	public void resolveAll(org.eclipse.emf.ecore.resource.ResourceSet resourceSet) {
		java.util.List<org.eclipse.emf.ecore.resource.Resource> resources = resourceSet.getResources();
		for (int i = 0; i < resources.size() && !terminate; i++) {
			resolveAll(resources.get(i));
		}
	}
	
	/**
	 * Visits all proxies in the resource and tries to resolve them.
	 * 
	 * @param resource the objects to visit.
	 */
	public void resolveAll(org.eclipse.emf.ecore.resource.Resource resource) {
		for (org.eclipse.emf.ecore.EObject eObject : resource.getContents()) {
			if (terminate) {
				return;
			}
			resolveAll(eObject);
		}
	}
	
	/**
	 * Visits all proxies referenced by the object and recursively any of its
	 * contained objects.
	 * 
	 * @param eObject the object to visit.
	 */
	public void resolveAll(org.eclipse.emf.ecore.EObject eObject) {
		eObject.eContainer();
		resolveCrossReferences(eObject);
		for (java.util.Iterator<org.eclipse.emf.ecore.EObject> i = eObject.eAllContents(); i.hasNext();) {
			if (terminate) {
				return;
			}
			org.eclipse.emf.ecore.EObject childEObject = i.next();
			resolveCrossReferences(childEObject);
		}
	}
	
	protected void resolveCrossReferences(org.eclipse.emf.ecore.EObject eObject) {
		for (java.util.Iterator<org.eclipse.emf.ecore.EObject> i = eObject.eCrossReferences().iterator(); i.hasNext(); i.next()) {
			// The loop resolves the cross references by visiting them.
			if (terminate) {
				return;
			}
		}
	}
	
}
