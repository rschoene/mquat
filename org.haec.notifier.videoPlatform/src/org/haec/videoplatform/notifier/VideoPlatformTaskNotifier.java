/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoplatform.notifier;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.ITaskPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.ITaskResultNotifier;
import org.coolsoftware.theatre.energymanager.TaskDescription;
import org.coolsoftware.theatre.energymanager.TaskResultDescription;
import org.haec.apps.util.VideoSettings;
import org.haec.theatre.rest.util.RestUtils;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.StringUtils;

/**
 * Notifier informing the VideoPlatform about new and finished tasks.
 * @author René Schöne
 */
public class VideoPlatformTaskNotifier implements ITaskPreExecuteNotifier, ITaskResultNotifier {

	private static final String KEY_TASK_ID = "taskId";
	private static final String KEY_ESTIMATED_TIME = "estimatedTime";
	private static final String KEY_HOSTS = "hosts";

	private static final String KEY_SUCCESS = "success";
	private static final String KEY_FILE = "file";
	private static final String KEY_SLAVE_NAME = "slaveName";

	private static final Logger log = Logger.getLogger(VideoPlatformTaskNotifier.class);

	private WebTarget rootNew;
	private WebTarget rootFinished;
	private VideoSettings settings;
	private Client client;
	
	public VideoPlatformTaskNotifier() {
		client = ClientBuilder.newClient();
		updatePaths();
	}
	
	private void updatePaths() {
		WebTarget root = client.target(UriBuilder.fromUri("http://" + settings.getVideoplatformHost().get())
				.port(settings.getVideoPlatformPort().get()).build());
		rootNew = root.path(settings.getVideoPlatformPathNew().get());
		rootFinished = root.path(settings.getVideoPlatformPathFinished().get());
	}

	@Override
	public void beforeTaskExecute(TaskDescription td) {
		log.debug("entry");
		if(!settings.getVideoplatformNotifierEnabled().get()) { return; }
		Double predicted = td.predictedResponseTime;
		Long taskId = td.taskId;
		log.debug(String.format("uri=%s, taskId=%d, estimated=%f",
				rootNew.getUri(), taskId, predicted));
		WebTarget rootWithHosts = rootNew;
		if(td.hosts != null) { for(String host : td.hosts) {
			String hostName = PlatformUtils.getHostNameOf(host);
			if(hostName != null) { rootWithHosts = rootWithHosts.queryParam(KEY_HOSTS, hostName); }
		}}
		Builder builder = logResource(rootWithHosts.
				queryParam(KEY_TASK_ID, StringUtils.l(taskId, -1)).
				queryParam(KEY_ESTIMATED_TIME, StringUtils.d(predicted, 0))).
				request(MediaType.TEXT_PLAIN);
		RestUtils.putResponse(builder, null, String.class, log);
	}
	
	@Override
	public void afterTaskFinish(TaskResultDescription trd) {
		log.debug("entry");
		if(!settings.getVideoplatformNotifierEnabled().get()) { return; }
		log.debug(trd);
		WebTarget wr = rootFinished;
		if(trd.success && trd.filePath != null) {
			wr = wr.queryParam(KEY_SUCCESS, "true");
			wr = wr.queryParam(KEY_SLAVE_NAME, trd.hostname).queryParam(KEY_FILE, trd.filePath);
		}
		else {
			// no success, check if rawObject is an Throwable
			wr = wr.queryParam(KEY_SUCCESS, "false");
			if(trd.rawObject instanceof Throwable) {
				String message = ((Throwable) trd.rawObject).getMessage();
				if(message != null) { wr = wr.queryParam("message", message); }
			}
		}
		Builder builder = logResource(wr.queryParam(KEY_TASK_ID, Long.toString(trd.taskId))).
				request(MediaType.TEXT_PLAIN);
		RestUtils.putResponse(builder, null, String.class, log);
	}

	private WebTarget logResource(WebTarget wr) {
		log.info(wr.getUri());
		return wr;
	}
	
	protected void setVideoSettings(VideoSettings vs) {
		this.settings = vs;
	}

	protected void unsetVideoSettings(VideoSettings vs) {
		this.settings = null;
	}

}
