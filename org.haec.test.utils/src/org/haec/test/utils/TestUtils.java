/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.test.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * Utility class used in tests.
 * @author René Schöne
 */
public class TestUtils {

	public static VariantModel loadVariantModel(ResourceSet rs, String serializedVariantModel, String fname) throws IOException {
		//create an inputstream of the serialized model
		ByteArrayInputStream bStruct = new ByteArrayInputStream(serializedVariantModel.getBytes());
		//load it using emf + emftext factory
		EObject result = null;
		URI varModelUri = URI.createPlatformResourceURI(fname, false);
		org.eclipse.emf.ecore.resource.Resource resource = rs.createResource(varModelUri);
		if (!resource.isLoaded())
			resource.load(bStruct, null);
		// no else.
	
		if (resource.getContents().size() > 0) {
			result = resource.getContents().get(0);
		}
		
		return (VariantModel) result;
	}
	
	/**
	 * Creates a new entry in the PlatformResourceMap of the {@link EcorePlugin}.
	 * @param name the name of the link
	 * @param target the target at which the link should point to
	 * @param basePath the base path to resolve the name to, may be <code>null</code> to use the current working dir path
	 * @see EcorePlugin#getPlatformResourceMap()
	 */
	public static void createEcoreLink(String name, String target, String basePath) {
		if(basePath == null) { basePath = new File(".").getAbsolutePath(); }
		URI uri = URI.createURI(target).resolve(URI.createFileURI(basePath));
		EcorePlugin.getPlatformResourceMap().put(name, uri);
	}

}
