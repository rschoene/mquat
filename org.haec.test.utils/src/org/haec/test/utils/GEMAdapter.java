/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.test.utils;

import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;

/**
 * Adapter for the GEM used in tests.
 * @author René Schöne
 */
public class GEMAdapter implements IGlobalEnergyManager {
	
	private IGlobalResourceManager grm;

	public GEMAdapter(IGlobalResourceManager grm) {
		this.grm = grm;
	}
	
	@Override
	public void waitForCurrentBenchmarksToFinish() { }

	@Override
	public void updateKnowledePlane(String implName, long frequency,
			double power) { }

	@Override
	public void updateContract(String appName, String eclURI,
			String newSerializedContract) { }

	@Override
	public boolean unregisterLem(URI container) { return false; }

	@Override
	public boolean unregister(String appName, URI container) { return false; }

	@Override
	public void registerVariant(String appName, String typeName,
			String implName, String benchName, String lemURI, String pluginName) {
	}

	@Override
	public boolean registerLEM(URI container) { return false; }

	@Override
	public boolean registerApp(String appName, URI containerURI,
			String structureModel, Map<String, String> contracts) {
		return false;
	}

	@Override
	public void reconfigure(String plan, String appName) { }

	@Override
	public Serializable optimize(String userRequest, boolean returnTaskId, Object... params) {
		return null;
	}

	@Override
	public String getUri() { return null; }

	@Override
	public Map<String, String> getRegisteredApplications() { return null; }

	@Override
	public List<String> getPortsMetaParameters(String appName, String compId,
			String portName) {
		return null;
	}

	@Override
	public List<String> getPortParameters(String appName, String compId,
			String portName) {
		return null;
	}

	@Override
	public List<URI> getLocalMgrs() { return null; }

	@Override
	public Object getGlobalUserManager() { return null; }

	@Override
	public Object getGlobalResourceManager() { return grm; }

	@Override
	public String getCurrentSystemConfiguration(String appUri) {
		return null;
	}

	@Override
	public Map<String, String> getContractsForComponent(String appName,
			String compName) { return null; }

	@Override
	public List<String> getComponentsOfApp(String appName) { return null; }

	@Override
	public List<String> getComponentProperties(String appName, String compId) {
		return null;
	}

	@Override
	public List<String> getComponentPorts(String appName, String compId) { return null; }

	@Override
	public InputStream getBundleBySymbolicName(String name) { return null; }

	@Override
	public String getApplicationModel(String appName) { return null; }

	@Override
	public Map<String, List<String>> getAllApplicationPropertyNames(
			String appName) { return null; }

	@Override
	public Serializable executeCurrentImpl(String appName, String compName,
			String portName, Map<String, Object> options, Object... params) {
		return null;
	}

	@Override
	public Serializable executeAtLem(URI lemUri, Map<String, Object> options,
			String portName, String appName, String compName, Object... params) {
		return null;
	}

	@Override
	public Map<String, VariantModel> getCurrentSystemConfigurations() { return null; }

	@Override
	public ExecuteResult getResultOfTask(long taskId, long t, TimeUnit unit) { return null; }

	@Override
	public Map<String, Map<String, String>> getContractsForApp(String appName, Map<String, List<String>> names) { return null; }

	@Override
	public Long[] getTaskIds() { return new Long[0]; }

	@Override
	public boolean reset(boolean full) { return true; }

	@Override
	public Serializable getResultOfJob(String appName, long jobId) { return null; }

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return "GEM-Adapter";
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.IGlobalEnergyManager#run(java.lang.String, boolean, boolean, java.lang.Object[])
	 */
	@Override
	public Serializable run(String userRequest, boolean returnTaskId,
			boolean optimize, Object... params) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.IGlobalEnergyManager#setReady(java.net.URI)
	 */
	@Override
	public void setReady(URI lemURI) {
	}
}