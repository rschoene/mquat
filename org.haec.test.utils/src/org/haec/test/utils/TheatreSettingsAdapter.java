/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.test.utils;

import org.haec.theatre.settings.TheatreSettingsImpl;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.MapCache;
import org.haec.theatre.utils.settings.Setting;

/**
 * Adapter to pass a settings object with only specific options to a test class.
 * @author René Schöne
 */
public class TheatreSettingsAdapter extends TheatreSettingsImpl {

	MapCache<Setting<String>, ChainSetting<String>> cacheString;
	MapCache<Setting<Integer>, ChainSetting<Integer>> cacheInteger;
	MapCache<Setting<Long>, ChainSetting<Long>> cacheLong;
	MapCache<Setting<Boolean>, ChainSetting<Boolean>> cacheBoolean;
	
	public TheatreSettingsAdapter() {
		class CreateChaingSetting<T> implements Function<Setting<T>, ChainSetting<T>> {
			@Override
			public ChainSetting<T> apply(Setting<T> s) {
				return new ChainSetting<T>(s, TheatreSettingsAdapter.this);
			}
			
		}
		cacheString = new MapCache<Setting<String>, ChainSetting<String>>(new CreateChaingSetting<String>());
		cacheInteger = new MapCache<Setting<Integer>, ChainSetting<Integer>>(new CreateChaingSetting<Integer>());
		cacheLong = new MapCache<Setting<Long>, ChainSetting<Long>>(new CreateChaingSetting<Long>());
		cacheBoolean = new MapCache<Setting<Boolean>, ChainSetting<Boolean>>(new CreateChaingSetting<Boolean>());
	}
	
	@Override
	public ChainSetting<Boolean> getNoLocalManagers() {
		return cacheBoolean.get(super.getNoLocalManagers());
	}

	@Override
	public ChainSetting<Boolean> getIpLan() {
		return cacheBoolean.get(super.getIpLan());
	}

	@Override
	public ChainSetting<Boolean> getIpCache() {
		return cacheBoolean.get(super.getIpCache());
	}

	@Override
	public ChainSetting<Boolean> getTaskRepositoryCreateGlobal() {
		return cacheBoolean.get(super.getTaskRepositoryCreateGlobal());
	}

	@Override
	public ChainSetting<String> getTaskRepositoryGlobalUris() {
		return cacheString.get(super.getTaskRepositoryGlobalUris());
	}

	@Override
	public ChainSetting<String> getRExe() {
		return cacheString.get(super.getRExe());
	}

	@Override
	public ChainSetting<String> getDatabaseUrl() {
		return cacheString.get(super.getDatabaseUrl());
	}

	@Override
	public ChainSetting<Integer> getDatabaseUpdaterate() {
		return cacheInteger.get(super.getDatabaseUpdaterate());
	}

	@Override
	public ChainSetting<Integer> getDatabaseCheckRunningRate() {
		return cacheInteger.get(super.getDatabaseCheckRunningRate());
	}

	@Override
	public ChainSetting<Boolean> getDatabaseUse() {
		return cacheBoolean.get(super.getDatabaseUse());
	}

	@Override
	public ChainSetting<Integer> getDatabaseStartupTries() {
		return cacheInteger.get(super.getDatabaseStartupTries());
	}

	@Override
	public ChainSetting<Long> getDatabaseStartupInterval() {
		return cacheLong.get(super.getDatabaseStartupInterval());
	}

	@Override
	public ChainSetting<Boolean> getDatabaseDummy() {
		return cacheBoolean.get(super.getDatabaseDummy());
	}

	@Override
	public ChainSetting<Boolean> getDatabaseLogSql() {
		return cacheBoolean.get(super.getDatabaseLogSql());
	}

	@Override
	public ChainSetting<Boolean> getGemAppsForceRetrieval() {
		return cacheBoolean.get(super.getGemAppsForceRetrieval());
	}

	@Override
	public ChainSetting<Boolean> getGemUpdateContractsUponNewLem() {
		return cacheBoolean.get(super.getGemUpdateContractsUponNewLem());
	}

	@Override
	public ChainSetting<Boolean> getGemLogLock() {
		return cacheBoolean.get(super.getGemLogLock());
	}

	@Override
	public ChainSetting<Boolean> getGemTestChangeMapping() {
		return cacheBoolean.get(super.getGemTestChangeMapping());
	}

	@Override
	public ChainSetting<Boolean> getGemValidResources() {
		return cacheBoolean.get(super.getGemValidResources());
	}

	@Override
	public ChainSetting<Boolean> getGemFakeMapping() {
		return cacheBoolean.get(super.getGemFakeMapping());
	}

	@Override
	public ChainSetting<Boolean> getGemFailAtNoLemConnected() {
		return cacheBoolean.get(super.getGemFailAtNoLemConnected());
	}

	@Override
	public ChainSetting<Long> getGemTimeToWaitForRegisterImpl() {
		return cacheLong.get(super.getGemTimeToWaitForRegisterImpl());
	}

	@Override
	public ChainSetting<Boolean> getGemSaveVisData() {
		return cacheBoolean.get(super.getGemSaveVisData());
	}

	@Override
	public ChainSetting<Boolean> getGemTryGracefulDegradation() {
		return cacheBoolean.get(super.getGemTryGracefulDegradation());
	}

	@Override
	public ChainSetting<Boolean> getGemVerboseConfig() {
		return cacheBoolean.get(super.getGemVerboseConfig());
	}

	@Override
	public ChainSetting<Boolean> getGrmLogSingleNotification() {
		return cacheBoolean.get(super.getGrmLogSingleNotification());
	}

	@Override
	public ChainSetting<Boolean> getGrmLogIpMismatch() {
		return cacheBoolean.get(super.getGrmLogIpMismatch());
	}

	@Override
	public ChainSetting<Boolean> getLemForceContracts() {
		return cacheBoolean.get(super.getLemForceContracts());
	}

	@Override
	public ChainSetting<Boolean> getLemContractsAtGem() {
		return cacheBoolean.get(super.getLemContractsAtGem());
	}

	@Override
	public ChainSetting<Long> getLemCheckForMasterPeriod() {
		return cacheLong.get(super.getLemCheckForMasterPeriod());
	}

	@Override
	public ChainSetting<Boolean> getLemSimpleTimeMeasure() {
		return cacheBoolean.get(super.getLemSimpleTimeMeasure());
	}

	@Override
	public ChainSetting<String> getLemTestPlugins() {
		return cacheString.get(super.getLemTestPlugins());
	}

	@Override
	public ChainSetting<Boolean> getLemFakeBenchmarks() {
		return cacheBoolean.get(super.getLemFakeBenchmarks());
	}

	@Override
	public ChainSetting<Long> getLrmUpdateRate() {
		return cacheLong.get(super.getLrmUpdateRate());
	}

	@Override
	public ChainSetting<Long> getLrmInitialDelay() {
		return cacheLong.get(super.getLrmInitialDelay());
	}

	@Override
	public ChainSetting<Boolean> getLrmCacheStatics() {
		return cacheBoolean.get(super.getLrmCacheStatics());
	}

	@Override
	public ChainSetting<String> getLrmBenchmarkDirectory() {
		return cacheString.get(super.getLrmBenchmarkDirectory());
	}

	@Override
	public ChainSetting<String> getRestClasses() {
		return cacheString.get(super.getRestClasses());
	}

	@Override
	public ChainSetting<Long> getPowermanagerMaxWait() {
		return cacheLong.get(super.getPowermanagerMaxWait());
	}

	@Override
	public ChainSetting<Long> getPowermanagerUpdateRate() {
		return cacheLong.get(super.getPowermanagerUpdateRate());
	}

	@Override
	public ChainSetting<Boolean> getPowermanagerVerbose() {
		return cacheBoolean.get(super.getPowermanagerVerbose());
	}

	@Override
	public ChainSetting<Integer> getMasterPort() {
		return cacheInteger.get(super.getMasterPort());
	}

	@Override
	public ChainSetting<String> getGemIp() {
		return cacheString.get(super.getGemIp());
	}

	@Override
	public ChainSetting<String> getGrmIp() {
		return cacheString.get(super.getGrmIp());
	}

	@Override
	public ChainSetting<String> getResourceDir() {
		return cacheString.get(super.getResourceDir());
	}

	@Override
	public ChainSetting<String> getGemUri() {
		return cacheString.get(super.getGemUri());
	}

	@Override
	public ChainSetting<String> getGrmUri() {
		return cacheString.get(super.getGrmUri());
	}

	@Override
	public ChainSetting<Boolean> getILPlogObjectiveSettings() {
		return cacheBoolean.get(super.getILPlogObjectiveSettings());
	}

	@Override
	public ChainSetting<Boolean> getILPlogConstraints() {
		return cacheBoolean.get(super.getILPlogConstraints());
	}

	@Override
	public ChainSetting<Boolean> getILPlogSingleIndicator() {
		return cacheBoolean.get(super.getILPlogSingleIndicator());
	}

	@Override
	public ChainSetting<Boolean> getILPlogModeServerMismatch() {
		return cacheBoolean.get(super.getILPlogModeServerMismatch());
	}

	@Override
	public ChainSetting<Boolean> getILPlogMissingEcl() {
		return cacheBoolean.get(super.getILPlogMissingEcl());
	}

	@Override
	public ChainSetting<Boolean> getILPlogUsageVar() {
		return cacheBoolean.get(super.getILPlogUsageVar());
	}

	@Override
	public ChainSetting<Boolean> getILPlogResourceProcessing() {
		return cacheBoolean.get(super.getILPlogResourceProcessing());
	}

	@Override
	public ChainSetting<Boolean> getILPlogProcessingResource() {
		return cacheBoolean.get(super.getILPlogProcessingResource());
	}

	@Override
	public ChainSetting<Boolean> getILPlogDuplicateElimination() {
		return cacheBoolean.get(super.getILPlogDuplicateElimination());
	}

	@Override
	public ChainSetting<Boolean> getILPobjectiveSimpleWeight() {
		return cacheBoolean.get(super.getILPobjectiveSimpleWeight());
	}

	@Override
	public ChainSetting<Boolean> getILPsolverExact() {
		return cacheBoolean.get(super.getILPsolverExact());
	}

	@Override
	public ChainSetting<Boolean> getILPobjectiveIncludeResponseTime() {
		return cacheBoolean.get(super.getILPobjectiveIncludeResponseTime());
	}

	@Override
	public ChainSetting<Boolean> getILPfeatureSequentialConstraint() {
		return cacheBoolean.get(super.getILPfeatureSequentialConstraint());
	}

	@Override
	public ChainSetting<Boolean> getILPfeaturePenalties() {
		return cacheBoolean.get(super.getILPfeaturePenalties());
	}

	@Override
	public ChainSetting<Boolean> getILPfeatureIgnoreUnknownProperties() {
		return cacheBoolean.get(super.getILPfeatureIgnoreUnknownProperties());
	}

	@Override
	public ChainSetting<Boolean> getILPfeatureIncludeTopLevelProperties() {
		return cacheBoolean.get(super.getILPfeatureIncludeTopLevelProperties());
	}

	@Override
	public ChainSetting<Boolean> getILPfeatureZeroIsUnknown() {
		return cacheBoolean.get(super.getILPfeatureZeroIsUnknown());
	}

	@Override
	public ChainSetting<Integer> getILPsetupInitTimeout() {
		return cacheInteger.get(super.getILPsetupInitTimeout());
	}

	@Override
	public ChainSetting<Boolean> getILPfeatureFuzzy() {
		return cacheBoolean.get(super.getILPfeatureFuzzy());
	}

	@Override
	public ChainSetting<Boolean> getILPfeatureOnlyUpdate() {
		return cacheBoolean.get(super.getILPfeatureOnlyUpdate());
	}

	@Override
	public ChainSetting<Integer> getILPresourceOfflineTimeout() {
		return cacheInteger.get(super.getILPresourceOfflineTimeout());
	}

	@Override
	public Setting<Boolean> isMaster() {
		return cacheBoolean.get(super.isMaster());
	}

}
