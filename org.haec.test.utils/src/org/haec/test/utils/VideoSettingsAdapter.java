/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.test.utils;

import org.haec.apps.util.VideoSettings;
import org.haec.theatre.utils.settings.Setting;

/**
 * Adapter to pass a settings object with only specific video options to a test class.
 * @author René Schöne
 */
public class VideoSettingsAdapter extends TheatreSettingsAdapter implements VideoSettings {

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoProviderName()
	 */
	@Override
	public Setting<String> getVideoProviderName() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoUtilBin(java.lang.String)
	 */
	@Override
	public Setting<String> getVideoUtilBin(String id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoutilTemporaryFiles(java.lang.String)
	 */
	@Override
	public Setting<Boolean> getVideoutilTemporaryFiles(String id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoutilPass(java.lang.String)
	 */
	@Override
	public Setting<Boolean> getVideoutilPass(String id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoutilMergeError(java.lang.String)
	 */
	@Override
	public Setting<Boolean> getVideoutilMergeError(String id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoUtilEncodeNoSound(java.lang.String)
	 */
	@Override
	public Setting<Boolean> getVideoUtilEncodeNoSound(String id) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getAppsDebug()
	 */
	@Override
	public Setting<Boolean> getAppsDebug() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoplatformHost()
	 */
	@Override
	public Setting<String> getVideoplatformHost() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPort()
	 */
	@Override
	public Setting<Integer> getVideoPlatformPort() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPathFinished()
	 */
	@Override
	public Setting<String> getVideoPlatformPathFinished() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPathNew()
	 */
	@Override
	public Setting<String> getVideoPlatformPathNew() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPathVideoByName()
	 */
	@Override
	public Setting<String> getVideoPlatformPathVideoByName() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoplatformNotifierEnabled()
	 */
	@Override
	public Setting<Boolean> getVideoplatformNotifierEnabled() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoplatformResolverEnabled()
	 */
	@Override
	public Setting<Boolean> getVideoplatformResolverEnabled() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getUiDebug()
	 */
	@Override
	public Setting<Boolean> getUiDebug() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCuiVideo1()
	 */
	@Override
	public Setting<String> getCuiVideo1() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCuiVideo2()
	 */
	@Override
	public Setting<String> getCuiVideo2() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCui2TaskConfigPath()
	 */
	@Override
	public Setting<String> getCui2TaskConfigPath() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCui2CallConfigPath()
	 */
	@Override
	public Setting<String> getCui2CallConfigPath() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runScalingFfmepg()
	 */
	@Override
	public Setting<Boolean> runScalingFfmepg() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runScalingMencoder()
	 */
	@Override
	public Setting<Boolean> runScalingMencoder() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runScalingHandbrake()
	 */
	@Override
	public Setting<Boolean> runScalingHandbrake() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runTranscoderFfmepg()
	 */
	@Override
	public Setting<Boolean> runTranscoderFfmepg() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runTranscoderMencoder()
	 */
	@Override
	public Setting<Boolean> runTranscoderMencoder() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runTranscoderHandbrake()
	 */
	@Override
	public Setting<Boolean> runTranscoderHandbrake() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runPipFfmepg()
	 */
	@Override
	public Setting<Boolean> runPipFfmepg() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#videoRegexName()
	 */
	@Override
	public Setting<String> videoRegexName() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#firstVideoName()
	 */
	@Override
	public Setting<String> firstVideoName() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#secondVideoName()
	 */
	@Override
	public Setting<String> secondVideoName() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#scalingWidth()
	 */
	@Override
	public Setting<Integer> scalingWidth() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#scalingHeight()
	 */
	@Override
	public Setting<Integer> scalingHeight() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#transcoderVCodec()
	 */
	@Override
	public Setting<String> transcoderVCodec() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#transcoderACodec()
	 */
	@Override
	public Setting<String> transcoderACodec() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#pipPos()
	 */
	@Override
	public Setting<Integer> pipPos() {
		return null;
	}

}
