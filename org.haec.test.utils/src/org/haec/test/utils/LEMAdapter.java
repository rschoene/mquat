/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.test.utils;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.osgi.framework.BundleException;

/**
 * Adapter for the LEM used in tests.
 * @author René Schöne
 */
public class LEMAdapter implements ILocalEnergyManager {

	@Override
	public boolean reset(boolean full) {
		return false;
	}

	@Override
	public IGlobalEnergyManager getGlobalMgr() {
		return null;
	}
	
	@Override
	public void setGlobalMgr(IGlobalEnergyManager gem) {
	}

	@Override
	public String getContainerId() {
		return null;
	}

	@Override
	public Map<String, String> getActiveComponents() {
		return null;
	}

	@Override
	public List<String> getDeployedComponentTypes() {
		return null;
	}

	@Override
	public List<String> getDeployedComponents(String componentType) {
		return null;
	}

	@Override
	public Map<String, Object> getComponentProperties(String component) {
		return null;
	}

	@Override
	public Object getComponentProperty(String component) {
		return null;
	}

	@Override
	public String getComponentModes(String component) {
		return null;
	}

	@Override
	public String getCurrentMode(String activeComponent) {
		return null;
	}

	@Override
	public boolean isComponentTypeDeployed(String componentType) {
		return false;
	}

	@Override
	public void deploy(String type, String variant) {
	}

	@Override
	public void undeploy(String type, String variant) {
	}

	@Override
	public void replace(String type, String srcComp, String trgtComp) {
	}

	@Override
	public void migrate(String type, String srcComp, String trgtContainer) {
	}

	@Override
	public void replace(String type, String oldComp, String newComp,
			String targetContainer) {
	}

	@Override
	public String computeContract(String appModel, String hwModel,
			String appName, String eclURI, String contract, String implName,
			String benchmark, String pluginName) {
		return null;
	}

	@Override
	public String getContractByURI(String uri) {
		return null;
	}

	@Override
	public Serializable execute(String pluginName, String implName,
			String portName, String appName, Boolean compIsConnector,
			Boolean compIsCopyType, Long taskId, Long jobId,
			List<String> portTypes, Object... params) {
		return null;
	}

	@Override
	public void updateContract(String appModelSer, String hwModelSer,
			String appName, String eclURI, String newSerializedContract) {
	}

	@Override
	public void removeResult(long jobId) {
	}

	@Override
	public Serializable getResult(long jobId) {
		return null;
	}

	@Override
	public boolean isFinished(long jobId) {
		return false;
	}

	@Override
	public long[] getJobIdsInQueue() {
		return null;
	}

	@Override
	public void retrieveAndStartBundle(String symbolicName) throws IOException,
			BundleException {
	}

	@Override
	public String getImplName(long jobId) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return "LEM-Adapter";
	}

}
