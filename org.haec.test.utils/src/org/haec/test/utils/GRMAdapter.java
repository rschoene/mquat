/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.test.utils;

import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;

/**
 * Adapter for the GRM used in tests.
 * @author René Schöne
 */
public class GRMAdapter implements IGlobalResourceManager {
	@Override
	public void sayHello() {
	}

	@Override
	public String getInfrastructure() {
		return null;
	}
	
	@Override
	public VariantModel getInfrastructureAsModel() {
		return null;
	}

	@Override
	public String getInfrastructureTypes() {
		return null;
	}

	@Override
	public void registerResource(String resource) {
	}

	@Override
	public void unregisterResource(String name) {
	}

	@Override
	public void shutdown() {
	}

	@Override
	public void updateWithModel(String serializeModel) {
	}

	@Override
	public boolean reset(boolean full) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return "GRM-Adapter";
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager#notifyPropertyChanged(double, java.lang.String, java.lang.String, java.lang.String, org.coolsoftware.coolcomponents.ccm.structure.ResourceType)
	 */
	@Override
	public boolean notifyPropertyChanged(double value, String ip, String rname,
			String pname, ResourceType type) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager#getType(java.lang.String)
	 */
	@Override
	public ResourceType getType(String name) {
		return null;
	}
}