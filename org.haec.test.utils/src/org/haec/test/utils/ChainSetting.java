package org.haec.test.utils;

import org.haec.theatre.utils.settings.Setting;

public class ChainSetting<T> implements Setting<T> {
	
	private Setting<T> delegatee;
	private T overriddenValue = null;
	private TheatreSettingsAdapter settings;
	
	public ChainSetting(Setting<T> delegatee, TheatreSettingsAdapter settings) {
		super();
		this.delegatee = delegatee;
		this.settings = settings;
	}

	@Override
	public T get() {
		return overriddenValue != null ? overriddenValue : delegatee.get();
	}

	@Override
	public boolean isDefault() {
		return overriddenValue != null ? false : delegatee.isDefault();
	}

	@Override
	public String getName() {
		return delegatee.getName();
	}
	
	public TheatreSettingsAdapter set(T newValue) {
		this.overriddenValue = newValue;
		return settings;
	}

}
