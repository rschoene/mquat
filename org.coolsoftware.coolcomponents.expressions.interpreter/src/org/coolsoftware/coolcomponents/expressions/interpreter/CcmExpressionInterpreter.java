/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.interpreter;

import java.util.ArrayList;
import java.util.List;

import org.coolsoftware.coolcomponents.expressions.Block;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment;
import org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression;
import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmComparable;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;

/**
 * Interpreter to interpret the result of an {@link Statement}.
 * 
 * @author Claas Wilke
 */
public class CcmExpressionInterpreter {

	/**
	 * Interprets an {@link Statement}.
	 * 
	 * @param exp
	 *            The {@link Statement} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	public CcmValue interpret(Statement exp) {
		
		//System.out.println("interpreting: "+exp);

		if(exp == null) throw new UnsupportedOperationException("Expression must not be null!");
		
		if (exp instanceof Block) {
			return this.interpretBlock((Block) exp);
		}

		if (exp instanceof LiteralExpression) {
			return this.interpretLiteralExp((LiteralExpression) exp);
		}

		else if (exp instanceof ParenthesisedExpression) {
			return this
					.interpretParenthesisedExp((ParenthesisedExpression) exp);
		}

		else if (exp instanceof OperationCallExpression) {
			return this
					.interpretOperationCallExp((OperationCallExpression) exp);
		}

		else if (exp instanceof VariableAssignment) {
			return this.interpretVariableAssignment((VariableAssignment) exp);
		}

		else if (exp instanceof VariableCallExpression) {
			return this.interpretVariableCallExp((VariableCallExpression) exp);
		}

		else if (exp instanceof VariableDeclaration) {
			return this.interpretVariableDeclaration((VariableDeclaration) exp);
		}
		// no else.

		throw new UnsupportedOperationException("Unknown kind of Expression: "
				+ exp.toString());
	}

	/**
	 * Interprets a {@link Block}.
	 * 
	 * @param exp
	 *            The {@link Block} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmValue interpretBlock(Block block) {
		CcmValue result = null;

		for (Expression exp : block.getExpressions())
			result = this.interpret(exp);
		// end for.

		return result;
	}

	/**
	 * Interprets a {@link BooleanLiteralExpression}.
	 * 
	 * @param exp
	 *            The {@link BooleanLiteralExpression} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmBoolean interpretBooleanLiteralExp(BooleanLiteralExpression exp) {
		CcmBoolean result = StdlibFactory.eINSTANCE.createCcmBoolean();
		result.setBooleanValue(exp.isValue());

		return result;
	}

	/**
	 * Interprets a {@link IntegerLiteralExpression}.
	 * 
	 * @param exp
	 *            The {@link IntegerLiteralExpression} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmInteger interpretIntegerLiteralExp(IntegerLiteralExpression exp) {
		CcmInteger result = StdlibFactory.eINSTANCE.createCcmInteger();
		result.setIntegerValue(exp.getValue());

		return result;
	}

	/**
	 * Interprets a {@link LiteralExpression}.
	 * 
	 * @param exp
	 *            The {@link LiteralExpression} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmValue interpretLiteralExp(LiteralExpression exp) {

		if (exp instanceof BooleanLiteralExpression) {
			return this
					.interpretBooleanLiteralExp((BooleanLiteralExpression) exp);
		}

		else if (exp instanceof IntegerLiteralExpression) {
			return this
					.interpretIntegerLiteralExp((IntegerLiteralExpression) exp);
		}

		else if (exp instanceof RealLiteralExpression) {
			return this.interpretRealLiteralExp((RealLiteralExpression) exp);
		}

		else if (exp instanceof StringLiteralExpression) {
			return this
					.interpretStringLiteralExp((StringLiteralExpression) exp);
		}

		throw new UnsupportedOperationException(
				"Unknown kind of LiteralExpression: " + exp.toString());
	}

	/**
	 * Interprets an {@link OperationCallExpression}.
	 * 
	 * @param exp
	 *            The {@link OperationCallExpression} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmValue interpretOperationCallExp(OperationCallExpression exp) {

		/* Compute source. */
		CcmValue source = this.interpret(exp.getSourceExp());

		/* Compute arguments. */
		List<CcmValue> arguments = new ArrayList<CcmValue>(exp
				.getArgumentExps().size());

		for (Expression argExp : exp.getArgumentExps()) {
			arguments.add(this.interpret(argExp));
		}
		// end for.

		String opName = exp.getOperationName();

		/* Operations on Booleans. */
		if (source instanceof CcmBoolean) {
			CcmBoolean booleanSource = (CcmBoolean) source;

			if (opName.equals("!") && arguments.size() == 0)
				return booleanSource.not();
			// no else.

			if (opName.equals("and") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmBoolean)
				return booleanSource.and((CcmBoolean) arguments.get(0));
			// no else.

			if (opName.equals("or") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmBoolean)
				return booleanSource.or((CcmBoolean) arguments.get(0));
			// no else.

			if (opName.equals("implies") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmBoolean)
				return booleanSource.implies((CcmBoolean) arguments.get(0));
			// no else.
		}
		// no else.

		/* Operations on Integers. */
		if (source instanceof CcmInteger) {
			CcmInteger intSource = (CcmInteger) source;

			if (opName.equals("-") && arguments.size() == 0)
				return intSource.negate();
			// no else.

			if (opName.equals("++") && arguments.size() == 0) {
				CcmInteger increment = StdlibFactory.eINSTANCE
						.createCcmInteger();
				increment.setIntegerValue(1);
				CcmInteger result = intSource.add(increment);

				/*
				 * Increment the variable as well (assumes that WFRs have been
				 * checked before).
				 */
				((VariableCallExpression) exp.getSourceExp())
						.getReferredVariable().setValue(result);

				return result;
			}
			// no else.

			if (opName.equals("--") && arguments.size() == 0) {
				CcmInteger decrement = StdlibFactory.eINSTANCE
						.createCcmInteger();
				decrement.setIntegerValue(1);
				CcmInteger result = intSource.subtract(decrement);

				/*
				 * Decrement the variable as well (assumes that WFRs have been
				 * checked before).
				 */
				((VariableCallExpression) exp.getSourceExp())
						.getReferredVariable().setValue(result);

				return result;
			}
			// no else.

			if (opName.equals("+") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmInteger)
				return intSource.add((CcmInteger) arguments.get(0));
			// no else.

			if (opName.equals("-") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmInteger)
				return intSource.subtract((CcmInteger) arguments.get(0));
			// no else.

			if (opName.equals("*") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmInteger)
				return intSource.multiply((CcmInteger) arguments.get(0));
			// no else.
			
			if (opName.equals("^") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmInteger)
				return intSource.power((CcmInteger) arguments.get(0));
			// no else.

			if (opName.equals("/") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmInteger)
				return intSource.divide((CcmInteger) arguments.get(0));
			// no else.

			if (opName.equals("+=") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmInteger) {
				CcmInteger result = intSource
						.add((CcmInteger) arguments.get(0));

				/*
				 * Set the variables value as well (assumes that WFRs have been
				 * checked before).
				 */
				((VariableCallExpression) exp.getSourceExp())
						.getReferredVariable().setValue(result);

				return result;
			}
			// no else.

			if (opName.equals("-=") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmInteger) {
				CcmInteger result = intSource.subtract((CcmInteger) arguments
						.get(0));

				/*
				 * Set the variables value as well (assumes that WFRs have been
				 * checked before).
				 */
				((VariableCallExpression) exp.getSourceExp())
						.getReferredVariable().setValue(result);

				return result;
			}
			// no else.
		}
		// no else.

		/* Operations on Reals. */
		if (source instanceof CcmReal) {
			CcmReal realSource = (CcmReal) source;

			if (opName.equals("-") && arguments.size() == 0)
				return realSource.negate();
			// no else.

			if (opName.equals("+") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmReal)
				return realSource.add((CcmReal) arguments.get(0));
			// no else.

			if (opName.equals("-") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmReal)
				return realSource.subtract((CcmReal) arguments.get(0));
			// no else.

			if (opName.equals("*") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmReal)
				return realSource.multiply((CcmReal) arguments.get(0));
			// no else.
			
			if (opName.equals("^") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmReal)
				return realSource.power((CcmReal) arguments.get(0));
			// no else.
			
			if (opName.equals("sin") && arguments.size() == 0)
				return realSource.sin();
			// no else.
			
			if (opName.equals("cos") && arguments.size() == 0) {
				//System.out.println("\t## cos("+realSource.getRealValue()+") = "+realSource.cos());
				return realSource.cos();
			}
			// no else.

			if (opName.equals("/") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmReal)
				return realSource.divide((CcmReal) arguments.get(0));
			// no else.

			if (opName.equals("+=") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmReal) {
				CcmReal result = realSource.add((CcmReal) arguments.get(0));

				/*
				 * Set the variables value as well (assumes that WFRs have been
				 * checked before).
				 */
				((VariableCallExpression) exp.getSourceExp())
						.getReferredVariable().setValue(result);

				return result;
			}
			// no else.

			if (opName.equals("-=") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmReal) {
				CcmReal result = realSource
						.subtract((CcmReal) arguments.get(0));

				/*
				 * Set the variables value as well (assumes that WFRs have been
				 * checked before).
				 */
				((VariableCallExpression) exp.getSourceExp())
						.getReferredVariable().setValue(result);

				return result;
			}
			// no else.
		}
		// no else.

		/* Operations on Strings. */
		if (source instanceof CcmString) {
			CcmString stringSource = (CcmString) source;

			if (opName.equals("+") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmString)
				return stringSource.concat((CcmString) arguments.get(0));
			// no else.

			if (opName.equals("+=") && arguments.size() == 1
					&& arguments.get(0) instanceof CcmString) {
				CcmString result = stringSource.concat((CcmString) arguments
						.get(0));

				/*
				 * Set the variables value as well (assumes that WFRs have been
				 * checked before).
				 */
				((VariableCallExpression) exp.getSourceExp())
						.getReferredVariable().setValue(result);

				return result;
			}
			// no else.
		}
		// no else.

		/* Operations on Comparables. */
		if (source instanceof CcmComparable) {
			CcmComparable comparableSource = (CcmComparable) source;

			if (opName.equals(">") && arguments.size() == 1)
				return comparableSource.greaterThan(arguments.get(0));
			// no else.

			if (opName.equals(">=") && arguments.size() == 1)
				return comparableSource.greaterThanEquals(arguments.get(0));
			// no else.

			if (opName.equals("<") && arguments.size() == 1)
				return comparableSource.lessThan(arguments.get(0));
			// no else.

			if (opName.equals("<=") && arguments.size() == 1)
				return comparableSource.lessThanEquals(arguments.get(0));
			// no else.

		}
		// no else.

		if (opName.equals("==") && arguments.size() == 1)
			return source.equals(arguments.get(0));
		// no else.

		if (opName.equals("!=") && arguments.size() == 1)
			return source.notEquals(arguments.get(0));
		// no else.

		throw new UnsupportedOperationException(
				"Unknown kind of LiteralExpression: " + exp.toString());
	}

	/**
	 * Interprets a {@link ParenthesisedExpression}.
	 * 
	 * @param exp
	 *            The {@link ParenthesisedExpression} to be interpreted
	 * @return The {@link CcmValue} result.
	 */
	protected CcmValue interpretParenthesisedExp(ParenthesisedExpression exp) {
		return this.interpret(exp.getSourceExp());
	}

	/**
	 * Interprets a {@link RealLiteralExpression}.
	 * 
	 * @param exp
	 *            The {@link RealLiteralExpression} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmReal interpretRealLiteralExp(RealLiteralExpression exp) {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		result.setRealValue(exp.getValue());

		return result;
	}

	/**
	 * Interprets a {@link StringLiteralExpression}.
	 * 
	 * @param exp
	 *            The {@link StringLiteralExpression} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmString interpretStringLiteralExp(StringLiteralExpression exp) {
		CcmString result = StdlibFactory.eINSTANCE.createCcmString();
		result.setStringValue(exp.getValue());

		return result;
	}

	/**
	 * Interprets a {@link VariableAssignment}.
	 * 
	 * @param exp
	 *            The {@link VariableAssignment} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmValue interpretVariableAssignment(VariableAssignment exp) {

		Variable var = ((VariableCallExpression) exp.getReferredVariable())
				.getReferredVariable();
		var.setValue(this.interpret(exp.getValueExpression()));

		return var.getValue();
	}

	/**
	 * Interprets a {@link VariableCallExpression}.
	 * 
	 * @param exp
	 *            The {@link VariableCallExpression} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmValue interpretVariableCallExp(VariableCallExpression exp) {
		if(exp.getReferredVariable().getValue() == null) {
			exp.getReferredVariable().setValue(interpret(exp.getReferredVariable().getInitialExpression()));
		}
		//System.out.println("\tvar = "+exp.getReferredVariable().getValue());
		return exp.getReferredVariable().getValue();
	}

	/**
	 * Interprets a {@link VariableDeclaration}.
	 * 
	 * @param exp
	 *            The {@link VariableDeclaration} to be interpreted.
	 * @return The {@link CcmValue} result.
	 */
	protected CcmValue interpretVariableDeclaration(VariableDeclaration exp) {

		Variable var = exp.getDeclaredVariable();

		/* Initialize the variable. */
		Expression initExp = var.getInitialExpression();
		if (initExp != null)
			var.setValue(this.interpret(initExp));
		else
			var.setValue(StdlibFactory.eINSTANCE.createCcmVoid());

		return var.getValue();
	}
}
