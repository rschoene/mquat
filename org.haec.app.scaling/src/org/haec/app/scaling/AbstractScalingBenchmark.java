/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.scaling;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.ScalingScale;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.utils.FileProxy;
import org.haec.videoprovider.IVideoProviderFactory;
import org.haec.videoprovider.VideoProvider;
import org.haec.videos.Video;

class ScalingData implements BenchmarkData {
	FileProxy in;
	int w = -2;
	int h = -2;
	boolean mult = true;
	int mvl;
	
	public ScalingData(FileProxy in, int mvl) {
		this.in = in;
		this.mvl = mvl;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mvl;
	}
}

class BenchmarkScalingScale extends TaskBasedImpl implements ScalingScale {
	
	private AbstractScaling delegatee;

	public BenchmarkScalingScale(AbstractScaling delegatee) {
		this.delegatee = delegatee;
	}

	/* (non-Javadoc)
	 * @see org.haec.app.videotranscodingserver.ScalingScale#scale(java.lang.Object, int, int, boolean)
	 */
	@Override
	public FileProxy scale(Object in, int w, int h, boolean mult) {
		File f = ((FileProxy) in).getFile();
		return wrap(delegatee.scale(w, h, mult, f));
	}
	public String getApplicationName() { return null; }
	public String getComponentType() { return null; }
	public String getVariantName() { return null; }
	public Benchmark getBenchmark() { return null; }
	public String getPluginName() { return null; }
}

/**
 * Abstract superclass of all Scaling benchmarks providing a generic benchmark.
 * @author René Schöne
 */
public class AbstractScalingBenchmark implements Benchmark {

	public int MAX_BENCHMARK_COUNT = Integer.getInteger(TranscodingServerUtil.MAX_BENCH_SCALING, 5);
	
	private BenchmarkScalingScale comp;
	
	protected IVideoProviderFactory fac;
	protected Logger log = Logger.getLogger(AbstractScalingBenchmark.class);
	
	public AbstractScalingBenchmark(AbstractScaling comp, IVideoProviderFactory fac) {
		this.comp = new BenchmarkScalingScale(comp);
		this.fac = fac;
	}
	
	/**
	 * Default impl using {@link VideoProvider#getOneVideoOfEachLength()}
	 * @return the list of videos to use
	 */
	protected List<Video> getList() {
		return fac.getCurrentVideoProvider().getOneVideoOfEachLength();
	}


	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>();
		log.info("Starting");
		int benchCountRemaining = MAX_BENCHMARK_COUNT > 0 ? MAX_BENCHMARK_COUNT : Integer.MAX_VALUE;
		List<Video> list = getList();
		log.debug("Going to benchmark " + list.size() + " videos, but at most " + benchCountRemaining);
		for(Video video : list) {
			if(benchCountRemaining-- <= 0) { break; }
			int mpValue = TranscodingServerUtil.getMetaparameterValue(video.length, video.resX, video.resY);
			result.add(new ScalingData(new FileProxy(video.file), mpValue));
		}
		log.info("Finished");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		ScalingData d = (ScalingData) data;
		log.debug("video.fileName=" + d.in.getFile().getName());
		return comp.scale(d.in, d.w, d.h, d.mult);
	}

}
