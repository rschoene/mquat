/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.scaling;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.ScalingScale;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.VideoResolution;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.apps.util.VideoUtil;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.FileUtils;
import org.haec.videoprovider.IVideoProviderFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * Abstract superclass of all Scaling implementations providing a generic scale method.
 * @author René Schöne
 */
public abstract class AbstractScaling extends TaskBasedImpl implements ScalingScale {

	protected final VideoUtil util;
	protected IVideoProviderFactory fac;
	protected static Logger log = Logger.getLogger(AbstractScaling.class);
	
	/**
	 * Creates a new abstract scaling component
	 * @param shortHand a short string to identify the scaler
	 * @param util instance of util object to create processes
	 */
	public AbstractScaling(VideoUtil util) {
		this.util = util;
	}
	
	@Override
	public FileProxy scale(Object fp1, int w, int h, boolean mult) {
		// this could actually be generated or handled by the framework
		FileProxy fp = invoke(FileProxy.class, "CopyScalingFile1", "getFile", fp1);
		File fileVideo = fp.getFileFor(thisUri);
		return wrap(scale(w, h, mult, fileVideo));
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return "org.haec.app.videoTranscodingServer";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "Scaling";
	}
	
	/** Provides template method for scaling. Should not be overridden by subclasses. */
	protected File scale(int w, int h, boolean mult, File fileVideo) {
		ErrorState state = util.createErrorState();
		if(util.shouldPass()) {
			log.debug("Pass. Returning " + fileVideo);
			return fileVideo;
		}
		File out;
		try {
			out = util.getTemporaryFile(getDefaultExtension(fileVideo)); //XXX change extension, i.e. container, if necessary
		} catch (IOException e) {
			log.warn("Could not create temporary output. Exiting.", e);
			return null;
		}
		boolean doScale = true;
		while(doScale) {
			List<String> params = new ArrayList<String>();
			addArguments(params, fileVideo, w, h, mult, out, state);
			try {
				int exitValue = handleProcess(util.createProcess(params), util);
				doScale = handleExitValue(exitValue, state);
			} catch (IOException e) {
				log.warn("Could not create process. Exiting.", e);
				doScale = false;
			} catch (InterruptedException e) {
				log.warn("Process was interrupted. Exiting.", e);
				doScale = false;
			}
		}
		// return result of temporary output
		log.debug("Return value: " + out);
		return out;
	}

	/**
	 * @param fileVideo the input video
	 * @return the default extension to use for the output video
	 */
	protected String getDefaultExtension(File fileVideo) {
		String extension = FileUtils.splitExt(fileVideo).extension;
		switch(extension.trim().toLowerCase()) {
		case "avi":
		case "":
			return "mp4";
		default: return extension;
		}
	}

	/**
	 * Searches for a service implementing {@link VideoResolutionCalculator}.
	 * Subclasses may override it to use a special calculator.
	 */
	protected VideoResolutionCalculator getResolutionCalculator() {
		BundleContext context = Activator.getContext();
		if(context == null) return null;
		Collection<ServiceReference<VideoResolutionCalculator>> srefs;
		try {
			srefs = context.getServiceReferences(
					VideoResolutionCalculator.class, null);
		} catch (InvalidSyntaxException ignore) {
			// can not happen since filter is null
			return null;
		}
		for(ServiceReference<VideoResolutionCalculator> sref : srefs) {
			if(sref != null) {
				VideoResolutionCalculator calc = context.getService(sref);
				if(calc.isAvailable()) {
					return calc;
				}
			}
		}
		// no available calculator has been found
		return null;
	}
	
	/**
	 * Calculates the target resolution based on the parameters.
	 * @param source the source resolution
	 * @param w the parameter for the width (either absolute or a factor)
	 * @param h the parameter for the height (either absolute or a factor)
	 * @param mult whether the parameters are factors
	 * @return the target resolution
	 */
	protected VideoResolution convert(VideoResolution source, int w, int h, boolean mult) {
		if(!mult) {
			return new VideoResolution(w, h);
		}
		int width;
		int height;
		if(w < 0) { width = source.width / (-w); }
		else { width = source.width * w; }
		if(h < 0) { height = source.height / (-h); }
		else { height = source.height * h; }
		return new VideoResolution(width, height);
	}
	
	/**
	 * Analyse the exitvalue of the process and decide whether to start another
	 * process (with other parameters).
	 * Default implementation returns always <code>false</code>. Subclass may override it
	 * for different behaviour.
	 * @param exitValue the exit value of the formerly started process
	 * @param state state for handling multiple attempts
	 * @return <code>true</code> to start another process, <code>false</code> to return the result
	 */
	protected boolean handleExitValue(int exitValue, ErrorState state) {
		if(exitValue != 0) {
			log.debug("Got exitValue of " + exitValue);
			//PANIC!
			if(Boolean.getBoolean(TranscodingServerUtil.FAIL_FAST)) {
				throw new IllegalStateException(getClass().getName() + " finished with non-zero exit-value " + exitValue);
			}
		}
		return false;
	}

	/**
	 * Given a started process, handles the processing, i.e. read output or
	 * wait for the process to terminate.
	 * @param startedProcess the started process
	 * @param util the created util class, e.g. to execute waiting
	 * @return the exitvalue of the process
	 * @throws InterruptedException if an Error occurs while waiting
	 */
	protected abstract int handleProcess(Process startedProcess, VideoUtil util) throws InterruptedException;

	/**
	 * Fills the list of parameters
	 * @param params a list of parameters, initially empty but never <code>null</code>
	 * @param fileVideo the video file to process
	 * @param w the width to scale to
	 * @param h the height to scale to
	 * @param mult whether height and width are factors
	 * @param out the output file
	 * @param state state for handling multiple attempts
	 */
	protected abstract void addArguments(List<String> params, File fileVideo, int w, int h,
			boolean mult, File out, ErrorState state);
	
	protected void setVideoProviderFactory(IVideoProviderFactory fac) {
		this.fac = fac;
	}

}
