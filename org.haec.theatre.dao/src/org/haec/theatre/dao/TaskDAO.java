/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

import java.util.List;

/**
 * Database access object concerning tasks created by THEATRE.
 * <br><br><b>Implementation remarks:</b>
 * <ul>
 * <li>IDs of tasks are expected to be unique, even across restarts of THEATRE. This can be achieved easily, if
 * the dummy entry uses {@link System#currentTimeMillis()} as id.</li>
 * </ul>
 * @author René Schöne
 */
public interface TaskDAO extends AbstractDAO {
	
	public static final long OPERATION_FAILED = -1;

	/**
	 * Query for a list of tasks.
	 * @param taskIdRequest if not <code>null</code>, search only for jobs of a task with this id
	 * @return the list of tasks, or an empty list if nothing was found
	 */
	public List<TaskInfo> getTasks(Long taskIdRequest);
	
	/**
	 * Inserts a dummy entry such that all following entries will be unique.
	 */
	public void insertDummyTaskEntry();

	/**
	 * Create a new task.
	 * @return the taskId of the new task
	 */
	public long createNewTask();
	
	/**
	 * Delete all information on tasks.
	 */
	public void clearTasks();
	
	
	/**
	 * Updates a task using parameters with non-<code>null</code> values.
	 * @param taskId the identifier of the task to update
	 * @param status the new status, or <code>null</code>
	 * @return <code>true</code> of success, <code>false</code> otherwise
	 */
	public boolean updateTask(long taskId, String status);

}
