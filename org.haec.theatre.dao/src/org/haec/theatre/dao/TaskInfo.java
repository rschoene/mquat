/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Task information stored in the database.
 * @author René Schöne
 */
@XmlRootElement
public class TaskInfo {

	@XmlElement(name="taskId")
	long taskId;

	@XmlElement(name="status", defaultValue = "unknown")
	String status;

	public TaskInfo() {	} // needed by JAXB
	public TaskInfo(long taskId, String status) {
		this.taskId = taskId;
		this.status = status;
	}

	/** Dummy to implement sequence. */
	public static final int STATUS_DUMMY_CODE = 20;
	/** Task created, but not started. */
	public static final int STATUS_CREATED_CODE = 21;
	/** Task started, but not finished. */
	public static final int STATUS_STARTED_CODE = 22;
	/** Task finished successfully. */
	public static final int STATUS_FINISHED_CODE = 23;
	/** Task partly failed. */
	public static final int STATUS_PARTLY_FAILED_CODE = 26;
	/** Task failed (finished without success) */
	public static final int STATUS_FAILED_CODE = 27;

	/** Dummy to implement sequence. */
	public static final String STATUS_DUMMY = "#dummy";
	/** Task created, but not started. */
	public static final String STATUS_CREATED = "created";
	/** Task started, but not finished. */
	public static final String STATUS_STARTED = "started";
	/** Task finished successfully. */
	public static final String STATUS_FINISHED = "finished";
	/** Task partly failed. */
	public static final String STATUS_PARTLY_FAILED = "partlyFailed";
	/** Task failed (finished without success) */
	public static final String STATUS_FAILED = "failed";

	
	public static String statusCodeToName(int statusCode) {
		switch(statusCode) {
		case STATUS_DUMMY_CODE: return STATUS_DUMMY;
		case STATUS_CREATED_CODE: return STATUS_CREATED;
		case STATUS_STARTED_CODE: return STATUS_STARTED;
		case STATUS_FINISHED_CODE: return STATUS_FINISHED;
		case STATUS_PARTLY_FAILED_CODE: return STATUS_PARTLY_FAILED;
		case STATUS_FAILED_CODE: return STATUS_FAILED;
		default:
			return "unknown code: " + statusCode;
		}
	}
	
	public static int nameToStatusCode(String name) {
		switch(name) {
		case STATUS_DUMMY: return STATUS_DUMMY_CODE;
		case STATUS_CREATED: return STATUS_CREATED_CODE;
		case STATUS_STARTED: return STATUS_STARTED_CODE;
		case STATUS_FINISHED: return STATUS_FINISHED_CODE;
		case STATUS_PARTLY_FAILED: return STATUS_PARTLY_FAILED_CODE;
		case STATUS_FAILED: return STATUS_FAILED_CODE;
		default:
			return -1;
		}
	}

}
