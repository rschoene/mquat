/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

import java.util.List;

/**
 * Special database access object for databases offering the concept of synopses.
 * @author René Schöne
 */
public interface SynopseDAO extends AbstractDAO {
	
	public static final int OPERATION_FAILED = -1;
	
	/**
	 * Create a new synopes for monitoring
	 * @param synopseTableName the name of new synopse, <code>null</code> to use random name
	 * @param sensorClassName the class of the sensor, e.g. "load1"
	 * @param hostIp the ip of the host to monitor
	 * @param windowValue the time frame in seconds to monitor
	 * @param frequencyValue the update rate in seconds
	 * @return an id to identify the created synopse, or {@link #OPERATION_FAILED}
	 */
	public long createSynopse(String synopseTableName, String sensorClassName,
			String hostIp, int windowValue, int frequencyValue);

	/**
	 * Delete a synopse.
	 * @param synopseId the identifier of the synopse
	 */
	public void deleteSynopse(long synopseId);
	
	/**
	 * @param synopseId the identifier of the synopse
	 * @return the measurement results for the synopse
	 */
	public List<Double> getResult(long synopseId);

}
