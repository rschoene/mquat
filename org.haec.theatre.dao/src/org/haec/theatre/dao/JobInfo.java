/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Job information stored in the database.
 * @author René Schöne
 */
@XmlRootElement
public class JobInfo {
	@XmlElement(name="taskId")
	public long taskId;
	
	@XmlElement(name="jobId")
	public long jobId;
	
	@XmlElement(name="startTime")
	public long startTime;
	
	@XmlElement(name="pred_duration")
	public double pred_duration;
	
	@XmlElement(name="endTime")
	public long endTime;
	
	@XmlElement(name="status", defaultValue = "unknown")
	public String status;
	
	@XmlElement(name="result", defaultValue = "unknown")
	public String result;
	
	@XmlElement(name="host")
	public String host;
	
	@XmlElement(name="implName", defaultValue = "unknown")
	public String implName;
	
	public JobInfo() { } // needed by JAXB
	public JobInfo(long taskId, long jobId, long startTime, double pred_duration,
			long endTime, String status, String implName, String host) {
		this.taskId = taskId;
		this.jobId = jobId;
		this.startTime = startTime;
		this.pred_duration = pred_duration;
		this.endTime = endTime;
		this.status = status;
		this.implName = implName;
		this.host = host;
	}
	
	/** Dummy to implement sequence. */
	public static final int STATUS_DUMMY_CODE = 10;
	/** Job created, but not started. */
	public static final int STATUS_CREATED_CODE = 11;
	/** Job started, but not finished. */
	public static final int STATUS_STARTED_CODE = 12;
	/** Job finished successfully. */
	public static final int STATUS_FINISHED_CODE = 13;
	/** Job failed (finished without success) */
	public static final int STATUS_FAILED_CODE = 17;

	/** Dummy to implement sequence. */
	public static final String STATUS_DUMMY = "#dummy";
	/** Job created, but not started. */
	public static final String STATUS_CREATED = "created";
	/** Job started, but not finished. */
	public static final String STATUS_STARTED = "started";
	/** Job finished successfully. */
	public static final String STATUS_FINISHED = "finished";
	/** Job failed (finished without success) */
	public static final String STATUS_FAILED = "failed";
	
	public static String statusCodeToName(int statusCode) {
		switch(statusCode) {
		case STATUS_CREATED_CODE: return STATUS_CREATED;
		case STATUS_STARTED_CODE: return STATUS_STARTED;
		case STATUS_FINISHED_CODE: return STATUS_FINISHED;
		case STATUS_FAILED_CODE: return STATUS_FAILED;
		default:
			return "unknown code: " + statusCode;
		}
	}
	
	public static int nameToStatusCode(String name) {
		switch(name) {
		case STATUS_CREATED: return STATUS_CREATED_CODE;
		case STATUS_STARTED: return STATUS_STARTED_CODE;
		case STATUS_FINISHED: return STATUS_FINISHED_CODE;
		case STATUS_FAILED: return STATUS_FAILED_CODE;
		default:
			return -1;
		}
	}

}