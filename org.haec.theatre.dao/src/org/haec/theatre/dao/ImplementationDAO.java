/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

/**
 * Database access object concerning information on implementation managed by THEATRE.
 * @author René Schöne
 */
public interface ImplementationDAO extends AbstractDAO {
	
	public static final long OPERATION_FAILED = -1;

	/**
	 * Update the information of an implementation
	 * @param implName the name of the implementation
	 * @param frequency <i>not used</i>
	 * @param power <i>not used</i>
	 */
	public void updateImplementation(String implName, long frequency, double power);
	
	/**
	 * Delete all information on implementations.
	 */
	public void clearImplementations();
	
	/**
	 * @param implName the name of the implementation
	 * @return the identifier of the implementation, or {@link #OPERATION_FAILED} if failed
	 */
	public long getOrCreateImplementation(String implName);

}
