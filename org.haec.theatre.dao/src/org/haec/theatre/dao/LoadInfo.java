/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

/**
 * Information on load of a server.
 * @author René Schöne
 */
public class LoadInfo extends AbstractResourceInfo {

	/** Load in the past minute */
	public float load1;
	/** Load in the past five minutes */
	public float load5;
	/** Load in the past fifteen minutes */
	public float load15;
	
}
