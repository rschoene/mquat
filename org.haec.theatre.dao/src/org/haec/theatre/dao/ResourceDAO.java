/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

import java.util.List;
import java.util.Map;

/**
 * Stateful database access object to query latest changes on resources such as CPU, RAM and the load of servers.
 * @author René Schöne
 */
public interface ResourceDAO extends AbstractDAO {
	
	public final static int RESOURCE_UPDATE_FAILED = -1;
	public static final String LOCALHOST_PREFIX = "127";
	
	/**
	 * Updates information on cpu cores
	 * @param info { Host -> CpuCoreInfo }
	 * @return the number of changes
	 */
	public int updateCpuCores(Map<String, List<CpuCoreInfo>> info);

	/**
	 * Updates information on RAM
	 * @param info { Host -> RamInfo }
	 * @return the number of changes
	 */
	public int updateRAM(Map<String, RamInfo> info);

	/**
	 * Updates information on load
	 * @param info { Host -> LoadInfo }
	 * @return the number of changes
	 */
	public int updateLoad(Map<String, LoadInfo> info);

}
