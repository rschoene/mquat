/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao;

import java.util.List;


/**
 * Database access object concerning jobs created by THEATRE.
 * <br><br><b>Implementation remarks:</b>
 * <ul>
 * <li>IDs of jobs are expected to be unique, even across restarts of THEATRE. This can be achieved easily, if
 * the dummy entry uses {@link System#currentTimeMillis()} as id.</li>
 * </ul>
 * @author René Schöne
 */
public interface JobDAO extends AbstractDAO {
	
	public static final long OPERATION_FAILED = -1;
	
	/**
	 * Query for a list of jobs.
	 * @param appName the name of the application
	 * @param jobIdRequest if not <code>null</code>, search only for jobs with this id
	 * @param taskIdRequest if not <code>null</code>, search only for jobs of a task with this id
	 * @return the list of jobs, or an empty list if nothing was found
	 */
	public List<JobInfo> getJobs(String appName, Long jobIdRequest, Long taskIdRequest);
	
	/**
	 * Inserts a dummy entry such that all following entries will be unique.
	 */
	public void insertDummyJobEntry();

	/**
	 * Create a new job.
	 * @return the jobId of the new job, or {@link #OPERATION_FAILED} if failed
	 */
	public long createNewJob();
	
	/**
	 * Delete all information on jobs.
	 */
	public void clearJobs();
	
	/**
	 * Updates a job using parameters with non-<code>null</code> values.
	 * @param jobId the identifier of the job to update
	 * @param taskId the new task identifier, or <code>null</code>
	 * @param implId the new impl identifier, or <code>null</code>
	 * @param hostIp the new ip of the host, or <code>null</code>
	 * @param start the new start timepoint, or <code>null</code>
	 * @param predDuration the new predicted duration, or <code>null</code>
	 * @param end the new end timepoint, or <code>null</code>
	 * @param status the new status name, or <code>null</code>
	 * @param result the new result , or <code>null</code>
	 * @return <code>true</code> on succesful update, <code>false</code> otherwise
	 */
	public boolean updateJob(long jobId, Long taskId, Long implId, String hostIp,
			Long start, Double predDuration, Long end, String status, String result);

}
