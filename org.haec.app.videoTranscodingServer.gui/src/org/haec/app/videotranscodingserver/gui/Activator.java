/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.gui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;

/**
 * The activator class controls the plug-in life cycle. Content copied from GEM.GUI.Activator.
 * @author René Schöne
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.haec.app.videoTranscodingServer.gui"; //$NON-NLS-1$

	private static BundleContext context;
	private Shell shell;

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		Thread gui = new Thread(new Runnable() {

			@Override
			public void run() {
				final Display display = Display.getDefault();
				try {
					display.syncExec(new Runnable() {

						public void run() {
							shell = Starter.runStarter(display, context);
							shell.addDisposeListener(new DisposeListener() {

								@Override
								public void widgetDisposed(DisposeEvent e) {
									try {
										FrameworkUtil.getBundle(Activator.class).stop();
									} catch (BundleException e1) {
										e1.printStackTrace();
									}
								}
							});
						} 
					});
					while (!shell.isDisposed()) {
						if (!display.readAndDispatch())
							display.sleep();
					}
					if(!display.isDisposed()){
						display.dispose();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		gui.start();
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				if (shell != null && !shell.isDisposed()){
					shell.close();
					shell.dispose();
				}
			}
		});
		Activator.context = null;
	}

}