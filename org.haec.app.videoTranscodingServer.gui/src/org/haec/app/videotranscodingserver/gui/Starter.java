/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.gui;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.coolsoftware.theatre.energymanager.util.ErrorResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.app.videotranscodingserver.ui.RequestStarter;
import org.haec.apps.util.VideoDurationCalculator;
import org.haec.apps.util.VideoResolution;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.videoprovider.IVideoProviderFactory;
import org.haec.videos.Video;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;
import ch.ethz.iks.r_osgi.URI;

/**
 * Simple GUI using SWT for starting requests.
 * @author René Schöne
 */
public class Starter extends RequestStarter {
	
	private static final String[] optimizerIds = new String[]{"ilp","pbo","aco"};
	private static final String[] optimizerNames = new String[] {
		"ILP (Integer Linear Programming)",
		"PBO (Pseudo Boolean Optimization)",
		"ACO (Ant Colony Optimization)"};
	
	private static final String maxVideoLength = "max_video_length";
	private final MetaparameterLine theMetaparameterLine = new MetaparameterLine(maxVideoLength);
	
	private static final String parameterVideo1 = "fileVideo1";
	private static final String parameterVideo2 = "fileVideo2";
	private final File video1;
	private final File video2;
	
	private static Starter ui;
	
	protected String title = "VideoTranscodingServer - Demo";
	
	private Shell s_1;
	private Text txtAppName;
	private Text txtComponentName;
	private Text txtResult;
	private Combo comboPorts;
	private TableViewer viewerParameters;
	private TableViewer viewerNfr;
	private TableViewer viewerMetaparameters;
	
	private final List<ParameterLine> params;
	private final List<NfrLine> nfrs;
	private final List<MetaparameterLine> metaparams;
	private boolean autoUpdateMetaparams;
	private int optimizerIndex;
	private File returnedFile;
	private Logger log = Logger.getLogger(Starter.class);

	public Starter(BundleContext context) {
		super(context);
		params = new ArrayList<ParameterLine>();
		nfrs = new ArrayList<NfrLine>();
		metaparams = new ArrayList<MetaparameterLine>();
		metaparams.add(theMetaparameterLine); //XXX rs: just for testing (only one metaparameter)
		video1 = getExampleVideo(4);
		video2 = getExampleVideo(3);
	}

	private File getExampleVideo(int length) {
		IVideoProviderFactory fac = context.getService(
				context.getServiceReference(IVideoProviderFactory.class));
		List<Video> list = fac.getCurrentVideoProvider().getVideos(length);
		if(list == null || list.size() == 0) {
			// no video found, maybe EVP hasn't found one
			return new File(".");
		}
		else {
			return list.get(0).file;
		}
	}

	/* Copied from gem.gui.Dashboard */
	/**
	 * @wbp.parser.entryPoint
	 */
	public static Shell runStarter(Display display, BundleContext context) {
		ui = new Starter(context);
		Shell shell = ui.createShell(display);
	
		return shell;
	}

	/* Copied from gem.gui.Dashboard */
	private Shell createShell(Display display) {
        s_1 = new Shell(display);
        s_1.setSize(837,567);
        s_1.setText(title);
        s_1.setImage(new Image(display, Starter.class.getResourceAsStream("sample.gif")));
        s_1.setLayout(new GridLayout(2, false));
        createComposite(s_1);
        
        s_1.open();
        s_1.setFocus();
		return s_1;
	}

	private void createComposite(Shell s) {
        Label lblApp = new Label(s, SWT.NONE);
        lblApp.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblApp.setText("App:");
        
        txtAppName = new Text(s, SWT.BORDER);
        txtAppName.setEditable(false);
        txtAppName.setText("org.haec.app.videoTranscodingServer");
        txtAppName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        Label lblComponent = new Label(s, SWT.NONE);
        lblComponent.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblComponent.setText("Component:");
        
        Composite compositeController = new Composite(s_1, SWT.NONE);
        compositeController.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        GridLayout gl_compositeController = new GridLayout(2, false);
        gl_compositeController.marginWidth = 0;
        gl_compositeController.marginHeight = 0;
        compositeController.setLayout(gl_compositeController);
        
        txtComponentName = new Text(compositeController, SWT.BORDER);
        txtComponentName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        txtComponentName.setEditable(false);
        txtComponentName.setText("Controller");
        
        Button btnUpdatePorts = new Button(compositeController, SWT.NONE);
        btnUpdatePorts.setToolTipText("Update parameters and non-functional requirements.");
        btnUpdatePorts.setText("Update");
        
                btnUpdatePorts.addSelectionListener(new SelectionAdapter() {
                	@Override
                	public void widgetSelected(SelectionEvent e) {
                		updatePorts();
                	}
                });
        
        Label lblPort = new Label(s, SWT.NONE);
        lblPort.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblPort.setText("Port:");
        
        comboPorts = new Combo(s, SWT.READ_ONLY);
        comboPorts.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        
        Label sep0 = new Label(s, SWT.SEPARATOR | SWT.HORIZONTAL);
        sep0.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
        
        Label lblParameters = new Label(s, SWT.NONE);
        lblParameters.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblParameters.setText("Parameters:");
        
        Composite compositeParameters = new Composite(s, SWT.NONE);
        GridData gd_compositeParameters = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_compositeParameters.heightHint = 100;
        compositeParameters.setLayoutData(gd_compositeParameters);
        TableColumnLayout tcl_compositeParameters = new TableColumnLayout();
        compositeParameters.setLayout(tcl_compositeParameters);
        
        viewerParameters = new TableViewer(compositeParameters, SWT.BORDER | SWT.FULL_SELECTION);
        final Table tableParameters = viewerParameters.getTable();
        tableParameters.setHeaderVisible(true);
        tableParameters.setLinesVisible(true);
        
        TableViewerColumn tblclmnName = new TableViewerColumn(viewerParameters, SWT.CENTER);
        tcl_compositeParameters.setColumnData(tblclmnName.getColumn(), new ColumnWeightData(20, 50));
        tblclmnName.getColumn().setText("Name");
        tblclmnName.setLabelProvider(new ColumnLabelProvider() {
        	@Override
        	public String getText(Object element) {
        		return ((ParameterLine) element).getParameterName();
        	}
		});
        // not editable
        
        TableViewerColumn tblclmnValue = new TableViewerColumn(viewerParameters, SWT.CENTER);
        ColumnWeightData cwd_tblclmnValue = new ColumnWeightData(80);
        cwd_tblclmnValue.minimumWidth = 399;
        tcl_compositeParameters.setColumnData(tblclmnValue.getColumn(), cwd_tblclmnValue);
        tblclmnValue.getColumn().setText("Value");
        tblclmnValue.setLabelProvider(new ColumnLabelProvider(){
        	@Override
        	public String getText(Object element) {
        		return ((ParameterLine) element).getValue().toString();
        	}
        });
        tblclmnValue.setEditingSupport(new EditingSupport(viewerParameters) {
			
			@Override
			protected void setValue(Object element, Object value) {
				ParameterLine parameterLine = (ParameterLine) element;
				Object oldValue = parameterLine.getValue();
				if(!oldValue.equals(value)) {
					parameterLine.setValue(value);
					viewerParameters.update(element, null);
					updateMetaparams();
				}
			}
			
			@Override
			protected Object getValue(Object element) {
				return element;
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				return new ParameterValueCellEditor(viewerParameters.getTable());
			}
			
			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});
        
        viewerParameters.setContentProvider(ArrayContentProvider.getInstance());
        viewerParameters.setInput(params);
        
        Label label = new Label(s_1, SWT.SEPARATOR | SWT.HORIZONTAL);
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
        
        Label lblRequirements = new Label(s_1, SWT.NONE);
        lblRequirements.setAlignment(SWT.RIGHT);
        lblRequirements.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblRequirements.setText("Non-functional\r\nrequirements:");
        
        Composite compositeNfr = new Composite(s_1, SWT.NONE);
        GridData gd_compositeNfr = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_compositeNfr.heightHint = 100;
        compositeNfr.setLayoutData(gd_compositeNfr);
        TableColumnLayout tcl_compositeNfr = new TableColumnLayout();
        compositeNfr.setLayout(tcl_compositeNfr);
        
        viewerNfr = new TableViewer(compositeNfr, SWT.BORDER | SWT.FULL_SELECTION);
        Table tableNfr = viewerNfr.getTable();
        tableNfr.setHeaderVisible(true);
        tableNfr.setLinesVisible(true);
        
        final CellEditor checkBoxEditorNfr = new CheckboxCellEditor(
        		viewerNfr.getTable(), SWT.CHECK); //XXX rs: is check style is appropriate?
        
        TableViewerColumn tblclmnNfrUse = new TableViewerColumn(viewerNfr, SWT.NONE);
        tcl_compositeNfr.setColumnData(tblclmnNfrUse.getColumn(), new ColumnWeightData(10));
        tblclmnNfrUse.getColumn().setText("Use");
        tblclmnNfrUse.setLabelProvider(new ColumnLabelProvider(){
        	@Override
        	public String getText(Object element) {
        		return Boolean.toString(((NfrLine) element).isUsed());
        	}
        });
        tblclmnNfrUse.setEditingSupport(new EditingSupport(viewerNfr) {
			
			@Override
			protected void setValue(Object element, Object value) {
				if(element instanceof NfrLine && value instanceof Boolean) {
        			((NfrLine) element).setUsed((Boolean) value);
					getViewer().update(element, null);
				}
			}
			
			@Override
			protected Object getValue(Object element) {
        		return Boolean.valueOf(((NfrLine) element).isUsed());
//				return false;
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				return checkBoxEditorNfr;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});
        
        TableViewerColumn tblclmnNfrName = new TableViewerColumn(viewerNfr, SWT.NONE);
        tcl_compositeNfr.setColumnData(tblclmnNfrName.getColumn(), new ColumnWeightData(20, 50));
        tblclmnNfrName.getColumn().setText("Name");
        tblclmnNfrName.setLabelProvider(new ColumnLabelProvider(){
        	@Override
        	public String getText(Object element) {
        		return ((NfrLine) element).getPropertyName().toString();
        	}
        });
        
        TableViewerColumn tblclmnNfrDirection = new TableViewerColumn(viewerNfr, SWT.NONE);
        tcl_compositeNfr.setColumnData(tblclmnNfrDirection.getColumn(), new ColumnWeightData(20));
        tblclmnNfrDirection.getColumn().setText("Direction");
        tblclmnNfrDirection.setLabelProvider(new ColumnLabelProvider(){
        	@Override
        	public String getText(Object element) {
        		return ((NfrLine) element).isMin() ? "min" : "max";
        	}
        });
        tblclmnNfrDirection.setEditingSupport(new EditingSupport(viewerNfr) {
			@Override
			protected void setValue(Object element, Object value) {
				if(element instanceof NfrLine && value instanceof Boolean) {
					((NfrLine) element).setMin((Boolean) value);
					getViewer().update(element, null);
				}
			}
			
			@Override
			protected Object getValue(Object element) {
				return Boolean.valueOf(((NfrLine) element).isUsed());
			}
			
			@Override
			protected CellEditor getCellEditor(Object element) {
				return checkBoxEditorNfr;
			}
			
			@Override
			protected boolean canEdit(Object element) {
				return true;
			}
		});
        
        TableViewerColumn tblclmnNfrValue = new TableViewerColumn(viewerNfr, SWT.NONE);
        tcl_compositeNfr.setColumnData(tblclmnNfrValue.getColumn(), new ColumnWeightData(50));
        tblclmnNfrValue.getColumn().setText("Value");
        tblclmnNfrValue.setLabelProvider(new ColumnLabelProvider(){
        	@Override
        	public String getText(Object element) {
        		return ((NfrLine) element).getValue().toString();
        	}
        });
        tblclmnNfrValue.setEditingSupport(new ValueEditingSupport(viewerNfr) {
			
			@Override
			protected boolean canEdit(Object element) {
				return true;
			}

			@Override
			protected Object parseStringValue(Object element, String stringValue) {
				//XXX rs: only integer supported for nfrs yet
				try {
					int intValue = Integer.parseInt(stringValue);
					return intValue;
				} catch (NumberFormatException e) {
					System.err.println("[vTS GUI] Error while converting " + stringValue + " to integer.");
					return null;
				}
			}
		});

        viewerNfr.setContentProvider(ArrayContentProvider.getInstance());
        viewerNfr.setInput(nfrs);
        
        Label sep1 = new Label(s, SWT.SEPARATOR | SWT.HORIZONTAL);
        sep1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
        
        Composite compositeMetaparameterLblBtn = new Composite(s_1, SWT.NONE);
        compositeMetaparameterLblBtn.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        GridLayout gl_compositeMetaparameterLblBtn = new GridLayout(1, false);
        gl_compositeMetaparameterLblBtn.marginWidth = 0;
        gl_compositeMetaparameterLblBtn.marginHeight = 0;
        compositeMetaparameterLblBtn.setLayout(gl_compositeMetaparameterLblBtn);
        
        Label lblMetaparameter = new Label(compositeMetaparameterLblBtn, SWT.NONE);
        lblMetaparameter.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));
        lblMetaparameter.setText("Metaparameters:");
        
        Button btnAutomatic = new Button(compositeMetaparameterLblBtn, SWT.CHECK);
        btnAutomatic.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		autoUpdateMetaparams = ((Button) e.widget).getSelection();
        	}
        });
        btnAutomatic.setSelection(true); // Default: Checked, i.e. doing auto update
        btnAutomatic.setToolTipText("Automatically update values of metaparameters based on parameter values");
        btnAutomatic.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        btnAutomatic.setText("Auto-Update");
        autoUpdateMetaparams = btnAutomatic.getSelection();
                
        Button btnUpdateMetaparams = new Button(compositeMetaparameterLblBtn, SWT.NONE);
        btnUpdateMetaparams.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
        btnUpdateMetaparams.setToolTipText("Update metaparameter values.");
        btnUpdateMetaparams.setText("Update");
        btnUpdateMetaparams.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		updateMetaparams(true);
        	}
        });
        
        Composite compositeMetaparameters = new Composite(s, SWT.NONE);
        GridData gd_compositeMetaparameters = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
        gd_compositeMetaparameters.heightHint = 100;
        compositeMetaparameters.setLayoutData(gd_compositeMetaparameters);
        TableColumnLayout tcl_compositeMetaparameters = new TableColumnLayout();
        compositeMetaparameters.setLayout(tcl_compositeMetaparameters);

        viewerMetaparameters = new TableViewer(compositeMetaparameters, SWT.BORDER | SWT.FULL_SELECTION);
        Table tableMetaparameters = viewerMetaparameters.getTable();
        tableMetaparameters.setHeaderVisible(true);
        tableMetaparameters.setLinesVisible(true);
        
        TableViewerColumn tblclmnMetaparameterName = new TableViewerColumn(viewerMetaparameters, SWT.CENTER);
        tcl_compositeMetaparameters.setColumnData(tblclmnMetaparameterName.getColumn(), new ColumnWeightData(20, 50));
        tblclmnMetaparameterName.getColumn().setText("Name");
        tblclmnMetaparameterName.setLabelProvider(new ColumnLabelProvider() {
        	@Override
        	public String getText(Object element) {
        		return ((MetaparameterLine) element).getMetaparameterName();
        	}
        });
        
        TableViewerColumn tblclmnMetaparameterValue = new TableViewerColumn(viewerMetaparameters, SWT.CENTER);
        tcl_compositeMetaparameters.setColumnData(tblclmnMetaparameterValue.getColumn(), new ColumnWeightData(80));
        tblclmnMetaparameterValue.getColumn().setText("Value");
        tblclmnMetaparameterValue.setLabelProvider(new ColumnLabelProvider() {
        	@Override
        	public String getText(Object element) {
        		return ((MetaparameterLine) element).getValue().toString();
        	}
        });
        tblclmnMetaparameterValue.setEditingSupport(new ValueEditingSupport(viewerMetaparameters) {
			
			@Override
			protected boolean canEdit(Object element) {
				//XXX rs: should not be allowed in final app
				return true;
			}

			@Override
			protected Object parseStringValue(Object element, String stringValue) {
				//XXX rs: only integer supported for metaparameters yet
				try {
					int intValue = Integer.parseInt(stringValue);
					return intValue;
				} catch (NumberFormatException e) {
					log.warn("Error while converting " + stringValue + " to integer.", e);
					return null;
				}
			}
		});

        viewerMetaparameters.setContentProvider(ArrayContentProvider.getInstance());
        viewerMetaparameters.setInput(metaparams);
        
        Label sep2 = new Label(s, SWT.SEPARATOR | SWT.HORIZONTAL);
        sep2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
        
        Label lblOptimizer = new Label(s, SWT.NONE);
        lblOptimizer.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblOptimizer.setText("Optimizer:");
        
        Composite compositeOptimizer = new Composite(s_1, SWT.NONE);
        compositeOptimizer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
        GridLayout gl_compositeOptimizer = new GridLayout(2, false);
        gl_compositeOptimizer.marginWidth = 0;
        gl_compositeOptimizer.marginHeight = 0;
        compositeOptimizer.setLayout(gl_compositeOptimizer);
        
        final Combo comboOptimizer = new Combo(compositeOptimizer, SWT.READ_ONLY);
        comboOptimizer.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboOptimizer.setItems(optimizerNames);
        comboOptimizer.select(0);
        comboOptimizer.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				optimizerIndex = comboOptimizer.getSelectionIndex();
			}
		});
        
        Button btnRun = new Button(compositeOptimizer, SWT.NONE);
        btnRun.setText("Run");
        btnRun.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		startAndAnalyzeRequest();
        	}
        });
        
        Label sep3 = new Label(s, SWT.SEPARATOR | SWT.HORIZONTAL);
        sep3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
        
        Label lblResult = new Label(s, SWT.NONE);
        lblResult.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
        lblResult.setText("Result:");
        
        txtResult = new Text(s, SWT.BORDER);
        txtResult.setEditable(false);
        txtResult.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
        comboPorts.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		onPortSelection();
        	}
        });
		new Label(s_1, SWT.NONE);
		
		Composite compositeOpen = new Composite(s_1, SWT.NONE);
		GridLayout gl_compositeOpen = new GridLayout(2, false);
		gl_compositeOpen.marginHeight = 0;
		compositeOpen.setLayout(gl_compositeOpen);
		
		Button btnOpenFile = new Button(compositeOpen, SWT.NONE);
		btnOpenFile.setText("Open file");
		btnOpenFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// get result file
				File resultFile = getReturnedFile();
				if(!resultFile.exists() || !resultFile.isFile()) {
					MessageDialog.openWarning(s_1, title, "Result file does not exist or is no file!");
					return;
				}
				// open file
				if(Desktop.isDesktopSupported()) {
					try {
						Desktop.getDesktop().open(resultFile);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				else {
					MessageDialog.openInformation(s_1, title, "Opening files not supported!");
				}
			}
		});
		
		Button btnOpenContainingDirectory = new Button(compositeOpen, SWT.NONE);
		btnOpenContainingDirectory.setText("Open containing directory");
		btnOpenContainingDirectory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// get result file
				File resultFile = getReturnedFile();
				if(!resultFile.exists() || !resultFile.isFile()) {
					MessageDialog.openWarning(s_1, title, "Result file does not exist or is no file!");
					return;
				}
				// get parent
				File parentDir = resultFile.getParentFile();
				if(!parentDir.exists() || !parentDir.isDirectory()) {
					MessageDialog.openWarning(s_1, title, "Directory of result file does not exists or is no directory!");
					return;
				}
				// open parent
				if(Desktop.isDesktopSupported()) {
					try {
						Desktop.getDesktop().open(parentDir);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				else {
					MessageDialog.openInformation(s_1, title, "Opening directories not supported!");
				}
			}
		});
		
		updatePorts();
	}

	protected File getReturnedFile() {
		return returnedFile;
	}

	protected void setReturnedFile(File returnValue) {
		returnedFile = returnValue;
		if(returnedFile == null) {
			txtResult.setText("");
		}
		else {
			txtResult.setText(returnedFile.getAbsolutePath());
		}
	}

	protected ILocalEnergyManager getLEM(String uri) {
		ServiceReference<?> remoteRef = context.getServiceReference(RemoteOSGiService.class.getName());
		if(remoteRef == null) {
			 System.out.println("[vTS GUI] Error: R-OSGi not found!");
		}
		RemoteOSGiService remote = (RemoteOSGiService)context.getService(remoteRef);
		final RemoteServiceReference[] srefs =
				remote.getRemoteServiceReferences(new URI(uri),
				ILocalEnergyManager.class.getName(), null);
		ILocalEnergyManager lem = (ILocalEnergyManager)remote.getRemoteService(srefs[0]);
		return lem;
	}

	/**
	 * Prints the given object and returns it as-is.
	 * @param toPrint the object to print
	 * @return the given object
	 */
	protected <T> T p(T toPrint) {
		System.out.print(toPrint + ", ");
		return toPrint;
	}
	
	protected <T> T pln(T toPrint) {
		System.out.println("[vTS GUI] " + toPrint);
		return toPrint;
	}

	protected void updateWithList(Combo comboBox, List<String> list) {
		comboBox.setItems(list.toArray(new String[list.size()]));
		comboBox.select(0);
	}

	@Override
	protected String getSelectedOptimizer() {
		return optimizerIds[optimizerIndex];
	}

	private void updatePorts() {
		updateWithList(comboPorts, getProvidedPorts(getGEM(), getAppName(), getCompName(), null));
		onPortSelection();
	}

	private void onPortSelection() {
		IGlobalEnergyManager gem = getGEM();
		String appName = getAppName();
		String compId = getCompName();
		String portName = getSelectedPortName();
		System.out.printf("[vTS GUI] <onPortSelection> appname: %s, compId:%s, portName: %s\n", appName, compId, portName);
		resetParameter(gem.getPortParameters(appName, compId, portName));
		resetNfrs(gem.getComponentProperties(appName, compId));
	}

	private void resetParameter(List<String> parameterNames) {
		System.out.println("[vTS GUI] <resetParameter> list:");
		params.clear();
		for(String parameterString : parameterNames) {
			int indexOfColon = parameterString.indexOf(':');
			String parameterName;
			String parameterType;
			if(indexOfColon == -1) {
				System.err.println("[vTS GUI] No valid format of parameterString: " + parameterString);
				parameterName = parameterString;
				parameterType = "Value";
			}
			else {
				parameterName = parameterString.substring(0, indexOfColon).trim();
				parameterType = parameterString.substring(indexOfColon + 1).trim();
			}
//			System.out.println(" <resetParameter> " + parameterName);
			params.add(pln(new ParameterLine(parameterName, parameterType, getDefaultParameterValue(parameterName))));
		}
		viewerParameters.refresh();
		updateMetaparams();
	}

	private Object getDefaultParameterValue(String parameterName) {
		if(parameterName != null) {
//			int indexOfColon = parameterString.indexOf(':');
//			if(indexOfColon == -1) {
//				System.err.println("No valid format of parameterString: " + parameterString);
//				indexOfColon = parameterString.length();
//			}
//			String parameterName = parameterString.substring(0, indexOfColon).trim();
			if(parameterName.equals(parameterVideo1) || parameterName.equals("in"))
				return video1;
			else if(parameterName.equals(parameterVideo2))
				return video2;
			else if(parameterName.equals("w") || parameterName.equals("w2")
					|| parameterName.equals("h") || parameterName.equals("h2"))
				return Integer.valueOf(-2);
			else if(parameterName.equals("mult"))
				return Boolean.TRUE;
			else if(parameterName.equals("pos"))
				return Integer.valueOf(PipDoPip.POS_CENTER);
			System.err.println("[vTS GUI] Unknown parameterName: '" + parameterName + "'");
		}
		return "null";
	}

	private void resetNfrs(List<String> propertyNames) {
		System.out.println("[vTS GUI] <resetNfrs> list:");
		nfrs.clear();
		for(String propertyName : propertyNames) {
			System.out.println("[vTS GUI]  <resetNfrs> " + propertyName);
			nfrs.add(new NfrLine(propertyName, getDefaultNfrValue(propertyName)));
		}
		viewerNfr.refresh();
	}

	private Object getDefaultNfrValue(String propertyName) {
		if(propertyName != null) {
			if(propertyName.equals("response_time"))
				return "80000";
		}
		System.err.println("[vTS GUI] Unknown propertyName: " + propertyName);
		return "null";
	}

	@Override
	protected String getAppName() {
		return txtAppName.getText();
	}

	@Override
	protected String getCompName() {
		return txtComponentName.getText();
	}

	@Override
	protected String getSelectedPortName() {
		//TODO rs: ensure always a valid index (else exception will be thrown by getItem)
		return comboPorts.getItem(comboPorts.getSelectionIndex());
	}
	
	@Override
	protected List<ParameterLine> getParams() {
		return params;
	}

	@Override
	protected List<NfrLine> getNfrs() {
		return nfrs;
	}

	@Override
	protected List<MetaparameterLine> getMetaparams() {
		return metaparams;
	}

	@Override
	protected IGlobalEnergyManager getGEM() {
		IGlobalEnergyManager ret = null;
		ServiceReference<?> remoteRef = context.getServiceReference(RemoteOSGiService.class.getName());
		if(remoteRef == null) {
			 System.out.println("[vTS GUI] Error: R-OSGi not found!");
		}
		RemoteOSGiService remote = (RemoteOSGiService)context.getService(remoteRef);
		
		String ip = "127.0.0.1";
		final RemoteServiceReference[] srefs =
			remote.getRemoteServiceReferences(RemoteOsgiUtil.createRemoteOsgiUri(ip),
				IGlobalEnergyManager.class.getName(), null);
		
		ret = (IGlobalEnergyManager)remote.getRemoteService(srefs[0]);
		return ret;
	}
	
	protected void updateMetaparams() {
		updateMetaparams(false);
	}
	
	protected void updateMetaparams(boolean forceUpdate) {
		if(!autoUpdateMetaparams && !forceUpdate)
			return;
		// search for parameters video1 and video2 (hard coded parameter names)
		File video1 = null, video2 = null;
		for(ParameterLine line : params) {
			if(line.getParameterName().equals(parameterVideo1)) {
				video1 = (File) line.getValue();
			}
			else if(line.getParameterName().equals(parameterVideo2)) {
				video2 = (File) line.getValue();
			}
		}
		// compute duration
		long durationVideo1 = 0, durationVideo2 = 0;
		if(video1 == null || video2 == null) return;
		try {
			Assert.isLegal(video1.exists() && video1.isFile(), video1 + " does not exist or is not a file!");
			Assert.isLegal(video2.exists() && video2.isFile(), video2 + " does not exist or is not a file!");
			VideoDurationCalculator durationCalc = getDurationCalculator();
			durationVideo1 = durationCalc.getDurationOfVideo(video1).roundToSeconds();
			durationVideo2 = durationCalc.getDurationOfVideo(video2).roundToSeconds();
		}
		catch(Exception ex) {
			ex.printStackTrace();
			System.err.println("[vTS GUI] Incomplete calculation, using default duration values.");
		};
		// compute resolution
		int resX1 = 0, resX2 = 0, resY1 = 0, resY2 = 0;
		try {
			VideoResolutionCalculator resCalc = getResolutionCalculator();
			VideoResolution res1 = resCalc.getVideoResolution(video1);
			VideoResolution res2 = resCalc.getVideoResolution(video2);
			resX1 = res1.width;
			resY1 = res1.height;
			resX2 = res2.width;
			resY2 = res2.height;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("[vTS GUI] Incomplete calculation, using default resolution values.");
		}
		if(durationVideo1 == 0) { durationVideo1 = 4; }
		if(durationVideo1 == 0) { durationVideo1 = 4; }
		if(resX1 == 0) { resX1 = 640; }
		if(resX2 == 0) { resX2 = 320; }
		if(resY1 == 0) { resY1 = 640; }
		if(resY2 == 0) { resY2 = 320; }
		// write to metaparameter max_video_length
		theMetaparameterLine.setValue(pln(TranscodingServerUtil.getMetaparameterValue(
				p((int) durationVideo1), p((int) durationVideo2), p(resX1), p(resY1), p(resX2), p(resY2))));
		viewerMetaparameters.update(theMetaparameterLine, null); //XXX rs: maybe use "value" for properties
	}
	
	private VideoResolutionCalculator getResolutionCalculator() {
		ServiceReference<VideoResolutionCalculator> sref = context.getServiceReference(
				VideoResolutionCalculator.class);
		return context.getService(sref);
	}

	private VideoDurationCalculator getDurationCalculator() {
		ServiceReference<VideoDurationCalculator> sref = context.getServiceReference(
				VideoDurationCalculator.class);
		return context.getService(sref);
	}

	private void startAndAnalyzeRequest() {
		ExecuteResult ee = startRequest();
		Serializable result = ee.getResult();
		if(result == null) {
			log.warn("Error during execution of method, returnValue is null!");
		}
		else if(ee instanceof ErrorResult) {
			log.warn("Error during execution of method.", (Throwable) ee.getResult());
		}
		else if(result instanceof File) {
			setReturnedFile((File) result);
		}
		else if(result instanceof FileProxy) {
			setReturnedFile(((FileProxy) result).getFileFor(getThisUri()));
		}
		else {
			log.warn("Unsupported class of return value: " + result.getClass().getName());
		}
	}

	private java.net.URI getThisUri() {
		try {
			return new java.net.URI(getGEM().getUri());
		} catch (URISyntaxException e) {
			log.warn("error during uri creation", e);
			return null;
		}
	}

	/**
	 * EditingSupport for ValueLines.
	 * @author Ren�
	 */
	abstract class ValueEditingSupport extends EditingSupport {
		final CellEditor cellEditor;

		public ValueEditingSupport(TableViewer viewer) {
			super(viewer);
			cellEditor = new TextCellEditor(viewer.getTable());
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return cellEditor;
		}

		@Override
		protected Object getValue(Object element) {
			return ((ValueLine) element).getValue().toString();
		}

		@Override
		protected void setValue(Object element, Object value) {
			String stringValue = (String) value;
			Object newValue = parseStringValue(element, stringValue);
			if(newValue != null) {
				((ValueLine) element).setValue(newValue);
				getViewer().update(element, getProperties(element, newValue));
			}
		}

		/** Parses the string value returned by the TextCellEditor.
		 * @param element the line of the stringValue
		 * @param stringValue the value returned by the TextCellEditor
		 * @return the parsed value or <code>null</code> on failure */
		protected abstract Object parseStringValue(Object element, String stringValue);
		
		/** Called in {@link #setValue(Object, Object)} to update viewer.
		 * Empty implementation returning <code>null</code>.
		 * Intended to be overriden by subclasses. */
		protected String[] getProperties(Object element, Object newValue) {
			return null;
		}
	}
	
	public class ParameterValueCellEditor extends TextCellEditor {
	
		private final String FILE_PREFIX = "file";
		private ParameterLine currentLine;
		private File selectedFile;
	
		public ParameterValueCellEditor(Table table) {
			super(table);
		}
	
		@Override
		protected Object doGetValue() {
			String type = currentLine.getParameterType();
			if(type.equals("String")) {
				return text.getText();
			}
			else if(type.equals("Integer")) {
				return Integer.parseInt(text.getText());
			}
			else if(type.equals("Boolean")) {
				return Boolean.parseBoolean(text.getText());
			}
			else if(currentLine.getParameterName().startsWith(FILE_PREFIX)) {
				if(selectedFile == null) {
					System.err.println("[vTS GUI] Selected file is null!");
					return currentLine.getValue();
				}
				return selectedFile;
			}
			return "<unknown type (" + type + ")>";
		}
		
		@Override
		protected void doSetFocus() {
			if(currentLine.getParameterName().startsWith(FILE_PREFIX)) {
				FileDialog fd = new FileDialog(s_1, SWT.OPEN);
				fd.setFileName(getCurrentPath());
				String selectedPath = fd.open();
				if(selectedPath != null) {
					text.setText(selectedPath);
					selectedFile = new File(selectedPath);
				}
				else {
					selectedFile = new File(text.getText());
				}
			}
			else {
				super.doSetFocus();
			}
		}
		
		private String getCurrentPath() {
			return ((File) currentLine.getValue()).getAbsolutePath();
		}
	
		@Override
		protected void doSetValue(Object value) {
			// the value will be a ParameterLine
			currentLine = (ParameterLine) value;
			super.doSetValue(currentLine.getParameterName().startsWith(FILE_PREFIX)?
					getCurrentPath() : currentLine.getValue().toString());
		}
		
	}

	public static void main(String[] args) {
		
	}
	
}
