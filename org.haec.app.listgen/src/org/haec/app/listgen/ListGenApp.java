package org.haec.app.listgen;

import org.haec.theatre.api.App;

/**
 * "Shortcut application" only containing the listgen component.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class ListGenApp implements App {

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getName()
	 */
	@Override
	public String getName() {
		return "org.haec.app.composedsort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getApplicationModelPath()
	 */
	@Override
	public String getApplicationModelPath() {
		return "models/listgen.structure";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getContractsPath()
	 */
	@Override
	public String getContractsPath() {
		return "models/contracts";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getBundleSymbolicName()
	 */
	@Override
	public String getBundleSymbolicName() {
		return "org.haec.app.listgen";
	}

}
