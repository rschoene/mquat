import ccm [../Server.structure]
import ccm [../listgen.structure]

contract org.haec.app.listgen.random.Random implements software ListGen.generate { 
	mode fast {		
		requires resource CPU {			
			frequency min: 1500			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 55.0		
		}		
		provides response_time <f_wtime_fast(list_size)> 	
	}	
	mode slow {		
		requires resource CPU {			
			frequency min: 900			
			cpu_time <f_cpu_time(list_size)>
			energyRate: 45.0
		}		
		provides response_time <f_wtime_slow(list_size)>
	}
}