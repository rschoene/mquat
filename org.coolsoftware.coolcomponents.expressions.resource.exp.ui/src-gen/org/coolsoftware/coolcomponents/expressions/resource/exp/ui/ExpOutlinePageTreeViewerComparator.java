/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.ui;

public class ExpOutlinePageTreeViewerComparator extends org.eclipse.jface.viewers.ViewerComparator {
	
	private static java.util.Map<org.eclipse.emf.ecore.EPackage, Integer> ePackageMap = new java.util.LinkedHashMap<org.eclipse.emf.ecore.EPackage, Integer>();
	private static int nextPackageID;
	
	private java.util.Comparator<Object> comparator = new java.util.Comparator<Object>() {
		
		public int compare(Object o1, Object o2) {
			if (!sortLexically) {
				return 0;
			}
			if (o1 instanceof String && o2 instanceof String) {
				String s1 = (String) o1;
				String s2 = (String) o2;
				return s1.compareTo(s2);
			}
			return 0;
		}
	};
	private boolean groupTypes;
	private boolean sortLexically;
	
	public void setGroupTypes(boolean on) {
		this.groupTypes = on;
	}
	
	public void setSortLexically(boolean on) {
		this.sortLexically = on;
	}
	
	@Override	
	public int category(Object element) {
		if (!groupTypes) {
			return 0;
		}
		if (element instanceof org.eclipse.emf.ecore.EObject) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) element;
			org.eclipse.emf.ecore.EClass eClass = eObject.eClass();
			org.eclipse.emf.ecore.EPackage ePackage = eClass.getEPackage();
			int packageID = getEPackageID(ePackage);
			int classifierID = eClass.getClassifierID();
			return packageID + classifierID;
		} else {
			return super.category(element);
		}
	}
	
	private int getEPackageID(org.eclipse.emf.ecore.EPackage ePackage) {
		Integer packageID = ePackageMap.get(ePackage);
		if (packageID == null) {
			packageID = nextPackageID;
			// we assumed that packages do contain at most 1000 classifiers
			nextPackageID += 1000;
			ePackageMap.put(ePackage, nextPackageID);
		}
		return packageID;
	}
	
	public java.util.Comparator<?> getComparator() {
		return this.comparator;
	}
	
	public int compare(org.eclipse.jface.viewers.Viewer viewer, Object o1, Object o2) {
		// first check categories
		int cat1 = category(o1);
		int cat2 = category(o2);
		if (cat1 != cat2) {
			return cat1 - cat2;
		}
		// then try to compare the names
		if (sortLexically && o1 instanceof org.eclipse.emf.ecore.EObject && o2 instanceof org.eclipse.emf.ecore.EObject) {
			org.eclipse.emf.ecore.EObject e1 = (org.eclipse.emf.ecore.EObject) o1;
			org.eclipse.emf.ecore.EObject e2 = (org.eclipse.emf.ecore.EObject) o2;
			org.coolsoftware.coolcomponents.expressions.resource.exp.IExpNameProvider nameProvider = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpMetaInformation().createNameProvider();
			java.util.List<String> names1 = nameProvider.getNames(e1);
			java.util.List<String> names2 = nameProvider.getNames(e2);
			if (names1 != null && !names1.isEmpty() && names2 != null && !names2.isEmpty()) {
				String name1 = names1.get(0);
				String name2 = names2.get(0);
				return name1.compareTo(name2);
			}
		}
		return super.compare(viewer, o1, o2);
	}
	
}
