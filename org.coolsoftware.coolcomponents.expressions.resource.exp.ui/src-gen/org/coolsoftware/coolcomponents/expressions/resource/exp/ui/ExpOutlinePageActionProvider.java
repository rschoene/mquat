/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.ui;

public class ExpOutlinePageActionProvider {
	
	public java.util.List<org.eclipse.jface.action.IAction> getActions(org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		java.util.List<org.eclipse.jface.action.IAction> defaultActions = new java.util.ArrayList<org.eclipse.jface.action.IAction>();
		defaultActions.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
