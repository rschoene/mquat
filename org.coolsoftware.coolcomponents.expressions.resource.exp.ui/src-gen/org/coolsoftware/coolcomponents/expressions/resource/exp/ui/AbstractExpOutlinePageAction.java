/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.ui;

public abstract class AbstractExpOutlinePageAction extends org.eclipse.jface.action.Action {
	
	private String preferenceKey = this.getClass().getSimpleName() + ".isChecked";
	
	private org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageTreeViewer treeViewer;
	
	public AbstractExpOutlinePageAction(org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageTreeViewer treeViewer, String text, int style) {
		super(text, style);
		this.treeViewer = treeViewer;
	}
	
	public void initialize(String imagePath) {
		org.eclipse.jface.resource.ImageDescriptor descriptor = org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpImageProvider.INSTANCE.getImageDescriptor(imagePath);
		setDisabledImageDescriptor(descriptor);
		setImageDescriptor(descriptor);
		setHoverImageDescriptor(descriptor);
		boolean checked = org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpUIPlugin.getDefault().getPreferenceStore().getBoolean(preferenceKey);
		valueChanged(checked, false);
	}
	
	@Override	
	public void run() {
		if (keepState()) {
			valueChanged(isChecked(), true);
		} else {
			runBusy(true);
		}
	}
	
	public void runBusy(final boolean on) {
		org.eclipse.swt.custom.BusyIndicator.showWhile(org.eclipse.swt.widgets.Display.getCurrent(), new Runnable() {
			public void run() {
				runInternal(on);
			}
		});
	}
	
	public abstract void runInternal(boolean on);
	
	private void valueChanged(boolean on, boolean store) {
		setChecked(on);
		runBusy(on);
		if (store) {
			org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpUIPlugin.getDefault().getPreferenceStore().setValue(preferenceKey, on);
		}
	}
	
	public boolean keepState() {
		return true;
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageTreeViewer getTreeViewer() {
		return treeViewer;
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageTreeViewerComparator getTreeViewerComparator() {
		return (org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpOutlinePageTreeViewerComparator) treeViewer.getComparator();
	}
	
}
