/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.ui;

/**
 * This class is deprecated and not used as of EMFText 1.4.1. The original
 * contents of this class have been moved to
 * org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpSourceViewerConfi
 * guration. This class is only generated to avoid compile errors with existing
 * versions of this class.
 */
@Deprecated
public class ExpEditorConfiguration {
	
}
