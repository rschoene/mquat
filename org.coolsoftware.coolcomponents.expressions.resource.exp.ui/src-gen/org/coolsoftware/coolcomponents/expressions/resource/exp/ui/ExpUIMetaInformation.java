/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.ui;

public class ExpUIMetaInformation extends org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpMetaInformation {
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpHoverTextProvider getHoverTextProvider() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpHoverTextProvider();
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpImageProvider getImageProvider() {
		return org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpImageProvider.INSTANCE;
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpColorManager createColorManager() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(org.coolsoftware.coolcomponents.expressions.resource.exp.IExp
	 * TextResource,
	 * org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpColorManager)
	 * instead.
	 */
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpTokenScanner createTokenScanner(org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpTokenScanner createTokenScanner(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource, org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpColorManager colorManager) {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpTokenScanner(resource, colorManager);
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpCodeCompletionHelper createCodeCompletionHelper() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.ui.ExpCodeCompletionHelper();
	}
	
}
