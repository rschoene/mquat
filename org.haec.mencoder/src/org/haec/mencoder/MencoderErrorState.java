/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.mencoder;

import java.util.List;

import org.haec.apps.util.IntegerErrorState;

/**
 * ErrorState implementation of mencoder.
 * @author René Schöne
 */
public class MencoderErrorState implements IntegerErrorState {
	
	private static final int START = 0;
	private static final int SWITCH_AUDIO_CODEC = 1;
	private static final int NO_SOUND = 2;
	private static final int GIVE_UP = 3;
	
	private int currentState = START;
	/* (non-Javadoc)
	 * @see org.haec.mencoder.ErrorState#incrementCurrentState()
	 */
	@Override
	public void nextState() {
		this.currentState += 1;
	}
	/* (non-Javadoc)
	 * @see org.haec.mencoder.ErrorState#getCurrentState()
	 */
	@Override
	public int getCurrentState() {
		return currentState;
	}
	private boolean isStart() {
		return currentState == START;
	}
	private boolean isSwitchAudioCodec() {
		return currentState == SWITCH_AUDIO_CODEC;
	}
	private boolean isNoSound() {
		return currentState == NO_SOUND;
	}
	/* (non-Javadoc)
	 * @see org.haec.mencoder.ErrorState#shouldGiveUp()
	 */
	@Override
	public boolean shouldGiveUp() {
		return currentState == GIVE_UP;
	}
	/* (non-Javadoc)
	 * @see org.haec.mencoder.ErrorState#addHandlingParams(java.util.List)
	 */
	@Override
	public void addHandlingParams(List<String> params) {
		if(isStart()) {
//			params.add("-oac");
//			params.add("copy");
		}
		if(isSwitchAudioCodec()) {
			// values for oac: copy, pcm (uncompressed), mp3lame (lipmp3lame), lavc (ffmpeg audio encoder)
			addOrModify(params, "-oac", "lavc", null);
			addOrModify(params, "-lavcopts", "acodec=ac3", ":");
		}
		if(isNoSound() || MencoderUtil.sharedInstance.shouldEncodeNoSound()) {
			params.add("-nosound");
		}
	}
	
	
	/**
	 * @param params list of parameters
	 * @param key the key to look for
	 * @param newValue the new value (to add or to override with)
	 * @param delimiter, if <code>null</code> than override the old value
	 * @return <code>true</code> if modified, <code>false</code> if added
	 */
	private boolean addOrModify(List<String> params, String key, String newValue, String delimiter) {
		int index = params.indexOf(key);
		if(index >= 0) {
			if(delimiter != null) { params.set(index+1, params.get(index+1) + delimiter + newValue);
			} else { params.set(index+1, newValue); }
			return true;
		} else {
			params.add(key);
			params.add(newValue);
			return false;
		}
	}
}


