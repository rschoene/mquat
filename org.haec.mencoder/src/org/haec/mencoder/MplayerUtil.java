/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.mencoder;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.haec.apps.util.AbstractVideoUtil;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.VideoDuration;
import org.haec.apps.util.VideoDurationCalculator;
import org.haec.apps.util.VideoResolution;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.apps.util.VideoSettings;

/**
 * VideoUtil of mplayer.
 * Implements VideoDurationCalculator and VideoResolutionCalculator.
 * @author René Schöne
 */
public class MplayerUtil extends AbstractVideoUtil implements VideoDurationCalculator, VideoResolutionCalculator {

	public final static MplayerUtil sharedInstance = new MplayerUtil();

	public MplayerUtil() {
		super(VideoSettings.MPLAYER);
	}
	
	@Override
	public VideoDuration getDurationOfVideo(File video) {
		String length = "ID_LENGTH=";
		VideoDuration result = VideoDuration.UNKNOWN;
		List<String> list = null;
		try {
			Map<String, List<String>> map = identify(getIdentifyPb(video), length);
			list = map.get(length);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(list == null || list.isEmpty()) {
			return result;
		}
		// format: <second>.<millisecond%only 2 digets>
		for(String line : list) {
			StringTokenizer st = new StringTokenizer(line, ".");
			if(st.countTokens() == 2) {
				String secondsString = st.nextToken();
				String millisString  = st.nextToken();
				int seconds = Integer.parseInt(secondsString);
				int millis = Integer.parseInt(millisString);
		
				result = merge(seconds, millis);
				break;
			}
		}
		return result;
	}

	protected ProcessBuilder getIdentifyPb(File video) {
		return new ProcessBuilder(processCommand, "-vo", "null", "-ao", "null",
				"-frames", "0",
				"-identify", video.getAbsolutePath()).
				redirectErrorStream(true);
//		Map<String, String> tmp;
//		try {
//			tmp = identify(pb, searchString);
//		} catch (IOException e) {
//			System.err.println("[MPU] Error while using mplayer process.");
//			e.printStackTrace();
//			return null;
//		}
//		return tmp;
	}

	private VideoDuration merge(int seconds, int millis) {
		int minutes = seconds / 60;
		int hours = 0;
		if(minutes > 0) {
			hours = minutes / 60;
			if(hours > 0) {
				minutes %= 60;
			}
			seconds %= 60;
		}
		return new VideoDuration(hours, minutes, seconds + millis*1.0/100);
	}

	/* (non-Javadoc)
	 * @see org.haec.mencoder.VideoResolutionCalculator#getVideoResolution(java.io.File)
	 */
	@Override
	public VideoResolution getVideoResolution(File video) {
		String width = "ID_VIDEO_WIDTH=", height = "ID_VIDEO_HEIGHT=";
		Map<String, List<String>> map;
		try {
			map = identify(getIdentifyPb(video), width, height);
		} catch (IOException e) {
			e.printStackTrace();
			return VideoResolution.UNKNOWN;
		}
		List<String> wList = map.get(width);
		List<String> hList = map.get(height);
		if(wList == null || wList.size() == 0 || hList == null || hList.size() == 0) {
			return VideoResolution.UNKNOWN;
		}
		// take only first matches
		int w = Integer.parseInt(wList.get(0).trim());
		int h = Integer.parseInt(hList.get(0).trim());
		return new VideoResolution(w, h);
	}

	public static void main(String[] args) {
		// quick test
		if(args.length == 0) {
			System.out.println("First parameter must be path to video file");
			return;
		}
		File file = new File(args[0]);
		test(sharedInstance, file);
	}
	
	@Override
	public ErrorState createErrorState() {
		// TODO check whether MencoderErrorState is needed here
		return null;
	}

}
