/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.mencoder;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.haec.apps.util.AbstractVideoUtil;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.VideoSettings;
import org.haec.apps.util.VideoUtil;

/**
 * VideoUtil of mencoder.
 * @author René Schöne
 */
public class MencoderUtil extends AbstractVideoUtil {
	
	public final static VideoUtil sharedInstance = new MencoderUtil();
	private static final String[] additionalCommands = new String[]{
//		"-ovc", "lavc",
//		// values for ovc : copy, raw, lavc (maybe need to set options with -lavcopts)
//		"-of", "lavf",
		"-msglevel", "all=1"
		// msglevel: -1 silence, 0 fatal, 1 error, 2 warnings, 3 short hints, 4 information,
		//   5 status information, 6 long information, 7..9 debug
		};
	
	public MencoderUtil() {
		super(VideoSettings.MENCODER);
	}

	@Override
	protected String[] getAdditionalCommands() {
		return additionalCommands;
	}
	
	@Override
	protected void handleErrorStream(InputStream error) {
		BufferedReader br = new BufferedReader(new InputStreamReader(error));
		String line;
		try {
			while((line = br.readLine()) != null)
				System.out.println(line);
		} catch (IOException ignore) { }
		finally {
			close(br);
			close(error);
		}
	}

	private void close(Closeable closeable) {
		try { closeable.close(); } catch(IOException ignore) { }
	}

	@Override
	public ErrorState createErrorState() {
		return new MencoderErrorState();
	}

}
