/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;

class SortBenchmarkData implements BenchmarkData {

	int mp;
	Integer[] inp;
	
	public SortBenchmarkData(int mp, Integer[] inp) {
		super();
		this.mp = mp;
		this.inp = inp;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mp;
	}
	
}

/**
 * Benchmark of Javasort
 * @author Sebastian Götz
 */
public class JavasortBenchmark implements Benchmark {

	private Javasort aut = new Javasort();

//	public static void main(String[] args) {
//		JavasortBenchmark b = new JavasortBenchmark();
//		b.addMonitor(MonitorRepository.shared.getMonitor("cpu_time"));
//		b.addMonitor(MonitorRepository.shared.getMonitor("response_time"));
//		b.benchmark();
//	}

	private Integer[] generateList(int num, int min, int max) {
		Integer[] ret = new Integer[num];
		Random r = new Random();
		for (int i = 0; i < num; i++) {
			ret[i] = r.nextInt(max - min) + min + 1;
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>();
		for (int list_size = 0; list_size < 2000000; list_size += 250000) {
			List<Integer> mpv = new ArrayList<Integer>();
			mpv.add(list_size);
			Integer[] inp = generateList(list_size, 1, 500);
			result.add(new SortBenchmarkData(list_size, inp));
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		SortBenchmarkData sbd = (SortBenchmarkData) data;
		return aut.sort0(sbd.inp);
	}
}
