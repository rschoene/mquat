/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.sort;

import java.util.Arrays;

import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.TaskBasedImpl;

/**
 * Implementation of Javasort
 * @author Sebastian Götz
 */
public class Javasort extends TaskBasedImpl {

	public Object sort(Object o) {
		Integer[] list;
		if(o instanceof Integer) {
			// we got invoked by the LEM. create list first.
			list = invoke(Integer[].class, "ListGen", "generate", (Integer) o);
		}
		else {
			list = (Integer[]) o;
		}
		return sort0(list);
	}

	// metaparameter: list_size = in.length
	public Object[] sort0(Object[] in) {
		Arrays.sort(in);
		return in;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return "org.haec.app.composedsort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "Sort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.sort.Javasort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new JavasortBenchmark();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.sort.javasort";
	}

	
}
