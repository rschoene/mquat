/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.resource.config.analysis.util;

import java.io.File;
import java.io.IOException;

import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * 
 * @author Sebastian Götz
 */
public class ResolverUtil {
	
	public static VariantModel getSelectRequest(Object elem) {
		if (elem instanceof IFile) {
			IFile file = (IFile) elem;

			/* First try a relative path. */
			String workspaceLocation = ResourcesPlugin.getWorkspace().getRoot()
					.getLocationURI().getPath();

			File ccmFile = new File(workspaceLocation + "."
					+ file.getFullPath());

			/* Probably try an absolute path as well. */
			if (!ccmFile.exists())
				ccmFile = new File("." + file.getFullPath());
			// no else.

			if (ccmFile.exists()) {

				/* Try to create an URI. */
				try {
					URI ccmURI = URI.createFileURI(ccmFile.getAbsolutePath());

					/* Get the resource. */
					ResourceSet resourceSet = new ResourceSetImpl();
					//Resource resource = resourceSet.createResource(ccmURI, null);
					Resource resource = resourceSet.getResource(ccmURI, true);
					//EcoreUtil.resolveAll(resource);
					
					
					if(!resource.isLoaded()){
						try {
							resource.load(null);
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (resource.getContents().size() > 0) {
						EObject contents = resource.getContents().get(0);

						if (contents instanceof VariantModel) {
							return (VariantModel) contents;
						}
					}

					else {
						System.err.println("The Resource '"
								+ file.getFullPath() + "' is empty.");
					}
				}

				catch (IllegalArgumentException e) {
					System.err.println("The URI '" + file.getFullPath()
							+ "' is invalid.");
				}
			}

			else {
				System.err.println("The file '" + file.getFullPath()
						+ "' cannot be found.");
			}
		}
		
		return null;
	}
	
}
