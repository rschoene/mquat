/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * 
 */
package org.coolsoftware.reconfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;

/**
 * Initial Implementation to derive a reconfiguration script from two different
 * software architectures. The reconfiguration planner is responsible to compare
 * a source and target configuration consisting of {@link SWComponent}s in order
 * to derive a {@link ReconfigurationPlan} to be executed by THEATRE at runtime.
 * 
 * The usage of the Planner as follows: Firstly, knowledge regarding which
 * {@link SWComponent} in the current configuration is mapped to which component
 * in the target configuration. This is achieved by using methods
 * {@link #extractSourceTargetMappings(SWComponent, SWComponent)} for single
 * components and {@link #extractSourceTargetMappings(List, List)} for multiple
 * components.
 * 
 * Secondly, this mapping can be used as input for the
 * {@link #deriveReconfigurationPlan(Map)} in order to extract a
 * {@link ReconfigurationPlan} for a source and a target model.
 * 
 * @author Sebastian Cech
 * 
 */
public class ReconfigurationPlanner {
	
	//TODO decide whether to keep or to remove this class

	/**
	 * Compares a source and a target {@link SWComponent} based on its type and
	 * creates a {@link Map} with the source {@link SWComponent} as key and the
	 * target {@link SWComponent} as value. Subcomponents are also considered.
	 * 
	 * This is a helper method encapsulate the source and the target component
	 * in a {@link List} and invkoes the method.
	 * {@link #extractSourceTargetMappings(List, List)}.
	 * 
	 * @param source
	 *            The {@link SWComponent} in the source architecture
	 * @param target
	 *            The {@link SWComponent} in the target architecture
	 * @return {@link Map}<source, target> which identifies mappings between the
	 *         source and the target architecture.
	 */
	public static Map<SWComponent, SWComponent> extractSourceTargetMappings(SWComponent source, SWComponent target) {
		ArrayList<SWComponent> lSource = new ArrayList<SWComponent>();
		ArrayList<SWComponent> lTarget = new ArrayList<SWComponent>();

		lSource.add(source);
		lTarget.add(target);
		return extractSourceTargetMappings(lSource, lTarget);
	}

	/**
	 * Compares a source and a target {@link List} of {@link SWComponent} based
	 * on its type and creates a {@link Map} with the source {@link SWComponent}
	 * as key and the target {@link SWComponent} as value. Subcomponents are
	 * also considered.
	 * 
	 * @param source
	 *            The {@link SWComponent} in the source architecture
	 * @param target
	 *            The {@link SWComponent} in the target architecture
	 * @return {@link Map}<source, target> which identifies mappings between the
	 *         source and the target architecture.
	 */
	public static Map<SWComponent, SWComponent> extractSourceTargetMappings(List<SWComponent> source,
			List<SWComponent> target) {
		HashMap<SWComponent, SWComponent> stmapping = new HashMap<SWComponent, SWComponent>();
//		// Cache for resolved and unresolved types
//		Set<ComponentType> unresolved = new HashSet<ComponentType>();
//		Set<String> resolved = new HashSet<String>();
//		for (SWComponent sComp : source) {
//			ComponentType type = sComp.getSpecification();
//			// avoid to collect already resolved types as unresolved
//			if(!resolved.contains(type.getName()))
//				unresolved.add(type);
//			for (SWComponent tComp : target) {
//				// the same name at type level and the same structure model is
//				// required
//				if ((tComp.getSpecification().getName().equals(type.getName()))
//						&& tComp.getSpecification().eResource().getURI().equals(type.eResource().getURI())) {
//					// && sComp.getName().equals(tComp.getName())) { //NOTE: src
//					// and target variant models will always contain all impls
//					// (reflecting whether they are deployed or not)
//					// in cases where source and target component have
//					// deployment information a correct mapping was found
//					StringBuffer buf = new StringBuffer();
//					buf.append("Source: " + sComp.getName() + " -> Target: " + tComp.getName());
//					if (sComp.getDeployedOn() != null && tComp.getDeployedOn() != null) {
//						stmapping.put(sComp, tComp);
//						unresolved.remove(type);
//						// store that the type was already resolved
//						resolved.add(type.getName());
//						buf.append(" added with deploymentInfo " + sComp.getDeployedOn().getName() + " -> "
//								+ tComp.getDeployedOn().getName());
//					}
//					System.out.println(buf.toString());
//					// handle sub models
//					stmapping.putAll(extractSourceTargetMappings(sComp.getSubtypes(), tComp.getSubtypes()));
//					
//				} // no else
//			}
//		}
//		// create mapping for deployments
//		for (ComponentType type : unresolved) {
//			//find the target component to be deployed
//			for (SWComponent tgtComp : target) {
//				if(tgtComp.getSpecification().getName().equals(type.getName()) && tgtComp.getDeployedOn()!=null){
//					// search the undeployed source component with the same name
//					for (SWComponent srcComp : source) {
//						if(srcComp.getName().equalsIgnoreCase(tgtComp.getName()) && srcComp.getDeployedOn()== null){
//							stmapping.put(srcComp, tgtComp);
//						}
//					}
//				}
//			}
//		}
//		// create mappings for undeployments
//		for (ComponentType type : unresolved) {
//			//find the target component to be undeployed
//			for (SWComponent srcComp : source) {
//				if(srcComp.getSpecification().getName().equals(type.getName()) && srcComp.getDeployedOn()!=null){
//					// search the undeployed source component with the same name
//					for (SWComponent tgtComp : target) {
//						if(tgtComp.getName().equalsIgnoreCase(srcComp.getName()) && tgtComp.getDeployedOn()== null){
//							stmapping.put(srcComp, tgtComp);
//						}
//					}
//				}
//			}
//		}
		return stmapping;
	}

	/**
	 * Derives a {@link ReconfigurationActivity} for a {@link SWComponentType}
	 * that is responsible to switch between two architectures with the same
	 * structural model. In case of a local replacement one component variant is
	 * exchanged by another variant on the same {@link ContainerProvider}.
	 * 
	 * @param mapping
	 *            The mapping between source and target {@link SWComponent}s.
	 *            The key is the source {@link SWComponent} whereas the value is
	 *            the target component. The assumption is that each key - value
	 *            pair has the same type.
	 * 
	 * @return A list of {@link ReconfigurationActivity} element for each
	 *         {@link SWComponentType} or an empty list in cases where no
	 *         changes are required.
	 */
	public static ReconfigurationPlan deriveReconfigurationPlan(Map<SWComponent, SWComponent> mapping) {
		ReconfigurationPlan plan = ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
//		Set<SWComponent> source = mapping.keySet();
//		for (SWComponent srcComp : source) {
//			SWComponent targetComp = mapping.get(srcComp);
//			if (srcComp.getDeployedOn() != null && targetComp.getDeployedOn() != null) {
//				// check remote reconfiguration
//				// the name of source and target are different, the name of
//				// container providers are different
//				if ((!srcComp.getName().equals(targetComp.getName()))
//						&& (!srcComp.getDeployedOn().getName().equals(targetComp.getDeployedOn().getName()))) {
//					plan.getActivities().add(createRemoteReplacement(srcComp, targetComp));
//				}
//				// check local reconfiguration
//				// the name of source and target are different, the name of
//				// container providers are equal
//				else if (!srcComp.getName().equals(targetComp.getName())
//						&& srcComp.getDeployedOn().getName().equals(targetComp.getDeployedOn().getName())) {
//					plan.getActivities().add(createLocalReplacement(srcComp, targetComp));
//				}
//				// check migration
//				// the name of source and target are equal, the name of
//				// container providers are different
//				else if (srcComp.getName().equals(targetComp.getName())
//						&& !srcComp.getDeployedOn().getName().equals(targetComp.getDeployedOn().getName())) {
//					plan.getActivities().add(createMigration(srcComp, targetComp));
//				} // no else
//			}
//			// check initial deployment
//			if (srcComp.getDeployedOn() == null && targetComp.getDeployedOn() != null) {
//				plan.getActivities().add(createDeployment(targetComp));
//			} // no else
//				// check undeployment
//			if (srcComp.getDeployedOn() != null && targetComp.getDeployedOn() == null) {
//				plan.getActivities().add(createUnDeployment(srcComp));
//			}
//		}
		return plan;
	}

//	/**
//	 * Creates a {@link ReconfigurationActivity} for a {@link SWComponentType}
//	 * that consist of a {@link UnDeployment}, i.e. a component variant is
//	 * removed on on a {@link ContainerProvider}.
//	 * 
//	 * @param source
//	 *            The source component *
//	 * @return A {@link ReconfigurationActivity} element for a specific
//	 *         {@link SWComponentType}.
//	 */
//	private static ReconfigurationActivity createUnDeployment(SWComponent source) {
//		ReconfigurationActivity act = ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
//		UnDeployment deployment = ReconfigurationFactory.eINSTANCE.createUnDeployment();
//		act.setComponentType(source.getSpecification().getName());
//		deployment.setSourceComponent(source.getName());
//		deployment.setSourceContainer(source.getDeployedOn().getName());
//		act.getSteps().add(deployment);
//		return act;
//	}
//
//	/**
//	 * Derives a {@link ReconfigurationActivity} for a {@link SWComponentType}
//	 * that consist of a {@link DistributedReplacement}, i.e. a component
//	 * variant is by another variant on another {@link ContainerProvider}.
//	 * 
//	 * @param source
//	 *            The source component
//	 * @param target
//	 *            The target component
//	 * 
//	 * @return A {@link ReconfigurationActivity} element for a specific
//	 *         {@link SWComponentType}.
//	 */
//	private static ReconfigurationActivity createRemoteReplacement(SWComponent source, SWComponent target) {
//		ReconfigurationActivity act = ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
//		DistributedReplacement replace = ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
//		act.setComponentType(source.getSpecification().getName());
//		replace.setSourceComponent(source.getName());
//		replace.setSourceContainer(source.getDeployedOn().getName());
//		replace.setTargetComponent(target.getName());
//		replace.setTargetContainer(target.getDeployedOn().getName());
//		act.getSteps().add(replace);
//		return act;
//	}
//
//	/**
//	 * Derives a {@link ReconfigurationActivity} for a {@link SWComponentType}
//	 * that consist of a {@link LocalReplacement}, i.e. a component variant is
//	 * replaced by another variant on the same {@link ContainerProvider}.
//	 * 
//	 * @param source
//	 *            The source component
//	 * @param target
//	 *            The target component
//	 * 
//	 * @return A {@link ReconfigurationActivity} element for a specific
//	 *         {@link SWComponentType}.
//	 */
//	private static ReconfigurationActivity createLocalReplacement(SWComponent source, SWComponent target) {
//		ReconfigurationActivity act = ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
//		LocalReplacement replace = ReconfigurationFactory.eINSTANCE.createLocalReplacement();
//		act.setComponentType(source.getSpecification().getName());
//		replace.setSourceComponent(source.getName());
//		replace.setSourceContainer(source.getDeployedOn().getName());
//		replace.setTargetComponent(target.getName());
//		act.getSteps().add(replace);
//		return act;
//	}
//
//	/**
//	 * Derives a {@link ReconfigurationActivity} for a {@link SWComponentType}
//	 * that consist of a {@link Migration}, i.e. a component variant is moved to
//	 * another {@link ContainerProvider}.
//	 * 
//	 * @param source
//	 *            The source component
//	 * @param target
//	 *            The target component
//	 * 
//	 * @return A {@link ReconfigurationActivity} element for a specific
//	 *         {@link SWComponentType}.
//	 */
//	private static ReconfigurationActivity createMigration(SWComponent source, SWComponent target) {
//		ReconfigurationActivity act = ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
//		Migration migrate = ReconfigurationFactory.eINSTANCE.createMigration();
//		act.setComponentType(source.getSpecification().getName());
//		migrate.setSourceComponent(source.getName());
//		migrate.setSourceContainer(source.getDeployedOn().getName());
//		migrate.setTargetContainer(target.getDeployedOn().getName());
//		act.getSteps().add(migrate);
//		return act;
//	}
//
//	/**
//	 * Creates a {@link ReconfigurationActivity} for a {@link SWComponentType}
//	 * that consist of a {@link Deployment}, i.e. a component variant is
//	 * deployed on on a {@link ContainerProvider}.
//	 * 
//	 * @param source
//	 *            The source component
//	 * @param target
//	 *            The target component
//	 * 
//	 * @return A {@link ReconfigurationActivity} element for a specific
//	 *         {@link SWComponentType}.
//	 */
//	private static ReconfigurationActivity createDeployment(SWComponent target) {
//		ReconfigurationActivity act = ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
//		Deployment deployment = ReconfigurationFactory.eINSTANCE.createDeployment();
//		act.setComponentType(target.getSpecification().getName());
//		deployment.setSourceComponent(target.getName());
//		deployment.setTargetContainer(target.getDeployedOn().getName());
//		act.getSteps().add(deployment);
//		return act;
//	}

}
