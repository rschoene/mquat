/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.util;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractConfigInterpreter<ResultType, ContextType> {
	
	private java.util.Stack<org.eclipse.emf.ecore.EObject> interpretationStack = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	private java.util.List<org.coolsoftware.reconfiguration.resource.config.IConfigInterpreterListener> listeners = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.IConfigInterpreterListener>();
	private org.eclipse.emf.ecore.EObject nextObjectToInterprete;
	private Object currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		org.eclipse.emf.ecore.EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (java.util.EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(org.eclipse.emf.ecore.EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof org.coolsoftware.reconfiguration.ReconfigurationPlan) {
			result = interprete_org_coolsoftware_reconfiguration_ReconfigurationPlan((org.coolsoftware.reconfiguration.ReconfigurationPlan) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.reconfiguration.LocalReplacement) {
			result = interprete_org_coolsoftware_reconfiguration_LocalReplacement((org.coolsoftware.reconfiguration.LocalReplacement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.reconfiguration.Migration) {
			result = interprete_org_coolsoftware_reconfiguration_Migration((org.coolsoftware.reconfiguration.Migration) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.reconfiguration.Deployment) {
			result = interprete_org_coolsoftware_reconfiguration_Deployment((org.coolsoftware.reconfiguration.Deployment) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.reconfiguration.ReconfigurationStep) {
			result = interprete_org_coolsoftware_reconfiguration_ReconfigurationStep((org.coolsoftware.reconfiguration.ReconfigurationStep) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.reconfiguration.ReconfigurationActivity) {
			result = interprete_org_coolsoftware_reconfiguration_ReconfigurationActivity((org.coolsoftware.reconfiguration.ReconfigurationActivity) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.reconfiguration.DistributedReplacement) {
			result = interprete_org_coolsoftware_reconfiguration_DistributedReplacement((org.coolsoftware.reconfiguration.DistributedReplacement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.reconfiguration.UnDeployment) {
			result = interprete_org_coolsoftware_reconfiguration_UnDeployment((org.coolsoftware.reconfiguration.UnDeployment) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_ReconfigurationPlan(org.coolsoftware.reconfiguration.ReconfigurationPlan reconfigurationPlan, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_ReconfigurationStep(org.coolsoftware.reconfiguration.ReconfigurationStep reconfigurationStep, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_LocalReplacement(org.coolsoftware.reconfiguration.LocalReplacement localReplacement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_Migration(org.coolsoftware.reconfiguration.Migration migration, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_ReconfigurationActivity(org.coolsoftware.reconfiguration.ReconfigurationActivity reconfigurationActivity, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_DistributedReplacement(org.coolsoftware.reconfiguration.DistributedReplacement distributedReplacement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_Deployment(org.coolsoftware.reconfiguration.Deployment deployment, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_reconfiguration_UnDeployment(org.coolsoftware.reconfiguration.UnDeployment unDeployment, ContextType context) {
		return null;
	}
	
	private void notifyListeners(org.eclipse.emf.ecore.EObject element) {
		for (org.coolsoftware.reconfiguration.resource.config.IConfigInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(org.eclipse.emf.ecore.EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		for (org.eclipse.emf.ecore.EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		java.util.List<org.eclipse.emf.ecore.EObject> reverse = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>(objects.size());
		reverse.addAll(objects);
		java.util.Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(org.eclipse.emf.ecore.EObject root) {
		java.util.List<org.eclipse.emf.ecore.EObject> objects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
		objects.add(root);
		java.util.Iterator<org.eclipse.emf.ecore.EObject> it = root.eAllContents();
		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(org.coolsoftware.reconfiguration.resource.config.IConfigInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(org.coolsoftware.reconfiguration.resource.config.IConfigInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public org.eclipse.emf.ecore.EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public java.util.Stack<org.eclipse.emf.ecore.EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public Object getCurrentContext() {
		return currentContext;
	}
	
}
