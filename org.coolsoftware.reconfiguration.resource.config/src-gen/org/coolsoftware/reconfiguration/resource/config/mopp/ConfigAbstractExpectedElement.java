/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class ConfigAbstractExpectedElement implements org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	
	private java.util.Set<org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]>> followers = new java.util.LinkedHashSet<org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]>>();
	
	public ConfigAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement follower, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] path) {
		followers.add(new org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]>(follower, path));
	}
	
	public java.util.Collection<org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
