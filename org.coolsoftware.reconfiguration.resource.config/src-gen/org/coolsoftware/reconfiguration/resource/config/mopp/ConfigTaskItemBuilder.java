/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

/**
 * The ConfigTaskItemBuilder is used to find task items in text documents. The
 * current implementation uses the generated lexer and the TaskItemDetector to
 * detect task items.
 */
public class ConfigTaskItemBuilder extends org.coolsoftware.reconfiguration.resource.config.mopp.ConfigBuilderAdapter {
	
	/**
	 * The ID of the item task builder.
	 */
	public final static String BUILDER_ID = "org.coolsoftware.reconfiguration.resource.config.taskItemBuilder";
	
	@Override	
	public void build(org.eclipse.core.resources.IFile resource, org.eclipse.core.runtime.IProgressMonitor monitor) {
		monitor.setTaskName("Searching for task items");
		new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper().removeAllMarkers(resource, org.eclipse.core.resources.IMarker.TASK);
		if (isInBinFolder(resource)) {
			return;
		}
		java.util.List<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTaskItem> taskItems = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTaskItem>();
		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTaskItemDetector taskItemDetector = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTaskItemDetector();
		try {
			java.io.InputStream inputStream = resource.getContents();
			String content = org.coolsoftware.reconfiguration.resource.config.util.ConfigStreamUtil.getContent(inputStream);
			org.coolsoftware.reconfiguration.resource.config.IConfigTextScanner lexer = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMetaInformation().createLexer();
			lexer.setText(content);
			
			org.coolsoftware.reconfiguration.resource.config.IConfigTextToken nextToken = lexer.getNextToken();
			while (nextToken != null) {
				String text = nextToken.getText();
				taskItems.addAll(taskItemDetector.findTaskItems(text, nextToken.getLine(), nextToken.getOffset()));
				nextToken = lexer.getNextToken();
			}
		} catch (java.io.IOException e) {
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigPlugin.logError("Exception while searching for task items", e);
		} catch (org.eclipse.core.runtime.CoreException e) {
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigPlugin.logError("Exception while searching for task items", e);
		}
		
		for (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTaskItem taskItem : taskItems) {
			java.util.Map<String, Object> markerAttributes = new java.util.LinkedHashMap<String, Object>();
			markerAttributes.put(org.eclipse.core.resources.IMarker.USER_EDITABLE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.DONE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.LINE_NUMBER, taskItem.getLine());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_START, taskItem.getCharStart());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_END, taskItem.getCharEnd());
			markerAttributes.put(org.eclipse.core.resources.IMarker.MESSAGE, taskItem.getMessage());
			new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper().createMarker(resource, org.eclipse.core.resources.IMarker.TASK, markerAttributes);
		}
	}
	
	public String getBuilderMarkerId() {
		return org.eclipse.core.resources.IMarker.TASK;
	}
	
	public boolean isInBinFolder(org.eclipse.core.resources.IFile resource) {
		org.eclipse.core.resources.IContainer parent = resource.getParent();
		while (parent != null) {
			if ("bin".equals(parent.getName())) {
				return true;
			}
			parent = parent.getParent();
		}
		return false;
	}
	
}
