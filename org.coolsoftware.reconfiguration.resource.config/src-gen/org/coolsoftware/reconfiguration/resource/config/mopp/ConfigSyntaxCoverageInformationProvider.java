/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

public class ConfigSyntaxCoverageInformationProvider {
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.eclipse.emf.ecore.EClass[] {
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationPlan(),
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity(),
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(),
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(),
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(),
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(),
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(),
		};
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.eclipse.emf.ecore.EClass[] {
			org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationPlan(),
		};
	}
	
}
