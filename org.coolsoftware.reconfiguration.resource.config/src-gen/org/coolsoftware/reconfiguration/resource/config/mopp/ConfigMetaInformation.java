/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

public class ConfigMetaInformation implements org.coolsoftware.reconfiguration.resource.config.IConfigMetaInformation {
	
	public String getSyntaxName() {
		return "config";
	}
	
	public String getURI() {
		return "http://cool-software.org/reconfiguration";
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTextScanner createLexer() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigAntlrScanner(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigLexer());
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigParser().createInstance(inputStream, encoding);
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTextPrinter createPrinter(java.io.OutputStream outputStream, org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolverSwitch getReferenceResolverSwitch() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigReferenceResolverSwitch();
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolverFactory getTokenResolverFactory() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "org.coolsoftware.reconfiguration/model/reconfiguration.cs";
	}
	
	public String[] getTokenNames() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigParser(null).getTokenNames();
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTokenStyle getDefaultTokenStyle(String tokenName) {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigBracketPair> getBracketPairs() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigResourceFactory();
	}
	
	public org.coolsoftware.reconfiguration.resource.config.mopp.ConfigNewFileContentProvider getNewFileContentProvider() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "org.coolsoftware.reconfiguration.resource.config.ui.launchConfigurationType";
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigNameProvider createNameProvider() {
		return new org.coolsoftware.reconfiguration.resource.config.analysis.ConfigDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigAntlrTokenHelper tokenHelper = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
