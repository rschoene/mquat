/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

/**
 * The ConfigTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class ConfigTokenResolverFactory implements org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolverFactory {
	
	private java.util.Map<String, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver> featureName2CollectInTokenResolver;
	private static org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver defaultResolver = new org.coolsoftware.reconfiguration.resource.config.analysis.ConfigDefaultTokenResolver();
	
	public ConfigTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver>();
		registerCollectInTokenResolver("comments", new org.coolsoftware.reconfiguration.resource.config.analysis.ConfigCOLLECT_commentsTokenResolver());
		registerCollectInTokenResolver("comments", new org.coolsoftware.reconfiguration.resource.config.analysis.ConfigCOLLECT_commentsTokenResolver());
		registerTokenResolver("TEXT", new org.coolsoftware.reconfiguration.resource.config.analysis.ConfigTEXTTokenResolver());
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver internalCreateResolver(java.util.Map<String, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver> resolverMap, String key, org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
