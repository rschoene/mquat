/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

public class ConfigTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("ML_COMMENT".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x00, 0x80, 0x00}, null, false, true, false, false);
		}
		if ("SL_COMMENT".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x00, 0x80, 0x00}, null, false, true, false, false);
		}
		if ("reconfigurationPlan".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("reconfiguration".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("for".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("SWComponentType".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("replace".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("SWComponent".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("on".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("with".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("migrate".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("from".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("to".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("deploy".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("undeploy".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("TASK_ITEM".equals(tokenName)) {
			return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		return null;
	}
	
}
