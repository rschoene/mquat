grammar Config;

options {
	superClass = ConfigANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package org.coolsoftware.reconfiguration.resource.config.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package org.coolsoftware.reconfiguration.resource.config.mopp;
}

@members{
	private org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolverFactory tokenResolverFactory = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new org.coolsoftware.reconfiguration.resource.config.IConfigProblem() {
					public org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity getSeverity() {
						return org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.ERROR;
					}
					public org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType getType() {
						return org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement terminal = org.coolsoftware.reconfiguration.resource.config.grammar.ConfigFollowSetProvider.TERMINALS[terminalID];
		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] containmentTrace = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentTrace[i - 2] = org.coolsoftware.reconfiguration.resource.config.grammar.ConfigFollowSetProvider.LINKS[ids[i]];
		}
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal expectedElement = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
		int currentPos = getTokenStream().index();
		if (currentPos == 0) {
			return;
		}
		int endPos = currentPos - 1;
		for (; endPos >= this.lastPosition; endPos--) {
			org.antlr.runtime3_4_0.Token token = getTokenStream().get(endPos);
			int _channel = token.getChannel();
			if (_channel != 99) {
				break;
			}
		}
		for (int pos = this.lastPosition; pos < endPos; pos++) {
			org.antlr.runtime3_4_0.Token token = getTokenStream().get(pos);
			int _channel = token.getChannel();
			if (_channel == 99) {
				if (token.getType() == ConfigLexer.SL_COMMENT) {
					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
					if (feature != null) {
						// call token resolver
						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
						resolvedResolver.setOptions(getOptions());
						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult resolvedResult = getFreshTokenResolveResult();
						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
						Object resolvedObject = resolvedResult.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
						}
						if (java.lang.String.class.isInstance(resolvedObject)) {
							addObjectToList(element, feature, resolvedObject);
						} else {
							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
						}
					} else {
						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
					}
				}
				if (token.getType() == ConfigLexer.ML_COMMENT) {
					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
					if (feature != null) {
						// call token resolver
						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
						resolvedResolver.setOptions(getOptions());
						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult resolvedResult = getFreshTokenResolveResult();
						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
						Object resolvedObject = resolvedResult.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
						}
						if (java.lang.String.class.isInstance(resolvedObject)) {
							addObjectToList(element, feature, resolvedObject);
						} else {
							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
						}
					} else {
						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
					}
				}
			}
		}
		this.lastPosition = (endPos < 0 ? 0 : endPos);
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
				org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
				org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
				org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new ConfigParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ConfigLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new ConfigParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ConfigLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public ConfigParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((ConfigLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((ConfigLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.ReconfigurationPlan.class) {
				return parse_org_coolsoftware_reconfiguration_ReconfigurationPlan();
			}
			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.ReconfigurationActivity.class) {
				return parse_org_coolsoftware_reconfiguration_ReconfigurationActivity();
			}
			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.LocalReplacement.class) {
				return parse_org_coolsoftware_reconfiguration_LocalReplacement();
			}
			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.DistributedReplacement.class) {
				return parse_org_coolsoftware_reconfiguration_DistributedReplacement();
			}
			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.Migration.class) {
				return parse_org_coolsoftware_reconfiguration_Migration();
			}
			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.Deployment.class) {
				return parse_org_coolsoftware_reconfiguration_Deployment();
			}
			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.UnDeployment.class) {
				return parse_org_coolsoftware_reconfiguration_UnDeployment();
			}
		}
		throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(org.coolsoftware.reconfiguration.resource.config.IConfigOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public org.coolsoftware.reconfiguration.resource.config.IConfigParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>>();
		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigParseResult parseResult = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.reconfiguration.resource.config.IConfigTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		org.coolsoftware.reconfiguration.resource.config.IConfigParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal>();
		java.util.List<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 54;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]> newFollowerPair : newFollowers) {
							org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal newFollowTerminal = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal(container, newFollower, followSetID, newFollowerPair.getRight());
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[0]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_org_coolsoftware_reconfiguration_ReconfigurationPlan{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_org_coolsoftware_reconfiguration_ReconfigurationPlan returns [org.coolsoftware.reconfiguration.ReconfigurationPlan element = null]
@init{
}
:
	a0 = 'reconfigurationPlan' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[1]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[2]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[3]);
	}
	
	(
		(
			(
				a2_0 = parse_org_coolsoftware_reconfiguration_ReconfigurationActivity				{
					if (terminateParsing) {
						throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
						startIncompleteElement(element);
					}
					if (a2_0 != null) {
						if (a2_0 != null) {
							Object value = a2_0;
							addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_PLAN__ACTIVITIES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_2_0_0_1, a2_0, true);
						copyLocalizationInfos(a2_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[4]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[5]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[6]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[7]);
	}
	
	a3 = '}' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
	}
	
;

parse_org_coolsoftware_reconfiguration_ReconfigurationActivity returns [org.coolsoftware.reconfiguration.ReconfigurationActivity element = null]
@init{
}
:
	a0 = 'reconfiguration' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[8]);
	}
	
	a1 = 'for' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[9]);
	}
	
	a2 = 'SWComponentType' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[10]);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_6, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[11]);
	}
	
	a4 = '{' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_8, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[12]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[13]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[14]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[15]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[16]);
	}
	
	(
		(
			(
				a5_0 = parse_org_coolsoftware_reconfiguration_ReconfigurationStep				{
					if (terminateParsing) {
						throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
						startIncompleteElement(element);
					}
					if (a5_0 != null) {
						if (a5_0 != null) {
							Object value = a5_0;
							addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_9_0_0_1, a5_0, true);
						copyLocalizationInfos(a5_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[17]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[18]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[19]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[20]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[21]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[22]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[23]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[24]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[25]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[26]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[27]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[28]);
	}
	
	a6 = '}' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[29]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[30]);
	}
	
;

parse_org_coolsoftware_reconfiguration_LocalReplacement returns [org.coolsoftware.reconfiguration.LocalReplacement element = null]
@init{
}
:
	a0 = 'replace' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[31]);
	}
	
	a1 = 'SWComponent' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[32]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[33]);
	}
	
	a3 = 'on' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[34]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_CONTAINER), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_CONTAINER), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_8, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[35]);
	}
	
	a5 = 'with' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[36]);
	}
	
	(
		a6 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
				startIncompleteElement(element);
			}
			if (a6 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_12, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[37]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[38]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[39]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[40]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[41]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[42]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[43]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[44]);
	}
	
	(
		(
			a7 = '{' {
				if (element == null) {
					element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[45]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[46]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[47]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[48]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[49]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[50]);
			}
			
			(
				(
					a8_0 = parse_org_coolsoftware_reconfiguration_ReconfigurationStep					{
						if (terminateParsing) {
							throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
						}
						if (element == null) {
							element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
							startIncompleteElement(element);
						}
						if (a8_0 != null) {
							if (a8_0 != null) {
								Object value = a8_0;
								addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SUB_STEP, value);
								completedElement(value, true);
							}
							collectHiddenTokens(element);
							retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_1_0_0_0, a8_0, true);
							copyLocalizationInfos(a8_0, element);
						}
					}
				)
				{
					// expected elements (follow set)
					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[51]);
				}
				
				
				|				(
					(
						(
							a9_0 = parse_org_coolsoftware_reconfiguration_ReconfigurationStep							{
								if (terminateParsing) {
									throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
								}
								if (element == null) {
									element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
									startIncompleteElement(element);
								}
								if (a9_0 != null) {
									if (a9_0 != null) {
										Object value = a9_0;
										addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SUB_STEP, value);
										completedElement(value, true);
									}
									collectHiddenTokens(element);
									retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_1_0_1_0_0_0_0, a9_0, true);
									copyLocalizationInfos(a9_0, element);
								}
							}
						)
						{
							// expected elements (follow set)
							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[52]);
							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[53]);
							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[54]);
							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[55]);
							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[56]);
							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[57]);
						}
						
					)
					
				)*				{
					// expected elements (follow set)
					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[58]);
					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[59]);
					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[60]);
					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[61]);
					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[62]);
					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[63]);
				}
				
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[64]);
			}
			
			a10 = '}' {
				if (element == null) {
					element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[65]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[66]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[67]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[68]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[69]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[70]);
				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[71]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[72]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[73]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[74]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[75]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[76]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[77]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[78]);
	}
	
	|//derived choice rules for sub-classes: 
	
	c0 = parse_org_coolsoftware_reconfiguration_DistributedReplacement{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parse_org_coolsoftware_reconfiguration_DistributedReplacement returns [org.coolsoftware.reconfiguration.DistributedReplacement element = null]
@init{
}
:
	a0 = 'replace' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[79]);
	}
	
	a1 = 'SWComponent' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[80]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[81]);
	}
	
	a3 = 'on' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[82]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_8, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[83]);
	}
	
	a5 = 'with' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[84]);
	}
	
	(
		a6 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
				startIncompleteElement(element);
			}
			if (a6 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_12, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[85]);
	}
	
	a7 = 'on' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_14, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[86]);
	}
	
	(
		a8 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
				startIncompleteElement(element);
			}
			if (a8 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_15, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[87]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[88]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[89]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[90]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[91]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[92]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[93]);
	}
	
;

parse_org_coolsoftware_reconfiguration_Migration returns [org.coolsoftware.reconfiguration.Migration element = null]
@init{
}
:
	a0 = 'migrate' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[94]);
	}
	
	a1 = 'SWComponent' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[95]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[96]);
	}
	
	a3 = 'from' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[97]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_CONTAINER), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_CONTAINER), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_8, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[98]);
	}
	
	a5 = 'to' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[99]);
	}
	
	(
		a6 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
				startIncompleteElement(element);
			}
			if (a6 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__TARGET_CONTAINER), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__TARGET_CONTAINER), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_12, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[100]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[101]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[102]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[103]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[104]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[105]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[106]);
	}
	
;

parse_org_coolsoftware_reconfiguration_Deployment returns [org.coolsoftware.reconfiguration.Deployment element = null]
@init{
}
:
	a0 = 'deploy' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[107]);
	}
	
	a1 = 'SWComponent' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[108]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[109]);
	}
	
	a3 = 'on' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[110]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__TARGET_CONTAINER), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__TARGET_CONTAINER), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_8, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[111]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[112]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[113]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[114]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[115]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[116]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[117]);
	}
	
;

parse_org_coolsoftware_reconfiguration_UnDeployment returns [org.coolsoftware.reconfiguration.UnDeployment element = null]
@init{
}
:
	a0 = 'undeploy' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[118]);
	}
	
	a1 = 'SWComponent' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[119]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[120]);
	}
	
	a3 = 'from' {
		if (element == null) {
			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[121]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_CONTAINER), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_CONTAINER), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_7, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[122]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[123]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[124]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[125]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[126]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[127]);
		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[128]);
	}
	
;

parse_org_coolsoftware_reconfiguration_ReconfigurationStep returns [org.coolsoftware.reconfiguration.ReconfigurationStep element = null]
:
	c0 = parse_org_coolsoftware_reconfiguration_LocalReplacement{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_org_coolsoftware_reconfiguration_DistributedReplacement{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_org_coolsoftware_reconfiguration_Migration{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_org_coolsoftware_reconfiguration_Deployment{ element = c3; /* this is a subclass or primitive expression choice */ }
	|	c4 = parse_org_coolsoftware_reconfiguration_UnDeployment{ element = c4; /* this is a subclass or primitive expression choice */ }
	
;

SL_COMMENT:
	( '--'(~('\n'|'\r'|'\uffff'))* )
	{ _channel = 99; }
;
ML_COMMENT:
	( '/*'.*'*/')
	{ _channel = 99; }
;
FILE_NAME:
	( '[' (('A' .. 'Z' | 'a'..'z') ':' '/')? ('.' | '/') ('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' | '/' | '.' | ' ')+ ']' )
	{ _channel = 99; }
;
TEXT:
	(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
;
WHITESPACE:
	((' ' | '\t' | '\f'))
	{ _channel = 99; }
;
LINEBREAK:
	(('\r\n' | '\r' | '\n'))
	{ _channel = 99; }
;

