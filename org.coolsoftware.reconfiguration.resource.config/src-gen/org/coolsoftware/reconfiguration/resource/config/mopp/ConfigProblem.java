/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

public class ConfigProblem implements org.coolsoftware.reconfiguration.resource.config.IConfigProblem {
	
	private String message;
	private org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType type;
	private org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity severity;
	private java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix> quickFixes;
	
	public ConfigProblem(String message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType type, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix>emptySet());
	}
	
	public ConfigProblem(String message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType type, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity severity, org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public ConfigProblem(String message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType type, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity severity, java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType getType() {
		return type;
	}
	
	public org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
