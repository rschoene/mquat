/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

public class ConfigPrinter implements org.coolsoftware.reconfiguration.resource.config.IConfigTextPrinter {
	
	protected org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolverFactory tokenResolverFactory = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenResolverFactory();
	
	protected java.io.OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource;
	
	private java.util.Map<?, ?> options;
	
	public ConfigPrinter(java.io.OutputStream outputStream, org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(java.util.Map<String, Integer> featureCounter, java.util.Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.io.PrintWriter out, String globaltab) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof org.coolsoftware.reconfiguration.ReconfigurationPlan) {
			print_org_coolsoftware_reconfiguration_ReconfigurationPlan((org.coolsoftware.reconfiguration.ReconfigurationPlan) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.reconfiguration.ReconfigurationActivity) {
			print_org_coolsoftware_reconfiguration_ReconfigurationActivity((org.coolsoftware.reconfiguration.ReconfigurationActivity) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.reconfiguration.DistributedReplacement) {
			print_org_coolsoftware_reconfiguration_DistributedReplacement((org.coolsoftware.reconfiguration.DistributedReplacement) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.reconfiguration.Migration) {
			print_org_coolsoftware_reconfiguration_Migration((org.coolsoftware.reconfiguration.Migration) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.reconfiguration.Deployment) {
			print_org_coolsoftware_reconfiguration_Deployment((org.coolsoftware.reconfiguration.Deployment) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.reconfiguration.UnDeployment) {
			print_org_coolsoftware_reconfiguration_UnDeployment((org.coolsoftware.reconfiguration.UnDeployment) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.reconfiguration.LocalReplacement) {
			print_org_coolsoftware_reconfiguration_LocalReplacement((org.coolsoftware.reconfiguration.LocalReplacement) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected org.coolsoftware.reconfiguration.resource.config.mopp.ConfigReferenceResolverSwitch getReferenceResolverSwitch() {
		return (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigReferenceResolverSwitch) new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigProblem(errorMessage, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.PRINT_PROBLEM, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(org.eclipse.emf.ecore.EObject element) {
		java.io.PrintWriter out = new java.io.PrintWriter(new java.io.BufferedOutputStream(outputStream));
		doPrint(element, out, "");
		out.flush();
		out.close();
	}
	
	public void print_org_coolsoftware_reconfiguration_ReconfigurationPlan(org.coolsoftware.reconfiguration.ReconfigurationPlan element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_PLAN__ACTIVITIES));
		printCountingMap.put("activities", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("reconfigurationPlan");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_coolsoftware_reconfiguration_ReconfigurationPlan_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_org_coolsoftware_reconfiguration_ReconfigurationPlan_0(org.coolsoftware.reconfiguration.ReconfigurationPlan element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (LineBreak)
		localtab += "	";
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("activities");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_PLAN__ACTIVITIES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("activities", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_org_coolsoftware_reconfiguration_ReconfigurationActivity(org.coolsoftware.reconfiguration.ReconfigurationActivity element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS));
		printCountingMap.put("steps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE));
		printCountingMap.put("componentType", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("reconfiguration");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("for");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("SWComponentType");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("componentType");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE), element));
			}
			printCountingMap.put("componentType", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		print_org_coolsoftware_reconfiguration_ReconfigurationActivity_0(element, localtab, out, printCountingMap);
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_coolsoftware_reconfiguration_ReconfigurationActivity_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_org_coolsoftware_reconfiguration_ReconfigurationActivity_0(org.coolsoftware.reconfiguration.ReconfigurationActivity element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (LineBreak)
		localtab += "	";
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("steps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("steps", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_org_coolsoftware_reconfiguration_LocalReplacement(org.coolsoftware.reconfiguration.LocalReplacement element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SUB_STEP));
		printCountingMap.put("subStep", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_COMPONENT));
		printCountingMap.put("sourceComponent", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_CONTAINER));
		printCountingMap.put("sourceContainer", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT));
		printCountingMap.put("targetComponent", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("replace");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("SWComponent");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceComponent");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_COMPONENT));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_COMPONENT), element));
			}
			printCountingMap.put("sourceComponent", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("on");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceContainer");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_CONTAINER));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_CONTAINER), element));
			}
			printCountingMap.put("sourceContainer", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("with");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("targetComponent");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT), element));
				out.print(" ");
			}
			printCountingMap.put("targetComponent", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_coolsoftware_reconfiguration_LocalReplacement_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_org_coolsoftware_reconfiguration_LocalReplacement_0(org.coolsoftware.reconfiguration.LocalReplacement element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		print_org_coolsoftware_reconfiguration_LocalReplacement_0_0(element, localtab, out, printCountingMap);
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_org_coolsoftware_reconfiguration_LocalReplacement_0_0(org.coolsoftware.reconfiguration.LocalReplacement element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		int alt = -1;
		alt = 0;
		int matches = 		matchCount(printCountingMap, java.util.Arrays.asList(		"subStep"		));
		int tempMatchCount;
		tempMatchCount = 		0;
		if (tempMatchCount > matches) {
			alt = 1;
			matches = tempMatchCount;
		}
		switch(alt) {
			case 1:			{
				boolean iterate = true;
				java.io.StringWriter sWriter = null;
				java.io.PrintWriter out1 = null;
				java.util.Map<String, Integer> printCountingMap1 = null;
				// DEFINITION PART BEGINS (CompoundDefinition)
				iterate = true;
				while (iterate) {
					sWriter = new java.io.StringWriter();
					out1 = new java.io.PrintWriter(sWriter);
					printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
					print_org_coolsoftware_reconfiguration_LocalReplacement_0_0_0(element, localtab, out1, printCountingMap1);
					if (printCountingMap.equals(printCountingMap1)) {
						iterate = false;
						out1.close();
					} else {
						out1.flush();
						out1.close();
						out.print(sWriter.toString());
						printCountingMap.putAll(printCountingMap1);
					}
				}
			}
			break;
			default:			// DEFINITION PART BEGINS (Containment)
			count = printCountingMap.get("subStep");
			if (count > 0) {
				Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SUB_STEP));
				java.util.List<?> list = (java.util.List<?>) o;
				int index = list.size() - count;
				if (index >= 0) {
					o = list.get(index);
				} else {
					o = null;
				}
				if (o != null) {
					doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
				}
				printCountingMap.put("subStep", count - 1);
			}
		}
	}
	
	public void print_org_coolsoftware_reconfiguration_LocalReplacement_0_0_0(org.coolsoftware.reconfiguration.LocalReplacement element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("subStep");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SUB_STEP));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("subStep", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_reconfiguration_DistributedReplacement(org.coolsoftware.reconfiguration.DistributedReplacement element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(5);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SUB_STEP));
		printCountingMap.put("subStep", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT));
		printCountingMap.put("sourceComponent", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER));
		printCountingMap.put("sourceContainer", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT));
		printCountingMap.put("targetComponent", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER));
		printCountingMap.put("targetContainer", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("replace");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("SWComponent");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceComponent");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT), element));
			}
			printCountingMap.put("sourceComponent", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("on");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceContainer");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER), element));
			}
			printCountingMap.put("sourceContainer", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("with");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("targetComponent");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT), element));
			}
			printCountingMap.put("targetComponent", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("on");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("targetContainer");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER), element));
				out.print(" ");
			}
			printCountingMap.put("targetContainer", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_reconfiguration_Migration(org.coolsoftware.reconfiguration.Migration element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SUB_STEP));
		printCountingMap.put("subStep", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_COMPONENT));
		printCountingMap.put("sourceComponent", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_CONTAINER));
		printCountingMap.put("sourceContainer", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__TARGET_CONTAINER));
		printCountingMap.put("targetContainer", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("migrate");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("SWComponent");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceComponent");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_COMPONENT));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_COMPONENT), element));
			}
			printCountingMap.put("sourceComponent", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("from");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceContainer");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_CONTAINER));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_CONTAINER), element));
			}
			printCountingMap.put("sourceContainer", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("to");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("targetContainer");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__TARGET_CONTAINER));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__TARGET_CONTAINER), element));
				out.print(" ");
			}
			printCountingMap.put("targetContainer", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_reconfiguration_Deployment(org.coolsoftware.reconfiguration.Deployment element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SUB_STEP));
		printCountingMap.put("subStep", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_COMPONENT));
		printCountingMap.put("sourceComponent", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_CONTAINER));
		printCountingMap.put("sourceContainer", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__TARGET_CONTAINER));
		printCountingMap.put("targetContainer", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("deploy");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("SWComponent");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceComponent");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_COMPONENT));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_COMPONENT), element));
			}
			printCountingMap.put("sourceComponent", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("on");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("targetContainer");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__TARGET_CONTAINER));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__TARGET_CONTAINER), element));
				out.print(" ");
			}
			printCountingMap.put("targetContainer", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_reconfiguration_UnDeployment(org.coolsoftware.reconfiguration.UnDeployment element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SUB_STEP));
		printCountingMap.put("subStep", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_COMPONENT));
		printCountingMap.put("sourceComponent", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_CONTAINER));
		printCountingMap.put("sourceContainer", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("undeploy");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("SWComponent");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceComponent");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_COMPONENT));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_COMPONENT), element));
			}
			printCountingMap.put("sourceComponent", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CsString)
		out.print("from");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingDefaultToken)
		count = printCountingMap.get("sourceContainer");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_CONTAINER));
			if (o != null) {
				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_CONTAINER), element));
				out.print(" ");
			}
			printCountingMap.put("sourceContainer", count - 1);
		}
	}
	
	
}
