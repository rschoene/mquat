/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

/**
 * This class contains some constants that are used during code completion.
 */
public class ConfigExpectationConstants {
	
	public final static int EXPECTATIONS[][] = new int[129][];
	
	public static void initialize0() {
		EXPECTATIONS[0] = new int[2];
		EXPECTATIONS[0][0] = 0;
		EXPECTATIONS[0][1] = 0;
		EXPECTATIONS[1] = new int[2];
		EXPECTATIONS[1][0] = 1;
		EXPECTATIONS[1][1] = 1;
		EXPECTATIONS[2] = new int[3];
		EXPECTATIONS[2][0] = 2;
		EXPECTATIONS[2][1] = 2;
		EXPECTATIONS[2][2] = 0;
		EXPECTATIONS[3] = new int[2];
		EXPECTATIONS[3][0] = 3;
		EXPECTATIONS[3][1] = 2;
		EXPECTATIONS[4] = new int[3];
		EXPECTATIONS[4][0] = 2;
		EXPECTATIONS[4][1] = 3;
		EXPECTATIONS[4][2] = 1;
		EXPECTATIONS[5] = new int[2];
		EXPECTATIONS[5][0] = 3;
		EXPECTATIONS[5][1] = 3;
		EXPECTATIONS[6] = new int[3];
		EXPECTATIONS[6][0] = 2;
		EXPECTATIONS[6][1] = 4;
		EXPECTATIONS[6][2] = 2;
		EXPECTATIONS[7] = new int[2];
		EXPECTATIONS[7][0] = 3;
		EXPECTATIONS[7][1] = 4;
		EXPECTATIONS[8] = new int[2];
		EXPECTATIONS[8][0] = 4;
		EXPECTATIONS[8][1] = 6;
		EXPECTATIONS[9] = new int[2];
		EXPECTATIONS[9][0] = 5;
		EXPECTATIONS[9][1] = 7;
		EXPECTATIONS[10] = new int[2];
		EXPECTATIONS[10][0] = 6;
		EXPECTATIONS[10][1] = 8;
		EXPECTATIONS[11] = new int[2];
		EXPECTATIONS[11][0] = 7;
		EXPECTATIONS[11][1] = 9;
		EXPECTATIONS[12] = new int[3];
		EXPECTATIONS[12][0] = 8;
		EXPECTATIONS[12][1] = 10;
		EXPECTATIONS[12][2] = 3;
		EXPECTATIONS[13] = new int[3];
		EXPECTATIONS[13][0] = 9;
		EXPECTATIONS[13][1] = 10;
		EXPECTATIONS[13][2] = 4;
		EXPECTATIONS[14] = new int[3];
		EXPECTATIONS[14][0] = 10;
		EXPECTATIONS[14][1] = 10;
		EXPECTATIONS[14][2] = 5;
		EXPECTATIONS[15] = new int[3];
		EXPECTATIONS[15][0] = 11;
		EXPECTATIONS[15][1] = 10;
		EXPECTATIONS[15][2] = 6;
		EXPECTATIONS[16] = new int[3];
		EXPECTATIONS[16][0] = 12;
		EXPECTATIONS[16][1] = 10;
		EXPECTATIONS[16][2] = 7;
		EXPECTATIONS[17] = new int[3];
		EXPECTATIONS[17][0] = 8;
		EXPECTATIONS[17][1] = 11;
		EXPECTATIONS[17][2] = 8;
		EXPECTATIONS[18] = new int[3];
		EXPECTATIONS[18][0] = 9;
		EXPECTATIONS[18][1] = 11;
		EXPECTATIONS[18][2] = 9;
		EXPECTATIONS[19] = new int[3];
		EXPECTATIONS[19][0] = 10;
		EXPECTATIONS[19][1] = 11;
		EXPECTATIONS[19][2] = 10;
		EXPECTATIONS[20] = new int[3];
		EXPECTATIONS[20][0] = 11;
		EXPECTATIONS[20][1] = 11;
		EXPECTATIONS[20][2] = 11;
		EXPECTATIONS[21] = new int[3];
		EXPECTATIONS[21][0] = 12;
		EXPECTATIONS[21][1] = 11;
		EXPECTATIONS[21][2] = 12;
		EXPECTATIONS[22] = new int[2];
		EXPECTATIONS[22][0] = 13;
		EXPECTATIONS[22][1] = 11;
		EXPECTATIONS[23] = new int[3];
		EXPECTATIONS[23][0] = 8;
		EXPECTATIONS[23][1] = 12;
		EXPECTATIONS[23][2] = 13;
		EXPECTATIONS[24] = new int[3];
		EXPECTATIONS[24][0] = 9;
		EXPECTATIONS[24][1] = 12;
		EXPECTATIONS[24][2] = 14;
		EXPECTATIONS[25] = new int[3];
		EXPECTATIONS[25][0] = 10;
		EXPECTATIONS[25][1] = 12;
		EXPECTATIONS[25][2] = 15;
		EXPECTATIONS[26] = new int[3];
		EXPECTATIONS[26][0] = 11;
		EXPECTATIONS[26][1] = 12;
		EXPECTATIONS[26][2] = 16;
		EXPECTATIONS[27] = new int[3];
		EXPECTATIONS[27][0] = 12;
		EXPECTATIONS[27][1] = 12;
		EXPECTATIONS[27][2] = 17;
		EXPECTATIONS[28] = new int[2];
		EXPECTATIONS[28][0] = 13;
		EXPECTATIONS[28][1] = 12;
		EXPECTATIONS[29] = new int[3];
		EXPECTATIONS[29][0] = 2;
		EXPECTATIONS[29][1] = 13;
		EXPECTATIONS[29][2] = 18;
		EXPECTATIONS[30] = new int[2];
		EXPECTATIONS[30][0] = 3;
		EXPECTATIONS[30][1] = 13;
		EXPECTATIONS[31] = new int[2];
		EXPECTATIONS[31][0] = 14;
		EXPECTATIONS[31][1] = 14;
		EXPECTATIONS[32] = new int[2];
		EXPECTATIONS[32][0] = 15;
		EXPECTATIONS[32][1] = 15;
		EXPECTATIONS[33] = new int[2];
		EXPECTATIONS[33][0] = 16;
		EXPECTATIONS[33][1] = 16;
		EXPECTATIONS[34] = new int[2];
		EXPECTATIONS[34][0] = 17;
		EXPECTATIONS[34][1] = 17;
		EXPECTATIONS[35] = new int[2];
		EXPECTATIONS[35][0] = 18;
		EXPECTATIONS[35][1] = 18;
		EXPECTATIONS[36] = new int[2];
		EXPECTATIONS[36][0] = 19;
		EXPECTATIONS[36][1] = 19;
		EXPECTATIONS[37] = new int[2];
		EXPECTATIONS[37][0] = 20;
		EXPECTATIONS[37][1] = 20;
		EXPECTATIONS[38] = new int[3];
		EXPECTATIONS[38][0] = 8;
		EXPECTATIONS[38][1] = 20;
		EXPECTATIONS[38][2] = 19;
		EXPECTATIONS[39] = new int[3];
		EXPECTATIONS[39][0] = 9;
		EXPECTATIONS[39][1] = 20;
		EXPECTATIONS[39][2] = 20;
		EXPECTATIONS[40] = new int[3];
		EXPECTATIONS[40][0] = 10;
		EXPECTATIONS[40][1] = 20;
		EXPECTATIONS[40][2] = 21;
		EXPECTATIONS[41] = new int[3];
		EXPECTATIONS[41][0] = 11;
		EXPECTATIONS[41][1] = 20;
		EXPECTATIONS[41][2] = 22;
		EXPECTATIONS[42] = new int[3];
		EXPECTATIONS[42][0] = 12;
		EXPECTATIONS[42][1] = 20;
		EXPECTATIONS[42][2] = 23;
		EXPECTATIONS[43] = new int[2];
		EXPECTATIONS[43][0] = 13;
		EXPECTATIONS[43][1] = 20;
		EXPECTATIONS[44] = new int[2];
		EXPECTATIONS[44][0] = 21;
		EXPECTATIONS[44][1] = 20;
		EXPECTATIONS[45] = new int[3];
		EXPECTATIONS[45][0] = 8;
		EXPECTATIONS[45][1] = 21;
		EXPECTATIONS[45][2] = 24;
		EXPECTATIONS[46] = new int[3];
		EXPECTATIONS[46][0] = 9;
		EXPECTATIONS[46][1] = 21;
		EXPECTATIONS[46][2] = 25;
		EXPECTATIONS[47] = new int[3];
		EXPECTATIONS[47][0] = 10;
		EXPECTATIONS[47][1] = 21;
		EXPECTATIONS[47][2] = 26;
		EXPECTATIONS[48] = new int[3];
		EXPECTATIONS[48][0] = 11;
		EXPECTATIONS[48][1] = 21;
		EXPECTATIONS[48][2] = 27;
		EXPECTATIONS[49] = new int[3];
		EXPECTATIONS[49][0] = 12;
		EXPECTATIONS[49][1] = 21;
		EXPECTATIONS[49][2] = 28;
		EXPECTATIONS[50] = new int[2];
		EXPECTATIONS[50][0] = 21;
		EXPECTATIONS[50][1] = 21;
		EXPECTATIONS[51] = new int[2];
		EXPECTATIONS[51][0] = 21;
		EXPECTATIONS[51][1] = 22;
		EXPECTATIONS[52] = new int[3];
		EXPECTATIONS[52][0] = 8;
		EXPECTATIONS[52][1] = 23;
		EXPECTATIONS[52][2] = 29;
		EXPECTATIONS[53] = new int[3];
		EXPECTATIONS[53][0] = 9;
		EXPECTATIONS[53][1] = 23;
		EXPECTATIONS[53][2] = 30;
		EXPECTATIONS[54] = new int[3];
		EXPECTATIONS[54][0] = 10;
		EXPECTATIONS[54][1] = 23;
		EXPECTATIONS[54][2] = 31;
		EXPECTATIONS[55] = new int[3];
		EXPECTATIONS[55][0] = 11;
		EXPECTATIONS[55][1] = 23;
		EXPECTATIONS[55][2] = 32;
		EXPECTATIONS[56] = new int[3];
		EXPECTATIONS[56][0] = 12;
		EXPECTATIONS[56][1] = 23;
		EXPECTATIONS[56][2] = 33;
		EXPECTATIONS[57] = new int[2];
		EXPECTATIONS[57][0] = 21;
		EXPECTATIONS[57][1] = 23;
		EXPECTATIONS[58] = new int[3];
		EXPECTATIONS[58][0] = 8;
		EXPECTATIONS[58][1] = 24;
		EXPECTATIONS[58][2] = 34;
		EXPECTATIONS[59] = new int[3];
		EXPECTATIONS[59][0] = 9;
		EXPECTATIONS[59][1] = 24;
		EXPECTATIONS[59][2] = 35;
		EXPECTATIONS[60] = new int[3];
		EXPECTATIONS[60][0] = 10;
		EXPECTATIONS[60][1] = 24;
		EXPECTATIONS[60][2] = 36;
		EXPECTATIONS[61] = new int[3];
		EXPECTATIONS[61][0] = 11;
		EXPECTATIONS[61][1] = 24;
		EXPECTATIONS[61][2] = 37;
		EXPECTATIONS[62] = new int[3];
		EXPECTATIONS[62][0] = 12;
		EXPECTATIONS[62][1] = 24;
		EXPECTATIONS[62][2] = 38;
		EXPECTATIONS[63] = new int[2];
		EXPECTATIONS[63][0] = 21;
		EXPECTATIONS[63][1] = 24;
		EXPECTATIONS[64] = new int[2];
		EXPECTATIONS[64][0] = 21;
		EXPECTATIONS[64][1] = 25;
		EXPECTATIONS[65] = new int[3];
		EXPECTATIONS[65][0] = 8;
		EXPECTATIONS[65][1] = 26;
		EXPECTATIONS[65][2] = 39;
		EXPECTATIONS[66] = new int[3];
		EXPECTATIONS[66][0] = 9;
		EXPECTATIONS[66][1] = 26;
		EXPECTATIONS[66][2] = 40;
		EXPECTATIONS[67] = new int[3];
		EXPECTATIONS[67][0] = 10;
		EXPECTATIONS[67][1] = 26;
		EXPECTATIONS[67][2] = 41;
		EXPECTATIONS[68] = new int[3];
		EXPECTATIONS[68][0] = 11;
		EXPECTATIONS[68][1] = 26;
		EXPECTATIONS[68][2] = 42;
		EXPECTATIONS[69] = new int[3];
		EXPECTATIONS[69][0] = 12;
		EXPECTATIONS[69][1] = 26;
		EXPECTATIONS[69][2] = 43;
		EXPECTATIONS[70] = new int[2];
		EXPECTATIONS[70][0] = 13;
		EXPECTATIONS[70][1] = 26;
		EXPECTATIONS[71] = new int[2];
		EXPECTATIONS[71][0] = 21;
		EXPECTATIONS[71][1] = 26;
		EXPECTATIONS[72] = new int[3];
		EXPECTATIONS[72][0] = 8;
		EXPECTATIONS[72][1] = 27;
		EXPECTATIONS[72][2] = 44;
		EXPECTATIONS[73] = new int[3];
		EXPECTATIONS[73][0] = 9;
		EXPECTATIONS[73][1] = 27;
		EXPECTATIONS[73][2] = 45;
		EXPECTATIONS[74] = new int[3];
		EXPECTATIONS[74][0] = 10;
		EXPECTATIONS[74][1] = 27;
		EXPECTATIONS[74][2] = 46;
		EXPECTATIONS[75] = new int[3];
		EXPECTATIONS[75][0] = 11;
		EXPECTATIONS[75][1] = 27;
		EXPECTATIONS[75][2] = 47;
		EXPECTATIONS[76] = new int[3];
		EXPECTATIONS[76][0] = 12;
		EXPECTATIONS[76][1] = 27;
		EXPECTATIONS[76][2] = 48;
		EXPECTATIONS[77] = new int[2];
		EXPECTATIONS[77][0] = 13;
		EXPECTATIONS[77][1] = 27;
		EXPECTATIONS[78] = new int[2];
		EXPECTATIONS[78][0] = 21;
		EXPECTATIONS[78][1] = 27;
		EXPECTATIONS[79] = new int[2];
		EXPECTATIONS[79][0] = 22;
		EXPECTATIONS[79][1] = 28;
		EXPECTATIONS[80] = new int[2];
		EXPECTATIONS[80][0] = 23;
		EXPECTATIONS[80][1] = 29;
		EXPECTATIONS[81] = new int[2];
		EXPECTATIONS[81][0] = 24;
		EXPECTATIONS[81][1] = 30;
		EXPECTATIONS[82] = new int[2];
		EXPECTATIONS[82][0] = 25;
		EXPECTATIONS[82][1] = 31;
		EXPECTATIONS[83] = new int[2];
		EXPECTATIONS[83][0] = 26;
		EXPECTATIONS[83][1] = 32;
		EXPECTATIONS[84] = new int[2];
		EXPECTATIONS[84][0] = 27;
		EXPECTATIONS[84][1] = 33;
		EXPECTATIONS[85] = new int[2];
		EXPECTATIONS[85][0] = 28;
		EXPECTATIONS[85][1] = 34;
		EXPECTATIONS[86] = new int[2];
		EXPECTATIONS[86][0] = 29;
		EXPECTATIONS[86][1] = 35;
		EXPECTATIONS[87] = new int[3];
		EXPECTATIONS[87][0] = 8;
		EXPECTATIONS[87][1] = 36;
		EXPECTATIONS[87][2] = 49;
		EXPECTATIONS[88] = new int[3];
		EXPECTATIONS[88][0] = 9;
		EXPECTATIONS[88][1] = 36;
		EXPECTATIONS[88][2] = 50;
		EXPECTATIONS[89] = new int[3];
		EXPECTATIONS[89][0] = 10;
		EXPECTATIONS[89][1] = 36;
		EXPECTATIONS[89][2] = 51;
		EXPECTATIONS[90] = new int[3];
		EXPECTATIONS[90][0] = 11;
		EXPECTATIONS[90][1] = 36;
		EXPECTATIONS[90][2] = 52;
		EXPECTATIONS[91] = new int[3];
		EXPECTATIONS[91][0] = 12;
		EXPECTATIONS[91][1] = 36;
		EXPECTATIONS[91][2] = 53;
		EXPECTATIONS[92] = new int[2];
		EXPECTATIONS[92][0] = 13;
		EXPECTATIONS[92][1] = 36;
		EXPECTATIONS[93] = new int[2];
		EXPECTATIONS[93][0] = 21;
		EXPECTATIONS[93][1] = 36;
		EXPECTATIONS[94] = new int[2];
		EXPECTATIONS[94][0] = 30;
		EXPECTATIONS[94][1] = 37;
		EXPECTATIONS[95] = new int[2];
		EXPECTATIONS[95][0] = 31;
		EXPECTATIONS[95][1] = 38;
		EXPECTATIONS[96] = new int[2];
		EXPECTATIONS[96][0] = 32;
		EXPECTATIONS[96][1] = 39;
		EXPECTATIONS[97] = new int[2];
		EXPECTATIONS[97][0] = 33;
		EXPECTATIONS[97][1] = 40;
		EXPECTATIONS[98] = new int[2];
		EXPECTATIONS[98][0] = 34;
		EXPECTATIONS[98][1] = 41;
		EXPECTATIONS[99] = new int[2];
		EXPECTATIONS[99][0] = 35;
		EXPECTATIONS[99][1] = 42;
		EXPECTATIONS[100] = new int[3];
		EXPECTATIONS[100][0] = 8;
		EXPECTATIONS[100][1] = 43;
		EXPECTATIONS[100][2] = 54;
		EXPECTATIONS[101] = new int[3];
		EXPECTATIONS[101][0] = 9;
		EXPECTATIONS[101][1] = 43;
		EXPECTATIONS[101][2] = 55;
		EXPECTATIONS[102] = new int[3];
		EXPECTATIONS[102][0] = 10;
		EXPECTATIONS[102][1] = 43;
		EXPECTATIONS[102][2] = 56;
		EXPECTATIONS[103] = new int[3];
		EXPECTATIONS[103][0] = 11;
		EXPECTATIONS[103][1] = 43;
		EXPECTATIONS[103][2] = 57;
		EXPECTATIONS[104] = new int[3];
		EXPECTATIONS[104][0] = 12;
		EXPECTATIONS[104][1] = 43;
		EXPECTATIONS[104][2] = 58;
		EXPECTATIONS[105] = new int[2];
		EXPECTATIONS[105][0] = 13;
		EXPECTATIONS[105][1] = 43;
		EXPECTATIONS[106] = new int[2];
		EXPECTATIONS[106][0] = 21;
		EXPECTATIONS[106][1] = 43;
		EXPECTATIONS[107] = new int[2];
		EXPECTATIONS[107][0] = 36;
		EXPECTATIONS[107][1] = 44;
		EXPECTATIONS[108] = new int[2];
		EXPECTATIONS[108][0] = 37;
		EXPECTATIONS[108][1] = 45;
		EXPECTATIONS[109] = new int[2];
		EXPECTATIONS[109][0] = 38;
		EXPECTATIONS[109][1] = 46;
		EXPECTATIONS[110] = new int[2];
		EXPECTATIONS[110][0] = 39;
		EXPECTATIONS[110][1] = 47;
		EXPECTATIONS[111] = new int[3];
		EXPECTATIONS[111][0] = 8;
		EXPECTATIONS[111][1] = 48;
		EXPECTATIONS[111][2] = 59;
		EXPECTATIONS[112] = new int[3];
		EXPECTATIONS[112][0] = 9;
		EXPECTATIONS[112][1] = 48;
		EXPECTATIONS[112][2] = 60;
		EXPECTATIONS[113] = new int[3];
		EXPECTATIONS[113][0] = 10;
		EXPECTATIONS[113][1] = 48;
		EXPECTATIONS[113][2] = 61;
		EXPECTATIONS[114] = new int[3];
		EXPECTATIONS[114][0] = 11;
		EXPECTATIONS[114][1] = 48;
		EXPECTATIONS[114][2] = 62;
		EXPECTATIONS[115] = new int[3];
		EXPECTATIONS[115][0] = 12;
		EXPECTATIONS[115][1] = 48;
		EXPECTATIONS[115][2] = 63;
		EXPECTATIONS[116] = new int[2];
		EXPECTATIONS[116][0] = 13;
		EXPECTATIONS[116][1] = 48;
		EXPECTATIONS[117] = new int[2];
		EXPECTATIONS[117][0] = 21;
		EXPECTATIONS[117][1] = 48;
		EXPECTATIONS[118] = new int[2];
		EXPECTATIONS[118][0] = 40;
		EXPECTATIONS[118][1] = 49;
		EXPECTATIONS[119] = new int[2];
		EXPECTATIONS[119][0] = 41;
		EXPECTATIONS[119][1] = 50;
		EXPECTATIONS[120] = new int[2];
		EXPECTATIONS[120][0] = 42;
		EXPECTATIONS[120][1] = 51;
		EXPECTATIONS[121] = new int[2];
		EXPECTATIONS[121][0] = 43;
		EXPECTATIONS[121][1] = 52;
		EXPECTATIONS[122] = new int[3];
		EXPECTATIONS[122][0] = 8;
		EXPECTATIONS[122][1] = 53;
		EXPECTATIONS[122][2] = 64;
		EXPECTATIONS[123] = new int[3];
		EXPECTATIONS[123][0] = 9;
		EXPECTATIONS[123][1] = 53;
		EXPECTATIONS[123][2] = 65;
		EXPECTATIONS[124] = new int[3];
		EXPECTATIONS[124][0] = 10;
		EXPECTATIONS[124][1] = 53;
		EXPECTATIONS[124][2] = 66;
		EXPECTATIONS[125] = new int[3];
		EXPECTATIONS[125][0] = 11;
		EXPECTATIONS[125][1] = 53;
		EXPECTATIONS[125][2] = 67;
		EXPECTATIONS[126] = new int[3];
		EXPECTATIONS[126][0] = 12;
		EXPECTATIONS[126][1] = 53;
		EXPECTATIONS[126][2] = 68;
		EXPECTATIONS[127] = new int[2];
		EXPECTATIONS[127][0] = 13;
		EXPECTATIONS[127][1] = 53;
		EXPECTATIONS[128] = new int[2];
		EXPECTATIONS[128][0] = 21;
		EXPECTATIONS[128][1] = 53;
	}
	
	public static void initialize() {
		initialize0();
	}
	
	static {
		initialize();
	}
	
}
