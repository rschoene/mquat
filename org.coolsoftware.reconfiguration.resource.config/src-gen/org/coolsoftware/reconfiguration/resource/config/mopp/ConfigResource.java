/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

public class ConfigResource extends org.eclipse.emf.ecore.resource.impl.ResourceImpl implements org.coolsoftware.reconfiguration.resource.config.IConfigTextResource {
	
	public class ElementBasedTextDiagnostic implements org.coolsoftware.reconfiguration.resource.config.IConfigTextDiagnostic {
		
		private final org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap;
		private final org.eclipse.emf.common.util.URI uri;
		private final org.eclipse.emf.ecore.EObject element;
		private final org.coolsoftware.reconfiguration.resource.config.IConfigProblem problem;
		
		public ElementBasedTextDiagnostic(org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap, org.eclipse.emf.common.util.URI uri, org.coolsoftware.reconfiguration.resource.config.IConfigProblem problem, org.eclipse.emf.ecore.EObject element) {
			super();
			this.uri = uri;
			this.locationMap = locationMap;
			this.element = element;
			this.problem = problem;
		}
		
		public String getMessage() {
			return problem.getMessage();
		}
		
		public org.coolsoftware.reconfiguration.resource.config.IConfigProblem getProblem() {
			return problem;
		}
		
		public String getLocation() {
			return uri.toString();
		}
		
		public int getCharStart() {
			return Math.max(0, locationMap.getCharStart(element));
		}
		
		public int getCharEnd() {
			return Math.max(0, locationMap.getCharEnd(element));
		}
		
		public int getColumn() {
			return Math.max(0, locationMap.getColumn(element));
		}
		
		public int getLine() {
			return Math.max(0, locationMap.getLine(element));
		}
		
		public org.eclipse.emf.ecore.EObject getElement() {
			return element;
		}
		
		public boolean wasCausedBy(org.eclipse.emf.ecore.EObject element) {
			if (this.element == null) {
				return false;
			}
			return this.element.equals(element);
		}
		
		public String toString() {
			return getMessage() + " at " + getLocation() + " line " + getLine() + ", column " + getColumn();
		}
	}
	
	public class PositionBasedTextDiagnostic implements org.coolsoftware.reconfiguration.resource.config.IConfigTextDiagnostic {
		
		private final org.eclipse.emf.common.util.URI uri;
		
		private int column;
		private int line;
		private int charStart;
		private int charEnd;
		private org.coolsoftware.reconfiguration.resource.config.IConfigProblem problem;
		
		public PositionBasedTextDiagnostic(org.eclipse.emf.common.util.URI uri, org.coolsoftware.reconfiguration.resource.config.IConfigProblem problem, int column, int line, int charStart, int charEnd) {
			
			super();
			this.uri = uri;
			this.column = column;
			this.line = line;
			this.charStart = charStart;
			this.charEnd = charEnd;
			this.problem = problem;
		}
		
		public org.coolsoftware.reconfiguration.resource.config.IConfigProblem getProblem() {
			return problem;
		}
		
		public int getCharStart() {
			return charStart;
		}
		
		public int getCharEnd() {
			return charEnd;
		}
		
		public int getColumn() {
			return column;
		}
		
		public int getLine() {
			return line;
		}
		
		public String getLocation() {
			return uri.toString();
		}
		
		public String getMessage() {
			return problem.getMessage();
		}
		
		public boolean wasCausedBy(org.eclipse.emf.ecore.EObject element) {
			return false;
		}
		
		public String toString() {
			return getMessage() + " at " + getLocation() + " line " + getLine() + ", column " + getColumn();
		}
	}
	
	private org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolverSwitch resolverSwitch;
	private org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap;
	private int proxyCounter = 0;
	private org.coolsoftware.reconfiguration.resource.config.IConfigTextParser parser;
	private java.util.Map<String, org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragment<? extends org.eclipse.emf.ecore.EObject>> internalURIFragmentMap = new java.util.LinkedHashMap<String, org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragment<? extends org.eclipse.emf.ecore.EObject>>();
	private java.util.Map<String, org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix> quickFixMap = new java.util.LinkedHashMap<String, org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix>();
	private java.util.Map<?, ?> loadOptions;
	
	/**
	 * If a post-processor is currently running, this field holds a reference to it.
	 * This reference is used to terminate post-processing if needed.
	 */
	private org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessor runningPostProcessor;
	
	/**
	 * A flag to indicate whether reloading of the resource shall be cancelled.
	 */
	private boolean terminateReload = false;
	
	protected org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMetaInformation metaInformation = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMetaInformation();
	
	public ConfigResource() {
		super();
		resetLocationMap();
	}
	
	public ConfigResource(org.eclipse.emf.common.util.URI uri) {
		super(uri);
		resetLocationMap();
	}
	
	protected void doLoad(java.io.InputStream inputStream, java.util.Map<?,?> options) throws java.io.IOException {
		this.loadOptions = options;
		resetLocationMap();
		this.terminateReload = false;
		String encoding = null;
		java.io.InputStream actualInputStream = inputStream;
		Object inputStreamPreProcessorProvider = null;
		if (options != null) {
			inputStreamPreProcessorProvider = options.get(org.coolsoftware.reconfiguration.resource.config.IConfigOptions.INPUT_STREAM_PREPROCESSOR_PROVIDER);
		}
		if (inputStreamPreProcessorProvider != null) {
			if (inputStreamPreProcessorProvider instanceof org.coolsoftware.reconfiguration.resource.config.IConfigInputStreamProcessorProvider) {
				org.coolsoftware.reconfiguration.resource.config.IConfigInputStreamProcessorProvider provider = (org.coolsoftware.reconfiguration.resource.config.IConfigInputStreamProcessorProvider) inputStreamPreProcessorProvider;
				org.coolsoftware.reconfiguration.resource.config.mopp.ConfigInputStreamProcessor processor = provider.getInputStreamProcessor(inputStream);
				actualInputStream = processor;
				encoding = processor.getOutputEncoding();
			}
		}
		
		parser = getMetaInformation().createParser(actualInputStream, encoding);
		parser.setOptions(options);
		org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolverSwitch referenceResolverSwitch = getReferenceResolverSwitch();
		referenceResolverSwitch.setOptions(options);
		org.coolsoftware.reconfiguration.resource.config.IConfigParseResult result = parser.parse();
		// dispose parser, we don't need it anymore
		parser = null;
		clearState();
		getContentsInternal().clear();
		org.eclipse.emf.ecore.EObject root = null;
		if (result != null) {
			root = result.getRoot();
			if (root != null) {
				getContentsInternal().add(root);
			}
			java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>> commands = result.getPostParseCommands();
			if (commands != null) {
				for (org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>  command : commands) {
					command.execute(this);
				}
			}
		}
		getReferenceResolverSwitch().setOptions(options);
		if (getErrors().isEmpty()) {
			runPostProcessors(options);
			if (root != null) {
				runValidators(root);
			}
		}
	}
	
	public void reload(java.io.InputStream inputStream, java.util.Map<?,?> options) throws java.io.IOException {
		try {
			isLoaded = false;
			java.util.Map<Object, Object> loadOptions = addDefaultLoadOptions(options);
			doLoad(inputStream, loadOptions);
			resolveAfterParsing();
		} catch (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException tpe) {
			// do nothing - the resource is left unchanged if this exception is thrown
		}
		isLoaded = true;
	}
	
	public void cancelReload() {
		org.coolsoftware.reconfiguration.resource.config.IConfigTextParser parserCopy = parser;
		if (parserCopy != null) {
			parserCopy.terminate();
		}
		this.terminateReload = true;
		org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessor runningPostProcessorCopy = runningPostProcessor;
		if (runningPostProcessorCopy != null) {
			runningPostProcessorCopy.terminate();
		}
	}
	
	protected void doSave(java.io.OutputStream outputStream, java.util.Map<?,?> options) throws java.io.IOException {
		org.coolsoftware.reconfiguration.resource.config.IConfigTextPrinter printer = getMetaInformation().createPrinter(outputStream, this);
		org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolverSwitch referenceResolverSwitch = getReferenceResolverSwitch();
		referenceResolverSwitch.setOptions(options);
		for (org.eclipse.emf.ecore.EObject root : getContentsInternal()) {
			printer.print(root);
		}
	}
	
	protected String getSyntaxName() {
		return "config";
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolverSwitch getReferenceResolverSwitch() {
		if (resolverSwitch == null) {
			resolverSwitch = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigReferenceResolverSwitch();
		}
		return resolverSwitch;
	}
	
	public org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMetaInformation getMetaInformation() {
		return new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMetaInformation();
	}
	
	protected void resetLocationMap() {
		if (isLocationMapEnabled()) {
			locationMap = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigLocationMap();
		} else {
			locationMap = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigDevNullLocationMap();
		}
	}
	
	public void addURIFragment(String internalURIFragment, org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragment<? extends org.eclipse.emf.ecore.EObject> uriFragment) {
		internalURIFragmentMap.put(internalURIFragment, uriFragment);
	}
	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> void registerContextDependentProxy(org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragmentFactory<ContainerType, ReferenceType> factory, ContainerType container, org.eclipse.emf.ecore.EReference reference, String id, org.eclipse.emf.ecore.EObject proxyElement, int position) {
		org.eclipse.emf.ecore.InternalEObject proxy = (org.eclipse.emf.ecore.InternalEObject) proxyElement;
		String internalURIFragment = org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragment.INTERNAL_URI_FRAGMENT_PREFIX + (proxyCounter++) + "_" + id;
		org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragment<?> uriFragment = factory.create(id, container, reference, position, proxy);
		proxy.eSetProxyURI(getURI().appendFragment(internalURIFragment));
		addURIFragment(internalURIFragment, uriFragment);
	}
	
	public org.eclipse.emf.ecore.EObject getEObject(String id) {
		if (internalURIFragmentMap.containsKey(id)) {
			org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragment<? extends org.eclipse.emf.ecore.EObject> uriFragment = internalURIFragmentMap.get(id);
			boolean wasResolvedBefore = uriFragment.isResolved();
			org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolveResult<? extends org.eclipse.emf.ecore.EObject> result = null;
			// catch and report all Exceptions that occur during proxy resolving
			try {
				result = uriFragment.resolve();
			} catch (Exception e) {
				String message = "An expection occured while resolving the proxy for: "+ id + ". (" + e.toString() + ")";
				addProblem(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigProblem(message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNRESOLVED_REFERENCE, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.ERROR), uriFragment.getProxy());
				new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().logError(message, e);
			}
			if (result == null) {
				// the resolving did call itself
				return null;
			}
			if (!wasResolvedBefore && !result.wasResolved()) {
				attachResolveError(result, uriFragment.getProxy());
				return null;
			} else if (!result.wasResolved()) {
				return null;
			} else {
				org.eclipse.emf.ecore.EObject proxy = uriFragment.getProxy();
				// remove an error that might have been added by an earlier attempt
				removeDiagnostics(proxy, getErrors());
				// remove old warnings and attach new
				removeDiagnostics(proxy, getWarnings());
				attachResolveWarnings(result, proxy);
				org.coolsoftware.reconfiguration.resource.config.IConfigReferenceMapping<? extends org.eclipse.emf.ecore.EObject> mapping = result.getMappings().iterator().next();
				org.eclipse.emf.ecore.EObject resultElement = getResultElement(uriFragment, mapping, proxy, result.getErrorMessage());
				org.eclipse.emf.ecore.EObject container = uriFragment.getContainer();
				replaceProxyInLayoutAdapters(container, proxy, resultElement);
				return resultElement;
			}
		} else {
			return super.getEObject(id);
		}
	}
	
	protected void replaceProxyInLayoutAdapters(org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EObject proxy, org.eclipse.emf.ecore.EObject target) {
		for (org.eclipse.emf.common.notify.Adapter adapter : container.eAdapters()) {
			if (adapter instanceof org.coolsoftware.reconfiguration.resource.config.mopp.ConfigLayoutInformationAdapter) {
				org.coolsoftware.reconfiguration.resource.config.mopp.ConfigLayoutInformationAdapter layoutInformationAdapter = (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigLayoutInformationAdapter) adapter;
				layoutInformationAdapter.replaceProxy(proxy, target);
			}
		}
	}
	
	protected org.eclipse.emf.ecore.EObject getResultElement(org.coolsoftware.reconfiguration.resource.config.IConfigContextDependentURIFragment<? extends org.eclipse.emf.ecore.EObject> uriFragment, org.coolsoftware.reconfiguration.resource.config.IConfigReferenceMapping<? extends org.eclipse.emf.ecore.EObject> mapping, org.eclipse.emf.ecore.EObject proxy, final String errorMessage) {
		if (mapping instanceof org.coolsoftware.reconfiguration.resource.config.IConfigURIMapping<?>) {
			org.eclipse.emf.common.util.URI uri = ((org.coolsoftware.reconfiguration.resource.config.IConfigURIMapping<? extends org.eclipse.emf.ecore.EObject>)mapping).getTargetIdentifier();
			if (uri != null) {
				org.eclipse.emf.ecore.EObject result = null;
				try {
					result = this.getResourceSet().getEObject(uri, true);
				} catch (Exception e) {
					// we can catch exceptions here, because EMF will try to resolve again and handle
					// the exception
				}
				if (result == null || result.eIsProxy()) {
					// unable to resolve: attach error
					if (errorMessage == null) {
						assert(false);
					} else {
						addProblem(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigProblem(errorMessage, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNRESOLVED_REFERENCE, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.ERROR), proxy);
					}
				}
				return result;
			}
			return null;
		} else if (mapping instanceof org.coolsoftware.reconfiguration.resource.config.IConfigElementMapping<?>) {
			org.eclipse.emf.ecore.EObject element = ((org.coolsoftware.reconfiguration.resource.config.IConfigElementMapping<? extends org.eclipse.emf.ecore.EObject>)mapping).getTargetElement();
			org.eclipse.emf.ecore.EReference reference = uriFragment.getReference();
			org.eclipse.emf.ecore.EReference oppositeReference = uriFragment.getReference().getEOpposite();
			if (!uriFragment.getReference().isContainment() && oppositeReference != null) {
				if (reference.isMany()) {
					org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList.ManyInverse<org.eclipse.emf.ecore.EObject> list = org.coolsoftware.reconfiguration.resource.config.util.ConfigCastUtil.cast(element.eGet(oppositeReference, false));										// avoids duplicate entries in the reference caused by adding to the
					// oppositeReference
					list.basicAdd(uriFragment.getContainer(),null);
				} else {
					uriFragment.getContainer().eSet(uriFragment.getReference(), element);
				}
			}
			return element;
		} else {
			assert(false);
			return null;
		}
	}
	
	protected void removeDiagnostics(org.eclipse.emf.ecore.EObject cause, java.util.List<org.eclipse.emf.ecore.resource.Resource.Diagnostic> diagnostics) {
		// remove all errors/warnings from this resource
		for (org.eclipse.emf.ecore.resource.Resource.Diagnostic errorCand : new org.eclipse.emf.common.util.BasicEList<org.eclipse.emf.ecore.resource.Resource.Diagnostic>(diagnostics)) {
			if (errorCand instanceof org.coolsoftware.reconfiguration.resource.config.IConfigTextDiagnostic) {
				if (((org.coolsoftware.reconfiguration.resource.config.IConfigTextDiagnostic) errorCand).wasCausedBy(cause)) {
					diagnostics.remove(errorCand);
					if (new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().isEclipsePlatformAvailable()) {
						org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper.unmark(this, cause);
					}
				}
			}
		}
	}
	
	protected void attachResolveError(org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolveResult<?> result, org.eclipse.emf.ecore.EObject proxy) {
		// attach errors to this resource
		assert result != null;
		final String errorMessage = result.getErrorMessage();
		if (errorMessage == null) {
			assert(false);
		} else {
			addProblem(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigProblem(errorMessage, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNRESOLVED_REFERENCE, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.ERROR, result.getQuickFixes()), proxy);
		}
	}
	
	protected void attachResolveWarnings(org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolveResult<? extends org.eclipse.emf.ecore.EObject> result, org.eclipse.emf.ecore.EObject proxy) {
		assert result != null;
		assert result.wasResolved();
		if (result.wasResolved()) {
			for (org.coolsoftware.reconfiguration.resource.config.IConfigReferenceMapping<? extends org.eclipse.emf.ecore.EObject> mapping : result.getMappings()) {
				final String warningMessage = mapping.getWarning();
				if (warningMessage == null) {
					continue;
				}
				addProblem(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigProblem(warningMessage, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNRESOLVED_REFERENCE, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.WARNING), proxy);
			}
		}
	}
	
	/**
	 * Extends the super implementation by clearing all information about element
	 * positions.
	 */
	protected void doUnload() {
		super.doUnload();
		clearState();
		loadOptions = null;
	}
	
	protected void runPostProcessors(java.util.Map<?, ?> loadOptions) {
		if (new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().isEclipsePlatformAvailable()) {
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper.unmark(this, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.ANALYSIS_PROBLEM);
		}
		if (terminateReload) {
			return;
		}
		// first, run the generated post processor
		runPostProcessor(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigResourcePostProcessor());
		if (loadOptions == null) {
			return;
		}
		// then, run post processors that are registered via the load options extension
		// point
		Object resourcePostProcessorProvider = loadOptions.get(org.coolsoftware.reconfiguration.resource.config.IConfigOptions.RESOURCE_POSTPROCESSOR_PROVIDER);
		if (resourcePostProcessorProvider != null) {
			if (resourcePostProcessorProvider instanceof org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessorProvider) {
				runPostProcessor(((org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessorProvider) resourcePostProcessorProvider).getResourcePostProcessor());
			} else if (resourcePostProcessorProvider instanceof java.util.Collection<?>) {
				java.util.Collection<?> resourcePostProcessorProviderCollection = (java.util.Collection<?>) resourcePostProcessorProvider;
				for (Object processorProvider : resourcePostProcessorProviderCollection) {
					if (terminateReload) {
						return;
					}
					if (processorProvider instanceof org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessorProvider) {
						org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessorProvider csProcessorProvider = (org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessorProvider) processorProvider;
						org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessor postProcessor = csProcessorProvider.getResourcePostProcessor();
						runPostProcessor(postProcessor);
					}
				}
			}
		}
	}
	
	protected void runPostProcessor(org.coolsoftware.reconfiguration.resource.config.IConfigResourcePostProcessor postProcessor) {
		try {
			this.runningPostProcessor = postProcessor;
			postProcessor.process(this);
		} catch (Exception e) {
			new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().logError("Exception while running a post-processor.", e);
		}
		this.runningPostProcessor = null;
	}
	
	public void load(java.util.Map<?, ?> options) throws java.io.IOException {
		java.util.Map<Object, Object> loadOptions = addDefaultLoadOptions(options);
		super.load(loadOptions);
		resolveAfterParsing();
	}
	
	protected void resolveAfterParsing() {
		org.eclipse.emf.ecore.util.EcoreUtil.resolveAll(this);
	}
	
	public void setURI(org.eclipse.emf.common.util.URI uri) {
		// because of the context dependent proxy resolving it is essential to resolve all
		// proxies before the URI is changed which can cause loss of object identities
		org.eclipse.emf.ecore.util.EcoreUtil.resolveAll(this);
		super.setURI(uri);
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap getLocationMap() {
		return locationMap;
	}
	
	public void addProblem(org.coolsoftware.reconfiguration.resource.config.IConfigProblem problem, org.eclipse.emf.ecore.EObject element) {
		ElementBasedTextDiagnostic diagnostic = new ElementBasedTextDiagnostic(locationMap, getURI(), problem, element);
		getDiagnostics(problem.getSeverity()).add(diagnostic);
		if (new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().isEclipsePlatformAvailable() && isMarkerCreationEnabled()) {
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper.mark(this, diagnostic);
		}
		addQuickFixesToQuickFixMap(problem);
	}
	
	public void addProblem(org.coolsoftware.reconfiguration.resource.config.IConfigProblem problem, int column, int line, int charStart, int charEnd) {
		PositionBasedTextDiagnostic diagnostic = new PositionBasedTextDiagnostic(getURI(), problem, column, line, charStart, charEnd);
		getDiagnostics(problem.getSeverity()).add(diagnostic);
		if (new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().isEclipsePlatformAvailable() && isMarkerCreationEnabled()) {
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper.mark(this, diagnostic);
		}
		addQuickFixesToQuickFixMap(problem);
	}
	
	protected void addQuickFixesToQuickFixMap(org.coolsoftware.reconfiguration.resource.config.IConfigProblem problem) {
		java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix> quickFixes = problem.getQuickFixes();
		if (quickFixes != null) {
			for (org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix quickFix : quickFixes) {
				if (quickFix != null) {
					quickFixMap.put(quickFix.getContextAsString(), quickFix);
				}
			}
		}
	}
	
	@Deprecated	
	public void addError(String message, org.eclipse.emf.ecore.EObject cause) {
		addError(message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNKNOWN, cause);
	}
	
	public void addError(String message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType type, org.eclipse.emf.ecore.EObject cause) {
		addProblem(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigProblem(message, type, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.ERROR), cause);
	}
	
	@Deprecated	
	public void addWarning(String message, org.eclipse.emf.ecore.EObject cause) {
		addWarning(message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNKNOWN, cause);
	}
	
	public void addWarning(String message, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType type, org.eclipse.emf.ecore.EObject cause) {
		addProblem(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigProblem(message, type, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.WARNING), cause);
	}
	
	protected java.util.List<org.eclipse.emf.ecore.resource.Resource.Diagnostic> getDiagnostics(org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity severity) {
		if (severity == org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.ERROR) {
			return getErrors();
		} else {
			return getWarnings();
		}
	}
	
	protected java.util.Map<Object, Object> addDefaultLoadOptions(java.util.Map<?, ?> loadOptions) {
		java.util.Map<Object, Object> loadOptionsCopy = org.coolsoftware.reconfiguration.resource.config.util.ConfigMapUtil.copySafelyToObjectToObjectMap(loadOptions);
		// first add static option provider
		loadOptionsCopy.putAll(new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigOptionProvider().getOptions());
		
		// second, add dynamic option providers that are registered via extension
		if (new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().isEclipsePlatformAvailable()) {
			new org.coolsoftware.reconfiguration.resource.config.util.ConfigEclipseProxy().getDefaultLoadOptionProviderExtensions(loadOptionsCopy);
		}
		return loadOptionsCopy;
	}
	
	/**
	 * Extends the super implementation by clearing all information about element
	 * positions.
	 */
	protected void clearState() {
		// clear concrete syntax information
		resetLocationMap();
		internalURIFragmentMap.clear();
		getErrors().clear();
		getWarnings().clear();
		if (new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().isEclipsePlatformAvailable() && isMarkerCreationEnabled()) {
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper.unmark(this, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNKNOWN);
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper.unmark(this, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.SYNTAX_ERROR);
			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigMarkerHelper.unmark(this, org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.UNRESOLVED_REFERENCE);
		}
		proxyCounter = 0;
		resolverSwitch = null;
	}
	
	/**
	 * Returns a copy of the contents of this resource wrapped in a list that
	 * propagates changes to the original resource list. Wrapping is required to make
	 * sure that clients which obtain a reference to the list of contents do not
	 * interfere when changing the list.
	 */
	public org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject> getContents() {
		return new org.coolsoftware.reconfiguration.resource.config.util.ConfigCopiedEObjectInternalEList((org.eclipse.emf.ecore.util.InternalEList<org.eclipse.emf.ecore.EObject>) super.getContents());
	}
	
	/**
	 * Returns the raw contents of this resource.
	 */
	public org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject> getContentsInternal() {
		return super.getContents();
	}
	
	public org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.resource.Resource.Diagnostic> getWarnings() {
		return new org.coolsoftware.reconfiguration.resource.config.util.ConfigCopiedEList<org.eclipse.emf.ecore.resource.Resource.Diagnostic>(super.getWarnings());
	}
	
	public org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.resource.Resource.Diagnostic> getErrors() {
		return new org.coolsoftware.reconfiguration.resource.config.util.ConfigCopiedEList<org.eclipse.emf.ecore.resource.Resource.Diagnostic>(super.getErrors());
	}
	
	protected void runValidators(org.eclipse.emf.ecore.EObject root) {
		// checking constraints provided by EMF validator classes was disabled by option
		// 'disableEValidators'.
		
		if (new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().isEclipsePlatformAvailable()) {
			new org.coolsoftware.reconfiguration.resource.config.util.ConfigEclipseProxy().checkEMFValidationConstraints(this, root);
		}
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix getQuickFix(String quickFixContext) {
		return quickFixMap.get(quickFixContext);
	}
	
	public boolean isMarkerCreationEnabled() {
		if (loadOptions == null) {
			return true;
		}
		return !loadOptions.containsKey(org.coolsoftware.reconfiguration.resource.config.IConfigOptions.DISABLE_CREATING_MARKERS_FOR_PROBLEMS);
	}
	
	protected boolean isLocationMapEnabled() {
		if (loadOptions == null) {
			return true;
		}
		return !loadOptions.containsKey(org.coolsoftware.reconfiguration.resource.config.IConfigOptions.DISABLE_LOCATION_MAP);
	}
	
}
