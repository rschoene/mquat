/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.mopp;

public class ConfigReferenceResolverSwitch implements org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
	}
	
	public org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(org.coolsoftware.reconfiguration.resource.config.IConfigOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().logWarning("Found value with invalid type for option " + org.coolsoftware.reconfiguration.resource.config.IConfigOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver) {
			org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver replacingResolver = (org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver) resolverValue;
			if (replacingResolver instanceof org.coolsoftware.reconfiguration.resource.config.IConfigDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((org.coolsoftware.reconfiguration.resource.config.IConfigDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof org.coolsoftware.reconfiguration.resource.config.IConfigReferenceCache) {
					org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver nextResolver = (org.coolsoftware.reconfiguration.resource.config.IConfigReferenceResolver) next;
					if (nextResolver instanceof org.coolsoftware.reconfiguration.resource.config.IConfigDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((org.coolsoftware.reconfiguration.resource.config.IConfigDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.reconfiguration.resource.config.IConfigOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.reconfiguration.resource.config.IConfigDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.reconfiguration.resource.config.IConfigOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.reconfiguration.resource.config.IConfigDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
