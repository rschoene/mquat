/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 3.4

	package org.coolsoftware.reconfiguration.resource.config.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class ConfigParser extends ConfigANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "FILE_NAME", "LINEBREAK", "ML_COMMENT", "SL_COMMENT", "TEXT", "WHITESPACE", "'SWComponent'", "'SWComponentType'", "'deploy'", "'for'", "'from'", "'migrate'", "'on'", "'reconfiguration'", "'reconfigurationPlan'", "'replace'", "'to'", "'undeploy'", "'with'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int FILE_NAME=4;
    public static final int LINEBREAK=5;
    public static final int ML_COMMENT=6;
    public static final int SL_COMMENT=7;
    public static final int TEXT=8;
    public static final int WHITESPACE=9;

    // delegates
    public ConfigANTLRParserBase[] getDelegates() {
        return new ConfigANTLRParserBase[] {};
    }

    // delegators


    public ConfigParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public ConfigParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(19 + 1);
         

    }

    public String[] getTokenNames() { return ConfigParser.tokenNames; }
    public String getGrammarFileName() { return "Config.g"; }


    	private org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolverFactory tokenResolverFactory = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
    			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new org.coolsoftware.reconfiguration.resource.config.IConfigProblem() {
    					public org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity getSeverity() {
    						return org.coolsoftware.reconfiguration.resource.config.ConfigEProblemSeverity.ERROR;
    					}
    					public org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType getType() {
    						return org.coolsoftware.reconfiguration.resource.config.ConfigEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement terminal = org.coolsoftware.reconfiguration.resource.config.grammar.ConfigFollowSetProvider.TERMINALS[terminalID];
    		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] containmentTrace = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentTrace[i - 2] = org.coolsoftware.reconfiguration.resource.config.grammar.ConfigFollowSetProvider.LINKS[ids[i]];
    		}
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal expectedElement = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    		int currentPos = getTokenStream().index();
    		if (currentPos == 0) {
    			return;
    		}
    		int endPos = currentPos - 1;
    		for (; endPos >= this.lastPosition; endPos--) {
    			org.antlr.runtime3_4_0.Token token = getTokenStream().get(endPos);
    			int _channel = token.getChannel();
    			if (_channel != 99) {
    				break;
    			}
    		}
    		for (int pos = this.lastPosition; pos < endPos; pos++) {
    			org.antlr.runtime3_4_0.Token token = getTokenStream().get(pos);
    			int _channel = token.getChannel();
    			if (_channel == 99) {
    				if (token.getType() == ConfigLexer.SL_COMMENT) {
    					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
    					if (feature != null) {
    						// call token resolver
    						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
    						resolvedResolver.setOptions(getOptions());
    						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult resolvedResult = getFreshTokenResolveResult();
    						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
    						Object resolvedObject = resolvedResult.getResolvedToken();
    						if (resolvedObject == null) {
    							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
    						}
    						if (java.lang.String.class.isInstance(resolvedObject)) {
    							addObjectToList(element, feature, resolvedObject);
    						} else {
    							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
    						}
    					} else {
    						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
    					}
    				}
    				if (token.getType() == ConfigLexer.ML_COMMENT) {
    					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
    					if (feature != null) {
    						// call token resolver
    						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
    						resolvedResolver.setOptions(getOptions());
    						org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult resolvedResult = getFreshTokenResolveResult();
    						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
    						Object resolvedObject = resolvedResult.getResolvedToken();
    						if (resolvedObject == null) {
    							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
    						}
    						if (java.lang.String.class.isInstance(resolvedObject)) {
    							addObjectToList(element, feature, resolvedObject);
    						} else {
    							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
    						}
    					} else {
    						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
    					}
    				}
    			}
    		}
    		this.lastPosition = (endPos < 0 ? 0 : endPos);
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
    			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
    				org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
    			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
    				org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>() {
    			public boolean execute(org.coolsoftware.reconfiguration.resource.config.IConfigTextResource resource) {
    				org.coolsoftware.reconfiguration.resource.config.IConfigLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public org.coolsoftware.reconfiguration.resource.config.IConfigTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new ConfigParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ConfigLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new ConfigParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ConfigLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new org.coolsoftware.reconfiguration.resource.config.util.ConfigRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public ConfigParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((ConfigLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((ConfigLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.ReconfigurationPlan.class) {
    				return parse_org_coolsoftware_reconfiguration_ReconfigurationPlan();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.ReconfigurationActivity.class) {
    				return parse_org_coolsoftware_reconfiguration_ReconfigurationActivity();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.LocalReplacement.class) {
    				return parse_org_coolsoftware_reconfiguration_LocalReplacement();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.DistributedReplacement.class) {
    				return parse_org_coolsoftware_reconfiguration_DistributedReplacement();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.Migration.class) {
    				return parse_org_coolsoftware_reconfiguration_Migration();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.Deployment.class) {
    				return parse_org_coolsoftware_reconfiguration_Deployment();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.reconfiguration.UnDeployment.class) {
    				return parse_org_coolsoftware_reconfiguration_UnDeployment();
    			}
    		}
    		throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(org.coolsoftware.reconfiguration.resource.config.IConfigOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public org.coolsoftware.reconfiguration.resource.config.IConfigParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource>>();
    		org.coolsoftware.reconfiguration.resource.config.mopp.ConfigParseResult parseResult = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.reconfiguration.resource.config.IConfigTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		org.coolsoftware.reconfiguration.resource.config.IConfigParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (org.coolsoftware.reconfiguration.resource.config.IConfigCommand<org.coolsoftware.reconfiguration.resource.config.IConfigTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal>();
    		java.util.List<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 54;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (org.coolsoftware.reconfiguration.resource.config.util.ConfigPair<org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement, org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[]> newFollowerPair : newFollowers) {
    							org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal newFollowTerminal = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal(container, newFollower, followSetID, newFollowerPair.getRight());
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Config.g:576:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_org_coolsoftware_reconfiguration_ReconfigurationPlan ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        org.coolsoftware.reconfiguration.ReconfigurationPlan c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Config.g:577:2: ( (c0= parse_org_coolsoftware_reconfiguration_ReconfigurationPlan ) EOF )
            // Config.g:578:2: (c0= parse_org_coolsoftware_reconfiguration_ReconfigurationPlan ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[0]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Config.g:583:2: (c0= parse_org_coolsoftware_reconfiguration_ReconfigurationPlan )
            // Config.g:584:3: c0= parse_org_coolsoftware_reconfiguration_ReconfigurationPlan
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan_in_start82);
            c0=parse_org_coolsoftware_reconfiguration_ReconfigurationPlan();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_ReconfigurationPlan"
    // Config.g:592:1: parse_org_coolsoftware_reconfiguration_ReconfigurationPlan returns [org.coolsoftware.reconfiguration.ReconfigurationPlan element = null] : a0= 'reconfigurationPlan' a1= '{' ( ( (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity ) ) )* a3= '}' ;
    public final org.coolsoftware.reconfiguration.ReconfigurationPlan parse_org_coolsoftware_reconfiguration_ReconfigurationPlan() throws RecognitionException {
        org.coolsoftware.reconfiguration.ReconfigurationPlan element =  null;

        int parse_org_coolsoftware_reconfiguration_ReconfigurationPlan_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a3=null;
        org.coolsoftware.reconfiguration.ReconfigurationActivity a2_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Config.g:595:2: (a0= 'reconfigurationPlan' a1= '{' ( ( (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity ) ) )* a3= '}' )
            // Config.g:596:2: a0= 'reconfigurationPlan' a1= '{' ( ( (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity ) ) )* a3= '}'
            {
            a0=(Token)match(input,18,FOLLOW_18_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan115); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[1]);
            	}

            a1=(Token)match(input,23,FOLLOW_23_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan129); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[2]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[3]);
            	}

            // Config.g:625:2: ( ( (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==17) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Config.g:626:3: ( (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity ) )
            	    {
            	    // Config.g:626:3: ( (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity ) )
            	    // Config.g:627:4: (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity )
            	    {
            	    // Config.g:627:4: (a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity )
            	    // Config.g:628:5: a2_0= parse_org_coolsoftware_reconfiguration_ReconfigurationActivity
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan158);
            	    a2_0=parse_org_coolsoftware_reconfiguration_ReconfigurationActivity();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a2_0 != null) {
            	    						if (a2_0 != null) {
            	    							Object value = a2_0;
            	    							addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_PLAN__ACTIVITIES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_2_0_0_1, a2_0, true);
            	    						copyLocalizationInfos(a2_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[4]);
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[5]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[6]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[7]);
            	}

            a3=(Token)match(input,24,FOLLOW_24_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan199); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationPlan();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_org_coolsoftware_reconfiguration_ReconfigurationPlan_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_ReconfigurationPlan"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_ReconfigurationActivity"
    // Config.g:677:1: parse_org_coolsoftware_reconfiguration_ReconfigurationActivity returns [org.coolsoftware.reconfiguration.ReconfigurationActivity element = null] : a0= 'reconfiguration' a1= 'for' a2= 'SWComponentType' (a3= TEXT ) a4= '{' ( ( (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )+ a6= '}' ;
    public final org.coolsoftware.reconfiguration.ReconfigurationActivity parse_org_coolsoftware_reconfiguration_ReconfigurationActivity() throws RecognitionException {
        org.coolsoftware.reconfiguration.ReconfigurationActivity element =  null;

        int parse_org_coolsoftware_reconfiguration_ReconfigurationActivity_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a6=null;
        org.coolsoftware.reconfiguration.ReconfigurationStep a5_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Config.g:680:2: (a0= 'reconfiguration' a1= 'for' a2= 'SWComponentType' (a3= TEXT ) a4= '{' ( ( (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )+ a6= '}' )
            // Config.g:681:2: a0= 'reconfiguration' a1= 'for' a2= 'SWComponentType' (a3= TEXT ) a4= '{' ( ( (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )+ a6= '}'
            {
            a0=(Token)match(input,17,FOLLOW_17_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity228); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[8]);
            	}

            a1=(Token)match(input,13,FOLLOW_13_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity242); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[9]);
            	}

            a2=(Token)match(input,11,FOLLOW_11_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity256); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[10]);
            	}

            // Config.g:723:2: (a3= TEXT )
            // Config.g:724:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity274); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_6, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[11]);
            	}

            a4=(Token)match(input,23,FOLLOW_23_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity295); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_8, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[12]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[13]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[14]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[15]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[16]);
            	}

            // Config.g:777:2: ( ( (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12||LA2_0==15||LA2_0==19||LA2_0==21) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Config.g:778:3: ( (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) )
            	    {
            	    // Config.g:778:3: ( (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) )
            	    // Config.g:779:4: (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
            	    {
            	    // Config.g:779:4: (a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
            	    // Config.g:780:5: a5_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity324);
            	    a5_0=parse_org_coolsoftware_reconfiguration_ReconfigurationStep();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a5_0 != null) {
            	    						if (a5_0 != null) {
            	    							Object value = a5_0;
            	    							addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_9_0_0_1, a5_0, true);
            	    						copyLocalizationInfos(a5_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[17]);
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[18]);
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[19]);
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[20]);
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[21]);
            	    				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[22]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[23]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[24]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[25]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[26]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[27]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[28]);
            	}

            a6=(Token)match(input,24,FOLLOW_24_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity365); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createReconfigurationActivity();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[29]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[30]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_org_coolsoftware_reconfiguration_ReconfigurationActivity_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_ReconfigurationActivity"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_LocalReplacement"
    // Config.g:839:1: parse_org_coolsoftware_reconfiguration_LocalReplacement returns [org.coolsoftware.reconfiguration.LocalReplacement element = null] : (a0= 'replace' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) a5= 'with' (a6= TEXT ) ( (a7= '{' ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* ) a10= '}' ) )? |c0= parse_org_coolsoftware_reconfiguration_DistributedReplacement );
    public final org.coolsoftware.reconfiguration.LocalReplacement parse_org_coolsoftware_reconfiguration_LocalReplacement() throws RecognitionException {
        org.coolsoftware.reconfiguration.LocalReplacement element =  null;

        int parse_org_coolsoftware_reconfiguration_LocalReplacement_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a10=null;
        org.coolsoftware.reconfiguration.ReconfigurationStep a8_0 =null;

        org.coolsoftware.reconfiguration.ReconfigurationStep a9_0 =null;

        org.coolsoftware.reconfiguration.DistributedReplacement c0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Config.g:842:2: (a0= 'replace' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) a5= 'with' (a6= TEXT ) ( (a7= '{' ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* ) a10= '}' ) )? |c0= parse_org_coolsoftware_reconfiguration_DistributedReplacement )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==10) ) {
                    int LA6_2 = input.LA(3);

                    if ( (LA6_2==TEXT) ) {
                        int LA6_3 = input.LA(4);

                        if ( (LA6_3==16) ) {
                            int LA6_4 = input.LA(5);

                            if ( (LA6_4==TEXT) ) {
                                int LA6_5 = input.LA(6);

                                if ( (LA6_5==22) ) {
                                    int LA6_6 = input.LA(7);

                                    if ( (LA6_6==TEXT) ) {
                                        int LA6_7 = input.LA(8);

                                        if ( (LA6_7==EOF||LA6_7==12||LA6_7==15||LA6_7==19||LA6_7==21||(LA6_7 >= 23 && LA6_7 <= 24)) ) {
                                            alt6=1;
                                        }
                                        else if ( (LA6_7==16) ) {
                                            alt6=2;
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 6, 7, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 6, 6, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 6, 5, input);

                                    throw nvae;

                                }
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 6, 4, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 6, 3, input);

                            throw nvae;

                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 2, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }
            switch (alt6) {
                case 1 :
                    // Config.g:843:2: a0= 'replace' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) a5= 'with' (a6= TEXT ) ( (a7= '{' ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* ) a10= '}' ) )?
                    {
                    a0=(Token)match(input,19,FOLLOW_19_in_parse_org_coolsoftware_reconfiguration_LocalReplacement394); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    		if (element == null) {
                    			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                    			startIncompleteElement(element);
                    		}
                    		collectHiddenTokens(element);
                    		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_0, null, true);
                    		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	}

                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[31]);
                    	}

                    a1=(Token)match(input,10,FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_LocalReplacement408); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    		if (element == null) {
                    			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                    			startIncompleteElement(element);
                    		}
                    		collectHiddenTokens(element);
                    		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_2, null, true);
                    		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    	}

                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[32]);
                    	}

                    // Config.g:871:2: (a2= TEXT )
                    // Config.g:872:3: a2= TEXT
                    {
                    a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_LocalReplacement426); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (terminateParsing) {
                    				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
                    			}
                    			if (element == null) {
                    				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                    				startIncompleteElement(element);
                    			}
                    			if (a2 != null) {
                    				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    				tokenResolver.setOptions(getOptions());
                    				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
                    				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_COMPONENT), result);
                    				Object resolvedObject = result.getResolvedToken();
                    				if (resolvedObject == null) {
                    					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
                    				}
                    				java.lang.String resolved = (java.lang.String) resolvedObject;
                    				if (resolved != null) {
                    					Object value = resolved;
                    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_COMPONENT), value);
                    					completedElement(value, false);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_4, resolved, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
                    			}
                    		}

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[33]);
                    	}

                    a3=(Token)match(input,16,FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_LocalReplacement447); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    		if (element == null) {
                    			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                    			startIncompleteElement(element);
                    		}
                    		collectHiddenTokens(element);
                    		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_6, null, true);
                    		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    	}

                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[34]);
                    	}

                    // Config.g:921:2: (a4= TEXT )
                    // Config.g:922:3: a4= TEXT
                    {
                    a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_LocalReplacement465); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (terminateParsing) {
                    				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
                    			}
                    			if (element == null) {
                    				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                    				startIncompleteElement(element);
                    			}
                    			if (a4 != null) {
                    				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    				tokenResolver.setOptions(getOptions());
                    				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
                    				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_CONTAINER), result);
                    				Object resolvedObject = result.getResolvedToken();
                    				if (resolvedObject == null) {
                    					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
                    				}
                    				java.lang.String resolved = (java.lang.String) resolvedObject;
                    				if (resolved != null) {
                    					Object value = resolved;
                    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SOURCE_CONTAINER), value);
                    					completedElement(value, false);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_8, resolved, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
                    			}
                    		}

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[35]);
                    	}

                    a5=(Token)match(input,22,FOLLOW_22_in_parse_org_coolsoftware_reconfiguration_LocalReplacement486); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    		if (element == null) {
                    			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                    			startIncompleteElement(element);
                    		}
                    		collectHiddenTokens(element);
                    		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_10, null, true);
                    		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    	}

                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[36]);
                    	}

                    // Config.g:971:2: (a6= TEXT )
                    // Config.g:972:3: a6= TEXT
                    {
                    a6=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_LocalReplacement504); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (terminateParsing) {
                    				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
                    			}
                    			if (element == null) {
                    				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                    				startIncompleteElement(element);
                    			}
                    			if (a6 != null) {
                    				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    				tokenResolver.setOptions(getOptions());
                    				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
                    				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT), result);
                    				Object resolvedObject = result.getResolvedToken();
                    				if (resolvedObject == null) {
                    					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
                    				}
                    				java.lang.String resolved = (java.lang.String) resolvedObject;
                    				if (resolved != null) {
                    					Object value = resolved;
                    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT), value);
                    					completedElement(value, false);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_12, resolved, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
                    			}
                    		}

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[37]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[38]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[39]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[40]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[41]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[42]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[43]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[44]);
                    	}

                    // Config.g:1014:2: ( (a7= '{' ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* ) a10= '}' ) )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==23) ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // Config.g:1015:3: (a7= '{' ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* ) a10= '}' )
                            {
                            // Config.g:1015:3: (a7= '{' ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* ) a10= '}' )
                            // Config.g:1016:4: a7= '{' ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* ) a10= '}'
                            {
                            a7=(Token)match(input,23,FOLLOW_23_in_parse_org_coolsoftware_reconfiguration_LocalReplacement534); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            				if (element == null) {
                            					element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                            					startIncompleteElement(element);
                            				}
                            				collectHiddenTokens(element);
                            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_0, null, true);
                            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
                            			}

                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[45]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[46]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[47]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[48]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[49]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[50]);
                            			}

                            // Config.g:1035:4: ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) | ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )* )
                            int alt4=2;
                            switch ( input.LA(1) ) {
                            case 19:
                                {
                                int LA4_1 = input.LA(2);

                                if ( (LA4_1==10) ) {
                                    int LA4_6 = input.LA(3);

                                    if ( (LA4_6==TEXT) ) {
                                        int LA4_10 = input.LA(4);

                                        if ( (LA4_10==16) ) {
                                            int LA4_14 = input.LA(5);

                                            if ( (LA4_14==TEXT) ) {
                                                int LA4_18 = input.LA(6);

                                                if ( (LA4_18==22) ) {
                                                    int LA4_22 = input.LA(7);

                                                    if ( (LA4_22==TEXT) ) {
                                                        int LA4_25 = input.LA(8);

                                                        if ( (synpred3_Config()) ) {
                                                            alt4=1;
                                                        }
                                                        else if ( (true) ) {
                                                            alt4=2;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 4, 25, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 4, 22, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 4, 18, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 4, 14, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 4, 10, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 4, 6, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 4, 1, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 15:
                                {
                                int LA4_2 = input.LA(2);

                                if ( (LA4_2==10) ) {
                                    int LA4_7 = input.LA(3);

                                    if ( (LA4_7==TEXT) ) {
                                        int LA4_11 = input.LA(4);

                                        if ( (LA4_11==14) ) {
                                            int LA4_15 = input.LA(5);

                                            if ( (LA4_15==TEXT) ) {
                                                int LA4_19 = input.LA(6);

                                                if ( (LA4_19==20) ) {
                                                    int LA4_23 = input.LA(7);

                                                    if ( (LA4_23==TEXT) ) {
                                                        int LA4_26 = input.LA(8);

                                                        if ( (synpred3_Config()) ) {
                                                            alt4=1;
                                                        }
                                                        else if ( (true) ) {
                                                            alt4=2;
                                                        }
                                                        else {
                                                            if (state.backtracking>0) {state.failed=true; return element;}
                                                            NoViableAltException nvae =
                                                                new NoViableAltException("", 4, 26, input);

                                                            throw nvae;

                                                        }
                                                    }
                                                    else {
                                                        if (state.backtracking>0) {state.failed=true; return element;}
                                                        NoViableAltException nvae =
                                                            new NoViableAltException("", 4, 23, input);

                                                        throw nvae;

                                                    }
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 4, 19, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 4, 15, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 4, 11, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 4, 7, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 4, 2, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 12:
                                {
                                int LA4_3 = input.LA(2);

                                if ( (LA4_3==10) ) {
                                    int LA4_8 = input.LA(3);

                                    if ( (LA4_8==TEXT) ) {
                                        int LA4_12 = input.LA(4);

                                        if ( (LA4_12==16) ) {
                                            int LA4_16 = input.LA(5);

                                            if ( (LA4_16==TEXT) ) {
                                                int LA4_20 = input.LA(6);

                                                if ( (synpred3_Config()) ) {
                                                    alt4=1;
                                                }
                                                else if ( (true) ) {
                                                    alt4=2;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 4, 20, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 4, 16, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 4, 12, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 4, 8, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 4, 3, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 21:
                                {
                                int LA4_4 = input.LA(2);

                                if ( (LA4_4==10) ) {
                                    int LA4_9 = input.LA(3);

                                    if ( (LA4_9==TEXT) ) {
                                        int LA4_13 = input.LA(4);

                                        if ( (LA4_13==14) ) {
                                            int LA4_17 = input.LA(5);

                                            if ( (LA4_17==TEXT) ) {
                                                int LA4_21 = input.LA(6);

                                                if ( (synpred3_Config()) ) {
                                                    alt4=1;
                                                }
                                                else if ( (true) ) {
                                                    alt4=2;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 4, 21, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 4, 17, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 4, 13, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 4, 9, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 4, 4, input);

                                    throw nvae;

                                }
                                }
                                break;
                            case 24:
                                {
                                alt4=2;
                                }
                                break;
                            default:
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 0, input);

                                throw nvae;

                            }

                            switch (alt4) {
                                case 1 :
                                    // Config.g:1036:5: (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
                                    {
                                    // Config.g:1036:5: (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
                                    // Config.g:1037:6: a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep
                                    {
                                    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_parse_org_coolsoftware_reconfiguration_LocalReplacement567);
                                    a8_0=parse_org_coolsoftware_reconfiguration_ReconfigurationStep();

                                    state._fsp--;
                                    if (state.failed) return element;

                                    if ( state.backtracking==0 ) {
                                    						if (terminateParsing) {
                                    							throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
                                    						}
                                    						if (element == null) {
                                    							element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                                    							startIncompleteElement(element);
                                    						}
                                    						if (a8_0 != null) {
                                    							if (a8_0 != null) {
                                    								Object value = a8_0;
                                    								addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SUB_STEP, value);
                                    								completedElement(value, true);
                                    							}
                                    							collectHiddenTokens(element);
                                    							retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_1_0_0_0, a8_0, true);
                                    							copyLocalizationInfos(a8_0, element);
                                    						}
                                    					}

                                    }


                                    if ( state.backtracking==0 ) {
                                    					// expected elements (follow set)
                                    					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[51]);
                                    				}

                                    }
                                    break;
                                case 2 :
                                    // Config.g:1063:10: ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )*
                                    {
                                    // Config.g:1063:10: ( ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) ) )*
                                    loop3:
                                    do {
                                        int alt3=2;
                                        int LA3_0 = input.LA(1);

                                        if ( (LA3_0==12||LA3_0==15||LA3_0==19||LA3_0==21) ) {
                                            alt3=1;
                                        }


                                        switch (alt3) {
                                    	case 1 :
                                    	    // Config.g:1064:6: ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) )
                                    	    {
                                    	    // Config.g:1064:6: ( (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) )
                                    	    // Config.g:1065:7: (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
                                    	    {
                                    	    // Config.g:1065:7: (a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
                                    	    // Config.g:1066:8: a9_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep
                                    	    {
                                    	    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_parse_org_coolsoftware_reconfiguration_LocalReplacement634);
                                    	    a9_0=parse_org_coolsoftware_reconfiguration_ReconfigurationStep();

                                    	    state._fsp--;
                                    	    if (state.failed) return element;

                                    	    if ( state.backtracking==0 ) {
                                    	    								if (terminateParsing) {
                                    	    									throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
                                    	    								}
                                    	    								if (element == null) {
                                    	    									element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                                    	    									startIncompleteElement(element);
                                    	    								}
                                    	    								if (a9_0 != null) {
                                    	    									if (a9_0 != null) {
                                    	    										Object value = a9_0;
                                    	    										addObjectToList(element, org.coolsoftware.reconfiguration.ReconfigurationPackage.LOCAL_REPLACEMENT__SUB_STEP, value);
                                    	    										completedElement(value, true);
                                    	    									}
                                    	    									collectHiddenTokens(element);
                                    	    									retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_1_0_1_0_0_0_0, a9_0, true);
                                    	    									copyLocalizationInfos(a9_0, element);
                                    	    								}
                                    	    							}

                                    	    }


                                    	    if ( state.backtracking==0 ) {
                                    	    							// expected elements (follow set)
                                    	    							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[52]);
                                    	    							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[53]);
                                    	    							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[54]);
                                    	    							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[55]);
                                    	    							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[56]);
                                    	    							addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[57]);
                                    	    						}

                                    	    }


                                    	    }
                                    	    break;

                                    	default :
                                    	    break loop3;
                                        }
                                    } while (true);


                                    if ( state.backtracking==0 ) {
                                    					// expected elements (follow set)
                                    					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[58]);
                                    					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[59]);
                                    					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[60]);
                                    					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[61]);
                                    					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[62]);
                                    					addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[63]);
                                    				}

                                    }
                                    break;

                            }


                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[64]);
                            			}

                            a10=(Token)match(input,24,FOLLOW_24_in_parse_org_coolsoftware_reconfiguration_LocalReplacement718); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            				if (element == null) {
                            					element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createLocalReplacement();
                            					startIncompleteElement(element);
                            				}
                            				collectHiddenTokens(element);
                            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_2, null, true);
                            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
                            			}

                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[65]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[66]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[67]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[68]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[69]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[70]);
                            				addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[71]);
                            			}

                            }


                            }
                            break;

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[72]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[73]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[74]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[75]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[76]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[77]);
                    		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[78]);
                    	}

                    }
                    break;
                case 2 :
                    // Config.g:1149:2: c0= parse_org_coolsoftware_reconfiguration_DistributedReplacement
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_DistributedReplacement_in_parse_org_coolsoftware_reconfiguration_LocalReplacement756);
                    c0=parse_org_coolsoftware_reconfiguration_DistributedReplacement();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_org_coolsoftware_reconfiguration_LocalReplacement_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_LocalReplacement"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_DistributedReplacement"
    // Config.g:1153:1: parse_org_coolsoftware_reconfiguration_DistributedReplacement returns [org.coolsoftware.reconfiguration.DistributedReplacement element = null] : a0= 'replace' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) a5= 'with' (a6= TEXT ) a7= 'on' (a8= TEXT ) ;
    public final org.coolsoftware.reconfiguration.DistributedReplacement parse_org_coolsoftware_reconfiguration_DistributedReplacement() throws RecognitionException {
        org.coolsoftware.reconfiguration.DistributedReplacement element =  null;

        int parse_org_coolsoftware_reconfiguration_DistributedReplacement_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Config.g:1156:2: (a0= 'replace' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) a5= 'with' (a6= TEXT ) a7= 'on' (a8= TEXT ) )
            // Config.g:1157:2: a0= 'replace' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) a5= 'with' (a6= TEXT ) a7= 'on' (a8= TEXT )
            {
            a0=(Token)match(input,19,FOLLOW_19_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement781); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[79]);
            	}

            a1=(Token)match(input,10,FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement795); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[80]);
            	}

            // Config.g:1185:2: (a2= TEXT )
            // Config.g:1186:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement813); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_4, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[81]);
            	}

            a3=(Token)match(input,16,FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement834); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[82]);
            	}

            // Config.g:1235:2: (a4= TEXT )
            // Config.g:1236:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement852); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_8, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[83]);
            	}

            a5=(Token)match(input,22,FOLLOW_22_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement873); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[84]);
            	}

            // Config.g:1285:2: (a6= TEXT )
            // Config.g:1286:3: a6= TEXT
            {
            a6=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement891); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            				startIncompleteElement(element);
            			}
            			if (a6 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_12, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[85]);
            	}

            a7=(Token)match(input,16,FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement912); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_14, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[86]);
            	}

            // Config.g:1335:2: (a8= TEXT )
            // Config.g:1336:3: a8= TEXT
            {
            a8=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement930); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDistributedReplacement();
            				startIncompleteElement(element);
            			}
            			if (a8 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_15, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[87]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[88]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[89]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[90]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[91]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[92]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[93]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_org_coolsoftware_reconfiguration_DistributedReplacement_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_DistributedReplacement"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_Migration"
    // Config.g:1379:1: parse_org_coolsoftware_reconfiguration_Migration returns [org.coolsoftware.reconfiguration.Migration element = null] : a0= 'migrate' a1= 'SWComponent' (a2= TEXT ) a3= 'from' (a4= TEXT ) a5= 'to' (a6= TEXT ) ;
    public final org.coolsoftware.reconfiguration.Migration parse_org_coolsoftware_reconfiguration_Migration() throws RecognitionException {
        org.coolsoftware.reconfiguration.Migration element =  null;

        int parse_org_coolsoftware_reconfiguration_Migration_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Config.g:1382:2: (a0= 'migrate' a1= 'SWComponent' (a2= TEXT ) a3= 'from' (a4= TEXT ) a5= 'to' (a6= TEXT ) )
            // Config.g:1383:2: a0= 'migrate' a1= 'SWComponent' (a2= TEXT ) a3= 'from' (a4= TEXT ) a5= 'to' (a6= TEXT )
            {
            a0=(Token)match(input,15,FOLLOW_15_in_parse_org_coolsoftware_reconfiguration_Migration966); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[94]);
            	}

            a1=(Token)match(input,10,FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_Migration980); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[95]);
            	}

            // Config.g:1411:2: (a2= TEXT )
            // Config.g:1412:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Migration998); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_COMPONENT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_COMPONENT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_4, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[96]);
            	}

            a3=(Token)match(input,14,FOLLOW_14_in_parse_org_coolsoftware_reconfiguration_Migration1019); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[97]);
            	}

            // Config.g:1461:2: (a4= TEXT )
            // Config.g:1462:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Migration1037); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_CONTAINER), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__SOURCE_CONTAINER), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_8, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[98]);
            	}

            a5=(Token)match(input,20,FOLLOW_20_in_parse_org_coolsoftware_reconfiguration_Migration1058); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[99]);
            	}

            // Config.g:1511:2: (a6= TEXT )
            // Config.g:1512:3: a6= TEXT
            {
            a6=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Migration1076); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createMigration();
            				startIncompleteElement(element);
            			}
            			if (a6 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__TARGET_CONTAINER), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.MIGRATION__TARGET_CONTAINER), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_12, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[100]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[101]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[102]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[103]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[104]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[105]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[106]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_org_coolsoftware_reconfiguration_Migration_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_Migration"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_Deployment"
    // Config.g:1555:1: parse_org_coolsoftware_reconfiguration_Deployment returns [org.coolsoftware.reconfiguration.Deployment element = null] : a0= 'deploy' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) ;
    public final org.coolsoftware.reconfiguration.Deployment parse_org_coolsoftware_reconfiguration_Deployment() throws RecognitionException {
        org.coolsoftware.reconfiguration.Deployment element =  null;

        int parse_org_coolsoftware_reconfiguration_Deployment_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Config.g:1558:2: (a0= 'deploy' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT ) )
            // Config.g:1559:2: a0= 'deploy' a1= 'SWComponent' (a2= TEXT ) a3= 'on' (a4= TEXT )
            {
            a0=(Token)match(input,12,FOLLOW_12_in_parse_org_coolsoftware_reconfiguration_Deployment1112); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[107]);
            	}

            a1=(Token)match(input,10,FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_Deployment1126); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[108]);
            	}

            // Config.g:1587:2: (a2= TEXT )
            // Config.g:1588:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Deployment1144); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_COMPONENT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__SOURCE_COMPONENT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_4, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[109]);
            	}

            a3=(Token)match(input,16,FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_Deployment1165); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[110]);
            	}

            // Config.g:1637:2: (a4= TEXT )
            // Config.g:1638:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Deployment1183); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createDeployment();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__TARGET_CONTAINER), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.DEPLOYMENT__TARGET_CONTAINER), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_8, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[111]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[112]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[113]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[114]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[115]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[116]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[117]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_org_coolsoftware_reconfiguration_Deployment_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_Deployment"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_UnDeployment"
    // Config.g:1681:1: parse_org_coolsoftware_reconfiguration_UnDeployment returns [org.coolsoftware.reconfiguration.UnDeployment element = null] : a0= 'undeploy' a1= 'SWComponent' (a2= TEXT ) a3= 'from' (a4= TEXT ) ;
    public final org.coolsoftware.reconfiguration.UnDeployment parse_org_coolsoftware_reconfiguration_UnDeployment() throws RecognitionException {
        org.coolsoftware.reconfiguration.UnDeployment element =  null;

        int parse_org_coolsoftware_reconfiguration_UnDeployment_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Config.g:1684:2: (a0= 'undeploy' a1= 'SWComponent' (a2= TEXT ) a3= 'from' (a4= TEXT ) )
            // Config.g:1685:2: a0= 'undeploy' a1= 'SWComponent' (a2= TEXT ) a3= 'from' (a4= TEXT )
            {
            a0=(Token)match(input,21,FOLLOW_21_in_parse_org_coolsoftware_reconfiguration_UnDeployment1219); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[118]);
            	}

            a1=(Token)match(input,10,FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_UnDeployment1233); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[119]);
            	}

            // Config.g:1713:2: (a2= TEXT )
            // Config.g:1714:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_UnDeployment1251); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_COMPONENT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_COMPONENT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_4, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[120]);
            	}

            a3=(Token)match(input,14,FOLLOW_14_in_parse_org_coolsoftware_reconfiguration_UnDeployment1272); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[121]);
            	}

            // Config.g:1763:2: (a4= TEXT )
            // Config.g:1764:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_UnDeployment1290); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.reconfiguration.ReconfigurationFactory.eINSTANCE.createUnDeployment();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.reconfiguration.resource.config.IConfigTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_CONTAINER), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.UN_DEPLOYMENT__SOURCE_CONTAINER), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_7, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[122]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[123]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[124]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[125]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[126]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[127]);
            		addExpectedElement(org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectationConstants.EXPECTATIONS[128]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_org_coolsoftware_reconfiguration_UnDeployment_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_UnDeployment"



    // $ANTLR start "parse_org_coolsoftware_reconfiguration_ReconfigurationStep"
    // Config.g:1807:1: parse_org_coolsoftware_reconfiguration_ReconfigurationStep returns [org.coolsoftware.reconfiguration.ReconfigurationStep element = null] : (c0= parse_org_coolsoftware_reconfiguration_LocalReplacement |c1= parse_org_coolsoftware_reconfiguration_DistributedReplacement |c2= parse_org_coolsoftware_reconfiguration_Migration |c3= parse_org_coolsoftware_reconfiguration_Deployment |c4= parse_org_coolsoftware_reconfiguration_UnDeployment );
    public final org.coolsoftware.reconfiguration.ReconfigurationStep parse_org_coolsoftware_reconfiguration_ReconfigurationStep() throws RecognitionException {
        org.coolsoftware.reconfiguration.ReconfigurationStep element =  null;

        int parse_org_coolsoftware_reconfiguration_ReconfigurationStep_StartIndex = input.index();

        org.coolsoftware.reconfiguration.LocalReplacement c0 =null;

        org.coolsoftware.reconfiguration.DistributedReplacement c1 =null;

        org.coolsoftware.reconfiguration.Migration c2 =null;

        org.coolsoftware.reconfiguration.Deployment c3 =null;

        org.coolsoftware.reconfiguration.UnDeployment c4 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Config.g:1808:2: (c0= parse_org_coolsoftware_reconfiguration_LocalReplacement |c1= parse_org_coolsoftware_reconfiguration_DistributedReplacement |c2= parse_org_coolsoftware_reconfiguration_Migration |c3= parse_org_coolsoftware_reconfiguration_Deployment |c4= parse_org_coolsoftware_reconfiguration_UnDeployment )
            int alt7=5;
            switch ( input.LA(1) ) {
            case 19:
                {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==10) ) {
                    int LA7_5 = input.LA(3);

                    if ( (LA7_5==TEXT) ) {
                        int LA7_6 = input.LA(4);

                        if ( (LA7_6==16) ) {
                            int LA7_7 = input.LA(5);

                            if ( (LA7_7==TEXT) ) {
                                int LA7_8 = input.LA(6);

                                if ( (LA7_8==22) ) {
                                    int LA7_9 = input.LA(7);

                                    if ( (LA7_9==TEXT) ) {
                                        int LA7_10 = input.LA(8);

                                        if ( (LA7_10==EOF||LA7_10==12||LA7_10==15||LA7_10==19||LA7_10==21||(LA7_10 >= 23 && LA7_10 <= 24)) ) {
                                            alt7=1;
                                        }
                                        else if ( (LA7_10==16) ) {
                                            int LA7_12 = input.LA(9);

                                            if ( (LA7_12==TEXT) ) {
                                                int LA7_13 = input.LA(10);

                                                if ( (synpred7_Config()) ) {
                                                    alt7=1;
                                                }
                                                else if ( (synpred8_Config()) ) {
                                                    alt7=2;
                                                }
                                                else {
                                                    if (state.backtracking>0) {state.failed=true; return element;}
                                                    NoViableAltException nvae =
                                                        new NoViableAltException("", 7, 13, input);

                                                    throw nvae;

                                                }
                                            }
                                            else {
                                                if (state.backtracking>0) {state.failed=true; return element;}
                                                NoViableAltException nvae =
                                                    new NoViableAltException("", 7, 12, input);

                                                throw nvae;

                                            }
                                        }
                                        else {
                                            if (state.backtracking>0) {state.failed=true; return element;}
                                            NoViableAltException nvae =
                                                new NoViableAltException("", 7, 10, input);

                                            throw nvae;

                                        }
                                    }
                                    else {
                                        if (state.backtracking>0) {state.failed=true; return element;}
                                        NoViableAltException nvae =
                                            new NoViableAltException("", 7, 9, input);

                                        throw nvae;

                                    }
                                }
                                else {
                                    if (state.backtracking>0) {state.failed=true; return element;}
                                    NoViableAltException nvae =
                                        new NoViableAltException("", 7, 8, input);

                                    throw nvae;

                                }
                            }
                            else {
                                if (state.backtracking>0) {state.failed=true; return element;}
                                NoViableAltException nvae =
                                    new NoViableAltException("", 7, 7, input);

                                throw nvae;

                            }
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 7, 6, input);

                            throw nvae;

                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 7, 5, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 1, input);

                    throw nvae;

                }
                }
                break;
            case 15:
                {
                alt7=3;
                }
                break;
            case 12:
                {
                alt7=4;
                }
                break;
            case 21:
                {
                alt7=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;

            }

            switch (alt7) {
                case 1 :
                    // Config.g:1809:2: c0= parse_org_coolsoftware_reconfiguration_LocalReplacement
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_LocalReplacement_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1322);
                    c0=parse_org_coolsoftware_reconfiguration_LocalReplacement();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Config.g:1810:4: c1= parse_org_coolsoftware_reconfiguration_DistributedReplacement
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_DistributedReplacement_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1332);
                    c1=parse_org_coolsoftware_reconfiguration_DistributedReplacement();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Config.g:1811:4: c2= parse_org_coolsoftware_reconfiguration_Migration
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_Migration_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1342);
                    c2=parse_org_coolsoftware_reconfiguration_Migration();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Config.g:1812:4: c3= parse_org_coolsoftware_reconfiguration_Deployment
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_Deployment_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1352);
                    c3=parse_org_coolsoftware_reconfiguration_Deployment();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Config.g:1813:4: c4= parse_org_coolsoftware_reconfiguration_UnDeployment
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_UnDeployment_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1362);
                    c4=parse_org_coolsoftware_reconfiguration_UnDeployment();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_org_coolsoftware_reconfiguration_ReconfigurationStep_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_reconfiguration_ReconfigurationStep"

    // $ANTLR start synpred3_Config
    public final void synpred3_Config_fragment() throws RecognitionException {
        org.coolsoftware.reconfiguration.ReconfigurationStep a8_0 =null;


        // Config.g:1036:5: ( (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep ) )
        // Config.g:1036:5: (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
        {
        // Config.g:1036:5: (a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep )
        // Config.g:1037:6: a8_0= parse_org_coolsoftware_reconfiguration_ReconfigurationStep
        {
        pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_synpred3_Config567);
        a8_0=parse_org_coolsoftware_reconfiguration_ReconfigurationStep();

        state._fsp--;
        if (state.failed) return ;

        }


        }

    }
    // $ANTLR end synpred3_Config

    // $ANTLR start synpred7_Config
    public final void synpred7_Config_fragment() throws RecognitionException {
        org.coolsoftware.reconfiguration.LocalReplacement c0 =null;


        // Config.g:1809:2: (c0= parse_org_coolsoftware_reconfiguration_LocalReplacement )
        // Config.g:1809:2: c0= parse_org_coolsoftware_reconfiguration_LocalReplacement
        {
        pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_LocalReplacement_in_synpred7_Config1322);
        c0=parse_org_coolsoftware_reconfiguration_LocalReplacement();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred7_Config

    // $ANTLR start synpred8_Config
    public final void synpred8_Config_fragment() throws RecognitionException {
        org.coolsoftware.reconfiguration.DistributedReplacement c1 =null;


        // Config.g:1810:4: (c1= parse_org_coolsoftware_reconfiguration_DistributedReplacement )
        // Config.g:1810:4: c1= parse_org_coolsoftware_reconfiguration_DistributedReplacement
        {
        pushFollow(FOLLOW_parse_org_coolsoftware_reconfiguration_DistributedReplacement_in_synpred8_Config1332);
        c1=parse_org_coolsoftware_reconfiguration_DistributedReplacement();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred8_Config

    // Delegated rules

    public final boolean synpred7_Config() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_Config_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_Config() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_Config_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred3_Config() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred3_Config_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan115 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan129 = new BitSet(new long[]{0x0000000001020000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan158 = new BitSet(new long[]{0x0000000001020000L});
    public static final BitSet FOLLOW_24_in_parse_org_coolsoftware_reconfiguration_ReconfigurationPlan199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity228 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity242 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity256 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity274 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity295 = new BitSet(new long[]{0x0000000000289000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity324 = new BitSet(new long[]{0x0000000001289000L});
    public static final BitSet FOLLOW_24_in_parse_org_coolsoftware_reconfiguration_ReconfigurationActivity365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_parse_org_coolsoftware_reconfiguration_LocalReplacement394 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_LocalReplacement408 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_LocalReplacement426 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_LocalReplacement447 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_LocalReplacement465 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_parse_org_coolsoftware_reconfiguration_LocalReplacement486 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_LocalReplacement504 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_23_in_parse_org_coolsoftware_reconfiguration_LocalReplacement534 = new BitSet(new long[]{0x0000000001289000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_parse_org_coolsoftware_reconfiguration_LocalReplacement567 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_parse_org_coolsoftware_reconfiguration_LocalReplacement634 = new BitSet(new long[]{0x0000000001289000L});
    public static final BitSet FOLLOW_24_in_parse_org_coolsoftware_reconfiguration_LocalReplacement718 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_DistributedReplacement_in_parse_org_coolsoftware_reconfiguration_LocalReplacement756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement781 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement795 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement813 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement834 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement852 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement873 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement891 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement912 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_DistributedReplacement930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_parse_org_coolsoftware_reconfiguration_Migration966 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_Migration980 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Migration998 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_org_coolsoftware_reconfiguration_Migration1019 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Migration1037 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_org_coolsoftware_reconfiguration_Migration1058 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Migration1076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_parse_org_coolsoftware_reconfiguration_Deployment1112 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_Deployment1126 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Deployment1144 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_org_coolsoftware_reconfiguration_Deployment1165 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_Deployment1183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_parse_org_coolsoftware_reconfiguration_UnDeployment1219 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_10_in_parse_org_coolsoftware_reconfiguration_UnDeployment1233 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_UnDeployment1251 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_org_coolsoftware_reconfiguration_UnDeployment1272 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_reconfiguration_UnDeployment1290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_LocalReplacement_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_DistributedReplacement_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_Migration_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_Deployment_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_UnDeployment_in_parse_org_coolsoftware_reconfiguration_ReconfigurationStep1362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_ReconfigurationStep_in_synpred3_Config567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_LocalReplacement_in_synpred7_Config1322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_reconfiguration_DistributedReplacement_in_synpred8_Config1332 = new BitSet(new long[]{0x0000000000000002L});

}