/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class ConfigRule extends org.coolsoftware.reconfiguration.resource.config.grammar.ConfigSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public ConfigRule(org.eclipse.emf.ecore.EClass metaclass, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigChoice choice, org.coolsoftware.reconfiguration.resource.config.grammar.ConfigCardinality cardinality) {
		super(cardinality, new org.coolsoftware.reconfiguration.resource.config.grammar.ConfigSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public org.coolsoftware.reconfiguration.resource.config.grammar.ConfigChoice getDefinition() {
		return (org.coolsoftware.reconfiguration.resource.config.grammar.ConfigChoice) getChildren()[0];
	}
	
}

