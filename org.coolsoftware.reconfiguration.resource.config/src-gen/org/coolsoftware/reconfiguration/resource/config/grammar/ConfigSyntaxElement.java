/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.grammar;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class ConfigSyntaxElement {
	
	private ConfigSyntaxElement[] children;
	private ConfigSyntaxElement parent;
	private org.coolsoftware.reconfiguration.resource.config.grammar.ConfigCardinality cardinality;
	
	public ConfigSyntaxElement(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigCardinality cardinality, ConfigSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (ConfigSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	public void setParent(ConfigSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	public ConfigSyntaxElement[] getChildren() {
		if (children == null) {
			return new ConfigSyntaxElement[0];
		}
		return children;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public org.coolsoftware.reconfiguration.resource.config.grammar.ConfigCardinality getCardinality() {
		return cardinality;
	}
	
}
