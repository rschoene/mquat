/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.reconfiguration.resource.config.grammar;

/**
 * This class provides the follow sets for all terminals of the grammar. These
 * sets are used during code completion.
 */
public class ConfigFollowSetProvider {
	
	public final static org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement TERMINALS[] = new org.coolsoftware.reconfiguration.resource.config.IConfigExpectedElement[44];
	
	public final static org.eclipse.emf.ecore.EStructuralFeature[] FEATURES = new org.eclipse.emf.ecore.EStructuralFeature[3];
	
	public final static org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] LINKS = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[69];
	
	public final static org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] EMPTY_LINK_ARRAY = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[0];
	
	public static void initializeTerminals0() {
		TERMINALS[0] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_0);
		TERMINALS[1] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_1);
		TERMINALS[2] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_0);
		TERMINALS[3] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_0_0_0_4);
		TERMINALS[4] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_2);
		TERMINALS[5] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_4);
		TERMINALS[6] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_6);
		TERMINALS[7] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_8);
		TERMINALS[8] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_0);
		TERMINALS[9] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_0);
		TERMINALS[10] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_0);
		TERMINALS[11] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_0);
		TERMINALS[12] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_0);
		TERMINALS[13] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_1_0_0_10);
		TERMINALS[14] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_2);
		TERMINALS[15] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_4);
		TERMINALS[16] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_6);
		TERMINALS[17] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_8);
		TERMINALS[18] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_10);
		TERMINALS[19] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_12);
		TERMINALS[20] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_0);
		TERMINALS[21] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_2_0_0_13_0_0_2);
		TERMINALS[22] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_2);
		TERMINALS[23] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_4);
		TERMINALS[24] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_6);
		TERMINALS[25] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_8);
		TERMINALS[26] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_10);
		TERMINALS[27] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_12);
		TERMINALS[28] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_14);
		TERMINALS[29] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_3_0_0_15);
		TERMINALS[30] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_2);
		TERMINALS[31] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_4);
		TERMINALS[32] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_6);
		TERMINALS[33] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_8);
		TERMINALS[34] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_10);
		TERMINALS[35] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_4_0_0_12);
		TERMINALS[36] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_2);
		TERMINALS[37] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_4);
		TERMINALS[38] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_6);
		TERMINALS[39] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_5_0_0_8);
		TERMINALS[40] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_2);
		TERMINALS[41] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_4);
		TERMINALS[42] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedCsString(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_6);
		TERMINALS[43] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigExpectedStructuralFeature(org.coolsoftware.reconfiguration.resource.config.grammar.ConfigGrammarInformationProvider.CONFIG_6_0_0_7);
	}
	
	public static void initializeTerminals() {
		initializeTerminals0();
	}
	
	public static void initializeFeatures0() {
		FEATURES[0] = org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationPlan().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_PLAN__ACTIVITIES);
		FEATURES[1] = org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS);
		FEATURES[2] = org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationStep().getEStructuralFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.RECONFIGURATION_STEP__SUB_STEP);
	}
	
	public static void initializeFeatures() {
		initializeFeatures0();
	}
	
	public static void initializeLinks0() {
		LINKS[0] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity(), FEATURES[0]);
		LINKS[1] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity(), FEATURES[0]);
		LINKS[2] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity(), FEATURES[0]);
		LINKS[3] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[4] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[5] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[6] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[7] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[8] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[9] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[10] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[11] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[12] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[13] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[14] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[15] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[16] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[17] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[18] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity(), FEATURES[0]);
		LINKS[19] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[20] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[21] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[22] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[23] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[24] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[2]);
		LINKS[25] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[2]);
		LINKS[26] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[2]);
		LINKS[27] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[2]);
		LINKS[28] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[2]);
		LINKS[29] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[2]);
		LINKS[30] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[2]);
		LINKS[31] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[2]);
		LINKS[32] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[2]);
		LINKS[33] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[2]);
		LINKS[34] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[2]);
		LINKS[35] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[2]);
		LINKS[36] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[2]);
		LINKS[37] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[2]);
		LINKS[38] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[2]);
		LINKS[39] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[40] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[41] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[42] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[43] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[44] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[45] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[46] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[47] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[48] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[49] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[50] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[51] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[52] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[53] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[54] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[55] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[56] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[57] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[58] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[59] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[60] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[61] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[62] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[63] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
		LINKS[64] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]);
		LINKS[65] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]);
		LINKS[66] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]);
		LINKS[67] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]);
		LINKS[68] = new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]);
	}
	
	public static void initializeLinks() {
		initializeLinks0();
	}
	
	public static void wire0() {
		TERMINALS[0].addFollower(TERMINALS[1], EMPTY_LINK_ARRAY);
		TERMINALS[1].addFollower(TERMINALS[2], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity(), FEATURES[0]), });
		TERMINALS[1].addFollower(TERMINALS[3], EMPTY_LINK_ARRAY);
		TERMINALS[2].addFollower(TERMINALS[4], EMPTY_LINK_ARRAY);
		TERMINALS[4].addFollower(TERMINALS[5], EMPTY_LINK_ARRAY);
		TERMINALS[5].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[7].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]), });
		TERMINALS[7].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]), });
		TERMINALS[7].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]), });
		TERMINALS[7].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]), });
		TERMINALS[7].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]), });
		TERMINALS[13].addFollower(TERMINALS[2], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getReconfigurationActivity(), FEATURES[0]), });
		TERMINALS[13].addFollower(TERMINALS[3], EMPTY_LINK_ARRAY);
		TERMINALS[8].addFollower(TERMINALS[14], EMPTY_LINK_ARRAY);
		TERMINALS[14].addFollower(TERMINALS[15], EMPTY_LINK_ARRAY);
		TERMINALS[15].addFollower(TERMINALS[16], EMPTY_LINK_ARRAY);
		TERMINALS[16].addFollower(TERMINALS[17], EMPTY_LINK_ARRAY);
		TERMINALS[17].addFollower(TERMINALS[18], EMPTY_LINK_ARRAY);
		TERMINALS[18].addFollower(TERMINALS[19], EMPTY_LINK_ARRAY);
		TERMINALS[19].addFollower(TERMINALS[20], EMPTY_LINK_ARRAY);
		TERMINALS[19].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]), });
		TERMINALS[19].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]), });
		TERMINALS[19].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]), });
		TERMINALS[19].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]), });
		TERMINALS[19].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]), });
		TERMINALS[19].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[19].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
		TERMINALS[20].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[2]), });
		TERMINALS[20].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[2]), });
		TERMINALS[20].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[2]), });
		TERMINALS[20].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[2]), });
		TERMINALS[20].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[2]), });
		TERMINALS[20].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
		TERMINALS[21].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]), });
		TERMINALS[21].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]), });
		TERMINALS[21].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]), });
		TERMINALS[21].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]), });
		TERMINALS[21].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]), });
		TERMINALS[21].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[21].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[22], EMPTY_LINK_ARRAY);
		TERMINALS[22].addFollower(TERMINALS[23], EMPTY_LINK_ARRAY);
		TERMINALS[23].addFollower(TERMINALS[24], EMPTY_LINK_ARRAY);
		TERMINALS[24].addFollower(TERMINALS[25], EMPTY_LINK_ARRAY);
		TERMINALS[25].addFollower(TERMINALS[26], EMPTY_LINK_ARRAY);
		TERMINALS[26].addFollower(TERMINALS[27], EMPTY_LINK_ARRAY);
		TERMINALS[27].addFollower(TERMINALS[28], EMPTY_LINK_ARRAY);
		TERMINALS[28].addFollower(TERMINALS[29], EMPTY_LINK_ARRAY);
		TERMINALS[29].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]), });
		TERMINALS[29].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]), });
		TERMINALS[29].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]), });
		TERMINALS[29].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]), });
		TERMINALS[29].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]), });
		TERMINALS[29].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[29].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[30], EMPTY_LINK_ARRAY);
		TERMINALS[30].addFollower(TERMINALS[31], EMPTY_LINK_ARRAY);
		TERMINALS[31].addFollower(TERMINALS[32], EMPTY_LINK_ARRAY);
		TERMINALS[32].addFollower(TERMINALS[33], EMPTY_LINK_ARRAY);
		TERMINALS[33].addFollower(TERMINALS[34], EMPTY_LINK_ARRAY);
		TERMINALS[34].addFollower(TERMINALS[35], EMPTY_LINK_ARRAY);
		TERMINALS[35].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]), });
		TERMINALS[35].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]), });
		TERMINALS[35].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]), });
		TERMINALS[35].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]), });
		TERMINALS[35].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]), });
		TERMINALS[35].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[35].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
		TERMINALS[11].addFollower(TERMINALS[36], EMPTY_LINK_ARRAY);
		TERMINALS[36].addFollower(TERMINALS[37], EMPTY_LINK_ARRAY);
		TERMINALS[37].addFollower(TERMINALS[38], EMPTY_LINK_ARRAY);
		TERMINALS[38].addFollower(TERMINALS[39], EMPTY_LINK_ARRAY);
		TERMINALS[39].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]), });
		TERMINALS[39].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]), });
		TERMINALS[39].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]), });
		TERMINALS[39].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]), });
		TERMINALS[39].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]), });
		TERMINALS[39].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[39].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
		TERMINALS[12].addFollower(TERMINALS[40], EMPTY_LINK_ARRAY);
		TERMINALS[40].addFollower(TERMINALS[41], EMPTY_LINK_ARRAY);
		TERMINALS[41].addFollower(TERMINALS[42], EMPTY_LINK_ARRAY);
		TERMINALS[42].addFollower(TERMINALS[43], EMPTY_LINK_ARRAY);
		TERMINALS[43].addFollower(TERMINALS[8], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getLocalReplacement(), FEATURES[1]), });
		TERMINALS[43].addFollower(TERMINALS[9], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDistributedReplacement(), FEATURES[1]), });
		TERMINALS[43].addFollower(TERMINALS[10], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getMigration(), FEATURES[1]), });
		TERMINALS[43].addFollower(TERMINALS[11], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getDeployment(), FEATURES[1]), });
		TERMINALS[43].addFollower(TERMINALS[12], new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature[] {new org.coolsoftware.reconfiguration.resource.config.mopp.ConfigContainedFeature(org.coolsoftware.reconfiguration.ReconfigurationPackage.eINSTANCE.getUnDeployment(), FEATURES[1]), });
		TERMINALS[43].addFollower(TERMINALS[13], EMPTY_LINK_ARRAY);
		TERMINALS[43].addFollower(TERMINALS[21], EMPTY_LINK_ARRAY);
	}
	
	public static void wire() {
		wire0();
	}
	
	static {
		// initialize the arrays
		initializeTerminals();
		initializeFeatures();
		initializeLinks();
		// wire the terminals
		wire();
	}
}
