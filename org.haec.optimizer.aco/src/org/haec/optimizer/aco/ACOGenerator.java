/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.Optimizer;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.GeneratorUtil;
import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.mquat.MQuATAnt;
import org.haec.optimizer.aco.mquat.MQuATGraph;
import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.HardwareComponent;
import org.haec.optimizer.aco.mquat.bo.Implementation;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.QualityMode.Comparator;
import org.haec.optimizer.aco.mquat.bo.Requirement;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.haec.theatre.utils.CollectionsUtil;

/**
 * Ant colony optimizer.
 * @author Sebastian Götz
 */
public class ACOGenerator implements Optimizer {
	
	private static Map<String, Set<HWComponentRequirementClause>> resourceReqs = new HashMap<String, Set<HWComponentRequirementClause>>();
	private static Map<String, Map<String,CcmValue>> providedProperties = new HashMap<String, Map<String,CcmValue>>();
	private static Map<String, Map<String,CcmValue>> requiredProperties = new HashMap<String, Map<String,CcmValue>>();
	
	public static final String OPTION_ANTS = "aco.ants";
	public static final String OPTION_ITERS = "aco.iters";
	//XXX real default value?
	private static final Integer defaultAnts = 1;
	//XXX real default value?
	private static final Integer defaultIters = 1;

	public static Mapping generateAndRunACOForRequest(
			Request req,  Map<String,EclFile> eclFiles, int numAnts, int iterations,
			boolean verbose, IGlobalEnergyManager gem, String appName) {
		double initPheromone = 10;
		
		Mapping ret = new Mapping();
		
		long start = System.nanoTime();
		
		resourceReqs = new HashMap<String, Set<HWComponentRequirementClause>>();
		providedProperties = new HashMap<String, Map<String,CcmValue>>();
		requiredProperties = new HashMap<String, Map<String,CcmValue>>();

		Platform platform = req.getHardware();
		VariantModel vm = platform.getHwmodel();
		Resource root = (Resource) vm.getRoot();
		
		MQuATGraph graph = new MQuATGraph();
		ACOComponent groot = new ACOComponent("root", 0);
		graph.addComponent(groot);
		
		// get all servers
		Map<String, Resource> containers = new HashMap<String, Resource>();
		for (Resource r : root.getSubresources()) {
			containers.put(r.getName(), r);
		}		
		
		Map<String,SoftwareComponent> compTypes = new HashMap<String,SoftwareComponent>();
		Map<String,HardwareComponent> hwTypes = new HashMap<String,HardwareComponent>();
		Map<String,Container> servers = new HashMap<String,Container>();
		
		for(String sName : containers.keySet()) {
			Container server = addServerToGraph(initPheromone, graph, groot,
					containers, servers, sName);
					
			EclFile e = eclFiles.get(sName);
			
			// get all software component types
			for(EclContract c : e.getContracts()) {
				addImplementationToGraph(req, gem, appName, initPheromone,
						graph, groot, compTypes, hwTypes, sName, server, servers, c);
				
			}
		}
		
		//make some Ants and let things role :D
		Requirement req1 = new Requirement(compTypes.get(req.getComponent().getName()));
		for(PropertyRequirementClause prc : req.getReqs()) {
			String propName = prc.getRequiredProperty().getDeclaredVariable().getName();
			CcmReal val = evaluateRequirementClause(req, prc);
			req1.addRequirementClause(propName, val.getRealValue(), (val.isAscending() ? Comparator.LET : Comparator.GET));
		}
		 
		AntColony colony = new AntColony(graph);
		
		for(int a = 0; a < numAnts; a++) {
			MQuATAnt ant = new MQuATAnt("ant"+(a+1), graph, groot, 200, iterations);
			ant.addInitRequirement(req1);
			colony.addAnt(ant);
		}
		
		colony.setVerbose(verbose);
		System.out.println("###");
		colony.scheduleActivities(iterations);
		
		long stop = System.nanoTime();
		
		System.out.println("Optimization took: "+(stop-start)/10000000+" ms");
		
		System.out.println("Final Graph:");
		System.out.println("============");
		System.out.println(colony.getGraph());
		
		System.out.println("Optimal Configuration:");
		System.out.println("======================");
		MQuATGraph g = (MQuATGraph)colony.getGraph();
		for(String str : g.getOptimalMapping()) {
			System.out.println(str);
			String[] parts = str.split("@");
			String server = parts[1];
			String impl = parts[0].split("#")[0];
			ret.put(impl, server);
		}
		
		return ret;
	}

	private static void addImplementationToGraph(Request req, IGlobalEnergyManager gem,
			String appName, double initPheromone, MQuATGraph graph,
			ACOComponent groot, Map<String, SoftwareComponent> compTypes,
			Map<String, HardwareComponent> hwTypes, String sName,
			Container server, Map<String, Container> servers, EclContract c) {
		//deptype,reftype,container,contract
		Map<String, Map<String, Map<String, EclFile>>> deps = new HashMap<String, Map<String, Map<String, EclFile>>>();
		
		if(c instanceof SWComponentContract) {
			setMetaparameters(req, c);
			
			SWComponentContract swc = (SWComponentContract)c;
			SWComponentType swcomp = swc.getComponentType();
			//get or create node for component type
			SoftwareComponent sc = compTypes.get(swcomp.getName());
			if(sc == null) {
				sc = new SoftwareComponent(graph, swcomp.getName(), initPheromone);
				graph.addComponent(sc);
				graph.connect(groot, sc, initPheromone);
				compTypes.put(swcomp.getName(), sc);
			}
			//get or create impl (i.e., this contract)
			Implementation impl = sc.getImplementationByName(c.getName());
			if(impl == null) {
				impl = sc.addImplementation(c.getName());
			}
			
			for(ContractMode cm : swc.getModes()) {
				if(cm instanceof SWContractMode) {
					SWContractMode scm = (SWContractMode)cm;
					//Modes have different requirements/provisions for different containers! -> always create a new mode 
					QualityMode qm = impl.addQualityMode(scm.getName()+"@"+sName, 0); //XXX check whether 0 is correct
					graph.addComponent(qm);
					graph.connect(impl, qm, initPheromone);
					
					for(SWContractClause cl : ((SWContractMode)scm).getClauses()) {
						if(cl instanceof ProvisionClause) {
							handleProvisionClause(req, sName, sc, qm,
									cl);
						} else if (cl instanceof SWComponentRequirementClause) {
							handleSWRequirementClause(req, gem,
									appName, initPheromone, graph,
									compTypes, sName, sc, qm, cl, deps);
						} else if (cl instanceof HWComponentRequirementClause) {
							handleHWRequirementClause(req,
									initPheromone, graph, hwTypes,
									sName, sc, qm, cl);
						}
					}
					//connect mode and server
					graph.connect(qm, server, initPheromone);
				}
			}
		}
		//handle deps (type,container,qcl)
		for(Map<String,Map<String,EclFile>> depContracts : deps.values()) {
			for(String depType : depContracts.keySet()) {
				for(String depContainer : depContracts.get(depType).keySet()) {
					EclFile qclFile = depContracts.get(depType).get(depContainer);
					for(EclContract ctr : qclFile.getContracts()) {
						//TODO req needs to be adjusted! (reqs defined in clause)
						addImplementationToGraph(req, gem, appName, initPheromone, graph, groot, compTypes, hwTypes, depContainer, servers.get(depContainer), servers, ctr);
					}
					
				}
			}
		}
	}

	private static void setMetaparameters(Request req, EclContract c) {
		for(Parameter mp : ((SWComponentContract) c).getPort().getMetaparameter()) {
			for(MetaParamValue mpv : req.getMetaParamValues()) {
				if(mpv.getMetaparam().getName().equals(mp.getName())) {
					RealLiteralExpression ile = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
					ile.setValue(mpv.getValue());
					mp.setValue(new CcmExpressionInterpreter().interpret(ile));
				}
			}
		}
	}

	private static Container addServerToGraph(double initPheromone,
			MQuATGraph graph, ACOComponent groot,
			Map<String, Resource> containers, Map<String, Container> servers,
			String sName) {
		Container server = servers.get(sName);
		if(server == null) {
			server = new Container(graph, sName, initPheromone);
			graph.addComponent(server);
			graph.connect(server.getOutPort(), groot, initPheromone);

			Resource res = containers.get(sName);
			for(Resource sres : res.getSubresources()) {
				HardwareComponent hc = new HardwareComponent(graph, sres.getName(), initPheromone);
				//set provisions
				for(VariantPropertyBinding vpb : sres.getPropertyBinding()) {
					String pname = vpb.getProperty()
							.getDeclaredVariable().getName();
					CcmExpressionInterpreter interpreter = new CcmExpressionInterpreter();
					CcmValue val = interpreter.interpret(vpb
							.getValueExpression());
					
					if(val instanceof CcmString) continue;
					if(val instanceof CcmInteger)
						hc.addProvision(pname, (double)((CcmInteger)val).getIntegerValue());
					else
					if(val instanceof CcmReal)
						hc.addProvision(pname, ((CcmReal)val).getRealValue());
				}
				server.addResource(hc, true);
			}
			servers.put(sName, server);
		}
		return server;
	}

	private static void handleHWRequirementClause(Request req,
			double initPheromone, MQuATGraph graph,
			Map<String, HardwareComponent> hwTypes, String sName,
			SoftwareComponent sc, QualityMode qm, SWContractClause cl) {
		ResourceType t = ((HWComponentRequirementClause) cl)
				.getRequiredResourceType();
		HardwareComponent dc = hwTypes.get(t.getName());
		if(dc == null) {
			dc = new HardwareComponent(graph, t.getName(), initPheromone);
			hwTypes.put(t.getName(), dc);
		}

		for(PropertyRequirementClause prc : ((HWComponentRequirementClause) cl).getRequiredProperties()) {
			CcmReal val = evaluateRequirementClause(
					req, prc);
			String propName = prc.getRequiredProperty().getDeclaredVariable().getName();
			Map<String,CcmValue> reqVal = requiredProperties.get(propName);
			if(reqVal == null) reqVal = new HashMap<String,CcmValue>();
			reqVal.put(sc.getName()+"#"+sName, val);
			requiredProperties.put(propName, reqVal);
			
			qm.addHWRequirementClause(dc.getName(), propName, val.getRealValue(), (val.isAscending() ? Comparator.GET : Comparator.LET));
		}
	}

	private static void handleSWRequirementClause(Request req, IGlobalEnergyManager gem,
			String appName, double initPheromone, MQuATGraph graph,
			Map<String, SoftwareComponent> compTypes, String sName,
			SoftwareComponent sc, QualityMode qm, SWContractClause cl, Map<String, Map<String, Map<String, EclFile>>> deps) {
		SWComponentType reftype = ((SWComponentRequirementClause) cl)
				.getRequiredComponentType();
		SoftwareComponent dc = compTypes.get(reftype.getName());
		if(dc == null) dc = new SoftwareComponent(graph, reftype.getName(), initPheromone);
		
		try {
			Map<String,String> serializedContractByContainer = gem.getContractsForComponent(appName, reftype.getName());
			Map<String, EclFile> referrencedContractByContainer = new HashMap<String, EclFile>();
			StructuralModel appModel = CCMUtil.deserializeSWStructModel(appName, gem.getApplicationModel(appName), false);
			StructuralModel hwModel = CCMUtil.deserializeHWStructModel(
					((IGlobalResourceManager) gem.getGlobalResourceManager()).getInfrastructureTypes(), false);
			for(String container : serializedContractByContainer.keySet()) {
				String serialContract = serializedContractByContainer.get(container);
				EclFile f = CCMUtil.deserializeECLContract(appModel, hwModel, appName, reftype.getName(), serialContract, false);
				referrencedContractByContainer.put(container, f);
			}
			Map<String, Map<String, EclFile>> dep = new HashMap<String, Map<String,EclFile>>();
			dep.put(reftype.getName(), referrencedContractByContainer);
			/** will be interpreted in addServerToGraph */
			deps.put(sc.getName(), dep);
		} catch(IOException ioe) {
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
		
		for(PropertyRequirementClause prc : ((SWComponentRequirementClause) cl).getRequiredProperties()) {
			CcmReal val = evaluateRequirementClause(
					req, prc);
			String propName = prc.getRequiredProperty().getDeclaredVariable().getName();
			Map<String,CcmValue> reqVal = requiredProperties.get(propName);
			if(reqVal == null) reqVal = new HashMap<String,CcmValue>();
			reqVal.put(sc.getName()+"#"+sName, val);
			requiredProperties.put(propName, reqVal);
			
			qm.addSWRequirementClause(dc, propName, val.getRealValue(), (val.isAscending() ? Comparator.GET : Comparator.LET));
		}
	}

	private static void handleProvisionClause(Request req, String sName,
			SoftwareComponent sc, QualityMode qm, SWContractClause cl) {
		CcmReal val = null;
		boolean min = true;
		Statement minmaxval = ((ProvisionClause)cl).getMinValue();
		if(minmaxval == null) {
			minmaxval = ((ProvisionClause)cl).getMaxValue();
			min = false;
		}
		if(minmaxval instanceof FunctionExpression) {
			val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min)
				((ProvisionClause)cl).setMinValue(rle);
			else 
				((ProvisionClause)cl).setMaxValue(rle);
		} else if (((ProvisionClause)cl).getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmReal)new CcmExpressionInterpreter().interpret(rle);
		} else {
			val = (CcmReal)new CcmExpressionInterpreter().interpret(minmaxval);
		}
		if(((ProvisionClause)cl).getProvidedProperty().getValOrder() == Order.INCREASING) {
			if(min) { val.setAscending(true); } else { val.setAscending(false); }
		} else {
			if(min) { val.setAscending(false); } else { val.setAscending(true); }
		}
		
		
		String propName = ((ProvisionClause)cl).getProvidedProperty().getDeclaredVariable().getName();
		Map<String,CcmValue> propVal = providedProperties.get(propName);
		if(propVal == null) propVal = new HashMap<String,CcmValue>();
		propVal.put(sc.getName()+"#"+sName, val);
		providedProperties.put(propName, propVal);
		qm.addProvision(propName, val.getRealValue(), (min ? Comparator.LET : Comparator.GET));
	}

	/**
	 * @param req
	 * @param prc
	 * @return
	 */
	private static CcmReal evaluateRequirementClause(Request req,
			PropertyRequirementClause prc) {
		boolean min = true;
		Statement minmaxval = prc.getMinValue();
		if(minmaxval == null) {
			minmaxval = prc.getMaxValue();
			min = false;
		}
		CcmReal val;
		if(minmaxval instanceof FunctionExpression) {
			val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min)	prc.setMinValue(rle);
			else prc.setMaxValue(rle);
		} else if (prc.getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmReal)new CcmExpressionInterpreter().interpret(rle);
		}	else {
			val = (CcmReal)new CcmExpressionInterpreter().interpret(minmaxval);
		}
		if(prc.getRequiredProperty().getValOrder() == Order.INCREASING) {
			if(min) { val.setAscending(true); } else { val.setAscending(false); }
		} else {
			if(min) { val.setAscending(false); } else { val.setAscending(true); }
		}
		return val;
	}

	@Override
	public Mapping optimize(Object[] args) {
		try {
			Request req = (Request)args[0];
			@SuppressWarnings("unchecked")
			Map<String, EclFile> eclFiles = (Map<String, EclFile>)args[1];
			IGlobalEnergyManager gem = (IGlobalEnergyManager)args[2];
			@SuppressWarnings("unchecked")
			Map<String, Object> options = (Map<String, Object>) args[3];
			Boolean verbose = CollectionsUtil.get(options, OPTION_VERBOSE, false);
			String appName = (String) options.get(OPTION_APPNAME);
			Map<String, List<String>> fileNameMap = CollectionsUtil.get(options, OPTION_RESOURCENAMES, new HashMap<String, List<String>>());
//			problemName = CollectionsUtil.get(options, OPTION_PROBLEMNAME, appName.replace(',', '_'));
			int ants = CollectionsUtil.get(options, OPTION_ANTS, defaultAnts);
			int iters = CollectionsUtil.get(options, OPTION_ITERS, defaultIters);
			return generateAndRunACOForRequest(req, eclFiles, ants, iters, verbose, gem, appName);
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.Optimizer#getId()
	 */
	@Override
	public String getId() {
		return "org.haec.optimizer.aco";
	}
}
