/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.graph.ACOConstraint;
import org.haec.optimizer.aco.graph.ACOLink;

/**
 * An ant, which runs through the graph and deposits and/or eats pheromone.
 * 
 * @author Sebastian Goetz
 * @date 14.12.2011
 */
public class Ant {
	/** name of the ant */
	protected String name;
	/** maximum amount of pheromne the ant can deposit */
	protected double maxPheromone;

	/** the graph the ant is running on */
	private AntGraph graph;

	/** the current position (component in the graph) of the ant */
	private ACOComponent currentPosition;
	/** the start position (component in the graph) of the ant */
	private ACOComponent startPosition;
	/** the stop position (component in the graph) of the ant */
	private ACOComponent stopPosition;

	/** tells, whether the ant searches for the shortest or longest path */
	private boolean longestPath;

	private boolean failed = false;

	/**
	 * the current level of the the ant (standard: number of steps the ant has
	 * performed)
	 */
	protected int currentLevel;

	/** the trail of the ant through the graph */
	private Set<ACOLink> trail;
	
	/** Verbosity (i.e., log a lot or not) */
	protected boolean verbose = false;

	/**
	 * Move the ant, if it has not already stopped.
	 * 
	 * @return true if the ant has not yet stopped
	 */
	public boolean move() {
		boolean done = stop();
		if (!done) {
			ACOLink l = getNextMove();
			if (l != null) {
				performMove(l);
				trail.add(l);
//				this.currentPosition.incPheromone(1);
			}
		}
		return !done;
	}

	/**
	 * Initialize a new ant with a stop position as stop criteria.
	 * 
	 * @param name
	 *            name of the ant
	 * @param graph
	 *            graph to be used
	 * @param startPosition
	 *            start position to be used
	 * @param maxPheromone
	 *            maximum pheromone the ant can deposit on a single component
	 * @param stop
	 *            stop position to be reached by the ant
	 */
	public Ant(String name, AntGraph graph, ACOComponent startPosition,
			double maxPheromone, ACOComponent stop) {
		this(name, graph, startPosition, maxPheromone);
		this.stopPosition = stop;
	}

	/**
	 * Initialize a new ant without stop position (another stop criteria is
	 * needed).
	 * 
	 * @param name
	 *            name of the ant
	 * @param graph
	 *            the graph to be used
	 * @param startPosition
	 *            the start position to be used
	 * @param maxPheromone
	 *            maximum pheromone the ant can deposit on a single component
	 */
	public Ant(String name, AntGraph graph, ACOComponent startPosition,
			double maxPheromone) {
		this.name = name;
		this.graph = graph;
		this.currentPosition = startPosition;
		this.startPosition = startPosition;
		this.maxPheromone = maxPheromone;
		currentLevel = 1;
		this.trail = new HashSet<ACOLink>();
	}

	/**
	 * Randomly chooses a feasible move, based on the pheromone level of the
	 * trail. Override for more sophisticated path finding.
	 * 
	 * @return a link to be used as the next move
	 */
	protected ACOLink getNextMove() {
		List<ACOLink> feasibleMoves = getNextFeasibleMoves();
		if (feasibleMoves.size() > 0) {
			// get pheromone of each feasible move
			double totalPheromone = 0;
			double trailPheromone[] = new double[feasibleMoves.size()];
			int i = 0;
			for (ACOLink f : feasibleMoves) {
				totalPheromone += f.getPheromone();
				trailPheromone[i++] = totalPheromone;
			}
			double randomNumber = Math.random() * totalPheromone;
			if(verbose) {
				System.out.print(getCurrentPosition() + " [");
				for (ACOLink f : feasibleMoves)
					System.out.print(f.getPheromone() + " ");
				System.out.print("]");
				System.out.print(" | " + randomNumber);
			}
				for (i = 0; i < trailPheromone.length; i++) {
					if (randomNumber <= trailPheromone[i])
						break;
				}
			if(verbose) System.out.println(" | " + i);
			// int idx = AntColony.getRandomIntBetween(0, feasibleMoves.size());
			return feasibleMoves.get(i);
		} else {
			return null;
		}
	}

	/**
	 * Compute pheromone strength / amount of pheromone to be placed by the ant
	 * at a certain level.<br/>
	 * Standard behavior: maxPheromone * (1 / level)
	 * 
	 * @param level
	 *            could be used for steps in the graph
	 * @return pheromone strength / amount
	 */
	protected double pheromoneStrength(int level) {
		return maxPheromone;
	}

	/**
	 * Stop criteria for the ant. Standard implementation: stop after 10 moves /
	 * at level 10.
	 * 
	 * @return to stop or not to stop
	 */
	protected boolean stop() {
		if (stopPosition != null)
			if (currentPosition == stopPosition)
				return true;
			else
				return false;
		else if (currentLevel < 10)
			return false;
		else
			return true;
	}

	/**
	 * Get the next feasible moves by evaluating the constraints on all existing
	 * links from the current component.
	 * 
	 * @return list of feasible links, to use as moves
	 */
	protected List<ACOLink> getNextFeasibleMoves() {
		List<ACOLink> allMoves = currentPosition.getOutgoingLinks();
		List<ACOLink> feasibleMoves = allMoves;
		for (ACOLink l : allMoves) {
			for (ACOConstraint c : graph.getConstraints()) {
				if (!c.evaluate(l)) {
					feasibleMoves.remove(l);
				}
			}
		}
		return feasibleMoves;
	}

	/**
	 * Perform a move in the graph. Increases the pheromones on link and target
	 * component. Updates the currentPosition of the ant. Increases the current
	 * level of the ant.
	 * 
	 * @param l
	 *            the link to use for the move
	 * @return current position after performing the move
	 */
	protected ACOComponent performMove(ACOLink l) {
		if (l != null) {
			assert l.getSource().equals(currentPosition);
			//this.currentPosition.incPheromone(1);
			this.currentPosition = l.getTarget();
			//l.incPheromone(pheromoneStrength(currentLevel));
			if(verbose)
			System.out
					.println(this + " goes from " + l.getSource() + " to "
							+ l.getTarget() + " pheromoneStrength: "
							+ l.getPheromone());
		}
		currentLevel++;
		return this.currentPosition;
	}

	/**
	 * Resets the level and current position of the ant.
	 */
	public void reset() {
		this.currentLevel = 1;
		this.currentPosition = startPosition;
		this.trail = new HashSet<ACOLink>();
		this.failed = false;
	}

	protected ACOComponent getCurrentPosition() {
		return currentPosition;
	}

	protected AntGraph getGraph() {
		return graph;
	}

	/**
	 * Get the trail of the ant.
	 * 
	 * @return the way of the ant through the graph.
	 */
	protected Set<ACOLink> getTrail() {
		return trail;
	}

	/** Update the pheromones of trail. Invoke after ant found a solution */
	protected void updatePheromoneTrail() {
		if (isFailed()) {
			for (ACOLink l : getTrail()) {
				ACOComponent x = l.getTarget();
				x.setPheromone(x.getPheromone()-1);
				l.setPheromone(l.getPheromone()-pheromoneStrength(currentLevel));
				if(l.getPheromone() < 0) l.setPheromone(0);
			}
			if(verbose)
				System.out.println(name+"[failed]:: "+getTrail());
		} else {
			for (ACOLink l : getTrail()) {

				int distance = this.getTrail().size();
				// here we consider shortest versus longest path
				if (longestPath) {
					l.setPheromone(l.getPheromone() / (distance * 1.5));
					l.getSource().incPheromone(distance * 1.5);
				} else {
					l.setPheromone(l.getPheromone() / (distance * 0.5));
					l.getSource().incPheromone(distance * 0.5);
				}
				if (l.getPheromone() < 1)
					l.setPheromone(1);
				if (l.getSource().getPheromone() < 1)
					l.getSource().setPheromone(1);
			}
			if(verbose)
				System.out.println(name+"[success]:: "+getTrail());
		}
	}

	/**
	 * Tells, whether the ant search for the shortest or longest path.
	 * 
	 * @return true if the ant searches for longest path, false otherwise
	 */
	public boolean isLongestPath() {
		return longestPath;
	}

	/**
	 * Set whether the ant search for the shortest or longest path.
	 * 
	 * @param longestPath
	 *            true if the ant shall search for longest path, false otherwise
	 */
	public void setLongestPath(boolean longestPath) {
		this.longestPath = longestPath;
	}

	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	@Override
	public String toString() {

		return name;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;		
	}
}
