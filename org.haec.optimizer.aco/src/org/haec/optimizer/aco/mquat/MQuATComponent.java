/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat;

import java.util.HashMap;
import java.util.Map;

import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.QualityMode.Comparator;

/**
 * A node modelling a MQuAT component.
 * @author Sebastian Götz
 */
public class MQuATComponent extends ACOComponent {
	public AntGraph g;
	public ACOComponent out;
	
	private Map<String, Map<Double, Comparator>> provisions;
	
	public MQuATComponent(AntGraph g, String name, double initPheromone) {
		super(name, initPheromone);
		this.g = g;
		out = new ACOComponent(name+"Out", 0);
		this.g.addComponent(out);
		provisions = new HashMap<String, Map<Double,Comparator>>();
	}
	
	public ACOComponent getOutPort() {
		return out;
	}
	
	public void addProvision(String propertyName, Double value, Comparator cmp) {
		Map<Double, Comparator> clauses = provisions.get(propertyName);
		if(clauses == null) clauses = new HashMap<Double, QualityMode.Comparator>();
		clauses.put(value, cmp);
		provisions.put(propertyName, clauses);
	}
	
	public Map<String, Map<Double, Comparator>> getProvisions() {
		return provisions;
	}

}
