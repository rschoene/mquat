/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat.bo;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.mquat.MQuATComponent;

/**
 * A special component modelling a mode of a contract.
 * @author Sebastian Götz
 */
public class QualityMode extends MQuATComponent implements Comparable<QualityMode> {
	
	public enum Comparator { LT, LET, EQ, GET, GT }

	private Map<SoftwareComponent, Requirement> swReqs;
	private Map<String, Requirement> hwReqs;
	private double weight;
	
	public QualityMode(AntGraph g, String name, double initPheromone) {
		super(g, name, initPheromone);
		swReqs = new HashMap<SoftwareComponent, Requirement>();
		hwReqs = new HashMap<String, Requirement>();
	}
	
	public void addSWRequirementClause(SoftwareComponent c, String propertyName, Double value, Comparator cmp) {
		Requirement r = swReqs.get(c);
		if(r == null) r = new Requirement(c);
		r.addRequirementClause(propertyName, value, cmp);
		swReqs.put(c, r);
	}
	
	public void addHWRequirementClause(String resource, String propertyName, Double value, Comparator cmp) {
		Requirement r = hwReqs.get(resource);
		if(r == null) r = new Requirement(resource);
		r.addRequirementClause(propertyName, value, cmp);
		hwReqs.put(resource, r);
	}
	
	public Collection<Requirement> getSWRequirements() {
		return swReqs.values();
	}
	
	public Collection<Requirement> getHWRequirements() {
		return hwReqs.values();
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}

	@Override
	public int compareTo(QualityMode o) {
		if(this.getPheromone() < o.getPheromone()) return -1;
		if(this.getPheromone() == o.getPheromone()) return 0;
		if(this.getPheromone() > o.getPheromone()) return 1;
		return 0;
	}
}
