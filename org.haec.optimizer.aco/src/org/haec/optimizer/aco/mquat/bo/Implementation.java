/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat.bo;

import java.util.HashSet;
import java.util.Set;

import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.graph.ACOComponent;

/**
 * A special component modelling a software implementation.
 * @author Sebastian Götz
 */
public class Implementation extends ACOComponent {
	private AntGraph g;
	private ACOComponent out;
	private Set<QualityMode> modes;

	public Implementation(AntGraph g, String name, double initPheromone, ACOComponent out) {
		super(name, initPheromone);
		this.g = g;
		this.out = out;
		modes = new HashSet<QualityMode>();
	}
	
	public QualityMode addQualityMode(String name, int pheromone) {
		QualityMode m = new QualityMode(g, name, pheromone);
		g.addComponent(m);
		//g.connect(this, m, pheromone);
		//g.connect(m, out, 100);
		modes.add(m);
		return m;
	}

	public Set<QualityMode> getQualityModes() {
		return modes;
	}
}
