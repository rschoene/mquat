/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat.bo;

import java.util.HashMap;
import java.util.Map;

import org.haec.optimizer.aco.mquat.MQuATComponent;

/**
 * A requirement inside a {@link QualityMode}.
 * @author Sebastian Götz
 */
public class Requirement implements Cloneable {
	
	
	
	private MQuATComponent requiredComponent;
	private String resourceName;
	/** Property clauses: <name of property, <value , comparator>>*/
	private Map<String,Map<Double, QualityMode.Comparator>> propertyClauses;
	
	public Requirement(MQuATComponent c) {
		this.requiredComponent = c;
		propertyClauses = new HashMap<String, Map<Double,QualityMode.Comparator>>();
	}
	
	public Requirement(String name) {
		this.resourceName = name;
		propertyClauses = new HashMap<String, Map<Double,QualityMode.Comparator>>();
	}
	
	public void addRequirementClause(String property, Double value, QualityMode.Comparator cmp) {
		Map<Double, QualityMode.Comparator> clause = propertyClauses.get(property);
		if(clause == null) clause = new HashMap<Double, QualityMode.Comparator>();
		clause.clear();
		clause.put(value, cmp);
		propertyClauses.put(property, clause);
	}
	
	public MQuATComponent getRequiredMQuATComponent() {
		return requiredComponent;
	}
	
	public String getResourceName() {
		return resourceName;
	}
	
	public Map<String,Map<Double, QualityMode.Comparator>> getPropertyRequirements() {
		return propertyClauses;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		Requirement r = new Requirement(requiredComponent);
		for(String prop : propertyClauses.keySet()) {
			for(Double val : propertyClauses.get(prop).keySet()) { 
				r.addRequirementClause(prop, val, propertyClauses.get(prop).get(val));
			}
		}
		return r;
	}
	
	
}
