/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat.bo;

import java.util.HashSet;
import java.util.Set;

import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.mquat.MQuATComponent;

/**
 * A special component modelling a software component.
 * @author Sebastian Götz
 */
public class SoftwareComponent extends MQuATComponent {
	
	private Set<Implementation> impls;

	public SoftwareComponent(AntGraph g, String name, double initPheromone) {
		super(g, name, initPheromone);		
		impls = new HashSet<Implementation>();
	}
	
	public Implementation addImplementation(String name) {
		Implementation impl = new Implementation(g, name, 0, out);
		g.addComponent(impl);
		g.connect(this, impl, 0);
		impls.add(impl);
		return impl;
	}
	
	public Set<Implementation> getImplementations() {
		return impls;
	}
	
	public Implementation getImplementationByName(String name) {
		for(Implementation impl : impls) {
			if(impl.getName().equals(name)) return impl;
		}
		return null;
	}
	
	public boolean hasImplementation(String name) {
		for(Implementation impl : impls) {
			if(impl.getName().equals(name)) return true;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SoftwareComponent) {
			SoftwareComponent ref = (SoftwareComponent)obj;
			return ref.getName().equals(this.getName());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}
}
