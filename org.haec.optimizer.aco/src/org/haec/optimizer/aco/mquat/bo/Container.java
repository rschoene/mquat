/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat.bo;

import java.util.HashSet;
import java.util.Set;

import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.mquat.MQuATComponent;

/**
 * A special component modelling a container.
 * @author Sebastian Götz
 */
public class Container extends MQuATComponent {

	private Set<HardwareComponent> resources;

	public Container(AntGraph g, String name, double initPheromone) {
		super(g, name, initPheromone);
		resources = new HashSet<HardwareComponent>();
	}

	public void addResource(HardwareComponent parent, HardwareComponent comp,
			boolean bottom) {
		resources.add(comp);
		if (parent == null)
			g.connect(this, comp, 100);
		else
			g.connect(parent, comp, 100);
		if (bottom)
			g.connect(comp, getOutPort(), 100);

	}
	
	public void addResource(HardwareComponent comp,
			boolean bottom) {
		resources.add(comp);
		
		g.connect(this, comp, 100);
		
		if (bottom)
			g.connect(comp, getOutPort(), 100);

	}
	
	public Set<HardwareComponent> getResources() {
		return resources;
	}

}
