/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat.bo;

import java.util.HashMap;
import java.util.Map;

import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.graph.ACOComponent;

/**
 * A special component modelling a hardware component.
 * @author Sebastian Götz
 */
public class HardwareComponent extends ACOComponent {

	AntGraph g;
	
	private Map<String, Double> budgets;
	
	public HardwareComponent(AntGraph g, String name, double initPheromone) {
		super(name, initPheromone);
		this.g = g;
		budgets = new HashMap<String, Double>();
	}
	
	
	public void addProvision(String property, Double value) {
		assert(!budgets.keySet().contains(property));
		budgets.put(property, value);
	}
	
	public Double getProvision(String property) {
		return budgets.get(property);
	}
	
	public Double decProvision(String property, Double amount) {
		Double current = budgets.get(property);
		Double newVal = current-amount;
		budgets.put(property, newVal);
		return newVal;
	}
	
	public Double incProvision(String property, Double amount) {
		Double current = budgets.get(property);
		Double newVal = current+amount;
		budgets.put(property, newVal);
		return newVal;
	}

}
