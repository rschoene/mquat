/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.haec.optimizer.aco.Ant;
import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.graph.ACOLink;
import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.HardwareComponent;
import org.haec.optimizer.aco.mquat.bo.Implementation;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.Requirement;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.haec.optimizer.aco.mquat.bo.QualityMode.Comparator;

/**
 * An ant for MQuAT
 * @author Sebastian Götz
 */
public class MQuATAnt extends Ant {

	private Set<ACOComponent> placesIveBeen;

	/**
	 * set of {@link PropertyRequirementClauses} collected while running through
	 * the graph. ensures that only valid solutions are found
	 */
	private Map<SoftwareComponent, Requirement> swRequirements;
	private Map<String, Requirement> hwRequirements;
	
	private Map<SoftwareComponent, Requirement> initRequirements;

	/**
	 * those {@link SoftwareComponent}s, for which still an
	 * {@link Implementation} and {@link QualityMode} needs to be chosen and
	 * mapped
	 */
	private Set<SoftwareComponent> pendingSWComponents;

	private SoftwareComponent currentSWComponent;
	
	private int plannedIterations;

	public MQuATAnt(String name, AntGraph graph, ACOComponent startPosition,
			double maxPheromone, int plannedIterations) {
		super(name, graph, startPosition, maxPheromone);
		placesIveBeen = new HashSet<ACOComponent>();
		swRequirements = new HashMap<SoftwareComponent, Requirement>();
		hwRequirements = new HashMap<String, Requirement>();
		initRequirements = new HashMap<SoftwareComponent, Requirement>();
		pendingSWComponents = new HashSet<SoftwareComponent>();
		this.plannedIterations = plannedIterations; 
	}

	/**
	 * Stops if all required component types have been mapped.
	 */
	@Override
	protected boolean stop() {
		return (pendingSWComponents.size() == 0 && getCurrentPosition()
				.getName().equals("root")) || isFailed();
	}
	
	@Override
	protected void updatePheromoneTrail() {
		if (isFailed()) {
			for (ACOLink l : getTrail()) {
				ACOComponent x = l.getTarget();
				x.setPheromone(x.getPheromone()*0.99);
				l.setPheromone(l.getPheromone()*0.99);
				if(l.getPheromone() < 0) l.setPheromone(0);
			}
			if(verbose)
			System.out.println(name+"[failed]:: "+getTrail());
		} else {
			for (ACOLink l : getTrail()) {
				ACOComponent x = l.getTarget();
				x.setPheromone(x.getPheromone()*1.01);
				l.setPheromone(l.getPheromone()*1.01);
				if(l.getPheromone() < 0) l.setPheromone(0);
			}
			if(verbose)
			System.out.println(name+"[success]:: "+getTrail());
		}
	}

	private double computeRatio(Map<String, Map<Double, Comparator>> provisions,
			Map<String, Map<Double, Comparator>> requirements) {
		double totalRatio = 1;
		for(String pname : requirements.keySet()) {
			Map<Double, Comparator> px = provisions.get(pname);
			Map<Double, Comparator> rx = requirements.get(pname);
			double a = px.keySet().iterator().next();
			double b = rx.keySet().iterator().next();
			if(b > a) {
				double tmp = a;
				a = b;
				b = tmp;
			}
			double ratio = b / a;
			totalRatio *= ratio;
		}
		return totalRatio;
	}

	private SoftwareComponent getSWComponentOfQualityMode(QualityMode qm) {
		return (SoftwareComponent)qm.getIncomingLinks().iterator().next().getSource().getIncomingLinks().iterator().next().getSource();
	}

	/**
	 * Perform the move along the specified link. In addition to standard
	 * behavior, remove current position from pending SW components and add the
	 * new position to the list of visited positions.
	 * 
	 * @param l
	 *            {@link ACOLink} the link to follow
	 */
	@Override
	protected ACOComponent performMove(ACOLink l) {
		// remove the current node from pending ones
		if (this.getCurrentPosition() instanceof SoftwareComponent) {
			pendingSWComponents.remove(this.getCurrentPosition());
			currentSWComponent = (SoftwareComponent) this.getCurrentPosition();
		}

		if (this.getCurrentPosition() instanceof QualityMode) {
			// check for dependencies to other software components and add them
			// to the pending ones
			for (Requirement req : ((QualityMode) this.getCurrentPosition())
					.getSWRequirements()) {
				if(req.getRequiredMQuATComponent() != null &&req.getRequiredMQuATComponent() instanceof SoftwareComponent) {
					pendingSWComponents.add((SoftwareComponent)req.getRequiredMQuATComponent());
					swRequirements.put((SoftwareComponent)req.getRequiredMQuATComponent(), req);
				} 
			}
			for(Requirement req : ((QualityMode) this.getCurrentPosition())
					.getHWRequirements()) {
				addRequirement(req);
			}

			// check for hardware requirements
		}

		ACOComponent ret = super.performMove(l);

		if (ret instanceof SoftwareComponent) {
			placesIveBeen.add(ret);
		}
		return ret;
	}

	/**
	 * Find all valid links for the ant from its current position to follow.
	 * 
	 * @return List of feasible links ({@link ACOLink})
	 */
	@Override
	protected List<ACOLink> getNextFeasibleMoves() {
		List<ACOLink> moves = super.getNextFeasibleMoves();
		List<ACOLink> filteredMoves = new ArrayList<ACOLink>();
		for (ACOLink l : moves) {
			if (l.getTarget() instanceof QualityMode) {
				// check if the mode provides what the ant remembers to need
				boolean ok = checkProvisionsOfMode(swRequirements.get(currentSWComponent),((MQuATComponent)l.getTarget()));
				if(!ok) continue;
			}
			if(l.getTarget() instanceof HardwareComponent) {
				boolean ok = checkBudget(hwRequirements.get(l.getTarget().getName()),((HardwareComponent)l.getTarget()));;
				if(!ok) {
					continue;
				}
			}
			if (pendingSWComponents.contains(l.getTarget())
					|| !(l.getTarget() instanceof SoftwareComponent))
				filteredMoves.add(l);
		}
		if(filteredMoves.size() == 0) {
			handleFailedAnt();
		}
		return filteredMoves;
	}
	
	@Override
	protected ACOLink getNextMove() {
		List<ACOLink> feasibleMoves = getNextFeasibleMoves();
		if (feasibleMoves.size() > 0) {
			double totalPheromone = 0;
			double trailPheromone[] = new double[feasibleMoves.size()];
			int i = 0;
			if(feasibleMoves.get(0).getTarget() instanceof QualityMode) {
				// get weights of the QualityMode of each feasible move - TODO add further objectives
				for (ACOLink f : feasibleMoves) {
					totalPheromone += ((QualityMode)f.getTarget()).getWeight();
					trailPheromone[i++] = totalPheromone;
				}
			} else {
				// get pheromone of each feasible move
				for (ACOLink f : feasibleMoves) {
					totalPheromone += f.getPheromone();
					trailPheromone[i++] = totalPheromone;
				}
			}			
			double randomNumber = Math.random() * totalPheromone;
			if(verbose) {
				System.out.print(getCurrentPosition() + " [");
				for (double p : trailPheromone)
					System.out.print(p + " ");
				System.out.print("]");
				System.out.print(" | " + randomNumber);
			}
				for (i = 0; i < trailPheromone.length; i++) {
					if (randomNumber <= trailPheromone[i])
						break;
				}
			if(verbose) System.out.println(" | " + i);
			// int idx = AntColony.getRandomIntBetween(0, feasibleMoves.size());
			return feasibleMoves.get(i);
		} else {
			return null;
		}
	}

	private void handleFailedAnt() {
		if(verbose) System.err.println(name+" failed");
		this.getCurrentPosition().setPheromone(getCurrentPosition().getPheromone()+1); //else the visit would not be noted
		this.setFailed(true);
	}
	
	public static boolean checkBudget(Requirement req, HardwareComponent c) {
		boolean ok = true;
		if(req == null) return ok;
		for(String property : req.getPropertyRequirements().keySet()) {
			Map<Double, Comparator> r = req.getPropertyRequirements().get(property);
			Double rv = r.keySet().iterator().next();
			Comparator cmp = r.get(rv);
			Double pv = c.getProvision(property);
			switch(cmp) {
			case EQ: if (!(pv == rv)) return false;
			case LT: if (!(pv < rv)) return false;
			case LET: if (!(pv <= rv)) return false;
			case GT: if(!(pv > rv)) return false;
			case GET: if(!(pv >= rv)) return false;
			}
		}
		return ok;
	}
		
	public static boolean checkProvisionsOfMode(Requirement req, MQuATComponent target) {
		if(req == null) return true;
		Map<String, Map<Double, QualityMode.Comparator>> provisions = target.getProvisions();
		boolean allRequirementsFulfilled = true;
		for (String property : req.getPropertyRequirements().keySet()) {
			if (provisions.keySet().contains(property)) { 
				Map<Double, Comparator> rx = req
						.getPropertyRequirements().get(property);
				Map<Double, Comparator> px = provisions.get(property);
				double reqval = rx.keySet().iterator().next();
				double proval = px.keySet().iterator().next();
				switch (rx.get(reqval)) {
				case LT: // req: v < X, can only be true for LT, LET and
							// EQ
					switch (px.get(proval)) {
					case LT:
						allRequirementsFulfilled = (proval < reqval);
						break;
					case LET:
						allRequirementsFulfilled = (proval < reqval);
						break;
					case EQ:
						allRequirementsFulfilled = (proval < reqval);
						break;
					default:
						allRequirementsFulfilled = false;
					}
					break;
				case LET: // req: v <= X, can only be true for LT, LET
							// and EQ
					switch (px.get(proval)) {
					case LT:
						allRequirementsFulfilled = (proval <= reqval);
						break;
					case LET:
						allRequirementsFulfilled = (proval <= reqval);
						break;
					case EQ:
						allRequirementsFulfilled = (proval <= reqval);
						break;
					default:
						allRequirementsFulfilled = false;
					}
					break;
				case EQ: // req: v = X, can only be true for EQ
					switch (px.get(proval)) {
					case EQ:
						allRequirementsFulfilled = (proval == reqval);
						break;
					default:
						allRequirementsFulfilled = false;
					}
					break;
				case GET: // req: v >= X, can only be true for EQ, GET
							// and GT
					switch (px.get(proval)) {
					case GT:
						allRequirementsFulfilled = (proval >= reqval);
						break;
					case GET:
						allRequirementsFulfilled = (proval >= reqval);
						break;
					case EQ:
						allRequirementsFulfilled = (proval >= reqval);
						break;
					default:
						allRequirementsFulfilled = false;
					}
					break;
				case GT: // req: v > X, can only be true for EQ, GET and
							// GT
					switch (px.get(proval)) {
					case GT:
						allRequirementsFulfilled = (proval > reqval);
						break;
					case GET:
						allRequirementsFulfilled = (proval > reqval);
						break;
					case EQ:
						allRequirementsFulfilled = (proval > reqval);
						break;
					default:
						allRequirementsFulfilled = false;
					}
					break;
				}
			}
		}
		return allRequirementsFulfilled;
	}

	/**
	 * Erase the ants memory.
	 */
	@Override
	public void reset() {
		super.reset();
		placesIveBeen = new HashSet<ACOComponent>();
		pendingSWComponents = new HashSet<SoftwareComponent>();
		swRequirements = new HashMap<SoftwareComponent, Requirement>();
		swRequirements = initRequirements;
		for(SoftwareComponent reqC : swRequirements.keySet()) pendingSWComponents.add(reqC);
		hwRequirements = new HashMap<String, Requirement>();
	}

	/**
	 * Add a software component to the list of pending components to be mapped.
	 * 
	 * @param comp
	 *            {@link SoftwareComponent} to be mapped
	 */
	public void addRequiredSoftwareComponent(SoftwareComponent comp) {
		pendingSWComponents.add(comp);
	}
	
	public void addRequirement(Requirement req) {
		if(req.getRequiredMQuATComponent() != null &&req.getRequiredMQuATComponent() instanceof SoftwareComponent)
			swRequirements.put((SoftwareComponent)req.getRequiredMQuATComponent(), req);
		else {
			hwRequirements.put(req.getResourceName(), req);
		}
	}
	
	public void addInitRequirement(Requirement req) {
		if(req.getRequiredMQuATComponent() != null && req.getRequiredMQuATComponent() instanceof SoftwareComponent) {
			pendingSWComponents.add((SoftwareComponent)req.getRequiredMQuATComponent());
			swRequirements.put((SoftwareComponent)req.getRequiredMQuATComponent(), req);
			initRequirements.put((SoftwareComponent)req.getRequiredMQuATComponent(), req);
		}
	}

}
