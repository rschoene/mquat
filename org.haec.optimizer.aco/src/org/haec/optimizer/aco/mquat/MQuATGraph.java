/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.mquat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.requests.Request;
import org.coolsoftware.theatre.util.GeneratorUtil;
import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.graph.ACOConstraint;
import org.haec.optimizer.aco.graph.ACOLink;
import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.HardwareComponent;
import org.haec.optimizer.aco.mquat.bo.Implementation;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.Requirement;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.haec.optimizer.aco.mquat.bo.QualityMode.Comparator;

/**
 * A graph containing {@link MQuATComponent}s for {@link MQuATAnt}s.
 * @author Sebastian Götz
 */
public class MQuATGraph extends AntGraph {

	private double objValue = 0.0;
	
	private Map<String,SoftwareComponent> sw = new HashMap<String,SoftwareComponent>();
	
	public MQuATGraph() {
		
	}

	public MQuATGraph(Set<ACOComponent> components, Set<ACOLink> links,
			Set<ACOConstraint> constraints) {
		super(components, links, constraints);
		
	}
	
	@Override
	public String toString() {
		String ret = "";
		//there's always a root
		ACOComponent root = null;
		Set<SoftwareComponent> sw = new HashSet<SoftwareComponent>();
		Set<Container> hw = new HashSet<Container>();
		
		for(ACOComponent c : getComponents()) {
			if(c.getName().equals("root")) root = c;
			if(c instanceof SoftwareComponent) sw.add((SoftwareComponent) c);
			if(c instanceof Container) hw.add((Container) c);
		}

		ret += root+"\n";
		for(SoftwareComponent swc : sw) {
			ret +="+  "+swc+"\n";
			for(Implementation impl : swc.getImplementations()) {
				ret +="   +  "+impl+"\n";
				for(QualityMode m : impl.getQualityModes()) {
					ret +="      +  "+m+"\n";
				}
			}
		}
		
		ret += "\n\n";
		
		for(Container hwc : hw) {
			ret +="+  "+hwc+"\n";
			for(HardwareComponent hc : hwc.getResources()) {
				ret += "   + "+hc+"\n";
			}
		}
		
		return ret; 
	}	
	
	public Set<QualityMode> getValidOptimalMapping(Request req) {
		Set<QualityMode> ret = new HashSet<QualityMode>();
		objValue = 0.0;
		//there's always a root
		ACOComponent root = null;
		sw = new HashMap<String,SoftwareComponent>();
		for(ACOComponent c : getComponents()) {
			if(c.getName().equals("root")) root = c;
			if(c instanceof SoftwareComponent) sw.put(c.getName(),(SoftwareComponent) c);
		}
		
		String requestedComponent = req.getComponent().getName();
		SoftwareComponent reqSC = sw.get(requestedComponent);
		
		Requirement r = new Requirement(reqSC);
		for(PropertyRequirementClause prc : req.getReqs()) {
			String propName = prc.getRequiredProperty().getDeclaredVariable().getName();
			CcmReal val = evaluateRequirementClause(req, prc);
			r.addRequirementClause(propName, val.getRealValue(), (val.isAscending() ? Comparator.GET : Comparator.LET));
		}
		ret = getBestConfiguration(reqSC, r, new HashMap<Container, Collection<Requirement>>());
		
		return ret;
	}
	
	private Set<QualityMode> getBestConfiguration(SoftwareComponent sc, Requirement r, Map<Container,Collection<Requirement>> hwReqs) {
		Set<QualityMode> ret = new HashSet<QualityMode>();
		Set<QualityMode> implsForReqComponent = new TreeSet<QualityMode>();
		for(ACOLink l : sc.getOutgoingLinks()) {
			Implementation impl = (Implementation)l.getTarget();
			for(QualityMode qq : impl.getQualityModes())
				implsForReqComponent.add(qq);
		}
		
		for(QualityMode bestMode : implsForReqComponent) {
			//check if HW provisions are fulfilled
			Container c = (Container)bestMode.getOutgoingLinks().iterator().next().getTarget();
			Collection<Requirement> mergedReqs = mergeHWReqs(hwReqs.get(c), 
					bestMode.getHWRequirements());
			hwReqs.put(c, mergedReqs);
			
			boolean ok = checkHWProvisions(hwReqs.get(c),c);
			if(ok) {
				//check SW requirements
				Set<QualityMode> x = new HashSet<QualityMode>();
				x.add(bestMode);
				List<Requirement> y = new ArrayList<Requirement>();
				y.add(r);
				ok = checkSWProvisions(x, y);
				if(ok) {
					ret.add(bestMode);
					objValue += bestMode.getWeight();
					//recursive dependency resolution
					for(Requirement nr : bestMode.getSWRequirements()) {
						SoftwareComponent reqSW = sw.get(nr.getRequiredMQuATComponent().getName());
						Set<QualityMode> tail = getBestConfiguration(reqSW, nr, hwReqs);
						ret.addAll(tail);
					}
					break;
				}
			}
		}
		
		return ret;
	}
	
	// hw reqs have to be added up per resource
	private Collection<Requirement> mergeHWReqs(
			Collection<Requirement> existingReqs,
			Collection<Requirement> newReqs) {
		Collection<Requirement> ret = new ArrayList<Requirement>();
	
		
		//case 1: reqs exist which are in newReqs --> sum
		if(existingReqs != null)
		for(Requirement exr : existingReqs) {
			String resName = exr.getResourceName();
			for(Requirement newr : newReqs) {
				if(resName.equals(newr.getResourceName())) {
					for(String propName : exr.getPropertyRequirements().keySet()) {
						double nv = newr.getPropertyRequirements().get(propName).keySet().iterator().next();
						double ev = exr.getPropertyRequirements().get(propName).keySet().iterator().next();
						exr.addRequirementClause(propName, nv+ev, exr.getPropertyRequirements().get(propName).values().iterator().next());
					}
				}
			}
			ret.add(exr);
		}
		
		//case 2: there are completely new reqs
		for(Requirement newr : newReqs) {
			boolean exists = false;
			if(existingReqs != null)
			for(Requirement exr : existingReqs) {
				if(newr.getResourceName().equals(exr.getResourceName())) {
					exists = true;
					break;
				}
			}
			if(!exists) {
				ret.add(newr);
			}
		}
		
		return ret;
	}

	public Set<String> getOptimalMapping() {
		objValue = 0.0;
		Set<String> ret = new HashSet<String>();
		
		//there's always a root
		ACOComponent root = null;
		Set<SoftwareComponent> sw = new HashSet<SoftwareComponent>();
		Set<Container> hw = new HashSet<Container>();
		
		for(ACOComponent c : getComponents()) {
			if(c.getName().equals("root")) root = c;
			if(c instanceof SoftwareComponent) sw.add((SoftwareComponent) c);
			if(c instanceof Container) hw.add((Container) c);
		}

		for(SoftwareComponent swc : sw) {
			if(swc.getPheromone() > 0) {
				QualityMode best = null;
				Implementation bestImpl = null;
				double minImpl = -1.0;
				for(Implementation impl : swc.getImplementations()) {
					if(impl.getPheromone() > 0) {
						if(impl.getPheromone() < minImpl || minImpl == -1.0) {
							minImpl = impl.getPheromone();
							double min = -1.0;
							best = impl.getQualityModes().iterator().next();
							for(QualityMode m : impl.getQualityModes()) {
								if(m.getPheromone() < min || min == -1.0) {
									best = m;
									min = m.getPheromone();
									bestImpl = impl;
								}
							}
						}
					}
				}
				if(best != null) {
					ret.add(bestImpl.getName()+"#"+best.getName());
					objValue += best.getWeight();
				}
			}
		}
		return ret;
	}	
	
	public boolean isOptimalMappingValid() {
//		Set<QualityMode> ret = new HashSet<QualityMode>();
//		
//		//there's always a root
//		ACOComponent root = null;
//		Set<SoftwareComponent> sw = new HashSet<SoftwareComponent>();
//		Set<Container> hw = new HashSet<Container>();
//		
//		for(ACOComponent c : getComponents()) {
//			if(c.getName().equals("root")) root = c;
//			if(c instanceof SoftwareComponent) sw.add((SoftwareComponent) c);
//			if(c instanceof Container) hw.add((Container) c);
//		}
//		
//		List<Requirement> allRequirements = new ArrayList<Requirement>();
//		Map<Container, Collection<Requirement>> hwRequirements = new HashMap<Container, Collection<Requirement>>();
//
//		for(SoftwareComponent swc : sw) {
//			if(swc.getPheromone() > 0) {
//				QualityMode best = null;
//				Implementation bestImpl = null;
//				double minImpl = -1.0;
//				for(Implementation impl : swc.getImplementations()) {
//					if(impl.getPheromone() > 0) {
//						if(impl.getPheromone() < minImpl || minImpl == -1.0) {
//							minImpl = impl.getPheromone();
//							double min = -1.0;
//							best = impl.getQualityModes().iterator().next();
//							for(QualityMode m : impl.getQualityModes()) {
//								if(m.getPheromone() < min || min == -1.0) {
//									best = m;
//									min = m.getPheromone();
//									bestImpl = impl;
//								}
//							}
//						}
//					}
//				}
//				if(best != null) {
//					ret.add(best);
//					objValue += best.getWeight();
//					//collect SW requirements
//					allRequirements.addAll(best.getSWRequirements());
//					//collect HW requirements
//					Container s = (Container)best.getOutgoingLinks().get(0).getTarget();
//					Collection<Requirement> reqs = hwRequirements.get(s);
//					if(reqs == null) reqs = new ArrayList<Requirement>();
//					reqs.addAll(best.getHWRequirements());
//					hwRequirements.put(s,reqs);
//				}
//			}
//		}
//		
//		//check SW provisions
//		boolean swOK = checkSWProvisions(ret,allRequirements);
//		boolean hwOK = checkHWProvisions(hwRequirements); 
//				
//		return swOK && hwOK;
		return true; //true by design
	}
	
	private boolean checkHWProvisions(
			Map<Container, Collection<Requirement>> hwRequirements) {
		for(Container c : hwRequirements.keySet()) {
			for(Requirement r : hwRequirements.get(c)) {
				boolean satisfied = false;
				for(HardwareComponent hc : c.getResources()) {
					if(MQuATAnt.checkBudget(r, hc)) {
						satisfied = true;
						break;
					}
				}
				if(!satisfied) return false;
			}
		}
		return true;
	}
	
	private boolean checkHWProvisions(
			Collection<Requirement> hwRequirements, Container c) {
		
			for(Requirement r : hwRequirements) {
				boolean satisfied = false;
				for(HardwareComponent hc : c.getResources()) {
					if(MQuATAnt.checkBudget(r, hc)) {
						satisfied = true;
						break;
					}
				}
				if(!satisfied) return false;
			}
		
		return true;
	}


	private boolean checkSWProvisions(Set<QualityMode> conf,
			List<Requirement> allRequirements) {
		for(Requirement r : allRequirements) {
			boolean satisfied = false;
			for(QualityMode m : conf) {
				if(MQuATAnt.checkProvisionsOfMode(r, m)) {
					satisfied = true;
					break;
				}
			}
			if(!satisfied) return false;
		}
		return true;
	}

	public double getObjValueOfOptimalMapping() {
		return objValue;
	}

	/**
	 * @param req
	 * @param prc
	 * @return
	 */
	private CcmReal evaluateRequirementClause(Request req,
			PropertyRequirementClause prc) {
		boolean min = true;
		Statement minmaxval = prc.getMinValue();
		if(minmaxval == null) {
			minmaxval = prc.getMaxValue();
			min = false;
		}
		CcmReal val;
		if(minmaxval instanceof FunctionExpression) {
			val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min)	prc.setMinValue(rle);
			else prc.setMaxValue(rle);
		} else if (prc.getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmReal)new CcmExpressionInterpreter().interpret(rle);
		}	else {
			val = (CcmReal)new CcmExpressionInterpreter().interpret(minmaxval);
		}
		if(prc.getRequiredProperty().getValOrder() == Order.INCREASING) {
			if(min) { val.setAscending(true); } else { val.setAscending(false); }
		} else {
			if(min) { val.setAscending(false); } else { val.setAscending(true); }
		}
		return val;
	}

}
