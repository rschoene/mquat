/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.test;

import org.haec.optimizer.aco.AntColony;
import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.mquat.MQuATAnt;
import org.haec.optimizer.aco.mquat.MQuATGraph;
import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.HardwareComponent;
import org.haec.optimizer.aco.mquat.bo.Implementation;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.Requirement;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.haec.optimizer.aco.mquat.bo.QualityMode.Comparator;

/**
 * A test class to test MQuAT ant colony optimization.
 * @author Sebastian Götz
 */
public class MQuATTest {
	public static void main(String[] args) {
		AntColony c = buildColony();
		
		c.scheduleActivities(1000);
		
		System.out.println(c.getGraph());
	}
	
	private static AntColony buildColony() {
		AntGraph graph = new MQuATGraph();
		ACOComponent root = new ACOComponent("root", 0);
		graph.addComponent(root);
		
		SoftwareComponent playerType = new SoftwareComponent(graph, "VideoPlayer", 0);
		graph.addComponent(playerType);
		
		Implementation vlc = playerType.addImplementation("vlc");
		Implementation wmp = playerType.addImplementation("wmp");
		QualityMode vlcHQ = vlc.addQualityMode("highQualityVLC",100);
		QualityMode vlcLQ = vlc.addQualityMode("lowQualityVLC",100);
		QualityMode wmpHQ = wmp.addQualityMode("highQualityWMP",100);
		QualityMode wmpLQ = wmp.addQualityMode("lowQualityWMP",100);
		
		
		SoftwareComponent decoderType = new SoftwareComponent(graph, "Decoder", 0);
		graph.addComponent(decoderType);
		
		vlcHQ.addSWRequirementClause(decoderType, "bitrate", 7.0, Comparator.GET);
		vlcHQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
		vlcHQ.addProvision("framerate", 25.0, Comparator.GET);
		vlcLQ.addSWRequirementClause(decoderType, "bitrate", 2.0, Comparator.GET);
		vlcLQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
		vlcLQ.addProvision("framerate", 15.0, Comparator.GET);
		wmpHQ.addSWRequirementClause(decoderType, "bitrate", 14.0, Comparator.GET); //special case, because only commercial decoder provides this (the free ones does not)
		wmpHQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
		wmpHQ.addProvision("framerate", 22.0, Comparator.GET);
		wmpLQ.addSWRequirementClause(decoderType, "bitrate", 3.0, Comparator.GET);
		wmpLQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
		wmpLQ.addProvision("framerate", 16.0, Comparator.GET);
		
		Implementation free = decoderType.addImplementation("free");
		Implementation com = decoderType.addImplementation("commercial");
		QualityMode freeHQ = free.addQualityMode("fastF",100);
		freeHQ.addProvision("bitrate", 12.0, Comparator.GET);
		QualityMode freeLQ = free.addQualityMode("slowF",100);
		freeLQ.addProvision("bitrate", 5.0, Comparator.GET);
		QualityMode comHQ = com.addQualityMode("fastC",100);
		comHQ.addProvision("bitrate", 20.0, Comparator.GET);
		QualityMode comLQ = com.addQualityMode("slowC",100);
		comLQ.addProvision("bitrate", 7.0, Comparator.GET);
		
//		freeHQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
//		freeLQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
//		comHQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
//		comLQ.addHWRequirementClause("CPU", "frequency", 1500.0, Comparator.GET);
		
		//TODO implement resource specific hardware checks (i.e., subcomponents of server)
		Container server1 = new Container(graph, "Server1", 0);
		HardwareComponent cpu1 = new HardwareComponent(graph, "CPU", 0);
		cpu1.addProvision("frequency", 2000.0);
		server1.addResource(null, cpu1, true);
		Container server2 = new Container(graph, "Server2", 0);
		HardwareComponent cpu2 = new HardwareComponent(graph, "CPU", 0);
		cpu2.addProvision("frequency", 1000.0);
		server2.addResource(null, cpu2, true);
		
		graph.addComponent(server1);
		graph.addComponent(server2);
		graph.connect(server1.getOutPort(), root, 100);
		graph.connect(server2.getOutPort(), root, 100);
		
		graph.connect(playerType.getOutPort(), server1, 100);
		graph.connect(playerType.getOutPort(), server2, 100);
		graph.connect(decoderType.getOutPort(), server1, 100);
		graph.connect(decoderType.getOutPort(), server2, 100);
		
		graph.connect(root, playerType, 100);
		graph.connect(root, decoderType, 100);
		
		Requirement req1 = new Requirement(playerType);
		req1.addRequirementClause("framerate", 20.0, Comparator.GET);
		Requirement req2 = new Requirement(playerType);
		req2.addRequirementClause("framerate", 10.0, Comparator.GET);
		
		AntColony colony = new AntColony(graph);
		
		MQuATAnt ant1 = new MQuATAnt("ant1", graph, root, 10, 1000);
		ant1.addInitRequirement(req1);
		
		MQuATAnt ant2 = new MQuATAnt("ant2", graph, root, 10, 1000);
		ant2.addInitRequirement(req1);
		
		MQuATAnt ant3 = new MQuATAnt("ant3", graph, root, 10, 1000);
		ant3.addInitRequirement(req1);
		
		MQuATAnt ant4 = new MQuATAnt("ant4", graph, root, 10, 1000);
		ant4.addInitRequirement(req1);
		
		MQuATAnt ant5 = new MQuATAnt("ant5", graph, root, 10, 1000);
		ant5.addInitRequirement(req1);
		
		colony.addAnt(ant1);
		colony.addAnt(ant2);
		colony.addAnt(ant3);
		colony.addAnt(ant4);
		colony.addAnt(ant5);
		
		return colony;
	}
}
