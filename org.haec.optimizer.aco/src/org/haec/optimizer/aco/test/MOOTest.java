/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.test;

import org.haec.optimizer.aco.AntColony;
import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.graph.ACOLink;

/**
 * A test class to test ant colony optimization.
 * @author Sebastian Götz
 */
public class MOOTest {

	static ACOComponent a = new ACOComponent("a", 0);
	static ACOComponent b = new ACOComponent("b", 0);
	static ACOComponent c = new ACOComponent("c", 0);
	static ACOComponent d = new ACOComponent("d", 0);
	static ACOComponent e = new ACOComponent("e", 0);
	static ACOComponent x = new ACOComponent("x", 0);
	static ACOComponent y = new ACOComponent("y", 0);
	static ACOComponent z = new ACOComponent("z", 0);

	public static void main(String[] args) {
		AntGraph graph = buildGraph();
		AntColony col = buildColony(graph);

		col.scheduleActivities(1000);

		System.out.println(col.getGraph());
		System.out.println("::");
		col.getGraph().printOptimalPath(a, e);
		col.getGraph().printOptimalPath(z, e);
	}

	private static AntColony buildColony(AntGraph graph) {
		AntColony colony = new AntColony(graph);
		colony.setEvaporationRate(0.01);
		
		ACOComponent start = a;
		ACOComponent start2 = z;
		ACOComponent stop = e;

		colony.addAnt(start, stop, true);
		colony.addAnt(start, stop, true);
		colony.addAnt(start, stop, true);
		colony.addAnt(start, stop, true);
		colony.addAnt(start, stop, true);	
		colony.addAnt(start2, stop, false);
		colony.addAnt(start2, stop, false);
		colony.addAnt(start2, stop, false);
		colony.addAnt(start2, stop, false);
		colony.addAnt(start2, stop, false);


		return colony;
	}

	private static AntGraph buildGraph() {
		AntGraph graph = new AntGraph();

		graph.addComponent(a);
		graph.addComponent(b);
		graph.addComponent(c);
		graph.addComponent(d);
		graph.addComponent(e);
		graph.addComponent(x);
		graph.addComponent(y);
		graph.addComponent(z);

		// initial pheromone of 50, so ants consider them
		ACOLink ab = new ACOLink(a, b, 100);
		ACOLink bc = new ACOLink(b, c, 100);
		ACOLink bd = new ACOLink(b, d, 100);
		ACOLink cx = new ACOLink(c, x, 100);
		ACOLink de = new ACOLink(d, e, 100);
		ACOLink bx = new ACOLink(b, x, 100);
		ACOLink by = new ACOLink(b, y, 100);
		ACOLink xy = new ACOLink(x, y, 100);
		ACOLink yd = new ACOLink(y, d, 100);
		ACOLink zy = new ACOLink(z, y, 100);
		ACOLink zb = new ACOLink(z, b, 100);

		graph.connect(ab);
		graph.connect(bc);
		graph.connect(bd);
		graph.connect(cx);
		graph.connect(de);
		graph.connect(bx);
		graph.connect(by);
		graph.connect(xy);
		graph.connect(yd);
		graph.connect(zy);
		graph.connect(zb);
		return graph;
	}
}
