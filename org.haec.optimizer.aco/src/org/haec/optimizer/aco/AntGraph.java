/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.graph.ACOConstraint;
import org.haec.optimizer.aco.graph.ACOLink;

/**The graph ants are running on. 
 * 
 * @author Sebastian Goetz
 * @date 14.12.2011
 */
public class AntGraph {
	/** Set of all components (i.e., nodes) */
	private Set<ACOComponent> components;
	/** Set of all links (i.e., vertices) */
	private Set<ACOLink> links;
	/** Set of all constraints (on the graph) */
	private Set<ACOConstraint> constraints;
	
	/**Initialize an empty graph */
	public AntGraph() {
		components = new HashSet<ACOComponent>();
		links = new HashSet<ACOLink>();
		constraints = new HashSet<ACOConstraint>();
	}
	
	/**Initialize a graph with specified sets of components, links and constraints. 
	 * 
	 * @param components the nodes/components of the graph
	 * @param links the links/vertices of the graph
	 * @param constraints the constraints of the graph
	 */
	public AntGraph(Set<ACOComponent> components, Set<ACOLink> links,
			Set<ACOConstraint> constraints) {
		super();
		this.components = components;
		this.links = links;
		this.constraints = constraints;
	}
	
	/**Adds a constraint to the graph.
	 * 
	 * @param c the constraint to be added
	 * @return success of adding the constraint
	 */
	public boolean addConstraint(ACOConstraint c) {
		return constraints.add(c);
	}
	
	/**Adds a node/component to the graph.
	 * 
	 * @param c the component to be added.
	 * @return success of adding the component.
	 */
	public boolean addComponent(ACOComponent c) {
		return components.add(c);
	}
	
	/**Connects two components of the node and assigns a specific pheromone level to this thereby created link.
	 * 
	 * @param source source component
	 * @param target target component
	 * @param initPheromone amount of pheromone to be assigned to the new link
	 * @return success of connecting the components
	 */
	public boolean connect(ACOComponent source, ACOComponent target, double initPheromone) {
		assert components.contains(source);
		assert components.contains(target);
		
		ACOLink l = new ACOLink(source, target, initPheromone);
		
		source.addOutgoingLink(l);
		target.addIncomingLink(l);
		
		return links.add(l);
	}
	
	/**Adds a new link to the graph.
	 * 
	 * @param l the link to be added.
	 * @return success of adding the link.
	 */
	public boolean connect(ACOLink l) {
		//check if the link is ok
		assert components.contains(l.getSource());
		assert components.contains(l.getTarget());
		
		l.getSource().addOutgoingLink(l);
		l.getTarget().addIncomingLink(l);
		
		return links.add(l);
	}

	/**Returns the components/nodes of the graph.
	 * 
	 * @return the components/nodes of the graph.
	 */
	public Set<ACOComponent> getComponents() {
		return components;
	}

	/**Sets the components of the graph.
	 * 
	 * @param components 
	 */
	public void setComponents(Set<ACOComponent> components) {
		this.components = components;
	}

	/**Returns all links of the graph.
	 * 
	 * @return the links of the graph.
	 */
	public Set<ACOLink> getLinks() {
		return links;
	}

	/**Sets the links of the graph.
	 * 
	 * @param links
	 */
	public void setLinks(Set<ACOLink> links) {
		this.links = links;
	}

	/**Returns the constraints of the graph.
	 * 
	 * @return the constraints of the graph.
	 */
	public Set<ACOConstraint> getConstraints() {
		return constraints;
	}

	/**Sets the constraints of the graph.
	 * 
	 * @param constraints
	 */
	public void setConstraints(Set<ACOConstraint> constraints) {
		this.constraints = constraints;
	}
    
    
	/**
	 * states are sequences/subsets of all components (= solutions)
	 * feasible states do not violate the constraints
	 * @return set of all feasible solutions
	 */
	public Set<Queue<ACOComponent>> getFeasibleStates() {
		
		return new HashSet<Queue<ACOComponent>>();
	}
	
	/**states are sequences/subsets of all components (= solutions)
	 * 
	 * @return all solutions (including infeasible ones)
	 */
    public Set<Queue<ACOComponent>> getAllStates() {
		
		return new HashSet<Queue<ACOComponent>>();
	}
    
    /**Get a random component of the graph.
     * 
     * @return a random component of the graph.
     */
    public ACOComponent getRandomPosition() {
    	int idx = AntColony.getRandomIntBetween(0, components.size()-1);
    	return components.toArray(new ACOComponent[0])[idx];
    }
    
    /**Print the shortest path from start to stop on the console.
     * 
     * @param start start node
     * @param stop stop node
     */
    public void printOptimalPath(ACOComponent start, ACOComponent stop) {
    	System.out.println("optimal path:\n\t"+start);
    	ACOComponent current = start;
    	while(current != stop) {
    		double maxPheromone = 0;
    		int idx = 0, i = 0;
    		for(ACOLink l : current.getOutgoingLinks()) {
    			if(l.getPheromone() > maxPheromone) {
    				maxPheromone = l.getPheromone();
    				idx = i;
    			}
    			i++;
    		}
    		current = current.getOutgoingLinks().get(idx).getTarget();
    		System.out.println("\t"+current);
    	}
    }
    
    @Override
    public String toString() {
    	StringBuffer ret = new StringBuffer();
    	for(ACOComponent c : components)
    		ret.append("# "+c+" - ");
    	ret.append("\n");
    	for(ACOLink l : links) 
    		ret.append(l+"\n");
    	ret.append("\n");
    	return ret.toString();
    }
}
