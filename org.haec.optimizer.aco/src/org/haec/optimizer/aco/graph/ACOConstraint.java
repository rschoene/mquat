/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.graph;

/**Constraint to be defined on graphs.
 * 
 * @author Sebastian Goetz
 * @date 14.12.2011
 */
public class ACOConstraint {
	/**Standard constraint returning true in any case. Override for more sophisticated constraints.
	 * This method is called by ants during determining their feasible moves.
	 * 
	 * @param l the link to evaluate
	 * @return whether the link holds the constraint or not
	 */
	public boolean evaluate(ACOLink l) {
		return true;
	}
}
