/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.graph;

/**A link/vertice in the {@link AntGraph}.
 * 
 * @author Sebastian Goetz
 * @date 14.12.2011
 */
public class ACOLink {
	/** source component of the link */
	private ACOComponent source;
	/** target component of the link */
	private ACOComponent target;
	/** amount of pheromone currently deposited on this link */
	private double pheromone;
	
	/**Initialize a new link between a source and target node with an initial amount of pheromone.
	 * 
	 * @param source the source component
	 * @param target the target component 
	 * @param initPheromone the initial amount of pheromone
	 */
	public ACOLink(ACOComponent source, ACOComponent target, double initPheromone) {
		this.source = source;
		this.target = target;
		this.pheromone = initPheromone;
	}

	public ACOComponent getSource() {
		return source;
	}

	public void setSource(ACOComponent source) {
		this.source = source;
	}

	public ACOComponent getTarget() {
		return target;
	}

	public void setTarget(ACOComponent target) {
		this.target = target;
	}

	public double getPheromone() {
		return pheromone;
	}

	public void setPheromone(double pheromone) {
		this.pheromone = pheromone;
	}
	
	/**increment the amount of pheromone
	 * 
	 * @param amount the increment
	 */
	public void incPheromone(double amount) {
		this.pheromone += amount;
	}
	
	@Override
	public String toString() {
		return source+"->"+target+" ["+pheromone+"]";
	}
}
