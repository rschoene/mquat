/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco.graph;

import java.util.ArrayList;
import java.util.List;

/**A component in the {@link AntGraph}.
 * 
 * @author Sebastian Goetz
 * @date 14.12.2011
 */
public class ACOComponent {
	//TODO heuristic information for the components
	// to be used for provision/requirements..
	
	/** name of the component */
	private String name;
	/** list of outgoing links */
	private List<ACOLink> outgoingLinks;
	/** list of incoming links */
	private List<ACOLink> incomingLinks;
	/** amount of pheromone deposited on this component */
	private double pheromone;
	
	/**Initialize a new component with a specified name and amount of pheromone.
	 * 
	 * @param name the name of the component
	 * @param initPheromone the amount of pheromone to be deposited initially
	 */
	public ACOComponent(String name, double initPheromone) {
		super();
		this.name = name;
		outgoingLinks = new ArrayList<ACOLink>();
		incomingLinks = new ArrayList<ACOLink>();
		this.pheromone = initPheromone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean addOutgoingLink(ACOLink x) {
		return outgoingLinks.add(x);
	}
	
	public boolean addIncomingLink(ACOLink x) {
		return incomingLinks.add(x);
	}

	public List<ACOLink> getOutgoingLinks() {
		return outgoingLinks;
	}

	public void setOutgoingLinks(List<ACOLink> outgoingLinks) {
		this.outgoingLinks = outgoingLinks;
	}

	public List<ACOLink> getIncomingLinks() {
		return incomingLinks;
	}

	public void setIncomingLinks(List<ACOLink> incomingLinks) {
		this.incomingLinks = incomingLinks;
	}

	public double getPheromone() {
		return pheromone;
	}

	public void setPheromone(double pheromone) {
		this.pheromone = pheromone;
	}
	
	/**Increment the amount of pheromone. 
	 * 
	 * @param amount the increment
	 */
	public void incPheromone(double amount) {
		this.pheromone += amount;
	}
	
	@Override
	public String toString() {
		return name+"("+pheromone+")";
	}
}
