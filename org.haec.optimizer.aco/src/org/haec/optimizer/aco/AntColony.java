/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.aco;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.haec.optimizer.aco.graph.ACOComponent;
import org.haec.optimizer.aco.graph.ACOLink;

/**Ant Colony Optimization Meta-Heuristic.
 * 
 * @author Sebastian Goetz
 * @date 14.12.2011
 */
public class AntColony {
	/** Set of all ants	 */
	private Set<Ant> ants;
	/** The graph to be used */
	private AntGraph graph;
	/** The number of ants (i.e., ants.size()) */
	private int numAnts = 0;
	/** The percentage of pheromone to evaporate after each iteration **/
	private double evaporationRate = 0.01;
	
	/** Verbosity (i.e., log a lot or not) */
	private boolean verbose = false;
	
	/** Instance of {@link Random} used by {@link #getRandomIntBetween(int, int)} */ 
	private static Random random = new Random();

	/**Initialize a colony for a specified graph with no ants.
	 * 
	 * @param graph the graph to be used
	 */
	public AntColony(AntGraph graph) {
		super();
		this.ants = new HashSet<Ant>();
		this.graph = graph;
	}

	/**Main loop of the ant colony optimization meta-heuristic.
	 * 
	 * @param nIterations number of iterations (i.e., complete runs of all ants through the graph)
	 */
	public void scheduleActivities(int nIterations) {
		long start = System.nanoTime();
		for (int i = 0; i < nIterations; i++) {
			manageAntsActivity();
			evaporatePheromone();
			daemonActions();
			//if(verbose) System.out.println("\n\n");
		}
		long diff = System.nanoTime() - start;
//		System.out.println("took "+(diff/1000000)+" ms");
	}

	/**A single run of all ants through the graph.
	 * 
	 */
	protected void manageAntsActivity() {

		for (Ant a : ants) {
			boolean running = true;
			while (running) {
				running = a.move();
			}
		}

	}

	/**Update the pheromone levels of the graph. Usually decreases the amount of pheromone. This standard implementation does not change any pheromone level. 
	 * 
	 */
	protected void evaporatePheromone() {
		for(ACOLink l : this.getGraph().getLinks()) {
			//evaporate 1% of pheromone per iteration
			double evaporated = l.getPheromone()*(1-evaporationRate);
			if(evaporated < 1) evaporated = 1.0;
			l.setPheromone(evaporated);
		}
	}

	/**Addtional actions to be performed after each single run of all ants through the graph. This standard implementation does nothing. 
	 * 
	 */
	private void daemonActions() {
		for(Ant a : ants) {
			a.updatePheromoneTrail();
			a.reset();
		}
	}
	
	/**Update the pheromones placed by a specific ant.
	 * 
	 * @param a the ant, which placed the pheromones
	 */
	protected void updatePheromones(Ant a) {
		
	}

	/**Add an ant with a random start position in the graph and a maximum pheromone of 100.
	 * 
	 * @return boolean success of addition
	 */
	public boolean addAnt() {
		numAnts ++;
		ACOComponent startPosition = graph.getRandomPosition();
		double maxPheromone = 100;
		Ant a = new Ant("ant"+numAnts,graph, startPosition, maxPheromone);
		a.setVerbose(verbose);
		return ants.add(a);
	}

	/**Add an ant at a specified start position and a maximum pheromone of 100.
	 * 
	 * @param start the start position in the graph to be used by the ant
	 * @return boolean success of addition
	 */
	public boolean addAnt(ACOComponent start) {
		numAnts++;
		ACOComponent startPosition = start;
		double maxPheromone = 100;
		Ant a = new Ant("ant"+numAnts,graph, startPosition, maxPheromone);
		a.setVerbose(verbose);
		return ants.add(a);
	}
	
	/**Add an ant at a specified start position with a specified stop position and a maximum pheromone of 100.
	 * 
	 * @param start start position in graph
	 * @param stop  stop position graph
	 * @return success of addition
	 */
	public boolean addAnt(ACOComponent start, ACOComponent stop, boolean longestPath) {
		numAnts++;
		ACOComponent startPosition = start;
		double maxPheromone = 100;
		Ant a = new Ant("ant"+numAnts,graph, startPosition, maxPheromone, stop);
		a.setVerbose(verbose);
		a.setLongestPath(longestPath);
		return ants.add(a);
	}
	
	public boolean addAnt(ACOComponent start, ACOComponent stop) {
		return addAnt(start,stop,false);
	}
	
	public boolean addAnt(Ant ant) {
		return ants.add(ant);
	}

	/**Helper method providing a random number between i and j (inclusive)
	 * 
	 * @param i minimum value of random number
	 * @param j maximum value of random number
	 * @return random number between i and j
	 */
	public static int getRandomIntBetween(int i, int j) {
		if(j == 0) return 0;
		assert (j > i);
		int ret = random.nextInt(j-i)+i;
		return ret;
	}

	/**Returns the graph being used by this colony.
	 * 
	 * @return the graph
	 */
	public AntGraph getGraph() {
		return graph;
	}

	/**Get the evaporation rate
	 * 
	 * @return the percentage of pheromone to evaporate at each iteration.
	 */
	public double getEvaporationRate() {
		return evaporationRate;
	}

	/**Set the percentage of pheromone to evaporate at each iteration.
	 * 
	 * @param evaporationRate the evaporation rate  
	 */
	public void setEvaporationRate(double evaporationRate) {
		this.evaporationRate = evaporationRate;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
		for(Ant a : ants) {
			a.setVerbose(verbose);
		}
	}
	
	
}
