/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.impl;

import org.coolsoftware.reconfiguration.LocalReplacement;
import org.coolsoftware.reconfiguration.ReconfigurationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Local Replacement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.impl.LocalReplacementImpl#getTargetComponent <em>Target Component</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LocalReplacementImpl extends ReconfigurationStepImpl implements LocalReplacement {
	/**
   * The default value of the '{@link #getTargetComponent() <em>Target Component</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getTargetComponent()
   * @generated
   * @ordered
   */
	protected static final String TARGET_COMPONENT_EDEFAULT = null;

	/**
   * The cached value of the '{@link #getTargetComponent() <em>Target Component</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getTargetComponent()
   * @generated
   * @ordered
   */
	protected String targetComponent = TARGET_COMPONENT_EDEFAULT;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected LocalReplacementImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	protected EClass eStaticClass() {
    return ReconfigurationPackage.Literals.LOCAL_REPLACEMENT;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public String getTargetComponent() {
    return targetComponent;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setTargetComponent(String newTargetComponent) {
    String oldTargetComponent = targetComponent;
    targetComponent = newTargetComponent;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT, oldTargetComponent, targetComponent));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID)
    {
      case ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT:
        return getTargetComponent();
    }
    return super.eGet(featureID, resolve, coreType);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eSet(int featureID, Object newValue) {
    switch (featureID)
    {
      case ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT:
        setTargetComponent((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eUnset(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT:
        setTargetComponent(TARGET_COMPONENT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public boolean eIsSet(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.LOCAL_REPLACEMENT__TARGET_COMPONENT:
        return TARGET_COMPONENT_EDEFAULT == null ? targetComponent != null : !TARGET_COMPONENT_EDEFAULT.equals(targetComponent);
    }
    return super.eIsSet(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (targetComponent: ");
    result.append(targetComponent);
    result.append(')');
    return result.toString();
  }

} //LocalReplacementImpl
