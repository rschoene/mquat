/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.impl;

import java.util.Collection;

import org.coolsoftware.reconfiguration.ReconfigurationActivity;
import org.coolsoftware.reconfiguration.ReconfigurationPackage;
import org.coolsoftware.reconfiguration.ReconfigurationStep;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.impl.ReconfigurationActivityImpl#getSteps <em>Steps</em>}</li>
 *   <li>{@link org.coolsoftware.reconfiguration.impl.ReconfigurationActivityImpl#getComponentType <em>Component Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReconfigurationActivityImpl extends EObjectImpl implements ReconfigurationActivity {
	/**
   * The cached value of the '{@link #getSteps() <em>Steps</em>}' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getSteps()
   * @generated
   * @ordered
   */
	protected EList<ReconfigurationStep> steps;

	/**
   * The default value of the '{@link #getComponentType() <em>Component Type</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getComponentType()
   * @generated
   * @ordered
   */
	protected static final String COMPONENT_TYPE_EDEFAULT = null;

	/**
   * The cached value of the '{@link #getComponentType() <em>Component Type</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getComponentType()
   * @generated
   * @ordered
   */
	protected String componentType = COMPONENT_TYPE_EDEFAULT;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected ReconfigurationActivityImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	protected EClass eStaticClass() {
    return ReconfigurationPackage.Literals.RECONFIGURATION_ACTIVITY;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EList<ReconfigurationStep> getSteps() {
    if (steps == null)
    {
      steps = new EObjectContainmentEList<ReconfigurationStep>(ReconfigurationStep.class, this, ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS);
    }
    return steps;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public String getComponentType() {
    return componentType;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setComponentType(String newComponentType) {
    String oldComponentType = componentType;
    componentType = newComponentType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE, oldComponentType, componentType));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS:
        return ((InternalEList<?>)getSteps()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS:
        return getSteps();
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE:
        return getComponentType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS:
        getSteps().clear();
        getSteps().addAll((Collection<? extends ReconfigurationStep>)newValue);
        return;
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE:
        setComponentType((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eUnset(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS:
        getSteps().clear();
        return;
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE:
        setComponentType(COMPONENT_TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public boolean eIsSet(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__STEPS:
        return steps != null && !steps.isEmpty();
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY__COMPONENT_TYPE:
        return COMPONENT_TYPE_EDEFAULT == null ? componentType != null : !COMPONENT_TYPE_EDEFAULT.equals(componentType);
    }
    return super.eIsSet(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (componentType: ");
    result.append(componentType);
    result.append(')');
    return result.toString();
  }

} //ReconfigurationActivityImpl
