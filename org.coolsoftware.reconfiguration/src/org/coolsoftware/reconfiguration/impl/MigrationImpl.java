/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.impl;

import org.coolsoftware.reconfiguration.Migration;
import org.coolsoftware.reconfiguration.ReconfigurationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Migration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.impl.MigrationImpl#getTargetContainer <em>Target Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MigrationImpl extends ReconfigurationStepImpl implements Migration {
	/**
   * The default value of the '{@link #getTargetContainer() <em>Target Container</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getTargetContainer()
   * @generated
   * @ordered
   */
	protected static final String TARGET_CONTAINER_EDEFAULT = null;

	/**
   * The cached value of the '{@link #getTargetContainer() <em>Target Container</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getTargetContainer()
   * @generated
   * @ordered
   */
	protected String targetContainer = TARGET_CONTAINER_EDEFAULT;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected MigrationImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	protected EClass eStaticClass() {
    return ReconfigurationPackage.Literals.MIGRATION;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public String getTargetContainer() {
    return targetContainer;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setTargetContainer(String newTargetContainer) {
    String oldTargetContainer = targetContainer;
    targetContainer = newTargetContainer;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ReconfigurationPackage.MIGRATION__TARGET_CONTAINER, oldTargetContainer, targetContainer));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID)
    {
      case ReconfigurationPackage.MIGRATION__TARGET_CONTAINER:
        return getTargetContainer();
    }
    return super.eGet(featureID, resolve, coreType);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eSet(int featureID, Object newValue) {
    switch (featureID)
    {
      case ReconfigurationPackage.MIGRATION__TARGET_CONTAINER:
        setTargetContainer((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eUnset(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.MIGRATION__TARGET_CONTAINER:
        setTargetContainer(TARGET_CONTAINER_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public boolean eIsSet(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.MIGRATION__TARGET_CONTAINER:
        return TARGET_CONTAINER_EDEFAULT == null ? targetContainer != null : !TARGET_CONTAINER_EDEFAULT.equals(targetContainer);
    }
    return super.eIsSet(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (targetContainer: ");
    result.append(targetContainer);
    result.append(')');
    return result.toString();
  }

} //MigrationImpl
