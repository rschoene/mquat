/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.impl;

import org.coolsoftware.reconfiguration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ReconfigurationFactoryImpl extends EFactoryImpl implements ReconfigurationFactory {
	/**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public static ReconfigurationFactory init() {
    try
    {
      ReconfigurationFactory theReconfigurationFactory = (ReconfigurationFactory)EPackage.Registry.INSTANCE.getEFactory("http://cool-software.org/reconfiguration"); 
      if (theReconfigurationFactory != null)
      {
        return theReconfigurationFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new ReconfigurationFactoryImpl();
  }

	/**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public ReconfigurationFactoryImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public EObject create(EClass eClass) {
    switch (eClass.getClassifierID())
    {
      case ReconfigurationPackage.RECONFIGURATION_PLAN: return createReconfigurationPlan();
      case ReconfigurationPackage.LOCAL_REPLACEMENT: return createLocalReplacement();
      case ReconfigurationPackage.MIGRATION: return createMigration();
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY: return createReconfigurationActivity();
      case ReconfigurationPackage.DISTRIBUTED_REPLACEMENT: return createDistributedReplacement();
      case ReconfigurationPackage.DEPLOYMENT: return createDeployment();
      case ReconfigurationPackage.UN_DEPLOYMENT: return createUnDeployment();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public ReconfigurationPlan createReconfigurationPlan() {
    ReconfigurationPlanImpl reconfigurationPlan = new ReconfigurationPlanImpl();
    return reconfigurationPlan;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public LocalReplacement createLocalReplacement() {
    LocalReplacementImpl localReplacement = new LocalReplacementImpl();
    return localReplacement;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public Migration createMigration() {
    MigrationImpl migration = new MigrationImpl();
    return migration;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public ReconfigurationActivity createReconfigurationActivity() {
    ReconfigurationActivityImpl reconfigurationActivity = new ReconfigurationActivityImpl();
    return reconfigurationActivity;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public DistributedReplacement createDistributedReplacement() {
    DistributedReplacementImpl distributedReplacement = new DistributedReplacementImpl();
    return distributedReplacement;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public Deployment createDeployment() {
    DeploymentImpl deployment = new DeploymentImpl();
    return deployment;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public UnDeployment createUnDeployment() {
    UnDeploymentImpl unDeployment = new UnDeploymentImpl();
    return unDeployment;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public ReconfigurationPackage getReconfigurationPackage() {
    return (ReconfigurationPackage)getEPackage();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
	@Deprecated
	public static ReconfigurationPackage getPackage() {
    return ReconfigurationPackage.eINSTANCE;
  }

} //ReconfigurationFactoryImpl
