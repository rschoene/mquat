/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.impl;

import java.util.Collection;

import org.coolsoftware.reconfiguration.Deployment;
import org.coolsoftware.reconfiguration.DistributedReplacement;
import org.coolsoftware.reconfiguration.LocalReplacement;
import org.coolsoftware.reconfiguration.Migration;
import org.coolsoftware.reconfiguration.ReconfigurationPackage;
import org.coolsoftware.reconfiguration.ReconfigurationStep;
import org.coolsoftware.reconfiguration.UnDeployment;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.impl.ReconfigurationStepImpl#getSubStep <em>Sub Step</em>}</li>
 *   <li>{@link org.coolsoftware.reconfiguration.impl.ReconfigurationStepImpl#getSourceComponent <em>Source Component</em>}</li>
 *   <li>{@link org.coolsoftware.reconfiguration.impl.ReconfigurationStepImpl#getSourceContainer <em>Source Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ReconfigurationStepImpl extends EObjectImpl implements ReconfigurationStep {
	/**
   * The cached value of the '{@link #getSubStep() <em>Sub Step</em>}' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getSubStep()
   * @generated
   * @ordered
   */
	protected EList<ReconfigurationStep> subStep;

	/**
   * The default value of the '{@link #getSourceComponent() <em>Source Component</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getSourceComponent()
   * @generated
   * @ordered
   */
	protected static final String SOURCE_COMPONENT_EDEFAULT = null;

	/**
   * The cached value of the '{@link #getSourceComponent() <em>Source Component</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getSourceComponent()
   * @generated
   * @ordered
   */
	protected String sourceComponent = SOURCE_COMPONENT_EDEFAULT;

	/**
   * The default value of the '{@link #getSourceContainer() <em>Source Container</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getSourceContainer()
   * @generated
   * @ordered
   */
	protected static final String SOURCE_CONTAINER_EDEFAULT = null;

	/**
   * The cached value of the '{@link #getSourceContainer() <em>Source Container</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getSourceContainer()
   * @generated
   * @ordered
   */
	protected String sourceContainer = SOURCE_CONTAINER_EDEFAULT;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected ReconfigurationStepImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	protected EClass eStaticClass() {
    return ReconfigurationPackage.Literals.RECONFIGURATION_STEP;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EList<ReconfigurationStep> getSubStep() {
    if (subStep == null)
    {
      subStep = new EObjectContainmentEList<ReconfigurationStep>(ReconfigurationStep.class, this, ReconfigurationPackage.RECONFIGURATION_STEP__SUB_STEP);
    }
    return subStep;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public String getSourceComponent() {
    return sourceComponent;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setSourceComponent(String newSourceComponent) {
    String oldSourceComponent = sourceComponent;
    sourceComponent = newSourceComponent;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_COMPONENT, oldSourceComponent, sourceComponent));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public String getSourceContainer() {
    return sourceContainer;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setSourceContainer(String newSourceContainer) {
    String oldSourceContainer = sourceContainer;
    sourceContainer = newSourceContainer;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_CONTAINER, oldSourceContainer, sourceContainer));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_STEP__SUB_STEP:
        return ((InternalEList<?>)getSubStep()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_STEP__SUB_STEP:
        return getSubStep();
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_COMPONENT:
        return getSourceComponent();
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_CONTAINER:
        return getSourceContainer();
    }
    return super.eGet(featureID, resolve, coreType);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_STEP__SUB_STEP:
        getSubStep().clear();
        getSubStep().addAll((Collection<? extends ReconfigurationStep>)newValue);
        return;
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_COMPONENT:
        setSourceComponent((String)newValue);
        return;
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_CONTAINER:
        setSourceContainer((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eUnset(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_STEP__SUB_STEP:
        getSubStep().clear();
        return;
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_COMPONENT:
        setSourceComponent(SOURCE_COMPONENT_EDEFAULT);
        return;
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_CONTAINER:
        setSourceContainer(SOURCE_CONTAINER_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public boolean eIsSet(int featureID) {
    switch (featureID)
    {
      case ReconfigurationPackage.RECONFIGURATION_STEP__SUB_STEP:
        return subStep != null && !subStep.isEmpty();
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_COMPONENT:
        return SOURCE_COMPONENT_EDEFAULT == null ? sourceComponent != null : !SOURCE_COMPONENT_EDEFAULT.equals(sourceComponent);
      case ReconfigurationPackage.RECONFIGURATION_STEP__SOURCE_CONTAINER:
        return SOURCE_CONTAINER_EDEFAULT == null ? sourceContainer != null : !SOURCE_CONTAINER_EDEFAULT.equals(sourceContainer);
    }
    return super.eIsSet(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (sourceComponent: ");
    result.append(sourceComponent);
    result.append(", sourceContainer: ");
    result.append(sourceContainer);
    result.append(')');
    return result.toString();
  }

} //ReconfigurationStepImpl
