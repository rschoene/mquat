/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.impl;

import org.coolsoftware.reconfiguration.Deployment;
import org.coolsoftware.reconfiguration.DistributedReplacement;
import org.coolsoftware.reconfiguration.LocalReplacement;
import org.coolsoftware.reconfiguration.Migration;
import org.coolsoftware.reconfiguration.ReconfigurationActivity;
import org.coolsoftware.reconfiguration.ReconfigurationFactory;
import org.coolsoftware.reconfiguration.ReconfigurationPackage;
import org.coolsoftware.reconfiguration.ReconfigurationPlan;
import org.coolsoftware.reconfiguration.ReconfigurationStep;
import org.coolsoftware.reconfiguration.UnDeployment;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ReconfigurationPackageImpl extends EPackageImpl implements ReconfigurationPackage {
	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass reconfigurationPlanEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass reconfigurationStepEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass localReplacementEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass migrationEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass reconfigurationActivityEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass distributedReplacementEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass deploymentEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass unDeploymentEClass = null;

	/**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#eNS_URI
   * @see #init()
   * @generated
   */
	private ReconfigurationPackageImpl() {
    super(eNS_URI, ReconfigurationFactory.eINSTANCE);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private static boolean isInited = false;

	/**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link ReconfigurationPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
	public static ReconfigurationPackage init() {
    if (isInited) return (ReconfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(ReconfigurationPackage.eNS_URI);

    // Obtain or create and register package
    ReconfigurationPackageImpl theReconfigurationPackage = (ReconfigurationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ReconfigurationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ReconfigurationPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theReconfigurationPackage.createPackageContents();

    // Initialize created meta-data
    theReconfigurationPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theReconfigurationPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(ReconfigurationPackage.eNS_URI, theReconfigurationPackage);
    return theReconfigurationPackage;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getReconfigurationPlan() {
    return reconfigurationPlanEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EReference getReconfigurationPlan_Activities() {
    return (EReference)reconfigurationPlanEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getReconfigurationStep() {
    return reconfigurationStepEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EReference getReconfigurationStep_SubStep() {
    return (EReference)reconfigurationStepEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EAttribute getReconfigurationStep_SourceComponent() {
    return (EAttribute)reconfigurationStepEClass.getEStructuralFeatures().get(1);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EAttribute getReconfigurationStep_SourceContainer() {
    return (EAttribute)reconfigurationStepEClass.getEStructuralFeatures().get(2);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getLocalReplacement() {
    return localReplacementEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EAttribute getLocalReplacement_TargetComponent() {
    return (EAttribute)localReplacementEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getMigration() {
    return migrationEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EAttribute getMigration_TargetContainer() {
    return (EAttribute)migrationEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getReconfigurationActivity() {
    return reconfigurationActivityEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EReference getReconfigurationActivity_Steps() {
    return (EReference)reconfigurationActivityEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EAttribute getReconfigurationActivity_ComponentType() {
    return (EAttribute)reconfigurationActivityEClass.getEStructuralFeatures().get(1);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getDistributedReplacement() {
    return distributedReplacementEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EAttribute getDistributedReplacement_TargetContainer() {
    return (EAttribute)distributedReplacementEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getDeployment() {
    return deploymentEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EAttribute getDeployment_TargetContainer() {
    return (EAttribute)deploymentEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getUnDeployment() {
    return unDeploymentEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public ReconfigurationFactory getReconfigurationFactory() {
    return (ReconfigurationFactory)getEFactoryInstance();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private boolean isCreated = false;

	/**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void createPackageContents() {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    reconfigurationPlanEClass = createEClass(RECONFIGURATION_PLAN);
    createEReference(reconfigurationPlanEClass, RECONFIGURATION_PLAN__ACTIVITIES);

    reconfigurationStepEClass = createEClass(RECONFIGURATION_STEP);
    createEReference(reconfigurationStepEClass, RECONFIGURATION_STEP__SUB_STEP);
    createEAttribute(reconfigurationStepEClass, RECONFIGURATION_STEP__SOURCE_COMPONENT);
    createEAttribute(reconfigurationStepEClass, RECONFIGURATION_STEP__SOURCE_CONTAINER);

    localReplacementEClass = createEClass(LOCAL_REPLACEMENT);
    createEAttribute(localReplacementEClass, LOCAL_REPLACEMENT__TARGET_COMPONENT);

    migrationEClass = createEClass(MIGRATION);
    createEAttribute(migrationEClass, MIGRATION__TARGET_CONTAINER);

    reconfigurationActivityEClass = createEClass(RECONFIGURATION_ACTIVITY);
    createEReference(reconfigurationActivityEClass, RECONFIGURATION_ACTIVITY__STEPS);
    createEAttribute(reconfigurationActivityEClass, RECONFIGURATION_ACTIVITY__COMPONENT_TYPE);

    distributedReplacementEClass = createEClass(DISTRIBUTED_REPLACEMENT);
    createEAttribute(distributedReplacementEClass, DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER);

    deploymentEClass = createEClass(DEPLOYMENT);
    createEAttribute(deploymentEClass, DEPLOYMENT__TARGET_CONTAINER);

    unDeploymentEClass = createEClass(UN_DEPLOYMENT);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private boolean isInitialized = false;

	/**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void initializePackageContents() {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    localReplacementEClass.getESuperTypes().add(this.getReconfigurationStep());
    migrationEClass.getESuperTypes().add(this.getReconfigurationStep());
    distributedReplacementEClass.getESuperTypes().add(this.getLocalReplacement());
    deploymentEClass.getESuperTypes().add(this.getReconfigurationStep());
    unDeploymentEClass.getESuperTypes().add(this.getReconfigurationStep());

    // Initialize classes and features; add operations and parameters
    initEClass(reconfigurationPlanEClass, ReconfigurationPlan.class, "ReconfigurationPlan", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getReconfigurationPlan_Activities(), this.getReconfigurationActivity(), null, "activities", null, 0, -1, ReconfigurationPlan.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(reconfigurationStepEClass, ReconfigurationStep.class, "ReconfigurationStep", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getReconfigurationStep_SubStep(), this.getReconfigurationStep(), null, "subStep", null, 0, -1, ReconfigurationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getReconfigurationStep_SourceComponent(), ecorePackage.getEString(), "sourceComponent", null, 1, 1, ReconfigurationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getReconfigurationStep_SourceContainer(), ecorePackage.getEString(), "sourceContainer", null, 0, 1, ReconfigurationStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(localReplacementEClass, LocalReplacement.class, "LocalReplacement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLocalReplacement_TargetComponent(), ecorePackage.getEString(), "targetComponent", null, 1, 1, LocalReplacement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(migrationEClass, Migration.class, "Migration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMigration_TargetContainer(), ecorePackage.getEString(), "targetContainer", null, 1, 1, Migration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(reconfigurationActivityEClass, ReconfigurationActivity.class, "ReconfigurationActivity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getReconfigurationActivity_Steps(), this.getReconfigurationStep(), null, "steps", null, 1, -1, ReconfigurationActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getReconfigurationActivity_ComponentType(), ecorePackage.getEString(), "componentType", null, 1, 1, ReconfigurationActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(distributedReplacementEClass, DistributedReplacement.class, "DistributedReplacement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDistributedReplacement_TargetContainer(), ecorePackage.getEString(), "targetContainer", null, 1, 1, DistributedReplacement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(deploymentEClass, Deployment.class, "Deployment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDeployment_TargetContainer(), ecorePackage.getEString(), "targetContainer", null, 1, 1, Deployment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(unDeploymentEClass, UnDeployment.class, "UnDeployment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    // Create resource
    createResource(eNS_URI);
  }

} //ReconfigurationPackageImpl
