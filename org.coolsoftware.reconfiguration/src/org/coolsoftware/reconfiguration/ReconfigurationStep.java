/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSubStep <em>Sub Step</em>}</li>
 *   <li>{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceComponent <em>Source Component</em>}</li>
 *   <li>{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceContainer <em>Source Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getReconfigurationStep()
 * @model abstract="true"
 * @generated
 */
public interface ReconfigurationStep extends EObject {
	/**
   * Returns the value of the '<em><b>Sub Step</b></em>' containment reference list.
   * The list contents are of type {@link org.coolsoftware.reconfiguration.ReconfigurationStep}.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Step</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Sub Step</em>' containment reference list.
   * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getReconfigurationStep_SubStep()
   * @model containment="true"
   * @generated
   */
	EList<ReconfigurationStep> getSubStep();

	/**
   * Returns the value of the '<em><b>Source Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Component</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Source Component</em>' attribute.
   * @see #setSourceComponent(String)
   * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getReconfigurationStep_SourceComponent()
   * @model required="true"
   * @generated
   */
	String getSourceComponent();

	/**
   * Sets the value of the '{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceComponent <em>Source Component</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source Component</em>' attribute.
   * @see #getSourceComponent()
   * @generated
   */
	void setSourceComponent(String value);

	/**
   * Returns the value of the '<em><b>Source Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Container</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Source Container</em>' attribute.
   * @see #setSourceContainer(String)
   * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getReconfigurationStep_SourceContainer()
   * @model
   * @generated
   */
	String getSourceContainer();

	/**
   * Sets the value of the '{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceContainer <em>Source Container</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source Container</em>' attribute.
   * @see #getSourceContainer()
   * @generated
   */
	void setSourceContainer(String value);

} // ReconfigurationStep
