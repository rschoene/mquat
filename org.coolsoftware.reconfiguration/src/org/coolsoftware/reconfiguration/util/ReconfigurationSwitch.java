/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration.util;

import java.util.List;

import org.coolsoftware.reconfiguration.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.reconfiguration.ReconfigurationPackage
 * @generated
 */
public class ReconfigurationSwitch<T> extends Switch<T> {
	/**
   * The cached model package
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected static ReconfigurationPackage modelPackage;

	/**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public ReconfigurationSwitch() {
    if (modelPackage == null)
    {
      modelPackage = ReconfigurationPackage.eINSTANCE;
    }
  }

	/**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
    return ePackage == modelPackage;
  }

	/**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
    switch (classifierID)
    {
      case ReconfigurationPackage.RECONFIGURATION_PLAN:
      {
        ReconfigurationPlan reconfigurationPlan = (ReconfigurationPlan)theEObject;
        T result = caseReconfigurationPlan(reconfigurationPlan);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ReconfigurationPackage.RECONFIGURATION_STEP:
      {
        ReconfigurationStep reconfigurationStep = (ReconfigurationStep)theEObject;
        T result = caseReconfigurationStep(reconfigurationStep);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ReconfigurationPackage.LOCAL_REPLACEMENT:
      {
        LocalReplacement localReplacement = (LocalReplacement)theEObject;
        T result = caseLocalReplacement(localReplacement);
        if (result == null) result = caseReconfigurationStep(localReplacement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ReconfigurationPackage.MIGRATION:
      {
        Migration migration = (Migration)theEObject;
        T result = caseMigration(migration);
        if (result == null) result = caseReconfigurationStep(migration);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ReconfigurationPackage.RECONFIGURATION_ACTIVITY:
      {
        ReconfigurationActivity reconfigurationActivity = (ReconfigurationActivity)theEObject;
        T result = caseReconfigurationActivity(reconfigurationActivity);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ReconfigurationPackage.DISTRIBUTED_REPLACEMENT:
      {
        DistributedReplacement distributedReplacement = (DistributedReplacement)theEObject;
        T result = caseDistributedReplacement(distributedReplacement);
        if (result == null) result = caseLocalReplacement(distributedReplacement);
        if (result == null) result = caseReconfigurationStep(distributedReplacement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ReconfigurationPackage.DEPLOYMENT:
      {
        Deployment deployment = (Deployment)theEObject;
        T result = caseDeployment(deployment);
        if (result == null) result = caseReconfigurationStep(deployment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case ReconfigurationPackage.UN_DEPLOYMENT:
      {
        UnDeployment unDeployment = (UnDeployment)theEObject;
        T result = caseUnDeployment(unDeployment);
        if (result == null) result = caseReconfigurationStep(unDeployment);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Plan</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Plan</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseReconfigurationPlan(ReconfigurationPlan object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Step</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Step</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseReconfigurationStep(ReconfigurationStep object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Local Replacement</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Local Replacement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseLocalReplacement(LocalReplacement object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Migration</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Migration</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseMigration(Migration object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Activity</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Activity</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseReconfigurationActivity(ReconfigurationActivity object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Distributed Replacement</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Distributed Replacement</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseDistributedReplacement(DistributedReplacement object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Deployment</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Deployment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseDeployment(Deployment object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>Un Deployment</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Un Deployment</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
	public T caseUnDeployment(UnDeployment object) {
    return null;
  }

	/**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
	@Override
	public T defaultCase(EObject object) {
    return null;
  }

} //ReconfigurationSwitch
