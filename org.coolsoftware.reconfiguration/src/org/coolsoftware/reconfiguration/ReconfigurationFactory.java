/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.reconfiguration.ReconfigurationPackage
 * @generated
 */
public interface ReconfigurationFactory extends EFactory {
	/**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	ReconfigurationFactory eINSTANCE = org.coolsoftware.reconfiguration.impl.ReconfigurationFactoryImpl.init();

	/**
   * Returns a new object of class '<em>Plan</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Plan</em>'.
   * @generated
   */
	ReconfigurationPlan createReconfigurationPlan();

	/**
   * Returns a new object of class '<em>Local Replacement</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Local Replacement</em>'.
   * @generated
   */
	LocalReplacement createLocalReplacement();

	/**
   * Returns a new object of class '<em>Migration</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Migration</em>'.
   * @generated
   */
	Migration createMigration();

	/**
   * Returns a new object of class '<em>Activity</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Activity</em>'.
   * @generated
   */
	ReconfigurationActivity createReconfigurationActivity();

	/**
   * Returns a new object of class '<em>Distributed Replacement</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Distributed Replacement</em>'.
   * @generated
   */
	DistributedReplacement createDistributedReplacement();

	/**
   * Returns a new object of class '<em>Deployment</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Deployment</em>'.
   * @generated
   */
	Deployment createDeployment();

	/**
   * Returns a new object of class '<em>Un Deployment</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Un Deployment</em>'.
   * @generated
   */
	UnDeployment createUnDeployment();

	/**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
	ReconfigurationPackage getReconfigurationPackage();

} //ReconfigurationFactory
