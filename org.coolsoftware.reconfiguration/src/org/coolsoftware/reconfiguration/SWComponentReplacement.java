/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SW Component Replacement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.SWComponentReplacement#getTargetComponent <em>Target Component</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getSWComponentReplacement()
 * @model
 * @generated
 */
public interface SWComponentReplacement extends ReconfigurationStep {
	/**
	 * Returns the value of the '<em><b>Target Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Component</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Component</em>' attribute.
	 * @see #setTargetComponent(String)
	 * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getSWComponentReplacement_TargetComponent()
	 * @model required="true"
	 * @generated
	 */
	String getTargetComponent();

	/**
	 * Sets the value of the '{@link org.coolsoftware.reconfiguration.SWComponentReplacement#getTargetComponent <em>Target Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Component</em>' attribute.
	 * @see #getTargetComponent()
	 * @generated
	 */
	void setTargetComponent(String value);

} // SWComponentReplacement
