/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Migration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.Migration#getTargetContainer <em>Target Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getMigration()
 * @model
 * @generated
 */
public interface Migration extends ReconfigurationStep {
	/**
   * Returns the value of the '<em><b>Target Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Container</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Target Container</em>' attribute.
   * @see #setTargetContainer(String)
   * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getMigration_TargetContainer()
   * @model required="true"
   * @generated
   */
	String getTargetContainer();

	/**
   * Sets the value of the '{@link org.coolsoftware.reconfiguration.Migration#getTargetContainer <em>Target Container</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Target Container</em>' attribute.
   * @see #getTargetContainer()
   * @generated
   */
	void setTargetContainer(String value);

} // Migration
