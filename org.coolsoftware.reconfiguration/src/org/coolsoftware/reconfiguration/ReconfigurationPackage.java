/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.reconfiguration.ReconfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface ReconfigurationPackage extends EPackage {
	/**
   * The package name.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNAME = "reconfiguration";

	/**
   * The package namespace URI.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNS_URI = "http://cool-software.org/reconfiguration";

	/**
   * The package namespace name.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNS_PREFIX = "reconf";

	/**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	ReconfigurationPackage eINSTANCE = org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl.init();

	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.ReconfigurationPlanImpl <em>Plan</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPlanImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getReconfigurationPlan()
   * @generated
   */
	int RECONFIGURATION_PLAN = 0;

	/**
   * The feature id for the '<em><b>Activities</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_PLAN__ACTIVITIES = 0;

	/**
   * The number of structural features of the '<em>Plan</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_PLAN_FEATURE_COUNT = 1;

	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.ReconfigurationStepImpl <em>Step</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationStepImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getReconfigurationStep()
   * @generated
   */
	int RECONFIGURATION_STEP = 1;

	/**
   * The feature id for the '<em><b>Sub Step</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_STEP__SUB_STEP = 0;

	/**
   * The feature id for the '<em><b>Source Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_STEP__SOURCE_COMPONENT = 1;

	/**
   * The feature id for the '<em><b>Source Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_STEP__SOURCE_CONTAINER = 2;

	/**
   * The number of structural features of the '<em>Step</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_STEP_FEATURE_COUNT = 3;

	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.LocalReplacementImpl <em>Local Replacement</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.LocalReplacementImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getLocalReplacement()
   * @generated
   */
	int LOCAL_REPLACEMENT = 2;

	/**
   * The feature id for the '<em><b>Sub Step</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int LOCAL_REPLACEMENT__SUB_STEP = RECONFIGURATION_STEP__SUB_STEP;

	/**
   * The feature id for the '<em><b>Source Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int LOCAL_REPLACEMENT__SOURCE_COMPONENT = RECONFIGURATION_STEP__SOURCE_COMPONENT;

	/**
   * The feature id for the '<em><b>Source Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int LOCAL_REPLACEMENT__SOURCE_CONTAINER = RECONFIGURATION_STEP__SOURCE_CONTAINER;

	/**
   * The feature id for the '<em><b>Target Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int LOCAL_REPLACEMENT__TARGET_COMPONENT = RECONFIGURATION_STEP_FEATURE_COUNT + 0;

	/**
   * The number of structural features of the '<em>Local Replacement</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int LOCAL_REPLACEMENT_FEATURE_COUNT = RECONFIGURATION_STEP_FEATURE_COUNT + 1;

	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.MigrationImpl <em>Migration</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.MigrationImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getMigration()
   * @generated
   */
	int MIGRATION = 3;

	/**
   * The feature id for the '<em><b>Sub Step</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int MIGRATION__SUB_STEP = RECONFIGURATION_STEP__SUB_STEP;

	/**
   * The feature id for the '<em><b>Source Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int MIGRATION__SOURCE_COMPONENT = RECONFIGURATION_STEP__SOURCE_COMPONENT;

	/**
   * The feature id for the '<em><b>Source Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int MIGRATION__SOURCE_CONTAINER = RECONFIGURATION_STEP__SOURCE_CONTAINER;

	/**
   * The feature id for the '<em><b>Target Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int MIGRATION__TARGET_CONTAINER = RECONFIGURATION_STEP_FEATURE_COUNT + 0;

	/**
   * The number of structural features of the '<em>Migration</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int MIGRATION_FEATURE_COUNT = RECONFIGURATION_STEP_FEATURE_COUNT + 1;

	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.ReconfigurationActivityImpl <em>Activity</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationActivityImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getReconfigurationActivity()
   * @generated
   */
	int RECONFIGURATION_ACTIVITY = 4;

	/**
   * The feature id for the '<em><b>Steps</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_ACTIVITY__STEPS = 0;

	/**
   * The feature id for the '<em><b>Component Type</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_ACTIVITY__COMPONENT_TYPE = 1;

	/**
   * The number of structural features of the '<em>Activity</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int RECONFIGURATION_ACTIVITY_FEATURE_COUNT = 2;


	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.DistributedReplacementImpl <em>Distributed Replacement</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.DistributedReplacementImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getDistributedReplacement()
   * @generated
   */
	int DISTRIBUTED_REPLACEMENT = 5;

	/**
   * The feature id for the '<em><b>Sub Step</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DISTRIBUTED_REPLACEMENT__SUB_STEP = LOCAL_REPLACEMENT__SUB_STEP;

	/**
   * The feature id for the '<em><b>Source Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DISTRIBUTED_REPLACEMENT__SOURCE_COMPONENT = LOCAL_REPLACEMENT__SOURCE_COMPONENT;

	/**
   * The feature id for the '<em><b>Source Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DISTRIBUTED_REPLACEMENT__SOURCE_CONTAINER = LOCAL_REPLACEMENT__SOURCE_CONTAINER;

	/**
   * The feature id for the '<em><b>Target Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DISTRIBUTED_REPLACEMENT__TARGET_COMPONENT = LOCAL_REPLACEMENT__TARGET_COMPONENT;

	/**
   * The feature id for the '<em><b>Target Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER = LOCAL_REPLACEMENT_FEATURE_COUNT + 0;

	/**
   * The number of structural features of the '<em>Distributed Replacement</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DISTRIBUTED_REPLACEMENT_FEATURE_COUNT = LOCAL_REPLACEMENT_FEATURE_COUNT + 1;


	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.DeploymentImpl <em>Deployment</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.DeploymentImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getDeployment()
   * @generated
   */
	int DEPLOYMENT = 6;

	/**
   * The feature id for the '<em><b>Sub Step</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DEPLOYMENT__SUB_STEP = RECONFIGURATION_STEP__SUB_STEP;

	/**
   * The feature id for the '<em><b>Source Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DEPLOYMENT__SOURCE_COMPONENT = RECONFIGURATION_STEP__SOURCE_COMPONENT;

	/**
   * The feature id for the '<em><b>Source Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DEPLOYMENT__SOURCE_CONTAINER = RECONFIGURATION_STEP__SOURCE_CONTAINER;

	/**
   * The feature id for the '<em><b>Target Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DEPLOYMENT__TARGET_CONTAINER = RECONFIGURATION_STEP_FEATURE_COUNT + 0;

	/**
   * The number of structural features of the '<em>Deployment</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int DEPLOYMENT_FEATURE_COUNT = RECONFIGURATION_STEP_FEATURE_COUNT + 1;


	/**
   * The meta object id for the '{@link org.coolsoftware.reconfiguration.impl.UnDeploymentImpl <em>Un Deployment</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.reconfiguration.impl.UnDeploymentImpl
   * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getUnDeployment()
   * @generated
   */
	int UN_DEPLOYMENT = 7;

	/**
   * The feature id for the '<em><b>Sub Step</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UN_DEPLOYMENT__SUB_STEP = RECONFIGURATION_STEP__SUB_STEP;

	/**
   * The feature id for the '<em><b>Source Component</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UN_DEPLOYMENT__SOURCE_COMPONENT = RECONFIGURATION_STEP__SOURCE_COMPONENT;

	/**
   * The feature id for the '<em><b>Source Container</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UN_DEPLOYMENT__SOURCE_CONTAINER = RECONFIGURATION_STEP__SOURCE_CONTAINER;

	/**
   * The number of structural features of the '<em>Un Deployment</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UN_DEPLOYMENT_FEATURE_COUNT = RECONFIGURATION_STEP_FEATURE_COUNT + 0;


	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.ReconfigurationPlan <em>Plan</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Plan</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationPlan
   * @generated
   */
	EClass getReconfigurationPlan();

	/**
   * Returns the meta object for the containment reference list '{@link org.coolsoftware.reconfiguration.ReconfigurationPlan#getActivities <em>Activities</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Activities</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationPlan#getActivities()
   * @see #getReconfigurationPlan()
   * @generated
   */
	EReference getReconfigurationPlan_Activities();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.ReconfigurationStep <em>Step</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Step</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationStep
   * @generated
   */
	EClass getReconfigurationStep();

	/**
   * Returns the meta object for the containment reference list '{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSubStep <em>Sub Step</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sub Step</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationStep#getSubStep()
   * @see #getReconfigurationStep()
   * @generated
   */
	EReference getReconfigurationStep_SubStep();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceComponent <em>Source Component</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Source Component</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceComponent()
   * @see #getReconfigurationStep()
   * @generated
   */
	EAttribute getReconfigurationStep_SourceComponent();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceContainer <em>Source Container</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Source Container</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationStep#getSourceContainer()
   * @see #getReconfigurationStep()
   * @generated
   */
	EAttribute getReconfigurationStep_SourceContainer();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.LocalReplacement <em>Local Replacement</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Local Replacement</em>'.
   * @see org.coolsoftware.reconfiguration.LocalReplacement
   * @generated
   */
	EClass getLocalReplacement();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.reconfiguration.LocalReplacement#getTargetComponent <em>Target Component</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Target Component</em>'.
   * @see org.coolsoftware.reconfiguration.LocalReplacement#getTargetComponent()
   * @see #getLocalReplacement()
   * @generated
   */
	EAttribute getLocalReplacement_TargetComponent();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.Migration <em>Migration</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Migration</em>'.
   * @see org.coolsoftware.reconfiguration.Migration
   * @generated
   */
	EClass getMigration();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.reconfiguration.Migration#getTargetContainer <em>Target Container</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Target Container</em>'.
   * @see org.coolsoftware.reconfiguration.Migration#getTargetContainer()
   * @see #getMigration()
   * @generated
   */
	EAttribute getMigration_TargetContainer();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.ReconfigurationActivity <em>Activity</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Activity</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationActivity
   * @generated
   */
	EClass getReconfigurationActivity();

	/**
   * Returns the meta object for the containment reference list '{@link org.coolsoftware.reconfiguration.ReconfigurationActivity#getSteps <em>Steps</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Steps</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationActivity#getSteps()
   * @see #getReconfigurationActivity()
   * @generated
   */
	EReference getReconfigurationActivity_Steps();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.reconfiguration.ReconfigurationActivity#getComponentType <em>Component Type</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Component Type</em>'.
   * @see org.coolsoftware.reconfiguration.ReconfigurationActivity#getComponentType()
   * @see #getReconfigurationActivity()
   * @generated
   */
	EAttribute getReconfigurationActivity_ComponentType();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.DistributedReplacement <em>Distributed Replacement</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Distributed Replacement</em>'.
   * @see org.coolsoftware.reconfiguration.DistributedReplacement
   * @generated
   */
	EClass getDistributedReplacement();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.reconfiguration.DistributedReplacement#getTargetContainer <em>Target Container</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Target Container</em>'.
   * @see org.coolsoftware.reconfiguration.DistributedReplacement#getTargetContainer()
   * @see #getDistributedReplacement()
   * @generated
   */
	EAttribute getDistributedReplacement_TargetContainer();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.Deployment <em>Deployment</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Deployment</em>'.
   * @see org.coolsoftware.reconfiguration.Deployment
   * @generated
   */
	EClass getDeployment();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.reconfiguration.Deployment#getTargetContainer <em>Target Container</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Target Container</em>'.
   * @see org.coolsoftware.reconfiguration.Deployment#getTargetContainer()
   * @see #getDeployment()
   * @generated
   */
	EAttribute getDeployment_TargetContainer();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.reconfiguration.UnDeployment <em>Un Deployment</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Un Deployment</em>'.
   * @see org.coolsoftware.reconfiguration.UnDeployment
   * @generated
   */
	EClass getUnDeployment();

	/**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
	ReconfigurationFactory getReconfigurationFactory();

	/**
   * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
   * @generated
   */
	interface Literals {
		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.ReconfigurationPlanImpl <em>Plan</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPlanImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getReconfigurationPlan()
     * @generated
     */
		EClass RECONFIGURATION_PLAN = eINSTANCE.getReconfigurationPlan();

		/**
     * The meta object literal for the '<em><b>Activities</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference RECONFIGURATION_PLAN__ACTIVITIES = eINSTANCE.getReconfigurationPlan_Activities();

		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.ReconfigurationStepImpl <em>Step</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationStepImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getReconfigurationStep()
     * @generated
     */
		EClass RECONFIGURATION_STEP = eINSTANCE.getReconfigurationStep();

		/**
     * The meta object literal for the '<em><b>Sub Step</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference RECONFIGURATION_STEP__SUB_STEP = eINSTANCE.getReconfigurationStep_SubStep();

		/**
     * The meta object literal for the '<em><b>Source Component</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute RECONFIGURATION_STEP__SOURCE_COMPONENT = eINSTANCE.getReconfigurationStep_SourceComponent();

		/**
     * The meta object literal for the '<em><b>Source Container</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute RECONFIGURATION_STEP__SOURCE_CONTAINER = eINSTANCE.getReconfigurationStep_SourceContainer();

		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.LocalReplacementImpl <em>Local Replacement</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.LocalReplacementImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getLocalReplacement()
     * @generated
     */
		EClass LOCAL_REPLACEMENT = eINSTANCE.getLocalReplacement();

		/**
     * The meta object literal for the '<em><b>Target Component</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute LOCAL_REPLACEMENT__TARGET_COMPONENT = eINSTANCE.getLocalReplacement_TargetComponent();

		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.MigrationImpl <em>Migration</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.MigrationImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getMigration()
     * @generated
     */
		EClass MIGRATION = eINSTANCE.getMigration();

		/**
     * The meta object literal for the '<em><b>Target Container</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute MIGRATION__TARGET_CONTAINER = eINSTANCE.getMigration_TargetContainer();

		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.ReconfigurationActivityImpl <em>Activity</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationActivityImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getReconfigurationActivity()
     * @generated
     */
		EClass RECONFIGURATION_ACTIVITY = eINSTANCE.getReconfigurationActivity();

		/**
     * The meta object literal for the '<em><b>Steps</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference RECONFIGURATION_ACTIVITY__STEPS = eINSTANCE.getReconfigurationActivity_Steps();

		/**
     * The meta object literal for the '<em><b>Component Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute RECONFIGURATION_ACTIVITY__COMPONENT_TYPE = eINSTANCE.getReconfigurationActivity_ComponentType();

		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.DistributedReplacementImpl <em>Distributed Replacement</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.DistributedReplacementImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getDistributedReplacement()
     * @generated
     */
		EClass DISTRIBUTED_REPLACEMENT = eINSTANCE.getDistributedReplacement();

		/**
     * The meta object literal for the '<em><b>Target Container</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute DISTRIBUTED_REPLACEMENT__TARGET_CONTAINER = eINSTANCE.getDistributedReplacement_TargetContainer();

		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.DeploymentImpl <em>Deployment</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.DeploymentImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getDeployment()
     * @generated
     */
		EClass DEPLOYMENT = eINSTANCE.getDeployment();

		/**
     * The meta object literal for the '<em><b>Target Container</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute DEPLOYMENT__TARGET_CONTAINER = eINSTANCE.getDeployment_TargetContainer();

		/**
     * The meta object literal for the '{@link org.coolsoftware.reconfiguration.impl.UnDeploymentImpl <em>Un Deployment</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.reconfiguration.impl.UnDeploymentImpl
     * @see org.coolsoftware.reconfiguration.impl.ReconfigurationPackageImpl#getUnDeployment()
     * @generated
     */
		EClass UN_DEPLOYMENT = eINSTANCE.getUnDeployment();

	}

} //ReconfigurationPackage
