/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.reconfiguration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.reconfiguration.ReconfigurationActivity#getSteps <em>Steps</em>}</li>
 *   <li>{@link org.coolsoftware.reconfiguration.ReconfigurationActivity#getComponentType <em>Component Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getReconfigurationActivity()
 * @model
 * @generated
 */
public interface ReconfigurationActivity extends EObject {
	/**
   * Returns the value of the '<em><b>Steps</b></em>' containment reference list.
   * The list contents are of type {@link org.coolsoftware.reconfiguration.ReconfigurationStep}.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Steps</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Steps</em>' containment reference list.
   * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getReconfigurationActivity_Steps()
   * @model containment="true" required="true"
   * @generated
   */
	EList<ReconfigurationStep> getSteps();

	/**
   * Returns the value of the '<em><b>Component Type</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Component Type</em>' attribute.
   * @see #setComponentType(String)
   * @see org.coolsoftware.reconfiguration.ReconfigurationPackage#getReconfigurationActivity_ComponentType()
   * @model required="true"
   * @generated
   */
	String getComponentType();

	/**
   * Sets the value of the '{@link org.coolsoftware.reconfiguration.ReconfigurationActivity#getComponentType <em>Component Type</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Component Type</em>' attribute.
   * @see #getComponentType()
   * @generated
   */
	void setComponentType(String value);

} // ReconfigurationActivity
