SYNTAXDEF config
FOR <http://cool-software.org/reconfiguration>
START ReconfigurationPlan

OPTIONS {
	reloadGeneratorModel = "true";
	//overrideClasspath = "false";
	//overrideManifest = "false";
	//overridePluginXML = "false";
	additionalDependencies = "org.coolsoftware.coolcomponents";
	additionalUIDependencies = "org.coolsoftware.coolcomponents";
	additionalExports = "org.coolsoftware.reconfiguration,org.coolsoftware.reconfiguration.resource.config.analysis.util";
	disableDebugSupport = "true";
	disableLaunchSupport = "true";
}

TOKENS {
	DEFINE SL_COMMENT				$ '--'(~('\n'|'\r'|'\uffff'))* $ COLLECT IN comments;
	DEFINE ML_COMMENT				$ '/*'.*'*/'$ COLLECT IN comments;
	
	DEFINE FILE_NAME
		$ '[' (('A' .. 'Z' | 'a'..'z') ':' '/')? ('.' | '/') ('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' | '/' | '.' | ' ')+ ']' $;
}

TOKENSTYLES {
	"ML_COMMENT" COLOR #008000, ITALIC;
	"SL_COMMENT" COLOR #008000, ITALIC;
}




RULES {
	ReconfigurationPlan ::= "reconfigurationPlan"  "{" (!1 activities !0 )* !0 "}";
	
	ReconfigurationActivity ::= "reconfiguration" #0 "for" #0 "SWComponentType" #0 componentType[] #0 "{" (!1 steps !0)+  "}"!0;
	
	LocalReplacement ::= "replace" #0 "SWComponent" #0 sourceComponent[] #0 "on" #0 sourceContainer[]  #0 "with" #0 targetComponent[] ("{"(subStep | (subStep)*)"}")?;
	@SuppressWarnings(featureWithoutSyntax)
	
	DistributedReplacement ::= "replace" #0 "SWComponent" #0 sourceComponent[] #0 "on" #0 sourceContainer[]  #0 "with" #0 targetComponent[] #0 "on" targetContainer[];
	
	@SuppressWarnings(featureWithoutSyntax)
	Migration ::= "migrate" #0 "SWComponent" #0 sourceComponent[] #0 "from" #0 sourceContainer[] #0 "to" #0 targetContainer[];
	
	Deployment ::= "deploy" #0 "SWComponent" #0 sourceComponent[] #0 "on" #0 targetContainer[];
	
	UnDeployment ::= "undeploy" #0 "SWComponent" #0 sourceComponent[] #0 "from" sourceContainer[];
}