/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.ui;

/**
 * An adapter from the Eclipse
 * <code>org.eclipse.jface.text.rules.ITokenScanner</code> interface to the
 * generated lexer.
 */
public class EclTokenScanner implements org.eclipse.jface.text.rules.ITokenScanner {
	
	private org.coolsoftware.ecl.resource.ecl.IEclTextScanner lexer;
	private org.coolsoftware.ecl.resource.ecl.IEclTextToken currentToken;
	private java.util.List<org.coolsoftware.ecl.resource.ecl.IEclTextToken> nextTokens;
	private int offset;
	private String languageId;
	private org.eclipse.jface.preference.IPreferenceStore store;
	private org.coolsoftware.ecl.resource.ecl.ui.EclColorManager colorManager;
	private org.coolsoftware.ecl.resource.ecl.IEclTextResource resource;
	
	/**
	 * 
	 * @param colorManager A manager to obtain color objects
	 */
	public EclTokenScanner(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource, org.coolsoftware.ecl.resource.ecl.ui.EclColorManager colorManager) {
		this.resource = resource;
		this.colorManager = colorManager;
		this.lexer = new org.coolsoftware.ecl.resource.ecl.mopp.EclMetaInformation().createLexer();
		this.languageId = new org.coolsoftware.ecl.resource.ecl.mopp.EclMetaInformation().getSyntaxName();
		org.coolsoftware.ecl.resource.ecl.ui.EclUIPlugin plugin = org.coolsoftware.ecl.resource.ecl.ui.EclUIPlugin.getDefault();
		if (plugin != null) {
			this.store = plugin.getPreferenceStore();
		}
		this.nextTokens = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.IEclTextToken>();
	}
	
	public int getTokenLength() {
		return currentToken.getLength();
	}
	
	public int getTokenOffset() {
		return offset + currentToken.getOffset();
	}
	
	public org.eclipse.jface.text.rules.IToken nextToken() {
		boolean isOriginalToken = true;
		if (!nextTokens.isEmpty()) {
			currentToken = nextTokens.remove(0);
			isOriginalToken = false;
		} else {
			currentToken = lexer.getNextToken();
		}
		if (currentToken == null || !currentToken.canBeUsedForSyntaxHighlighting()) {
			return org.eclipse.jface.text.rules.Token.EOF;
		}
		
		if (isOriginalToken) {
			splitCurrentToken();
		}
		
		org.eclipse.jface.text.TextAttribute textAttribute = null;
		String tokenName = currentToken.getName();
		if (tokenName != null) {
			org.coolsoftware.ecl.resource.ecl.IEclTokenStyle staticStyle = getStaticTokenStyle();
			// now call dynamic token styler to allow to apply modifications to the static
			// style
			org.coolsoftware.ecl.resource.ecl.IEclTokenStyle dynamicStyle = getDynamicTokenStyle(staticStyle);
			if (dynamicStyle != null) {
				textAttribute = getTextAttribute(dynamicStyle);
			}
		}
		
		return new org.eclipse.jface.text.rules.Token(textAttribute);
	}
	
	public void setRange(org.eclipse.jface.text.IDocument document, int offset, int length) {
		this.offset = offset;
		try {
			lexer.setText(document.get(offset, length));
		} catch (org.eclipse.jface.text.BadLocationException e) {
			// ignore this error. It might occur during editing when locations are outdated
			// quickly.
		}
	}
	
	public String getTokenText() {
		return currentToken.getText();
	}
	
	public int[] convertToIntArray(org.eclipse.swt.graphics.RGB rgb) {
		if (rgb == null) {
			return null;
		}
		return new int[] {rgb.red, rgb.green, rgb.blue};
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTokenStyle getStaticTokenStyle() {
		org.coolsoftware.ecl.resource.ecl.IEclTokenStyle staticStyle = null;
		String tokenName = currentToken.getName();
		String enableKey = org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.ENABLE);
		boolean enabled = store.getBoolean(enableKey);
		if (enabled) {
			String colorKey = org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.COLOR);
			org.eclipse.swt.graphics.RGB foregroundRGB = org.eclipse.jface.preference.PreferenceConverter.getColor(store, colorKey);
			org.eclipse.swt.graphics.RGB backgroundRGB = null;
			boolean bold = store.getBoolean(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.BOLD));
			boolean italic = store.getBoolean(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.ITALIC));
			boolean strikethrough = store.getBoolean(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.STRIKETHROUGH));
			boolean underline = store.getBoolean(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.UNDERLINE));
			staticStyle = new org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyle(convertToIntArray(foregroundRGB), convertToIntArray(backgroundRGB), bold, italic, strikethrough, underline);
		}
		return staticStyle;
	}
	
	public org.coolsoftware.ecl.resource.ecl.IEclTokenStyle getDynamicTokenStyle(org.coolsoftware.ecl.resource.ecl.IEclTokenStyle staticStyle) {
		org.coolsoftware.ecl.resource.ecl.mopp.EclDynamicTokenStyler dynamicTokenStyler = new org.coolsoftware.ecl.resource.ecl.mopp.EclDynamicTokenStyler();
		org.coolsoftware.ecl.resource.ecl.IEclTokenStyle dynamicStyle = dynamicTokenStyler.getDynamicTokenStyle(resource, currentToken, staticStyle);
		return dynamicStyle;
	}
	
	public org.eclipse.jface.text.TextAttribute getTextAttribute(org.coolsoftware.ecl.resource.ecl.IEclTokenStyle tokeStyle) {
		int[] foregroundColorArray = tokeStyle.getColorAsRGB();
		org.eclipse.swt.graphics.Color foregroundColor = null;
		if (colorManager != null) {
			foregroundColor = colorManager.getColor(new org.eclipse.swt.graphics.RGB(foregroundColorArray[0], foregroundColorArray[1], foregroundColorArray[2]));
		}
		int[] backgroundColorArray = tokeStyle.getBackgroundColorAsRGB();
		org.eclipse.swt.graphics.Color backgroundColor = null;
		if (backgroundColorArray != null) {
			org.eclipse.swt.graphics.RGB backgroundRGB = new org.eclipse.swt.graphics.RGB(backgroundColorArray[0], backgroundColorArray[1], backgroundColorArray[2]);
			if (colorManager != null) {
				backgroundColor = colorManager.getColor(backgroundRGB);
			}
		}
		int style = org.eclipse.swt.SWT.NORMAL;
		if (tokeStyle.isBold()) {
			style = style | org.eclipse.swt.SWT.BOLD;
		}
		if (tokeStyle.isItalic()) {
			style = style | org.eclipse.swt.SWT.ITALIC;
		}
		if (tokeStyle.isStrikethrough()) {
			style = style | org.eclipse.jface.text.TextAttribute.STRIKETHROUGH;
		}
		if (tokeStyle.isUnderline()) {
			style = style | org.eclipse.jface.text.TextAttribute.UNDERLINE;
		}
		return new org.eclipse.jface.text.TextAttribute(foregroundColor, backgroundColor, style);
	}
	
	/**
	 * Tries to split the current token if it contains task items.
	 */
	public void splitCurrentToken() {
		final String text = currentToken.getText();
		final String name = currentToken.getName();
		final int line = currentToken.getLine();
		final int charStart = currentToken.getOffset();
		final int column = currentToken.getColumn();
		
		java.util.List<org.coolsoftware.ecl.resource.ecl.mopp.EclTaskItem> taskItems = new org.coolsoftware.ecl.resource.ecl.mopp.EclTaskItemDetector().findTaskItems(text, line, charStart);
		
		// this is the offset for the next token to be added
		int offset = charStart;
		int itemBeginRelative;
		java.util.List<org.coolsoftware.ecl.resource.ecl.IEclTextToken> newItems = new java.util.ArrayList<org.coolsoftware.ecl.resource.ecl.IEclTextToken>();
		for (org.coolsoftware.ecl.resource.ecl.mopp.EclTaskItem taskItem : taskItems) {
			int itemBegin = taskItem.getCharStart();
			int itemLine = taskItem.getLine();
			int itemColumn = 0;
			
			itemBeginRelative = itemBegin - charStart;
			// create token before task item (TODO if required)
			String textBefore = text.substring(offset - charStart, itemBeginRelative);
			int textBeforeLength = textBefore.length();
			newItems.add(new org.coolsoftware.ecl.resource.ecl.mopp.EclTextToken(name, textBefore, offset, textBeforeLength, line, column, true));
			
			// create token for the task item itself
			offset = offset + textBeforeLength;
			String itemText = taskItem.getKeyword();
			int itemTextLength = itemText.length();
			newItems.add(new org.coolsoftware.ecl.resource.ecl.mopp.EclTextToken(org.coolsoftware.ecl.resource.ecl.mopp.EclTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME, itemText, offset, itemTextLength, itemLine, itemColumn, true));
			
			offset = offset + itemTextLength;
		}
		
		if (!taskItems.isEmpty()) {
			// create token after last task item (TODO if required)
			String textAfter = text.substring(offset - charStart);
			newItems.add(new org.coolsoftware.ecl.resource.ecl.mopp.EclTextToken(name, textAfter, offset, textAfter.length(), line, column, true));
		}
		
		if (!newItems.isEmpty()) {
			// replace tokens
			currentToken = newItems.remove(0);
			nextTokens = newItems;
		}
		
	}
}
