/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.ui;

public class EclUIMetaInformation extends org.coolsoftware.ecl.resource.ecl.mopp.EclMetaInformation {
	
	public org.coolsoftware.ecl.resource.ecl.IEclHoverTextProvider getHoverTextProvider() {
		return new org.coolsoftware.ecl.resource.ecl.ui.EclHoverTextProvider();
	}
	
	public org.coolsoftware.ecl.resource.ecl.ui.EclImageProvider getImageProvider() {
		return org.coolsoftware.ecl.resource.ecl.ui.EclImageProvider.INSTANCE;
	}
	
	public org.coolsoftware.ecl.resource.ecl.ui.EclColorManager createColorManager() {
		return new org.coolsoftware.ecl.resource.ecl.ui.EclColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(org.coolsoftware.ecl.resource.ecl.IEclTextResource,
	 * org.coolsoftware.ecl.resource.ecl.ui.EclColorManager) instead.
	 */
	public org.coolsoftware.ecl.resource.ecl.ui.EclTokenScanner createTokenScanner(org.coolsoftware.ecl.resource.ecl.ui.EclColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public org.coolsoftware.ecl.resource.ecl.ui.EclTokenScanner createTokenScanner(org.coolsoftware.ecl.resource.ecl.IEclTextResource resource, org.coolsoftware.ecl.resource.ecl.ui.EclColorManager colorManager) {
		return new org.coolsoftware.ecl.resource.ecl.ui.EclTokenScanner(resource, colorManager);
	}
	
	public org.coolsoftware.ecl.resource.ecl.ui.EclCodeCompletionHelper createCodeCompletionHelper() {
		return new org.coolsoftware.ecl.resource.ecl.ui.EclCodeCompletionHelper();
	}
	
}
