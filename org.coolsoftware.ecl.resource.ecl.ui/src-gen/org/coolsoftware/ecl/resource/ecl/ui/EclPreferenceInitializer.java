/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.ecl.resource.ecl.ui;

/**
 * A class used to initialize default preference values.
 */
public class EclPreferenceInitializer extends org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer {
	
	public void initializeDefaultPreferences() {
		
		initializeDefaultSyntaxHighlighting();
		initializeDefaultBrackets();
		
		org.eclipse.jface.preference.IPreferenceStore store = org.coolsoftware.ecl.resource.ecl.ui.EclUIPlugin.getDefault().getPreferenceStore();
		// Set default value for matching brackets
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclPreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR, "192,192,192");
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclPreferenceConstants.EDITOR_MATCHING_BRACKETS_CHECKBOX, true);
		
	}
	
	private void initializeDefaultBrackets() {
		org.eclipse.jface.preference.IPreferenceStore store = org.coolsoftware.ecl.resource.ecl.ui.EclUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultBrackets(store, new org.coolsoftware.ecl.resource.ecl.mopp.EclMetaInformation());
	}
	
	public void initializeDefaultSyntaxHighlighting() {
		org.eclipse.jface.preference.IPreferenceStore store = org.coolsoftware.ecl.resource.ecl.ui.EclUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultSyntaxHighlighting(store, new org.coolsoftware.ecl.resource.ecl.mopp.EclMetaInformation());
	}
	
	private void initializeDefaultBrackets(org.eclipse.jface.preference.IPreferenceStore store, org.coolsoftware.ecl.resource.ecl.IEclMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		// set default brackets for ITextResource bracket set
		org.coolsoftware.ecl.resource.ecl.ui.EclBracketSet bracketSet = new org.coolsoftware.ecl.resource.ecl.ui.EclBracketSet(null, null);
		final java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclBracketPair> bracketPairs = metaInformation.getBracketPairs();
		if (bracketPairs != null) {
			for (org.coolsoftware.ecl.resource.ecl.IEclBracketPair bracketPair : bracketPairs) {
				bracketSet.addBracketPair(bracketPair.getOpeningBracket(), bracketPair.getClosingBracket(), bracketPair.isClosingEnabledInside());
			}
		}
		store.setDefault(languageId + org.coolsoftware.ecl.resource.ecl.ui.EclPreferenceConstants.EDITOR_BRACKETS_SUFFIX, bracketSet.getBracketString());
	}
	
	private void initializeDefaultSyntaxHighlighting(org.eclipse.jface.preference.IPreferenceStore store, org.coolsoftware.ecl.resource.ecl.mopp.EclMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		String[] tokenNames = metaInformation.getSyntaxHighlightableTokenNames();
		if (tokenNames == null) {
			return;
		}
		for (int i = 0; i < tokenNames.length; i++) {
			String tokenName = tokenNames[i];
			org.coolsoftware.ecl.resource.ecl.IEclTokenStyle style = metaInformation.getDefaultTokenStyle(tokenName);
			if (style != null) {
				String color = getColorString(style.getColorAsRGB());
				setProperties(store, languageId, tokenName, color, style.isBold(), true, style.isItalic(), style.isStrikethrough(), style.isUnderline());
			} else {
				setProperties(store, languageId, tokenName, "0,0,0", false, false, false, false, false);
			}
		}
	}
	
	private void setProperties(org.eclipse.jface.preference.IPreferenceStore store, String languageID, String tokenName, String color, boolean bold, boolean enable, boolean italic, boolean strikethrough, boolean underline) {
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.BOLD), bold);
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.COLOR), color);
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.ENABLE), enable);
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.ITALIC), italic);
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.STRIKETHROUGH), strikethrough);
		store.setDefault(org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, org.coolsoftware.ecl.resource.ecl.ui.EclSyntaxColoringHelper.StyleProperty.UNDERLINE), underline);
	}
	
	private String getColorString(int[] colorAsRGB) {
		if (colorAsRGB == null) {
			return "0,0,0";
		}
		if (colorAsRGB.length != 3) {
			return "0,0,0";
		}
		return colorAsRGB[0] + "," +colorAsRGB[1] + ","+ colorAsRGB[2];
	}
}
