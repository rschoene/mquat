/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j;

import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.ecl.SWContractMode;
import org.opt4j.core.problem.Decoder;
import org.opt4j.core.problem.PhenotypeWrapper;
import org.opt4j.genotype.PermutationGenotype;

/**
 * 
 * @author Sebastian Götz
 */
public class MQuATConfigDecoder implements Decoder<PermutationGenotype<Map<SWContractMode, ContainerProvider>>, PhenotypeWrapper<Map<SWContractMode, ContainerProvider>>> {

	@Override
	public PhenotypeWrapper<Map<SWContractMode, ContainerProvider>> decode(
			PermutationGenotype<Map<SWContractMode, ContainerProvider>> gene) {
		
		Map<SWContractMode, ContainerProvider> config = gene.iterator().next();		
		return new PhenotypeWrapper<Map<SWContractMode, ContainerProvider>>(config);
	}

}
