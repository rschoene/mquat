/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.Request;
import org.opt4j.core.problem.Creator;
import org.opt4j.genotype.PermutationGenotype;
import org.opt4j.genotype.SelectGenotype;

/**
 * 
 * @author Sebastian Götz
 */
public class MQuATConfigCreator implements Creator<SelectGenotype<Map<SWContractMode,ContainerProvider>>> {

	List<ContainerProvider> availContainers = new ArrayList<ContainerProvider>();
	Map<SWComponentType, List<SWContractMode>> reqCompsAndImpls = new HashMap<SWComponentType, List<SWContractMode>>();

	
	public MQuATConfigCreator(Request req, EclFile directContracts,
			StructuralModel hwTypes, StructuralModel swTypes,
			VariantModel infrastructure) {

		// find all containers
		Resource res = (Resource) infrastructure.getRoot();
		for (Resource container : res.getSubresources()) {
			availContainers.add((ContainerProvider)container);
		}

		// create possible configurations
		reqCompsAndImpls = resolveDependencies(req.getComponent(), directContracts);
	}

	private Map<SWComponentType, List<SWContractMode>> resolveDependencies(SWComponentType type, EclFile contracts) {
		
		Map<SWComponentType, List<SWContractMode>> reqCompsAndImpls = new HashMap<SWComponentType, List<SWContractMode>>();
		
		for (EclContract c : contracts.getContracts()) {
			if (c instanceof SWComponentContract) {
				SWComponentContract swc = (SWComponentContract) c;
				
				for (SWContractMode m : swc.getModes()) {
					
					List<SWContractMode> impls = reqCompsAndImpls.get(type);
					if (impls == null)
						impls = new ArrayList<SWContractMode>();
					impls.add(m);
					reqCompsAndImpls.put(type, impls);
					
					for (SWContractClause cl : m.getClauses()) {
						if (cl instanceof SWComponentRequirementClause) {
							SWComponentRequirementClause rc = (SWComponentRequirementClause) cl;
							SWComponentType reqType = rc
									.getRequiredComponentType();
							EclFile reqContracts = loadEclFile(reqType
									.getEclUri());
							reqCompsAndImpls.putAll(resolveDependencies(reqType, reqContracts));
						}
					}
				}

			}
		}
		return reqCompsAndImpls;
	}

	private EclFile loadEclFile(String eclUri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SelectGenotype<Map<SWContractMode,ContainerProvider>> create() {
		
		List<Map<SWContractMode,ContainerProvider>> allMappings = new ArrayList<Map<SWContractMode,ContainerProvider>>();
		
		Map<SWContractMode, ContainerProvider> mapping = new HashMap<SWContractMode, ContainerProvider>();
		
		//create all possible mappings
		// first create all possible impl selections
		List<List<SWContractMode>> allPossibleSelections = new ArrayList<List<SWContractMode>>();
		int depth = reqCompsAndImpls.keySet().size();
		int curLevel = 0;
		for(SWComponentType type : reqCompsAndImpls.keySet()) {
			List<SWContractMode> selection = new ArrayList<SWContractMode>();
			for(int i = 0; i < reqCompsAndImpls.get(type).size(); i++) {
				SWContractMode selectedImpl = reqCompsAndImpls.get(type).get(i);
				if(curLevel == depth-1) {
					//end reached
				} else {
					//duplicate current selection + add n
				}
			}
			curLevel++;
		}
		// than create all possible mappings of these selections to resources
		
		allMappings.add(mapping);		
		
		SelectGenotype<Map<SWContractMode, ContainerProvider>> gene = new SelectGenotype<Map<SWContractMode,ContainerProvider>>(allMappings);
		return gene;
	}

	/** Create a list of all combinations of elements from a and b.
	 */
	public <T> List<List<T>> crossproduct(List<T> a, List<T> b) {
		List<List<T>> ret = new ArrayList<List<T>>();
		for(T x : a) {
			for(T y : b) {
				List<T> sub = new ArrayList<T>();
				sub.add(x);
				sub.add(y);
				ret.add(sub);
			}
		}
		return ret;
	}
	
	public <T> List<List<T>> crossproduct2(List<List<T>> src) {
		List<List<T>> ret = new ArrayList<List<T>>();
		if(src.size() == 0) {
			return src;
		} else if(src.size() == 1) {
			return src;
		} else {
			List<T> list = src.get(0);
			for(T elem : list) {
				src.remove(list);
				crossproduct2(src);
			}
		}
		return ret;
	}
}
