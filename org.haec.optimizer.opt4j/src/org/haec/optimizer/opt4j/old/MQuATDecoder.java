/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j.old;

import java.util.HashMap;
import java.util.Map;

import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.Implementation;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.opt4j.core.problem.Decoder;
import org.opt4j.core.problem.PhenotypeWrapper;
import org.opt4j.genotype.SelectMapGenotype;

/**
 * 
 * @author Sebastian Götz
 */
public class MQuATDecoder implements Decoder<SelectMapGenotype<SoftwareComponent, Container>, PhenotypeWrapper<Map<QualityMode, Container>>> {

	@Override
	public PhenotypeWrapper<Map<QualityMode, Container>> decode(
			SelectMapGenotype<SoftwareComponent, Container> genotype) {
		
		Map<QualityMode, Container> ret = new HashMap<QualityMode, Container>();
		
		for(SoftwareComponent sc : genotype.getKeys()) {
			Container c = genotype.getValue(sc);
			QualityMode qm = null;
			for(Implementation impl : sc.getImplementations()) {
				for(QualityMode mode : impl.getQualityModes()) {
					qm = mode;
					if(Math.random() > 0.5) break;
				}
			}
			ret.put(qm, c);
		}
		
		return new PhenotypeWrapper<Map<QualityMode,Container>>(ret);
	} 
	
	
}
