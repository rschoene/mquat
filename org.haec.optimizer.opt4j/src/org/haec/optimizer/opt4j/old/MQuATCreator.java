/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j.old;
import java.util.Random;

import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.opt4j.core.problem.Creator;
import org.opt4j.genotype.SelectMapGenotype;

import com.google.inject.Inject;


/**
 * 
 * @author Sebastian Götz
 */
public class MQuATCreator implements Creator<SelectMapGenotype<SoftwareComponent, Container>> {

	private MQuATProblem problem;
	
	@Inject
	public MQuATCreator(MQuATProblem problem) {
		this.problem = problem;
	}
	
	@Override
	public SelectMapGenotype<SoftwareComponent, Container> create() {
		SelectMapGenotype<SoftwareComponent, Container> genotype = new SelectMapGenotype<SoftwareComponent, Container>(problem.getComponents(), problem.getContainers());
		genotype.init(new Random());
		return genotype;
	}
	
}
