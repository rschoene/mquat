/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j.old;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebParam.Mode;

import org.haec.optimizer.aco.AntGraph;
import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.Implementation;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.SoftwareComponent;
import org.haec.optimizer.aco.mquat.bo.QualityMode.Comparator;
import org.opt4j.start.Constant;

import com.google.inject.Inject;

/**
 * 
 * @author Sebastian Götz
 */
public class MQuATProblem {
	private AntGraph g = new AntGraph();
	private List<SoftwareComponent> components = new ArrayList<SoftwareComponent>();
	private List<Container> containers = new ArrayList<Container>();

	@Inject
	public MQuATProblem(@Constant(value = "numTypes") int numTypes,
			@Constant(value = "numImplsPerType") int numImplsPerType,
			@Constant(value = "numModesPerImpl") int numModesPerImpl,
			@Constant(value = "numContainers") int numContainers) {

		for (int i = 0; i < numTypes; i++) {
			SoftwareComponent sc = new SoftwareComponent(g, "SC_" + i, 0);
			for (int j = 0; j < numImplsPerType; j++) {
				Implementation impl = sc.addImplementation("impl_" + i + "."
						+ j);
				for(int k = 0; k < numModesPerImpl; k++) {
					QualityMode qm = impl.addQualityMode("QM_"+i+"."+j+"."+k, 100);
					double x = Math.random();
					double y = x + Math.random();
					qm.addHWRequirementClause("CPU", "cpu_time", x*50 , Comparator.GET);
					qm.addHWRequirementClause("Server", "response_time", y*100 , Comparator.LET);
				}
			}
			components.add(sc);
		}
		
		for (int i = 0; i < numContainers; i++) {
			Container c = new Container(g, "server_" + i, 0);
			containers.add(c);
		}
	}

	public List<SoftwareComponent> getComponents() {
		return components;
	}

	public List<Container> getContainers() {
		return containers;
	}
}
