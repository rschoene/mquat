/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j.old;

import java.util.Map;

import org.haec.optimizer.aco.mquat.bo.Container;
import org.haec.optimizer.aco.mquat.bo.QualityMode;
import org.haec.optimizer.aco.mquat.bo.Requirement;
import org.opt4j.core.Objective;
import org.opt4j.core.Objective.Sign;
import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;
import org.opt4j.core.problem.PhenotypeWrapper;

/**
 * 
 * @author Sebastian Götz
 */
public class MQuATEvaluator implements Evaluator<PhenotypeWrapper<Map<QualityMode,Container>>> {

	@Override
	public Objectives evaluate(
			PhenotypeWrapper<Map<QualityMode, Container>> phenotype) {
		
		Objectives objs = new Objectives();
		Objective energy = new Objective("Energy", Sign.MIN);
		Objective perf = new Objective("Performance", Sign.MAX);
		Objective relia = new Objective("Reliability", Sign.MAX);
		
		Map<QualityMode, Container> solution = phenotype.get();
		for(QualityMode m : solution.keySet()) {
			for(Requirement r : m.getHWRequirements()) {
				for(String prop : r.getPropertyRequirements().keySet()) {
					double val = r.getPropertyRequirements().get(prop).keySet().iterator().next();
					if(prop.equals("response_time")) {
						objs.add(perf, val);
					} else if(prop.equals("cpu_time")) {
						//TODO use Simulator to compute energy
						objs.add(energy, val);
					}
				}
			}
		}
		
		//TODO
		objs.add(relia, Math.random());
		
		return objs;
	}

}
