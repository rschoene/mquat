/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j.old;

import org.opt4j.core.problem.ProblemModule;
import org.opt4j.start.Constant;

/**
 * 
 * @author Sebastian Götz
 */
public class MQuATModule extends ProblemModule {
	
	@Constant(value = "numTypes")
	protected int numTypes = 10;

	public int getNumTypes() {
		return numTypes;
	}

	public void setNumTypes(int numTypes) {
		this.numTypes = numTypes;
	}
	
	@Constant(value = "numImplsPerType")
	protected int numImplsPerType = 10;

	public int getNumImplsPerType() {
		return numImplsPerType;
	}

	public void setNumImplsPerType(int numImplsPerType) {
		this.numImplsPerType = numImplsPerType;
	}
	
	@Constant(value = "numContainers")
	protected int numContainers = 10;
	
	public int getNumContainers() {
		return numContainers;
	}

	public void setNumContainers(int numContainers) {
		this.numContainers = numContainers;
	}
	
	@Constant(value = "numModesPerImpl")
	protected int numModesPerImpl = 10;

	public int getNumModesPerImpl() {
		return numModesPerImpl;
	}

	public void setNumModesPerImpl(int numModesPerImpl) {
		this.numModesPerImpl = numModesPerImpl;
	}

	@Override
	protected void config() {
		bindProblem(MQuATCreator.class, MQuATDecoder.class, MQuATEvaluator.class);
		
	}

}
