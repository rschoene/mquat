/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.opt4j;

import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.opt4j.core.Objective;
import org.opt4j.core.Objective.Sign;
import org.opt4j.core.Objectives;
import org.opt4j.core.problem.Evaluator;
import org.opt4j.core.problem.PhenotypeWrapper;

/**
 * 
 * @author Sebastian Götz
 */
public class MQuATConfigEvaluator implements Evaluator<PhenotypeWrapper<Map<SWContractMode, ContainerProvider>>> {

	@Override
	public Objectives evaluate(
			PhenotypeWrapper<Map<SWContractMode, ContainerProvider>> pheno) {
		
		Objective response = new Objective("response", Sign.MIN);
		Objectives objectives = new Objectives();
		
		Map<SWContractMode, ContainerProvider> config = pheno.get();
		
		for(SWContractMode implMode : config.keySet()) {
			for(SWContractClause cl : implMode.getClauses()) {
				if(cl instanceof HWComponentRequirementClause) {
					
				} else if (cl instanceof SWComponentRequirementClause) {
				
				} else if (cl instanceof ProvisionClause) {
					//search for response_time TODO generalize lateron
					if(((ProvisionClause)cl).getProvidedProperty().getDeclaredVariable().getName().equals("response_time")) {
						CcmReal v = (CcmReal)((ProvisionClause)cl).getProvidedProperty().getDeclaredVariable().getValue();
						double provision = v.getRealValue();
						objectives.add(response, provision);
					}
				}
			}
		}
		
		return null;
	}
	
}
