/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.transcoder.handbrake;

import java.io.File;
import java.util.List;

import org.haec.app.transcoder.AbstractTranscoder;
import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.apps.util.ErrorState;
import org.haec.handbrake.HandbrakeUtil;
import org.haec.theatre.api.Benchmark;

/**
 * Implementation of org.haec.app.transcoder.handbrake.TranscoderHandbrake
 * @author René Schöne
 */
public class TranscoderHandbrake extends AbstractTranscoder implements TranscoderTranscode {

	public TranscoderHandbrake() {
		super(HandbrakeUtil.sharedInstance);
	}

	@Override
	protected int handleProcess(Process startedProcess)
			throws InterruptedException {
		return util.waitForever(startedProcess, true);
	}

	@Override
	protected void addArguments(List<String> params, File fileVideo,
			String outputVideoCodec, String outputAudioCodec, File out, ErrorState state) {
		params.add("-i");
		params.add(fileVideo.getAbsolutePath());
		params.add("-o");
		params.add(out.getAbsolutePath());
		if(outputVideoCodec != null) {
			outputVideoCodec = handleSpecialVideoCodec(params, outputVideoCodec);
			params.add("--encoder");
			params.add(outputVideoCodec);
		}
		if(outputAudioCodec != null) {
			params.add("--aencoder");
			params.add(outputAudioCodec);
		}
		addOtherArguments(params);
	}
	
	private String handleSpecialVideoCodec(List<String> params,
			String outputVideoCodec) {
		switch(outputVideoCodec) {
		case "theora":
			// set output format (autodetected av_mp4 does not work with theora)
			params.add("-f");
			params.add("av_mkv");
			break;
		case "h264":
			return "x264";
		}
		return outputVideoCodec;
	}

	private void addOtherArguments(List<String> params) {
		if(util.shouldEncodeNoSound()) {
			params.add("-a");
			params.add("none");
		}
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.transcoder.handbrake.TranscoderHandbrake";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new TranscoderHandbrakeBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.transcoder.handbrake";
	}

}
