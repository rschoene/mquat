/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.Direction;
import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.PropertyKind;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.structure.StructureFactory;
import org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl;
import org.coolsoftware.coolcomponents.expressions.ExpressionsFactory;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral;
import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.ecl.resource.ecl.mopp.EclPrinter;
import org.coolsoftware.ecl.resource.ecl.mopp.EclResource;
import org.coolsoftware.requests.Import;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.RequestsFactory;
import org.coolsoftware.requests.RequestsPackage;
import org.coolsoftware.requests.resource.request.mopp.RequestPrinter;
import org.coolsoftware.requests.resource.request.mopp.RequestPrinter2;
import org.coolsoftware.requests.resource.request.mopp.RequestResource;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryRegistryImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.StringUtils;

/**
 * Small application generating problems to profile an optimizer.
 * All problems use a pipe-and-filter system architecture and a standard hardware model.
 * @author Sebastian Götz
 */
public class SystemGenerator {

	private static final String SERVER_STRUCTURE = "Server.structure";

	private int curNumCompTypes = 0;

	private Map<String, EclFile> generatedEclFiles = new HashMap<String, EclFile>();
	private Map<String, List<SWComponentType>> swCompTypeDependencies = new HashMap<String, List<SWComponentType>>();
	private List<SWComponentType> generatedTypes = new ArrayList<SWComponentType>();
	private SWComponentType first;

	// servername, resourcename, propertyname, value
	private Map<String, Map<String, Map<String, Long>>> hwProvision = new HashMap<String, Map<String, Map<String, Long>>>();
	// providing contract name, propertyname, value
	private Map<String, Map<String, Long>> requestedPropertiesByMode = new HashMap<String, Map<String, Long>>();

	private StructuralModel hwStructModel = null;

	private URI baseLocation = null;
	
	private List<String> createdFiles = new ArrayList<String>();

	static int toInt(String[] s, int index) { return Integer.parseInt(s[index]); }

	public static void main(String[] args) throws IOException {
		if(args.length > 0) {
			// assume specs file
			for(String line : StringUtils.tokenize(FileUtils.readAsString(args[0], true), "\n")) {
				if(line.startsWith("==>") || line.isEmpty()) { continue; }
				String[] tokens = line.split(" ");
				int numServers = toInt(tokens, 2);
				int numCompTypes = toInt(tokens, 4);
				int implsPerType = toInt(tokens, 5);
				int modesPerImpl = toInt(tokens, 6);
				SystemGenerator gen = new SystemGenerator();
				gen.generateProblem("Generated", numCompTypes, numCompTypes, 1, 1, implsPerType, modesPerImpl, 1, 1, 1,
						numServers, 1, 0, URI.createFileURI("."), "../project_systems/gen_sys");
			}
		} else {
			SystemGenerator gen = new SystemGenerator();
			gen.generateProblem("Generated", 10, 10, 1, 1, 1, 2, 1, 1, 1,
					9, 1, 0, URI.createFileURI("."), "../gen_sys_manual/");
		}
	}
	
	public static void generateProblem(String name, int numCompTypes,
			int typeDepth, int typeWidth, int propsPerSWType, int implsPerType,
			int modesPerImpl, int hwReqPerMode, int nfpsPerSWDep,
			int provNFPsPerMode, int numServers, int numResPerServer,
			int numNFPsPerRes, URI location) {
		new SystemGenerator().generateProblem(name, numCompTypes, typeDepth, typeWidth,
				propsPerSWType, implsPerType, modesPerImpl, hwReqPerMode, nfpsPerSWDep,
				provNFPsPerMode, numServers, numResPerServer, numNFPsPerRes, location, null);
	}


	public void generateProblem(String name, int numCompTypes,
			int typeDepth, int typeWidth, int propsPerSWType, int implsPerType,
			int modesPerImpl, int hwReqPerMode, int nfpsPerSWDep,
			int provNFPsPerMode, int numServers, int numResPerServer,
			int numNFPsPerRes, URI location, String afterTargetDir) {

		generatedEclFiles = new HashMap<String, EclFile>();
		swCompTypeDependencies = new HashMap<String, List<SWComponentType>>();
		generatedTypes = new ArrayList<SWComponentType>();
		curNumCompTypes = 0;
		hwProvision = new HashMap<String, Map<String, Map<String, Long>>>();
		requestedPropertiesByMode = new HashMap<String, Map<String, Long>>();
		hwStructModel = null;
		
		baseLocation = location;
		generatedEclFiles = new HashMap<String, EclFile>();

		// generate SW structure model
		// - int numCompTypes, int typeDepth, int typeWidth, int propsPerSWType
		StructuralModel swStructModel = generateStructuralModel(numCompTypes,
				typeDepth, typeWidth, propsPerSWType, implsPerType,
				modesPerImpl, hwReqPerMode, nfpsPerSWDep, provNFPsPerMode, name);

		Resource swModelResource = persist(swStructModel);

		// generate HW variant model use Server.structure
		// - int numServers, int numResPerServer, int numNFPsPerRes
		VariantModel hwVarModel = generateVariantModel(numServers,
				numResPerServer, numNFPsPerRes);

		Resource hwVarModelResource = persist(hwVarModel);

		// generate QCL contracts
		// - int implsPerType, int modesPerImpl, int hwReqPerMode, int
		// nfpsPerSWDep, int provNFPsPerMode
		// iterate over types and
		generateContracts(typeWidth, implsPerType, modesPerImpl, hwReqPerMode,
				nfpsPerSWDep, provNFPsPerMode, name, swStructModel,
				swModelResource, hwVarModel);

		System.out.println(requestedPropertiesByMode);
		
		if(afterTargetDir != null) {
			// moving files
			File afterTargetDirFile = new File(afterTargetDir, numServers + "_" + numCompTypes + "_" + 
					implsPerType + "_" + modesPerImpl);
			if(!afterTargetDirFile.exists()) { afterTargetDirFile.mkdirs(); }
			for(String file : createdFiles) {
				File targetDest = new File(afterTargetDirFile, new File(file).getName());
				System.out.print("Move " + file + " to " + targetDest);
				boolean success = new File(file).renameTo(targetDest);
				System.out.println(" - " + (success ? "successful" : "failed"));
			}
			// copy SERVER_STRUCTURE
			try {
				Files.copy(new File(SERVER_STRUCTURE).toPath(), new File(afterTargetDirFile, SERVER_STRUCTURE).toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.err.println("Could not copy " + SERVER_STRUCTURE);
				e.printStackTrace();
			}
		}
	}

	private StructuralModel generateStructuralModel(int numCompTypes,
			int typeDepth, int typeWidth, int propsPerSWType, int implsPerType,
			int modesPerImpl, int hwReqPerMode, int nfpsPerSWDep,
			int provNFPsPerMode, String problemName) {

		StructuralModel sm = StructureFactory.eINSTANCE.createStructuralModel();

		SWComponentType root = StructureFactory.eINSTANCE
				.createSWComponentType();
		root.setName("sw_type_root");

		sm.setRoot(root);

		first = StructureFactory.eINSTANCE.createSWComponentType();
		first.setName("FirstSC");
		first = addProperties(first, propsPerSWType);
		first.setEclUri(problemName + "_" + first.getName() + ".ecl");

		root.getSubtypes().add(first);

		root = genSWTypesBreadth(numCompTypes, typeWidth, typeDepth,
				propsPerSWType, 0, first, root, implsPerType, modesPerImpl,
				hwReqPerMode, nfpsPerSWDep, provNFPsPerMode, problemName);

		return sm;
	}

	private void generateContracts(int typeWidth, int implsPerType,
			int modesPerImpl, int hwReqPerMode, int nfpsPerSWDep,
			int provNFPsPerMode, String problemName, StructuralModel sm,
			Resource swModelResource, VariantModel hwVarModel) {

		EclFile f = generateContractsForType(first, typeWidth, implsPerType,
				modesPerImpl, hwReqPerMode, nfpsPerSWDep, provNFPsPerMode,
				problemName, hwVarModel, sm);

		String path = problemName + "_" + first.getName() + ".ecl";
		persist(f, path, swModelResource);

		generateSuitableRequest(f, first, sm, hwVarModel);

		for (SWComponentType tx : generatedTypes) {
			EclFile txf = generateContractsForType(tx, typeWidth, implsPerType,
					modesPerImpl, hwReqPerMode, nfpsPerSWDep, provNFPsPerMode,
					problemName, hwVarModel, sm);
			String pathTXF = problemName + "_" + tx.getName() + ".ecl";
			generatedEclFiles.put(pathTXF, txf);
		}

		for (String eclPath : generatedEclFiles.keySet()) {
			EclFile eclFile = generatedEclFiles.get(eclPath);
			persist(eclFile, eclPath, swModelResource);
		}
	}

	private void generateSuitableRequest(EclFile f, SWComponentType t,
			StructuralModel sm, VariantModel vm) {
		Map<Property, Long> minvals = new HashMap<Property, Long>();
		Map<Property, Long> maxvals = new HashMap<Property, Long>();

		for (EclContract c : f.getContracts()) {
			SWComponentContract swc = (SWComponentContract) c;
			for (SWContractMode m : swc.getModes()) {
				for (SWContractClause cl : m.getClauses()) {
					if (cl instanceof ProvisionClause) {
						ProvisionClause src = (ProvisionClause) cl;

						CcmExpressionInterpreter i = new CcmExpressionInterpreter();
						boolean min = src.getMinValue() != null;
						if(min) {
							CcmInteger v = (CcmInteger) i.interpret(src
									.getMinValue());
							long val = v.getIntegerValue();
							Property idx = src.getProvidedProperty();
							if (minvals.get(idx) == null)
								minvals.put(idx, val);
							else if (val < minvals.get(idx))
								minvals.put(idx, val);
						} else {
							CcmInteger v = (CcmInteger) i.interpret(src
									.getMaxValue());
							long val = v.getIntegerValue();
							Property idx = src.getProvidedProperty();
							if (maxvals.get(idx) == null)
								maxvals.put(idx, val);
							else if (val > maxvals.get(idx))
								maxvals.put(idx, val);
							
						}

					}
				}
			}
		}

		Request req = RequestsFactory.eINSTANCE.createRequest();
		req.setComponent(t);
		req.setTarget(t.getPorttypes().iterator().next());
		Import im = RequestsFactory.eINSTANCE.createImport();
		im.setModel(sm);
		req.setImport(im);
		Platform p = RequestsFactory.eINSTANCE.createPlatform();
		p.setHwmodel(vm);
		req.setHardware(p);

		for (Property prop : minvals.keySet()) {
			PropertyRequirementClause prc = EclFactory.eINSTANCE
					.createPropertyRequirementClause();
			prc.setRequiredProperty(prop);
			RealLiteralExpression stmt = LiteralsFactory.eINSTANCE
					.createRealLiteralExpression();
			stmt.setValue(minvals.get(prop));
			prc.setMinValue(stmt);
			req.getReqs().add(prc);
		}
		for (Property prop : maxvals.keySet()) {
			PropertyRequirementClause prc = EclFactory.eINSTANCE
					.createPropertyRequirementClause();
			prc.setRequiredProperty(prop);
			RealLiteralExpression stmt = LiteralsFactory.eINSTANCE
					.createRealLiteralExpression();
			stmt.setValue(maxvals.get(prop));
			prc.setMaxValue(stmt);
			req.getReqs().add(prc);
		}

		persist(req, "generated.request");
	}

	private VariantModel generateVariantModel(int numServers,
			int numResPerServer, int numNFPsPerRes) {
		// load Server.structure
		try {
			URI uri = URI.createFileURI(SERVER_STRUCTURE);
			URI copied = baseLocation.appendSegment(SERVER_STRUCTURE);
			FileInputStream fis = new FileInputStream(new File(
					uri.toFileString()));

			ResourceSet rs = createResourceSet();
			Resource res = rs.createResource(uri);
			Resource copiedRes = rs.createResource(copied);
			res.load(fis, null);

			if (res.getContents() != null && res.getContents().size() > 0) {
				hwStructModel = (StructuralModel) res.getContents().get(0);
				copiedRes.getContents().add(hwStructModel);
				copiedRes.save(null);

				List<ResourceType> availableResTypes = new ArrayList<ResourceType>();
				ResourceType serverType = null;
				for (ResourceType rt : ((ResourceType) hwStructModel.getRoot())
						.getSubtypes()) {
					if (rt.getName().equals("Server"))
						serverType = rt;
				}

				if (serverType == null)
					return null;
				for (ResourceType rt : serverType.getSubtypes()) {
					availableResTypes.add(rt);
				}

				// now generated the variant model
				VariantModel vm = VariantFactory.eINSTANCE.createVariantModel();
				org.coolsoftware.coolcomponents.ccm.variant.Resource root = VariantFactory.eINSTANCE
						.createResource();
				root.setName("root");
				vm.setRoot(root);

				for (int s = 0; s < numServers; s++) {
					ContainerProvider serverX = VariantFactory.eINSTANCE
							.createContainerProvider();
					serverX.setName("Server" + s);
					serverX.setSpecification(serverType);

					Map<String, Map<String, Long>> resourceProvision = new HashMap<String, Map<String, Long>>();

					root.getSubresources().add(serverX);
					for (int r = 0; r < numResPerServer; r++) {
						org.coolsoftware.coolcomponents.ccm.variant.Resource srvRes = VariantFactory.eINSTANCE
								.createResource();
						srvRes.setName(serverX.getName() + "." + r);
						int index = r % availableResTypes.size();
						ResourceType curResType = availableResTypes.get(index);
						srvRes.setSpecification(curResType);
						serverX.getSubresources().add(srvRes);

						Map<String, Long> propertyProvision = new HashMap<String, Long>();

						for (Property p : curResType.getProperties()) {
							VariantPropertyBinding vpb = VariantFactory.eINSTANCE
									.createVariantPropertyBinding();
							vpb.setProperty(p);
							IntegerLiteralExpression ile = LiteralsFactory.eINSTANCE
									.createIntegerLiteralExpression();
							ile.setValue(getRandomIntBetween(1, 100));
							vpb.setValueExpression(ile);

							VariableCallExpression vce = VariablesFactory.eINSTANCE
									.createVariableCallExpression();
							vce.setReferredVariable(p.getDeclaredVariable());
							vpb.setReferredVariable(vce);

							srvRes.getPropertyBinding().add(vpb);

							propertyProvision.put(p.getDeclaredVariable()
									.getName(), ile.getValue());
						}

						resourceProvision.put(curResType.getName(),
								propertyProvision);
					}
					hwProvision.put(serverX.getName(), resourceProvision);
				}

				return vm;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private EclFile generateContractsForType(SWComponentType swct,
			int typeWidth, int implsPerType, int modesPerImpl,
			int hwReqPerMode, int nfpsPerSWDep, int provNFPsPerMode,
			String problemName, VariantModel hwVarModel,
			StructuralModel swStructModel) {

		EclFile f = EclFactory.eINSTANCE.createEclFile();

		CcmImport imp = EclFactory.eINSTANCE.createCcmImport();
		imp.setCcmStructuralModel(swStructModel);
		f.getImports().add(imp);
		if (hwStructModel != null) {
			CcmImport imp2 = EclFactory.eINSTANCE.createCcmImport();
			imp2.setCcmStructuralModel(hwStructModel);
			f.getImports().add(imp2);
		}
		// TODO extract as parameter
		int validContractModes = 1;

		for (int c = 0; c < implsPerType; c++) {

			SWComponentContract contract = EclFactory.eINSTANCE
					.createSWComponentContract();
			contract.setComponentType(swct);
			contract.setName(swct.getName() + "_Impl" + c);
			contract.setPort(swct.getPorttypes().get(0)); // TODO choose a
															// random porttype

			f.getContracts().add(contract);

			for (int m = 0; m < modesPerImpl; m++) {

				boolean validMode = false;
				if (validContractModes > 0) {
					validMode = true;
					validContractModes--;
				}

				SWContractMode mode = EclFactory.eINSTANCE
						.createSWContractMode();
				mode.setName(contract.getName() + "_mode" + m);

				System.out.println("valid [" + (validMode ? "x" : "-") + "] "
						+ mode.getName());

				contract.getModes().add(mode);

				// SW deps
				// count defined by width/breadth
				List<SWComponentType> reqSwCompTypes = swCompTypeDependencies
						.get(swct.getName());
				if (reqSwCompTypes != null) // that's the case for ends of
											// chains
					for (int w = 0; w < typeWidth; w++) {
						SWComponentType reqType = reqSwCompTypes.get(w);

						SWComponentRequirementClause scrc = EclFactory.eINSTANCE
								.createSWComponentRequirementClause();
						scrc.setRequiredComponentType(reqType);

						Map<String, Long> reqProps = new HashMap<String, Long>();

						// property requirements scrc.getRequiredProperties();
						int numReqs = 0;
						for (Property prop : reqType.getProperties()) {
							if (numReqs < nfpsPerSWDep) {
								PropertyRequirementClause prc = EclFactory.eINSTANCE
										.createPropertyRequirementClause();
								prc.setRequiredProperty(prop);
								IntegerLiteralExpression ile = LiteralsFactory.eINSTANCE
										.createIntegerLiteralExpression();
								ile.setValue(getRandomIntBetween(1, 100));
								if (prop.getValOrder().equals(Order.INCREASING)) {
									prc.setMinValue(ile);
								} else {
									prc.setMaxValue(ile);
								}
								scrc.getRequiredProperties().add(prc);

								if (validMode)
									reqProps.put(prop.getDeclaredVariable()
											.getName(), ile.getValue());
								numReqs++;
							} else
								break;
						}
						// TODO merge if multiple source modes require something
						// from reqType..
						if (validMode)
							requestedPropertiesByMode.put(reqType.getName(),
									reqProps);

						mode.getClauses().add(scrc);
					}

				// HW dependencies
				// use hwVarModel
				int h = 0;
				// fetch resource types
				for (ResourceType rt : ((ResourceType) hwStructModel.getRoot())
						.getSubtypes()) {
					if (rt.getName().equals("Server")) {
						// if this is a valid mode, ensure that it does not
						// require more, than provided by the hardware
						// -> randomly select a server, which has to fit
						int idx = getRandomIntBetween(0, hwProvision.size() - 1);
						if (validMode)
							System.out.println(" -> valid for Server: "
									+ hwProvision.keySet().toArray()[idx]);
						Map<String, Map<String, Long>> resourceProvision = hwProvision
								.get(hwProvision.keySet().toArray()[idx]);

						for (ResourceType resType : rt.getSubtypes()) {
							if (resType.getProperties() != null
									&& resType.getProperties().size() > 0) {
								Map<String, Long> propertyProvision = resourceProvision
										.get(resType.getName());

								HWComponentRequirementClause hrc = EclFactory.eINSTANCE
										.createHWComponentRequirementClause();
								hrc.setRequiredResourceType(resType);
								for (Property prop : resType.getProperties()) {
									PropertyRequirementClause prc = EclFactory.eINSTANCE
											.createPropertyRequirementClause();
									prc.setRequiredProperty(prop);
									IntegerLiteralExpression exp = LiteralsFactory.eINSTANCE
											.createIntegerLiteralExpression();

									if (validMode) {
										// stay <= hw provision + TODO: already
										// used provision by other components
										long max = propertyProvision.get(prop
												.getDeclaredVariable()
												.getName());
										int use = getRandomIntBetween(1,
												(int) (max * 0.9));
										propertyProvision.put(prop
												.getDeclaredVariable()
												.getName(), max - use);
										resourceProvision.put(
												resType.getName(),
												propertyProvision);
										hwProvision.put(hwProvision.keySet()
												.toArray()[idx].toString(),
												resourceProvision);
										exp.setValue(use);
									} else {
										exp.setValue(getRandomIntBetween(1, 100));
									}
									if (prop.getValOrder().equals(
											Order.DECREASING)) {
										prc.setMaxValue(exp);
									} else {
										prc.setMinValue(exp);
									}
									hrc.getRequiredProperties().add(prc);
									h++;
									if (h >= hwReqPerMode)
										break;
								}
								mode.getClauses().add(hrc);
								if (h == hwReqPerMode)
									break;
							}
						}
						if (h == hwReqPerMode)
							break;
					}
				}

				// Provisions based on properties of swct
				// if this is a valid mode, adhere to potentially required
				// properties by other modes (requestedPropertiesByMode)
				int nAvailableProperties = swct.getProperties().size();
				for (int pp = 0; pp < provNFPsPerMode
						&& pp < nAvailableProperties; pp++) {
					Property property = swct.getProperties().get(pp);

					ProvisionClause provClause = EclFactory.eINSTANCE
							.createProvisionClause();
					provClause.setProvidedProperty(property);
					IntegerLiteralExpression exp = LiteralsFactory.eINSTANCE
							.createIntegerLiteralExpression();
					if (validMode) {
						Map<String, Long> reqProps = requestedPropertiesByMode
								.get(swct.getName());
						long min;
						if (reqProps != null) // reqProps is null if no other
												// mode depends on this one
							min = reqProps.get(property.getDeclaredVariable()
									.getName());
						else
							min = 0;
						exp.setValue(getRandomIntBetween(min + 1, 100));
					} else {
						exp.setValue(getRandomIntBetween(1, 100));
					}

					if (property.getValOrder().equals(Order.DECREASING)) {
						provClause.setMaxValue(exp);
					} else {
						provClause.setMinValue(exp);
					}
					mode.getClauses().add(provClause);
				}
			}
		}

		String path = problemName + "_" + swct.getName() + ".ecl";
		swct.setEclUri(path);

		return f;
	}

	private int getRandomIntBetween(long i, long j) {
		assert (j > i);
		int ret = (int) (Math.random() * (j - i));
		ret += i;
		return ret;
	}

	private SWComponentType genSWTypesBreadth(int numCompTypes,
			int typeWidth, int typeDepth, int propsPerSWType, int curDepth,
			SWComponentType parent, SWComponentType root, int implsPerType,
			int modesPerImpl, int hwReqPerMode, int nfpsPerSWDep,
			int provNFPsPerMode, String problemName) {
		curDepth++;
		for (int w = 0; w < typeWidth; w++) {

			if (curNumCompTypes == numCompTypes)
				break;

			// add provided port to parent
			PortType ppt = StructureFactory.eINSTANCE.createPortType();
			ppt.setDirection(Direction.PROVIDED);
			ppt.setName("provSC" + curNumCompTypes);

			Parameter mp = StructureFactory.eINSTANCE.createParameter();
			mp.setName("meta" + curNumCompTypes);
			ppt.getMetaparameter().add(mp);

			parent.getPorttypes().add(ppt);

			// create new component type
			SWComponentType tx = StructureFactory.eINSTANCE
					.createSWComponentType();
			tx.setName("SC" + curNumCompTypes);
			root.getSubtypes().add(tx);

			tx.setEclUri(problemName + "_" + tx.getName() + ".ecl");

			List<SWComponentType> x = swCompTypeDependencies.get(parent
					.getName());
			if (x == null)
				x = new ArrayList<SWComponentType>();
			x.add(tx);
			swCompTypeDependencies.put(parent.getName(), x);

			// create req port type for new component
			PortType rpt = StructureFactory.eINSTANCE.createPortType();
			rpt.setDirection(Direction.REQUIRED);
			rpt.setName("reqSC" + curNumCompTypes);

			Parameter mpreq = StructureFactory.eINSTANCE.createParameter();
			mpreq.setName("meta" + curNumCompTypes);
			rpt.getMetaparameter().add(mpreq);

			tx.getPorttypes().add(rpt);

			// create connector
			PortConnectorType pct = StructureFactory.eINSTANCE
					.createPortConnectorType();
			pct.setSource(rpt);
			pct.setTarget(ppt);
			parent.getConnectors().add(pct);

			tx = addProperties(tx, propsPerSWType);

			curNumCompTypes++;

			generatedTypes.add(tx);

			if (curDepth < typeDepth)
				tx = genSWTypesBreadth(numCompTypes, typeWidth, typeDepth,
						propsPerSWType, curDepth, tx, root, implsPerType,
						modesPerImpl, hwReqPerMode, nfpsPerSWDep,
						provNFPsPerMode, problemName);
		}
		return parent;
	}

	private SWComponentType addProperties(SWComponentType swct,
			int propsPerSWType) {

		for (int i = 0; i < propsPerSWType; i++) {
			Property px = StructureFactory.eINSTANCE.createProperty();

			Variable v = VariablesFactory.eINSTANCE.createVariable();
			v.setName(swct.getName() + "_v" + i);
			v.setType(TypeLiteral.INTEGER);

			px.setDeclaredVariable(v);
			px.setKind(PropertyKind.RUNTIME);
			px.setValOrder(Order.INCREASING);
//			px.setValOrder(Order.DECREASING); //XXX: this should break the ILP
			swct.getProperties().add(px);
		}

		return swct;
	}

	public ResourceSet createResourceSet() {
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry()
				.getExtensionToFactoryMap()
				.put(ResourceFactoryRegistryImpl.DEFAULT_EXTENSION,
						new EcoreResourceFactoryImpl());
		rs.getPackageRegistry()
				.put("structure", StructurePackageImpl.eINSTANCE);
		rs.getPackageRegistry().put("variant", VariantPackageImpl.eINSTANCE);
		rs.getPackageRegistry().put("behavior", VariantPackageImpl.eINSTANCE);
		rs.getPackageRegistry().put("ecl", EclPackage.eINSTANCE);
		rs.getPackageRegistry().put("request", RequestsPackage.eINSTANCE);
		return rs;
	}

	private Resource persist(VariantModel hwVarModel) {
		try {
			ResourceSet rs = createResourceSet();
			URI uri;
			if (baseLocation != null) {
				uri = baseLocation.appendSegment("GeneratedHW.variant");
			} else {
				uri = URI.createFileURI("GeneratedHW.variant");
			}
			Resource res = rs.createResource(uri);
			res.getContents().add(hwVarModel);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			res.save(out, null);

			System.out.println("generated " + uri.toFileString());

			res.save(null);
			createdFiles.add(uri.toFileString());

			return res;
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	private Resource persist(StructuralModel swStructModel) {
		try {
			ResourceSet rs = createResourceSet();

			URI uri;
			if (baseLocation != null) {
				uri = baseLocation.appendSegment("Generated.structure");
			} else {
				uri = URI.createFileURI("Generated.structure");
			}
			Resource res = rs.createResource(uri);
			res.getContents().add(swStructModel);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			res.save(out, null);

			System.out.println("generated " + uri.toFileString());

			res.save(null);
			createdFiles.add(uri.toFileString());
			return res;
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	private void persist(EclFile f, String path,
			Resource swStructResource) {
		try {
			URI uri;
			if (baseLocation != null) {
				uri = baseLocation.appendSegment(path);
			} else {
				uri = URI.createFileURI(path);
			}

			FileOutputStream fos = new FileOutputStream(uri.toFileString());

			EclResource x = (EclResource) f.eResource();

			EclPrinter printer = new EclPrinter(fos, x);
			printer.print(f);

			System.out.println("generated " + uri.toFileString());
			createdFiles.add(uri.toFileString());

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private void persist(Request req, String path) {
		try {
			URI uri;
			if (baseLocation != null) {
				uri = baseLocation.appendSegment(path);
			} else {
				uri = URI.createFileURI(path);
			}

			FileOutputStream fos = new FileOutputStream(uri.toFileString());

			RequestResource reqRes = (RequestResource) req.eResource();
			RequestPrinter2 printer = new RequestPrinter2(fos, reqRes);
			printer.print(req);

			System.out.println("generated " + uri.toFileString());
			createdFiles.add(uri.toFileString());

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
