/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.variant.Component;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.RequestsFactory;
import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;
import org.eclipse.emf.common.util.URI;

/**
 * Utility class providing methods computing an optimal configuration using holistic search.
 * @author Sebastian Götz
 */
public class ContractChecker {

	/**
	 * Search valid quality modes for a request with demand (Expressions about
	 * Properties
	 * 
	 * @param request
	 * @return
	 */
	public static Set<ContractMode> searchValidModesForRequest(
			Request request, EclFile file) {

		if(request == null) return null;
		
		Map<String,PropertyRequirementClause> innerReq = new HashMap<String, PropertyRequirementClause>();
		for(PropertyRequirementClause rc : request.getReqs()) {
			innerReq.put(rc.getRequiredProperty().getDeclaredVariable().getName(), rc);
		}
		
		Set<ContractMode> foundModes = new HashSet<ContractMode>();

		for (EclContract c : file.getContracts()) {
			if (c instanceof SWComponentContract) {
				SWComponentContract swc = (SWComponentContract) c;
				for (ContractMode mode : swc.getModes()) {
					if (mode instanceof SWContractMode) {
						SWContractMode swcMode = (SWContractMode) mode;

						for (SWContractClause clause : swcMode.getClauses()) {
							if (clause instanceof ProvisionClause) {
								ProvisionClause pc = (ProvisionClause) clause;
								Property prop = pc.getProvidedProperty(); 
								
								if (innerReq.containsKey(prop.getDeclaredVariable().getName())) {
									PropertyRequirementClause rc = innerReq.get(prop.getDeclaredVariable().getName());
									
									CcmExpressionInterpreter interpreter = new CcmExpressionInterpreter();
									CcmValue minValue = null;
									if (pc.getMinValue() != null)
										minValue = interpreter.interpret(pc
												.getMinValue());
									CcmValue maxValue = null;
									if (pc.getMaxValue() != null)
										maxValue = interpreter.interpret(pc
												.getMaxValue());
									interpreter = null;

									
									if (fulfils(rc, minValue, maxValue))
										foundModes.add(mode);
								}
							}
						}
					}
				}
			}
		}
		return foundModes;
	}

	/**
	 * Identifies chains of modes
	 * 
	 * @param request
	 *            user/generated request, which has to be fulfilled
	 * @param file
	 *            the ECL file wherein the needed contract is defined
	 * @return set of valid ContractMode-Sets (i.e. pathes)
	 */
	public static Set<Set<ContractMode>> resolveDependencies(
			Request request, EclFile file,
			Set<ContractMode> originPath, Request origRequest) {

		Set<Set<ContractMode>> ret = new HashSet<Set<ContractMode>>();

		// find valid modes for current request
		Set<ContractMode> modes = searchValidModesForRequest(request, file);
		
		System.out.println(request.getComponent().getName()+" - "+modes);
		
		// if there are multiple modes valid, new paths need to be created
		for (ContractMode curMode : modes) {
			Set<ContractMode> curPath = copy(originPath);
			curPath.add(curMode);

			boolean followed = false;

			if (curMode instanceof SWContractMode) {
				SWContractMode swcMode = (SWContractMode) curMode;

				for (SWContractClause reqClause : swcMode.getClauses()) {
					if (reqClause instanceof SWComponentRequirementClause) {
						SWComponentRequirementClause rc = (SWComponentRequirementClause) reqClause;
						SWComponentType reqType = rc.getRequiredComponentType();
						 
						followed = true;
						
						String eclURI = reqType.getEclUri();
						URI uri = origRequest.eResource().getURI();
						String reqStr = uri.toFileString();
						if(reqStr.indexOf(File.separator) != -1) {
							reqStr = reqStr.substring(0,reqStr.lastIndexOf(File.separator));
						}
						eclURI = reqStr+File.separator+eclURI;
						EclFile eclFile = ResolverUtil.loadEclFile(eclURI);
						
						Request newReq = RequestsFactory.eINSTANCE.createRequest();
						newReq.setComponent(reqType);
						
						for (PropertyRequirementClause prc : rc.getRequiredProperties()) {							
							newReq.getReqs().add(copyPRC(prc));							
						}
						ret.addAll(resolveDependencies(newReq, eclFile, curPath, origRequest));
					}
				}
			}

			if (!followed) {
				ret.add(curPath);
			}
		}

		return ret;
	}
	
	/**Find all mappings of components to containers, which fulfill the resource restriction constraints
	 * 
	 * @param req user request
	 * @return Map mapping quality paths to validity 
	 */
	public static Map<Set<ContractMode>, Boolean> findValidMappings(Request req) {
		if(req == null) {
			System.err.println("Request must not be null!");
		} else {
			String eclURI = req.getComponent().getEclUri();
			URI uri = req.eResource().getURI();
			String reqStr = uri.toFileString();
			if(reqStr.indexOf(File.separator) != -1) {
				reqStr = reqStr.substring(0,reqStr.lastIndexOf(File.separator));
			}
			eclURI = reqStr+File.separator+eclURI;
			EclFile file = ResolverUtil.loadEclFile(eclURI);

			Set<Set<ContractMode>> found = ContractChecker.resolveDependencies(req, file, new HashSet<ContractMode>(), req);

			System.out.println();
			System.out.println("===========================");
			System.out.println();
			
			VariantModel platform = req.getHardware().getHwmodel();
			
			Map<Set<ContractMode>, Boolean> validPathes = new HashMap<Set<ContractMode>, Boolean>();
			
			for(Set<ContractMode> qualitypath : found) {
				Map<ContractMode, Set<Resource>> variant = ContractChecker.findHWvariant(qualitypath, platform);
				if(variant.keySet().size() == qualitypath.size()) {
					validPathes.put(qualitypath, true);
				} else {
					validPathes.put(qualitypath, false);
				}
			}
			
			return validPathes;
		}
		return null;
	}
	
	public static Map<ContractMode, Set<Resource>> findHWvariant(Set<ContractMode> modes, VariantModel platform) {
		CcmExpressionInterpreter interpreter = new CcmExpressionInterpreter();
		System.out.println("Find HW Variant for "+modes);
		Map<ContractMode, Set<Resource>> validResourcesForMode = new HashMap<ContractMode, Set<Resource>>();
		
		//note: each mode belongs to a different sw-component
		for(ContractMode mode : modes) {
			System.out.println(((SWComponentContract)mode.eContainer()).getComponentType().getName()+"."+mode.getName()+":");
			//we want to determine a valid top-level resource (i.e. second level wrt "infrastructure")
			
			//check against platform
			//filter by componenttype
			if(platform.getRoot() instanceof Resource) {
				//this is structure-specific (we assume, that resources, able to be a container, are second level
				Resource infrastructure = (Resource)platform.getRoot();
				for(Resource serverX : infrastructure.getSubresources()) {
					System.out.println("\t=Server: "+serverX.getName());
					boolean allClausesSatisfied = true;
					if(mode instanceof SWContractMode) {
						for(SWContractClause swc : ((SWContractMode)mode).getClauses()) {
							if(swc instanceof HWComponentRequirementClause) {
								for(PropertyRequirementClause prc : ((HWComponentRequirementClause) swc).getRequiredProperties()) {
									//System.out.println("\t   "+((ComponentType)prc.getRequiredProperty().eContainer()).getName()+" with "+prc.getRequiredProperty().getDeclaredVariable().getName()+" min: "+prc.getMinValue()+" max: "+prc.getMaxValue());
									boolean satisfiedByAtLeastOne = false;
									List<Resource> availableCompsOfType = getAllCompsOfType(serverX, ((ComponentType)prc.getRequiredProperty().eContainer()));
									//now check if components' properties fit
									for(Resource curResource : availableCompsOfType) {
										for(VariantPropertyBinding bind : curResource.getPropertyBinding()) {
											
											CcmValue value = interpreter.interpret(bind.getValueExpression());
											if(prc.getRequiredProperty().getDeclaredVariable().getName().equals(bind.getProperty().getDeclaredVariable().getName())) {
												if(fulfils(prc,value,value)) {
													System.out.println("\t>>>    "+curResource.getName()+" ["+value+"] fulfils requirement "+prc.getRequiredProperty().getDeclaredVariable().getName()+" min: "+prc.getMinValue()+" max: "+prc.getMaxValue());
													satisfiedByAtLeastOne = true;
												} else {
													System.out.println("\t>>>    not "+curResource.getName()+" ["+value+"]");
												}
											}
										}
									}
									if(satisfiedByAtLeastOne) {
										System.out.println("\t--- "+serverX.getName()+" fits!");
									} else {
										allClausesSatisfied = false;
										System.out.println("\t--- "+serverX.getName()+" fits NOT!");
									}
								}
							}
						}						
					}
					if(allClausesSatisfied) {
						Set<Resource> valids = validResourcesForMode.get(mode);
						if(valids == null) valids = new HashSet<Resource>();
						valids.add(serverX);
						validResourcesForMode.put(mode,valids);
					}
				}
			}
		}
		
		for(ContractMode mode : validResourcesForMode.keySet()) {
			System.out.print(((SWComponentContract)mode.eContainer()).getComponentType().getName()+"."+mode.getName()+": ");
			for(Component c : validResourcesForMode.get(mode)) 
				System.out.print(c.getName()+" ");
			System.out.println();
		}
		
		interpreter = null;
		
		return validResourcesForMode;
	}

	private static List<Resource> getAllCompsOfType(Resource root,
			ComponentType componentType) {
		List<Resource> ret = new ArrayList<Resource>();
		//Component root = platform.getRoot();
		
		if(root.getSpecification().getName().equals(componentType.getName())) ret.add(root);
		
		if(root instanceof Resource) {
			for(Resource subResource : ((Resource) root).getSubresources()) {
				ret.addAll(getAllCompsOfType(subResource, componentType));
			}
		}
		
		return ret;
	}

	private static Set<ContractMode> copy(Set<ContractMode> x) {
		Set<ContractMode> ret = new HashSet<ContractMode>();
		for (ContractMode c : x) {
			ret.add(c);
		}
		return ret;
	}
	
	private static PropertyRequirementClause copyPRC(PropertyRequirementClause in) {
		PropertyRequirementClause out = EclFactory.eINSTANCE.createPropertyRequirementClause();
		
		out.setMaxValue(in.getMaxValue());
		out.setMinValue(in.getMinValue());
		out.setRequiredProperty(in.getRequiredProperty());
		
		return out;
	}
	
	//provided: m1(min 20 fps) m2(min 10 fps, max 19 fps) m3(max 9 fps), requested min 15 fps -> m1,m2
	//provided: m1(max 9s) m2(min 10s, max 14s) m3(min 15s), requested max 15s 
	private static boolean fulfils(PropertyRequirementClause rc, CcmValue minProv, CcmValue maxProv) {
		//val + comparator
		String comparator = "min";
		double val = 0;
		CcmExpressionInterpreter inter = new CcmExpressionInterpreter();
		if(rc.getMaxValue() != null) { 
			comparator = "max";
			val = ((CcmReal)inter.interpret(rc.getMaxValue())).getRealValue();
		} else {
			CcmReal realval = (CcmReal)inter.interpret(rc.getMinValue());
			if(realval instanceof CcmInteger) val = ((CcmInteger)realval).getIntegerValue();
			else val = realval.getRealValue();
		}
		
		//is value inbetween min provided and max provided
		if(comparator.equals("min")) { //min-request-case (e.g. framerate)
			if(maxProv != null) {
				//min requirement
				double max;
				if(maxProv instanceof CcmInteger)
					max = ((CcmInteger)maxProv).getIntegerValue();
				else
					max = ((CcmReal)maxProv).getRealValue();
				return max >= val;
			} else if(minProv != null){
				double min;
				if(minProv instanceof CcmInteger)
					min = ((CcmInteger)minProv).getIntegerValue();
				else
					min = ((CcmReal)minProv).getRealValue();
				return min >= val;
			}
		} else if(comparator.equals("max")) { //max-request-case (e.g. time)
			if(maxProv != null) {
				double max;
				if(maxProv instanceof CcmInteger) 
					max = ((CcmInteger)maxProv).getIntegerValue();
				else 
					max = ((CcmReal)maxProv).getRealValue();
				return max <= val;
			} else if(minProv != null) {
				//could always be more than val (upper bound required)
			}
		}
		return false;
	}
}
