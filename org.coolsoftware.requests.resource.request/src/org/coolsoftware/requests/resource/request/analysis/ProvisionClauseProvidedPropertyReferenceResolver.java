/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.analysis;

import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.ecl.HWComponentContract;
import org.coolsoftware.ecl.SWComponentContract;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

public class ProvisionClauseProvidedPropertyReferenceResolver implements org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property> {
	
	private org.coolsoftware.ecl.resource.ecl.analysis.ProvisionClauseProvidedPropertyReferenceResolver delegate = new org.coolsoftware.ecl.resource.ecl.analysis.ProvisionClauseProvidedPropertyReferenceResolver();
	
	public void resolve(String identifier, org.coolsoftware.ecl.ProvisionClause container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final org.coolsoftware.requests.resource.request.IRequestReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property> result) {
		EObject contract = container.eContainer().eContainer();
		EList<Property> props = null;
		if(contract instanceof SWComponentContract) {
			props = ((SWComponentContract)contract).getComponentType().getProperties();
		} else {
			props = ((HWComponentContract)contract).getComponentType().getProperties();
		}
		
		for(Property prop : props) {
			if(resolveFuzzy) {
				if(prop.getDeclaredVariable().getName().startsWith(identifier)) result.addMapping(identifier, prop);
			} else {
				if(prop.getDeclaredVariable().getName().equals(identifier)) result.addMapping(identifier, prop);
			}
		}
		
	}
	
	public String deResolve(org.coolsoftware.coolcomponents.ccm.structure.Property element, org.coolsoftware.ecl.ProvisionClause container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
