/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.resource.request.analysis.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Component;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.Import;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.resource.request.IRequestReferenceResolveResult;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;

/**
 * Utility class providing convenience methods to load ECL contracts and resolve contained references.
 * @author Sebastian Götz
 */
public class ResolverUtil {
	
	/**
	 * Name of the {@link DataType} {@link Int}.
	 */
	public static final String DATATYPE_INTEGER = "Integer";

	/**
	 * Name of the {@link DataType} {@link datatype.Float}.
	 */
	public static final String DATATYPE_REAL = "Real";

	/**
	 * Name of unknown {@link DataType}s.
	 */
	public static final String DATATYPE_UNKNOWN = "Unknown";

	/**
	 * Postfix of the {@link URL} of a {@link EResource} to be imported into an
	 * ECL contract.
	 */
	public static final String IMPORT_PATH_POSTFIX = "]";

	/**
	 * Prefix of the {@link URL} of a {@link EResource} to be imported into an
	 * ECL contract.
	 */
	public static final String IMPORT_PATH_PREFIX = "[";

	/**
	 * Name of variable to identify the current context (similar to
	 * <code>this</code> in Java).
	 */
	public static final String SELF_VARIABLE_NAME = "self";
	
	private static Map<String, SWComponentType> componentCache = new HashMap<String, SWComponentType>();
	private static Map<String, Map<PortType, ComponentType>> portCache = new HashMap<String, Map<PortType, ComponentType>>();
	
	public static void resolveComponentReference(String identifier, org.coolsoftware.requests.Request container, final IRequestReferenceResolveResult<SWComponentType> result, boolean resolveFuzzy) {
		
		for(String resName : componentCache.keySet()) {
			if(resolveFuzzy) {
				if(resName.startsWith(identifier)) {
					result.addMapping(identifier, componentCache.get(resName));
				}
			} else {
				if(resName.equals(identifier)) {
					result.addMapping(identifier, componentCache.get(resName));
				}
			}
		}
		
	}
	
	public static void resolvePortReference(String identifier, org.coolsoftware.requests.Request container, final IRequestReferenceResolveResult<PortType> result, boolean resolveFuzzy) {
		//fetch selected Component
		container.getImport();
		
		for(PortType pt : container.getComponent().getPorttypes()) {
			if(resolveFuzzy) {
				if(pt.getName().startsWith(identifier)) {
					result.addMapping(identifier, pt);
				}
			} else {
				if(pt.getName().equals(identifier)) {
					result.addMapping(identifier, pt);
				}
			}
		}
		
//		String selectedComponent = container.getComponent().getName();
//		
//		for(String resName : portCache.keySet()) {
//			if(resolveFuzzy) {
//				if(resName.startsWith(identifier)) {
//					Map<PortType, ComponentType> portOfComponent = portCache.get(resName);
//					PortType port = portOfComponent.keySet().iterator().next();
//					if(portOfComponent.get(port).getName().equals(selectedComponent))
//						result.addMapping(identifier, port);
//				}
//			} else {
//				if(resName.equals(identifier)) {
//					Map<PortType, ComponentType> portOfComponent = portCache.get(resName);
//					PortType port = portOfComponent.keySet().iterator().next();
//					if(portOfComponent.get(port).getName().equals(selectedComponent))
//						result.addMapping(identifier, port);
//				}
//			}
//		}
		
	}
	
	public static void resolvePropertyReference(String identifier, org.coolsoftware.ecl.PropertyRequirementClause container, final IRequestReferenceResolveResult<Property> result, boolean resolveFuzzy) {
		
		//fetch selected Component
		SWComponentType selectedComponent = ((Request)container.eContainer()).getComponent();
		
		for(Property prop : selectedComponent.getProperties()) {
			if(resolveFuzzy) {
				if(prop.getDeclaredVariable().getName().startsWith(identifier)) {
						result.addMapping(identifier, prop);
				}
			} else {
				if(prop.getDeclaredVariable().getName().equals(identifier)) {
						result.addMapping(identifier, prop);
				}
			}
		}
		
	}
	
	public static void loadStructuralModel(
			String identifier, EObject container,
			final IRequestReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> result) {
		
		/* Remove leading '[' and tailing ']'. */
		identifier = identifier.substring(IMPORT_PATH_PREFIX.length(),
				identifier.length() - IMPORT_PATH_POSTFIX.length());
		
		/* First try a relative path. */
		String workspaceLocation = ResourcesPlugin.getWorkspace().getRoot()
				.getLocationURI().getPath();

		File ccmFile = new File(workspaceLocation + identifier);

		/* Probably try an absolute path as well. */
		if (!ccmFile.exists())
			ccmFile = new File(identifier);
		// no else.
		URI ccmURI = null;
		if (!ccmFile.exists()) {
			if(identifier.startsWith("./")) {
				URI request = container.eResource().getURI();
				request = request.trimSegments(1);
				String str = request.toString();
				//System.out.println(str);
				if(str.endsWith("/")) str += identifier.substring(2);
				else str += identifier.substring(1);
				//System.out.println(str);
				ccmURI = URI.createURI(str);
			}
		}

		if (ccmFile.exists() || ccmURI != null) {
			/* Try to create an URI. */
			try {
				if(ccmURI == null) ccmURI = URI.createFileURI(ccmFile.getAbsolutePath());
				ResourceSet rs = new ResourceSetImpl();
				Resource resource = rs.getResource(ccmURI, true);

				/* Check if the resource has already been created. */
				if (resource == null) {
					/* We only want to create the resource, not load it. */
					resource = rs.createResource(ccmURI);
					resource.load(null);
				}
				// no else.

				if (resource.getContents().size() > 0) {
					EObject contents = resource.getContents().get(0);
					

					if (contents instanceof StructuralModel) {
						StructuralModel vm = (StructuralModel) contents;
						
						result.addMapping(identifier, vm);
						
						ComponentType ct = vm.getRoot();
						
						if(ct instanceof org.coolsoftware.coolcomponents.ccm.structure.SWComponentType) {
							fillComponentCache((SWComponentType)ct);
						} else {
							result.setErrorMessage("Invalid type of EObject. Expected Resource but was "
									+ vm.getRoot().getClass().getName() + ".");
						}
						
					}

					else {
						result.setErrorMessage("Invalid type of EObject. Expected Resource but was "
								+ contents.eClass().getName() + ".");
					}
				}

				else {
					result.setErrorMessage("The Resource '" + identifier
							+ "' is empty.");
				}
			}

			catch (IllegalArgumentException e) {
				result.setErrorMessage("The URI '" + identifier
						+ "' is invalid.");
			}

			catch (IOException e) {
				result.setErrorMessage("IOException during opening the file '"
						+ identifier + "': "+e.getMessage()+" || "+e.getCause());
			}

		}

		else {
			result.setErrorMessage("The file '" + identifier
					+ "' cannot be found.");
		}
	}
	
	public static void loadVariantModel(
			String identifier, EObject container,
			final IRequestReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.variant.VariantModel> result) {
		
		/* Remove leading '[' and tailing ']'. */
		identifier = identifier.substring(IMPORT_PATH_PREFIX.length(),
				identifier.length() - IMPORT_PATH_POSTFIX.length());

		/* First try a relative path. */
		String workspaceLocation = ResourcesPlugin.getWorkspace().getRoot()
				.getLocationURI().getPath();

		File ccmFile = new File(workspaceLocation + identifier);

		/* Probably try an absolute path as well. */
		if (!ccmFile.exists())
			ccmFile = new File(identifier);
		// no else.
		URI ccmURI = null;
		if (!ccmFile.exists()) {
			if(identifier.startsWith("./")) {
				URI request = container.eResource().getURI();
				request = request.trimSegments(1);
				String str = request.toString();
				//System.out.println(str);
				if(str.endsWith("/")) str += identifier.substring(2);
				else str += identifier.substring(1);
				//System.out.println(str);
				ccmURI = URI.createURI(str);
			}
		}
		if (ccmFile.exists() || ccmURI != null) {
			/* Try to create an URI. */
			try {
				if(ccmURI == null) ccmURI = URI.createFileURI(ccmFile.getAbsolutePath());
				ResourceSet rs = new ResourceSetImpl();
				Resource resource = rs.getResource(ccmURI, true);

				/* Check if the resource has already been created. */
				if (resource == null) {
					/* We only want to create the resource, not load it. */
					resource = rs.createResource(ccmURI);
					resource.load(null);
				}
				// no else.

				if (resource.getContents().size() > 0) {
					EObject contents = resource.getContents().get(0);
					

					if (contents instanceof VariantModel) {
						VariantModel vm = (VariantModel) contents;
						
						result.addMapping(identifier, vm);
						
						Component comp = vm.getRoot();
						
//						if(ct instanceof org.coolsoftware.coolcomponents.ccm.structure.SWComponentType) {
//							fillComponentCache((SWComponentType)ct);
//						} else {
//							result.setErrorMessage("Invalid type of EObject. Expected Resource but was "
//									+ vm.getRoot().getClass().getName() + ".");
//						}
//						
					}

					else {
						result.setErrorMessage("Invalid type of EObject. Expected Resource but was "
								+ contents.eClass().getName() + ".");
					}
				}

				else {
					result.setErrorMessage("The Resource '" + identifier
							+ "' is empty.");
				}
			}

			catch (IllegalArgumentException e) {
				result.setErrorMessage("The URI '" + identifier
						+ "' is invalid.");
			}

			catch (IOException e) {
				result.setErrorMessage("IOException during opening the file '"
						+ identifier + "': "+e.getMessage()+" || "+e.getCause());
			}

		}

		else {
			result.setErrorMessage("The file '" + identifier
					+ "' cannot be found.");
		}
	}
	
	public static EclFile loadEclFile(String eclURI) {
		if(eclURI == null) {
		
		} else {
			// try to use the workspace location
			String workspaceLocation = ResourcesPlugin.getWorkspace().getRoot()
			.getLocationURI().getPath();
			File eclFile = new File(workspaceLocation + "." +eclURI);
			if (!eclFile.exists()) {
				eclFile = new File(eclURI);
			}
			try {
				if (eclFile.exists()) {
					/* Try to create an URI. */
					URI ccmURI = URI.createFileURI(eclFile.getAbsolutePath());
					if(ccmURI.isFile())
						return loadEclFile(ccmURI);
				}

				 else {
					// try to use the Platform URI
					URI ccmUri = URI.createURI(eclURI);
						return loadEclFile(ccmUri);
					
				}
			} catch (IllegalArgumentException e) {
				System.err.println("The URI '" + eclURI + "' is invalid.");
			}
		}
		return null;
	}
	
	public static EclFile loadEclFile(String eclURI, String currentDir) {
		return loadEclFile(currentDir+File.separator+eclURI);
	}
	
	public static EclFile loadEclFile(URI ccmUri){
		/* Get the resource. */		
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(ccmUri, true);
		if(resource != null)
		if (resource.getContents().size() > 0) {
			EObject contents = resource.getContents().get(0);

			if(contents instanceof EclFile) {
				return (EclFile)contents;
			}
		}

		else {
			System.err.println("The Resource '" + ccmUri.toString()
					+ "' is empty.");
		}
		
		return null;
	}
	
	public static Request getSelectRequest(ISelection selection) {
		File ccmFile = getSelectedRequestFile(selection);
		if (ccmFile.exists()) {

			/* Try to create an URI. */
			try {
				URI ccmURI = URI.createFileURI(ccmFile.getAbsolutePath());

				/* Get the resource. */
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource resource = resourceSet.getResource(ccmURI, true);

				if (resource.getContents().size() > 0) {
					EObject contents = resource.getContents().get(0);

					if(contents instanceof Request) {
						return (Request)contents;
					}
				}

				else { System.err.println("The Resource '" + ccmFile + "' is empty."); }
			}

			catch (IllegalArgumentException e) {
				System.err.println("The URI '" + ccmFile + "' is invalid.");
			}
		}
		return null;
	}
	
	// rename to getSelectedFile
	public static File getSelectedRequestFile(ISelection selection) {
		if(selection instanceof TreeSelection) {
			TreeSelection ts = (TreeSelection)selection;
			if(ts.getFirstElement() instanceof org.eclipse.core.internal.resources.File) {
				org.eclipse.core.internal.resources.File f = ( org.eclipse.core.internal.resources.File)ts.getFirstElement();
				
				/* First try a relative path. */
				String workspaceLocation = ResourcesPlugin.getWorkspace().getRoot()
						.getLocationURI().getPath();
				if(!workspaceLocation.endsWith(File.separator)) workspaceLocation += File.separator;

				File ccmFile = new File(workspaceLocation + "." +f.getFullPath());

				/* Probably try an absolute path as well. */
				if (!ccmFile.exists())
					ccmFile = new File(f.getLocation().toOSString());
				// no else.

				if (ccmFile.exists()) {
					return ccmFile;					
				}

				else {
					System.err.println("The file '" + f.getFullPath()
							+ "' cannot be found.");
				}
			}
		}
		return null;
	}

	public static File getSelectedFolder(ISelection selection) {
		if(selection instanceof TreeSelection) {
			TreeSelection ts = (TreeSelection)selection;
			if(ts.getFirstElement() instanceof org.eclipse.core.internal.resources.Folder) {
				org.eclipse.core.internal.resources.Folder folder = (org.eclipse.core.internal.resources.Folder)
						ts.getFirstElement();
				
				/* First try a relative path. */
				String workspaceLocation = ResourcesPlugin.getWorkspace().getRoot()
						.getLocationURI().getPath();
				if(!workspaceLocation.endsWith(File.separator)) workspaceLocation += File.separator;

				File file = new File(workspaceLocation + "." +folder.getFullPath());

				/* Probably try an absolute path as well. */
				if (!file.exists()) { file = new File(folder.getLocation().toOSString()); }
				// no else.

				if (file.exists()) {
					if(!file.isDirectory()) {
						System.err.println("The file " + " is not a directory.");
					}
					return file;
				}

				System.err.println("The file '" + folder.getFullPath()
						+ "' cannot be found.");
			}
		}
		return null;
	}
	
	private static void fillComponentCache(SWComponentType ct) {
		componentCache.put(ct.getName(), ct);
		
		for(PortType pt : ct.getPorttypes()) {
			Map<PortType, ComponentType> p2c = new HashMap<PortType, ComponentType>();
			p2c.put(pt, ct);
			portCache.put(pt.getName(), p2c);
		}
		for(SWComponentType sub : ((SWComponentType)ct).getSubtypes()) {
			fillComponentCache(sub);
		}
		
		
	}
	
}
