/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.analysis;

public class HWComponentRequirementClauseRequiredResourceTypeReferenceResolver implements org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.HWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.ResourceType> {
	
	private org.coolsoftware.ecl.resource.ecl.analysis.HWComponentRequirementClauseRequiredResourceTypeReferenceResolver delegate = new org.coolsoftware.ecl.resource.ecl.analysis.HWComponentRequirementClauseRequiredResourceTypeReferenceResolver();
	
	public void resolve(String identifier, org.coolsoftware.ecl.HWComponentRequirementClause container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final org.coolsoftware.requests.resource.request.IRequestReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType> result) {
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, new org.coolsoftware.ecl.resource.ecl.IEclReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType>() {
			
			public boolean wasResolvedUniquely() {
				return result.wasResolvedUniquely();
			}
			
			public boolean wasResolvedMultiple() {
				return result.wasResolvedMultiple();
			}
			
			public boolean wasResolved() {
				return result.wasResolved();
			}
			
			public void setErrorMessage(String message) {
				result.setErrorMessage(message);
			}
			
			public java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclReferenceMapping<org.coolsoftware.coolcomponents.ccm.structure.ResourceType>> getMappings() {
				throw new UnsupportedOperationException();
			}
			
			public String getErrorMessage() {
				return result.getErrorMessage();
			}
			
			public void addMapping(String identifier, org.eclipse.emf.common.util.URI newIdentifier) {
				result.addMapping(identifier, newIdentifier);
			}
			
			public void addMapping(String identifier, org.eclipse.emf.common.util.URI newIdentifier, String warning) {
				result.addMapping(identifier, newIdentifier, warning);
			}
			
			public void addMapping(String identifier, org.coolsoftware.coolcomponents.ccm.structure.ResourceType target) {
				result.addMapping(identifier, target);
			}
			
			public void addMapping(String identifier, org.coolsoftware.coolcomponents.ccm.structure.ResourceType target, String warning) {
				result.addMapping(identifier, target, warning);
			}
			
			public java.util.Collection<org.coolsoftware.ecl.resource.ecl.IEclQuickFix> getQuickFixes() {
				return java.util.Collections.emptySet();
			}
			
			public void addQuickFix(final org.coolsoftware.ecl.resource.ecl.IEclQuickFix quickFix) {
				result.addQuickFix(new org.coolsoftware.requests.resource.request.IRequestQuickFix() {
					
					public String getImageKey() {
						return quickFix.getImageKey();
					}
					
					public String getDisplayString() {
						return quickFix.getDisplayString();
					}
					
					public java.util.Collection<org.eclipse.emf.ecore.EObject> getContextObjects() {
						return quickFix.getContextObjects();
					}
					
					public String getContextAsString() {
						return quickFix.getContextAsString();
					}
					
					public String apply(String currentText) {
						return quickFix.apply(currentText);
					}
				});
			}
		});
		
	}
	
	public String deResolve(org.coolsoftware.coolcomponents.ccm.structure.ResourceType element, org.coolsoftware.ecl.HWComponentRequirementClause container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
