/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.analysis;

import org.coolsoftware.requests.resource.request.analysis.util.ResolverUtil;

public class PlatformHwmodelReferenceResolver implements org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.requests.Platform, org.coolsoftware.coolcomponents.ccm.variant.VariantModel> {
	
	private org.coolsoftware.requests.resource.request.analysis.RequestDefaultResolverDelegate<org.coolsoftware.requests.Platform, org.coolsoftware.coolcomponents.ccm.variant.VariantModel> delegate = new org.coolsoftware.requests.resource.request.analysis.RequestDefaultResolverDelegate<org.coolsoftware.requests.Platform, org.coolsoftware.coolcomponents.ccm.variant.VariantModel>();
	
	public void resolve(String identifier, org.coolsoftware.requests.Platform container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final org.coolsoftware.requests.resource.request.IRequestReferenceResolveResult<org.coolsoftware.coolcomponents.ccm.variant.VariantModel> result) {
		ResolverUtil.loadVariantModel(identifier, container, result);
	}
	
	public String deResolve(org.coolsoftware.coolcomponents.ccm.variant.VariantModel element, org.coolsoftware.requests.Platform container, org.eclipse.emf.ecore.EReference reference) {
		return "[./"+element.eResource().getURI().lastSegment()+"]";
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
