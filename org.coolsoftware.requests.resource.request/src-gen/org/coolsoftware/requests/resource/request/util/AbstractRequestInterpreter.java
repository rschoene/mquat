/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.util;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractRequestInterpreter<ResultType, ContextType> {
	
	private java.util.Stack<org.eclipse.emf.ecore.EObject> interpretationStack = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	private java.util.List<org.coolsoftware.requests.resource.request.IRequestInterpreterListener> listeners = new java.util.ArrayList<org.coolsoftware.requests.resource.request.IRequestInterpreterListener>();
	private org.eclipse.emf.ecore.EObject nextObjectToInterprete;
	private Object currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		org.eclipse.emf.ecore.EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (java.util.EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(org.eclipse.emf.ecore.EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof org.coolsoftware.ecl.Metaparameter) {
			result = interprete_org_coolsoftware_ecl_Metaparameter((org.coolsoftware.ecl.Metaparameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.FormulaTemplate) {
			result = interprete_org_coolsoftware_ecl_FormulaTemplate((org.coolsoftware.ecl.FormulaTemplate) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.SelfRequirementClause) {
			result = interprete_org_coolsoftware_ecl_SelfRequirementClause((org.coolsoftware.ecl.SelfRequirementClause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.HWComponentRequirementClause) {
			result = interprete_org_coolsoftware_ecl_HWComponentRequirementClause((org.coolsoftware.ecl.HWComponentRequirementClause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.ProvisionClause) {
			result = interprete_org_coolsoftware_ecl_ProvisionClause((org.coolsoftware.ecl.ProvisionClause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.HWContractClause) {
			result = interprete_org_coolsoftware_ecl_HWContractClause((org.coolsoftware.ecl.HWContractClause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.SWContractMode) {
			result = interprete_org_coolsoftware_ecl_SWContractMode((org.coolsoftware.ecl.SWContractMode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.HWContractMode) {
			result = interprete_org_coolsoftware_ecl_HWContractMode((org.coolsoftware.ecl.HWContractMode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.HWComponentContract) {
			result = interprete_org_coolsoftware_ecl_HWComponentContract((org.coolsoftware.ecl.HWComponentContract) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.PropertyRequirementClause) {
			result = interprete_org_coolsoftware_ecl_PropertyRequirementClause((org.coolsoftware.ecl.PropertyRequirementClause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.SWComponentRequirementClause) {
			result = interprete_org_coolsoftware_ecl_SWComponentRequirementClause((org.coolsoftware.ecl.SWComponentRequirementClause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.EclContract) {
			result = interprete_org_coolsoftware_ecl_EclContract((org.coolsoftware.ecl.EclContract) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.SWContractClause) {
			result = interprete_org_coolsoftware_ecl_SWContractClause((org.coolsoftware.ecl.SWContractClause) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.ContractMode) {
			result = interprete_org_coolsoftware_ecl_ContractMode((org.coolsoftware.ecl.ContractMode) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.EclFile) {
			result = interprete_org_coolsoftware_ecl_EclFile((org.coolsoftware.ecl.EclFile) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.SWComponentContract) {
			result = interprete_org_coolsoftware_ecl_SWComponentContract((org.coolsoftware.ecl.SWComponentContract) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.CcmImport) {
			result = interprete_org_coolsoftware_ecl_CcmImport((org.coolsoftware.ecl.CcmImport) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.EclImport) {
			result = interprete_org_coolsoftware_ecl_EclImport((org.coolsoftware.ecl.EclImport) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.ecl.Import) {
			result = interprete_org_coolsoftware_ecl_Import((org.coolsoftware.ecl.Import) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.requests.MetaParamValue) {
			result = interprete_org_coolsoftware_requests_MetaParamValue((org.coolsoftware.requests.MetaParamValue) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.requests.Platform) {
			result = interprete_org_coolsoftware_requests_Platform((org.coolsoftware.requests.Platform) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.requests.Import) {
			result = interprete_org_coolsoftware_requests_Import((org.coolsoftware.requests.Import) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.requests.Request) {
			result = interprete_org_coolsoftware_requests_Request((org.coolsoftware.requests.Request) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_org_coolsoftware_requests_Request(org.coolsoftware.requests.Request request, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_requests_Import(org.coolsoftware.requests.Import _import, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_requests_Platform(org.coolsoftware.requests.Platform platform, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_requests_MetaParamValue(org.coolsoftware.requests.MetaParamValue metaParamValue, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_Import(org.coolsoftware.ecl.Import _import, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_EclImport(org.coolsoftware.ecl.EclImport eclImport, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_CcmImport(org.coolsoftware.ecl.CcmImport ccmImport, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_SWComponentContract(org.coolsoftware.ecl.SWComponentContract sWComponentContract, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_EclFile(org.coolsoftware.ecl.EclFile eclFile, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_ContractMode(org.coolsoftware.ecl.ContractMode contractMode, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_ProvisionClause(org.coolsoftware.ecl.ProvisionClause provisionClause, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_SWContractClause(org.coolsoftware.ecl.SWContractClause sWContractClause, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_EclContract(org.coolsoftware.ecl.EclContract eclContract, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_SWComponentRequirementClause(org.coolsoftware.ecl.SWComponentRequirementClause sWComponentRequirementClause, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_PropertyRequirementClause(org.coolsoftware.ecl.PropertyRequirementClause propertyRequirementClause, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_HWComponentContract(org.coolsoftware.ecl.HWComponentContract hWComponentContract, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_HWContractMode(org.coolsoftware.ecl.HWContractMode hWContractMode, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_SWContractMode(org.coolsoftware.ecl.SWContractMode sWContractMode, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_HWContractClause(org.coolsoftware.ecl.HWContractClause hWContractClause, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_HWComponentRequirementClause(org.coolsoftware.ecl.HWComponentRequirementClause hWComponentRequirementClause, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_SelfRequirementClause(org.coolsoftware.ecl.SelfRequirementClause selfRequirementClause, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_FormulaTemplate(org.coolsoftware.ecl.FormulaTemplate formulaTemplate, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_ecl_Metaparameter(org.coolsoftware.ecl.Metaparameter metaparameter, ContextType context) {
		return null;
	}
	
	private void notifyListeners(org.eclipse.emf.ecore.EObject element) {
		for (org.coolsoftware.requests.resource.request.IRequestInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(org.eclipse.emf.ecore.EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		for (org.eclipse.emf.ecore.EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		java.util.List<org.eclipse.emf.ecore.EObject> reverse = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>(objects.size());
		reverse.addAll(objects);
		java.util.Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(org.eclipse.emf.ecore.EObject root) {
		java.util.List<org.eclipse.emf.ecore.EObject> objects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
		objects.add(root);
		java.util.Iterator<org.eclipse.emf.ecore.EObject> it = root.eAllContents();
		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(org.coolsoftware.requests.resource.request.IRequestInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(org.coolsoftware.requests.resource.request.IRequestInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public org.eclipse.emf.ecore.EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public java.util.Stack<org.eclipse.emf.ecore.EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public Object getCurrentContext() {
		return currentContext;
	}
	
}
