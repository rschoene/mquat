/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.util;

/**
 * Class RequestTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * org.coolsoftware.requests.resource.request.util.RequestResourceUtil.
 */
public class RequestTextResourceUtil {
	
	/**
	 * Use
	 * org.coolsoftware.requests.resource.request.util.RequestResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static org.coolsoftware.requests.resource.request.mopp.RequestResource getResource(org.eclipse.core.resources.IFile file) {
		return new org.coolsoftware.requests.resource.request.util.RequestEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * org.coolsoftware.requests.resource.request.util.RequestResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static org.coolsoftware.requests.resource.request.mopp.RequestResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return org.coolsoftware.requests.resource.request.util.RequestResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * org.coolsoftware.requests.resource.request.util.RequestResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static org.coolsoftware.requests.resource.request.mopp.RequestResource getResource(org.eclipse.emf.common.util.URI uri) {
		return org.coolsoftware.requests.resource.request.util.RequestResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * org.coolsoftware.requests.resource.request.util.RequestResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated	
	public static org.coolsoftware.requests.resource.request.mopp.RequestResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return org.coolsoftware.requests.resource.request.util.RequestResourceUtil.getResource(uri, options);
	}
	
}
