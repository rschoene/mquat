/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.mopp;

public class RequestReferenceResolverSwitch implements org.coolsoftware.requests.resource.request.IRequestReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected org.coolsoftware.requests.resource.request.analysis.RequestComponentReferenceResolver requestComponentReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.RequestComponentReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.RequestTargetReferenceResolver requestTargetReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.RequestTargetReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.MetaParamValueMetaparamReferenceResolver metaParamValueMetaparamReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.MetaParamValueMetaparamReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.ImportModelReferenceResolver importModelReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.ImportModelReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.PlatformHwmodelReferenceResolver platformHwmodelReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.PlatformHwmodelReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.CcmImportCcmStructuralModelReferenceResolver ccmImportCcmStructuralModelReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.CcmImportCcmStructuralModelReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.SWComponentContractComponentTypeReferenceResolver sWComponentContractComponentTypeReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.SWComponentContractComponentTypeReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.SWComponentContractPortReferenceResolver sWComponentContractPortReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.SWComponentContractPortReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.HWComponentContractComponentTypeReferenceResolver hWComponentContractComponentTypeReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.HWComponentContractComponentTypeReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.ProvisionClauseProvidedPropertyReferenceResolver provisionClauseProvidedPropertyReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.ProvisionClauseProvidedPropertyReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.SWComponentRequirementClauseRequiredComponentTypeReferenceResolver sWComponentRequirementClauseRequiredComponentTypeReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.SWComponentRequirementClauseRequiredComponentTypeReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.HWComponentRequirementClauseRequiredResourceTypeReferenceResolver hWComponentRequirementClauseRequiredResourceTypeReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.HWComponentRequirementClauseRequiredResourceTypeReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.PropertyRequirementClauseRequiredPropertyReferenceResolver propertyRequirementClauseRequiredPropertyReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.PropertyRequirementClauseRequiredPropertyReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.FormulaTemplateMetaparameterReferenceResolver formulaTemplateMetaparameterReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.FormulaTemplateMetaparameterReferenceResolver();
	protected org.coolsoftware.requests.resource.request.analysis.VariableCallExpressionReferredVariableReferenceResolver variableCallExpressionReferredVariableReferenceResolver = new org.coolsoftware.requests.resource.request.analysis.VariableCallExpressionReferredVariableReferenceResolver();
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.requests.Request, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> getRequestComponentReferenceResolver() {
		return getResolverChain(org.coolsoftware.requests.RequestsPackage.eINSTANCE.getRequest_Component(), requestComponentReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.requests.Request, org.coolsoftware.coolcomponents.ccm.structure.PortType> getRequestTargetReferenceResolver() {
		return getResolverChain(org.coolsoftware.requests.RequestsPackage.eINSTANCE.getRequest_Target(), requestTargetReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.requests.MetaParamValue, org.coolsoftware.coolcomponents.ccm.structure.Parameter> getMetaParamValueMetaparamReferenceResolver() {
		return getResolverChain(org.coolsoftware.requests.RequestsPackage.eINSTANCE.getMetaParamValue_Metaparam(), metaParamValueMetaparamReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.requests.Import, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> getImportModelReferenceResolver() {
		return getResolverChain(org.coolsoftware.requests.RequestsPackage.eINSTANCE.getImport_Model(), importModelReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.requests.Platform, org.coolsoftware.coolcomponents.ccm.variant.VariantModel> getPlatformHwmodelReferenceResolver() {
		return getResolverChain(org.coolsoftware.requests.RequestsPackage.eINSTANCE.getPlatform_Hwmodel(), platformHwmodelReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.CcmImport, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> getCcmImportCcmStructuralModelReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getCcmImport_CcmStructuralModel(), ccmImportCcmStructuralModelReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> getSWComponentContractComponentTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_ComponentType(), sWComponentContractComponentTypeReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType> getSWComponentContractPortReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_Port(), sWComponentContractPortReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.HWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.ResourceType> getHWComponentContractComponentTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract_ComponentType(), hWComponentContractComponentTypeReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property> getProvisionClauseProvidedPropertyReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause_ProvidedProperty(), provisionClauseProvidedPropertyReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.SWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> getSWComponentRequirementClauseRequiredComponentTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause_RequiredComponentType(), sWComponentRequirementClauseRequiredComponentTypeReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.HWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.ResourceType> getHWComponentRequirementClauseRequiredResourceTypeReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause_RequiredResourceType(), hWComponentRequirementClauseRequiredResourceTypeReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.PropertyRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.Property> getPropertyRequirementClauseRequiredPropertyReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause_RequiredProperty(), propertyRequirementClauseRequiredPropertyReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter> getFormulaTemplateMetaparameterReferenceResolver() {
		return getResolverChain(org.coolsoftware.ecl.EclPackage.eINSTANCE.getFormulaTemplate_Metaparameter(), formulaTemplateMetaparameterReferenceResolver);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable> getVariableCallExpressionReferredVariableReferenceResolver() {
		return getResolverChain(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression_ReferredVariable(), variableCallExpressionReferredVariableReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		requestComponentReferenceResolver.setOptions(options);
		requestTargetReferenceResolver.setOptions(options);
		metaParamValueMetaparamReferenceResolver.setOptions(options);
		importModelReferenceResolver.setOptions(options);
		platformHwmodelReferenceResolver.setOptions(options);
		ccmImportCcmStructuralModelReferenceResolver.setOptions(options);
		sWComponentContractComponentTypeReferenceResolver.setOptions(options);
		sWComponentContractPortReferenceResolver.setOptions(options);
		hWComponentContractComponentTypeReferenceResolver.setOptions(options);
		provisionClauseProvidedPropertyReferenceResolver.setOptions(options);
		sWComponentRequirementClauseRequiredComponentTypeReferenceResolver.setOptions(options);
		hWComponentRequirementClauseRequiredResourceTypeReferenceResolver.setOptions(options);
		propertyRequirementClauseRequiredPropertyReferenceResolver.setOptions(options);
		formulaTemplateMetaparameterReferenceResolver.setOptions(options);
		variableCallExpressionReferredVariableReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, org.coolsoftware.requests.resource.request.IRequestReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (org.coolsoftware.requests.RequestsPackage.eINSTANCE.getRequest().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("component")) {
				requestComponentReferenceResolver.resolve(identifier, (org.coolsoftware.requests.Request) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.requests.RequestsPackage.eINSTANCE.getRequest().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.PortType> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.PortType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("target")) {
				requestTargetReferenceResolver.resolve(identifier, (org.coolsoftware.requests.Request) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.requests.RequestsPackage.eINSTANCE.getMetaParamValue().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Parameter> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Parameter>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("metaparam")) {
				metaParamValueMetaparamReferenceResolver.resolve(identifier, (org.coolsoftware.requests.MetaParamValue) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.requests.RequestsPackage.eINSTANCE.getImport().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("model")) {
				importModelReferenceResolver.resolve(identifier, (org.coolsoftware.requests.Import) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.requests.RequestsPackage.eINSTANCE.getPlatform().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.variant.VariantModel> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.variant.VariantModel>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("hwmodel")) {
				platformHwmodelReferenceResolver.resolve(identifier, (org.coolsoftware.requests.Platform) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getCcmImport().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("ccmStructuralModel")) {
				ccmImportCcmStructuralModelReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.CcmImport) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("componentType")) {
				sWComponentContractComponentTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.SWComponentContract) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.PortType> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.PortType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("port")) {
				sWComponentContractPortReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.SWComponentContract) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("componentType")) {
				hWComponentContractComponentTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.HWComponentContract) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("providedProperty")) {
				provisionClauseProvidedPropertyReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.ProvisionClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("requiredComponentType")) {
				sWComponentRequirementClauseRequiredComponentTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.SWComponentRequirementClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("requiredResourceType")) {
				hWComponentRequirementClauseRequiredResourceTypeReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.HWComponentRequirementClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Property>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("requiredProperty")) {
				propertyRequirementClauseRequiredPropertyReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.PropertyRequirementClause) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.ecl.EclPackage.eINSTANCE.getFormulaTemplate().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Parameter> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.ccm.structure.Parameter>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("metaparameter")) {
				formulaTemplateMetaparameterReferenceResolver.resolve(identifier, (org.coolsoftware.ecl.FormulaTemplate) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression().isInstance(container)) {
			RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.expressions.variables.Variable> frr = new RequestFuzzyResolveResult<org.coolsoftware.coolcomponents.expressions.variables.Variable>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("referredVariable")) {
				variableCallExpressionReferredVariableReferenceResolver.resolve(identifier, (org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == org.coolsoftware.requests.RequestsPackage.eINSTANCE.getRequest_Component()) {
			return getResolverChain(reference, requestComponentReferenceResolver);
		}
		if (reference == org.coolsoftware.requests.RequestsPackage.eINSTANCE.getRequest_Target()) {
			return getResolverChain(reference, requestTargetReferenceResolver);
		}
		if (reference == org.coolsoftware.requests.RequestsPackage.eINSTANCE.getMetaParamValue_Metaparam()) {
			return getResolverChain(reference, metaParamValueMetaparamReferenceResolver);
		}
		if (reference == org.coolsoftware.requests.RequestsPackage.eINSTANCE.getImport_Model()) {
			return getResolverChain(reference, importModelReferenceResolver);
		}
		if (reference == org.coolsoftware.requests.RequestsPackage.eINSTANCE.getPlatform_Hwmodel()) {
			return getResolverChain(reference, platformHwmodelReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getCcmImport_CcmStructuralModel()) {
			return getResolverChain(reference, ccmImportCcmStructuralModelReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_ComponentType()) {
			return getResolverChain(reference, sWComponentContractComponentTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentContract_Port()) {
			return getResolverChain(reference, sWComponentContractPortReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentContract_ComponentType()) {
			return getResolverChain(reference, hWComponentContractComponentTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getProvisionClause_ProvidedProperty()) {
			return getResolverChain(reference, provisionClauseProvidedPropertyReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getSWComponentRequirementClause_RequiredComponentType()) {
			return getResolverChain(reference, sWComponentRequirementClauseRequiredComponentTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getHWComponentRequirementClause_RequiredResourceType()) {
			return getResolverChain(reference, hWComponentRequirementClauseRequiredResourceTypeReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getPropertyRequirementClause_RequiredProperty()) {
			return getResolverChain(reference, propertyRequirementClauseRequiredPropertyReferenceResolver);
		}
		if (reference == org.coolsoftware.ecl.EclPackage.eINSTANCE.getFormulaTemplate_Metaparameter()) {
			return getResolverChain(reference, formulaTemplateMetaparameterReferenceResolver);
		}
		if (reference == org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression_ReferredVariable()) {
			return getResolverChain(reference, variableCallExpressionReferredVariableReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> org.coolsoftware.requests.resource.request.IRequestReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, org.coolsoftware.requests.resource.request.IRequestReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(org.coolsoftware.requests.resource.request.IRequestOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new org.coolsoftware.requests.resource.request.util.RequestRuntimeUtil().logWarning("Found value with invalid type for option " + org.coolsoftware.requests.resource.request.IRequestOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof org.coolsoftware.requests.resource.request.IRequestReferenceResolver) {
			org.coolsoftware.requests.resource.request.IRequestReferenceResolver replacingResolver = (org.coolsoftware.requests.resource.request.IRequestReferenceResolver) resolverValue;
			if (replacingResolver instanceof org.coolsoftware.requests.resource.request.IRequestDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((org.coolsoftware.requests.resource.request.IRequestDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			org.coolsoftware.requests.resource.request.IRequestReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof org.coolsoftware.requests.resource.request.IRequestReferenceCache) {
					org.coolsoftware.requests.resource.request.IRequestReferenceResolver nextResolver = (org.coolsoftware.requests.resource.request.IRequestReferenceResolver) next;
					if (nextResolver instanceof org.coolsoftware.requests.resource.request.IRequestDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((org.coolsoftware.requests.resource.request.IRequestDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new org.coolsoftware.requests.resource.request.util.RequestRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.requests.resource.request.IRequestOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.requests.resource.request.IRequestDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new org.coolsoftware.requests.resource.request.util.RequestRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.requests.resource.request.IRequestOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.requests.resource.request.IRequestDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
