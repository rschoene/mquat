/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.mopp;

public class RequestPrinter2 implements org.coolsoftware.requests.resource.request.IRequestTextPrinter {
	
	protected class PrintToken {
		
		private String text;
		private String tokenName;
		
		public PrintToken(String text, String tokenName) {
			this.text = text;
			this.tokenName = tokenName;
		}
		
		public String getText() {
			return text;
		}
		
		public String getTokenName() {
			return tokenName;
		}
		
		public String toString() {
			return "'" + text + "' [" + tokenName + "]";
		}
		
	}
	
	/**
	 * The PrintCountingMap keeps tracks of the values that must be printed for each
	 * feature of an EObject. It is also used to store the indices of all values that
	 * have been printed. This knowledge is used to avoid printing values twice. We
	 * must store the concrete indices of the printed values instead of basically
	 * counting them, because values may be printed in an order that differs from the
	 * order in which they are stored in the EObject's feature.
	 */
	protected class PrintCountingMap {
		
		private java.util.Map<String, java.util.List<Object>> featureToValuesMap = new java.util.LinkedHashMap<String, java.util.List<Object>>();
		private java.util.Map<String, java.util.Set<Integer>> featureToPrintedIndicesMap = new java.util.LinkedHashMap<String, java.util.Set<Integer>>();
		
		public void setFeatureValues(String featureName, java.util.List<Object> values) {
			featureToValuesMap.put(featureName, values);
			// If the feature does not have values it won't be printed. An entry in
			// 'featureToPrintedIndicesMap' is therefore not needed in this case.
			if (values != null) {
				featureToPrintedIndicesMap.put(featureName, new java.util.LinkedHashSet<Integer>());
			}
		}
		
		public java.util.Set<Integer> getIndicesToPrint(String featureName) {
			return featureToPrintedIndicesMap.get(featureName);
		}
		
		public void addIndexToPrint(String featureName, int index) {
			featureToPrintedIndicesMap.get(featureName).add(index);
		}
		
		public int getCountLeft(org.coolsoftware.requests.resource.request.grammar.RequestTerminal terminal) {
			org.eclipse.emf.ecore.EStructuralFeature feature = terminal.getFeature();
			String featureName = feature.getName();
			java.util.List<Object> totalValuesToPrint = featureToValuesMap.get(featureName);
			java.util.Set<Integer> printedIndices = featureToPrintedIndicesMap.get(featureName);
			if (totalValuesToPrint == null) {
				return 0;
			}
			if (feature instanceof org.eclipse.emf.ecore.EAttribute) {
				// for attributes we do not need to check the type, since the CS languages does
				// not allow type restrictions for attributes.
				return totalValuesToPrint.size() - printedIndices.size();
			} else if (feature instanceof org.eclipse.emf.ecore.EReference) {
				org.eclipse.emf.ecore.EReference reference = (org.eclipse.emf.ecore.EReference) feature;
				if (!reference.isContainment()) {
					// for non-containment references we also do not need to check the type, since the
					// CS languages does not allow type restrictions for these either.
					return totalValuesToPrint.size() - printedIndices.size();
				}
			}
			// now we're left with containment references for which we check the type of the
			// objects to print
			java.util.List<Class<?>> allowedTypes = getAllowedTypes(terminal);
			java.util.Set<Integer> indicesWithCorrectType = new java.util.LinkedHashSet<Integer>();
			int index = 0;
			for (Object valueToPrint : totalValuesToPrint) {
				for (Class<?> allowedType : allowedTypes) {
					if (allowedType.isInstance(valueToPrint)) {
						indicesWithCorrectType.add(index);
					}
				}
				index++;
			}
			indicesWithCorrectType.removeAll(printedIndices);
			return indicesWithCorrectType.size();
		}
		
		public int getNextIndexToPrint(String featureName) {
			int printedValues = featureToPrintedIndicesMap.get(featureName).size();
			return printedValues;
		}
		
	}
	
	public final static String NEW_LINE = java.lang.System.getProperties().getProperty("line.separator");
	
	private final PrintToken SPACE_TOKEN = new PrintToken(" ", null);
	private final PrintToken TAB_TOKEN = new PrintToken("\t", null);
	private final PrintToken NEW_LINE_TOKEN = new PrintToken(NEW_LINE, null);
	
	private final org.coolsoftware.requests.resource.request.util.RequestEClassUtil eClassUtil = new org.coolsoftware.requests.resource.request.util.RequestEClassUtil();
	
	/**
	 * Holds the resource that is associated with this printer. May be null if the
	 * printer is used stand alone.
	 */
	private org.coolsoftware.requests.resource.request.IRequestTextResource resource;
	
	private java.util.Map<?, ?> options;
	private java.io.OutputStream outputStream;
	protected java.util.List<PrintToken> tokenOutputStream;
	private org.coolsoftware.requests.resource.request.IRequestTokenResolverFactory tokenResolverFactory = new org.coolsoftware.requests.resource.request.mopp.RequestTokenResolverFactory();
	private boolean handleTokenSpaceAutomatically = true;
	private int tokenSpace = 1;
	/**
	 * A flag that indicates whether tokens have already been printed for some object.
	 * The flag is set to false whenever printing of an EObject tree is started. The
	 * status of the flag is used to avoid printing default token space in front of
	 * the root object.
	 */
	private boolean startedPrintingObject = false;
	/**
	 * The number of tab characters that were printed before the current line. This
	 * number is used to calculate the relative indentation when printing contained
	 * objects, because all contained objects must start with this indentation
	 * (tabsBeforeCurrentObject + currentTabs).
	 */
	private int currentTabs;
	/**
	 * The number of tab characters that must be printed before the current object.
	 * This number is used to calculate the indentation of new lines, when line breaks
	 * are printed within one object.
	 */
	private int tabsBeforeCurrentObject;
	/**
	 * This flag is used to indicate whether the number of tabs before the current
	 * object has been set for the current object. The flag is needed, because setting
	 * the number of tabs must be performed when the first token of the contained
	 * element is printed.
	 */
	private boolean startedPrintingContainedObject;
	
	public RequestPrinter2(java.io.OutputStream outputStream, org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	public void print(org.eclipse.emf.ecore.EObject element) throws java.io.IOException {
		tokenOutputStream = new java.util.ArrayList<PrintToken>();
		currentTabs = 0;
		tabsBeforeCurrentObject = 0;
		startedPrintingObject = true;
		startedPrintingContainedObject = false;
		java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement>  formattingElements = new java.util.ArrayList<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement>();
		doPrint(element, formattingElements);
		// print all remaining formatting elements
		java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations = getCopyOfLayoutInformation(element);
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation eofLayoutInformation = getLayoutInformation(layoutInformations, null, null, null);
		printFormattingElements(formattingElements, layoutInformations, eofLayoutInformation);
		java.io.PrintWriter writer = new java.io.PrintWriter(new java.io.BufferedOutputStream(outputStream));
		if (handleTokenSpaceAutomatically) {
			printSmart(writer);
		} else {
			printBasic(writer);
		}
		writer.flush();
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (outputStream == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof org.coolsoftware.requests.Request) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.requests.MetaParamValue) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_1, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.requests.Import) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_2, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.requests.Platform) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_3, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.EclFile) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_0, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.CcmImport) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_1, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.SWComponentContract) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.HWComponentContract) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.SWContractMode) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.HWContractMode) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.ProvisionClause) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.SWComponentRequirementClause) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.HWComponentRequirementClause) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.SelfRequirementClause) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_9, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.PropertyRequirementClause) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.FormulaTemplate) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.ecl.Metaparameter) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_12, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.Block) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_1, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_2, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_3, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_4, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_5, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_6, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_7, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_8, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_9, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_10, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_11, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_12, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_13, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_14, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_15, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_16, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_17, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_18, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_19, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_20, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_21, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_22, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_23, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_24, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_25, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_26, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_27, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_28, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_29, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.Variable) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_31, foundFormattingElements);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment) {
			printInternal(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_32, foundFormattingElements);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	public void printInternal(org.eclipse.emf.ecore.EObject eObject, org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement ruleElement, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements) {
		java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations = getCopyOfLayoutInformation(eObject);
		org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator decoratorTree = getDecoratorTree(ruleElement);
		decorateTree(decoratorTree, eObject);
		printTree(decoratorTree, eObject, foundFormattingElements, layoutInformations);
	}
	
	/**
	 * creates a tree of decorator objects which reflects the syntax tree that is
	 * attached to the given syntax element
	 */
	public org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator getDecoratorTree(org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement syntaxElement) {
		org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement[] children = syntaxElement.getChildren();
		int childCount = children.length;
		org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator[] childDecorators = new org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator[childCount];
		for (int i = 0; i < childCount; i++) {
			childDecorators[i] = getDecoratorTree(children[i]);
		}
		org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator decorator = new org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator(syntaxElement, childDecorators);
		return decorator;
	}
	
	public void decorateTree(org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator decorator, org.eclipse.emf.ecore.EObject eObject) {
		PrintCountingMap printCountingMap = initializePrintCountingMap(eObject);
		java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator> keywordsToPrint = new java.util.ArrayList<org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator>();
		decorateTreeBasic(decorator, eObject, printCountingMap, keywordsToPrint);
		for (org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator keywordToPrint : keywordsToPrint) {
			// for keywords the concrete index does not matter, but we must add one to
			// indicate that the keyword needs to be printed here. Thus, we use 0 as index.
			keywordToPrint.addIndexToPrint(0);
		}
	}
	
	/**
	 * Tries to decorate the decorator with an attribute value, or reference held by
	 * the given EObject. Returns true if an attribute value or reference was found.
	 */
	public boolean decorateTreeBasic(org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator decorator, org.eclipse.emf.ecore.EObject eObject, PrintCountingMap printCountingMap, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator> keywordsToPrint) {
		boolean foundFeatureToPrint = false;
		org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement syntaxElement = decorator.getDecoratedElement();
		org.coolsoftware.requests.resource.request.grammar.RequestCardinality cardinality = syntaxElement.getCardinality();
		boolean isFirstIteration = true;
		while (true) {
			java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator> subKeywordsToPrint = new java.util.ArrayList<org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator>();
			boolean keepDecorating = false;
			if (syntaxElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestKeyword) {
				subKeywordsToPrint.add(decorator);
			} else if (syntaxElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestTerminal) {
				org.coolsoftware.requests.resource.request.grammar.RequestTerminal terminal = (org.coolsoftware.requests.resource.request.grammar.RequestTerminal) syntaxElement;
				org.eclipse.emf.ecore.EStructuralFeature feature = terminal.getFeature();
				if (feature == org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ANONYMOUS_FEATURE) {
					return false;
				}
				String featureName = feature.getName();
				int countLeft = printCountingMap.getCountLeft(terminal);
				if (countLeft > terminal.getMandatoryOccurencesAfter()) {
					// normally we print the element at the next index
					int indexToPrint = printCountingMap.getNextIndexToPrint(featureName);
					// But, if there are type restrictions for containments, we must choose an index
					// of an element that fits (i.e., which has the correct type)
					if (terminal instanceof org.coolsoftware.requests.resource.request.grammar.RequestContainment) {
						org.coolsoftware.requests.resource.request.grammar.RequestContainment containment = (org.coolsoftware.requests.resource.request.grammar.RequestContainment) terminal;
						indexToPrint = findElementWithCorrectType(eObject, feature, printCountingMap.getIndicesToPrint(featureName), containment);
					}
					if (indexToPrint >= 0) {
						decorator.addIndexToPrint(indexToPrint);
						printCountingMap.addIndexToPrint(featureName, indexToPrint);
						keepDecorating = true;
					}
				}
			}
			if (syntaxElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestChoice) {
				// for choices we do print only the choice which does print at least one feature
				org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator childToPrint = null;
				for (org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator childDecorator : decorator.getChildDecorators()) {
					// pick first choice as default, will be overridden if a choice that prints a
					// feature is found
					if (childToPrint == null) {
						childToPrint = childDecorator;
					}
					if (doesPrintFeature(childDecorator, eObject, printCountingMap)) {
						childToPrint = childDecorator;
						break;
					}
				}
				keepDecorating |= decorateTreeBasic(childToPrint, eObject, printCountingMap, subKeywordsToPrint);
			} else {
				// for all other syntax element we do print all children
				for (org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator childDecorator : decorator.getChildDecorators()) {
					keepDecorating |= decorateTreeBasic(childDecorator, eObject, printCountingMap, subKeywordsToPrint);
				}
			}
			foundFeatureToPrint |= keepDecorating;
			// only print keywords if a feature was printed or the syntax element is mandatory
			if (cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.ONE) {
				keywordsToPrint.addAll(subKeywordsToPrint);
			} else if (cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.PLUS) {
				if (isFirstIteration) {
					keywordsToPrint.addAll(subKeywordsToPrint);
				} else {
					if (keepDecorating) {
						keywordsToPrint.addAll(subKeywordsToPrint);
					}
				}
			} else if (keepDecorating && (cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.STAR || cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.QUESTIONMARK)) {
				keywordsToPrint.addAll(subKeywordsToPrint);
			}
			if (cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.ONE || cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.QUESTIONMARK) {
				break;
			} else if (!keepDecorating) {
				break;
			}
			isFirstIteration = false;
		}
		return foundFeatureToPrint;
	}
	
	private int findElementWithCorrectType(org.eclipse.emf.ecore.EObject eObject, org.eclipse.emf.ecore.EStructuralFeature feature, java.util.Set<Integer> indicesToPrint, org.coolsoftware.requests.resource.request.grammar.RequestContainment containment) {
		// By default the type restrictions that are defined in the CS definition are
		// considered when printing models. You can change this behavior by setting the
		// 'ignoreTypeRestrictionsForPrinting' option to true.
		boolean ignoreTypeRestrictions = false;
		org.eclipse.emf.ecore.EClass[] allowedTypes = containment.getAllowedTypes();
		Object value = eObject.eGet(feature);
		if (value instanceof java.util.List<?>) {
			java.util.List<?> valueList = (java.util.List<?>) value;
			int listSize = valueList.size();
			for (int index = 0; index < listSize; index++) {
				if (indicesToPrint.contains(index)) {
					continue;
				}
				Object valueAtIndex = valueList.get(index);
				if (eClassUtil.isInstance(valueAtIndex, allowedTypes) || ignoreTypeRestrictions) {
					return index;
				}
			}
		} else {
			if (eClassUtil.isInstance(value, allowedTypes) || ignoreTypeRestrictions) {
				return 0;
			}
		}
		return -1;
	}
	
	/**
	 * Checks whether decorating the given node will use at least one attribute value,
	 * or reference held by eObject. Returns true if a printable attribute value or
	 * reference was found. This method is used to decide which choice to pick, when
	 * multiple choices are available. We pick the choice that prints at least one
	 * attribute or reference.
	 */
	public boolean doesPrintFeature(org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator decorator, org.eclipse.emf.ecore.EObject eObject, PrintCountingMap printCountingMap) {
		org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement syntaxElement = decorator.getDecoratedElement();
		if (syntaxElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestTerminal) {
			org.coolsoftware.requests.resource.request.grammar.RequestTerminal terminal = (org.coolsoftware.requests.resource.request.grammar.RequestTerminal) syntaxElement;
			org.eclipse.emf.ecore.EStructuralFeature feature = terminal.getFeature();
			if (feature == org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ANONYMOUS_FEATURE) {
				return false;
			}
			int countLeft = printCountingMap.getCountLeft(terminal);
			if (countLeft > terminal.getMandatoryOccurencesAfter()) {
				// found a feature to print
				return true;
			}
		}
		for (org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator childDecorator : decorator.getChildDecorators()) {
			if (doesPrintFeature(childDecorator, eObject, printCountingMap)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean printTree(org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator decorator, org.eclipse.emf.ecore.EObject eObject, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement printElement = decorator.getDecoratedElement();
		org.coolsoftware.requests.resource.request.grammar.RequestCardinality cardinality = printElement.getCardinality();
		java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> cloned = new java.util.ArrayList<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement>();
		cloned.addAll(foundFormattingElements);
		boolean foundSomethingAtAll = false;
		boolean foundSomethingToPrint;
		while (true) {
			foundSomethingToPrint = false;
			Integer indexToPrint = decorator.getNextIndexToPrint();
			if (indexToPrint != null) {
				if (printElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestKeyword) {
					printKeyword(eObject, (org.coolsoftware.requests.resource.request.grammar.RequestKeyword) printElement, foundFormattingElements, layoutInformations);
					foundSomethingToPrint = true;
				} else if (printElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestPlaceholder) {
					org.coolsoftware.requests.resource.request.grammar.RequestPlaceholder placeholder = (org.coolsoftware.requests.resource.request.grammar.RequestPlaceholder) printElement;
					printFeature(eObject, placeholder, indexToPrint, foundFormattingElements, layoutInformations);
					foundSomethingToPrint = true;
				} else if (printElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestContainment) {
					org.coolsoftware.requests.resource.request.grammar.RequestContainment containment = (org.coolsoftware.requests.resource.request.grammar.RequestContainment) printElement;
					printContainedObject(eObject, containment, indexToPrint, foundFormattingElements, layoutInformations);
					foundSomethingToPrint = true;
				} else if (printElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestBooleanTerminal) {
					org.coolsoftware.requests.resource.request.grammar.RequestBooleanTerminal booleanTerminal = (org.coolsoftware.requests.resource.request.grammar.RequestBooleanTerminal) printElement;
					printBooleanTerminal(eObject, booleanTerminal, indexToPrint, foundFormattingElements, layoutInformations);
					foundSomethingToPrint = true;
				} else if (printElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestEnumerationTerminal) {
					org.coolsoftware.requests.resource.request.grammar.RequestEnumerationTerminal enumTerminal = (org.coolsoftware.requests.resource.request.grammar.RequestEnumerationTerminal) printElement;
					printEnumerationTerminal(eObject, enumTerminal, indexToPrint, foundFormattingElements, layoutInformations);
					foundSomethingToPrint = true;
				}
			}
			if (foundSomethingToPrint) {
				foundSomethingAtAll = true;
			}
			if (printElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestWhiteSpace) {
				foundFormattingElements.add((org.coolsoftware.requests.resource.request.grammar.RequestWhiteSpace) printElement);
			}
			if (printElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestLineBreak) {
				foundFormattingElements.add((org.coolsoftware.requests.resource.request.grammar.RequestLineBreak) printElement);
			}
			for (org.coolsoftware.requests.resource.request.mopp.RequestSyntaxElementDecorator childDecorator : decorator.getChildDecorators()) {
				foundSomethingToPrint |= printTree(childDecorator, eObject, foundFormattingElements, layoutInformations);
				org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement decoratedElement = decorator.getDecoratedElement();
				if (foundSomethingToPrint && decoratedElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestChoice) {
					break;
				}
			}
			if (cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.ONE || cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.QUESTIONMARK) {
				break;
			} else if (!foundSomethingToPrint) {
				break;
			}
		}
		// only print formatting elements if a feature was printed or the syntax element
		// is mandatory
		if (!foundSomethingAtAll && (cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.STAR || cardinality == org.coolsoftware.requests.resource.request.grammar.RequestCardinality.QUESTIONMARK)) {
			foundFormattingElements.clear();
			foundFormattingElements.addAll(cloned);
		}
		return foundSomethingToPrint;
	}
	
	public void printKeyword(org.eclipse.emf.ecore.EObject eObject, org.coolsoftware.requests.resource.request.grammar.RequestKeyword keyword, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation keywordLayout = getLayoutInformation(layoutInformations, keyword, null, eObject);
		printFormattingElements(foundFormattingElements, layoutInformations, keywordLayout);
		String value = keyword.getValue();
		tokenOutputStream.add(new PrintToken(value, "'" + org.coolsoftware.requests.resource.request.util.RequestStringUtil.escapeToANTLRKeyword(value) + "'"));
	}
	
	public void printFeature(org.eclipse.emf.ecore.EObject eObject, org.coolsoftware.requests.resource.request.grammar.RequestPlaceholder placeholder, int count, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		org.eclipse.emf.ecore.EStructuralFeature feature = placeholder.getFeature();
		if (feature instanceof org.eclipse.emf.ecore.EAttribute) {
			printAttribute(eObject, (org.eclipse.emf.ecore.EAttribute) feature, placeholder, count, foundFormattingElements, layoutInformations);
		} else {
			printReference(eObject, (org.eclipse.emf.ecore.EReference) feature, placeholder, count, foundFormattingElements, layoutInformations);
		}
	}
	
	public void printAttribute(org.eclipse.emf.ecore.EObject eObject, org.eclipse.emf.ecore.EAttribute attribute, org.coolsoftware.requests.resource.request.grammar.RequestPlaceholder placeholder, int index, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		String result = null;
		Object attributeValue = org.coolsoftware.requests.resource.request.util.RequestEObjectUtil.getFeatureValue(eObject, attribute, index);
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation attributeLayout = getLayoutInformation(layoutInformations, placeholder, attributeValue, eObject);
		String visibleTokenText = getVisibleTokenText(attributeLayout);
		// if there is text for the attribute we use it
		if (visibleTokenText != null) {
			result = visibleTokenText;
		}
		
		if (result == null) {
			// if no text is available, the attribute is deresolved to obtain its textual
			// representation
			org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver(placeholder.getTokenName());
			tokenResolver.setOptions(getOptions());
			String deResolvedValue = tokenResolver.deResolve(attributeValue, attribute, eObject);
			result = deResolvedValue;
		}
		
		if (result != null && !"".equals(result)) {
			printFormattingElements(foundFormattingElements, layoutInformations, attributeLayout);
			// write result to the output stream
			tokenOutputStream.add(new PrintToken(result, placeholder.getTokenName()));
		}
	}
	
	
	public void printBooleanTerminal(org.eclipse.emf.ecore.EObject eObject, org.coolsoftware.requests.resource.request.grammar.RequestBooleanTerminal booleanTerminal, int index, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		org.eclipse.emf.ecore.EAttribute attribute = booleanTerminal.getAttribute();
		String result = null;
		Object attributeValue = org.coolsoftware.requests.resource.request.util.RequestEObjectUtil.getFeatureValue(eObject, attribute, index);
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation attributeLayout = getLayoutInformation(layoutInformations, booleanTerminal, attributeValue, eObject);
		String visibleTokenText = getVisibleTokenText(attributeLayout);
		// if there is text for the attribute we use it
		if (visibleTokenText != null) {
			result = visibleTokenText;
		}
		
		if (result == null) {
			// if no text is available, the boolean attribute is converted to its textual
			// representation using the literals of the boolean terminal
			if (Boolean.TRUE.equals(attributeValue)) {
				result = booleanTerminal.getTrueLiteral();
			} else {
				result = booleanTerminal.getFalseLiteral();
			}
		}
		
		if (result != null && !"".equals(result)) {
			printFormattingElements(foundFormattingElements, layoutInformations, attributeLayout);
			// write result to the output stream
			tokenOutputStream.add(new PrintToken(result, "'" + org.coolsoftware.requests.resource.request.util.RequestStringUtil.escapeToANTLRKeyword(result) + "'"));
		}
	}
	
	
	public void printEnumerationTerminal(org.eclipse.emf.ecore.EObject eObject, org.coolsoftware.requests.resource.request.grammar.RequestEnumerationTerminal enumTerminal, int index, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		org.eclipse.emf.ecore.EAttribute attribute = enumTerminal.getAttribute();
		String result = null;
		Object attributeValue = org.coolsoftware.requests.resource.request.util.RequestEObjectUtil.getFeatureValue(eObject, attribute, index);
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation attributeLayout = getLayoutInformation(layoutInformations, enumTerminal, attributeValue, eObject);
		String visibleTokenText = getVisibleTokenText(attributeLayout);
		// if there is text for the attribute we use it
		if (visibleTokenText != null) {
			result = visibleTokenText;
		}
		
		if (result == null) {
			// if no text is available, the enumeration attribute is converted to its textual
			// representation using the literals of the enumeration terminal
			assert attributeValue instanceof org.eclipse.emf.common.util.Enumerator;
			result = enumTerminal.getText(((org.eclipse.emf.common.util.Enumerator) attributeValue).getName());
		}
		
		if (result != null && !"".equals(result)) {
			printFormattingElements(foundFormattingElements, layoutInformations, attributeLayout);
			// write result to the output stream
			tokenOutputStream.add(new PrintToken(result, "'" + org.coolsoftware.requests.resource.request.util.RequestStringUtil.escapeToANTLRKeyword(result) + "'"));
		}
	}
	
	
	public void printContainedObject(org.eclipse.emf.ecore.EObject eObject, org.coolsoftware.requests.resource.request.grammar.RequestContainment containment, int index, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		org.eclipse.emf.ecore.EStructuralFeature reference = containment.getFeature();
		Object o = org.coolsoftware.requests.resource.request.util.RequestEObjectUtil.getFeatureValue(eObject, reference, index);
		// save current number of tabs to restore them after printing the contained object
		int oldTabsBeforeCurrentObject = tabsBeforeCurrentObject;
		int oldCurrentTabs = currentTabs;
		// use current number of tabs to indent contained object. we do not directly set
		// 'tabsBeforeCurrentObject' because the first element of the new object must be
		// printed with the old number of tabs.
		startedPrintingContainedObject = false;
		currentTabs = 0;
		doPrint((org.eclipse.emf.ecore.EObject) o, foundFormattingElements);
		// restore number of tabs after printing the contained object
		tabsBeforeCurrentObject = oldTabsBeforeCurrentObject;
		currentTabs = oldCurrentTabs;
	}
	
	public void printFormattingElements(java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations, org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation layoutInformation) {
		String hiddenTokenText = getHiddenTokenText(layoutInformation);
		if (hiddenTokenText != null) {
			// removed used information
			if (layoutInformations != null) {
				layoutInformations.remove(layoutInformation);
			}
			tokenOutputStream.add(new PrintToken(hiddenTokenText, null));
			foundFormattingElements.clear();
			startedPrintingObject = false;
			setTabsBeforeCurrentObject(0);
			return;
		}
		int printedTabs = 0;
		if (foundFormattingElements.size() > 0) {
			for (org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement foundFormattingElement : foundFormattingElements) {
				if (foundFormattingElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestWhiteSpace) {
					int amount = ((org.coolsoftware.requests.resource.request.grammar.RequestWhiteSpace) foundFormattingElement).getAmount();
					for (int i = 0; i < amount; i++) {
						tokenOutputStream.add(SPACE_TOKEN);
					}
				}
				if (foundFormattingElement instanceof org.coolsoftware.requests.resource.request.grammar.RequestLineBreak) {
					currentTabs = ((org.coolsoftware.requests.resource.request.grammar.RequestLineBreak) foundFormattingElement).getTabs();
					printedTabs += currentTabs;
					tokenOutputStream.add(NEW_LINE_TOKEN);
					for (int i = 0; i < tabsBeforeCurrentObject + currentTabs; i++) {
						tokenOutputStream.add(TAB_TOKEN);
					}
				}
			}
			foundFormattingElements.clear();
			startedPrintingObject = false;
		} else {
			if (startedPrintingObject) {
				// if no elements have been printed yet, we do not add the default token space,
				// because spaces before the first element are not desired.
				startedPrintingObject = false;
			} else {
				if (!handleTokenSpaceAutomatically) {
					tokenOutputStream.add(new PrintToken(getWhiteSpaceString(tokenSpace), null));
				}
			}
		}
		// after printing the first element, we can use the new number of tabs.
		setTabsBeforeCurrentObject(printedTabs);
	}
	
	private void setTabsBeforeCurrentObject(int tabs) {
		if (startedPrintingContainedObject) {
			return;
		}
		tabsBeforeCurrentObject = tabsBeforeCurrentObject + tabs;
		startedPrintingContainedObject = true;
	}
	
	@SuppressWarnings("unchecked")	
	public void printReference(org.eclipse.emf.ecore.EObject eObject, org.eclipse.emf.ecore.EReference reference, org.coolsoftware.requests.resource.request.grammar.RequestPlaceholder placeholder, int index, java.util.List<org.coolsoftware.requests.resource.request.grammar.RequestFormattingElement> foundFormattingElements, java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations) {
		String tokenName = placeholder.getTokenName();
		Object referencedObject = org.coolsoftware.requests.resource.request.util.RequestEObjectUtil.getFeatureValue(eObject, reference, index, false);
		// first add layout before the reference
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation referenceLayout = getLayoutInformation(layoutInformations, placeholder, referencedObject, eObject);
		printFormattingElements(foundFormattingElements, layoutInformations, referenceLayout);
		// proxy objects must be printed differently
		String deresolvedReference = null;
		if (referencedObject instanceof org.eclipse.emf.ecore.EObject) {
			org.eclipse.emf.ecore.EObject eObjectToDeResolve = (org.eclipse.emf.ecore.EObject) referencedObject;
			if (eObjectToDeResolve.eIsProxy()) {
				deresolvedReference = ((org.eclipse.emf.ecore.InternalEObject) eObjectToDeResolve).eProxyURI().fragment();
				// If the proxy was created by EMFText, we can try to recover the identifier from
				// the proxy URI
				if (deresolvedReference != null && deresolvedReference.startsWith(org.coolsoftware.requests.resource.request.IRequestContextDependentURIFragment.INTERNAL_URI_FRAGMENT_PREFIX)) {
					deresolvedReference = deresolvedReference.substring(org.coolsoftware.requests.resource.request.IRequestContextDependentURIFragment.INTERNAL_URI_FRAGMENT_PREFIX.length());
					deresolvedReference = deresolvedReference.substring(deresolvedReference.indexOf("_") + 1);
				}
			}
		}
		if (deresolvedReference == null) {
			// NC-References must always be printed by deresolving the reference. We cannot
			// use the visible token information, because deresolving usually depends on
			// attribute values of the referenced object instead of the object itself.
			@SuppressWarnings("rawtypes")			
			org.coolsoftware.requests.resource.request.IRequestReferenceResolver referenceResolver = getReferenceResolverSwitch().getResolver(reference);
			referenceResolver.setOptions(getOptions());
			deresolvedReference = referenceResolver.deResolve((org.eclipse.emf.ecore.EObject) referencedObject, eObject, reference);
		}
		org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver(tokenName);
		tokenResolver.setOptions(getOptions());
		String deresolvedToken = tokenResolver.deResolve(deresolvedReference, reference, eObject);
		// write result to output stream
		tokenOutputStream.add(new PrintToken(deresolvedToken, tokenName));
	}
	
	@SuppressWarnings("unchecked")	
	public PrintCountingMap initializePrintCountingMap(org.eclipse.emf.ecore.EObject eObject) {
		// The PrintCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		PrintCountingMap printCountingMap = new PrintCountingMap();
		java.util.List<org.eclipse.emf.ecore.EStructuralFeature> features = eObject.eClass().getEAllStructuralFeatures();
		for (org.eclipse.emf.ecore.EStructuralFeature feature : features) {
			// We get the feature value without resolving it, because resolving is not
			// required to count the number of elements that are referenced by the feature.
			// Moreover, triggering reference resolving is not desired here, because we'd also
			// like to print models that contain unresolved references.
			Object featureValue = eObject.eGet(feature, false);
			if (featureValue != null) {
				if (featureValue instanceof java.util.List<?>) {
					printCountingMap.setFeatureValues(feature.getName(), (java.util.List<Object>) featureValue);
				} else {
					printCountingMap.setFeatureValues(feature.getName(), java.util.Collections.singletonList(featureValue));
				}
			} else {
				printCountingMap.setFeatureValues(feature.getName(), null);
			}
		}
		return printCountingMap;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTextResource getResource() {
		return resource;
	}
	
	protected org.coolsoftware.requests.resource.request.mopp.RequestReferenceResolverSwitch getReferenceResolverSwitch() {
		return (org.coolsoftware.requests.resource.request.mopp.RequestReferenceResolverSwitch) new org.coolsoftware.requests.resource.request.mopp.RequestMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		org.coolsoftware.requests.resource.request.IRequestTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new org.coolsoftware.requests.resource.request.mopp.RequestProblem(errorMessage, org.coolsoftware.requests.resource.request.RequestEProblemType.PRINT_PROBLEM, org.coolsoftware.requests.resource.request.RequestEProblemSeverity.WARNING), cause);
	}
	
	protected org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformationAdapter getLayoutInformationAdapter(org.eclipse.emf.ecore.EObject element) {
		for (org.eclipse.emf.common.notify.Adapter adapter : element.eAdapters()) {
			if (adapter instanceof org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformationAdapter) {
				return (org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformationAdapter) adapter;
			}
		}
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformationAdapter newAdapter = new org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformationAdapter();
		element.eAdapters().add(newAdapter);
		return newAdapter;
	}
	
	private org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation getLayoutInformation(java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations, org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement syntaxElement, Object object, org.eclipse.emf.ecore.EObject container) {
		for (org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation layoutInformation : layoutInformations) {
			if (syntaxElement == layoutInformation.getSyntaxElement()) {
				if (object == null) {
					return layoutInformation;
				}
				// The layout information adapter must only try to resolve the object it refers
				// to, if we compare with a non-proxy object. If we're printing a resource that
				// contains proxy objects, resolving must not be triggered.
				boolean isNoProxy = true;
				if (object instanceof org.eclipse.emf.ecore.EObject) {
					org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) object;
					isNoProxy = !eObject.eIsProxy();
				}
				if (isSame(object, layoutInformation.getObject(container, isNoProxy))) {
					return layoutInformation;
				}
			}
		}
		return null;
	}
	
	public java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> getCopyOfLayoutInformation(org.eclipse.emf.ecore.EObject eObject) {
		org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformationAdapter layoutInformationAdapter = getLayoutInformationAdapter(eObject);
		java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> originalLayoutInformations = layoutInformationAdapter.getLayoutInformations();
		// create a copy of the original list of layout information object in order to be
		// able to remove used informations during printing
		java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation> layoutInformations = new java.util.ArrayList<org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation>(originalLayoutInformations.size());
		layoutInformations.addAll(originalLayoutInformations);
		return layoutInformations;
	}
	
	private String getHiddenTokenText(org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation layoutInformation) {
		if (layoutInformation != null) {
			return layoutInformation.getHiddenTokenText();
		} else {
			return null;
		}
	}
	
	private String getVisibleTokenText(org.coolsoftware.requests.resource.request.mopp.RequestLayoutInformation layoutInformation) {
		if (layoutInformation != null) {
			return layoutInformation.getVisibleTokenText();
		} else {
			return null;
		}
	}
	
	protected String getWhiteSpaceString(int count) {
		return getRepeatingString(count, ' ');
	}
	
	private String getRepeatingString(int count, char character) {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < count; i++) {
			result.append(character);
		}
		return result.toString();
	}
	
	public void setHandleTokenSpaceAutomatically(boolean handleTokenSpaceAutomatically) {
		this.handleTokenSpaceAutomatically = handleTokenSpaceAutomatically;
	}
	
	public void setTokenSpace(int tokenSpace) {
		this.tokenSpace = tokenSpace;
	}
	
	/**
	 * Prints the current tokenOutputStream to the given writer (as it is).
	 */
	public void printBasic(java.io.PrintWriter writer) throws java.io.IOException {
		for (PrintToken nextToken : tokenOutputStream) {
			writer.write(nextToken.getText());
		}
	}
	
	/**
	 * Prints the current tokenOutputStream to the given writer.
	 * 
	 * This methods implements smart whitespace printing. It does so by writing output
	 * to a token stream instead of printing the raw token text to a PrintWriter.
	 * Tokens in this stream hold both the text and the type of the token (i.e., its
	 * name).
	 * 
	 * To decide where whitespace is needed, sequences of successive tokens are
	 * searched that can be printed without separating whitespace. To determine such
	 * groups we start with two successive non-whitespace tokens, concatenate their
	 * text and use the generated ANTLR lexer to split the text. If the resulting
	 * token sequence of the concatenated text is exactly the same as the one that is
	 * to be printed, no whitespace is needed. The tokens in the sequence are checked
	 * both regarding their type and their text. If two tokens successfully form a
	 * group a third one is added and so on.
	 */
	public void printSmart(java.io.PrintWriter writer) throws java.io.IOException {
		// stores the text of the current group of tokens. this text is given to the lexer
		// to check whether it can be correctly scanned.
		StringBuilder currentBlock = new StringBuilder();
		// stores the index of the first token of the current group.
		int currentBlockStart = 0;
		// stores the text that was already successfully checked (i.e., is can be scanned
		// correctly and can thus be printed).
		String validBlock = "";
		for (int i = 0; i < tokenOutputStream.size(); i++) {
			PrintToken tokenI = tokenOutputStream.get(i);
			currentBlock.append(tokenI.getText());
			// if declared or preserved whitespace is found - print block
			if (tokenI.getTokenName() == null) {
				writer.write(currentBlock.toString());
				// reset all values
				currentBlock = new StringBuilder();
				currentBlockStart = i + 1;
				validBlock = "";
				continue;
			}
			// now check whether the current block can be scanned
			org.coolsoftware.requests.resource.request.IRequestTextScanner scanner = new org.coolsoftware.requests.resource.request.mopp.RequestMetaInformation().createLexer();
			scanner.setText(currentBlock.toString());
			// retrieve all tokens from scanner and add them to list 'tempTokens'
			java.util.List<org.coolsoftware.requests.resource.request.IRequestTextToken> tempTokens = new java.util.ArrayList<org.coolsoftware.requests.resource.request.IRequestTextToken>();
			org.coolsoftware.requests.resource.request.IRequestTextToken nextToken = scanner.getNextToken();
			while (nextToken != null && nextToken.getText() != null) {
				tempTokens.add(nextToken);
				nextToken = scanner.getNextToken();
			}
			boolean sequenceIsValid = true;
			// check whether the current block was scanned to the same token sequence
			for (int t = 0; t < tempTokens.size(); t++) {
				PrintToken printTokenT = tokenOutputStream.get(currentBlockStart + t);
				org.coolsoftware.requests.resource.request.IRequestTextToken tempToken = tempTokens.get(t);
				if (!tempToken.getText().equals(printTokenT.getText())) {
					sequenceIsValid = false;
					break;
				}
				String commonTokenName = tempToken.getName();
				String printTokenName = printTokenT.getTokenName();
				if (printTokenName.length() > 2 && printTokenName.startsWith("'") && printTokenName.endsWith("'")) {
					printTokenName = printTokenName.substring(1, printTokenName.length() - 1);
				}
				if (!commonTokenName.equals(printTokenName)) {
					sequenceIsValid = false;
					break;
				}
			}
			if (sequenceIsValid) {
				// sequence is still valid, try adding one more token in the next iteration of the
				// loop
				validBlock += tokenI.getText();
			} else {
				// sequence is not valid, must print whitespace to separate tokens
				// print text that is valid so far
				writer.write(validBlock);
				// print separating whitespace
				writer.write(" ");
				// add current token as initial value for next iteration
				currentBlock = new StringBuilder(tokenI.getText());
				currentBlockStart = i;
				validBlock = tokenI.getText();
			}
		}
		// flush remaining valid text to writer
		writer.write(validBlock);
	}
	
	private boolean isSame(Object o1, Object o2) {
		if (o1 instanceof Integer || o1 instanceof Long || o1 instanceof Byte || o1 instanceof Short || o1 instanceof Float || o2 instanceof Double) {
			return o1.equals(o2);
		}
		return o1 == o2;
	}
	
	protected java.util.List<Class<?>> getAllowedTypes(org.coolsoftware.requests.resource.request.grammar.RequestTerminal terminal) {
		java.util.List<Class<?>> allowedTypes = new java.util.ArrayList<Class<?>>();
		allowedTypes.add(terminal.getFeature().getEType().getInstanceClass());
		if (terminal instanceof org.coolsoftware.requests.resource.request.grammar.RequestContainment) {
			org.coolsoftware.requests.resource.request.grammar.RequestContainment printingContainment = (org.coolsoftware.requests.resource.request.grammar.RequestContainment) terminal;
			org.eclipse.emf.ecore.EClass[] typeRestrictions = printingContainment.getAllowedTypes();
			if (typeRestrictions != null && typeRestrictions.length > 0) {
				allowedTypes.clear();
				for (org.eclipse.emf.ecore.EClass eClass : typeRestrictions) {
					allowedTypes.add(eClass.getInstanceClass());
				}
			}
		}
		return allowedTypes;
	}
	
}
