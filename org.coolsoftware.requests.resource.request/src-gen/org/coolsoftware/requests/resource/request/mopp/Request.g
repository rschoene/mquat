grammar Request;

options {
	superClass = RequestANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package org.coolsoftware.requests.resource.request.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package org.coolsoftware.requests.resource.request.mopp;
}

@members{
	private org.coolsoftware.requests.resource.request.IRequestTokenResolverFactory tokenResolverFactory = new org.coolsoftware.requests.resource.request.mopp.RequestTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new org.coolsoftware.requests.resource.request.IRequestProblem() {
					public org.coolsoftware.requests.resource.request.RequestEProblemSeverity getSeverity() {
						return org.coolsoftware.requests.resource.request.RequestEProblemSeverity.ERROR;
					}
					public org.coolsoftware.requests.resource.request.RequestEProblemType getType() {
						return org.coolsoftware.requests.resource.request.RequestEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<org.coolsoftware.requests.resource.request.IRequestQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		org.coolsoftware.requests.resource.request.IRequestExpectedElement terminal = org.coolsoftware.requests.resource.request.grammar.RequestFollowSetProvider.TERMINALS[terminalID];
		org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[] containmentTrace = new org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentTrace[i - 2] = org.coolsoftware.requests.resource.request.grammar.RequestFollowSetProvider.LINKS[ids[i]];
		}
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal expectedElement = new org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
		int currentPos = getTokenStream().index();
		if (currentPos == 0) {
			return;
		}
		int endPos = currentPos - 1;
		for (; endPos >= this.lastPosition; endPos--) {
			org.antlr.runtime3_4_0.Token token = getTokenStream().get(endPos);
			int _channel = token.getChannel();
			if (_channel != 99) {
				break;
			}
		}
		for (int pos = this.lastPosition; pos < endPos; pos++) {
			org.antlr.runtime3_4_0.Token token = getTokenStream().get(pos);
			int _channel = token.getChannel();
			if (_channel == 99) {
				if (token.getType() == RequestLexer.SL_COMMENT) {
					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
					if (feature != null) {
						// call token resolver
						org.coolsoftware.requests.resource.request.IRequestTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
						resolvedResolver.setOptions(getOptions());
						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult resolvedResult = getFreshTokenResolveResult();
						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
						Object resolvedObject = resolvedResult.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
						}
						if (java.lang.String.class.isInstance(resolvedObject)) {
							addObjectToList(element, feature, resolvedObject);
						} else {
							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
						}
					} else {
						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
					}
				}
				if (token.getType() == RequestLexer.ML_COMMENT) {
					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
					if (feature != null) {
						// call token resolver
						org.coolsoftware.requests.resource.request.IRequestTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
						resolvedResolver.setOptions(getOptions());
						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult resolvedResult = getFreshTokenResolveResult();
						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
						Object resolvedObject = resolvedResult.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
						}
						if (java.lang.String.class.isInstance(resolvedObject)) {
							addObjectToList(element, feature, resolvedObject);
						} else {
							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
						}
					} else {
						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
					}
				}
			}
		}
		this.lastPosition = (endPos < 0 ? 0 : endPos);
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
				org.coolsoftware.requests.resource.request.IRequestLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
				org.coolsoftware.requests.resource.request.IRequestLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
				org.coolsoftware.requests.resource.request.IRequestLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new RequestParser(new org.antlr.runtime3_4_0.CommonTokenStream(new RequestLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new RequestParser(new org.antlr.runtime3_4_0.CommonTokenStream(new RequestLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new org.coolsoftware.requests.resource.request.util.RequestRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public RequestParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((RequestLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((RequestLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == org.coolsoftware.requests.Request.class) {
				return parse_org_coolsoftware_requests_Request();
			}
			if (type.getInstanceClass() == org.coolsoftware.requests.MetaParamValue.class) {
				return parse_org_coolsoftware_requests_MetaParamValue();
			}
			if (type.getInstanceClass() == org.coolsoftware.requests.Import.class) {
				return parse_org_coolsoftware_requests_Import();
			}
			if (type.getInstanceClass() == org.coolsoftware.requests.Platform.class) {
				return parse_org_coolsoftware_requests_Platform();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.EclFile.class) {
				return parse_org_coolsoftware_ecl_EclFile();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.CcmImport.class) {
				return parse_org_coolsoftware_ecl_CcmImport();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentContract.class) {
				return parse_org_coolsoftware_ecl_SWComponentContract();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentContract.class) {
				return parse_org_coolsoftware_ecl_HWComponentContract();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SWContractMode.class) {
				return parse_org_coolsoftware_ecl_SWContractMode();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.HWContractMode.class) {
				return parse_org_coolsoftware_ecl_HWContractMode();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.ProvisionClause.class) {
				return parse_org_coolsoftware_ecl_ProvisionClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentRequirementClause.class) {
				return parse_org_coolsoftware_ecl_SWComponentRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentRequirementClause.class) {
				return parse_org_coolsoftware_ecl_HWComponentRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.SelfRequirementClause.class) {
				return parse_org_coolsoftware_ecl_SelfRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.PropertyRequirementClause.class) {
				return parse_org_coolsoftware_ecl_PropertyRequirementClause();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.FormulaTemplate.class) {
				return parse_org_coolsoftware_ecl_FormulaTemplate();
			}
			if (type.getInstanceClass() == org.coolsoftware.ecl.Metaparameter.class) {
				return parse_org_coolsoftware_ecl_Metaparameter();
			}
			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.Block.class) {
				return parse_org_coolsoftware_coolcomponents_expressions_Block();
			}
			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.variables.Variable.class) {
				return parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();
			}
		}
		throw new org.coolsoftware.requests.resource.request.mopp.RequestUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(org.coolsoftware.requests.resource.request.IRequestOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public org.coolsoftware.requests.resource.request.IRequestParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>>();
		org.coolsoftware.requests.resource.request.mopp.RequestParseResult parseResult = new org.coolsoftware.requests.resource.request.mopp.RequestParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.requests.resource.request.IRequestTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		org.coolsoftware.requests.resource.request.IRequestParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal>();
		java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 154;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]> newFollowerPair : newFollowers) {
							org.coolsoftware.requests.resource.request.IRequestExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal newFollowTerminal = new org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal(container, newFollower, followSetID, newFollowerPair.getRight());
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[0]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_org_coolsoftware_requests_Request{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_org_coolsoftware_requests_Request returns [org.coolsoftware.requests.Request element = null]
@init{
}
:
	(
		a0_0 = parse_org_coolsoftware_requests_Import		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
				startIncompleteElement(element);
			}
			if (a0_0 != null) {
				if (a0_0 != null) {
					Object value = a0_0;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__IMPORT), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_0, a0_0, true);
				copyLocalizationInfos(a0_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1]);
	}
	
	(
		a1_0 = parse_org_coolsoftware_requests_Platform		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__HARDWARE), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[2]);
	}
	
	a2 = 'call' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[3]);
	}
	
	(
		a3 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__COMPONENT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Request, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRequestComponentReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__COMPONENT), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__COMPONENT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_6, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[4]);
	}
	
	a4 = '.' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[5]);
	}
	
	(
		a5 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
				startIncompleteElement(element);
			}
			if (a5 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__TARGET), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.PortType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createPortType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Request, org.coolsoftware.coolcomponents.ccm.structure.PortType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRequestTargetReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__TARGET), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__TARGET), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_8, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[6]);
	}
	
	a6 = 'expecting' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[7]);
	}
	
	a7 = '{' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_11, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[8]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[9]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[10]);
	}
	
	(
		(
			a8_0 = parse_org_coolsoftware_requests_MetaParamValue			{
				if (terminateParsing) {
					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
					startIncompleteElement(element);
				}
				if (a8_0 != null) {
					if (a8_0 != null) {
						Object value = a8_0;
						addObjectToList(element, org.coolsoftware.requests.RequestsPackage.REQUEST__META_PARAM_VALUES, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_13, a8_0, true);
					copyLocalizationInfos(a8_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[11]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[12]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[13]);
	}
	
	(
		(
			a9_0 = parse_org_coolsoftware_ecl_PropertyRequirementClause			{
				if (terminateParsing) {
					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
					startIncompleteElement(element);
				}
				if (a9_0 != null) {
					if (a9_0 != null) {
						Object value = a9_0;
						addObjectToList(element, org.coolsoftware.requests.RequestsPackage.REQUEST__REQS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_15, a9_0, true);
					copyLocalizationInfos(a9_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[14]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[15]);
	}
	
	a10 = '}' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_17, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
	}
	{
		// expected elements (follow set)
	}
	
;

parse_org_coolsoftware_requests_MetaParamValue returns [org.coolsoftware.requests.MetaParamValue element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createMetaParamValue();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__METAPARAM), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.Parameter proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createParameter();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.MetaParamValue, org.coolsoftware.coolcomponents.ccm.structure.Parameter>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getMetaParamValueMetaparamReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__METAPARAM), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__METAPARAM), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_1_0_0_0, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[16]);
	}
	
	a1 = '=' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createMetaParamValue();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_1_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[17]);
	}
	
	(
		a2 = NUMBER		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createMetaParamValue();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__VALUE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__VALUE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_1_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[18]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[19]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[20]);
	}
	
;

parse_org_coolsoftware_requests_Import returns [org.coolsoftware.requests.Import element = null]
@init{
}
:
	a0 = 'import' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createImport();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[21]);
	}
	
	a1 = 'ccm' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createImport();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_2_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[22]);
	}
	
	(
		a2 = FILE_NAME		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createImport();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("FILE_NAME");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.IMPORT__MODEL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.StructuralModel proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createStructuralModel();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Import, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getImportModelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.IMPORT__MODEL), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.IMPORT__MODEL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_2_0_0_3, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[23]);
	}
	
;

parse_org_coolsoftware_requests_Platform returns [org.coolsoftware.requests.Platform element = null]
@init{
}
:
	a0 = 'target' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createPlatform();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_3_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[24]);
	}
	
	a1 = 'platform' {
		if (element == null) {
			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createPlatform();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_3_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[25]);
	}
	
	(
		a2 = FILE_NAME		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createPlatform();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("FILE_NAME");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.PLATFORM__HWMODEL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.variant.VariantModel proxy = org.coolsoftware.coolcomponents.ccm.variant.VariantFactory.eINSTANCE.createVariantModel();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Platform, org.coolsoftware.coolcomponents.ccm.variant.VariantModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPlatformHwmodelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.PLATFORM__HWMODEL), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.PLATFORM__HWMODEL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_3_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[26]);
	}
	
;

parse_org_coolsoftware_ecl_EclFile returns [org.coolsoftware.ecl.EclFile element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_coolsoftware_ecl_Import			{
				if (terminateParsing) {
					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__IMPORTS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_0_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[27]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[28]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[29]);
	}
	
	(
		(
			a1_0 = parse_org_coolsoftware_ecl_EclContract			{
				if (terminateParsing) {
					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
					startIncompleteElement(element);
				}
				if (a1_0 != null) {
					if (a1_0 != null) {
						Object value = a1_0;
						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__CONTRACTS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_0_0_0_2, a1_0, true);
					copyLocalizationInfos(a1_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[30]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[31]);
	}
	
;

parse_org_coolsoftware_ecl_CcmImport returns [org.coolsoftware.ecl.CcmImport element = null]
@init{
}
:
	a0 = 'import' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_1_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[32]);
	}
	
	a1 = 'ccm' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_1_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[33]);
	}
	
	(
		a2 = QUOTED_91_93		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_91_93");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.StructuralModel proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createStructuralModel();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.CcmImport, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getCcmImportCcmStructuralModelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_1_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[34]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[35]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[36]);
	}
	
;

parse_org_coolsoftware_ecl_SWComponentContract returns [org.coolsoftware.ecl.SWComponentContract element = null]
@init{
}
:
	a0 = 'contract' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[37]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[38]);
	}
	
	a2 = 'implements' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[39]);
	}
	
	a3 = 'software' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[40]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_8, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[41]);
	}
	
	a5 = '.' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[42]);
	}
	
	(
		a6 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
				startIncompleteElement(element);
			}
			if (a6 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.PortType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createPortType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractPortReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_10, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[43]);
	}
	
	a7 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_11, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[44]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[45]);
	}
	
	(
		(
			a8 = 'metaparameter:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_13_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[46]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[47]);
			}
			
			(
				(
					(
						a9_0 = parse_org_coolsoftware_ecl_Metaparameter						{
							if (terminateParsing) {
								throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
							}
							if (element == null) {
								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
								startIncompleteElement(element);
							}
							if (a9_0 != null) {
								if (a9_0 != null) {
									Object value = a9_0;
									addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__METAPARAMS, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_13_0_0_1_0_0_0, a9_0, true);
								copyLocalizationInfos(a9_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[48]);
						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[49]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[50]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[51]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[52]);
	}
	
	(
		(
			(
				a10_0 = parse_org_coolsoftware_ecl_SWContractMode				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
						startIncompleteElement(element);
					}
					if (a10_0 != null) {
						if (a10_0 != null) {
							Object value = a10_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__MODES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_15_0_0_0, a10_0, true);
						copyLocalizationInfos(a10_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[53]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[54]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[55]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[56]);
	}
	
	a11 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_16, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[57]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[58]);
	}
	
;

parse_org_coolsoftware_ecl_HWComponentContract returns [org.coolsoftware.ecl.HWComponentContract element = null]
@init{
}
:
	a0 = 'contract' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[59]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[60]);
	}
	
	a2 = 'implements' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[61]);
	}
	
	a3 = 'hardware' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[62]);
	}
	
	(
		a4 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_8, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[63]);
	}
	
	a5 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[64]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[65]);
	}
	
	(
		(
			a6 = 'metaparameter:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_12_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[66]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[67]);
			}
			
			(
				(
					(
						a7_0 = parse_org_coolsoftware_ecl_Metaparameter						{
							if (terminateParsing) {
								throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
							}
							if (element == null) {
								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
								startIncompleteElement(element);
							}
							if (a7_0 != null) {
								if (a7_0 != null) {
									Object value = a7_0;
									addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__METAPARAMS, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_12_0_0_1_0_0_0, a7_0, true);
								copyLocalizationInfos(a7_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[68]);
						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[69]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[70]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[71]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[72]);
	}
	
	(
		(
			(
				a8_0 = parse_org_coolsoftware_ecl_HWContractMode				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
						startIncompleteElement(element);
					}
					if (a8_0 != null) {
						if (a8_0 != null) {
							Object value = a8_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__MODES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_14_0_0_0, a8_0, true);
						copyLocalizationInfos(a8_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[73]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[74]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[75]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[76]);
	}
	
	a9 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_15, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[77]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[78]);
	}
	
;

parse_org_coolsoftware_ecl_SWContractMode returns [org.coolsoftware.ecl.SWContractMode element = null]
@init{
}
:
	a0 = 'mode' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[79]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[80]);
	}
	
	a2 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[81]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[82]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[83]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[84]);
	}
	
	(
		(
			(
				a3_0 = parse_org_coolsoftware_ecl_SWContractClause				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__CLAUSES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_6_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[85]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[86]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[87]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[88]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[89]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[90]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[91]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[92]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[93]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[94]);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[95]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[96]);
	}
	
;

parse_org_coolsoftware_ecl_HWContractMode returns [org.coolsoftware.ecl.HWContractMode element = null]
@init{
}
:
	a0 = 'mode' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[97]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[98]);
	}
	
	a2 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[99]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[100]);
	}
	
	(
		(
			(
				a3_0 = parse_org_coolsoftware_ecl_HWContractClause				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__CLAUSES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_6_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[101]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[102]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[103]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[104]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[105]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[106]);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[107]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[108]);
	}
	
;

parse_org_coolsoftware_ecl_ProvisionClause returns [org.coolsoftware.ecl.ProvisionClause element = null]
@init{
}
:
	a0 = 'provides' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[109]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getProvisionClauseProvidedPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_2, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[110]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[111]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[112]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[113]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[114]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[115]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[116]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[117]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[118]);
	}
	
	(
		(
			a2 = 'min:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_4_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[119]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[120]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[121]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[122]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[123]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[124]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[125]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[126]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[127]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[128]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[129]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[130]);
			}
			
			(
				a3_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MIN_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_4_0_0_2, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[131]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[132]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[133]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[134]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[135]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[136]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[137]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[138]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[139]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[140]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[141]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[142]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[143]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[144]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[145]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[146]);
	}
	
	(
		(
			a4 = 'max:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[147]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[148]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[149]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[150]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[151]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[152]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[153]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[154]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[155]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[156]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[157]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[158]);
			}
			
			(
				a5_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
						startIncompleteElement(element);
					}
					if (a5_0 != null) {
						if (a5_0 != null) {
							Object value = a5_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MAX_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_5_0_0_2, a5_0, true);
						copyLocalizationInfos(a5_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[159]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[160]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[161]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[162]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[163]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[164]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[165]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[166]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[167]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[168]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[169]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[170]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[171]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[172]);
	}
	
	(
		(
			a6_0 = parse_org_coolsoftware_ecl_FormulaTemplate			{
				if (terminateParsing) {
					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
					startIncompleteElement(element);
				}
				if (a6_0 != null) {
					if (a6_0 != null) {
						Object value = a6_0;
						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__FORMULA), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_6, a6_0, true);
					copyLocalizationInfos(a6_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[173]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[174]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[175]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[176]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[177]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[178]);
	}
	
;

parse_org_coolsoftware_ecl_SWComponentRequirementClause returns [org.coolsoftware.ecl.SWComponentRequirementClause element = null]
@init{
}
:
	a0 = 'requires' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[179]);
	}
	
	a1 = 'component' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[180]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentRequirementClauseRequiredComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[181]);
	}
	
	a3 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[182]);
	}
	
	(
		(
			(
				a4_0 = parse_org_coolsoftware_ecl_PropertyRequirementClause				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_8_0_0_0, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[183]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[184]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[185]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[186]);
	}
	
	a5 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[187]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[188]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[189]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[190]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[191]);
	}
	
;

parse_org_coolsoftware_ecl_HWComponentRequirementClause returns [org.coolsoftware.ecl.HWComponentRequirementClause element = null]
@init{
}
:
	a0 = 'requires' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[192]);
	}
	
	a1 = 'resource' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[193]);
	}
	
	(
		a2 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentRequirementClauseRequiredResourceTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_4, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[194]);
	}
	
	a3 = '{' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[195]);
	}
	
	(
		(
			(
				a4_0 = parse_org_coolsoftware_ecl_PropertyRequirementClause				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_8_0_0_0, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[196]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[197]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[198]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[199]);
	}
	
	a5 = '}' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_9, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[200]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[201]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[202]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[203]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[204]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[205]);
	}
	
;

parse_org_coolsoftware_ecl_SelfRequirementClause returns [org.coolsoftware.ecl.SelfRequirementClause element = null]
@init{
}
:
	a0 = 'requires' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_9_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[206]);
	}
	
	(
		a1_0 = parse_org_coolsoftware_ecl_PropertyRequirementClause		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_9_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[207]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[208]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[209]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[210]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[211]);
	}
	
;

parse_org_coolsoftware_ecl_PropertyRequirementClause returns [org.coolsoftware.ecl.PropertyRequirementClause element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
				collectHiddenTokens(element);
				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.PropertyRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPropertyRequirementClauseRequiredPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_0, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[212]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[213]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[214]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[215]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[216]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[217]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[218]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[219]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[220]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[221]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[222]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[223]);
	}
	
	(
		(
			a1 = 'min:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[224]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[225]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[226]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[227]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[228]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[229]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[230]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[231]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[232]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[233]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[234]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[235]);
			}
			
			(
				a2_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
						startIncompleteElement(element);
					}
					if (a2_0 != null) {
						if (a2_0 != null) {
							Object value = a2_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_2_0_0_2, a2_0, true);
						copyLocalizationInfos(a2_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[236]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[237]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[238]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[239]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[240]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[241]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[242]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[243]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[244]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[245]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[246]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[247]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[248]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[249]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[250]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[251]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[252]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[253]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[254]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[255]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[256]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[257]);
	}
	
	(
		(
			a3 = 'max:' {
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[258]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[259]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[260]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[261]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[262]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[263]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[264]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[265]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[266]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[267]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[268]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[269]);
			}
			
			(
				a4_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_3_0_0_2, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[270]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[271]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[272]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[273]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[274]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[275]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[276]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[277]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[278]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[279]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[280]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[281]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[282]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[283]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[284]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[285]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[286]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[287]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[288]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[289]);
	}
	
	(
		(
			a5_0 = parse_org_coolsoftware_ecl_FormulaTemplate			{
				if (terminateParsing) {
					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
				}
				if (element == null) {
					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
					startIncompleteElement(element);
				}
				if (a5_0 != null) {
					if (a5_0 != null) {
						Object value = a5_0;
						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA), value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_4, a5_0, true);
					copyLocalizationInfos(a5_0, element);
				}
			}
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[290]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[291]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[292]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[293]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[294]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[295]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[296]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[297]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[298]);
	}
	
;

parse_org_coolsoftware_ecl_FormulaTemplate returns [org.coolsoftware.ecl.FormulaTemplate element = null]
@init{
}
:
	a0 = '<' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[299]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[300]);
	}
	
	a2 = '(' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[301]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[302]);
	}
	
	(
		(
			(
				a3 = TEXT				
				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
						tokenResolver.setOptions(getOptions());
						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.coolsoftware.coolcomponents.ccm.structure.Parameter proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createParameter();
						collectHiddenTokens(element);
						registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFormulaTemplateMetaparameterReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_3_0_0_0, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[303]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[304]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[305]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[306]);
	}
	
	a4 = ')>' {
		if (element == null) {
			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[307]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[308]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[309]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[310]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[311]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[312]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[313]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[314]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[315]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[316]);
	}
	
;

parse_org_coolsoftware_ecl_Metaparameter returns [org.coolsoftware.ecl.Metaparameter element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createMetaparameter();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_12_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[317]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[318]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[319]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[320]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[321]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[322]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[323]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[324]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[325]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[326]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[327]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[328]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[329]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[330]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[331]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[332]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[333]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[334]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[335]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[336]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[337]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[338]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[339]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[340]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[341]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[342]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[343]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[344]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[345]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[346]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[347]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[348]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[349]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[350]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[351]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[352]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[353]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[354]);
	}
	
;

parse_org_coolsoftware_coolcomponents_expressions_Block returns [org.coolsoftware.coolcomponents.expressions.Block element = null]
@init{
}
:
	a0 = '{' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[355]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[356]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[357]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[358]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[359]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[360]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[361]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[362]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[363]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[364]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[365]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[366]);
	}
	
	(
		a1_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[367]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[368]);
	}
	
	(
		(
			a2 = ';' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_4_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[369]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[370]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[371]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[372]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[373]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[374]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[375]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[376]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[377]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[378]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[379]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[380]);
			}
			
			(
				a3_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_4_0_0_2, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[381]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[382]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[383]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[384]);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
	}
	
;

parse_org_coolsoftware_coolcomponents_expressions_variables_Variable returns [org.coolsoftware.coolcomponents.expressions.variables.Variable element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[385]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[386]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[387]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[388]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[389]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[390]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[391]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[392]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[393]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[394]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[395]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[396]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[397]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[398]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[399]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[400]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[401]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[402]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[403]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[404]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[405]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[406]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[407]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[408]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[409]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[410]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[411]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[412]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[413]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[414]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[415]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[416]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[417]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[418]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[419]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[420]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[421]);
	}
	
	(
		(
			a1 = ':' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_1_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[422]);
			}
			
			(
				a2 = TYPE_LITERAL				
				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
						startIncompleteElement(element);
					}
					if (a2 != null) {
						org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TYPE_LITERAL");
						tokenResolver.setOptions(getOptions());
						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
						}
						org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral resolved = (org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_1_0_0_3, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[423]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[424]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[425]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[426]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[427]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[428]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[429]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[430]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[431]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[432]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[433]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[434]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[435]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[436]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[437]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[438]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[439]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[440]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[441]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[442]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[443]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[444]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[445]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[446]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[447]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[448]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[449]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[450]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[451]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[452]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[453]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[454]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[455]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[456]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[457]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[458]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[459]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[460]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[461]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[462]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[463]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[464]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[465]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[466]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[467]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[468]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[469]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[470]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[471]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[472]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[473]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[474]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[475]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[476]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[477]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[478]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[479]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[480]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[481]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[482]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[483]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[484]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[485]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[486]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[487]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[488]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[489]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[490]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[491]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[492]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[493]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[494]);
	}
	
	(
		(
			a3 = '=' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[495]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[496]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[497]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[498]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[499]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[500]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[501]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[502]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[503]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[504]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[505]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[506]);
			}
			
			(
				a4_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_2_0_0_3, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[507]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[508]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[509]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[510]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[511]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[512]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[513]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[514]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[515]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[516]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[517]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[518]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[519]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[520]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[521]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[522]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[523]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[524]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[525]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[526]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[527]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[528]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[529]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[530]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[531]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[532]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[533]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[534]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[535]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[536]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[537]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[538]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[539]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[540]);
				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[541]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[542]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[543]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[544]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[545]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[546]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[547]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[548]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[549]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[550]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[551]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[552]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[553]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[554]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[555]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[556]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[557]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[558]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[559]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[560]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[561]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[562]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[563]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[564]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[565]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[566]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[567]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[568]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[569]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[570]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[571]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[572]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[573]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[574]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[575]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[576]);
	}
	
	|//derived choice rules for sub-classes: 
	
	c0 = parse_org_coolsoftware_ecl_Metaparameter{ element = c0; /* this is a subclass or primitive expression choice */ }
	
;

parseop_Expression_level_03 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
	leftArg = parseop_Expression_level_04	((
		()
		{ element = null; }
		a0 = 'or' {
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_25_0_0_2, null, true);
			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[577]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[578]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[579]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[580]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[581]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[582]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[583]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[584]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[585]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[586]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[587]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[588]);
		}
		
		rightArg = parseop_Expression_level_04		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_25_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_25_0_0_4, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
		|		
		()
		{ element = null; }
		a0 = 'and' {
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_26_0_0_2, null, true);
			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[589]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[590]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[591]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[592]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[593]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[594]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[595]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[596]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[597]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[598]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[599]);
			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[600]);
		}
		
		rightArg = parseop_Expression_level_04		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_26_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_26_0_0_4, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
	)+ | /* epsilon */ { element = leftArg; }
	
)
;

parseop_Expression_level_04 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_5((
	()
	{ element = null; }
	a0 = 'implies' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_27_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[601]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[602]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[603]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[604]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[605]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[606]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[607]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[608]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[609]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[610]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[611]);
		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[612]);
	}
	
	rightArg = parseop_Expression_level_5	{
		if (terminateParsing) {
			throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
		}
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		if (leftArg != null) {
			if (leftArg != null) {
				Object value = leftArg;
				element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_27_0_0_0, leftArg, true);
			copyLocalizationInfos(leftArg, element);
		}
	}
	{
		if (terminateParsing) {
			throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
		}
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		if (rightArg != null) {
			if (rightArg != null) {
				Object value = rightArg;
				addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_27_0_0_4, rightArg, true);
			copyLocalizationInfos(rightArg, element);
		}
	}
	{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_5 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'f' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_11_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[613]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[614]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[615]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[616]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[617]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[618]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[619]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[620]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[621]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[622]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[623]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[624]);
}

arg = parseop_Expression_level_11{
if (terminateParsing) {
	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
	startIncompleteElement(element);
}
if (arg != null) {
	if (arg != null) {
		Object value = arg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_11_0_0_2, arg, true);
	copyLocalizationInfos(arg, element);
}
}
|
a0 = '!' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_28_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[625]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[626]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[627]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[628]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[629]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[630]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[631]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[632]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[633]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[634]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[635]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[636]);
}

arg = parseop_Expression_level_11{
if (terminateParsing) {
	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
	startIncompleteElement(element);
}
if (arg != null) {
	if (arg != null) {
		Object value = arg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_28_0_0_2, arg, true);
	copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_11{ element = arg; }
;

parseop_Expression_level_11 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_12((
()
{ element = null; }
a0 = '>' {
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_19_0_0_2, null, true);
	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[637]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[638]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[639]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[640]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[641]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[642]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[643]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[644]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[645]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[646]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[647]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[648]);
}

rightArg = parseop_Expression_level_12{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_19_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_19_0_0_4, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '>=' {
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_20_0_0_2, null, true);
	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[649]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[650]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[651]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[652]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[653]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[654]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[655]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[656]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[657]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[658]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[659]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[660]);
}

rightArg = parseop_Expression_level_12{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_20_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_20_0_0_4, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '<' {
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_21_0_0_2, null, true);
	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[661]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[662]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[663]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[664]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[665]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[666]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[667]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[668]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[669]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[670]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[671]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[672]);
}

rightArg = parseop_Expression_level_12{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_21_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_21_0_0_4, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '<=' {
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_22_0_0_2, null, true);
	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[673]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[674]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[675]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[676]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[677]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[678]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[679]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[680]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[681]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[682]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[683]);
	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[684]);
}

rightArg = parseop_Expression_level_12{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_22_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_22_0_0_4, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_12 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_19((
()
{ element = null; }
a0 = '==' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_23_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[685]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[686]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[687]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[688]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[689]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[690]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[691]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[692]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[693]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[694]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[695]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[696]);
}

rightArg = parseop_Expression_level_19{
if (terminateParsing) {
	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_23_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_23_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '!=' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_24_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[697]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[698]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[699]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[700]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[701]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[702]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[703]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[704]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[705]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[706]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[707]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[708]);
}

rightArg = parseop_Expression_level_19{
if (terminateParsing) {
	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_24_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_24_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_19 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_21((
()
{ element = null; }
a0 = '^' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_18_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[709]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[710]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[711]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[712]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[713]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[714]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[715]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[716]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[717]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[718]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[719]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[720]);
}

rightArg = parseop_Expression_level_21{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
	Object value = leftArg;
	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_18_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
	Object value = rightArg;
	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_18_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_21 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_22((
()
{ element = null; }
a0 = '+' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_12_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[721]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[722]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[723]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[724]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[725]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[726]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[727]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[728]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[729]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[730]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[731]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[732]);
}

rightArg = parseop_Expression_level_22{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_12_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_12_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '-' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_13_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[733]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[734]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[735]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[736]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[737]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[738]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[739]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[740]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[741]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[742]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[743]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[744]);
}

rightArg = parseop_Expression_level_22{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_13_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_13_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_22 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_80((
()
{ element = null; }
a0 = '*' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_16_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[745]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[746]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[747]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[748]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[749]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[750]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[751]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[752]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[753]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[754]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[755]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[756]);
}

rightArg = parseop_Expression_level_80{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_16_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_16_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '/' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_17_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[757]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[758]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[759]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[760]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[761]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[762]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[763]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[764]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[765]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[766]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[767]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[768]);
}

rightArg = parseop_Expression_level_80{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_17_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_17_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_80 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'sin' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_9_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[769]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[770]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[771]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[772]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[773]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[774]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[775]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[776]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[777]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[778]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[779]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[780]);
}

arg = parseop_Expression_level_87{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_9_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
a0 = 'cos' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_10_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[781]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[782]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[783]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[784]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[785]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[786]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[787]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[788]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[789]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[790]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[791]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[792]);
}

arg = parseop_Expression_level_87{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_10_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_87{ element = arg; }
;

parseop_Expression_level_87 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'not' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_8_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[793]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[794]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[795]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[796]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[797]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[798]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[799]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[800]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[801]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[802]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[803]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[804]);
}

arg = parseop_Expression_level_88{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_8_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_88{ element = arg; }
;

parseop_Expression_level_88 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
arg = parseop_Expression_level_90(
a0 = '++' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_6_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[805]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[806]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[807]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[808]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[809]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[810]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[811]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[812]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[813]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[814]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[815]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[816]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[817]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[818]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[819]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[820]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[821]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[822]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[823]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[824]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[825]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[826]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[827]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[828]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[829]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[830]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[831]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[832]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[833]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[834]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[835]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[836]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[837]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[838]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[839]);
}

{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_6_0_0_0, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
a0 = '--' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_7_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[840]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[841]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[842]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[843]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[844]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[845]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[846]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[847]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[848]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[849]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[850]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[851]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[852]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[853]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[854]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[855]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[856]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[857]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[858]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[859]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[860]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[861]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[862]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[863]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[864]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[865]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[866]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[867]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[868]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[869]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[870]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[871]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[872]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[873]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[874]);
}

{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_7_0_0_0, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
a0 = '=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_32_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[875]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[876]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[877]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[878]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[879]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[880]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[881]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[882]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[883]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[884]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[885]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[886]);
}

(
a1_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
Object value = a1_0;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_32_0_0_4, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[887]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[888]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[889]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[890]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[891]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[892]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[893]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[894]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[895]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[896]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[897]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[898]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[899]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[900]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[901]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[902]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[903]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[904]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[905]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[906]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[907]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[908]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[909]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[910]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[911]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[912]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[913]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[914]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[915]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[916]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[917]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[918]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[919]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[920]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[921]);
}

{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_32_0_0_0, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
/* epsilon */ { element = arg; } 
)
;

parseop_Expression_level_90 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_91((
()
{ element = null; }
a0 = '+=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_14_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[922]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[923]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[924]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[925]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[926]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[927]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[928]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[929]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[930]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[931]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[932]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[933]);
}

rightArg = parseop_Expression_level_91{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_14_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_14_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '-=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_15_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[934]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[935]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[936]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[937]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[938]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[939]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[940]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[941]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[942]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[943]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[944]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[945]);
}

rightArg = parseop_Expression_level_91{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_15_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_15_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_91 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
c0 = parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression{ element = c1; /* this is a subclass or primitive expression choice */ }
|c2 = parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression{ element = c2; /* this is a subclass or primitive expression choice */ }
|c3 = parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression{ element = c3; /* this is a subclass or primitive expression choice */ }
|c4 = parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression{ element = c4; /* this is a subclass or primitive expression choice */ }
|c5 = parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration{ element = c5; /* this is a subclass or primitive expression choice */ }
|c6 = parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression{ element = c6; /* this is a subclass or primitive expression choice */ }
;

parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element = null]
@init{
}
:
(
a0 = INTEGER_LITERAL
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createIntegerLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER_LITERAL");
tokenResolver.setOptions(getOptions());
org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.Long resolved = (java.lang.Long) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_1_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[946]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[947]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[948]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[949]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[950]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[951]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[952]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[953]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[954]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[955]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[956]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[957]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[958]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[959]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[960]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[961]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[962]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[963]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[964]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[965]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[966]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[967]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[968]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[969]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[970]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[971]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[972]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[973]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[974]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[975]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[976]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[977]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[978]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[979]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[980]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element = null]
@init{
}
:
(
a0 = REAL_LITERAL
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createRealLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
tokenResolver.setOptions(getOptions());
org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.Double resolved = (java.lang.Double) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_2_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[981]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[982]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[983]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[984]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[985]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[986]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[987]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[988]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[989]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[990]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[991]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[992]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[993]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[994]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[995]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[996]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[997]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[998]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[999]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1000]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1001]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1002]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1003]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1004]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1005]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1006]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1007]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1008]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1009]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1010]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1011]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1012]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1013]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1014]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1015]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element = null]
@init{
}
:
(
(
a0 = 'true' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_3_0_0_0, true, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
// set value of boolean attribute
Object value = true;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
|a1 = 'false' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_3_0_0_0, false, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
// set value of boolean attribute
Object value = false;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
)
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1016]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1017]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1018]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1019]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1020]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1021]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1022]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1023]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1024]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1025]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1026]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1027]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1028]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1029]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1030]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1031]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1032]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1033]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1034]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1035]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1036]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1037]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1038]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1039]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1040]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1041]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1042]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1043]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1044]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1045]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1046]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1047]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1048]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1049]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1050]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element = null]
@init{
}
:
(
a0 = QUOTED_34_34
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createStringLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
tokenResolver.setOptions(getOptions());
org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.String resolved = (java.lang.String) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_4_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1051]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1052]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1053]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1054]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1055]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1056]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1057]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1058]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1059]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1060]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1061]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1062]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1063]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1064]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1065]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1066]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1067]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1068]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1069]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1070]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1071]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1072]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1073]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1074]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1075]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1076]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1077]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1078]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1079]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1080]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1081]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1082]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1083]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1084]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1085]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression returns [org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element = null]
@init{
}
:
a0 = '(' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_5_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1086]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1087]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1088]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1089]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1090]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1091]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1092]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1093]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1094]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1095]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1096]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1097]);
}

(
a1_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
Object value = a1_0;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_5_0_0_2, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1098]);
}

a2 = ')' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_5_0_0_4, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1099]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1100]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1101]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1102]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1103]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1104]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1105]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1106]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1107]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1108]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1109]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1110]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1111]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1112]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1113]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1114]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1115]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1116]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1117]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1118]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1119]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1120]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1121]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1122]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1123]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1124]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1125]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1126]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1127]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1128]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1129]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1130]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1131]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1132]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1133]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration returns [org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element = null]
@init{
}
:
a0 = 'var' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_29_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1134]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1135]);
}

(
a1_0 = parse_org_coolsoftware_coolcomponents_expressions_variables_Variable{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
Object value = a1_0;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_29_0_0_2, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1136]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1137]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1138]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1139]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1140]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1141]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1142]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1143]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1144]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1145]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1146]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1147]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1148]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1149]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1150]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1151]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1152]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1153]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1154]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1155]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1156]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1157]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1158]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1159]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1160]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1161]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1162]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1163]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1164]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1165]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1166]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1167]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1168]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1169]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1170]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression returns [org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element = null]
@init{
}
:
(
a0 = TEXT
{
if (terminateParsing) {
throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableCallExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
tokenResolver.setOptions(getOptions());
org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
String resolved = (String) resolvedObject;
org.coolsoftware.coolcomponents.expressions.variables.Variable proxy = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
collectHiddenTokens(element);
registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableCallExpressionReferredVariableReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), resolved, proxy);
if (proxy != null) {
Object value = proxy;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_31_0_0_0, proxy, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
}
}
)
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1171]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1172]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1173]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1174]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1175]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1176]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1177]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1178]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1179]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1180]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1181]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1182]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1183]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1184]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1185]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1186]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1187]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1188]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1189]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1190]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1191]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1192]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1193]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1194]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1195]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1196]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1197]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1198]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1199]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1200]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1201]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1202]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1203]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1204]);
addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1205]);
}

;

parse_org_coolsoftware_ecl_Import returns [org.coolsoftware.ecl.Import element = null]
:
c0 = parse_org_coolsoftware_ecl_CcmImport{ element = c0; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_ecl_EclContract returns [org.coolsoftware.ecl.EclContract element = null]
:
c0 = parse_org_coolsoftware_ecl_SWComponentContract{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_ecl_HWComponentContract{ element = c1; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_ecl_SWContractClause returns [org.coolsoftware.ecl.SWContractClause element = null]
:
c0 = parse_org_coolsoftware_ecl_ProvisionClause{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_ecl_SWComponentRequirementClause{ element = c1; /* this is a subclass or primitive expression choice */ }
|c2 = parse_org_coolsoftware_ecl_HWComponentRequirementClause{ element = c2; /* this is a subclass or primitive expression choice */ }
|c3 = parse_org_coolsoftware_ecl_SelfRequirementClause{ element = c3; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_ecl_HWContractClause returns [org.coolsoftware.ecl.HWContractClause element = null]
:
c0 = parse_org_coolsoftware_ecl_ProvisionClause{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_ecl_HWComponentRequirementClause{ element = c1; /* this is a subclass or primitive expression choice */ }

;

parse_org_coolsoftware_coolcomponents_expressions_Expression returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
:
c = parseop_Expression_level_03{ element = c; /* this rule is an expression root */ }

;

SL_COMMENT:
( '--'(~('\n'|'\r'|'\uffff'))* )
{ _channel = 99; }
;
ML_COMMENT:
( '/*'.*'*/')
{ _channel = 99; }
;
FILE_NAME:
( '[' (('A' .. 'Z' | 'a'..'z') ':' '/')? ('.' | '/') ('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' | '/' | '.' | ' ')+ ']' )
;
NUMBER:
( '0' | ('1'..'9')('0'..'9')+ )
;
INTEGER_LITERAL:
('-'?('0'..'9')+('e'('-')?('0'..'9')+)?)
;
WHITESPACE:
((' ' | '\t' | '\f'))
{ _channel = 99; }
;
LINEBREAK:
(('\r\n' | '\r' | '\n'))
{ _channel = 99; }
;
TYPE_LITERAL:
(('Boolean' | 'Integer' | 'Real' | 'String'))
;
QUOTED_91_93:
(('[')(~(']'))*(']'))
;
TEXT:
(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
;
REAL_LITERAL:
('-'?('0'..'9')+('e'('-')?('0'..'9')+)?'.''-'?('0'..'9')+('e'('-')?('0'..'9')+)?)
;
QUOTED_34_34:
(('"')(~('"'))*('"'))
;

