/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.mopp;

public class RequestParseResult implements org.coolsoftware.requests.resource.request.IRequestParseResult {
	
	private org.eclipse.emf.ecore.EObject root;
	private java.util.Collection<org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>> commands = new java.util.ArrayList<org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>>();
	
	public RequestParseResult() {
		super();
	}
	
	public void setRoot(org.eclipse.emf.ecore.EObject root) {
		this.root = root;
	}
	
	public org.eclipse.emf.ecore.EObject getRoot() {
		return root;
	}
	
	public java.util.Collection<org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
