/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.mopp;

/**
 * The RequestTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class RequestTokenResolverFactory implements org.coolsoftware.requests.resource.request.IRequestTokenResolverFactory {
	
	private java.util.Map<String, org.coolsoftware.requests.resource.request.IRequestTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, org.coolsoftware.requests.resource.request.IRequestTokenResolver> featureName2CollectInTokenResolver;
	private static org.coolsoftware.requests.resource.request.IRequestTokenResolver defaultResolver = new org.coolsoftware.requests.resource.request.analysis.RequestDefaultTokenResolver();
	
	public RequestTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, org.coolsoftware.requests.resource.request.IRequestTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, org.coolsoftware.requests.resource.request.IRequestTokenResolver>();
		registerCollectInTokenResolver("comments", new org.coolsoftware.requests.resource.request.analysis.RequestCOLLECT_commentsTokenResolver());
		registerCollectInTokenResolver("comments", new org.coolsoftware.requests.resource.request.analysis.RequestCOLLECT_commentsTokenResolver());
		registerTokenResolver("FILE_NAME", new org.coolsoftware.requests.resource.request.analysis.RequestFILE_NAMETokenResolver());
		registerTokenResolver("NUMBER", new org.coolsoftware.requests.resource.request.analysis.RequestNUMBERTokenResolver());
		registerTokenResolver("INTEGER_LITERAL", new org.coolsoftware.requests.resource.request.analysis.ExpINTEGER_LITERALTokenResolver());
		registerTokenResolver("TYPE_LITERAL", new org.coolsoftware.requests.resource.request.analysis.ExpTYPE_LITERALTokenResolver());
		registerTokenResolver("QUOTED_91_93", new org.coolsoftware.requests.resource.request.analysis.EclQUOTED_91_93TokenResolver());
		registerTokenResolver("TEXT", new org.coolsoftware.requests.resource.request.analysis.RequestTEXTTokenResolver());
		registerTokenResolver("REAL_LITERAL", new org.coolsoftware.requests.resource.request.analysis.ExpREAL_LITERALTokenResolver());
		registerTokenResolver("QUOTED_34_34", new org.coolsoftware.requests.resource.request.analysis.ExpQUOTED_34_34TokenResolver());
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, org.coolsoftware.requests.resource.request.IRequestTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, org.coolsoftware.requests.resource.request.IRequestTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected org.coolsoftware.requests.resource.request.IRequestTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private org.coolsoftware.requests.resource.request.IRequestTokenResolver internalCreateResolver(java.util.Map<String, org.coolsoftware.requests.resource.request.IRequestTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, org.coolsoftware.requests.resource.request.IRequestTokenResolver> resolverMap, String key, org.coolsoftware.requests.resource.request.IRequestTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
