/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.mopp;

public class RequestMetaInformation implements org.coolsoftware.requests.resource.request.IRequestMetaInformation {
	
	public String getSyntaxName() {
		return "request";
	}
	
	public String getURI() {
		return "http://www.cool-software.org/requests";
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTextScanner createLexer() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestAntlrScanner(new org.coolsoftware.requests.resource.request.mopp.RequestLexer());
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new org.coolsoftware.requests.resource.request.mopp.RequestParser().createInstance(inputStream, encoding);
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTextPrinter createPrinter(java.io.OutputStream outputStream, org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
		return new org.coolsoftware.requests.resource.request.mopp.RequestPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public org.coolsoftware.requests.resource.request.IRequestReferenceResolverSwitch getReferenceResolverSwitch() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestReferenceResolverSwitch();
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTokenResolverFactory getTokenResolverFactory() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "org.coolsoftware.requests/model/requests.cs";
	}
	
	public String[] getTokenNames() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestParser(null).getTokenNames();
	}
	
	public org.coolsoftware.requests.resource.request.IRequestTokenStyle getDefaultTokenStyle(String tokenName) {
		return new org.coolsoftware.requests.resource.request.mopp.RequestTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<org.coolsoftware.requests.resource.request.IRequestBracketPair> getBracketPairs() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestResourceFactory();
	}
	
	public org.coolsoftware.requests.resource.request.mopp.RequestNewFileContentProvider getNewFileContentProvider() {
		return new org.coolsoftware.requests.resource.request.mopp.RequestNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new org.coolsoftware.requests.resource.request.mopp.RequestResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "org.coolsoftware.requests.resource.request.ui.launchConfigurationType";
	}
	
	public org.coolsoftware.requests.resource.request.IRequestNameProvider createNameProvider() {
		return new org.coolsoftware.requests.resource.request.analysis.RequestDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		org.coolsoftware.requests.resource.request.mopp.RequestAntlrTokenHelper tokenHelper = new org.coolsoftware.requests.resource.request.mopp.RequestAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(org.coolsoftware.requests.resource.request.mopp.RequestTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
