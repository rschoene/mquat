/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 3.4

	package org.coolsoftware.requests.resource.request.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class RequestParser extends RequestANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "FILE_NAME", "INTEGER_LITERAL", "LINEBREAK", "ML_COMMENT", "NUMBER", "QUOTED_34_34", "QUOTED_91_93", "REAL_LITERAL", "SL_COMMENT", "TEXT", "TYPE_LITERAL", "WHITESPACE", "'!'", "'!='", "'('", "')'", "')>'", "'*'", "'+'", "'++'", "'+='", "'-'", "'--'", "'-='", "'.'", "'/'", "':'", "';'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'^'", "'and'", "'call'", "'ccm'", "'component'", "'contract'", "'cos'", "'expecting'", "'f'", "'false'", "'hardware'", "'implements'", "'implies'", "'import'", "'max:'", "'metaparameter:'", "'min:'", "'mode'", "'not'", "'or'", "'platform'", "'provides'", "'requires'", "'resource'", "'sin'", "'software'", "'target'", "'true'", "'var'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int FILE_NAME=4;
    public static final int INTEGER_LITERAL=5;
    public static final int LINEBREAK=6;
    public static final int ML_COMMENT=7;
    public static final int NUMBER=8;
    public static final int QUOTED_34_34=9;
    public static final int QUOTED_91_93=10;
    public static final int REAL_LITERAL=11;
    public static final int SL_COMMENT=12;
    public static final int TEXT=13;
    public static final int TYPE_LITERAL=14;
    public static final int WHITESPACE=15;

    // delegates
    public RequestANTLRParserBase[] getDelegates() {
        return new RequestANTLRParserBase[] {};
    }

    // delegators


    public RequestParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public RequestParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(114 + 1);
         

    }

    public String[] getTokenNames() { return RequestParser.tokenNames; }
    public String getGrammarFileName() { return "Request.g"; }


    	private org.coolsoftware.requests.resource.request.IRequestTokenResolverFactory tokenResolverFactory = new org.coolsoftware.requests.resource.request.mopp.RequestTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
    			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new org.coolsoftware.requests.resource.request.IRequestProblem() {
    					public org.coolsoftware.requests.resource.request.RequestEProblemSeverity getSeverity() {
    						return org.coolsoftware.requests.resource.request.RequestEProblemSeverity.ERROR;
    					}
    					public org.coolsoftware.requests.resource.request.RequestEProblemType getType() {
    						return org.coolsoftware.requests.resource.request.RequestEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<org.coolsoftware.requests.resource.request.IRequestQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		org.coolsoftware.requests.resource.request.IRequestExpectedElement terminal = org.coolsoftware.requests.resource.request.grammar.RequestFollowSetProvider.TERMINALS[terminalID];
    		org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[] containmentTrace = new org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentTrace[i - 2] = org.coolsoftware.requests.resource.request.grammar.RequestFollowSetProvider.LINKS[ids[i]];
    		}
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal expectedElement = new org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    		int currentPos = getTokenStream().index();
    		if (currentPos == 0) {
    			return;
    		}
    		int endPos = currentPos - 1;
    		for (; endPos >= this.lastPosition; endPos--) {
    			org.antlr.runtime3_4_0.Token token = getTokenStream().get(endPos);
    			int _channel = token.getChannel();
    			if (_channel != 99) {
    				break;
    			}
    		}
    		for (int pos = this.lastPosition; pos < endPos; pos++) {
    			org.antlr.runtime3_4_0.Token token = getTokenStream().get(pos);
    			int _channel = token.getChannel();
    			if (_channel == 99) {
    				if (token.getType() == RequestLexer.SL_COMMENT) {
    					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
    					if (feature != null) {
    						// call token resolver
    						org.coolsoftware.requests.resource.request.IRequestTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
    						resolvedResolver.setOptions(getOptions());
    						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult resolvedResult = getFreshTokenResolveResult();
    						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
    						Object resolvedObject = resolvedResult.getResolvedToken();
    						if (resolvedObject == null) {
    							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
    						}
    						if (java.lang.String.class.isInstance(resolvedObject)) {
    							addObjectToList(element, feature, resolvedObject);
    						} else {
    							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
    						}
    					} else {
    						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
    					}
    				}
    				if (token.getType() == RequestLexer.ML_COMMENT) {
    					org.eclipse.emf.ecore.EStructuralFeature feature = element.eClass().getEStructuralFeature("comments");
    					if (feature != null) {
    						// call token resolver
    						org.coolsoftware.requests.resource.request.IRequestTokenResolver resolvedResolver = tokenResolverFactory.createCollectInTokenResolver("comments");
    						resolvedResolver.setOptions(getOptions());
    						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult resolvedResult = getFreshTokenResolveResult();
    						resolvedResolver.resolve(token.getText(), feature, resolvedResult);
    						Object resolvedObject = resolvedResult.getResolvedToken();
    						if (resolvedObject == null) {
    							addErrorToResource(resolvedResult.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) token).getLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) token).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) token).getStopIndex());
    						}
    						if (java.lang.String.class.isInstance(resolvedObject)) {
    							addObjectToList(element, feature, resolvedObject);
    						} else {
    							System.out.println("WARNING: Attribute comments for token " + token + " has wrong type in element " + element + " (expected java.lang.String).");
    						}
    					} else {
    						System.out.println("WARNING: Attribute comments for token " + token + " was not found in element " + element + ".");
    					}
    				}
    			}
    		}
    		this.lastPosition = (endPos < 0 ? 0 : endPos);
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
    			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
    				org.coolsoftware.requests.resource.request.IRequestLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
    			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
    				org.coolsoftware.requests.resource.request.IRequestLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>() {
    			public boolean execute(org.coolsoftware.requests.resource.request.IRequestTextResource resource) {
    				org.coolsoftware.requests.resource.request.IRequestLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public org.coolsoftware.requests.resource.request.IRequestTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new RequestParser(new org.antlr.runtime3_4_0.CommonTokenStream(new RequestLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new RequestParser(new org.antlr.runtime3_4_0.CommonTokenStream(new RequestLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new org.coolsoftware.requests.resource.request.util.RequestRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public RequestParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((RequestLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((RequestLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == org.coolsoftware.requests.Request.class) {
    				return parse_org_coolsoftware_requests_Request();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.requests.MetaParamValue.class) {
    				return parse_org_coolsoftware_requests_MetaParamValue();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.requests.Import.class) {
    				return parse_org_coolsoftware_requests_Import();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.requests.Platform.class) {
    				return parse_org_coolsoftware_requests_Platform();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.EclFile.class) {
    				return parse_org_coolsoftware_ecl_EclFile();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.CcmImport.class) {
    				return parse_org_coolsoftware_ecl_CcmImport();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentContract.class) {
    				return parse_org_coolsoftware_ecl_SWComponentContract();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentContract.class) {
    				return parse_org_coolsoftware_ecl_HWComponentContract();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SWContractMode.class) {
    				return parse_org_coolsoftware_ecl_SWContractMode();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.HWContractMode.class) {
    				return parse_org_coolsoftware_ecl_HWContractMode();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.ProvisionClause.class) {
    				return parse_org_coolsoftware_ecl_ProvisionClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SWComponentRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_SWComponentRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.HWComponentRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_HWComponentRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.SelfRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_SelfRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.PropertyRequirementClause.class) {
    				return parse_org_coolsoftware_ecl_PropertyRequirementClause();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.FormulaTemplate.class) {
    				return parse_org_coolsoftware_ecl_FormulaTemplate();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.ecl.Metaparameter.class) {
    				return parse_org_coolsoftware_ecl_Metaparameter();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.Block.class) {
    				return parse_org_coolsoftware_coolcomponents_expressions_Block();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.variables.Variable.class) {
    				return parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();
    			}
    		}
    		throw new org.coolsoftware.requests.resource.request.mopp.RequestUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(org.coolsoftware.requests.resource.request.IRequestOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public org.coolsoftware.requests.resource.request.IRequestParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource>>();
    		org.coolsoftware.requests.resource.request.mopp.RequestParseResult parseResult = new org.coolsoftware.requests.resource.request.mopp.RequestParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.requests.resource.request.IRequestTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		org.coolsoftware.requests.resource.request.IRequestParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (org.coolsoftware.requests.resource.request.IRequestCommand<org.coolsoftware.requests.resource.request.IRequestTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal>();
    		java.util.List<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 154;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]> newFollowerPair : newFollowers) {
    							org.coolsoftware.requests.resource.request.IRequestExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal newFollowTerminal = new org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal(container, newFollower, followSetID, newFollowerPair.getRight());
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(org.coolsoftware.requests.resource.request.mopp.RequestExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Request.g:612:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_org_coolsoftware_requests_Request ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        org.coolsoftware.requests.Request c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Request.g:613:2: ( (c0= parse_org_coolsoftware_requests_Request ) EOF )
            // Request.g:614:2: (c0= parse_org_coolsoftware_requests_Request ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[0]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Request.g:619:2: (c0= parse_org_coolsoftware_requests_Request )
            // Request.g:620:3: c0= parse_org_coolsoftware_requests_Request
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_requests_Request_in_start82);
            c0=parse_org_coolsoftware_requests_Request();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_org_coolsoftware_requests_Request"
    // Request.g:628:1: parse_org_coolsoftware_requests_Request returns [org.coolsoftware.requests.Request element = null] : (a0_0= parse_org_coolsoftware_requests_Import ) (a1_0= parse_org_coolsoftware_requests_Platform ) a2= 'call' (a3= TEXT ) a4= '.' (a5= TEXT ) a6= 'expecting' a7= '{' ( (a8_0= parse_org_coolsoftware_requests_MetaParamValue ) )* ( (a9_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )* a10= '}' ;
    public final org.coolsoftware.requests.Request parse_org_coolsoftware_requests_Request() throws RecognitionException {
        org.coolsoftware.requests.Request element =  null;

        int parse_org_coolsoftware_requests_Request_StartIndex = input.index();

        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a10=null;
        org.coolsoftware.requests.Import a0_0 =null;

        org.coolsoftware.requests.Platform a1_0 =null;

        org.coolsoftware.requests.MetaParamValue a8_0 =null;

        org.coolsoftware.ecl.PropertyRequirementClause a9_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Request.g:631:2: ( (a0_0= parse_org_coolsoftware_requests_Import ) (a1_0= parse_org_coolsoftware_requests_Platform ) a2= 'call' (a3= TEXT ) a4= '.' (a5= TEXT ) a6= 'expecting' a7= '{' ( (a8_0= parse_org_coolsoftware_requests_MetaParamValue ) )* ( (a9_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )* a10= '}' )
            // Request.g:632:2: (a0_0= parse_org_coolsoftware_requests_Import ) (a1_0= parse_org_coolsoftware_requests_Platform ) a2= 'call' (a3= TEXT ) a4= '.' (a5= TEXT ) a6= 'expecting' a7= '{' ( (a8_0= parse_org_coolsoftware_requests_MetaParamValue ) )* ( (a9_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )* a10= '}'
            {
            // Request.g:632:2: (a0_0= parse_org_coolsoftware_requests_Import )
            // Request.g:633:3: a0_0= parse_org_coolsoftware_requests_Import
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_requests_Import_in_parse_org_coolsoftware_requests_Request119);
            a0_0=parse_org_coolsoftware_requests_Import();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            				startIncompleteElement(element);
            			}
            			if (a0_0 != null) {
            				if (a0_0 != null) {
            					Object value = a0_0;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__IMPORT), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_0, a0_0, true);
            				copyLocalizationInfos(a0_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1]);
            	}

            // Request.g:658:2: (a1_0= parse_org_coolsoftware_requests_Platform )
            // Request.g:659:3: a1_0= parse_org_coolsoftware_requests_Platform
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_requests_Platform_in_parse_org_coolsoftware_requests_Request141);
            a1_0=parse_org_coolsoftware_requests_Platform();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__HARDWARE), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[2]);
            	}

            a2=(Token)match(input,40,FOLLOW_40_in_parse_org_coolsoftware_requests_Request159); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[3]);
            	}

            // Request.g:698:2: (a3= TEXT )
            // Request.g:699:3: a3= TEXT
            {
            a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_requests_Request177); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__COMPONENT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Request, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRequestComponentReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__COMPONENT), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__COMPONENT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_6, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[4]);
            	}

            a4=(Token)match(input,28,FOLLOW_28_in_parse_org_coolsoftware_requests_Request198); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[5]);
            	}

            // Request.g:752:2: (a5= TEXT )
            // Request.g:753:3: a5= TEXT
            {
            a5=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_requests_Request216); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            				startIncompleteElement(element);
            			}
            			if (a5 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__TARGET), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.PortType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createPortType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Request, org.coolsoftware.coolcomponents.ccm.structure.PortType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getRequestTargetReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__TARGET), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.REQUEST__TARGET), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_8, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[6]);
            	}

            a6=(Token)match(input,45,FOLLOW_45_in_parse_org_coolsoftware_requests_Request237); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[7]);
            	}

            a7=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_requests_Request251); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_11, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[8]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[9]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[10]);
            	}

            // Request.g:822:2: ( (a8_0= parse_org_coolsoftware_requests_MetaParamValue ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==TEXT) ) {
                    int LA1_1 = input.LA(2);

                    if ( (LA1_1==34) ) {
                        alt1=1;
                    }


                }


                switch (alt1) {
            	case 1 :
            	    // Request.g:823:3: (a8_0= parse_org_coolsoftware_requests_MetaParamValue )
            	    {
            	    // Request.g:823:3: (a8_0= parse_org_coolsoftware_requests_MetaParamValue )
            	    // Request.g:824:4: a8_0= parse_org_coolsoftware_requests_MetaParamValue
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_requests_MetaParamValue_in_parse_org_coolsoftware_requests_Request274);
            	    a8_0=parse_org_coolsoftware_requests_MetaParamValue();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a8_0 != null) {
            	    					if (a8_0 != null) {
            	    						Object value = a8_0;
            	    						addObjectToList(element, org.coolsoftware.requests.RequestsPackage.REQUEST__META_PARAM_VALUES, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_13, a8_0, true);
            	    					copyLocalizationInfos(a8_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[11]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[12]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[13]);
            	}

            // Request.g:852:2: ( (a9_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==TEXT) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Request.g:853:3: (a9_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    {
            	    // Request.g:853:3: (a9_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    // Request.g:854:4: a9_0= parse_org_coolsoftware_ecl_PropertyRequirementClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_requests_Request309);
            	    a9_0=parse_org_coolsoftware_ecl_PropertyRequirementClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a9_0 != null) {
            	    					if (a9_0 != null) {
            	    						Object value = a9_0;
            	    						addObjectToList(element, org.coolsoftware.requests.RequestsPackage.REQUEST__REQS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_15, a9_0, true);
            	    					copyLocalizationInfos(a9_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[14]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[15]);
            	}

            a10=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_requests_Request335); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createRequest();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_0_0_0_17, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_org_coolsoftware_requests_Request_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_requests_Request"



    // $ANTLR start "parse_org_coolsoftware_requests_MetaParamValue"
    // Request.g:896:1: parse_org_coolsoftware_requests_MetaParamValue returns [org.coolsoftware.requests.MetaParamValue element = null] : (a0= TEXT ) a1= '=' (a2= NUMBER ) ;
    public final org.coolsoftware.requests.MetaParamValue parse_org_coolsoftware_requests_MetaParamValue() throws RecognitionException {
        org.coolsoftware.requests.MetaParamValue element =  null;

        int parse_org_coolsoftware_requests_MetaParamValue_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Request.g:899:2: ( (a0= TEXT ) a1= '=' (a2= NUMBER ) )
            // Request.g:900:2: (a0= TEXT ) a1= '=' (a2= NUMBER )
            {
            // Request.g:900:2: (a0= TEXT )
            // Request.g:901:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_requests_MetaParamValue368); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createMetaParamValue();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__METAPARAM), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.Parameter proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createParameter();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.MetaParamValue, org.coolsoftware.coolcomponents.ccm.structure.Parameter>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getMetaParamValueMetaparamReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__METAPARAM), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__METAPARAM), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_1_0_0_0, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[16]);
            	}

            a1=(Token)match(input,34,FOLLOW_34_in_parse_org_coolsoftware_requests_MetaParamValue389); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createMetaParamValue();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_1_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[17]);
            	}

            // Request.g:954:2: (a2= NUMBER )
            // Request.g:955:3: a2= NUMBER
            {
            a2=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_coolsoftware_requests_MetaParamValue407); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createMetaParamValue();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__VALUE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.META_PARAM_VALUE__VALUE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_1_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[18]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[19]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[20]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_org_coolsoftware_requests_MetaParamValue_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_requests_MetaParamValue"



    // $ANTLR start "parse_org_coolsoftware_requests_Import"
    // Request.g:994:1: parse_org_coolsoftware_requests_Import returns [org.coolsoftware.requests.Import element = null] : a0= 'import' a1= 'ccm' (a2= FILE_NAME ) ;
    public final org.coolsoftware.requests.Import parse_org_coolsoftware_requests_Import() throws RecognitionException {
        org.coolsoftware.requests.Import element =  null;

        int parse_org_coolsoftware_requests_Import_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Request.g:997:2: (a0= 'import' a1= 'ccm' (a2= FILE_NAME ) )
            // Request.g:998:2: a0= 'import' a1= 'ccm' (a2= FILE_NAME )
            {
            a0=(Token)match(input,51,FOLLOW_51_in_parse_org_coolsoftware_requests_Import443); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createImport();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_2_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[21]);
            	}

            a1=(Token)match(input,41,FOLLOW_41_in_parse_org_coolsoftware_requests_Import457); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createImport();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_2_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[22]);
            	}

            // Request.g:1026:2: (a2= FILE_NAME )
            // Request.g:1027:3: a2= FILE_NAME
            {
            a2=(Token)match(input,FILE_NAME,FOLLOW_FILE_NAME_in_parse_org_coolsoftware_requests_Import475); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createImport();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("FILE_NAME");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.IMPORT__MODEL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.StructuralModel proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createStructuralModel();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Import, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getImportModelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.IMPORT__MODEL), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.IMPORT__MODEL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_2_0_0_3, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[23]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_org_coolsoftware_requests_Import_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_requests_Import"



    // $ANTLR start "parse_org_coolsoftware_requests_Platform"
    // Request.g:1068:1: parse_org_coolsoftware_requests_Platform returns [org.coolsoftware.requests.Platform element = null] : a0= 'target' a1= 'platform' (a2= FILE_NAME ) ;
    public final org.coolsoftware.requests.Platform parse_org_coolsoftware_requests_Platform() throws RecognitionException {
        org.coolsoftware.requests.Platform element =  null;

        int parse_org_coolsoftware_requests_Platform_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Request.g:1071:2: (a0= 'target' a1= 'platform' (a2= FILE_NAME ) )
            // Request.g:1072:2: a0= 'target' a1= 'platform' (a2= FILE_NAME )
            {
            a0=(Token)match(input,64,FOLLOW_64_in_parse_org_coolsoftware_requests_Platform511); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createPlatform();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_3_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[24]);
            	}

            a1=(Token)match(input,58,FOLLOW_58_in_parse_org_coolsoftware_requests_Platform525); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createPlatform();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_3_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[25]);
            	}

            // Request.g:1100:2: (a2= FILE_NAME )
            // Request.g:1101:3: a2= FILE_NAME
            {
            a2=(Token)match(input,FILE_NAME,FOLLOW_FILE_NAME_in_parse_org_coolsoftware_requests_Platform543); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.requests.RequestsFactory.eINSTANCE.createPlatform();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("FILE_NAME");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.PLATFORM__HWMODEL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.variant.VariantModel proxy = org.coolsoftware.coolcomponents.ccm.variant.VariantFactory.eINSTANCE.createVariantModel();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.requests.Platform, org.coolsoftware.coolcomponents.ccm.variant.VariantModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPlatformHwmodelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.PLATFORM__HWMODEL), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.requests.RequestsPackage.PLATFORM__HWMODEL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.REQUEST_3_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[26]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_org_coolsoftware_requests_Platform_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_requests_Platform"



    // $ANTLR start "parse_org_coolsoftware_ecl_EclFile"
    // Request.g:1142:1: parse_org_coolsoftware_ecl_EclFile returns [org.coolsoftware.ecl.EclFile element = null] : ( (a0_0= parse_org_coolsoftware_ecl_Import ) )* ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )* ;
    public final org.coolsoftware.ecl.EclFile parse_org_coolsoftware_ecl_EclFile() throws RecognitionException {
        org.coolsoftware.ecl.EclFile element =  null;

        int parse_org_coolsoftware_ecl_EclFile_StartIndex = input.index();

        org.coolsoftware.ecl.Import a0_0 =null;

        org.coolsoftware.ecl.EclContract a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Request.g:1145:2: ( ( (a0_0= parse_org_coolsoftware_ecl_Import ) )* ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )* )
            // Request.g:1146:2: ( (a0_0= parse_org_coolsoftware_ecl_Import ) )* ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )*
            {
            // Request.g:1146:2: ( (a0_0= parse_org_coolsoftware_ecl_Import ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==51) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // Request.g:1147:3: (a0_0= parse_org_coolsoftware_ecl_Import )
            	    {
            	    // Request.g:1147:3: (a0_0= parse_org_coolsoftware_ecl_Import )
            	    // Request.g:1148:4: a0_0= parse_org_coolsoftware_ecl_Import
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Import_in_parse_org_coolsoftware_ecl_EclFile588);
            	    a0_0=parse_org_coolsoftware_ecl_Import();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a0_0 != null) {
            	    					if (a0_0 != null) {
            	    						Object value = a0_0;
            	    						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__IMPORTS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_0_0_0_0, a0_0, true);
            	    					copyLocalizationInfos(a0_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[27]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[28]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[29]);
            	}

            // Request.g:1176:2: ( (a1_0= parse_org_coolsoftware_ecl_EclContract ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==43) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // Request.g:1177:3: (a1_0= parse_org_coolsoftware_ecl_EclContract )
            	    {
            	    // Request.g:1177:3: (a1_0= parse_org_coolsoftware_ecl_EclContract )
            	    // Request.g:1178:4: a1_0= parse_org_coolsoftware_ecl_EclContract
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_EclContract_in_parse_org_coolsoftware_ecl_EclFile623);
            	    a1_0=parse_org_coolsoftware_ecl_EclContract();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createEclFile();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a1_0 != null) {
            	    					if (a1_0 != null) {
            	    						Object value = a1_0;
            	    						addObjectToList(element, org.coolsoftware.ecl.EclPackage.ECL_FILE__CONTRACTS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_0_0_0_2, a1_0, true);
            	    					copyLocalizationInfos(a1_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[30]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[31]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_org_coolsoftware_ecl_EclFile_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_EclFile"



    // $ANTLR start "parse_org_coolsoftware_ecl_CcmImport"
    // Request.g:1207:1: parse_org_coolsoftware_ecl_CcmImport returns [org.coolsoftware.ecl.CcmImport element = null] : a0= 'import' a1= 'ccm' (a2= QUOTED_91_93 ) ;
    public final org.coolsoftware.ecl.CcmImport parse_org_coolsoftware_ecl_CcmImport() throws RecognitionException {
        org.coolsoftware.ecl.CcmImport element =  null;

        int parse_org_coolsoftware_ecl_CcmImport_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Request.g:1210:2: (a0= 'import' a1= 'ccm' (a2= QUOTED_91_93 ) )
            // Request.g:1211:2: a0= 'import' a1= 'ccm' (a2= QUOTED_91_93 )
            {
            a0=(Token)match(input,51,FOLLOW_51_in_parse_org_coolsoftware_ecl_CcmImport664); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_1_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[32]);
            	}

            a1=(Token)match(input,41,FOLLOW_41_in_parse_org_coolsoftware_ecl_CcmImport678); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_1_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[33]);
            	}

            // Request.g:1239:2: (a2= QUOTED_91_93 )
            // Request.g:1240:3: a2= QUOTED_91_93
            {
            a2=(Token)match(input,QUOTED_91_93,FOLLOW_QUOTED_91_93_in_parse_org_coolsoftware_ecl_CcmImport696); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createCcmImport();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_91_93");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.StructuralModel proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createStructuralModel();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.CcmImport, org.coolsoftware.coolcomponents.ccm.structure.StructuralModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getCcmImportCcmStructuralModelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_1_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[34]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[35]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[36]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_org_coolsoftware_ecl_CcmImport_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_CcmImport"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWComponentContract"
    // Request.g:1283:1: parse_org_coolsoftware_ecl_SWComponentContract returns [org.coolsoftware.ecl.SWComponentContract element = null] : a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'software' (a4= TEXT ) a5= '.' (a6= TEXT ) a7= '{' ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+ a11= '}' ;
    public final org.coolsoftware.ecl.SWComponentContract parse_org_coolsoftware_ecl_SWComponentContract() throws RecognitionException {
        org.coolsoftware.ecl.SWComponentContract element =  null;

        int parse_org_coolsoftware_ecl_SWComponentContract_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a11=null;
        org.coolsoftware.ecl.Metaparameter a9_0 =null;

        org.coolsoftware.ecl.SWContractMode a10_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Request.g:1286:2: (a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'software' (a4= TEXT ) a5= '.' (a6= TEXT ) a7= '{' ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+ a11= '}' )
            // Request.g:1287:2: a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'software' (a4= TEXT ) a5= '.' (a6= TEXT ) a7= '{' ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+ a11= '}'
            {
            a0=(Token)match(input,43,FOLLOW_43_in_parse_org_coolsoftware_ecl_SWComponentContract732); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[37]);
            	}

            // Request.g:1301:2: (a1= TEXT )
            // Request.g:1302:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract750); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[38]);
            	}

            a2=(Token)match(input,49,FOLLOW_49_in_parse_org_coolsoftware_ecl_SWComponentContract771); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[39]);
            	}

            a3=(Token)match(input,63,FOLLOW_63_in_parse_org_coolsoftware_ecl_SWComponentContract785); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[40]);
            	}

            // Request.g:1365:2: (a4= TEXT )
            // Request.g:1366:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract803); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_8, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[41]);
            	}

            a5=(Token)match(input,28,FOLLOW_28_in_parse_org_coolsoftware_ecl_SWComponentContract824); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[42]);
            	}

            // Request.g:1419:2: (a6= TEXT )
            // Request.g:1420:3: a6= TEXT
            {
            a6=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract842); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a6 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.PortType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createPortType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.PortType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentContractPortReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__PORT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_10, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[43]);
            	}

            a7=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_ecl_SWComponentContract863); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_11, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[44]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[45]);
            	}

            // Request.g:1474:2: ( (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==53) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // Request.g:1475:3: (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    {
                    // Request.g:1475:3: (a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    // Request.g:1476:4: a8= 'metaparameter:' ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    {
                    a8=(Token)match(input,53,FOLLOW_53_in_parse_org_coolsoftware_ecl_SWComponentContract886); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_13_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[46]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[47]);
                    			}

                    // Request.g:1491:4: ( ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==TEXT) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // Request.g:1492:5: ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    {
                    	    // Request.g:1492:5: ( (a9_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    // Request.g:1493:6: (a9_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    {
                    	    // Request.g:1493:6: (a9_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    // Request.g:1494:7: a9_0= parse_org_coolsoftware_ecl_Metaparameter
                    	    {
                    	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_SWComponentContract927);
                    	    a9_0=parse_org_coolsoftware_ecl_Metaparameter();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a9_0 != null) {
                    	    								if (a9_0 != null) {
                    	    									Object value = a9_0;
                    	    									addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__METAPARAMS, value);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_13_0_0_1_0_0_0, a9_0, true);
                    	    								copyLocalizationInfos(a9_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[48]);
                    	    						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[49]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[50]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[51]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[52]);
            	}

            // Request.g:1535:2: ( ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) ) )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==55) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // Request.g:1536:3: ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) )
            	    {
            	    // Request.g:1536:3: ( (a10_0= parse_org_coolsoftware_ecl_SWContractMode ) )
            	    // Request.g:1537:4: (a10_0= parse_org_coolsoftware_ecl_SWContractMode )
            	    {
            	    // Request.g:1537:4: (a10_0= parse_org_coolsoftware_ecl_SWContractMode )
            	    // Request.g:1538:5: a10_0= parse_org_coolsoftware_ecl_SWContractMode
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWContractMode_in_parse_org_coolsoftware_ecl_SWComponentContract1016);
            	    a10_0=parse_org_coolsoftware_ecl_SWContractMode();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a10_0 != null) {
            	    						if (a10_0 != null) {
            	    							Object value = a10_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_CONTRACT__MODES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_15_0_0_0, a10_0, true);
            	    						copyLocalizationInfos(a10_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[53]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[54]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[55]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[56]);
            	}

            a11=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_ecl_SWComponentContract1057); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_2_0_0_16, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a11, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[57]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[58]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_org_coolsoftware_ecl_SWComponentContract_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWComponentContract"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWComponentContract"
    // Request.g:1589:1: parse_org_coolsoftware_ecl_HWComponentContract returns [org.coolsoftware.ecl.HWComponentContract element = null] : a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'hardware' (a4= TEXT ) a5= '{' ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+ a9= '}' ;
    public final org.coolsoftware.ecl.HWComponentContract parse_org_coolsoftware_ecl_HWComponentContract() throws RecognitionException {
        org.coolsoftware.ecl.HWComponentContract element =  null;

        int parse_org_coolsoftware_ecl_HWComponentContract_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a9=null;
        org.coolsoftware.ecl.Metaparameter a7_0 =null;

        org.coolsoftware.ecl.HWContractMode a8_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Request.g:1592:2: (a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'hardware' (a4= TEXT ) a5= '{' ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+ a9= '}' )
            // Request.g:1593:2: a0= 'contract' (a1= TEXT ) a2= 'implements' a3= 'hardware' (a4= TEXT ) a5= '{' ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )? ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+ a9= '}'
            {
            a0=(Token)match(input,43,FOLLOW_43_in_parse_org_coolsoftware_ecl_HWComponentContract1086); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[59]);
            	}

            // Request.g:1607:2: (a1= TEXT )
            // Request.g:1608:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract1104); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[60]);
            	}

            a2=(Token)match(input,49,FOLLOW_49_in_parse_org_coolsoftware_ecl_HWComponentContract1125); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[61]);
            	}

            a3=(Token)match(input,48,FOLLOW_48_in_parse_org_coolsoftware_ecl_HWComponentContract1139); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[62]);
            	}

            // Request.g:1671:2: (a4= TEXT )
            // Request.g:1672:3: a4= TEXT
            {
            a4=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract1157); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentContract, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentContractComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__COMPONENT_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_8, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[63]);
            	}

            a5=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_ecl_HWComponentContract1178); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[64]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[65]);
            	}

            // Request.g:1726:2: ( (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==53) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // Request.g:1727:3: (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    {
                    // Request.g:1727:3: (a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )* )
                    // Request.g:1728:4: a6= 'metaparameter:' ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    {
                    a6=(Token)match(input,53,FOLLOW_53_in_parse_org_coolsoftware_ecl_HWComponentContract1201); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_12_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[66]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[67]);
                    			}

                    // Request.g:1743:4: ( ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) ) )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==TEXT) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // Request.g:1744:5: ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    {
                    	    // Request.g:1744:5: ( (a7_0= parse_org_coolsoftware_ecl_Metaparameter ) )
                    	    // Request.g:1745:6: (a7_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    {
                    	    // Request.g:1745:6: (a7_0= parse_org_coolsoftware_ecl_Metaparameter )
                    	    // Request.g:1746:7: a7_0= parse_org_coolsoftware_ecl_Metaparameter
                    	    {
                    	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_HWComponentContract1242);
                    	    a7_0=parse_org_coolsoftware_ecl_Metaparameter();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a7_0 != null) {
                    	    								if (a7_0 != null) {
                    	    									Object value = a7_0;
                    	    									addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__METAPARAMS, value);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_12_0_0_1_0_0_0, a7_0, true);
                    	    								copyLocalizationInfos(a7_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[68]);
                    	    						addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[69]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[70]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[71]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[72]);
            	}

            // Request.g:1787:2: ( ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==55) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // Request.g:1788:3: ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) )
            	    {
            	    // Request.g:1788:3: ( (a8_0= parse_org_coolsoftware_ecl_HWContractMode ) )
            	    // Request.g:1789:4: (a8_0= parse_org_coolsoftware_ecl_HWContractMode )
            	    {
            	    // Request.g:1789:4: (a8_0= parse_org_coolsoftware_ecl_HWContractMode )
            	    // Request.g:1790:5: a8_0= parse_org_coolsoftware_ecl_HWContractMode
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWContractMode_in_parse_org_coolsoftware_ecl_HWComponentContract1331);
            	    a8_0=parse_org_coolsoftware_ecl_HWContractMode();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a8_0 != null) {
            	    						if (a8_0 != null) {
            	    							Object value = a8_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_CONTRACT__MODES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_14_0_0_0, a8_0, true);
            	    						copyLocalizationInfos(a8_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[73]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[74]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[75]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[76]);
            	}

            a9=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_ecl_HWComponentContract1372); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentContract();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_3_0_0_15, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[77]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[78]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_org_coolsoftware_ecl_HWComponentContract_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWComponentContract"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWContractMode"
    // Request.g:1841:1: parse_org_coolsoftware_ecl_SWContractMode returns [org.coolsoftware.ecl.SWContractMode element = null] : a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+ a4= '}' ;
    public final org.coolsoftware.ecl.SWContractMode parse_org_coolsoftware_ecl_SWContractMode() throws RecognitionException {
        org.coolsoftware.ecl.SWContractMode element =  null;

        int parse_org_coolsoftware_ecl_SWContractMode_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.ecl.SWContractClause a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Request.g:1844:2: (a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+ a4= '}' )
            // Request.g:1845:2: a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+ a4= '}'
            {
            a0=(Token)match(input,55,FOLLOW_55_in_parse_org_coolsoftware_ecl_SWContractMode1401); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[79]);
            	}

            // Request.g:1859:2: (a1= TEXT )
            // Request.g:1860:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWContractMode1419); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[80]);
            	}

            a2=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_ecl_SWContractMode1440); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[81]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[82]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[83]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[84]);
            	}

            // Request.g:1912:2: ( ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) ) )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0 >= 59 && LA11_0 <= 60)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // Request.g:1913:3: ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) )
            	    {
            	    // Request.g:1913:3: ( (a3_0= parse_org_coolsoftware_ecl_SWContractClause ) )
            	    // Request.g:1914:4: (a3_0= parse_org_coolsoftware_ecl_SWContractClause )
            	    {
            	    // Request.g:1914:4: (a3_0= parse_org_coolsoftware_ecl_SWContractClause )
            	    // Request.g:1915:5: a3_0= parse_org_coolsoftware_ecl_SWContractClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWContractClause_in_parse_org_coolsoftware_ecl_SWContractMode1469);
            	    a3_0=parse_org_coolsoftware_ecl_SWContractClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_CONTRACT_MODE__CLAUSES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_6_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[85]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[86]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[87]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[88]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[89]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[90]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[91]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[92]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[93]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[94]);
            	}

            a4=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_ecl_SWContractMode1510); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_4_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[95]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[96]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_org_coolsoftware_ecl_SWContractMode_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWContractMode"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWContractMode"
    // Request.g:1972:1: parse_org_coolsoftware_ecl_HWContractMode returns [org.coolsoftware.ecl.HWContractMode element = null] : a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+ a4= '}' ;
    public final org.coolsoftware.ecl.HWContractMode parse_org_coolsoftware_ecl_HWContractMode() throws RecognitionException {
        org.coolsoftware.ecl.HWContractMode element =  null;

        int parse_org_coolsoftware_ecl_HWContractMode_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.ecl.HWContractClause a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Request.g:1975:2: (a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+ a4= '}' )
            // Request.g:1976:2: a0= 'mode' (a1= TEXT ) a2= '{' ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+ a4= '}'
            {
            a0=(Token)match(input,55,FOLLOW_55_in_parse_org_coolsoftware_ecl_HWContractMode1539); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[97]);
            	}

            // Request.g:1990:2: (a1= TEXT )
            // Request.g:1991:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWContractMode1557); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[98]);
            	}

            a2=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_ecl_HWContractMode1578); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[99]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[100]);
            	}

            // Request.g:2041:2: ( ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) ) )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0 >= 59 && LA12_0 <= 60)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // Request.g:2042:3: ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) )
            	    {
            	    // Request.g:2042:3: ( (a3_0= parse_org_coolsoftware_ecl_HWContractClause ) )
            	    // Request.g:2043:4: (a3_0= parse_org_coolsoftware_ecl_HWContractClause )
            	    {
            	    // Request.g:2043:4: (a3_0= parse_org_coolsoftware_ecl_HWContractClause )
            	    // Request.g:2044:5: a3_0= parse_org_coolsoftware_ecl_HWContractClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWContractClause_in_parse_org_coolsoftware_ecl_HWContractMode1607);
            	    a3_0=parse_org_coolsoftware_ecl_HWContractClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_CONTRACT_MODE__CLAUSES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_6_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[101]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[102]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[103]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[104]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[105]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[106]);
            	}

            a4=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_ecl_HWContractMode1648); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWContractMode();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_5_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[107]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[108]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_org_coolsoftware_ecl_HWContractMode_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWContractMode"



    // $ANTLR start "parse_org_coolsoftware_ecl_ProvisionClause"
    // Request.g:2097:1: parse_org_coolsoftware_ecl_ProvisionClause returns [org.coolsoftware.ecl.ProvisionClause element = null] : a0= 'provides' (a1= TEXT ) ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? ;
    public final org.coolsoftware.ecl.ProvisionClause parse_org_coolsoftware_ecl_ProvisionClause() throws RecognitionException {
        org.coolsoftware.ecl.ProvisionClause element =  null;

        int parse_org_coolsoftware_ecl_ProvisionClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.coolcomponents.expressions.Expression a3_0 =null;

        org.coolsoftware.coolcomponents.expressions.Expression a5_0 =null;

        org.coolsoftware.ecl.FormulaTemplate a6_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Request.g:2100:2: (a0= 'provides' (a1= TEXT ) ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? )
            // Request.g:2101:2: a0= 'provides' (a1= TEXT ) ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            {
            a0=(Token)match(input,59,FOLLOW_59_in_parse_org_coolsoftware_ecl_ProvisionClause1677); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[109]);
            	}

            // Request.g:2115:2: (a1= TEXT )
            // Request.g:2116:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_ProvisionClause1695); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.ProvisionClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getProvisionClauseProvidedPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__PROVIDED_PROPERTY), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_2, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[110]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[111]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[112]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[113]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[114]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[115]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[116]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[117]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[118]);
            	}

            // Request.g:2163:2: ( (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==54) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // Request.g:2164:3: (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Request.g:2164:3: (a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Request.g:2165:4: a2= 'min:' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a2=(Token)match(input,54,FOLLOW_54_in_parse_org_coolsoftware_ecl_ProvisionClause1725); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_4_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[119]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[120]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[121]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[122]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[123]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[124]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[125]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[126]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[127]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[128]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[129]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[130]);
                    			}

                    // Request.g:2190:4: (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Request.g:2191:5: a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1751);
                    a3_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a3_0 != null) {
                    						if (a3_0 != null) {
                    							Object value = a3_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MIN_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_4_0_0_2, a3_0, true);
                    						copyLocalizationInfos(a3_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[131]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[132]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[133]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[134]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[135]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[136]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[137]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[138]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[139]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[140]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[141]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[142]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[143]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[144]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[145]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[146]);
            	}

            // Request.g:2237:2: ( (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==52) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // Request.g:2238:3: (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Request.g:2238:3: (a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Request.g:2239:4: a4= 'max:' (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a4=(Token)match(input,52,FOLLOW_52_in_parse_org_coolsoftware_ecl_ProvisionClause1801); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_5_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[147]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[148]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[149]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[150]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[151]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[152]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[153]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[154]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[155]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[156]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[157]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[158]);
                    			}

                    // Request.g:2264:4: (a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Request.g:2265:5: a5_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1827);
                    a5_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a5_0 != null) {
                    						if (a5_0 != null) {
                    							Object value = a5_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__MAX_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_5_0_0_2, a5_0, true);
                    						copyLocalizationInfos(a5_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[159]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[160]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[161]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[162]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[163]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[164]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[165]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[166]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[167]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[168]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[169]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[170]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[171]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[172]);
            	}

            // Request.g:2309:2: ( (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==32) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // Request.g:2310:3: (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    {
                    // Request.g:2310:3: (a6_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    // Request.g:2311:4: a6_0= parse_org_coolsoftware_ecl_FormulaTemplate
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_ProvisionClause1877);
                    a6_0=parse_org_coolsoftware_ecl_FormulaTemplate();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createProvisionClause();
                    					startIncompleteElement(element);
                    				}
                    				if (a6_0 != null) {
                    					if (a6_0 != null) {
                    						Object value = a6_0;
                    						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROVISION_CLAUSE__FORMULA), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_6_0_0_6, a6_0, true);
                    					copyLocalizationInfos(a6_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[173]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[174]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[175]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[176]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[177]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[178]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_org_coolsoftware_ecl_ProvisionClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_ProvisionClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWComponentRequirementClause"
    // Request.g:2344:1: parse_org_coolsoftware_ecl_SWComponentRequirementClause returns [org.coolsoftware.ecl.SWComponentRequirementClause element = null] : a0= 'requires' a1= 'component' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= '}' ;
    public final org.coolsoftware.ecl.SWComponentRequirementClause parse_org_coolsoftware_ecl_SWComponentRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.SWComponentRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_SWComponentRequirementClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a5=null;
        org.coolsoftware.ecl.PropertyRequirementClause a4_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Request.g:2347:2: (a0= 'requires' a1= 'component' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= '}' )
            // Request.g:2348:2: a0= 'requires' a1= 'component' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= '}'
            {
            a0=(Token)match(input,60,FOLLOW_60_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1918); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[179]);
            	}

            a1=(Token)match(input,42,FOLLOW_42_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1932); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[180]);
            	}

            // Request.g:2376:2: (a2= TEXT )
            // Request.g:2377:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1950); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.SWComponentType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createSWComponentType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.SWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.SWComponentType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getSWComponentRequirementClauseRequiredComponentTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[181]);
            	}

            a3=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1971); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[182]);
            	}

            // Request.g:2430:2: ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==TEXT) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // Request.g:2431:3: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            	    {
            	    // Request.g:2431:3: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            	    // Request.g:2432:4: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    {
            	    // Request.g:2432:4: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    // Request.g:2433:5: a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause2000);
            	    a4_0=parse_org_coolsoftware_ecl_PropertyRequirementClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a4_0 != null) {
            	    						if (a4_0 != null) {
            	    							Object value = a4_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_8_0_0_0, a4_0, true);
            	    						copyLocalizationInfos(a4_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[183]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[184]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[185]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[186]);
            	}

            a5=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause2041); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_7_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[187]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[188]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[189]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[190]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[191]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_org_coolsoftware_ecl_SWComponentRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWComponentRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWComponentRequirementClause"
    // Request.g:2487:1: parse_org_coolsoftware_ecl_HWComponentRequirementClause returns [org.coolsoftware.ecl.HWComponentRequirementClause element = null] : a0= 'requires' a1= 'resource' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= '}' ;
    public final org.coolsoftware.ecl.HWComponentRequirementClause parse_org_coolsoftware_ecl_HWComponentRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.HWComponentRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_HWComponentRequirementClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a5=null;
        org.coolsoftware.ecl.PropertyRequirementClause a4_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Request.g:2490:2: (a0= 'requires' a1= 'resource' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= '}' )
            // Request.g:2491:2: a0= 'requires' a1= 'resource' (a2= TEXT ) a3= '{' ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+ a5= '}'
            {
            a0=(Token)match(input,60,FOLLOW_60_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2070); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[192]);
            	}

            a1=(Token)match(input,61,FOLLOW_61_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2084); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[193]);
            	}

            // Request.g:2519:2: (a2= TEXT )
            // Request.g:2520:3: a2= TEXT
            {
            a2=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2102); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.ResourceType proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createResourceType();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.HWComponentRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.ResourceType>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getHWComponentRequirementClauseRequiredResourceTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_4, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[194]);
            	}

            a3=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2123); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[195]);
            	}

            // Request.g:2573:2: ( ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ) )+
            int cnt17=0;
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==TEXT) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // Request.g:2574:3: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            	    {
            	    // Request.g:2574:3: ( (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            	    // Request.g:2575:4: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    {
            	    // Request.g:2575:4: (a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            	    // Request.g:2576:5: a4_0= parse_org_coolsoftware_ecl_PropertyRequirementClause
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2152);
            	    a4_0=parse_org_coolsoftware_ecl_PropertyRequirementClause();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a4_0 != null) {
            	    						if (a4_0 != null) {
            	    							Object value = a4_0;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_8_0_0_0, a4_0, true);
            	    						copyLocalizationInfos(a4_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[196]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[197]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt17 >= 1 ) break loop17;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(17, input);
                        throw eee;
                }
                cnt17++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[198]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[199]);
            	}

            a5=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2193); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createHWComponentRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_8_0_0_9, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[200]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[201]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[202]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[203]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[204]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[205]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_org_coolsoftware_ecl_HWComponentRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWComponentRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_SelfRequirementClause"
    // Request.g:2631:1: parse_org_coolsoftware_ecl_SelfRequirementClause returns [org.coolsoftware.ecl.SelfRequirementClause element = null] : a0= 'requires' (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) ;
    public final org.coolsoftware.ecl.SelfRequirementClause parse_org_coolsoftware_ecl_SelfRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.SelfRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_SelfRequirementClause_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.ecl.PropertyRequirementClause a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return element; }

            // Request.g:2634:2: (a0= 'requires' (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause ) )
            // Request.g:2635:2: a0= 'requires' (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            {
            a0=(Token)match(input,60,FOLLOW_60_in_parse_org_coolsoftware_ecl_SelfRequirementClause2222); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_9_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[206]);
            	}

            // Request.g:2649:2: (a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause )
            // Request.g:2650:3: a1_0= parse_org_coolsoftware_ecl_PropertyRequirementClause
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SelfRequirementClause2240);
            a1_0=parse_org_coolsoftware_ecl_PropertyRequirementClause();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createSelfRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_9_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[207]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[208]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[209]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[210]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[211]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 15, parse_org_coolsoftware_ecl_SelfRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SelfRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_PropertyRequirementClause"
    // Request.g:2681:1: parse_org_coolsoftware_ecl_PropertyRequirementClause returns [org.coolsoftware.ecl.PropertyRequirementClause element = null] : (a0= TEXT ) ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? ;
    public final org.coolsoftware.ecl.PropertyRequirementClause parse_org_coolsoftware_ecl_PropertyRequirementClause() throws RecognitionException {
        org.coolsoftware.ecl.PropertyRequirementClause element =  null;

        int parse_org_coolsoftware_ecl_PropertyRequirementClause_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a2_0 =null;

        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;

        org.coolsoftware.ecl.FormulaTemplate a5_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return element; }

            // Request.g:2684:2: ( (a0= TEXT ) ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )? )
            // Request.g:2685:2: (a0= TEXT ) ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            {
            // Request.g:2685:2: (a0= TEXT )
            // Request.g:2686:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2277); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.coolsoftware.coolcomponents.ccm.structure.Property proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createProperty();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.PropertyRequirementClause, org.coolsoftware.coolcomponents.ccm.structure.Property>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getPropertyRequirementClauseRequiredPropertyReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_0, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[212]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[213]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[214]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[215]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[216]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[217]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[218]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[219]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[220]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[221]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[222]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[223]);
            	}

            // Request.g:2736:2: ( (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==54) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // Request.g:2737:3: (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Request.g:2737:3: (a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Request.g:2738:4: a1= 'min:' (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a1=(Token)match(input,54,FOLLOW_54_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2307); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_2_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[224]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[225]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[226]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[227]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[228]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[229]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[230]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[231]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[232]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[233]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[234]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[235]);
                    			}

                    // Request.g:2763:4: (a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Request.g:2764:5: a2_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2333);
                    a2_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a2_0 != null) {
                    						if (a2_0 != null) {
                    							Object value = a2_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_2_0_0_2, a2_0, true);
                    						copyLocalizationInfos(a2_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[236]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[237]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[238]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[239]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[240]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[241]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[242]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[243]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[244]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[245]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[246]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[247]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[248]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[249]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[250]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[251]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[252]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[253]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[254]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[255]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[256]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[257]);
            	}

            // Request.g:2816:2: ( (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==52) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // Request.g:2817:3: (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Request.g:2817:3: (a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Request.g:2818:4: a3= 'max:' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a3=(Token)match(input,52,FOLLOW_52_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2383); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[258]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[259]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[260]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[261]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[262]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[263]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[264]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[265]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[266]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[267]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[268]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[269]);
                    			}

                    // Request.g:2843:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Request.g:2844:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2409);
                    a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    						startIncompleteElement(element);
                    					}
                    					if (a4_0 != null) {
                    						if (a4_0 != null) {
                    							Object value = a4_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_3_0_0_2, a4_0, true);
                    						copyLocalizationInfos(a4_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[270]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[271]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[272]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[273]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[274]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[275]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[276]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[277]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[278]);
                    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[279]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[280]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[281]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[282]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[283]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[284]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[285]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[286]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[287]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[288]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[289]);
            	}

            // Request.g:2894:2: ( (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==32) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // Request.g:2895:3: (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    {
                    // Request.g:2895:3: (a5_0= parse_org_coolsoftware_ecl_FormulaTemplate )
                    // Request.g:2896:4: a5_0= parse_org_coolsoftware_ecl_FormulaTemplate
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2459);
                    a5_0=parse_org_coolsoftware_ecl_FormulaTemplate();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (terminateParsing) {
                    					throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    				}
                    				if (element == null) {
                    					element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createPropertyRequirementClause();
                    					startIncompleteElement(element);
                    				}
                    				if (a5_0 != null) {
                    					if (a5_0 != null) {
                    						Object value = a5_0;
                    						element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA), value);
                    						completedElement(value, true);
                    					}
                    					collectHiddenTokens(element);
                    					retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_10_0_0_4, a5_0, true);
                    					copyLocalizationInfos(a5_0, element);
                    				}
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[290]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[291]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[292]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[293]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[294]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[295]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[296]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[297]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[298]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 16, parse_org_coolsoftware_ecl_PropertyRequirementClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_PropertyRequirementClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_FormulaTemplate"
    // Request.g:2932:1: parse_org_coolsoftware_ecl_FormulaTemplate returns [org.coolsoftware.ecl.FormulaTemplate element = null] : a0= '<' (a1= TEXT ) a2= '(' ( ( (a3= TEXT ) ) )* a4= ')>' ;
    public final org.coolsoftware.ecl.FormulaTemplate parse_org_coolsoftware_ecl_FormulaTemplate() throws RecognitionException {
        org.coolsoftware.ecl.FormulaTemplate element =  null;

        int parse_org_coolsoftware_ecl_FormulaTemplate_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return element; }

            // Request.g:2935:2: (a0= '<' (a1= TEXT ) a2= '(' ( ( (a3= TEXT ) ) )* a4= ')>' )
            // Request.g:2936:2: a0= '<' (a1= TEXT ) a2= '(' ( ( (a3= TEXT ) ) )* a4= ')>'
            {
            a0=(Token)match(input,32,FOLLOW_32_in_parse_org_coolsoftware_ecl_FormulaTemplate2500); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[299]);
            	}

            // Request.g:2950:2: (a1= TEXT )
            // Request.g:2951:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2518); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[300]);
            	}

            a2=(Token)match(input,18,FOLLOW_18_in_parse_org_coolsoftware_ecl_FormulaTemplate2539); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[301]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[302]);
            	}

            // Request.g:3001:2: ( ( (a3= TEXT ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==TEXT) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // Request.g:3002:3: ( (a3= TEXT ) )
            	    {
            	    // Request.g:3002:3: ( (a3= TEXT ) )
            	    // Request.g:3003:4: (a3= TEXT )
            	    {
            	    // Request.g:3003:4: (a3= TEXT )
            	    // Request.g:3004:5: a3= TEXT
            	    {
            	    a3=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2568); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3 != null) {
            	    						org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            	    						tokenResolver.setOptions(getOptions());
            	    						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            	    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), result);
            	    						Object resolvedObject = result.getResolvedToken();
            	    						if (resolvedObject == null) {
            	    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            	    						}
            	    						String resolved = (String) resolvedObject;
            	    						org.coolsoftware.coolcomponents.ccm.structure.Parameter proxy = org.coolsoftware.coolcomponents.ccm.structure.StructureFactory.eINSTANCE.createParameter();
            	    						collectHiddenTokens(element);
            	    						registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.ecl.FormulaTemplate, org.coolsoftware.coolcomponents.ccm.structure.Parameter>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getFormulaTemplateMetaparameterReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER), resolved, proxy);
            	    						if (proxy != null) {
            	    							Object value = proxy;
            	    							addObjectToList(element, org.coolsoftware.ecl.EclPackage.FORMULA_TEMPLATE__METAPARAMETER, value);
            	    							completedElement(value, false);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_3_0_0_0, proxy, true);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[303]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[304]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[305]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[306]);
            	}

            a4=(Token)match(input,20,FOLLOW_20_in_parse_org_coolsoftware_ecl_FormulaTemplate2614); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createFormulaTemplate();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_11_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[307]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[308]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[309]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[310]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[311]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[312]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[313]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[314]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[315]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[316]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 17, parse_org_coolsoftware_ecl_FormulaTemplate_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_FormulaTemplate"



    // $ANTLR start "parse_org_coolsoftware_ecl_Metaparameter"
    // Request.g:3077:1: parse_org_coolsoftware_ecl_Metaparameter returns [org.coolsoftware.ecl.Metaparameter element = null] : (a0= TEXT ) ;
    public final org.coolsoftware.ecl.Metaparameter parse_org_coolsoftware_ecl_Metaparameter() throws RecognitionException {
        org.coolsoftware.ecl.Metaparameter element =  null;

        int parse_org_coolsoftware_ecl_Metaparameter_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return element; }

            // Request.g:3080:2: ( (a0= TEXT ) )
            // Request.g:3081:2: (a0= TEXT )
            {
            // Request.g:3081:2: (a0= TEXT )
            // Request.g:3082:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_Metaparameter2647); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.ecl.EclFactory.eINSTANCE.createMetaparameter();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.ecl.EclPackage.METAPARAMETER__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.ECL_12_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[317]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[318]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[319]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[320]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[321]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[322]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[323]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[324]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[325]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[326]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[327]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[328]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[329]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[330]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[331]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[332]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[333]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[334]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[335]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[336]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[337]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[338]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[339]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[340]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[341]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[342]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[343]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[344]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[345]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[346]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[347]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[348]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[349]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[350]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[351]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[352]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[353]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[354]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 18, parse_org_coolsoftware_ecl_Metaparameter_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_Metaparameter"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_Block"
    // Request.g:3156:1: parse_org_coolsoftware_coolcomponents_expressions_Block returns [org.coolsoftware.coolcomponents.expressions.Block element = null] : a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}' ;
    public final org.coolsoftware.coolcomponents.expressions.Block parse_org_coolsoftware_coolcomponents_expressions_Block() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Block element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_Block_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;

        org.coolsoftware.coolcomponents.expressions.Expression a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return element; }

            // Request.g:3159:2: (a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}' )
            // Request.g:3160:2: a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}'
            {
            a0=(Token)match(input,67,FOLLOW_67_in_parse_org_coolsoftware_coolcomponents_expressions_Block2683); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[355]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[356]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[357]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[358]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[359]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[360]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[361]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[362]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[363]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[364]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[365]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[366]);
            	}

            // Request.g:3185:2: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            // Request.g:3186:3: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2701);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[367]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[368]);
            	}

            // Request.g:3212:2: ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==31) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // Request.g:3213:3: (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
            	    {
            	    // Request.g:3213:3: (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
            	    // Request.g:3214:4: a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            	    {
            	    a2=(Token)match(input,31,FOLLOW_31_in_parse_org_coolsoftware_coolcomponents_expressions_Block2728); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_4_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[369]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[370]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[371]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[372]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[373]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[374]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[375]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[376]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[377]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[378]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[379]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[380]);
            	    			}

            	    // Request.g:3239:4: (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            	    // Request.g:3240:5: a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2754);
            	    a3_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_4_0_0_2, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[381]);
            	    				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[382]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[383]);
            		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[384]);
            	}

            a4=(Token)match(input,68,FOLLOW_68_in_parse_org_coolsoftware_coolcomponents_expressions_Block2795); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_0_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 19, parse_org_coolsoftware_coolcomponents_expressions_Block_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_Block"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_Variable"
    // Request.g:3289:1: parse_org_coolsoftware_coolcomponents_expressions_variables_Variable returns [org.coolsoftware.coolcomponents.expressions.variables.Variable element = null] : ( (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? |c0= parse_org_coolsoftware_ecl_Metaparameter );
    public final org.coolsoftware.coolcomponents.expressions.variables.Variable parse_org_coolsoftware_coolcomponents_expressions_variables_Variable() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.Variable element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;

        org.coolsoftware.ecl.Metaparameter c0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return element; }

            // Request.g:3292:2: ( (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? |c0= parse_org_coolsoftware_ecl_Metaparameter )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==TEXT) ) {
                int LA25_1 = input.LA(2);

                if ( (synpred25_Request()) ) {
                    alt25=1;
                }
                else if ( (true) ) {
                    alt25=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 25, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;

            }
            switch (alt25) {
                case 1 :
                    // Request.g:3293:2: (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
                    {
                    // Request.g:3293:2: (a0= TEXT )
                    // Request.g:3294:3: a0= TEXT
                    {
                    a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2828); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (terminateParsing) {
                    				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    			}
                    			if (element == null) {
                    				element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                    				startIncompleteElement(element);
                    			}
                    			if (a0 != null) {
                    				org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
                    				tokenResolver.setOptions(getOptions());
                    				org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
                    				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), result);
                    				Object resolvedObject = result.getResolvedToken();
                    				if (resolvedObject == null) {
                    					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
                    				}
                    				java.lang.String resolved = (java.lang.String) resolvedObject;
                    				if (resolved != null) {
                    					Object value = resolved;
                    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), value);
                    					completedElement(value, false);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_0, resolved, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
                    			}
                    		}

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[385]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[386]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[387]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[388]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[389]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[390]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[391]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[392]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[393]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[394]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[395]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[396]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[397]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[398]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[399]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[400]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[401]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[402]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[403]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[404]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[405]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[406]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[407]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[408]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[409]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[410]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[411]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[412]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[413]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[414]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[415]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[416]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[417]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[418]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[419]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[420]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[421]);
                    	}

                    // Request.g:3365:2: ( (a1= ':' (a2= TYPE_LITERAL ) ) )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==30) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // Request.g:3366:3: (a1= ':' (a2= TYPE_LITERAL ) )
                            {
                            // Request.g:3366:3: (a1= ':' (a2= TYPE_LITERAL ) )
                            // Request.g:3367:4: a1= ':' (a2= TYPE_LITERAL )
                            {
                            a1=(Token)match(input,30,FOLLOW_30_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2858); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            				if (element == null) {
                            					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            					startIncompleteElement(element);
                            				}
                            				collectHiddenTokens(element);
                            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_1_0_0_1, null, true);
                            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                            			}

                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[422]);
                            			}

                            // Request.g:3381:4: (a2= TYPE_LITERAL )
                            // Request.g:3382:5: a2= TYPE_LITERAL
                            {
                            a2=(Token)match(input,TYPE_LITERAL,FOLLOW_TYPE_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2884); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            					if (terminateParsing) {
                            						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                            					}
                            					if (element == null) {
                            						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            						startIncompleteElement(element);
                            					}
                            					if (a2 != null) {
                            						org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TYPE_LITERAL");
                            						tokenResolver.setOptions(getOptions());
                            						org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
                            						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), result);
                            						Object resolvedObject = result.getResolvedToken();
                            						if (resolvedObject == null) {
                            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
                            						}
                            						org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral resolved = (org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral) resolvedObject;
                            						if (resolved != null) {
                            							Object value = resolved;
                            							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), value);
                            							completedElement(value, false);
                            						}
                            						collectHiddenTokens(element);
                            						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_1_0_0_3, resolved, true);
                            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
                            					}
                            				}

                            }


                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[423]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[424]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[425]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[426]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[427]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[428]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[429]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[430]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[431]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[432]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[433]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[434]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[435]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[436]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[437]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[438]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[439]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[440]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[441]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[442]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[443]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[444]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[445]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[446]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[447]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[448]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[449]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[450]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[451]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[452]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[453]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[454]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[455]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[456]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[457]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[458]);
                            			}

                            }


                            }
                            break;

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[459]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[460]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[461]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[462]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[463]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[464]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[465]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[466]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[467]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[468]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[469]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[470]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[471]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[472]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[473]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[474]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[475]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[476]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[477]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[478]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[479]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[480]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[481]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[482]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[483]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[484]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[485]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[486]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[487]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[488]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[489]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[490]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[491]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[492]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[493]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[494]);
                    	}

                    // Request.g:3494:2: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
                    int alt24=2;
                    int LA24_0 = input.LA(1);

                    if ( (LA24_0==34) ) {
                        int LA24_1 = input.LA(2);

                        if ( (synpred24_Request()) ) {
                            alt24=1;
                        }
                    }
                    switch (alt24) {
                        case 1 :
                            // Request.g:3495:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                            {
                            // Request.g:3495:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                            // Request.g:3496:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                            {
                            a3=(Token)match(input,34,FOLLOW_34_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2939); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            				if (element == null) {
                            					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            					startIncompleteElement(element);
                            				}
                            				collectHiddenTokens(element);
                            				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_2_0_0_1, null, true);
                            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                            			}

                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[495]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[496]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[497]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[498]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[499]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[500]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[501]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[502]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[503]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[504]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[505]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[506]);
                            			}

                            // Request.g:3521:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                            // Request.g:3522:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                            {
                            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2965);
                            a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                            state._fsp--;
                            if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            					if (terminateParsing) {
                            						throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                            					}
                            					if (element == null) {
                            						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                            						startIncompleteElement(element);
                            					}
                            					if (a4_0 != null) {
                            						if (a4_0 != null) {
                            							Object value = a4_0;
                            							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION), value);
                            							completedElement(value, true);
                            						}
                            						collectHiddenTokens(element);
                            						retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_30_0_0_2_0_0_3, a4_0, true);
                            						copyLocalizationInfos(a4_0, element);
                            					}
                            				}

                            }


                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[507]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[508]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[509]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[510]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[511]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[512]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[513]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[514]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[515]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[516]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[517]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[518]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[519]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[520]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[521]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[522]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[523]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[524]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[525]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[526]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[527]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[528]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[529]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[530]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[531]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[532]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[533]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[534]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[535]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[536]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[537]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[538]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[539]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[540]);
                            				addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[541]);
                            			}

                            }


                            }
                            break;

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[542]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[543]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[544]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[545]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[546]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[547]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[548]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[549]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[550]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[551]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[552]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[553]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[554]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[555]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[556]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[557]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[558]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[559]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[560]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[561]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[562]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[563]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[564]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[565]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[566]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[567]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[568]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[569]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[570]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[571]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[572]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[573]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[574]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[575]);
                    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[576]);
                    	}

                    }
                    break;
                case 2 :
                    // Request.g:3624:2: c0= parse_org_coolsoftware_ecl_Metaparameter
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable3011);
                    c0=parse_org_coolsoftware_ecl_Metaparameter();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 20, parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_Variable"



    // $ANTLR start "parseop_Expression_level_03"
    // Request.g:3628:1: parseop_Expression_level_03 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_03() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_03_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return element; }

            // Request.g:3631:2: (leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |) )
            // Request.g:3632:2: leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_033036);
            leftArg=parseop_Expression_level_04();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:3632:40: ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |)
            int alt27=2;
            switch ( input.LA(1) ) {
            case 57:
                {
                int LA27_1 = input.LA(2);

                if ( (synpred28_Request()) ) {
                    alt27=1;
                }
                else if ( (true) ) {
                    alt27=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 27, 1, input);

                    throw nvae;

                }
                }
                break;
            case 39:
                {
                int LA27_2 = input.LA(2);

                if ( (synpred28_Request()) ) {
                    alt27=1;
                }
                else if ( (true) ) {
                    alt27=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 27, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 17:
            case 19:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 29:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 50:
            case 52:
            case 59:
            case 60:
            case 68:
                {
                alt27=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;

            }

            switch (alt27) {
                case 1 :
                    // Request.g:3632:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
                    {
                    // Request.g:3632:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
                    int cnt26=0;
                    loop26:
                    do {
                        int alt26=3;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==57) ) {
                            int LA26_2 = input.LA(2);

                            if ( (synpred26_Request()) ) {
                                alt26=1;
                            }


                        }
                        else if ( (LA26_0==39) ) {
                            int LA26_3 = input.LA(2);

                            if ( (synpred27_Request()) ) {
                                alt26=2;
                            }


                        }


                        switch (alt26) {
                    	case 1 :
                    	    // Request.g:3633:3: () a0= 'or' rightArg= parseop_Expression_level_04
                    	    {
                    	    // Request.g:3633:3: ()
                    	    // Request.g:3633:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,57,FOLLOW_57_in_parseop_Expression_level_033056); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_25_0_0_2, null, true);
                    	    			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[577]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[578]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[579]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[580]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[581]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[582]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[583]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[584]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[585]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[586]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[587]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[588]);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_033073);
                    	    rightArg=parseop_Expression_level_04();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_25_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_25_0_0_4, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Request.g:3700:3: () a0= 'and' rightArg= parseop_Expression_level_04
                    	    {
                    	    // Request.g:3700:3: ()
                    	    // Request.g:3700:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,39,FOLLOW_39_in_parseop_Expression_level_033107); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_26_0_0_2, null, true);
                    	    			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[589]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[590]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[591]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[592]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[593]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[594]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[595]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[596]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[597]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[598]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[599]);
                    	    			addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[600]);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_033124);
                    	    rightArg=parseop_Expression_level_04();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_26_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_26_0_0_4, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt26 >= 1 ) break loop26;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(26, input);
                                throw eee;
                        }
                        cnt26++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:3766:21: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 21, parseop_Expression_level_03_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_03"



    // $ANTLR start "parseop_Expression_level_04"
    // Request.g:3771:1: parseop_Expression_level_04 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_04() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_04_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return element; }

            // Request.g:3774:9: (leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |) )
            // Request.g:3775:9: leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_043170);
            leftArg=parseop_Expression_level_5();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:3775:37: ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |)
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==50) ) {
                int LA29_1 = input.LA(2);

                if ( (synpred30_Request()) ) {
                    alt29=1;
                }
                else if ( (true) ) {
                    alt29=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 29, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA29_0==EOF||LA29_0==TEXT||LA29_0==17||LA29_0==19||(LA29_0 >= 21 && LA29_0 <= 27)||LA29_0==29||(LA29_0 >= 31 && LA29_0 <= 39)||LA29_0==52||LA29_0==57||(LA29_0 >= 59 && LA29_0 <= 60)||LA29_0==68) ) {
                alt29=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;

            }
            switch (alt29) {
                case 1 :
                    // Request.g:3775:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
                    {
                    // Request.g:3775:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
                    int cnt28=0;
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==50) ) {
                            int LA28_2 = input.LA(2);

                            if ( (synpred29_Request()) ) {
                                alt28=1;
                            }


                        }


                        switch (alt28) {
                    	case 1 :
                    	    // Request.g:3776:2: () a0= 'implies' rightArg= parseop_Expression_level_5
                    	    {
                    	    // Request.g:3776:2: ()
                    	    // Request.g:3776:3: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,50,FOLLOW_50_in_parseop_Expression_level_043186); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_27_0_0_2, null, true);
                    	    		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		// expected elements (follow set)
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[601]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[602]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[603]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[604]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[605]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[606]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[607]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[608]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[609]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[610]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[611]);
                    	    		addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[612]);
                    	    	}

                    	    pushFollow(FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_043200);
                    	    rightArg=parseop_Expression_level_5();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (leftArg != null) {
                    	    			if (leftArg != null) {
                    	    				Object value = leftArg;
                    	    				element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_27_0_0_0, leftArg, true);
                    	    			copyLocalizationInfos(leftArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (rightArg != null) {
                    	    			if (rightArg != null) {
                    	    				Object value = rightArg;
                    	    				addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_27_0_0_4, rightArg, true);
                    	    			copyLocalizationInfos(rightArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt28 >= 1 ) break loop28;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(28, input);
                                throw eee;
                        }
                        cnt28++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:3842:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 22, parseop_Expression_level_04_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_04"



    // $ANTLR start "parseop_Expression_level_5"
    // Request.g:3847:1: parseop_Expression_level_5 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'f' arg= parseop_Expression_level_11 |a0= '!' arg= parseop_Expression_level_11 |arg= parseop_Expression_level_11 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_5() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_5_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return element; }

            // Request.g:3850:0: (a0= 'f' arg= parseop_Expression_level_11 |a0= '!' arg= parseop_Expression_level_11 |arg= parseop_Expression_level_11 )
            int alt30=3;
            switch ( input.LA(1) ) {
            case 46:
                {
                alt30=1;
                }
                break;
            case 16:
                {
                alt30=2;
                }
                break;
            case INTEGER_LITERAL:
            case QUOTED_34_34:
            case REAL_LITERAL:
            case TEXT:
            case 18:
            case 44:
            case 47:
            case 56:
            case 62:
            case 65:
            case 66:
                {
                alt30=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;

            }

            switch (alt30) {
                case 1 :
                    // Request.g:3851:0: a0= 'f' arg= parseop_Expression_level_11
                    {
                    a0=(Token)match(input,46,FOLLOW_46_in_parseop_Expression_level_53241); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_11_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[613]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[614]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[615]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[616]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[617]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[618]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[619]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[620]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[621]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[622]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[623]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[624]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_53252);
                    arg=parseop_Expression_level_11();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
                    	startIncompleteElement(element);
                    }
                    if (arg != null) {
                    	if (arg != null) {
                    		Object value = arg;
                    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP), value);
                    		completedElement(value, true);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_11_0_0_2, arg, true);
                    	copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Request.g:3896:0: a0= '!' arg= parseop_Expression_level_11
                    {
                    a0=(Token)match(input,16,FOLLOW_16_in_parseop_Expression_level_53261); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_28_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[625]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[626]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[627]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[628]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[629]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[630]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[631]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[632]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[633]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[634]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[635]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[636]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_53272);
                    arg=parseop_Expression_level_11();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
                    	startIncompleteElement(element);
                    }
                    if (arg != null) {
                    	if (arg != null) {
                    		Object value = arg;
                    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    		completedElement(value, true);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_28_0_0_2, arg, true);
                    	copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Request.g:3942:5: arg= parseop_Expression_level_11
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_53282);
                    arg=parseop_Expression_level_11();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 23, parseop_Expression_level_5_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_5"



    // $ANTLR start "parseop_Expression_level_11"
    // Request.g:3945:1: parseop_Expression_level_11 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_11() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_11_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return element; }

            // Request.g:3948:9: (leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |) )
            // Request.g:3949:9: leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113304);
            leftArg=parseop_Expression_level_12();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:3949:38: ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |)
            int alt32=2;
            switch ( input.LA(1) ) {
            case 36:
                {
                int LA32_1 = input.LA(2);

                if ( (synpred37_Request()) ) {
                    alt32=1;
                }
                else if ( (true) ) {
                    alt32=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 32, 1, input);

                    throw nvae;

                }
                }
                break;
            case 37:
                {
                int LA32_2 = input.LA(2);

                if ( (synpred37_Request()) ) {
                    alt32=1;
                }
                else if ( (true) ) {
                    alt32=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 32, 2, input);

                    throw nvae;

                }
                }
                break;
            case 32:
                {
                int LA32_3 = input.LA(2);

                if ( (synpred37_Request()) ) {
                    alt32=1;
                }
                else if ( (true) ) {
                    alt32=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 32, 3, input);

                    throw nvae;

                }
                }
                break;
            case 33:
                {
                int LA32_4 = input.LA(2);

                if ( (synpred37_Request()) ) {
                    alt32=1;
                }
                else if ( (true) ) {
                    alt32=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 32, 4, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 17:
            case 19:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 29:
            case 31:
            case 34:
            case 35:
            case 38:
            case 39:
            case 50:
            case 52:
            case 57:
            case 59:
            case 60:
            case 68:
                {
                alt32=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;

            }

            switch (alt32) {
                case 1 :
                    // Request.g:3949:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
                    {
                    // Request.g:3949:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
                    int cnt31=0;
                    loop31:
                    do {
                        int alt31=5;
                        switch ( input.LA(1) ) {
                        case 32:
                            {
                            int LA31_2 = input.LA(2);

                            if ( (synpred35_Request()) ) {
                                alt31=3;
                            }


                            }
                            break;
                        case 36:
                            {
                            int LA31_3 = input.LA(2);

                            if ( (synpred33_Request()) ) {
                                alt31=1;
                            }


                            }
                            break;
                        case 37:
                            {
                            int LA31_4 = input.LA(2);

                            if ( (synpred34_Request()) ) {
                                alt31=2;
                            }


                            }
                            break;
                        case 33:
                            {
                            int LA31_5 = input.LA(2);

                            if ( (synpred36_Request()) ) {
                                alt31=4;
                            }


                            }
                            break;

                        }

                        switch (alt31) {
                    	case 1 :
                    	    // Request.g:3950:0: () a0= '>' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Request.g:3950:2: ()
                    	    // Request.g:3950:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,36,FOLLOW_36_in_parseop_Expression_level_113317); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_19_0_0_2, null, true);
                    	    	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[637]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[638]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[639]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[640]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[641]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[642]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[643]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[644]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[645]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[646]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[647]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[648]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113328);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_19_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_19_0_0_4, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Request.g:4017:0: () a0= '>=' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Request.g:4017:2: ()
                    	    // Request.g:4017:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,37,FOLLOW_37_in_parseop_Expression_level_113346); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_20_0_0_2, null, true);
                    	    	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[649]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[650]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[651]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[652]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[653]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[654]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[655]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[656]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[657]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[658]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[659]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[660]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113357);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_20_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_20_0_0_4, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 3 :
                    	    // Request.g:4084:0: () a0= '<' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Request.g:4084:2: ()
                    	    // Request.g:4084:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,32,FOLLOW_32_in_parseop_Expression_level_113375); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_21_0_0_2, null, true);
                    	    	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[661]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[662]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[663]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[664]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[665]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[666]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[667]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[668]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[669]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[670]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[671]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[672]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113386);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_21_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_21_0_0_4, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 4 :
                    	    // Request.g:4151:0: () a0= '<=' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Request.g:4151:2: ()
                    	    // Request.g:4151:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,33,FOLLOW_33_in_parseop_Expression_level_113404); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_22_0_0_2, null, true);
                    	    	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[673]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[674]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[675]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[676]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[677]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[678]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[679]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[680]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[681]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[682]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[683]);
                    	    	addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[684]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113415);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_22_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_22_0_0_4, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt31 >= 1 ) break loop31;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(31, input);
                                throw eee;
                        }
                        cnt31++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:4217:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 24, parseop_Expression_level_11_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_11"



    // $ANTLR start "parseop_Expression_level_12"
    // Request.g:4222:1: parseop_Expression_level_12 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_12() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_12_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return element; }

            // Request.g:4225:9: (leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |) )
            // Request.g:4226:9: leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123453);
            leftArg=parseop_Expression_level_19();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:4226:38: ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |)
            int alt34=2;
            switch ( input.LA(1) ) {
            case 35:
                {
                int LA34_1 = input.LA(2);

                if ( (synpred40_Request()) ) {
                    alt34=1;
                }
                else if ( (true) ) {
                    alt34=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 34, 1, input);

                    throw nvae;

                }
                }
                break;
            case 17:
                {
                int LA34_2 = input.LA(2);

                if ( (synpred40_Request()) ) {
                    alt34=1;
                }
                else if ( (true) ) {
                    alt34=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 34, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 19:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 29:
            case 31:
            case 32:
            case 33:
            case 34:
            case 36:
            case 37:
            case 38:
            case 39:
            case 50:
            case 52:
            case 57:
            case 59:
            case 60:
            case 68:
                {
                alt34=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;

            }

            switch (alt34) {
                case 1 :
                    // Request.g:4226:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
                    {
                    // Request.g:4226:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
                    int cnt33=0;
                    loop33:
                    do {
                        int alt33=3;
                        int LA33_0 = input.LA(1);

                        if ( (LA33_0==35) ) {
                            int LA33_2 = input.LA(2);

                            if ( (synpred38_Request()) ) {
                                alt33=1;
                            }


                        }
                        else if ( (LA33_0==17) ) {
                            int LA33_3 = input.LA(2);

                            if ( (synpred39_Request()) ) {
                                alt33=2;
                            }


                        }


                        switch (alt33) {
                    	case 1 :
                    	    // Request.g:4227:0: () a0= '==' rightArg= parseop_Expression_level_19
                    	    {
                    	    // Request.g:4227:2: ()
                    	    // Request.g:4227:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,35,FOLLOW_35_in_parseop_Expression_level_123466); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_23_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[685]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[686]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[687]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[688]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[689]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[690]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[691]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[692]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[693]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[694]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[695]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[696]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123477);
                    	    rightArg=parseop_Expression_level_19();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_23_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_23_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Request.g:4294:0: () a0= '!=' rightArg= parseop_Expression_level_19
                    	    {
                    	    // Request.g:4294:2: ()
                    	    // Request.g:4294:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,17,FOLLOW_17_in_parseop_Expression_level_123495); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_24_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[697]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[698]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[699]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[700]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[701]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[702]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[703]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[704]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[705]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[706]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[707]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[708]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123506);
                    	    rightArg=parseop_Expression_level_19();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_24_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_24_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt33 >= 1 ) break loop33;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(33, input);
                                throw eee;
                        }
                        cnt33++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:4360:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 25, parseop_Expression_level_12_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_12"



    // $ANTLR start "parseop_Expression_level_19"
    // Request.g:4365:1: parseop_Expression_level_19 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_19() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_19_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return element; }

            // Request.g:4368:9: (leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |) )
            // Request.g:4369:9: leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193544);
            leftArg=parseop_Expression_level_21();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:4369:38: ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |)
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==38) ) {
                int LA36_1 = input.LA(2);

                if ( (synpred42_Request()) ) {
                    alt36=1;
                }
                else if ( (true) ) {
                    alt36=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA36_0==EOF||LA36_0==TEXT||LA36_0==17||LA36_0==19||(LA36_0 >= 21 && LA36_0 <= 27)||LA36_0==29||(LA36_0 >= 31 && LA36_0 <= 37)||LA36_0==39||LA36_0==50||LA36_0==52||LA36_0==57||(LA36_0 >= 59 && LA36_0 <= 60)||LA36_0==68) ) {
                alt36=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;

            }
            switch (alt36) {
                case 1 :
                    // Request.g:4369:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
                    {
                    // Request.g:4369:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
                    int cnt35=0;
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( (LA35_0==38) ) {
                            int LA35_2 = input.LA(2);

                            if ( (synpred41_Request()) ) {
                                alt35=1;
                            }


                        }


                        switch (alt35) {
                    	case 1 :
                    	    // Request.g:4370:0: () a0= '^' rightArg= parseop_Expression_level_21
                    	    {
                    	    // Request.g:4370:2: ()
                    	    // Request.g:4370:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,38,FOLLOW_38_in_parseop_Expression_level_193557); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_18_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[709]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[710]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[711]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[712]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[713]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[714]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[715]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[716]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[717]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[718]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[719]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[720]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193568);
                    	    rightArg=parseop_Expression_level_21();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    	Object value = leftArg;
                    	    	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_18_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    	Object value = rightArg;
                    	    	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_18_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt35 >= 1 ) break loop35;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(35, input);
                                throw eee;
                        }
                        cnt35++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:4436:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 26, parseop_Expression_level_19_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_19"



    // $ANTLR start "parseop_Expression_level_21"
    // Request.g:4441:1: parseop_Expression_level_21 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_21() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_21_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return element; }

            // Request.g:4444:9: (leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |) )
            // Request.g:4445:9: leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213606);
            leftArg=parseop_Expression_level_22();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:4445:38: ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |)
            int alt38=2;
            switch ( input.LA(1) ) {
            case 22:
                {
                int LA38_1 = input.LA(2);

                if ( (synpred45_Request()) ) {
                    alt38=1;
                }
                else if ( (true) ) {
                    alt38=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 38, 1, input);

                    throw nvae;

                }
                }
                break;
            case 25:
                {
                int LA38_2 = input.LA(2);

                if ( (synpred45_Request()) ) {
                    alt38=1;
                }
                else if ( (true) ) {
                    alt38=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 38, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 17:
            case 19:
            case 21:
            case 23:
            case 24:
            case 26:
            case 27:
            case 29:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 50:
            case 52:
            case 57:
            case 59:
            case 60:
            case 68:
                {
                alt38=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;

            }

            switch (alt38) {
                case 1 :
                    // Request.g:4445:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
                    {
                    // Request.g:4445:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
                    int cnt37=0;
                    loop37:
                    do {
                        int alt37=3;
                        int LA37_0 = input.LA(1);

                        if ( (LA37_0==22) ) {
                            int LA37_2 = input.LA(2);

                            if ( (synpred43_Request()) ) {
                                alt37=1;
                            }


                        }
                        else if ( (LA37_0==25) ) {
                            int LA37_3 = input.LA(2);

                            if ( (synpred44_Request()) ) {
                                alt37=2;
                            }


                        }


                        switch (alt37) {
                    	case 1 :
                    	    // Request.g:4446:0: () a0= '+' rightArg= parseop_Expression_level_22
                    	    {
                    	    // Request.g:4446:2: ()
                    	    // Request.g:4446:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,22,FOLLOW_22_in_parseop_Expression_level_213619); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_12_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[721]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[722]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[723]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[724]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[725]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[726]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[727]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[728]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[729]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[730]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[731]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[732]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213630);
                    	    rightArg=parseop_Expression_level_22();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_12_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_12_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Request.g:4513:0: () a0= '-' rightArg= parseop_Expression_level_22
                    	    {
                    	    // Request.g:4513:2: ()
                    	    // Request.g:4513:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,25,FOLLOW_25_in_parseop_Expression_level_213648); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_13_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[733]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[734]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[735]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[736]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[737]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[738]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[739]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[740]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[741]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[742]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[743]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[744]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213659);
                    	    rightArg=parseop_Expression_level_22();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_13_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_13_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt37 >= 1 ) break loop37;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(37, input);
                                throw eee;
                        }
                        cnt37++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:4579:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 27, parseop_Expression_level_21_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_21"



    // $ANTLR start "parseop_Expression_level_22"
    // Request.g:4584:1: parseop_Expression_level_22 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_22() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_22_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return element; }

            // Request.g:4587:9: (leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |) )
            // Request.g:4588:9: leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223697);
            leftArg=parseop_Expression_level_80();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:4588:38: ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |)
            int alt40=2;
            switch ( input.LA(1) ) {
            case 21:
                {
                int LA40_1 = input.LA(2);

                if ( (synpred48_Request()) ) {
                    alt40=1;
                }
                else if ( (true) ) {
                    alt40=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 40, 1, input);

                    throw nvae;

                }
                }
                break;
            case 29:
                {
                int LA40_2 = input.LA(2);

                if ( (synpred48_Request()) ) {
                    alt40=1;
                }
                else if ( (true) ) {
                    alt40=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 40, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 17:
            case 19:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 50:
            case 52:
            case 57:
            case 59:
            case 60:
            case 68:
                {
                alt40=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;

            }

            switch (alt40) {
                case 1 :
                    // Request.g:4588:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
                    {
                    // Request.g:4588:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
                    int cnt39=0;
                    loop39:
                    do {
                        int alt39=3;
                        int LA39_0 = input.LA(1);

                        if ( (LA39_0==21) ) {
                            int LA39_2 = input.LA(2);

                            if ( (synpred46_Request()) ) {
                                alt39=1;
                            }


                        }
                        else if ( (LA39_0==29) ) {
                            int LA39_3 = input.LA(2);

                            if ( (synpred47_Request()) ) {
                                alt39=2;
                            }


                        }


                        switch (alt39) {
                    	case 1 :
                    	    // Request.g:4589:0: () a0= '*' rightArg= parseop_Expression_level_80
                    	    {
                    	    // Request.g:4589:2: ()
                    	    // Request.g:4589:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,21,FOLLOW_21_in_parseop_Expression_level_223710); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_16_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[745]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[746]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[747]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[748]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[749]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[750]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[751]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[752]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[753]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[754]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[755]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[756]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223721);
                    	    rightArg=parseop_Expression_level_80();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_16_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_16_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Request.g:4656:0: () a0= '/' rightArg= parseop_Expression_level_80
                    	    {
                    	    // Request.g:4656:2: ()
                    	    // Request.g:4656:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,29,FOLLOW_29_in_parseop_Expression_level_223739); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_17_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[757]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[758]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[759]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[760]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[761]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[762]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[763]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[764]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[765]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[766]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[767]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[768]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223750);
                    	    rightArg=parseop_Expression_level_80();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_17_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_17_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt39 >= 1 ) break loop39;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(39, input);
                                throw eee;
                        }
                        cnt39++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:4722:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 28, parseop_Expression_level_22_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_22"



    // $ANTLR start "parseop_Expression_level_80"
    // Request.g:4727:1: parseop_Expression_level_80 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'sin' arg= parseop_Expression_level_87 |a0= 'cos' arg= parseop_Expression_level_87 |arg= parseop_Expression_level_87 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_80() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_80_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return element; }

            // Request.g:4730:0: (a0= 'sin' arg= parseop_Expression_level_87 |a0= 'cos' arg= parseop_Expression_level_87 |arg= parseop_Expression_level_87 )
            int alt41=3;
            switch ( input.LA(1) ) {
            case 62:
                {
                alt41=1;
                }
                break;
            case 44:
                {
                alt41=2;
                }
                break;
            case INTEGER_LITERAL:
            case QUOTED_34_34:
            case REAL_LITERAL:
            case TEXT:
            case 18:
            case 47:
            case 56:
            case 65:
            case 66:
                {
                alt41=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;

            }

            switch (alt41) {
                case 1 :
                    // Request.g:4731:0: a0= 'sin' arg= parseop_Expression_level_87
                    {
                    a0=(Token)match(input,62,FOLLOW_62_in_parseop_Expression_level_803788); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_9_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[769]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[770]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[771]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[772]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[773]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[774]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[775]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[776]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[777]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[778]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[779]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[780]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803799);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_9_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Request.g:4776:0: a0= 'cos' arg= parseop_Expression_level_87
                    {
                    a0=(Token)match(input,44,FOLLOW_44_in_parseop_Expression_level_803808); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_10_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[781]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[782]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[783]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[784]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[785]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[786]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[787]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[788]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[789]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[790]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[791]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[792]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803819);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_10_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Request.g:4822:5: arg= parseop_Expression_level_87
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803829);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 29, parseop_Expression_level_80_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_80"



    // $ANTLR start "parseop_Expression_level_87"
    // Request.g:4825:1: parseop_Expression_level_87 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'not' arg= parseop_Expression_level_88 |arg= parseop_Expression_level_88 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_87() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_87_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return element; }

            // Request.g:4828:0: (a0= 'not' arg= parseop_Expression_level_88 |arg= parseop_Expression_level_88 )
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==56) ) {
                alt42=1;
            }
            else if ( (LA42_0==INTEGER_LITERAL||LA42_0==QUOTED_34_34||LA42_0==REAL_LITERAL||LA42_0==TEXT||LA42_0==18||LA42_0==47||(LA42_0 >= 65 && LA42_0 <= 66)) ) {
                alt42=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;

            }
            switch (alt42) {
                case 1 :
                    // Request.g:4829:0: a0= 'not' arg= parseop_Expression_level_88
                    {
                    a0=(Token)match(input,56,FOLLOW_56_in_parseop_Expression_level_873851); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_8_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[793]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[794]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[795]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[796]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[797]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[798]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[799]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[800]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[801]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[802]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[803]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[804]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873862);
                    arg=parseop_Expression_level_88();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_8_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Request.g:4875:5: arg= parseop_Expression_level_88
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873872);
                    arg=parseop_Expression_level_88();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 30, parseop_Expression_level_87_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_87"



    // $ANTLR start "parseop_Expression_level_88"
    // Request.g:4878:1: parseop_Expression_level_88 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |a0= '=' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_88() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_88_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;

        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return element; }

            // Request.g:4881:5: (arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |a0= '=' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) |) )
            // Request.g:4882:5: arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |a0= '=' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_90_in_parseop_Expression_level_883894);
            arg=parseop_Expression_level_90();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:4882:34: (a0= '++' |a0= '--' |a0= '=' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) |)
            int alt43=4;
            switch ( input.LA(1) ) {
            case 23:
                {
                int LA43_1 = input.LA(2);

                if ( (synpred52_Request()) ) {
                    alt43=1;
                }
                else if ( (true) ) {
                    alt43=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 43, 1, input);

                    throw nvae;

                }
                }
                break;
            case 26:
                {
                int LA43_2 = input.LA(2);

                if ( (synpred53_Request()) ) {
                    alt43=2;
                }
                else if ( (true) ) {
                    alt43=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 43, 2, input);

                    throw nvae;

                }
                }
                break;
            case 34:
                {
                int LA43_3 = input.LA(2);

                if ( (synpred54_Request()) ) {
                    alt43=3;
                }
                else if ( (true) ) {
                    alt43=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 43, 3, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 17:
            case 19:
            case 21:
            case 22:
            case 24:
            case 25:
            case 27:
            case 29:
            case 31:
            case 32:
            case 33:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 50:
            case 52:
            case 57:
            case 59:
            case 60:
            case 68:
                {
                alt43=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;

            }

            switch (alt43) {
                case 1 :
                    // Request.g:4883:0: a0= '++'
                    {
                    a0=(Token)match(input,23,FOLLOW_23_in_parseop_Expression_level_883901); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_6_0_0_2, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[805]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[806]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[807]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[808]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[809]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[810]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[811]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[812]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[813]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[814]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[815]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[816]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[817]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[818]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[819]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[820]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[821]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[822]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[823]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[824]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[825]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[826]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[827]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[828]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[829]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[830]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[831]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[832]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[833]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[834]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[835]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[836]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[837]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[838]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[839]);
                    }

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_6_0_0_0, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Request.g:4951:0: a0= '--'
                    {
                    a0=(Token)match(input,26,FOLLOW_26_in_parseop_Expression_level_883916); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_7_0_0_2, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[840]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[841]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[842]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[843]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[844]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[845]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[846]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[847]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[848]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[849]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[850]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[851]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[852]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[853]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[854]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[855]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[856]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[857]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[858]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[859]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[860]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[861]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[862]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[863]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[864]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[865]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[866]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[867]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[868]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[869]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[870]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[871]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[872]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[873]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[874]);
                    }

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_7_0_0_0, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Request.g:5019:0: a0= '=' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a0=(Token)match(input,34,FOLLOW_34_in_parseop_Expression_level_883931); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_32_0_0_2, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[875]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[876]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[877]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[878]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[879]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[880]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[881]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[882]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[883]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[884]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[885]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[886]);
                    }

                    // Request.g:5044:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Request.g:5045:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parseop_Expression_level_883944);
                    a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    startIncompleteElement(element);
                    }
                    if (a1_0 != null) {
                    if (a1_0 != null) {
                    Object value = a1_0;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_32_0_0_4, a1_0, true);
                    copyLocalizationInfos(a1_0, element);
                    }
                    }

                    }


                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[887]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[888]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[889]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[890]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[891]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[892]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[893]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[894]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[895]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[896]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[897]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[898]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[899]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[900]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[901]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[902]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[903]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[904]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[905]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[906]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[907]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[908]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[909]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[910]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[911]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[912]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[913]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[914]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[915]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[916]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[917]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[918]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[919]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[920]);
                    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[921]);
                    }

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_32_0_0_0, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 4 :
                    // Request.g:5124:15: 
                    {
                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 31, parseop_Expression_level_88_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_88"



    // $ANTLR start "parseop_Expression_level_90"
    // Request.g:5128:1: parseop_Expression_level_90 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_90() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_90_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return element; }

            // Request.g:5131:9: (leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |) )
            // Request.g:5132:9: leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903982);
            leftArg=parseop_Expression_level_91();

            state._fsp--;
            if (state.failed) return element;

            // Request.g:5132:38: ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |)
            int alt45=2;
            switch ( input.LA(1) ) {
            case 24:
                {
                int LA45_1 = input.LA(2);

                if ( (synpred57_Request()) ) {
                    alt45=1;
                }
                else if ( (true) ) {
                    alt45=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 45, 1, input);

                    throw nvae;

                }
                }
                break;
            case 27:
                {
                int LA45_2 = input.LA(2);

                if ( (synpred57_Request()) ) {
                    alt45=1;
                }
                else if ( (true) ) {
                    alt45=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 45, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case TEXT:
            case 17:
            case 19:
            case 21:
            case 22:
            case 23:
            case 25:
            case 26:
            case 29:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 50:
            case 52:
            case 57:
            case 59:
            case 60:
            case 68:
                {
                alt45=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;

            }

            switch (alt45) {
                case 1 :
                    // Request.g:5132:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
                    {
                    // Request.g:5132:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
                    int cnt44=0;
                    loop44:
                    do {
                        int alt44=3;
                        int LA44_0 = input.LA(1);

                        if ( (LA44_0==24) ) {
                            int LA44_2 = input.LA(2);

                            if ( (synpred55_Request()) ) {
                                alt44=1;
                            }


                        }
                        else if ( (LA44_0==27) ) {
                            int LA44_3 = input.LA(2);

                            if ( (synpred56_Request()) ) {
                                alt44=2;
                            }


                        }


                        switch (alt44) {
                    	case 1 :
                    	    // Request.g:5133:0: () a0= '+=' rightArg= parseop_Expression_level_91
                    	    {
                    	    // Request.g:5133:2: ()
                    	    // Request.g:5133:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,24,FOLLOW_24_in_parseop_Expression_level_903995); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_14_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[922]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[923]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[924]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[925]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[926]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[927]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[928]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[929]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[930]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[931]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[932]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[933]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_904006);
                    	    rightArg=parseop_Expression_level_91();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_14_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_14_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Request.g:5200:0: () a0= '-=' rightArg= parseop_Expression_level_91
                    	    {
                    	    // Request.g:5200:2: ()
                    	    // Request.g:5200:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,27,FOLLOW_27_in_parseop_Expression_level_904024); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_15_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[934]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[935]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[936]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[937]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[938]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[939]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[940]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[941]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[942]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[943]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[944]);
                    	    addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[945]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_904035);
                    	    rightArg=parseop_Expression_level_91();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_15_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_15_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt44 >= 1 ) break loop44;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(44, input);
                                throw eee;
                        }
                        cnt44++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Request.g:5266:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 32, parseop_Expression_level_90_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_90"



    // $ANTLR start "parseop_Expression_level_91"
    // Request.g:5271:1: parseop_Expression_level_91 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression |c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression |c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression |c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression |c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression |c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration |c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_91() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_91_StartIndex = input.index();

        org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression c0 =null;

        org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression c1 =null;

        org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression c2 =null;

        org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression c3 =null;

        org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression c4 =null;

        org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration c5 =null;

        org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression c6 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return element; }

            // Request.g:5274:0: (c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression |c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression |c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression |c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression |c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression |c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration |c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression )
            int alt46=7;
            switch ( input.LA(1) ) {
            case INTEGER_LITERAL:
                {
                alt46=1;
                }
                break;
            case REAL_LITERAL:
                {
                alt46=2;
                }
                break;
            case 47:
            case 65:
                {
                alt46=3;
                }
                break;
            case QUOTED_34_34:
                {
                alt46=4;
                }
                break;
            case 18:
                {
                alt46=5;
                }
                break;
            case 66:
                {
                alt46=6;
                }
                break;
            case TEXT:
                {
                alt46=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;

            }

            switch (alt46) {
                case 1 :
                    // Request.g:5275:0: c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_in_parseop_Expression_level_914073);
                    c0=parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Request.g:5276:2: c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_in_parseop_Expression_level_914081);
                    c1=parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Request.g:5277:2: c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_in_parseop_Expression_level_914089);
                    c2=parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Request.g:5278:2: c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_in_parseop_Expression_level_914097);
                    c3=parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Request.g:5279:2: c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_in_parseop_Expression_level_914105);
                    c4=parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 6 :
                    // Request.g:5280:2: c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_in_parseop_Expression_level_914113);
                    c5=parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c5; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 7 :
                    // Request.g:5281:2: c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_in_parseop_Expression_level_914121);
                    c6=parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c6; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 33, parseop_Expression_level_91_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_91"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression"
    // Request.g:5284:1: parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element = null] : (a0= INTEGER_LITERAL ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return element; }

            // Request.g:5287:4: ( (a0= INTEGER_LITERAL ) )
            // Request.g:5288:4: (a0= INTEGER_LITERAL )
            {
            // Request.g:5288:4: (a0= INTEGER_LITERAL )
            // Request.g:5289:4: a0= INTEGER_LITERAL
            {
            a0=(Token)match(input,INTEGER_LITERAL,FOLLOW_INTEGER_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression4145); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createIntegerLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("INTEGER_LITERAL");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.Long resolved = (java.lang.Long) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_1_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[946]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[947]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[948]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[949]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[950]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[951]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[952]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[953]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[954]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[955]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[956]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[957]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[958]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[959]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[960]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[961]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[962]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[963]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[964]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[965]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[966]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[967]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[968]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[969]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[970]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[971]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[972]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[973]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[974]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[975]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[976]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[977]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[978]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[979]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[980]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 34, parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression"
    // Request.g:5360:1: parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element = null] : (a0= REAL_LITERAL ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return element; }

            // Request.g:5363:4: ( (a0= REAL_LITERAL ) )
            // Request.g:5364:4: (a0= REAL_LITERAL )
            {
            // Request.g:5364:4: (a0= REAL_LITERAL )
            // Request.g:5365:4: a0= REAL_LITERAL
            {
            a0=(Token)match(input,REAL_LITERAL,FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression4175); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createRealLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.Double resolved = (java.lang.Double) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_2_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[981]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[982]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[983]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[984]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[985]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[986]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[987]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[988]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[989]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[990]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[991]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[992]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[993]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[994]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[995]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[996]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[997]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[998]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[999]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1000]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1001]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1002]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1003]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1004]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1005]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1006]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1007]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1008]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1009]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1010]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1011]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1012]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1013]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1014]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1015]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 35, parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression"
    // Request.g:5436:1: parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element = null] : ( (a0= 'true' |a1= 'false' ) ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return element; }

            // Request.g:5439:0: ( ( (a0= 'true' |a1= 'false' ) ) )
            // Request.g:5440:0: ( (a0= 'true' |a1= 'false' ) )
            {
            // Request.g:5440:0: ( (a0= 'true' |a1= 'false' ) )
            // Request.g:5441:0: (a0= 'true' |a1= 'false' )
            {
            // Request.g:5441:0: (a0= 'true' |a1= 'false' )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==65) ) {
                alt47=1;
            }
            else if ( (LA47_0==47) ) {
                alt47=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;

            }
            switch (alt47) {
                case 1 :
                    // Request.g:5442:0: a0= 'true'
                    {
                    a0=(Token)match(input,65,FOLLOW_65_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression4207); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_3_0_0_0, true, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    // set value of boolean attribute
                    Object value = true;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;
                case 2 :
                    // Request.g:5455:2: a1= 'false'
                    {
                    a1=(Token)match(input,47,FOLLOW_47_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression4216); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_3_0_0_0, false, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    // set value of boolean attribute
                    Object value = false;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1016]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1017]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1018]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1019]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1020]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1021]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1022]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1023]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1024]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1025]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1026]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1027]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1028]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1029]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1030]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1031]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1032]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1033]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1034]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1035]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1036]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1037]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1038]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1039]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1040]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1041]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1042]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1043]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1044]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1045]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1046]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1047]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1048]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1049]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1050]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 36, parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression"
    // Request.g:5511:1: parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element = null] : (a0= QUOTED_34_34 ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return element; }

            // Request.g:5514:4: ( (a0= QUOTED_34_34 ) )
            // Request.g:5515:4: (a0= QUOTED_34_34 )
            {
            // Request.g:5515:4: (a0= QUOTED_34_34 )
            // Request.g:5516:4: a0= QUOTED_34_34
            {
            a0=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression4248); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createStringLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.String resolved = (java.lang.String) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_4_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1051]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1052]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1053]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1054]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1055]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1056]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1057]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1058]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1059]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1060]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1061]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1062]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1063]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1064]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1065]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1066]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1067]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1068]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1069]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1070]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1071]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1072]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1073]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1074]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1075]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1076]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1077]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1078]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1079]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1080]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1081]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1082]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1083]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1084]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1085]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 37, parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression"
    // Request.g:5587:1: parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression returns [org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element = null] : a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')' ;
    public final org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return element; }

            // Request.g:5590:4: (a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')' )
            // Request.g:5591:4: a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')'
            {
            a0=(Token)match(input,18,FOLLOW_18_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression4276); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_5_0_0_0, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1086]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1087]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1088]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1089]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1090]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1091]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1092]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1093]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1094]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1095]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1096]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1097]);
            }

            // Request.g:5616:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            // Request.g:5617:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression4289);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            Object value = a1_0;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP), value);
            completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_5_0_0_2, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1098]);
            }

            a2=(Token)match(input,19,FOLLOW_19_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression4301); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_5_0_0_4, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1099]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1100]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1101]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1102]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1103]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1104]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1105]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1106]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1107]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1108]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1109]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1110]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1111]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1112]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1113]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1114]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1115]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1116]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1117]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1118]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1119]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1120]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1121]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1122]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1123]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1124]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1125]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1126]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1127]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1128]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1129]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1130]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1131]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1132]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1133]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 38, parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration"
    // Request.g:5692:1: parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration returns [org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element = null] : a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable ) ;
    public final org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.variables.Variable a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return element; }

            // Request.g:5695:4: (a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable ) )
            // Request.g:5696:4: a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable )
            {
            a0=(Token)match(input,66,FOLLOW_66_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4327); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_29_0_0_0, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1134]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1135]);
            }

            // Request.g:5711:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable )
            // Request.g:5712:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4340);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            Object value = a1_0;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE), value);
            completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_29_0_0_2, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1136]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1137]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1138]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1139]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1140]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1141]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1142]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1143]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1144]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1145]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1146]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1147]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1148]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1149]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1150]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1151]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1152]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1153]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1154]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1155]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1156]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1157]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1158]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1159]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1160]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1161]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1162]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1163]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1164]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1165]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1166]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1167]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1168]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1169]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1170]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 39, parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression"
    // Request.g:5773:1: parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression returns [org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element = null] : (a0= TEXT ) ;
    public final org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return element; }

            // Request.g:5776:4: ( (a0= TEXT ) )
            // Request.g:5777:4: (a0= TEXT )
            {
            // Request.g:5777:4: (a0= TEXT )
            // Request.g:5778:4: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression4369); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.requests.resource.request.mopp.RequestTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableCallExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.requests.resource.request.IRequestTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.requests.resource.request.IRequestTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            String resolved = (String) resolvedObject;
            org.coolsoftware.coolcomponents.expressions.variables.Variable proxy = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
            collectHiddenTokens(element);
            registerContextDependentProxy(new org.coolsoftware.requests.resource.request.mopp.RequestContextDependentURIFragmentFactory<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableCallExpressionReferredVariableReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), resolved, proxy);
            if (proxy != null) {
            Object value = proxy;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.requests.resource.request.grammar.RequestGrammarInformationProvider.EXP_31_0_0_0, proxy, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1171]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1172]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1173]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1174]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1175]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1176]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1177]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1178]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1179]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1180]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1181]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1182]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1183]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1184]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1185]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1186]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1187]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1188]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1189]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1190]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1191]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1192]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1193]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1194]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1195]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1196]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1197]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1198]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1199]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1200]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1201]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1202]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1203]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1204]);
            addExpectedElement(org.coolsoftware.requests.resource.request.mopp.RequestExpectationConstants.EXPECTATIONS[1205]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 40, parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression"



    // $ANTLR start "parse_org_coolsoftware_ecl_Import"
    // Request.g:5853:1: parse_org_coolsoftware_ecl_Import returns [org.coolsoftware.ecl.Import element = null] : c0= parse_org_coolsoftware_ecl_CcmImport ;
    public final org.coolsoftware.ecl.Import parse_org_coolsoftware_ecl_Import() throws RecognitionException {
        org.coolsoftware.ecl.Import element =  null;

        int parse_org_coolsoftware_ecl_Import_StartIndex = input.index();

        org.coolsoftware.ecl.CcmImport c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return element; }

            // Request.g:5854:4: (c0= parse_org_coolsoftware_ecl_CcmImport )
            // Request.g:5855:4: c0= parse_org_coolsoftware_ecl_CcmImport
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_ecl_CcmImport_in_parse_org_coolsoftware_ecl_Import4393);
            c0=parse_org_coolsoftware_ecl_CcmImport();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 41, parse_org_coolsoftware_ecl_Import_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_Import"



    // $ANTLR start "parse_org_coolsoftware_ecl_EclContract"
    // Request.g:5859:1: parse_org_coolsoftware_ecl_EclContract returns [org.coolsoftware.ecl.EclContract element = null] : (c0= parse_org_coolsoftware_ecl_SWComponentContract |c1= parse_org_coolsoftware_ecl_HWComponentContract );
    public final org.coolsoftware.ecl.EclContract parse_org_coolsoftware_ecl_EclContract() throws RecognitionException {
        org.coolsoftware.ecl.EclContract element =  null;

        int parse_org_coolsoftware_ecl_EclContract_StartIndex = input.index();

        org.coolsoftware.ecl.SWComponentContract c0 =null;

        org.coolsoftware.ecl.HWComponentContract c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 42) ) { return element; }

            // Request.g:5860:0: (c0= parse_org_coolsoftware_ecl_SWComponentContract |c1= parse_org_coolsoftware_ecl_HWComponentContract )
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==43) ) {
                int LA48_1 = input.LA(2);

                if ( (LA48_1==TEXT) ) {
                    int LA48_2 = input.LA(3);

                    if ( (LA48_2==49) ) {
                        int LA48_3 = input.LA(4);

                        if ( (LA48_3==63) ) {
                            alt48=1;
                        }
                        else if ( (LA48_3==48) ) {
                            alt48=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return element;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 48, 3, input);

                            throw nvae;

                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 48, 2, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 48, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;

            }
            switch (alt48) {
                case 1 :
                    // Request.g:5861:0: c0= parse_org_coolsoftware_ecl_SWComponentContract
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4412);
                    c0=parse_org_coolsoftware_ecl_SWComponentContract();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Request.g:5862:2: c1= parse_org_coolsoftware_ecl_HWComponentContract
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4420);
                    c1=parse_org_coolsoftware_ecl_HWComponentContract();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 42, parse_org_coolsoftware_ecl_EclContract_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_EclContract"



    // $ANTLR start "parse_org_coolsoftware_ecl_SWContractClause"
    // Request.g:5866:1: parse_org_coolsoftware_ecl_SWContractClause returns [org.coolsoftware.ecl.SWContractClause element = null] : (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_SWComponentRequirementClause |c2= parse_org_coolsoftware_ecl_HWComponentRequirementClause |c3= parse_org_coolsoftware_ecl_SelfRequirementClause );
    public final org.coolsoftware.ecl.SWContractClause parse_org_coolsoftware_ecl_SWContractClause() throws RecognitionException {
        org.coolsoftware.ecl.SWContractClause element =  null;

        int parse_org_coolsoftware_ecl_SWContractClause_StartIndex = input.index();

        org.coolsoftware.ecl.ProvisionClause c0 =null;

        org.coolsoftware.ecl.SWComponentRequirementClause c1 =null;

        org.coolsoftware.ecl.HWComponentRequirementClause c2 =null;

        org.coolsoftware.ecl.SelfRequirementClause c3 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 43) ) { return element; }

            // Request.g:5867:0: (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_SWComponentRequirementClause |c2= parse_org_coolsoftware_ecl_HWComponentRequirementClause |c3= parse_org_coolsoftware_ecl_SelfRequirementClause )
            int alt49=4;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==59) ) {
                alt49=1;
            }
            else if ( (LA49_0==60) ) {
                switch ( input.LA(2) ) {
                case 42:
                    {
                    alt49=2;
                    }
                    break;
                case 61:
                    {
                    alt49=3;
                    }
                    break;
                case TEXT:
                    {
                    alt49=4;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 49, 2, input);

                    throw nvae;

                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;

            }
            switch (alt49) {
                case 1 :
                    // Request.g:5868:0: c0= parse_org_coolsoftware_ecl_ProvisionClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_SWContractClause4439);
                    c0=parse_org_coolsoftware_ecl_ProvisionClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Request.g:5869:2: c1= parse_org_coolsoftware_ecl_SWComponentRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4447);
                    c1=parse_org_coolsoftware_ecl_SWComponentRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Request.g:5870:2: c2= parse_org_coolsoftware_ecl_HWComponentRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4455);
                    c2=parse_org_coolsoftware_ecl_HWComponentRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Request.g:5871:2: c3= parse_org_coolsoftware_ecl_SelfRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_SelfRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4463);
                    c3=parse_org_coolsoftware_ecl_SelfRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 43, parse_org_coolsoftware_ecl_SWContractClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_SWContractClause"



    // $ANTLR start "parse_org_coolsoftware_ecl_HWContractClause"
    // Request.g:5875:1: parse_org_coolsoftware_ecl_HWContractClause returns [org.coolsoftware.ecl.HWContractClause element = null] : (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_HWComponentRequirementClause );
    public final org.coolsoftware.ecl.HWContractClause parse_org_coolsoftware_ecl_HWContractClause() throws RecognitionException {
        org.coolsoftware.ecl.HWContractClause element =  null;

        int parse_org_coolsoftware_ecl_HWContractClause_StartIndex = input.index();

        org.coolsoftware.ecl.ProvisionClause c0 =null;

        org.coolsoftware.ecl.HWComponentRequirementClause c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 44) ) { return element; }

            // Request.g:5876:0: (c0= parse_org_coolsoftware_ecl_ProvisionClause |c1= parse_org_coolsoftware_ecl_HWComponentRequirementClause )
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==59) ) {
                alt50=1;
            }
            else if ( (LA50_0==60) ) {
                alt50=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;

            }
            switch (alt50) {
                case 1 :
                    // Request.g:5877:0: c0= parse_org_coolsoftware_ecl_ProvisionClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_HWContractClause4482);
                    c0=parse_org_coolsoftware_ecl_ProvisionClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Request.g:5878:2: c1= parse_org_coolsoftware_ecl_HWComponentRequirementClause
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_HWContractClause4490);
                    c1=parse_org_coolsoftware_ecl_HWComponentRequirementClause();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 44, parse_org_coolsoftware_ecl_HWContractClause_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_ecl_HWContractClause"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_Expression"
    // Request.g:5882:1: parse_org_coolsoftware_coolcomponents_expressions_Expression returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : c= parseop_Expression_level_03 ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parse_org_coolsoftware_coolcomponents_expressions_Expression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_Expression_StartIndex = input.index();

        org.coolsoftware.coolcomponents.expressions.Expression c =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 45) ) { return element; }

            // Request.g:5883:3: (c= parseop_Expression_level_03 )
            // Request.g:5884:3: c= parseop_Expression_level_03
            {
            pushFollow(FOLLOW_parseop_Expression_level_03_in_parse_org_coolsoftware_coolcomponents_expressions_Expression4509);
            c=parseop_Expression_level_03();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c; /* this rule is an expression root */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 45, parse_org_coolsoftware_coolcomponents_expressions_Expression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_Expression"

    // $ANTLR start synpred24_Request
    public final void synpred24_Request_fragment() throws RecognitionException {
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;


        // Request.g:3495:3: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )
        // Request.g:3495:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
        {
        // Request.g:3495:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
        // Request.g:3496:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        {
        a3=(Token)match(input,34,FOLLOW_34_in_synpred24_Request2939); if (state.failed) return ;

        // Request.g:3521:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        // Request.g:3522:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
        {
        pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred24_Request2965);
        a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }

    }
    // $ANTLR end synpred24_Request

    // $ANTLR start synpred25_Request
    public final void synpred25_Request_fragment() throws RecognitionException {
        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;


        // Request.g:3293:2: ( (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? )
        // Request.g:3293:2: (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
        {
        // Request.g:3293:2: (a0= TEXT )
        // Request.g:3294:3: a0= TEXT
        {
        a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_synpred25_Request2828); if (state.failed) return ;

        }


        // Request.g:3365:2: ( (a1= ':' (a2= TYPE_LITERAL ) ) )?
        int alt53=2;
        int LA53_0 = input.LA(1);

        if ( (LA53_0==30) ) {
            alt53=1;
        }
        switch (alt53) {
            case 1 :
                // Request.g:3366:3: (a1= ':' (a2= TYPE_LITERAL ) )
                {
                // Request.g:3366:3: (a1= ':' (a2= TYPE_LITERAL ) )
                // Request.g:3367:4: a1= ':' (a2= TYPE_LITERAL )
                {
                a1=(Token)match(input,30,FOLLOW_30_in_synpred25_Request2858); if (state.failed) return ;

                // Request.g:3381:4: (a2= TYPE_LITERAL )
                // Request.g:3382:5: a2= TYPE_LITERAL
                {
                a2=(Token)match(input,TYPE_LITERAL,FOLLOW_TYPE_LITERAL_in_synpred25_Request2884); if (state.failed) return ;

                }


                }


                }
                break;

        }


        // Request.g:3494:2: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
        int alt54=2;
        int LA54_0 = input.LA(1);

        if ( (LA54_0==34) ) {
            alt54=1;
        }
        switch (alt54) {
            case 1 :
                // Request.g:3495:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                {
                // Request.g:3495:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                // Request.g:3496:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                {
                a3=(Token)match(input,34,FOLLOW_34_in_synpred25_Request2939); if (state.failed) return ;

                // Request.g:3521:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                // Request.g:3522:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                {
                pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred25_Request2965);
                a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                state._fsp--;
                if (state.failed) return ;

                }


                }


                }
                break;

        }


        }

    }
    // $ANTLR end synpred25_Request

    // $ANTLR start synpred26_Request
    public final void synpred26_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:3633:3: ( () a0= 'or' rightArg= parseop_Expression_level_04 )
        // Request.g:3633:3: () a0= 'or' rightArg= parseop_Expression_level_04
        {
        // Request.g:3633:3: ()
        // Request.g:3633:4: 
        {
        }


        a0=(Token)match(input,57,FOLLOW_57_in_synpred26_Request3056); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred26_Request3073);
        rightArg=parseop_Expression_level_04();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred26_Request

    // $ANTLR start synpred27_Request
    public final void synpred27_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:3700:3: ( () a0= 'and' rightArg= parseop_Expression_level_04 )
        // Request.g:3700:3: () a0= 'and' rightArg= parseop_Expression_level_04
        {
        // Request.g:3700:3: ()
        // Request.g:3700:4: 
        {
        }


        a0=(Token)match(input,39,FOLLOW_39_in_synpred27_Request3107); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred27_Request3124);
        rightArg=parseop_Expression_level_04();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred27_Request

    // $ANTLR start synpred28_Request
    public final void synpred28_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:3632:41: ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ )
        // Request.g:3632:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
        {
        // Request.g:3632:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
        int cnt55=0;
        loop55:
        do {
            int alt55=3;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==57) ) {
                alt55=1;
            }
            else if ( (LA55_0==39) ) {
                alt55=2;
            }


            switch (alt55) {
        	case 1 :
        	    // Request.g:3633:3: () a0= 'or' rightArg= parseop_Expression_level_04
        	    {
        	    // Request.g:3633:3: ()
        	    // Request.g:3633:4: 
        	    {
        	    }


        	    a0=(Token)match(input,57,FOLLOW_57_in_synpred28_Request3056); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred28_Request3073);
        	    rightArg=parseop_Expression_level_04();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Request.g:3700:3: () a0= 'and' rightArg= parseop_Expression_level_04
        	    {
        	    // Request.g:3700:3: ()
        	    // Request.g:3700:4: 
        	    {
        	    }


        	    a0=(Token)match(input,39,FOLLOW_39_in_synpred28_Request3107); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred28_Request3124);
        	    rightArg=parseop_Expression_level_04();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt55 >= 1 ) break loop55;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(55, input);
                    throw eee;
            }
            cnt55++;
        } while (true);


        }

    }
    // $ANTLR end synpred28_Request

    // $ANTLR start synpred29_Request
    public final void synpred29_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:3776:2: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )
        // Request.g:3776:2: () a0= 'implies' rightArg= parseop_Expression_level_5
        {
        // Request.g:3776:2: ()
        // Request.g:3776:3: 
        {
        }


        a0=(Token)match(input,50,FOLLOW_50_in_synpred29_Request3186); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_5_in_synpred29_Request3200);
        rightArg=parseop_Expression_level_5();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred29_Request

    // $ANTLR start synpred30_Request
    public final void synpred30_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:3775:38: ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ )
        // Request.g:3775:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
        {
        // Request.g:3775:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
        int cnt56=0;
        loop56:
        do {
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==50) ) {
                alt56=1;
            }


            switch (alt56) {
        	case 1 :
        	    // Request.g:3776:2: () a0= 'implies' rightArg= parseop_Expression_level_5
        	    {
        	    // Request.g:3776:2: ()
        	    // Request.g:3776:3: 
        	    {
        	    }


        	    a0=(Token)match(input,50,FOLLOW_50_in_synpred30_Request3186); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_5_in_synpred30_Request3200);
        	    rightArg=parseop_Expression_level_5();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt56 >= 1 ) break loop56;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(56, input);
                    throw eee;
            }
            cnt56++;
        } while (true);


        }

    }
    // $ANTLR end synpred30_Request

    // $ANTLR start synpred33_Request
    public final void synpred33_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:3950:2: ( () a0= '>' rightArg= parseop_Expression_level_12 )
        // Request.g:3950:2: () a0= '>' rightArg= parseop_Expression_level_12
        {
        // Request.g:3950:2: ()
        // Request.g:3950:2: 
        {
        }


        a0=(Token)match(input,36,FOLLOW_36_in_synpred33_Request3317); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred33_Request3328);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred33_Request

    // $ANTLR start synpred34_Request
    public final void synpred34_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4017:2: ( () a0= '>=' rightArg= parseop_Expression_level_12 )
        // Request.g:4017:2: () a0= '>=' rightArg= parseop_Expression_level_12
        {
        // Request.g:4017:2: ()
        // Request.g:4017:2: 
        {
        }


        a0=(Token)match(input,37,FOLLOW_37_in_synpred34_Request3346); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred34_Request3357);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred34_Request

    // $ANTLR start synpred35_Request
    public final void synpred35_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4084:2: ( () a0= '<' rightArg= parseop_Expression_level_12 )
        // Request.g:4084:2: () a0= '<' rightArg= parseop_Expression_level_12
        {
        // Request.g:4084:2: ()
        // Request.g:4084:2: 
        {
        }


        a0=(Token)match(input,32,FOLLOW_32_in_synpred35_Request3375); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred35_Request3386);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred35_Request

    // $ANTLR start synpred36_Request
    public final void synpred36_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4151:2: ( () a0= '<=' rightArg= parseop_Expression_level_12 )
        // Request.g:4151:2: () a0= '<=' rightArg= parseop_Expression_level_12
        {
        // Request.g:4151:2: ()
        // Request.g:4151:2: 
        {
        }


        a0=(Token)match(input,33,FOLLOW_33_in_synpred36_Request3404); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred36_Request3415);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred36_Request

    // $ANTLR start synpred37_Request
    public final void synpred37_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:3949:39: ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ )
        // Request.g:3949:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
        {
        // Request.g:3949:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
        int cnt57=0;
        loop57:
        do {
            int alt57=5;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt57=1;
                }
                break;
            case 37:
                {
                alt57=2;
                }
                break;
            case 32:
                {
                alt57=3;
                }
                break;
            case 33:
                {
                alt57=4;
                }
                break;

            }

            switch (alt57) {
        	case 1 :
        	    // Request.g:3950:0: () a0= '>' rightArg= parseop_Expression_level_12
        	    {
        	    // Request.g:3950:2: ()
        	    // Request.g:3950:2: 
        	    {
        	    }


        	    a0=(Token)match(input,36,FOLLOW_36_in_synpred37_Request3317); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred37_Request3328);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Request.g:4017:0: () a0= '>=' rightArg= parseop_Expression_level_12
        	    {
        	    // Request.g:4017:2: ()
        	    // Request.g:4017:2: 
        	    {
        	    }


        	    a0=(Token)match(input,37,FOLLOW_37_in_synpred37_Request3346); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred37_Request3357);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 3 :
        	    // Request.g:4084:0: () a0= '<' rightArg= parseop_Expression_level_12
        	    {
        	    // Request.g:4084:2: ()
        	    // Request.g:4084:2: 
        	    {
        	    }


        	    a0=(Token)match(input,32,FOLLOW_32_in_synpred37_Request3375); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred37_Request3386);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 4 :
        	    // Request.g:4151:0: () a0= '<=' rightArg= parseop_Expression_level_12
        	    {
        	    // Request.g:4151:2: ()
        	    // Request.g:4151:2: 
        	    {
        	    }


        	    a0=(Token)match(input,33,FOLLOW_33_in_synpred37_Request3404); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred37_Request3415);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt57 >= 1 ) break loop57;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(57, input);
                    throw eee;
            }
            cnt57++;
        } while (true);


        }

    }
    // $ANTLR end synpred37_Request

    // $ANTLR start synpred38_Request
    public final void synpred38_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4227:2: ( () a0= '==' rightArg= parseop_Expression_level_19 )
        // Request.g:4227:2: () a0= '==' rightArg= parseop_Expression_level_19
        {
        // Request.g:4227:2: ()
        // Request.g:4227:2: 
        {
        }


        a0=(Token)match(input,35,FOLLOW_35_in_synpred38_Request3466); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred38_Request3477);
        rightArg=parseop_Expression_level_19();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred38_Request

    // $ANTLR start synpred39_Request
    public final void synpred39_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4294:2: ( () a0= '!=' rightArg= parseop_Expression_level_19 )
        // Request.g:4294:2: () a0= '!=' rightArg= parseop_Expression_level_19
        {
        // Request.g:4294:2: ()
        // Request.g:4294:2: 
        {
        }


        a0=(Token)match(input,17,FOLLOW_17_in_synpred39_Request3495); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred39_Request3506);
        rightArg=parseop_Expression_level_19();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred39_Request

    // $ANTLR start synpred40_Request
    public final void synpred40_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4226:39: ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ )
        // Request.g:4226:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
        {
        // Request.g:4226:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
        int cnt58=0;
        loop58:
        do {
            int alt58=3;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==35) ) {
                alt58=1;
            }
            else if ( (LA58_0==17) ) {
                alt58=2;
            }


            switch (alt58) {
        	case 1 :
        	    // Request.g:4227:0: () a0= '==' rightArg= parseop_Expression_level_19
        	    {
        	    // Request.g:4227:2: ()
        	    // Request.g:4227:2: 
        	    {
        	    }


        	    a0=(Token)match(input,35,FOLLOW_35_in_synpred40_Request3466); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred40_Request3477);
        	    rightArg=parseop_Expression_level_19();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Request.g:4294:0: () a0= '!=' rightArg= parseop_Expression_level_19
        	    {
        	    // Request.g:4294:2: ()
        	    // Request.g:4294:2: 
        	    {
        	    }


        	    a0=(Token)match(input,17,FOLLOW_17_in_synpred40_Request3495); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred40_Request3506);
        	    rightArg=parseop_Expression_level_19();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt58 >= 1 ) break loop58;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(58, input);
                    throw eee;
            }
            cnt58++;
        } while (true);


        }

    }
    // $ANTLR end synpred40_Request

    // $ANTLR start synpred41_Request
    public final void synpred41_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4370:2: ( () a0= '^' rightArg= parseop_Expression_level_21 )
        // Request.g:4370:2: () a0= '^' rightArg= parseop_Expression_level_21
        {
        // Request.g:4370:2: ()
        // Request.g:4370:2: 
        {
        }


        a0=(Token)match(input,38,FOLLOW_38_in_synpred41_Request3557); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_21_in_synpred41_Request3568);
        rightArg=parseop_Expression_level_21();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred41_Request

    // $ANTLR start synpred42_Request
    public final void synpred42_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4369:39: ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ )
        // Request.g:4369:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
        {
        // Request.g:4369:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
        int cnt59=0;
        loop59:
        do {
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==38) ) {
                alt59=1;
            }


            switch (alt59) {
        	case 1 :
        	    // Request.g:4370:0: () a0= '^' rightArg= parseop_Expression_level_21
        	    {
        	    // Request.g:4370:2: ()
        	    // Request.g:4370:2: 
        	    {
        	    }


        	    a0=(Token)match(input,38,FOLLOW_38_in_synpred42_Request3557); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_21_in_synpred42_Request3568);
        	    rightArg=parseop_Expression_level_21();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt59 >= 1 ) break loop59;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(59, input);
                    throw eee;
            }
            cnt59++;
        } while (true);


        }

    }
    // $ANTLR end synpred42_Request

    // $ANTLR start synpred43_Request
    public final void synpred43_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4446:2: ( () a0= '+' rightArg= parseop_Expression_level_22 )
        // Request.g:4446:2: () a0= '+' rightArg= parseop_Expression_level_22
        {
        // Request.g:4446:2: ()
        // Request.g:4446:2: 
        {
        }


        a0=(Token)match(input,22,FOLLOW_22_in_synpred43_Request3619); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred43_Request3630);
        rightArg=parseop_Expression_level_22();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred43_Request

    // $ANTLR start synpred44_Request
    public final void synpred44_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4513:2: ( () a0= '-' rightArg= parseop_Expression_level_22 )
        // Request.g:4513:2: () a0= '-' rightArg= parseop_Expression_level_22
        {
        // Request.g:4513:2: ()
        // Request.g:4513:2: 
        {
        }


        a0=(Token)match(input,25,FOLLOW_25_in_synpred44_Request3648); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred44_Request3659);
        rightArg=parseop_Expression_level_22();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred44_Request

    // $ANTLR start synpred45_Request
    public final void synpred45_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4445:39: ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ )
        // Request.g:4445:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
        {
        // Request.g:4445:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
        int cnt60=0;
        loop60:
        do {
            int alt60=3;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==22) ) {
                alt60=1;
            }
            else if ( (LA60_0==25) ) {
                alt60=2;
            }


            switch (alt60) {
        	case 1 :
        	    // Request.g:4446:0: () a0= '+' rightArg= parseop_Expression_level_22
        	    {
        	    // Request.g:4446:2: ()
        	    // Request.g:4446:2: 
        	    {
        	    }


        	    a0=(Token)match(input,22,FOLLOW_22_in_synpred45_Request3619); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred45_Request3630);
        	    rightArg=parseop_Expression_level_22();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Request.g:4513:0: () a0= '-' rightArg= parseop_Expression_level_22
        	    {
        	    // Request.g:4513:2: ()
        	    // Request.g:4513:2: 
        	    {
        	    }


        	    a0=(Token)match(input,25,FOLLOW_25_in_synpred45_Request3648); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred45_Request3659);
        	    rightArg=parseop_Expression_level_22();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt60 >= 1 ) break loop60;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(60, input);
                    throw eee;
            }
            cnt60++;
        } while (true);


        }

    }
    // $ANTLR end synpred45_Request

    // $ANTLR start synpred46_Request
    public final void synpred46_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4589:2: ( () a0= '*' rightArg= parseop_Expression_level_80 )
        // Request.g:4589:2: () a0= '*' rightArg= parseop_Expression_level_80
        {
        // Request.g:4589:2: ()
        // Request.g:4589:2: 
        {
        }


        a0=(Token)match(input,21,FOLLOW_21_in_synpred46_Request3710); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred46_Request3721);
        rightArg=parseop_Expression_level_80();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred46_Request

    // $ANTLR start synpred47_Request
    public final void synpred47_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4656:2: ( () a0= '/' rightArg= parseop_Expression_level_80 )
        // Request.g:4656:2: () a0= '/' rightArg= parseop_Expression_level_80
        {
        // Request.g:4656:2: ()
        // Request.g:4656:2: 
        {
        }


        a0=(Token)match(input,29,FOLLOW_29_in_synpred47_Request3739); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred47_Request3750);
        rightArg=parseop_Expression_level_80();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred47_Request

    // $ANTLR start synpred48_Request
    public final void synpred48_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:4588:39: ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ )
        // Request.g:4588:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
        {
        // Request.g:4588:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
        int cnt61=0;
        loop61:
        do {
            int alt61=3;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==21) ) {
                alt61=1;
            }
            else if ( (LA61_0==29) ) {
                alt61=2;
            }


            switch (alt61) {
        	case 1 :
        	    // Request.g:4589:0: () a0= '*' rightArg= parseop_Expression_level_80
        	    {
        	    // Request.g:4589:2: ()
        	    // Request.g:4589:2: 
        	    {
        	    }


        	    a0=(Token)match(input,21,FOLLOW_21_in_synpred48_Request3710); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred48_Request3721);
        	    rightArg=parseop_Expression_level_80();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Request.g:4656:0: () a0= '/' rightArg= parseop_Expression_level_80
        	    {
        	    // Request.g:4656:2: ()
        	    // Request.g:4656:2: 
        	    {
        	    }


        	    a0=(Token)match(input,29,FOLLOW_29_in_synpred48_Request3739); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred48_Request3750);
        	    rightArg=parseop_Expression_level_80();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt61 >= 1 ) break loop61;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(61, input);
                    throw eee;
            }
            cnt61++;
        } while (true);


        }

    }
    // $ANTLR end synpred48_Request

    // $ANTLR start synpred52_Request
    public final void synpred52_Request_fragment() throws RecognitionException {
        Token a0=null;

        // Request.g:4883:4: (a0= '++' )
        // Request.g:4883:4: a0= '++'
        {
        a0=(Token)match(input,23,FOLLOW_23_in_synpred52_Request3901); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred52_Request

    // $ANTLR start synpred53_Request
    public final void synpred53_Request_fragment() throws RecognitionException {
        Token a0=null;

        // Request.g:4951:4: (a0= '--' )
        // Request.g:4951:4: a0= '--'
        {
        a0=(Token)match(input,26,FOLLOW_26_in_synpred53_Request3916); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred53_Request

    // $ANTLR start synpred54_Request
    public final void synpred54_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;


        // Request.g:5019:4: (a0= '=' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
        // Request.g:5019:4: a0= '=' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        {
        a0=(Token)match(input,34,FOLLOW_34_in_synpred54_Request3931); if (state.failed) return ;

        // Request.g:5044:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        // Request.g:5045:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
        {
        pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred54_Request3944);
        a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

        state._fsp--;
        if (state.failed) return ;

        }


        }

    }
    // $ANTLR end synpred54_Request

    // $ANTLR start synpred55_Request
    public final void synpred55_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:5133:2: ( () a0= '+=' rightArg= parseop_Expression_level_91 )
        // Request.g:5133:2: () a0= '+=' rightArg= parseop_Expression_level_91
        {
        // Request.g:5133:2: ()
        // Request.g:5133:2: 
        {
        }


        a0=(Token)match(input,24,FOLLOW_24_in_synpred55_Request3995); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred55_Request4006);
        rightArg=parseop_Expression_level_91();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred55_Request

    // $ANTLR start synpred56_Request
    public final void synpred56_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:5200:2: ( () a0= '-=' rightArg= parseop_Expression_level_91 )
        // Request.g:5200:2: () a0= '-=' rightArg= parseop_Expression_level_91
        {
        // Request.g:5200:2: ()
        // Request.g:5200:2: 
        {
        }


        a0=(Token)match(input,27,FOLLOW_27_in_synpred56_Request4024); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred56_Request4035);
        rightArg=parseop_Expression_level_91();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred56_Request

    // $ANTLR start synpred57_Request
    public final void synpred57_Request_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Request.g:5132:39: ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ )
        // Request.g:5132:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
        {
        // Request.g:5132:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
        int cnt62=0;
        loop62:
        do {
            int alt62=3;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==24) ) {
                alt62=1;
            }
            else if ( (LA62_0==27) ) {
                alt62=2;
            }


            switch (alt62) {
        	case 1 :
        	    // Request.g:5133:0: () a0= '+=' rightArg= parseop_Expression_level_91
        	    {
        	    // Request.g:5133:2: ()
        	    // Request.g:5133:2: 
        	    {
        	    }


        	    a0=(Token)match(input,24,FOLLOW_24_in_synpred57_Request3995); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred57_Request4006);
        	    rightArg=parseop_Expression_level_91();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Request.g:5200:0: () a0= '-=' rightArg= parseop_Expression_level_91
        	    {
        	    // Request.g:5200:2: ()
        	    // Request.g:5200:2: 
        	    {
        	    }


        	    a0=(Token)match(input,27,FOLLOW_27_in_synpred57_Request4024); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred57_Request4035);
        	    rightArg=parseop_Expression_level_91();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt62 >= 1 ) break loop62;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(62, input);
                    throw eee;
            }
            cnt62++;
        } while (true);


        }

    }
    // $ANTLR end synpred57_Request

    // Delegated rules

    public final boolean synpred38_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred38_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred57_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred57_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred30_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred30_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred40_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred40_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred37_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred37_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred45_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred45_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred52_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred52_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred29_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred29_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred48_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred48_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred39_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred39_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred26_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred26_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred28_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred28_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred27_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred27_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred41_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred41_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred42_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred42_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred43_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred43_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred47_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred47_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred33_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred33_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred25_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred25_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred53_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred53_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred34_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred34_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred44_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred44_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred54_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred54_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred46_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred46_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred35_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred35_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred56_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred56_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred55_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred55_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred36_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred36_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred24_Request() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred24_Request_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_parse_org_coolsoftware_requests_Request_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_requests_Import_in_parse_org_coolsoftware_requests_Request119 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_requests_Platform_in_parse_org_coolsoftware_requests_Request141 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_parse_org_coolsoftware_requests_Request159 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_requests_Request177 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_parse_org_coolsoftware_requests_Request198 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_requests_Request216 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_parse_org_coolsoftware_requests_Request237 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_requests_Request251 = new BitSet(new long[]{0x0000000000002000L,0x0000000000000010L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_requests_MetaParamValue_in_parse_org_coolsoftware_requests_Request274 = new BitSet(new long[]{0x0000000000002000L,0x0000000000000010L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_requests_Request309 = new BitSet(new long[]{0x0000000000002000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_requests_Request335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_requests_MetaParamValue368 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_parse_org_coolsoftware_requests_MetaParamValue389 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_coolsoftware_requests_MetaParamValue407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_parse_org_coolsoftware_requests_Import443 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_org_coolsoftware_requests_Import457 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_FILE_NAME_in_parse_org_coolsoftware_requests_Import475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_parse_org_coolsoftware_requests_Platform511 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_58_in_parse_org_coolsoftware_requests_Platform525 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_FILE_NAME_in_parse_org_coolsoftware_requests_Platform543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Import_in_parse_org_coolsoftware_ecl_EclFile588 = new BitSet(new long[]{0x0008080000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_EclContract_in_parse_org_coolsoftware_ecl_EclFile623 = new BitSet(new long[]{0x0000080000000002L});
    public static final BitSet FOLLOW_51_in_parse_org_coolsoftware_ecl_CcmImport664 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_parse_org_coolsoftware_ecl_CcmImport678 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_QUOTED_91_93_in_parse_org_coolsoftware_ecl_CcmImport696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_parse_org_coolsoftware_ecl_SWComponentContract732 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract750 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_49_in_parse_org_coolsoftware_ecl_SWComponentContract771 = new BitSet(new long[]{0x8000000000000000L});
    public static final BitSet FOLLOW_63_in_parse_org_coolsoftware_ecl_SWComponentContract785 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract803 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_parse_org_coolsoftware_ecl_SWComponentContract824 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentContract842 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_ecl_SWComponentContract863 = new BitSet(new long[]{0x00A0000000000000L});
    public static final BitSet FOLLOW_53_in_parse_org_coolsoftware_ecl_SWComponentContract886 = new BitSet(new long[]{0x0080000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_SWComponentContract927 = new BitSet(new long[]{0x0080000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWContractMode_in_parse_org_coolsoftware_ecl_SWComponentContract1016 = new BitSet(new long[]{0x0080000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_ecl_SWComponentContract1057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_parse_org_coolsoftware_ecl_HWComponentContract1086 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract1104 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_49_in_parse_org_coolsoftware_ecl_HWComponentContract1125 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_parse_org_coolsoftware_ecl_HWComponentContract1139 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentContract1157 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_ecl_HWComponentContract1178 = new BitSet(new long[]{0x00A0000000000000L});
    public static final BitSet FOLLOW_53_in_parse_org_coolsoftware_ecl_HWComponentContract1201 = new BitSet(new long[]{0x0080000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_ecl_HWComponentContract1242 = new BitSet(new long[]{0x0080000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWContractMode_in_parse_org_coolsoftware_ecl_HWComponentContract1331 = new BitSet(new long[]{0x0080000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_ecl_HWComponentContract1372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_parse_org_coolsoftware_ecl_SWContractMode1401 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWContractMode1419 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_ecl_SWContractMode1440 = new BitSet(new long[]{0x1800000000000000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWContractClause_in_parse_org_coolsoftware_ecl_SWContractMode1469 = new BitSet(new long[]{0x1800000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_ecl_SWContractMode1510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_parse_org_coolsoftware_ecl_HWContractMode1539 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWContractMode1557 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_ecl_HWContractMode1578 = new BitSet(new long[]{0x1800000000000000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWContractClause_in_parse_org_coolsoftware_ecl_HWContractMode1607 = new BitSet(new long[]{0x1800000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_ecl_HWContractMode1648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_parse_org_coolsoftware_ecl_ProvisionClause1677 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_ProvisionClause1695 = new BitSet(new long[]{0x0050000100000002L});
    public static final BitSet FOLLOW_54_in_parse_org_coolsoftware_ecl_ProvisionClause1725 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1751 = new BitSet(new long[]{0x0010000100000002L});
    public static final BitSet FOLLOW_52_in_parse_org_coolsoftware_ecl_ProvisionClause1801 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_ProvisionClause1827 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_ProvisionClause1877 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1918 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1932 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1950 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause1971 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause2000 = new BitSet(new long[]{0x0000000000002000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_ecl_SWComponentRequirementClause2041 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2070 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_61_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2084 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2102 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2123 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2152 = new BitSet(new long[]{0x0000000000002000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_ecl_HWComponentRequirementClause2193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_parse_org_coolsoftware_ecl_SelfRequirementClause2222 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_PropertyRequirementClause_in_parse_org_coolsoftware_ecl_SelfRequirementClause2240 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2277 = new BitSet(new long[]{0x0050000100000002L});
    public static final BitSet FOLLOW_54_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2307 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2333 = new BitSet(new long[]{0x0010000100000002L});
    public static final BitSet FOLLOW_52_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2383 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2409 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_FormulaTemplate_in_parse_org_coolsoftware_ecl_PropertyRequirementClause2459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_parse_org_coolsoftware_ecl_FormulaTemplate2500 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2518 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_parse_org_coolsoftware_ecl_FormulaTemplate2539 = new BitSet(new long[]{0x0000000000102000L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_FormulaTemplate2568 = new BitSet(new long[]{0x0000000000102000L});
    public static final BitSet FOLLOW_20_in_parse_org_coolsoftware_ecl_FormulaTemplate2614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_ecl_Metaparameter2647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_67_in_parse_org_coolsoftware_coolcomponents_expressions_Block2683 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2701 = new BitSet(new long[]{0x0000000080000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_31_in_parse_org_coolsoftware_coolcomponents_expressions_Block2728 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block2754 = new BitSet(new long[]{0x0000000080000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_68_in_parse_org_coolsoftware_coolcomponents_expressions_Block2795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2828 = new BitSet(new long[]{0x0000000440000002L});
    public static final BitSet FOLLOW_30_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2858 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_TYPE_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2884 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2939 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable2965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_Metaparameter_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable3011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_033036 = new BitSet(new long[]{0x0200008000000002L});
    public static final BitSet FOLLOW_57_in_parseop_Expression_level_033056 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_033073 = new BitSet(new long[]{0x0200008000000002L});
    public static final BitSet FOLLOW_39_in_parseop_Expression_level_033107 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_033124 = new BitSet(new long[]{0x0200008000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_043170 = new BitSet(new long[]{0x0004000000000002L});
    public static final BitSet FOLLOW_50_in_parseop_Expression_level_043186 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_043200 = new BitSet(new long[]{0x0004000000000002L});
    public static final BitSet FOLLOW_46_in_parseop_Expression_level_53241 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_53252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_parseop_Expression_level_53261 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_53272 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_53282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113304 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_36_in_parseop_Expression_level_113317 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113328 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_37_in_parseop_Expression_level_113346 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113357 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_32_in_parseop_Expression_level_113375 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113386 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_33_in_parseop_Expression_level_113404 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_113415 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123453 = new BitSet(new long[]{0x0000000800020002L});
    public static final BitSet FOLLOW_35_in_parseop_Expression_level_123466 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123477 = new BitSet(new long[]{0x0000000800020002L});
    public static final BitSet FOLLOW_17_in_parseop_Expression_level_123495 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_123506 = new BitSet(new long[]{0x0000000800020002L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193544 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_38_in_parseop_Expression_level_193557 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_193568 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213606 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_22_in_parseop_Expression_level_213619 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213630 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_25_in_parseop_Expression_level_213648 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_213659 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223697 = new BitSet(new long[]{0x0000000020200002L});
    public static final BitSet FOLLOW_21_in_parseop_Expression_level_223710 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223721 = new BitSet(new long[]{0x0000000020200002L});
    public static final BitSet FOLLOW_29_in_parseop_Expression_level_223739 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_223750 = new BitSet(new long[]{0x0000000020200002L});
    public static final BitSet FOLLOW_62_in_parseop_Expression_level_803788 = new BitSet(new long[]{0x0100800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803799 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_parseop_Expression_level_803808 = new BitSet(new long[]{0x0100800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803819 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_803829 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_parseop_Expression_level_873851 = new BitSet(new long[]{0x0000800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_873872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_90_in_parseop_Expression_level_883894 = new BitSet(new long[]{0x0000000404800002L});
    public static final BitSet FOLLOW_23_in_parseop_Expression_level_883901 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_parseop_Expression_level_883916 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_parseop_Expression_level_883931 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parseop_Expression_level_883944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_903982 = new BitSet(new long[]{0x0000000009000002L});
    public static final BitSet FOLLOW_24_in_parseop_Expression_level_903995 = new BitSet(new long[]{0x0000800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_904006 = new BitSet(new long[]{0x0000000009000002L});
    public static final BitSet FOLLOW_27_in_parseop_Expression_level_904024 = new BitSet(new long[]{0x0000800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_904035 = new BitSet(new long[]{0x0000000009000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_in_parseop_Expression_level_914073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_in_parseop_Expression_level_914081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_in_parseop_Expression_level_914089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_in_parseop_Expression_level_914097 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_in_parseop_Expression_level_914105 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_in_parseop_Expression_level_914113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_in_parseop_Expression_level_914121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INTEGER_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression4145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression4175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression4207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression4216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression4248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression4276 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression4289 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression4301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4327 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration4340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression4369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_CcmImport_in_parse_org_coolsoftware_ecl_Import4393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWComponentContract_in_parse_org_coolsoftware_ecl_EclContract4420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_SWContractClause4439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_SelfRequirementClause_in_parse_org_coolsoftware_ecl_SWContractClause4463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_ProvisionClause_in_parse_org_coolsoftware_ecl_HWContractClause4482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_ecl_HWComponentRequirementClause_in_parse_org_coolsoftware_ecl_HWContractClause4490 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_03_in_parse_org_coolsoftware_coolcomponents_expressions_Expression4509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_synpred24_Request2939 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred24_Request2965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_synpred25_Request2828 = new BitSet(new long[]{0x0000000440000002L});
    public static final BitSet FOLLOW_30_in_synpred25_Request2858 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_TYPE_LITERAL_in_synpred25_Request2884 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34_in_synpred25_Request2939 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred25_Request2965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_57_in_synpred26_Request3056 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred26_Request3073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_synpred27_Request3107 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred27_Request3124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_57_in_synpred28_Request3056 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred28_Request3073 = new BitSet(new long[]{0x0200008000000002L});
    public static final BitSet FOLLOW_39_in_synpred28_Request3107 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred28_Request3124 = new BitSet(new long[]{0x0200008000000002L});
    public static final BitSet FOLLOW_50_in_synpred29_Request3186 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_synpred29_Request3200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_synpred30_Request3186 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_synpred30_Request3200 = new BitSet(new long[]{0x0004000000000002L});
    public static final BitSet FOLLOW_36_in_synpred33_Request3317 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred33_Request3328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_synpred34_Request3346 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred34_Request3357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_synpred35_Request3375 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred35_Request3386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_synpred36_Request3404 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred36_Request3415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_synpred37_Request3317 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred37_Request3328 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_37_in_synpred37_Request3346 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred37_Request3357 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_32_in_synpred37_Request3375 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred37_Request3386 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_33_in_synpred37_Request3404 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred37_Request3415 = new BitSet(new long[]{0x0000003300000002L});
    public static final BitSet FOLLOW_35_in_synpred38_Request3466 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred38_Request3477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_synpred39_Request3495 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred39_Request3506 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_synpred40_Request3466 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred40_Request3477 = new BitSet(new long[]{0x0000000800020002L});
    public static final BitSet FOLLOW_17_in_synpred40_Request3495 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred40_Request3506 = new BitSet(new long[]{0x0000000800020002L});
    public static final BitSet FOLLOW_38_in_synpred41_Request3557 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_synpred41_Request3568 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_synpred42_Request3557 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_synpred42_Request3568 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_22_in_synpred43_Request3619 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred43_Request3630 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_synpred44_Request3648 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred44_Request3659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_synpred45_Request3619 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred45_Request3630 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_25_in_synpred45_Request3648 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred45_Request3659 = new BitSet(new long[]{0x0000000002400002L});
    public static final BitSet FOLLOW_21_in_synpred46_Request3710 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred46_Request3721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_synpred47_Request3739 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred47_Request3750 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_synpred48_Request3710 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred48_Request3721 = new BitSet(new long[]{0x0000000020200002L});
    public static final BitSet FOLLOW_29_in_synpred48_Request3739 = new BitSet(new long[]{0x4100900000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred48_Request3750 = new BitSet(new long[]{0x0000000020200002L});
    public static final BitSet FOLLOW_23_in_synpred52_Request3901 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_synpred53_Request3916 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_synpred54_Request3931 = new BitSet(new long[]{0x4100D00000052A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred54_Request3944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_synpred55_Request3995 = new BitSet(new long[]{0x0000800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred55_Request4006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_synpred56_Request4024 = new BitSet(new long[]{0x0000800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred56_Request4035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_synpred57_Request3995 = new BitSet(new long[]{0x0000800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred57_Request4006 = new BitSet(new long[]{0x0000000009000002L});
    public static final BitSet FOLLOW_27_in_synpred57_Request4024 = new BitSet(new long[]{0x0000800000042A20L,0x0000000000000006L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred57_Request4035 = new BitSet(new long[]{0x0000000009000002L});

}