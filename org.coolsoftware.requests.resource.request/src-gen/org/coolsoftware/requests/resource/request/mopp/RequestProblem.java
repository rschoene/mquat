/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.mopp;

public class RequestProblem implements org.coolsoftware.requests.resource.request.IRequestProblem {
	
	private String message;
	private org.coolsoftware.requests.resource.request.RequestEProblemType type;
	private org.coolsoftware.requests.resource.request.RequestEProblemSeverity severity;
	private java.util.Collection<org.coolsoftware.requests.resource.request.IRequestQuickFix> quickFixes;
	
	public RequestProblem(String message, org.coolsoftware.requests.resource.request.RequestEProblemType type, org.coolsoftware.requests.resource.request.RequestEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<org.coolsoftware.requests.resource.request.IRequestQuickFix>emptySet());
	}
	
	public RequestProblem(String message, org.coolsoftware.requests.resource.request.RequestEProblemType type, org.coolsoftware.requests.resource.request.RequestEProblemSeverity severity, org.coolsoftware.requests.resource.request.IRequestQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public RequestProblem(String message, org.coolsoftware.requests.resource.request.RequestEProblemType type, org.coolsoftware.requests.resource.request.RequestEProblemSeverity severity, java.util.Collection<org.coolsoftware.requests.resource.request.IRequestQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<org.coolsoftware.requests.resource.request.IRequestQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public org.coolsoftware.requests.resource.request.RequestEProblemType getType() {
		return type;
	}
	
	public org.coolsoftware.requests.resource.request.RequestEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<org.coolsoftware.requests.resource.request.IRequestQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
