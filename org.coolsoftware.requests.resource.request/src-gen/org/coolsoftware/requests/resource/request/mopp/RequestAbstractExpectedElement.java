/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class RequestAbstractExpectedElement implements org.coolsoftware.requests.resource.request.IRequestExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	
	private java.util.Set<org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]>> followers = new java.util.LinkedHashSet<org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]>>();
	
	public RequestAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(org.coolsoftware.requests.resource.request.IRequestExpectedElement follower, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[] path) {
		followers.add(new org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]>(follower, path));
	}
	
	public java.util.Collection<org.coolsoftware.requests.resource.request.util.RequestPair<org.coolsoftware.requests.resource.request.IRequestExpectedElement, org.coolsoftware.requests.resource.request.mopp.RequestContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
