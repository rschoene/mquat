/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.grammar;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class RequestSyntaxElement {
	
	private RequestSyntaxElement[] children;
	private RequestSyntaxElement parent;
	private org.coolsoftware.requests.resource.request.grammar.RequestCardinality cardinality;
	
	public RequestSyntaxElement(org.coolsoftware.requests.resource.request.grammar.RequestCardinality cardinality, RequestSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (RequestSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	public void setParent(RequestSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	public RequestSyntaxElement[] getChildren() {
		if (children == null) {
			return new RequestSyntaxElement[0];
		}
		return children;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public org.coolsoftware.requests.resource.request.grammar.RequestCardinality getCardinality() {
		return cardinality;
	}
	
}
