/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.requests.resource.request.grammar;

public class RequestCompound extends org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement {
	
	public RequestCompound(org.coolsoftware.requests.resource.request.grammar.RequestChoice choice, org.coolsoftware.requests.resource.request.grammar.RequestCardinality cardinality) {
		super(cardinality, new org.coolsoftware.requests.resource.request.grammar.RequestSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
