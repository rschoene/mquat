/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Wrapper for future result of invocation.
 * @author René Schöne
 */
public class EntryPointAndResult implements Serializable {
	private static final long serialVersionUID = 3102999425095008533L;
	private String entryPoint;
	private Future<Serializable> result;
	public EntryPointAndResult() {
	}
	public EntryPointAndResult(String entryPoint, Future<Serializable> result) {
		super();
		this.entryPoint = entryPoint;
		this.result = result;
	}
	public String getEntryPoint() {
		return entryPoint;
	}
	public void setEntryPoint(String entryPoint) {
		this.entryPoint = entryPoint;
	}
	public Future<Serializable> getResult() {
		return result;
	}
	public void setResult(Future<Serializable> result) {
		this.result = result;
	}
	public void setFixedResult(final Serializable s) {
		this.result = new Future<Serializable>() {

			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				return false;
			}

			@Override
			public boolean isCancelled() {
				return false;
			}

			@Override
			public boolean isDone() {
				return true;
			}

			@Override
			public Serializable get() throws InterruptedException,
					ExecutionException {
				return s;
			}

			@Override
			public Serializable get(long timeout, TimeUnit unit)
					throws InterruptedException, ExecutionException,
					TimeoutException {
				return get();
			}
		};
	}
	public Serializable getResultOrException() {
		try { return result.get(); }
		catch (InterruptedException e) { return e; }
		catch (ExecutionException e) { return e; }
	}

	private synchronized void writeObject( java.io.ObjectOutputStream s )
			  throws IOException {
		s.writeObject(entryPoint);
		s.writeObject(result);
		System.out.printf("writeEntryAndResult: entryPoint:%s, result:%s%n", entryPoint, result);
	}

	@SuppressWarnings("unchecked")
	private synchronized void readObject( java.io.ObjectInputStream s )
			  throws IOException, ClassNotFoundException {
		entryPoint = (String) s.readObject();
		result = (Future<Serializable>) s.readObject();
		System.out.printf("readEntryAndResult: entryPoint:%s, result:%s%n", entryPoint, result);
	}

	private static class LockAndCondition {
		Lock l;
		Condition c;
	}
	
	public static class ExecuteFutureResult implements Future<Serializable>, Serializable {
		private static final long serialVersionUID = -1474072138385748749L;
		
		Lock resultLock;
		Condition asyncCallFinished;
		boolean done = false;
		static Object staticObject = new Object();
		static Map<Integer, LockAndCondition> map = new HashMap<Integer, LockAndCondition>();
		static Map<Integer, Serializable> resultsAfterSerialization = new HashMap<Integer, Serializable>();
		static int staticId = 0;
		int id;
		Serializable result;
		
		public ExecuteFutureResult() {
			synchronized (staticObject) {
				id = staticId++;
			}
			System.out.println("*+* New efr, id=" + id);
			initLockAndCondition();
		}

		private void initLockAndCondition() {
			LockAndCondition lac = map.get(id);
			if(lac == null) {
				lac = new LockAndCondition();
				lac.l = new ReentrantLock();
				lac.c = lac.l.newCondition();
				map.put(id, lac);
			}
			asyncCallFinished = lac.c;
			resultLock = lac.l;
		}
		
		private synchronized void writeObject( java.io.ObjectOutputStream s )
				  throws IOException {
			s.writeInt(id);
			s.writeObject(result);
			s.writeBoolean(done);
			System.out.printf("writeFutureResult, id:%d, result:%s, done:%s%n", id, result, done);
		}
		
		private synchronized void readObject( java.io.ObjectInputStream s )
				  throws IOException, ClassNotFoundException {
			id = s.readInt();
			initLockAndCondition();
			result = (Serializable) s.readObject();
			done = s.readBoolean();
			System.out.printf("readFutureResult, id:%d, result:%s, done:%s%n", id, result, done);
		}
	
		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean isCancelled() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean isDone() {
			return done;
		}
	
		@Override
		public Serializable get() throws InterruptedException,
				ExecutionException {
			System.out.println("get, before lock");
			lock();
			if(!done) {
				System.out.println("get, !done, before await (" + asyncCallFinished + ")");
				await();
			}
			unlock();
			System.out.println("get, after unlock");
			return result;
		}
	
		@Override
		public Serializable get(long timeout, TimeUnit unit)
				throws InterruptedException, ExecutionException,
				TimeoutException {
	//		long millis = System.currentTimeMillis() + unit.toMillis(timeout);
	//		while(!done || millis < System.currentTimeMillis()) {
	//			Thread.sleep(100);
	//		}
			System.out.println("getTimeout, before lock");
			lock();
			if(!done) {
				System.out.println("getTimeout, !done, before awaitTimeout");
				asyncCallFinished.await(timeout, unit);
			}
			unlock();
			System.out.println("getTimeout, after unlock");
			return result;
		}
		
		public void lock() {
			System.out.println(">> lock");
			resultLock.lock();
		}
		public void unlock() {
			System.out.println("<< unlock");
			resultLock.unlock();
		}
		public void await() throws InterruptedException {
			asyncCallFinished.await();
		}
		public void signalAll() {
			System.out.println("SignalAll (" + asyncCallFinished + ")");
			asyncCallFinished.signalAll();
		}
		public void setDone() {
			this.done = true;
		}
		public void setResult(Serializable result) {
			this.result = result;
			resultsAfterSerialization.put(id, result);
		}
		public Serializable getResult() {
			if(result == null) {
				Serializable missedResult = resultsAfterSerialization.get(id);
				if(missedResult != null)
					result = missedResult;
			}
			return result;
		}
	};
}

