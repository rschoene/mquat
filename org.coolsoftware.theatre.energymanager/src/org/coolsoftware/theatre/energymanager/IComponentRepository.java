/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * A repository storing components.
 * @author Sebastian Götz
 */
public interface IComponentRepository {

	public abstract URL getComponentTypeLocation(String compType)
			throws MalformedURLException;

	public abstract URL getComponentLocation(String compType, String impl)
			throws MalformedURLException;

	public Map<String, Map<String, String>> getAll();
}