/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.theatre.Resettable;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;

/**
 * A Global Energy Manager is responsible to control the energy auto tuning 
 * process consisting of the of the folling phases: 
 * 
 * <li> identification of valid system configurations </li>
 * <li> assessment of the required energy consumption for each system configuration</li>
 * <li> determination of the most efficient variant</li>
 * <li> construction of a reconfiguration plan</li>
 * <li> system reconfiguration</li>
 * 
 * For this purpose the global energy manager has to fulfill the following tasks:
 * <li> Collecting data from the user and the resource layer
 * <li> management of energy-adaptive applications, its variants and its non-functional properties </li>
 * <li> management of local energy managers</li>
 * <li> optimzing energy-adaptive application against energy consumption and expected qualities </li>
 * 
 * @author Sebastian Cech
 * @author Sebastian Götz
 * @author René Schöne
 */
public interface IGlobalEnergyManager extends Resettable {
	
	/** Whether to execute synchronously. */
	public static final String OPTION_SYNC_CALL = "execute.sync";
	/** Use the LEM at this uri. */
	public static final String OPTION_LEM_URI = "execute.lemUri";
	/** Use the implementation with this name. */
	public static final String OPTION_IMPL_NAME = "execute.implName";
	/** Refer to this task id. */
	public static final String OPTION_TASK_ID = "execute.taskId";
	/** Refer to this job id. */
	public static final String OPTION_JOB_ID = "execute.jobId";

	/** When passing parameters to optimize, use this to seperate parts of the request. */
	public static final String MAIN_DELIMITER = "#";
	/** When passing parameters to optimize, use this to seperate elements within parts of the request. */
	public static final String ELEMENT_DELIMITER = "|";
	/** When passing parameters to optimize, prepend strings with this to mark resources. */
	public static final String RESOURCE_IDENTIFIER = "resource!";
	/** When passing parameters to optimize, use this to seperate component type name from resource name.
	 * Resources are only considered for Types with the given name */
	public static final String RESOURCE_TYPE_DELIMITER = ":";

	/** Key for the profile to use, when passing settings to optimize.
	 * Valid values: "green", "normal", "best". Default: "normal". */
	public static final String SETTINGS_PROFILE = "profile";

	/** 
	 * @return Return the {@link URI} of the global energy manager as String
	 */
	public String getUri();
	
	// REQ 1 energy manager structure

	/**
	 * This method returns a list of identifiers for all energy managers registered in the THEATRE. 
	 */
	public List<URI> getLocalMgrs();
	
	
	/**
	 * This method has to be invoked by an LEM after its container is started
	 * @param container A global identifier of the container
	 * @return <code>true</code> if the registration process is completed successfully and <code>false</code> otherwise.
	 */
	public boolean registerLEM(URI container);
	
	/**
	 * This method has to be invoked by an LEM after all variants have been registered
	 * @param lemURI
	 */
	void setReady(URI lemURI);

	/**
	 * This method has to be invoked by an LEM before a new container is stopped
	 * @param container A global identifier of the container
	 * @return <code>true</code> if the unregistration process is completed successfully and <code>false</code> otherwise.
	 */
	public boolean unregisterLem(URI container);
	
	//REQ 2 access to data from resource and user layer
	/**
	 * @return the GUM needed for acquiring data from the user layer
	 */
	public Object getGlobalUserManager();

	/**
	 * @return the GRM needed for acquiring data from the resource layer
	 */
	public Object getGlobalResourceManager();
	
	//REQ 3 Application architecture and variants
	
	/**
	 * Responsible to register an application based on a unique identifier. After registration the application can
	 * be used and it is under control of the GEM. The appUri provides information about the appliaction itself,
	 * related components and corresponding component archives.
	 * @param appName An unique identifier where application details can be found.
	 * @param containerURI the hosting container
	 * @param structureModel the model describing the software components of this app
	 * @param contracts { typeName -> contract of type }
	 * @return <code>true</code> if the registration process is completed successfully and <code>false</code> otherwise.
	 */
	public boolean registerApp(String appName, URI containerURI, String structureModel, Map<String, String> contracts);
	
	public void registerVariant(String appName, String typeName, String implName, String benchName, String lemURI, String pluginName);
	
	/**
	 * Responsible to unregister an application based on a unique identifier. Before an application can be 
	 * unregistered all existing requests needs to be processed. After unregistration the application will be 
	 * not avaiable any more in THEATRE.
	 * @param appName A unique identifier of the application
	 * @param container A unique identifier of the container as  {@link URI}
	 * @return <code>true</code> if the registration process is completed successfully and <code>false</code> otherwise.
	 */
	public boolean unregister(String appName, URI container);
	
	/**
	 * @return All registered applications as a map of app names and their respective structural models
	 */
	public Map<String, String> getRegisteredApplications();
	
	/**
	 * System configurations can be captured as {@link VariantModel}s. This method provides such models for 
	 * all active applications where each application has a unique identifer.
	 * @return A {@link Map} of {@link VariantModel}s of applications that are currently active.
	 */
	public Map<String, VariantModel> getCurrentSystemConfigurations();
	
	/**
	 * Provides a system configuration of a certain application as {@link VariantModel}. In cases where the 
	 * application is not active, an empty {@link VariantModel} is returned.
	 * @param appURI The unique identifier of the application.
	 * @return a {@link String} A serialized {@link VariantModel} representing the current system configuration of an active application or
	 * an empty model.
	 */
	public String getCurrentSystemConfiguration(String appUri);
	
	//REQ 4 NFPs
	
	/**
	 * @param appUri The application identifier
	 * @return Returns a {@link Map} application property names to be provided by implementations. It contains a {@link List}
	 * of property names for each spcefici component.
	 */
	public Map <String, List<String>> getAllApplicationPropertyNames(String appName);
	
	/**
	 * 
	 * @param appName The application identifier.
	 * @return a list of component type identifiers of the respective app
	 */
	public List<String> getComponentsOfApp(String appName);
	
	/**
	 * 
	 * @param compId The component identifier.
	 * @param appName The application identifier.
	 * @return a list of ports
	 */
	public List<String> getComponentPorts(String appName, String compId);
	
	/**
	 * 
	 * @param compId The component identifier.
	 * @param appName The application identifier.
	 * @param portName The port type identifier
	 * @return a list of parameters in the form "name : type"
	 */
	public List<String> getPortParameters(String appName, String compId, String portName);
	
	/**
	 * 
	 * @param compId The component identifier.
	 * @param appName The application identifier.
	 * @return a list of ports
	 */
	public List<String> getPortsMetaParameters(String appName, String compId, String portName);
	
	/**
	 * 
	 * @param appUri The application identifier.
	 * @param compId the identifier of the component type which defines the properties
	 * @return a list for component specific property names.
	 */
	public List<String> getComponentProperties(String appName, String compId);
	
	//REQ 5 Optimization
	/**
	 * Invokes the optimization process, i.e. one cycle through the energy auto tuning control loop.
	 * @param userRequest String format:
	 *  [ilp|pbo|aco]{@value #MAIN_DELIMITER}appName{@value #MAIN_DELIMITER}compName{@value #MAIN_DELIMITER}
	 *  portName{@value #MAIN_DELIMITER}(NFR{@value #ELEMENT_DELIMITER})*{@value #MAIN_DELIMITER}
	 *  metaparam=value{@value #MAIN_DELIMITER}(settingsKey=settingsValue{@value #ELEMENT_DELIMITER})*
	 * @param returnTaskId if <code>true</code>, returns the taskId and executes most of the optimization
	 *  asynchronously. If <code>false</code>, executes the optimization synchronously and returns a
	 *  {@link ExecuteResult}.
	 * @param params a list of parameters passed to the method on execution
	 * @return {@link ExecuteResult} in serialized form,
	 *  or just the taskId ({@link Long}) if returnTaskId is <code>true</code>
	 */
	public Serializable optimize(String userRequest, boolean returnTaskId, Object... params);

	/**
	 * Invokes the optimization process, i.e. one cycle through the energy auto tuning control loop.
	 * @param userRequest String format:
	 *  [ilp|pbo|aco]{@value #MAIN_DELIMITER}appName{@value #MAIN_DELIMITER}compName{@value #MAIN_DELIMITER}
	 *  portName{@value #MAIN_DELIMITER}(NFR{@value #ELEMENT_DELIMITER})*{@value #MAIN_DELIMITER}
	 *  metaparam=value{@value #MAIN_DELIMITER}(settingsKey=settingsValue{@value #ELEMENT_DELIMITER})*
	 * @param returnTaskId if <code>true</code>, returns the taskId and executes most of the optimization
	 *  asynchronously. If <code>false</code>, executes the optimization synchronously and returns a
	 *  {@link ExecuteResult}.
	 * @param params a list of parameters passed to the method on execution
	 * @return {@link ExecuteResult} in serialized form,
	 *  or just the taskId ({@link Long}) if returnTaskId is <code>true</code>
	 */
	public Serializable run(String userRequest, boolean returnTaskId, boolean optimize, Object... params);

	/**
	 * Blocks at most t units and either returns the result or returns null, if no
	 * result is available.
	 * @param taskId the identifier of the task
	 * @param t the amount of units to wait
	 * @param unit the unit of time
	 * @return the result in serialized form, or <code>null</code> if none available
	 */
	public Serializable getResultOfTask(long taskId, long t, TimeUnit unit);

	public void reconfigure(String plan, String appName);
	
	public InputStream getBundleBySymbolicName(String name);

	/**
	 * Returns the contracts for component for every container.
	 * @param appName name of application
	 * @param compName name of component type
	 * @return Map {containerURI -> contract}
	 */
	public Map<String, String> getContractsForComponent(String appName,
			String compName);

	/**
	 * Returns the contracts for components of an application for every container.
	 * @param appName name of application
	 * @param fileNameMap names of resources to be resolved (mapped from typeName)
	 * @return Map {compName -> {containerURI -> contract}}
	 */
	public Map<String, Map<String, String>> getContractsForApp(String appName, Map<String, List<String>> fileNameMap);

	public String getApplicationModel(String appName);
	
	//XXX maybe move to Utility class
	public void updateContract(String appName, String eclURI, String newSerializedContract);

	public void waitForCurrentBenchmarksToFinish();

	/**
	 * Execute the port of the given component in the given app at the given LEM.
	 * @param lemUri the uri of the LEM to use
	 * @param options a map of options, using constants in {@link IGlobalEnergyManager}. May be <code>null</code> or empty.
	 * @param portName the name of the port to invoke
	 * @param appName the name of the application containing the components
	 * @param compName the name of the component containing the port
	 * @param params the parameters to pass to the method upon call
	 * @return {@link ExecuteResult} in serialized form
	 */
	public Serializable executeAtLem(URI lemUri, Map<String, Object> options, String portName, String appName, String compName,
				Object... params);
		
	public void updateKnowledePlane(String implName, long frequency, double power);

	/** Fetches a component from a LEM and execute the port of it.
	 *  @param options use the constants provied by {@link IGlobalEnergyManager} to specify options for the call.
	 *  Use either <code>null</code> or an empty map to select no options. 
	 *  @return {@link ExecuteResult} in serialized form
	 */
	public Serializable executeCurrentImpl(String appName, String compName, String portName,
			Map<String, Object> options, Object... params);

	/** @return an array of stored task identifiers */
	public Long[] getTaskIds();
	
	/**
	 * Returns the result of the job with the given id, or <code>null</code> if no such job.
	 * @param appName the name of the application containing the components
	 * @param jobId the given id
	 * @return the result or null
	 * @see ILocalEnergyManager#getResult(int)
	 */
	public Serializable getResultOfJob(String appName, long jobId);

}
