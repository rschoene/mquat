/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.util;

import java.io.Serializable;

import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;

/**
 * The result of {@link IGlobalEnergyManager#optimize(String, boolean, Object...)} if returnTaskId was set
 * to <code>false</code>, the operation was finished successfully.
 * @author René Schöne
 */
public class SyncExecuteResult implements ExecuteResult {
	
	private static final long serialVersionUID = 4814624504899154033L;
	
	private Serializable result;
	
	public SyncExecuteResult() {
		// empty
	}
	
	public SyncExecuteResult(Serializable result) {
		this.result = result;
	}

	@Override
	public Serializable getResult() {
		return result;
	}

	@Override
	public boolean isResultReady() {
		// always ready
		return true;
	}
	
	@Override
	public String toString() {
		return "SyncExecuteResult_"+hashCode()+":"+result;
	}

	@Override
	public Serializable serialize() {
		return result;
	}

	@Override
	public void deserialize(Serializable serializedPart) {
		this.result = serializedPart;
	}

}
