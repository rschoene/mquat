/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.util;

import java.io.Serializable;

import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;

/**
 * The result of {@link IGlobalEnergyManager#optimize(String, boolean, Object...)} if
 * the operation was aborted, either during optimization or execution of implementations.
 * @author René Schöne
 */
public class ErrorResult implements ExecuteResult {
	
	private static final long serialVersionUID = 8272548260376681098L;
	
	private Throwable t;
	
	public ErrorResult() {
		// empty
	}

	public ErrorResult(Throwable t) {
		super();
		this.t = t;
	}

	@Override
	public Serializable getResult() {
		return t;
	}

	@Override
	public boolean isResultReady() {
		return true;
	}
	
	@Override
	public String toString() {
		return "ErrorResult_"+hashCode()+":"+t;
	}

	@Override
	public Serializable serialize() {
		return t;
	}

	@Override
	public void deserialize(Serializable serializedPart) {
		t = (Throwable) serializedPart;
	}
	
	public Throwable getError() {
		return t;
	}

}
