/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.util;

import java.io.Serializable;

/**
 * Utility class to serialize and deserialize {@link ExecuteResult ExecuteResults}.
 * @author René Schöne
 */
public class ExecuteResultSerializer {
	
	public static Serializable serialize(ExecuteResult er) {
		return er.serialize();
	}
	
	public static ExecuteResult deserialize(Serializable serializedEr) {
		// only AsyncEr, SyncEr and Error are supported
		if(serializedEr == null) {
			return null;
		}
		ExecuteResult er;
		if(serializedEr instanceof String) {
			er = new AsyncExecuteResult();
		}
		else if(serializedEr instanceof Throwable) {
			er = new ErrorResult();
		}
		else {
			er = new SyncExecuteResult();
		}
		er.deserialize(serializedEr);
		return er;
	}

}
