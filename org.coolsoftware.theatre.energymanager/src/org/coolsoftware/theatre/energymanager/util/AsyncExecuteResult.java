/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.util;

import java.io.Serializable;

import org.coolsoftware.theatre.TheatreActivator;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

/**
 * The result of {@link IGlobalEnergyManager#optimize(String, boolean, Object...)} if returnTaskId was set
 * to <code>true</code>, the operation was finished successfully.
 * @author René Schöne
 */
public class AsyncExecuteResult implements ExecuteResult {
	
	private static final long serialVersionUID = -7147618147358277080L;

	private static final String seperator = "|";
	
	private long taskId;
	private long jobId;
	private String lemURI;
	
	private transient ILocalEnergyManager lem = null;
	private transient BundleContext context = null;

	public AsyncExecuteResult() {
		// empty
	}
	
	public AsyncExecuteResult(long taskId, long jobId, String lemURI) {
		super();
		this.taskId = taskId;
		this.jobId = jobId;
		this.lemURI = lemURI;
	}
	
	private ILocalEnergyManager getLem() {
		if(context == null) {
			context = FrameworkUtil.getBundle(TheatreActivator.class).getBundleContext();
		}
		if(lem == null) {
			RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
			RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
					remote, lemURI, ILocalEnergyManager.class.getName());
			lem = (ILocalEnergyManager) remote.getRemoteService(srefs[0]);
		}
		return lem;
	}

	@Override
	public Serializable getResult() {
		return getLem().getResult(jobId);
	}

	@Override
	public boolean isResultReady() {
		return getLem().isFinished(jobId);
	}
	
	@Override
	public String toString() {
		return "AsyncExecuteResult_"+hashCode()+"("+taskId+" -> "+jobId+"@"+lemURI+")";
	}

	@Override
	public String serialize() {
		return taskId + seperator + jobId + seperator + lemURI;
	}

	@Override
	public void deserialize(Serializable serializedPart) {
		if(!(serializedPart instanceof String)) {
			return;
		}
		int i = 0;
		for(String tok : StringUtils.tokenize((String) serializedPart, seperator)) {
			switch(i) {
			case 0: taskId = Long.parseLong(tok); break;
			case 1: jobId = Long.parseLong(tok); break;
			case 2: lemURI = tok; break;
			default: System.err.println("Superfluous token:" + tok);
			}
			i++;
		}
	}
	
	public long getTaskId() {
		return taskId;
	}
	
	public long getJobId() {
		return jobId;
	}
	
	public String getLemURI() {
		return lemURI;
	}

}
