/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager.util;

/**
 * To be run before actual optimization takes place.
 * Used for deferred computation not to be done before answering a request.
 * @author René Schöne
 */
public interface PreOptimizeRunner {

	public class RequestAndParams {
		public String userRequest;
		public Object[] params;
		public String errorMessage;
		
		public RequestAndParams() {
			errorMessage = null;
		}
		
		public boolean hasErrors() { return errorMessage != null; }
	}
	
	public RequestAndParams update(RequestAndParams rap);

}
