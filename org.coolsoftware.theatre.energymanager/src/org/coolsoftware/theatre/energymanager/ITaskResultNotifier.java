/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

/**
 * Notifier that is called whenever a task finishes.
 * @author René Schöne
 */
public interface ITaskResultNotifier {
	
	/**
	 * Whenever an optimize call finishes, this method is invoked.
	 * A optimize call is the only call which creates tasks, thus the finish
	 * of an optimize call is equivalent with the finish of a task and vice versa.
	 * @param taskResultDescription the description of the result
	 */
	public abstract void afterTaskFinish(TaskResultDescription trd);
}
