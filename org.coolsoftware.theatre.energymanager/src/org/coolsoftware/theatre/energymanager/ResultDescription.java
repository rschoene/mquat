/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

/**
 * Description of a finished item.
 * @author René Schöne
 */
public class ResultDescription {
	
	/** Stores the name of application. */
	public String appName;
	/** Was call successful? */
	public boolean success;
	/** Timestamp of finish (millis since epoch) */
	public long endTime;
	/** the raw result of the call */
	public Object rawObject;
	/** If {@link #rawObject} is a FileProxy, this contains the filePath. */
	public String filePath;
	/** If {@link #rawObject} is a FileProxy, this contains the hostname (without r-osgi stuff). */
	public String hostname;

}
