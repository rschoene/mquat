/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

import java.util.HashMap;
import java.util.Map;

/**
 * The planner component responsible for scheduling implementations on containers.
 * @author Sebastian Götz
 * @author René Schöne
 */
public interface Optimizer {

	/** Be verbose. Type: Boolean. Default: False. */
	public static final String OPTION_VERBOSE = "optimizer.verbose";
	/** Name of the application (required). Type: String. */
	public static final String OPTION_APPNAME = "optimizerp.name.ap";
	/** Map from type names (of CopyFileTypes) to resources.
	 * Type: Map&lt;String, List&lt;String&gt;&gt;. Default: empty map. */
	public static final String OPTION_RESOURCENAMES = "optimizer.name.resources";
	/** Name of the problem. Type: Boolean. Default: Provided application name. */
	public static final String OPTION_PROBLEMNAME = "optimizer.name.problem";
	/** Profile to be used (use constants provided in {@link Optimizer}). Type: String. Default: {@link #PROFILE_NORMAL}. */
	public static final String OPTION_PROFILE = "optimizer.profile";

	/** Save some energy, i.e. do nothing? */
	public static final String PROFILE_ENERGY_SAVING = "green";
	/** Nothing special, find a good way between energy and utility. */
	public static final String PROFILE_NORMAL = "normal";
	/** Always provide best utility. */
	public static final String PROFILE_UTILITY = "best";
	/** The maximum time in seconds to run an operation (generate or solve). Default: -1 for no timeout. */
	public static final String OPTION_TIMEOUT = "timeout";

	public class Mapping extends HashMap<String, String> {
		private static final long serialVersionUID = -5729442792961011844L;

		private static final TimeInfo EMPTY_TIME_INFO = new TimeInfo();
		
		private final Map<String, TimeInfo> times;
		private String errorMessage;

		/**
		 * Creates an empty Mapping without errors nor mapping information.
		 */
		public Mapping() {
			this(null);
		}
		
		/**
		 * Creates an erroneous Mapping, containing no mapping information, but the given error message.
		 * @param errorMessage the message why the optimizer failed
		 */
		public Mapping(String errorMessage) {
			this.errorMessage = errorMessage;
			this.times = new HashMap<>();
		}
		
		public void setStartTime(String compName, long startTime) {
			TimeInfo ti = getInfo(compName, true);
			ti.startTime = startTime;
		}
		public void setRunTime(String compName, double runTime) {
			TimeInfo ti = getInfo(compName, true);
			ti.runTime = runTime;
		}
		public void setResponseTime(String compName, double responseTime) {
			TimeInfo ti = getInfo(compName, true);
			ti.responseTime = responseTime;
		}
		private TimeInfo getInfo(String compName, boolean create) {
			TimeInfo ti = times.get(compName);
			if(ti == null) {
				if(create) {
					ti = new TimeInfo();
					times.put(compName, ti);
				}
				else {
					ti = EMPTY_TIME_INFO;
				}
			}
			return ti;
		}
		public Long getStartTime(String compName) {
			return getInfo(compName, false).startTime;
		}
		public Double getRunTime(String compName) {
			return getInfo(compName, false).runTime;
		}
		public Double getResponseTime(String compName) {
			return getInfo(compName, false).responseTime;
		}
		public TimeInfo getTimes(String compName) {
			return getInfo(compName, false);
		}
		/** Did the optimizer failed to compute a mapping? */
		public boolean hasErrors() {
			return errorMessage != null;
		}
		/**
		 * @param errorMessage the errorMessage to set
		 */
		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
		/** Get the detailed error message, why the optimizer failed. */
		public String getErrorMessage() {
			return errorMessage;
		}
		@Override
		public String toString() {
			return errorMessage == null ? super.toString() + times.toString() : "BadMapping: " + errorMessage;
		}
	}
	
	public class TimeInfo {
		public Long startTime;
		public Double runTime;
		public Double responseTime;
		@Override
		public String toString() {
			return "TimeInfo [startTime=" + startTime + ", runTime=" + runTime
					+ ", responseTime=" + responseTime + "]";
		}
	}
	
	/**
	 * Calls the optimizer.
	 * args are the following:
	 * <ol>
	 * <li>Request req</li>
	 * <li>Map&lt;String, EclFile&gt; eclFiles</li>
	 * <li>IGlobalEnergyManager gem</li>
	 * <li>Map&lt;String, Object&gt; options, use constanst of {@link Optimizer}</li>
	 * </ol>
	 * @param args an array as described above
	 * @return the mapping { implName -> targetServer }
	 */
	public Mapping optimize(Object[] args);
	
	/**
	 * @return a unique identifier for this optimizer
	 */
	public String getId();
	
}
