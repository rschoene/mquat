/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

/**
 * Notifier that is called whenever a job finishes.
 * @author René Schöne
 */
public interface IJobResultNotifier {

	/**
	 * Whenever a job finishes, this method is invoked.
	 * @param jrd the description of the result
	 */
	public abstract void afterJobFinish(JobResultDescription jrd);
}
