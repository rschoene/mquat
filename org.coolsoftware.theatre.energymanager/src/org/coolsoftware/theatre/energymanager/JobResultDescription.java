/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

/**
 * Description of a finished job.
 * @author René Schöne
 */
public class JobResultDescription extends ResultDescription {

	/** the identifier of the finished job */
	public long jobId;
	
	@Override
	public String toString() {
		return "JobResultDescription [jobId=" + jobId + ", success=" + success
				+ ", endTime=" + endTime + ", rawObject=" + rawObject
				+ ", filePath=" + filePath + ", hostname=" + hostname + "]";
	}
}
