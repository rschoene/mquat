/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

/**
 * Description of a finished task.
 * @author René Schöne
 */
public class TaskResultDescription extends ResultDescription {

	/** @param taskId the identifier of the finished task */
	public long taskId;

	@Override
	public String toString() {
		return "TaskResultDescription [taskId=" + taskId + ", success="
				+ success + ", endTime=" + endTime + ", rawObject=" + rawObject
				+ ", filePath=" + filePath + ", hostname=" + hostname + "]";
	}
}