/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

import java.util.HashSet;
import java.util.Set;

/**
 * Description of a started task.
 * @author René Schöne
 */
public class TaskDescription {
	/** Stores the predicted response time. */
	public Double predictedResponseTime;
	/** Stores the task identifier. */
	public Long taskId;
	/** Stores the executing hosts. */
	public Set<String> hosts = new HashSet<>();
	@Override
	public String toString() {
		return "TaskDescription [predictedResponseTime="
				+ predictedResponseTime + ", taskId=" + taskId + ", hosts="
				+ hosts + "]";
	}
}