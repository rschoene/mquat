/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.energymanager;

/**
 * Description of a started job.
 * @author René Schöne
 */
public class JobDescription {
	/** Stores the name of application. */
	public String appName;
	/** Stores the name of implementation. */
	public String implName;
	/** Stores the predicted duration (in milliseconds). TODO check if really in milliseconds */
	public Double predicted;
	/** Stores the start time (millis since epoch). */
	public Long startTime;
	/** Stores the container on which the impl will run. */
	public String containerName;
	/** Stores the task identifier (per request). */
	public Long taskId;
	/** Stores the job identifier (per impl execution). */
	public Long jobId;
	@Override
	public String toString() {
		return "JobDescription [appName=" + appName + ", implName=" + implName
				+ ", predicted=" + predicted + ", startTime=" + startTime
				+ ", containerName=" + containerName + ", taskId=" + taskId
				+ ", jobId=" + jobId + "]";
	}
}