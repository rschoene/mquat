/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * 
 */
package org.coolsoftware.theatre.energymanager;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.theatre.Resettable;
import org.osgi.framework.BundleException;

/**
 * The interface for local energy managers (LEM), i.e. for energy managers able to manage 
 * software component container. A LEM knows about software components that are deployed on 
 * the container managed by an LEM. Further, for each component it knows about the 
 * corresponding ComponentType.
 * 
 * Data provided by a LEM is used by the {@link IGlobalEnergyManager} for energy auto tuning purposes.
 * 
 * 
 * @author Sebastian Cech
 *
 */
public interface ILocalEnergyManager extends Resettable {
	// REQ 1 energy manager structure
	
	/**
	 * Each energy manager in THEATRE has to be a reference to the global energy manager that can be used to register
	 * a container to the global resource manager. 
	 * It has to be assured that the global energy manager is not <code>null</code>. 
	 * @return The global energy manager.
	 */
	public IGlobalEnergyManager getGlobalMgr();
	
	/**
	 * Sets a new global manager. This will happen most likely during a reconnect.
	 * @param gem the new global manager
	 */
	public void setGlobalMgr(IGlobalEnergyManager gem);
	
	/**
	 * @return provides access to the Uri of the container
	 */
	public String getContainerId();
	
	//REQ 3 Application architecture and variants
	
	/**
	 * Returns a {@link Map} consisting of active components implementations for each ComponentType.
	 * The key attribute represents a unique identifier of the componentType as String. <br>
	 * The value attribute is a {@link List} of {@link String} representing the currently active 
	 * components for a specific type as a unique identifier.
	 * 
	 * @return A {@link Map} of active components ordered to its type
	 */
	public Map <String, String> getActiveComponents();
	
	
	/**
	 * Provides a set of ComponentTypes that are deployed in the container. It is assumed that 
	 * each ComponentType is identified with a unique {@link String}.
	 * This method returns an empty set in cases where no types are registered.
	 * 
	 * @return A {@link Set} of component types managed by this energy manager. 
	 */
	public List<String> getDeployedComponentTypes();
	
	/**
	 * Returns a collection of available component variants for a specific type. In case where no managed component 
	 * variants are available an emtpy collection is returned. The collection consist of {@link String} values
	 * that are unique identifiers of a component
	 * 
	 * @param componentType The unique identifier of the ComponentType for which component implementations should be searched.
	 * 
	 * @return A {@link Collection} of component identifiers as strings
	 */
	public List<String> getDeployedComponents(String componentType);
	
	//REQ 4 NFPs
	
	public Map<String, Object> getComponentProperties(String component);
	
	public Object getComponentProperty(String component);
	
	public String getComponentModes(String component);
	

	public String getCurrentMode(String activeComponent);

	/**
	 * Check whether or not a given SWComponentType is already deployed in this container. 
	 * @param componentType A {@link String} representation of the SWComponentType
	 * @return <code>true</code> if the type is deployed and <code>false</code> otherwise.s
	 */
	boolean isComponentTypeDeployed(String componentType);
	
	/**
	 * The deploy method is responsible to deploy software component implementations as well as 
	 * its dependent components into the local {@link ContainerProvider}. If the component type 
	 * is not deployed, it has to be deployed at first from the {@link IComponentRepository}. 
	 * After that, the implementation is deployed.
	 * @param type Identifies the name of the component type 
	 * @param variant Identifies the name of the implementation
	 * @throws LemException 
	 */
	public void deploy(String type, String variant);

	/**
	 * The undeploy method is responsible to undeploy software component implementations as well as 
	 * its dependent components from the local {@link ContainerProvider}. Undeployment means that the
	 * component is deleted from the container.
	 * This method undeploys the component type in cases where no additional software component 
	 * impementations are availabe in this container. 
	 * @param type Identifies the name of the component type 
	 * @param variant Identifies the name of the implementation
	 */
	public void undeploy(String type, String variant);

	public void replace(String type, String srcComp, String trgtComp);

	public void migrate(String type, String srcComp, String trgtContainer);

	public void replace(String type, String oldComp, String newComp, String targetContainer);

	
	/**This method invokes the benchmarking process to derive the formulas of contract templates.  
	 * 
	 * @param contract the contract template containing template functions subject to benchmarking
	 * @return contract with substituted formulas
	 */
	public String computeContract(String appModel, String hwModel, String appName, String eclURI,
			String contract, String implName, String benchmark, String pluginName);

	
	public String getContractByURI(String uri);
	
	/**This method will retrieve a Bundle by its symbolic name from the GEM and start it.
	 * 
	 * @param symbolicName of bundle
	 */
	public void retrieveAndStartBundle(String symbolicName) throws IOException, BundleException;
	
	/**
	 * Executes the method behind the port on the given implementation of the component of the app.
	 * Returns the object, if any. A possible exception is caught inside this method and returned.
	 * Otherwise the return value of the method is returned.
	 * @param pluginName the name of the bundle containing the implementation
	 * @param implName the name of the implementation class
	 * @param portName the name of the port, i.e. the method to be executed
	 * @param appName the name of the top-level component
	 * @param compIsConnector whether the component is of type SWConnectorType
	 * @param compIsCopyType whether the component is of type CopyFileType
	 * @param taskId the id of the task to which this invocation belongs to
	 * @param jobId the id of the job for this invocation
	 * @param portTypes the names and types of the parameters. Each entry must have the form
	 *  <pre>"&lt;name&gt; : &lt;type&gt;"</pre>
	 * @param params a list of parameters passed to the method on execution
	 * @return the result of the invocation at the port
	 */
	public Serializable execute(String pluginName, String implName, String portName, String appName,
			Boolean compIsConnector, Boolean compIsCopyType,
			Long taskId, Long jobId, List<String> portTypes, Object... params);

	/**
	 * Updates the stored contracts while preserving already computed benchmark results.
	 * @param appModelSer the serialized application model
	 * @param hwModelSer the serialized hardware model
	 * @param appName the name of the application
	 * @param eclURI the URI of the ecl file to update
	 * @param newSerializedContract the new serialized contract
	 */
	public abstract void updateContract(String appModelSer, String hwModelSer,
			String appName, String eclURI, String newSerializedContract);

	/**
	 * Remove a job from the local queue.
	 * Currently not used from outside of a LEM.
	 * @param jobId the id of the job to remove
	 */
	public abstract void removeResult(long jobId);

	/**
	 * Get the result of the job. This is either the real result, or an exception.
	 * @param jobId the id of the job
	 * @return the result, or the thrown exception
	 */
	public abstract Serializable getResult(long jobId);

	/**
	 * Check, whether the job is already finished. Failed jobs are finished as well.
	 * @param jobId the id of the job to check
	 * @return <code>true</code> if the job finished (independent of success), or <code>false</code> if it is still executing
	 */
	public abstract boolean isFinished(long jobId);

//	/**
//	 * Draws the next unique, valid callId (strictly increasing within one LEM)
//	 * @return the next unique callId
//	 */
//	public int getNextCallId();
	
	/**
	 * @return a list of all jobIds in the current queue, also includes finished jobs
	 */
	public long[] getJobIdsInQueue();

	/**
	 * @param jobId the id of the job to check
	 * @return the name of the impl to be executed
	 */
	public String getImplName(long jobId);
}
