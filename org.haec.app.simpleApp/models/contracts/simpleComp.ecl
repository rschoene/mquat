import ccm [../Server.structure]
import ccm [../slam.structure]

contract SimpleImpl1 implements software SimpleComponent.doWork {
	mode easy {
		requires resource Server {
			server_energy <fe_sample1(particle_count)>
			energyRate: 1.67}
		provides run_time <fr_sample1(particle_count)>
		provides precision <fp_sample1(particle_count)>
	}

}
