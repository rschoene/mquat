/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

/**
 * Utility methods concerning monitors.
 * @author René Schöne
 */
public class MonitorUtil {

	public static String createFilter(String propertyName) {
		return "("+MonitorUtil.PROPERTY_NAME+"=*"+propertyName+"*)";
	}

	//	public static final MonitorRepository shared = new MonitorRepository();
	public static final String PROPERTY_NAME = "org.haec.theatre.monitor.propertyName";

}
