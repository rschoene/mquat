/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.theatre.api.Monitor;
import org.haec.theatre.utils.BundleUtils;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.settings.BooleanSetting;
import org.haec.theatre.utils.settings.Setting;

/**
 * Activator to set up the {@link MonitorRepository} and some default monitors.
 * @author René Schöne
 */
public class MonitorRepositoryActivator extends MonitorActivator {
	
	List<Monitor> monitors;
	
	/** Use System.nanoTime() for time measurements in contract computation (default: true)*/
	public static Setting<Boolean> simpleTime = new BooleanSetting("theatre.lem.simpleTime", true);

	public MonitorRepositoryActivator() {
		super(false);
		monitors = new ArrayList<>();
	}

	private static Logger log = Logger.getLogger(MonitorRepositoryActivator.class);

	public void preRegister() {
		loadTheatreProperties(MonitorRepositoryActivator.class);
		log.info("theatre.monitor started");
		monitors.add(new CPUEnergyMonitor());
		monitors.add(new CPUTimeMonitor());
		monitors.add(new RunTimeMonitor());
		monitors.add(new WallTimeMonitor());
		Function<Monitor, String> getName = new Function<Monitor, String>() {

			@Override
			public String apply(Monitor s) {
				return s.getClass().getName() + "(" + (s.isAvailable() ? "available" : "offline") + ")";
			}
		};
		log.debug("defaults " + CollectionsUtil.map(monitors, getName, true));
		
		BundleUtils.register(new MonitorCommandProvider(), getContext());
		log.info("Registered MonitorCommandProvider");
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.MonitorActivator#getNewInstances()
	 */
	@Override
	protected List<Monitor> getNewInstances() {
		return monitors;
	}

}
