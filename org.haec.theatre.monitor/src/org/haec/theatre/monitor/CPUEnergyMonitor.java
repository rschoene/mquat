/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.haec.theatre.api.BenchmarkData;

/**
 * A monitor for observing consumed energy of the CPU using estat.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class CPUEnergyMonitor extends AbstractMonitor {

	private static final String CPU_ENERGY = "cpu_energy";
	private double start = 0;
	private double stop = 0;
	private double cpu_energy = 0;
//	private long start_t = 0;
//	private long stop_t = 0;
	
	public CPUEnergyMonitor() {
		super(CPU_ENERGY, false);
	}
	
	private double getJouleFromEStat(boolean start) {
		String estat = System.getProperty("estat.path");
		File efile = new File(estat+"/estat");
		FileReader fr;
		double ret = 0;
		try {
			fr = new FileReader(efile);
			BufferedReader br = new BufferedReader(fr);
			String answer = br.readLine();
//			String test = "";
//			while((test = br.readLine()) != null) {}
			br.close();
			String[] parts = answer.split(";");
			ret = (double) (Long.parseLong(parts[2]) * 1/(Math.pow(2, 16)));
//			if(start) start_t = Long.parseLong(parts[0]);
//			else stop_t = Long.parseLong(parts[1]);
			//System.out.println(System.currentTimeMillis()+" ## "+answer);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	@Override
	public void collectBefore() {
		start = getJouleFromEStat(true);
		//System.out.println("start: "+start+" mJ");
	}

	@Override
	public void collectAfter(BenchmarkData input, Object output) {
		stop = getJouleFromEStat(false);
		//System.out.println("stop: "+stop+" mJ");
	}

	@Override
	public void compute() {
		cpu_energy = stop - start;
		//System.out.println(cpu_energy+";"+(stop_t-start_t));
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.AbstractMonitor#getValue()
	 */
	@Override
	protected Object getValue() {
		return cpu_energy;
	}
	
	public double getCpuEnergy() { compute(); return cpu_energy; }

	@Override
	public boolean isAvailable() {
		String estat = System.getProperty("estat.path");
		if(estat == null) {
			return false;
		}
		File efile = new File(estat+"/estat");
		if(!efile.exists()) {
			return false;
		}
		boolean success = true;
		FileReader fr = null;
		try {
			fr = new FileReader(efile);
			fr.read();
		} catch (FileNotFoundException e) {
			success = false;
		} catch (IOException e) {
			success = false;
		}
		finally {
			if(fr != null) { try { fr.close(); } catch(IOException ignore) {} }
		}
		return success;
	}

}
