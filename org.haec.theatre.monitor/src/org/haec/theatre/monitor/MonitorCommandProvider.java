/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import java.util.List;

import org.coolsoftware.theatre.TheatreCommandProvider;
import org.haec.theatre.api.Monitor;
import org.haec.theatre.utils.AbstractTheatreCommandProvider;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;

/**
 * Get all monitors along with their availability.
 * @author René Schöne
 */
public class MonitorCommandProvider extends AbstractTheatreCommandProvider {

	public static final String COMMAND_NAME = "monitors";
	public static final String DESCRIPTION = "Lists all monitors and their availability.";
	
	public MonitorCommandProvider() {
		super(COMMAND_NAME);
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.AbstractTheatreCommandProvider#execute0(java.lang.String[])
	 */
	@Override
	protected String execute0(String[] params) {
		ServiceReference<MonitorRepository> ref = Activator.getContext().getServiceReference(MonitorRepository.class);
		if(ref == null) {
			return "Monitor-Repository unavailable";
		}
		MonitorRepository repo = Activator.getContext().getService(ref);
		List<Monitor> monitorList = repo.getAllMonitors(false);
		Function<Monitor, String> getName = new Function<Monitor, String>() {
			@Override
			public String apply(Monitor s) {
				return StringUtils.join("+", s.getMeasuredPropertyNames());
			}
		};
		List<String> names = CollectionsUtil.map(monitorList, getName );
		return StringUtils.joinIterable(",", names);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.TheatreCommandProvider#getDescription()
	 */
	@Override
	public String getDescription() {
		return DESCRIPTION;
	}
	
	protected void activate(ComponentContext ctx) {
		RemoteOsgiUtil.register(ctx.getBundleContext(), TheatreCommandProvider.class, this);
	}

}
