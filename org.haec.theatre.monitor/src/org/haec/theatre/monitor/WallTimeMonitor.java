/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import java.lang.management.ManagementFactory;

import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.settings.TheatreSettings;

/**
 * Monitor for the property response_time.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class WallTimeMonitor extends AbstractMonitor {

	private static final String RESPONSE_TIME = "response_time";
	private long start = 0;
	private long stop = 0;
	private long wall_time = 0;
	private boolean simpleTime;
	
	public WallTimeMonitor() {
		super(RESPONSE_TIME, false);
	}
	
	@Override
	public void collectBefore() {
		start = simpleTime ? System.nanoTime() :
			ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime()+ManagementFactory.getThreadMXBean().getCurrentThreadUserTime();
	}

	@Override
	public void collectAfter(BenchmarkData input, Object output) {
		stop = simpleTime ? System.nanoTime() + Math.round(Math.random()*10000000.0): //XXX just for testing fake benchmarks
			ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime()+ManagementFactory.getThreadMXBean().getCurrentThreadUserTime();
	}

	@Override
	public void compute() {
		wall_time = stop - start;
	}
	
	public long getWallTime() {
		compute(); return wall_time;
	}

	@Override
	public boolean isAvailable() {
		// java.lang.ManagementFactory should be always available
		return true;
	}
	
	protected void setTheatreSettings(TheatreSettings s) {
		simpleTime = s.getLemSimpleTimeMeasure().get();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.AbstractMonitor#getValue()
	 */
	@Override
	protected Object getValue() {
		return wall_time;
	}
}
