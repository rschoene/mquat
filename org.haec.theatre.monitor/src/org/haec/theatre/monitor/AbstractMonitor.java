/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import java.util.Hashtable;

import org.haec.theatre.api.Monitor;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;

/**
 * Abstract monitor for one property, optionally registering remotely.
 * @author René Schöne
 */
public abstract class AbstractMonitor implements Monitor {
	
	protected String propertyName;
	protected String[] propertyNames;
	private ServiceRegistration<Monitor> reg;
	private boolean registerRemotely;
	
	public AbstractMonitor(String propertyName, boolean registerRemotely) {
		this.propertyName = propertyName;
		this.propertyNames = new String[]{propertyName};
		this.registerRemotely = registerRemotely;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Monitor#getMeasuredPropertyNames()
	 */
	@Override
	public String[] getMeasuredPropertyNames() {
		return propertyNames;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Monitor#propertyAndValue()
	 */
	@Override
	public String propertyAndValue() {
		return propertyName + PROPERTY_DELIMITER + getValue();
	}
	
	protected abstract Object getValue();

	@Override
	public String toString() {
		return propertyAndValue();
	}

	protected void activate(ComponentContext ctx) {
		if(registerRemotely) {
			Hashtable<String, Object> props = new Hashtable<String, Object>();
			props.put(MonitorUtil.PROPERTY_NAME, StringUtils.join(",", this.getMeasuredPropertyNames()));
			reg = RemoteOsgiUtil.register(ctx.getBundleContext(), Monitor.class, this, props);
		}
	}
	
	protected void deactivate(ComponentContext ctx) {
		if(reg != null) {
			reg.unregister();
		}
	}

}
