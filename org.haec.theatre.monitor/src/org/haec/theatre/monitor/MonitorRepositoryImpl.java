/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.theatre.api.Monitor;
import org.haec.theatre.settings.TheatreSettings;
//import org.coolsoftware.theatre.globalenergymanager.GemActivator;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;

/**
 * Simple service based implementation of a monitor repository. Allows
 * to search for remote monitors as well.
 * @author René Schöne
 */
public class MonitorRepositoryImpl implements MonitorRepository {
	
private Logger log = Logger.getLogger(MonitorRepositoryImpl.class);
	private BundleContext context;

	private TheatreSettings settings;
	
	@Override
	public Monitor getMonitor(final String propertyName) {
		String filter = MonitorUtil.createFilter(propertyName);
		List<Monitor> list = getMonitors(filter, true);
		if(list == null || list.isEmpty()) {
			log.warn("List of monitors is null/empty for property " + propertyName + ". Returning monitorproxy pointing to gem.");
			return new MonitorProxy(propertyName, settings.getGemUri().get(), getContext());
		}
		if(list.size() == 1) {
			log.debug(String.format("Using %s for property %s.", nameOf(list.get(0)), propertyName));
			return list.get(0);
		}
		// more than one match was found
		log.debug(String.format("More than monitor found for property %s. Using %s.", propertyName, nameOf(list.get(0))));
		return list.get(0);
	}

	private static String nameOf(Monitor m) {
		return m.getClass().getName();
	}
	
	@Override
	public List<Monitor> getAllMonitors(boolean onlyAvailable) {
		return getMonitors(null, onlyAvailable);
	}

	protected List<Monitor> getMonitors(String filter, boolean onlyAvailable) {
		List<Monitor> result = new ArrayList<Monitor>();
		BundleContext context = getContext();
		if(context == null) return result;
		Collection<ServiceReference<Monitor>> srefs;
		try {
			srefs = context.getServiceReferences(Monitor.class, filter);
		} catch (InvalidSyntaxException ignore) {
			// should not happen since filter is either null or valid filter
			log.debug("Invalid filter", ignore);
			return result;
		}
		for(ServiceReference<Monitor> sref : srefs) {
			if(sref != null) {
				Monitor m = context.getService(sref);
				if(!onlyAvailable || m.isAvailable()) {
					result.add(m);
				}
			}
		}
		// no available calculator has been found
		return result;
	}

	@Deprecated
	public ServiceRegistration<?> registerService(Class<? extends Monitor> clazz) {
		Hashtable<String, Object> props = new Hashtable<String, Object>();
//		props.put(RemoteOSGiService.R_OSGi_REGISTRATION, Boolean.TRUE);
		Monitor instance = getNewInstance(clazz);
		props.put(MonitorUtil.PROPERTY_NAME, StringUtils.join(",", instance.getMeasuredPropertyNames()));
		ServiceRegistration<?> result = getContext().registerService(Monitor.class.getName(), instance, props);
		log.debug(String.format("Registered class %s (instance: %s) with properties %s", clazz, instance, props));
		return result;
	}

	@Deprecated
	private Monitor getNewInstance(Class<? extends Monitor> clazz) {
		try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected void setTheatreSettings(TheatreSettings s) {
		this.settings = s;
	}
	
	protected void activate(ComponentContext ctx) {
		this.context = ctx.getBundleContext();
	}
	
	private BundleContext getContext() {
		return context;
//		return FrameworkUtil.getBundle(MonitorRepository.class).getBundleContext();
//		return GemActivator.getContext();
//		return MonitorRepositoryActivator.getContext();
	}

}
