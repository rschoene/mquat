/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.haec.theatre.api.Monitor;
import org.haec.theatre.utils.StringUtils;
import org.haec.theatre.utils.settings.SettingHolder;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * Every plugin providing a new monitor should extend this activator to
 * register its monitor to be found by {@link MonitorRepository}.
 * @author René Schöne
 *
 * @param <T> the class of the monitor to register
 */
public abstract class MonitorActivator extends SettingHolder implements BundleActivator {

	private static BundleContext context;
	private boolean remoteRegistration;
	private List<ServiceRegistration<Monitor>> regs;
	
	public MonitorActivator(boolean remoteRegistration) {
		this.remoteRegistration = remoteRegistration;
		this.regs = new ArrayList<>();
	}

	@Override
	public void start(BundleContext context) throws Exception {
		MonitorActivator.context = context;
		preRegister();
		registerService();
	}
	
	/**
	 * Operations executed before {@link #registerService()}.
	 * This is called in {@link #start(BundleContext)}.
	 */
	protected void preRegister() { }

	private void registerService() {
		Hashtable<String, Object> props = new Hashtable<String, Object>();
		props.put(RemoteOSGiService.R_OSGi_REGISTRATION, Boolean.valueOf(remoteRegistration));
		List<Monitor> instances = getNewInstances();
		for(Monitor instance : instances) {
			props.put(MonitorRepository.PROPERTY_NAME, StringUtils.join(",", instance.getMeasuredPropertyNames()));
			regs.add(context.registerService(Monitor.class, instance, props));
		}
	}
	
	/**
	 * Returns the instances to register.
	 * Mind to use {@link #preRegister()} to setup the common parts.
	 * @return the instances to register
	 */
	protected abstract List<Monitor> getNewInstances();
	
	@Override
	public void stop(BundleContext context) throws Exception {
		MonitorActivator.context = null;
		if(regs != null) { 
			for(ServiceRegistration<Monitor> reg : regs) {
				reg.unregister();
			}
		}
	}

	/**
	 * @return the context
	 */
	public static BundleContext getContext() {
		return context;
	}
}
