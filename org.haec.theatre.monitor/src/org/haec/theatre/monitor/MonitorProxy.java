/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.monitor;

import org.apache.log4j.Logger;
import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.api.Monitor;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

/**
 * Monitor delegating all methods to a proxy.
 * @author René Schöne
 */
public class MonitorProxy implements Monitor {
	
	protected String[] propertyNames;
	protected String uri;
	protected Monitor delegatee;
	private BundleContext context;
	protected Logger log = Logger.getLogger(MonitorProxy.class);

	public MonitorProxy(String propertyName, String uri, BundleContext context) {
		this.propertyNames = new String[]{propertyName};
		this.uri = uri;
		this.context = context;
		this.delegatee = null;
	}
	
	/** Set and returns the delegatee */
	protected Monitor ensureDelegatee() {
		// check if active (maybe "old" proxy)
		if(this.delegatee != null) {
			try {
				delegatee.isAvailable();
				return this.delegatee;
			} catch (Exception e) {
				log.warn("Got exception although not null. Resetting.", e);
				this.delegatee = null;
			}
		}
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
		String filterString = MonitorUtil.createFilter(propertyNames[0]);
		Filter filter;
		try {
			filter = context.createFilter(filterString);
		} catch (InvalidSyntaxException e) {
			log.error("Invalid filter " + filterString, e);
			return null;
		}
		RemoteServiceReference[] refs = RemoteOsgiUtil.getRemoteServiceReferences(remote,
				uri, Monitor.class.getName(), filter);
		if(refs == null) {
			log.error("Could not find suitable delegatee for " + StringUtils.join(",", propertyNames));
			return null;
		}
		// take first non null service
		for(RemoteServiceReference ref : refs) {
			Object service = remote.getRemoteService(ref);
			if(service == null) { continue; }
			this.delegatee = (Monitor) service;
			break;
		}
		if(this.delegatee != null) {
			String myIp = RemoteOsgiUtil.getLemIp();
			if(this.delegatee instanceof RemoteMonitor) {
				((RemoteMonitor) this.delegatee).setHostIp(myIp);
			}
			else {
				log.warn("Get non-RemoteMonitor " + this.delegatee);
			}
		}
		return this.delegatee;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#collectBefore()
	 */
	@Override
	public void collectBefore() {
		if(ensureDelegatee() != null) { delegatee.collectBefore(); }
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#collectAfter()
	 */
	@Override
	public void collectAfter(BenchmarkData input, Object output) {
		if(ensureDelegatee() != null) { delegatee.collectAfter(input, output); }
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#compute()
	 */
	@Override
	public void compute() {
		if(ensureDelegatee() != null) { delegatee.compute(); }
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#getMeasuredPropertyNames()
	 */
	@Override
	public String[] getMeasuredPropertyNames() {
		return propertyNames;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#isAvailable()
	 */
	@Override
	public boolean isAvailable() {
		return ensureDelegatee() != null ? delegatee.isAvailable() : false;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#propertyAndValue()
	 */
	@Override
	public String propertyAndValue() {
		return ensureDelegatee() != null ? delegatee.propertyAndValue() : propertyNames[0] + PROPERTY_DELIMITER + "0.0";
	}

}
