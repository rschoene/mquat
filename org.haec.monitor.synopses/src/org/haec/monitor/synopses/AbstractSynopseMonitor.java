/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.monitor.synopses;

import java.util.List;

import org.apache.log4j.Logger;
import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.dao.SynopseDAO;
import org.haec.theatre.monitor.AbstractMonitor;
import org.haec.theatre.monitor.RemoteMonitor;

/**
 * The abstract superclass for all monitor using Dexter.
 * @author René Schöne
 */
public abstract class AbstractSynopseMonitor extends AbstractMonitor implements RemoteMonitor {
	
	protected static final int TIMESTAMP_INDEX = 1;
	protected static final int VALUE_INDEX = 2;
	
	public static final int windowValueDefault = 10; // time in s to keep values
	public static final int frequencyValueDefault = 1; // time in s to update values
	
	public static final int sensorClassNotFound = -1;
	protected int sensorclass;
	private String sensorClassName;
	protected String hostIp;
	/** Fully quantified name of the synopse. <code>null</code> if not used. */
	protected String synopseTableName;
	protected long synopseId;
	protected long sensorId;
	protected Double value;

	protected int windowValue = windowValueDefault;
	protected int frequencyValue = frequencyValueDefault;
	
	protected Logger log = Logger.getLogger(AbstractSynopseMonitor.class);
	protected SynopseDAO synopseDAO;
	
	/**
	 * @param sensorclassName corresponds to sensorClasses.class in Dexter
	 * @param propertyName corresponds to name in the contract (and to the name of the property)
	 */
	public AbstractSynopseMonitor(String sensorclassName, String propertyName) {
		super(propertyName, true);
		this.sensorClassName = sensorclassName;
		this.synopseTableName = null;
		this.value = null;
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.RmoteMonitor#setHostIp(String)
	 */
	@Override
	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#collectBefore()
	 */
	@Override
	public void collectBefore() {
//		checkInit();
		this.value = null;
		synopseTableName = "s" + sensorclass + "at" + System.currentTimeMillis();
		synopseId = synopseDAO.createSynopse(synopseTableName, sensorClassName, hostIp, windowValue, frequencyValue);
		if(synopseId == SynopseDAO.OPERATION_FAILED) {
			log.error("Could not create synopse " + synopseTableName);
		}
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#collectAfter()
	 */
	@Override
	public void collectAfter(BenchmarkData input, Object output) {
		// empty, synopse should only be deleted after compute
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#compute()
	 */
	@Override
	public void compute() {
		// set error value, aggregate value from synopse, delete synopse
		value = -1.0;
		value = aggregate();
		synopseDAO.deleteSynopse(synopseId);
	}

	/**
	 * Aggregate the results and returns the result.
	 */
	protected abstract double aggregate();

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#isAvailable()
	 */
	@Override
	public boolean isAvailable() {
		return sensorclass > 0 && synopseDAO != null;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.AbstractMonitor#getValue()
	 */
	@Override
	protected Object getValue() {
		return value;
	}
	
	/** To be used by subclasses in {@link #aggregate()} if interested in minimum */
	protected double aggregateMinimum() {
		// interested in minimum
		double min = Double.MAX_VALUE;
		for(double val : synopseDAO.getResult(synopseId)) {
			if(val < min) { min = val; }
		}
		return min;
	}
	
	/** To be used by subclasses in {@link #aggregate()} if interested in maximum */
	protected double aggregateMaximum() {
		// interested in minimum
		double max = Double.MIN_VALUE;
		for(double val : synopseDAO.getResult(synopseId)) {
			if(val > max) { max = val; }
		}
		return max;
	}
	
	/** To be used by subclasses in {@link #aggregate()} if interested in average */
	protected double aggregateAverage()  {
		// interested in average
		double total = 0f;
		List<Double> result = synopseDAO.getResult(synopseId);
		int rowCount = result.size();
		for(double val : result) {
			total += val;
		}
		// avoid div by zero
		return rowCount > 0 ? total/rowCount : total;
	}

	public void setSynopseDAO(SynopseDAO sd) {
		this.synopseDAO = sd;
	}

	public void unsetSynopseDAO(SynopseDAO sd) {
		this.synopseDAO = null;
	}

}
