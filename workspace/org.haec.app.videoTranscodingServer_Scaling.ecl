import ccm [platform:/resource/theatre/ServerD.structure] 
import ccm [platform:/resource/theatre/Apps/org.haec.app.videoTranscodingServer/org.haec.app.videoTranscodingServer.structure] 

contract org_haec_app_scaling_ffmpeg_ScalingFfmpeg implements software Scaling . scale { 
	
		mode scaleFfmpeg1 { 
			requires resource CPU { 
				frequency min: 253.3  
				cpu_time max: f( max_video_length  )  = 0.0  + 5.77419090049704E-4  * ( max_video_length  )  
				
					energyRate:  5.33 
						} 
			requires component CopyScalingFile1 { 
				} 
			provides utility max: 1.0  
			requires resource RAM { 
				free min: 53.3  
				
					energyRate:  5.33 
						} 
			provides run_time min: f( max_video_length  )  = 0.0  + 5.76901913705504E-4  * ( max_video_length  )  
			} 
		} 
		contract org_haec_app_scaling_mencoder_ScalingMencoder implements software Scaling . scale { 
	
		mode scaleMencoder1 { 
			requires resource CPU { 
				frequency min: 503.2  
				cpu_time max: f( max_video_length  )  = 167.705850409997  + 3.86665430027266E-4  * ( max_video_length  )  
				
					energyRate:  5.32 
						} 
			requires component CopyScalingFile1 { 
				} 
			provides utility max: 1.0  
			requires resource RAM { 
				free min: 53.2  
				
					energyRate:  5.32 
						} 
			provides run_time min: f( max_video_length  )  = 167.521533258921  + 3.86284881886199E-4  * ( max_video_length  )  
			} 
		} 
		contract org_haec_app_scaling_handbrake_ScalingHandbrake implements software Scaling . scale { 
	
		mode scaleHandbrake1 { 
			requires resource CPU { 
				frequency min: 503.1  
				cpu_time max: f( max_video_length  )  = 925.261088161231  + 4.36326144828815E-4  * ( max_video_length  )  
				
					energyRate:  5.31 
						} 
			requires component CopyScalingFile1 { 
				} 
			provides utility max: 1.0  
			requires resource RAM { 
				free min: 53.1  
				
					energyRate:  5.31 
						} 
			provides run_time min: f( max_video_length  )  = 928.217075188816  + 4.35880138847453E-4  * ( max_video_length  )  
			} 
		} 
		
