import ccm [platform:/resource/theatre/ServerD.structure] 
import ccm [platform:/resource/theatre/Apps/org.haec.app.videoTranscodingServer/org.haec.app.videoTranscodingServer.structure] 

contract org_haec_app_transcoder_ffmpeg_TranscoderFfmpeg implements software Transcoder . transcode { 
	
		mode transcoderFfmpeg1 { 
			requires resource CPU { 
				frequency min: 500  
				cpu_time < f_cpu_time ( null )> 
				
					energyRate:  5.0 
						} 
			requires resource RAM { 
				free min: 50  
				
					energyRate:  5.0 
						} 
			requires component CopyTranscoderFile1 { 
				} 
			provides run_time < ( )> 
			} 
		} 
		contract org_haec_app_transcoder_mencoder_TranscoderMencoder implements software Transcoder . transcode { 
	
		mode transcoderMencoder1 { 
			requires resource CPU { 
				frequency min: 500  
				cpu_time < f_cpu_time ( null )> 
				
					energyRate:  5.0 
						} 
			requires resource RAM { 
				free min: 50  
				
					energyRate:  5.0 
						} 
			requires component CopyTranscoderFile1 { 
				} 
			provides run_time < ( )> 
			} 
		} 
		contract org_haec_app_transcoder_handbrake_TranscoderHandbrake implements software Transcoder . transcode { 
	
		mode transcoderHandbrake1 { 
			requires resource CPU { 
				frequency min: 500  
				cpu_time < f_cpu_time ( null )> 
				
					energyRate:  5.0 
						} 
			requires resource RAM { 
				free min: 50  
				
					energyRate:  5.0 
						} 
			requires component CopyTranscoderFile1 { 
				} 
			provides run_time < ( )> 
			} 
		} 
		contract org_haec_app_transcoder_noop_TranscoderNoop implements software Transcoder . transcode { 
	
		mode transcoderNoop1 { 
			requires resource CPU { 
				frequency min: 500  
				
					energyRate:  0.1 
						} 
			requires component CopyTranscoderFile1 { 
				} 
			provides utility max: 0.1  
			provides run_time max: 0.01  
			} 
		} 
		
