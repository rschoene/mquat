import ccm [platform:/resource/theatre/ServerD.structure] 
import ccm [platform:/resource/theatre/Apps/org.haec.app.videoTranscodingServer/org.haec.app.videoTranscodingServer.structure] 

contract org_haec_app_censor_ffmpeg_CensorFfmpeg implements software Censor . censor { 
	
		mode censorFfmpeg1 { 
			requires resource CPU { 
				frequency min: 501  
				cpu_time max: f( max_video_length  )  = 0.0  + 6.11572265625E-4  * ( max_video_length  )  
				
					energyRate:  5.1 
						} 
			requires resource RAM { 
				free min: 51  
				
					energyRate:  5.1 
						} 
			provides run_time min: f( max_video_length  )  = 7.3000088205664  + 6.09741208003305E-4  * ( max_video_length  )  
			} 
		} 
		
