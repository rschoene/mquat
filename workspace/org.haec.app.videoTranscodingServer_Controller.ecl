import ccm [platform:/resource/theatre/ServerD.structure] 
import ccm [platform:/resource/theatre/Apps/org.haec.app.videoTranscodingServer/org.haec.app.videoTranscodingServer.structure] 

contract org_haec_app_controller_delegating_DelegatingController implements software Controller . pipScaled { 
	
		mode pipScaledModeDelegating { 
			requires component Scaling { 
				response_time max: f( max_video_length  )  = 6.90003355276717  + 0.00122326659039337  * ( max_video_length  )  
				} 
			requires component PiP { 
				response_time max: f( max_video_length  )  = 6.90003355276717  + 0.00122326659039337  * ( max_video_length  )  
				} 
			provides response_time min: f( max_video_length  )  = 6.90003355276717  + 0.00122326659039337  * ( max_video_length  )  
			} 
		} 
		
