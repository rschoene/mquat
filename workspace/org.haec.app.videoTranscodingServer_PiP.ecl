import ccm [platform:/resource/theatre/ServerD.structure] 
import ccm [platform:/resource/theatre/Apps/org.haec.app.videoTranscodingServer/org.haec.app.videoTranscodingServer.structure] 

contract org_haec_app_pip_ffmpeg_PipFfmpeg implements software PiP . doPip { 
	
		mode pipFfmpeg1 { 
			requires resource CPU { 
				frequency min: 252  
				cpu_time max: f( max_video_length  )  = 0.0  + 7.32776988634928E-4  * ( max_video_length  )  
				
					energyRate:  5.2 
						} 
			requires resource RAM { 
				free min: 52  
				
					energyRate:  5.2 
						} 
			provides run_time min: f( max_video_length  )  = 6.50000543799449  + 7.31323240375965E-4  * ( max_video_length  )  
			} 
		} 
		
