/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.filter.unsortedfilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;

class FilterBenchmarkData implements BenchmarkData {

	int mp;
	Integer[] inp;
	Integer upperBound;

	public FilterBenchmarkData(int mp, Integer[] inp, Integer upperBound) {
		super();
		this.mp = mp;
		this.inp = inp;
		this.upperBound = upperBound;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mp;
	}
	
}

/**
 * Benchmark of org.haec.app.filter.unsortedfilter.UnsortedFilter
 * @author Sebastian Götz
 */
public class UnsortedFilterBenchmark implements Benchmark {

	private UnsortedFilter<Integer> aut = new UnsortedFilter<Integer>();

//	public static void main(String[] args) {
//		UnsortedFilterBenchmark b = new UnsortedFilterBenchmark();
//		b.addMonitor(MonitorRepository.shared.getMonitor("cpu_time"));
//		b.addMonitor(MonitorRepository.shared.getMonitor("response_time"));
//		b.setBenchData(new File("usfilter.txt"));
//		b.benchmark();
//	}

	private Integer[] generateList(int num, int min, int max) {
		Integer[] ret = new Integer[num];
		Random r = new Random();
		for (int i = 0; i < num; i++) {
			ret[i] = r.nextInt(max - min) + min + 1;
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>();
		// metaparameters: list_size
		for (int list_size = 0; list_size < 1000000; list_size += 100000) {
			List<Integer> mpv = new ArrayList<Integer>();
			mpv.add(list_size);
			Integer[] inp = generateList(list_size, 1, 500);
			int upperBound = new Random().nextInt(500);
			result.add(new FilterBenchmarkData(list_size, inp, upperBound));
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		FilterBenchmarkData fbd = (FilterBenchmarkData) data;
		return aut.filter(fbd.inp, fbd.upperBound);
	}
}
