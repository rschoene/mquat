/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.monitor.yeppp;

import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.monitor.AbstractMonitor;

/**
 * Monitor measureing "cpu_energy" using Yeppp and RAPL counters.
 * @author René Schöne
 */
public class YepppCPUEnergyMonitor extends AbstractMonitor {
	
	private static final String CPU_ENERGY = "cpu_energy";
	final EnergyCounterWrapper wrapper;
	private double diff;
	
	protected YepppCPUEnergyMonitor() {
		super(CPU_ENERGY, false);
		wrapper = new EnergyCounterWrapper();
	}

	@Override
	public void collectBefore() {
		try {
			wrapper.collectBefore();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void collectAfter(BenchmarkData input, Object output) {
		try {
			diff = wrapper.collectAfter();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void compute() {
		// nothing to do here
	}

	@Override
	public boolean isAvailable() {
		// test functions
		try {
			collectBefore();
			collectAfter(null, null);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.AbstractMonitor#getValue()
	 */
	@Override
	protected Object getValue() {
		return diff;
	}

}