/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.monitor.yeppp;

import info.yeppp.CpuCycleCounterState;
import info.yeppp.Library;
import info.yeppp.Version;

import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.monitor.AbstractMonitor;

/**
 * Monitor measureing "cpu_time" using Yeppp.
 * @author René Schöne
 */
public class YepppCPUCyclesMonitor extends AbstractMonitor {

	private static final String CPU_TIME = "cpu_time";
	private CpuCycleCounterState state;
	private long cycles;
	
	public YepppCPUCyclesMonitor() {
		super(CPU_TIME, false);
	}

	@Override
	public void collectBefore() {
		state = Library.acquireCycleCounter();
	}

	@Override
	public void collectAfter(BenchmarkData input, Object output) {
		cycles = Library.releaseCycleCounter(state);
	}

	@Override
	public void compute() {
		// nothing to do here
	}

	@Override
	public boolean isAvailable() {
		Version v = Library.getVersion();
		if(v != null && v.getReleaseName() != null) {
			return true;
		}
		else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.AbstractMonitor#getValue()
	 */
	@Override
	protected Object getValue() {
		return cycles;
	}
	
	/*
	final info.yeppp.CpuCycleCounterState state = info.yeppp.Library.acquireCycleCounter();

	final Random rng = new Random(42l);
	for (int i = 0; i < arraySize; i++) {
		array[i] = (short)rng.nextInt();
	}

	final long cycles = info.yeppp.Library.releaseCycleCounter(state);
	 */

}
