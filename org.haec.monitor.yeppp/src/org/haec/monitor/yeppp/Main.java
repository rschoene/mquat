/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.monitor.yeppp;

import org.haec.monitor.yeppp.YepppCPUEnergyMonitor;
import org.haec.theatre.api.Monitor;

/**
 * Simple application to test the {@link YepppCPUEnergyMonitor}.
 * @author René Schöne
 */
public class Main implements Runnable {

	@Override
	public void run() {
		Monitor m = null;
		try {
			m = new YepppCPUEnergyMonitor();
		}
		catch(Throwable e) {
			e.printStackTrace();
		}
		if(m == null) {
			return;
		}
		println("Monitor created successfully. Calling collectBefore now.");
		m.collectBefore();
		println("Done. Doing some computations now.");
		doSomeComputation();
		println("Done. Calling collectAfter now.");
		m.collectAfter(null, null);
		println("Done. Calling compute now.");
		m.compute();
		println("Done finally.");
		System.out.println(m);
	}

	private void println(String message) {
		System.out.println("[Playground] " + message);
	}
	
	private void doSomeComputation() {
		for (int i = 0; i < 1000 * 1000; i++) {
			double tmp = Math.random();
			tmp = tmp * tmp;
		}
	}

}
