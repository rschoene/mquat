/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.monitor.yeppp;

import java.io.File;
import java.security.AccessController;
import java.security.PrivilegedAction;

/**
 * The wrapper calling the C++ class.
 * @author René Schöne
 */
public class EnergyCounterWrapper {
	
	static {
		System.out.println("[MonitorYeppp] Activator.start()");
		String jlp = System.getProperty("java.library.path");
		// FIXME these properties are actually in TheatreSettings
		String yepppBundlePath = System.getProperty("yeppp.bundle.path");
		String yepppWrapperPath = System.getProperty("yeppp.wrapper.path");
		if(!jlp.endsWith(File.pathSeparator))
			jlp += File.pathSeparator;
		jlp += yepppBundlePath + File.pathSeparator;
		jlp += yepppWrapperPath + File.pathSeparator;
		System.setProperty("java.library.path", jlp);

		String libname = "yepppEnergyCounterWrapperImpl";
//		String libname = "YepppClient";
		System.out.println("[EnCoWr] static block");
		System.out.println(System.getProperty("java.library.path"));
		System.loadLibrary(libname);
		System.out.println("[EnCoWr] Mapped name: " + System.mapLibraryName(libname));
	}
	
	public void collectBefore() throws Exception {
		int result = AccessController.doPrivileged(new PrivilegedAction<Integer>() {

			@Override
			public Integer run() {
				int result = acquire();
				return Integer.valueOf(result);
			}
		});
		if(result < 0) {
			throw new Exception("Got error code: " + (-result));
		}
	}
	
	private native int acquire();
	
	public double collectAfter() throws Exception {
		double result = release();
		if(result < 0) {
			throw new Exception("Got error code (or wrong result): " + (-result));
		}
		return result;
	}
	
	private native double release();

}
