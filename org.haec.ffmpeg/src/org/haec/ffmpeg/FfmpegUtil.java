/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.ffmpeg;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.haec.apps.util.AbstractVideoUtil;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.SharedMethods;
import org.haec.apps.util.VideoDuration;
import org.haec.apps.util.VideoDurationCalculator;
import org.haec.apps.util.VideoResolution;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.apps.util.VideoSettings;

/**
 * VideoUtil of ffmpeg.
 * Implements VideoDurationCalculator and VideoResolutionCalculator.
 * @author René Schöne
 */
public final class FfmpegUtil extends AbstractVideoUtil implements VideoDurationCalculator, VideoResolutionCalculator {
	
	public static final FfmpegUtil sharedInstance = new FfmpegUtil();
	public static final String[] additionalCommands = new String[]{"-loglevel","error"};
	
	public FfmpegUtil() {
		super(VideoSettings.FFMPEG);
	}
	
	@Override
	public VideoDuration getDurationOfVideo(File video) {
		String searchDuration = "Duration: ";
		Map<String, List<String>> map;
		VideoDuration result = VideoDuration.UNKNOWN;
		try {
			map = identify(getIdentifyPb(video), searchDuration);
		} catch (IOException e) {
			e.printStackTrace();
			// return unknown
			return result;
		}
		List<String> list = map.get(searchDuration);
		return SharedMethods.getVideoDurationFfmpegHandbrake(list);
	}

	private ProcessBuilder getIdentifyPb(File video) {
		return new ProcessBuilder(this.processCommand, "-i", video.getAbsolutePath(), "--scan").redirectErrorStream(true);
	}

	@Override
	public VideoResolution getVideoResolution(File video) {
		String searchStream = "Stream #"; //XXX: maybe not for every video?
		VideoResolution result = VideoResolution.UNKNOWN;
		Map<String, List<String>> map;
		try {
			map = identify(getIdentifyPb(video), searchStream);
		} catch (IOException e) {
			e.printStackTrace();
			// return unknown
			return result;
		}
		List<String> list = map.get(searchStream);
		return SharedMethods.getVideoResolutionFfmpegHandbrake(list);
	}
	
	@Override
	protected String[] getAdditionalCommands() {
		return additionalCommands;
	}
	
	@Override
	protected void handleErrorStream(InputStream error) {
		logStream(error);
	}
	
	@Override
	protected void handleInputStream(InputStream input) {
		logStream(input);
	}

	@Override
	public ErrorState createErrorState() {
		// TODO Check whether new class need to be created
		return null;
	}

}
