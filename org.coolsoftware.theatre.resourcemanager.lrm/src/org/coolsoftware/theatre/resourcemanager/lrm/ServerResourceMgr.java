/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.resourcemanager.lrm;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.behavior.StateMachine;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl;
import org.coolsoftware.coolcomponents.ccm.variant.Behavior;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.resourcemanager.ILocalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryRegistryImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * Implementation of the LRM using SIGAR. To be revised.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class ServerResourceMgr implements ILocalResourceManager {
	private class NamedValue {
		double value;
		String name;
		public NamedValue(double value, String name) {
			super();
			this.value = value;
			this.name = name;
		}
	}
	private class Cacheline {
		Callable<Object> refresher;
		Object lastValue;
		boolean neverRefresh;
		
		public Cacheline(Callable<Object> refresher, boolean neverRefresh) {
			super();
			this.refresher = refresher;
			this.neverRefresh = neverRefresh;
			if(this.neverRefresh) {
				// call it once
				try {
					lastValue = refresher.call();
				} catch (Exception e) {
					log.warn("Could not initialize value", e);
				}
			}
		}

		public void updateWithRefresher() {
			if(neverRefresh) {
				return;
			}
			try {
				lastValue = refresher.call();
			} catch (Exception ignore) { }
		}
	}
	private class Cache {
		Map<String, Cacheline> singletonCache = new HashMap<String, Cacheline>();
		
		void newLine(String key, Cacheline line) {
			singletonCache.put(key, line);
		}
		Cacheline update(String key) {
			Cacheline line = singletonCache.get(key);
			line.updateWithRefresher();
			return line;
		}
		Object get(String key) {
			Cacheline line = update(key);
			return line.lastValue;
		}
	}
	
	
	private static final Logger log = Logger.getLogger(ServerResourceMgr.class);
	private static final String staticsCache = "static_values";
	
	private static final String key_server_load = "server.load";
	
	private static final String key_cpu_type = "cpu.type";
	private static final String key_cpu_freq = "cpu.frequency";
	private static final String propName_cpu_freq = "frequency";

	private static final String resName_ram = "ram";
	private static final String key_ram_type = "ram.type";
	private static final String key_ram_total = "ram.total";
	private static final String key_ram_free = "ram.free";
	private static final String propName_ram_used = "used";
	private static final String propName_ram_free = "free";

//	private static final String key_net_type = "net.type";
//	private static final String key_net_throughput = "net.throughput";
	
	private Sigar s;
	IGlobalResourceManager grm;
	StructuralModel resourceTypes;
	private File staticValueCacheFile;
	private Cache dynamicValues;
	private Timer timer;
	private Long observerRate;
	private Long initialObserveDelay;
	private String serverName;
	private TheatreSettings settings;
	
	public ServerResourceMgr() {
		System.out.println(new File(".").getAbsolutePath());
		String workspace = FileUtils.getWorkspaceDir();
		String cachePath = FileUtils.join(workspace, staticsCache);
		staticValueCacheFile = new File(cachePath);

		s = new Sigar();
	}

	private void createCacheRefresher() {
		class RamTypeRefresher implements Callable<Object> {
			public Object call() throws Exception {
				ResourceType ramType = null;
				for (ResourceType sub : ((ResourceType) resourceTypes.getRoot())
						.getSubtypes()) {
					if (sub.getName().toLowerCase().equals("server")) {
						for (ResourceType dev : sub.getSubtypes()) {
							if (dev.getName().toLowerCase().equals("ram")) {
								ramType = dev;
							}
						}
					}
				}
				return ramType;
			}
		}
		class CpuTypeRefresher implements Callable<Object> {
			public Object call() throws Exception {
				ResourceType cpuType = null;
				for (ResourceType sub : ((ResourceType) resourceTypes.getRoot())
						.getSubtypes()) {
					if (sub.getName().toLowerCase().equals("server")) {
						for (ResourceType dev : sub.getSubtypes()) {
							if (dev.getName().toLowerCase().equals("cpu")) {
								cpuType = dev;
							}
						}
					}
				}
				return cpuType;
			}
		};
//		Callable<Object> networkType = new Callable<Object>() {
//			public Object call() throws Exception {
//				ResourceType cpuType = null;
//				for (ResourceType sub : ((ResourceType) resourceTypes.getRoot())
//						.getSubtypes()) {
//					if (sub.getName().toLowerCase().equals("server")) {
//						for (ResourceType dev : sub.getSubtypes()) {
//							if (dev.getName().toLowerCase().equals("network")) {
//								cpuType = dev;
//							}
//						}
//					}
//				}
//				return cpuType;
//			}
//		};
		dynamicValues = new Cache();
		dynamicValues.newLine(key_cpu_type, new Cacheline(new CpuTypeRefresher(), true));
		dynamicValues.newLine(key_ram_type, new Cacheline(new RamTypeRefresher(), true));
//		dynamicValues.newLine(key_net_type, new Cacheline(networkType, true));

		class ServerLoadRefresher implements Callable<Object> {
			public Object call() throws Exception {
				double[] loads = s.getLoadAverage();
				log.debug(Arrays.toString(loads));
				return loads;
			}
		};

		class CpuFreqRefresher implements Callable<Object> {
			public Object call() throws Exception {
				CpuInfo[] list = s.getCpuInfoList();
				NamedValue[] result = new NamedValue[list.length];
				for (int i = 0; i < result.length; i++) {
					CpuInfo ci = list[i];
					String name = ensureValidName(ci.getModel());
					int freq = ci.getMhz();
					result[i] = new NamedValue(freq, name);
				}
				return result;
			}
		};
		
		class RamFreeRefresher implements Callable<Object> {
			public Object call() throws Exception {
				Mem m = s.getMem();
				float free = m.getActualFree() / 1024 / 1024;
				return Float.valueOf(free);
			}
		};
		class RamTotalRefresher implements Callable<Object> {
			public Object call() throws Exception {
				Mem m = s.getMem();
				float total = m.getTotal() / 1024 / 1024;
				return Float.valueOf(total);
			}
		};
		
		dynamicValues.newLine(key_server_load, new Cacheline(new ServerLoadRefresher(), false));
		dynamicValues.newLine(key_cpu_freq, new Cacheline(new CpuFreqRefresher(), false));
		dynamicValues.newLine(key_ram_total, new Cacheline(new RamTotalRefresher(), true)); // only get total value once
		dynamicValues.newLine(key_ram_free, new Cacheline(new RamFreeRefresher(), false));
	}

	private void startObserving() {
		timer = new Timer();
		observerRate = settings.getLrmUpdateRate().get();
		initialObserveDelay = settings.getLrmInitialDelay().get();
		log.info(String.format("Update-rate: %s, delay: %s", observerRate, initialObserveDelay));
		
		observeCPU();
		observeLoad();
		observeRAM();
	}

	private void observeCPU() {
		TimerTask cpuObserver = new TimerTask() {
			@Override
			public void run() {
				log.debug(".");
				NamedValue[] freeValues = (NamedValue[]) dynamicValues.get(key_cpu_freq);
				for(NamedValue nv : freeValues) {
					grm.notifyPropertyChanged(nv.value, serverName, nv.name, propName_cpu_freq, grm.getType("cpu"));
				}
			}
		};
		
		timer.schedule(cpuObserver, initialObserveDelay, observerRate);
		
	}

	private void observeLoad() {
		TimerTask loadObserver = new TimerTask() {
			final String[] names = new String[]{"load1", "load5", "load15"};
			@Override
			public void run() {
				double[] loads = (double[]) dynamicValues.get(key_server_load);
				for (int i = 0; i < loads.length; i++) {
					grm.notifyPropertyChanged(loads[i], serverName, serverName, names[i], grm.getType("server"));
				}
			}
		};
			
		timer.schedule(loadObserver, initialObserveDelay, observerRate);
		
	}

	private void observeRAM() {
		TimerTask ramObserver = new TimerTask() {
			@Override
			public void run() {
				log.debug(".");
				Float free = (Float) dynamicValues.get(key_ram_free);
				grm.notifyPropertyChanged(free, serverName, resName_ram, propName_ram_free, grm.getType("ram"));
			}
		};
		
		timer.schedule(ramObserver, initialObserveDelay, observerRate);
	}

	public void shutdown() {
		//TODO adjust
		String name = RemoteOsgiUtil.getPlatformIndependentExternalIp();
		grm.unregisterResource(name);
	}

	private Resource benchmarkAndregisterServer() {
		Resource ret = VariantFactory.eINSTANCE.createContainerProvider();

		// get the name of this server
		if(settings.getIpLan().get()) {
			serverName = RemoteOsgiUtil.getLemIp();
		}
		else {
			serverName = RemoteOsgiUtil.getExternalIp();
		}
		serverName = RemoteOsgiUtil.createRemoteOsgiUriString(removeBadLetters(serverName));
		System.out.println("name of the server: " + serverName);
		ret.setName(serverName);
		// set server type
		for (ResourceType sub : ((ResourceType) resourceTypes.getRoot())
				.getSubtypes()) {
			if (sub.getName().toLowerCase().equals("server")) {
				ret.setSpecification(sub);
			}
		}

		ret = getCPUInfos(ret);

		ret = getDiskInfos(ret);

		ret = getNetInfos(ret);

		ret = getRAMInfos(ret);

		VariantModel vm = VariantFactory.eINSTANCE.createVariantModel();
		vm.setRoot(ret);

		int triesLeft = 1;
		boolean failed = false;
		do {
			try {
				org.eclipse.emf.common.util.URI ccmURI = org.eclipse.emf.common.util.URI
						.createPlatformResourceURI(
								CCMUtil.THIS_SERVER_VARIANT_PATH, true);
	
				org.eclipse.emf.ecore.resource.Resource resource = resourceTypes
						.eResource().getResourceSet().createResource(ccmURI);
				resource.getContents().add(vm);
				String serialized = "";
				serialized = CCMUtil.serializeResource(resource);
	
				grm.registerResource(serialized);
	
			} catch (IOException ioe) {
				log.warn("not good", ioe);
				failed = true;
			} catch (RuntimeException re) {
				log.warn("Likely that serialization failed", re);
				failed = true;
			}
		} while(failed && triesLeft-- > 0);

		return ret;
	}

	private String removeBadLetters(String name) {
		name = name.replaceAll("�", "oe");
		return name;
	}

	private Resource getRAMInfos(Resource ret) {
		// get RAM infos
		System.out.println("RAM Infos\n=========");

		ResourceType ramType = (ResourceType) dynamicValues.get(key_ram_type);

		float total = (Float) dynamicValues.get(key_ram_total);
		float free = (Float) dynamicValues.get(key_ram_free);
		float used = total - free;
		System.out.println("Memory (total/free/used): " + total
				+ " / " + free + " / " + used);

		Resource ram = VariantFactory.eINSTANCE.createResource();
		ram.setName("RAM");
		ram.setSpecification(ramType);

		for (Property prop : ramType.getProperties()) {
			if (prop.getDeclaredVariable().getName().equals("size")) {
				VariantPropertyBinding vpb = VariantFactory.eINSTANCE
						.createVariantPropertyBinding();
				vpb.setProperty(prop);

				RealLiteralExpression stre = LiteralsFactory.eINSTANCE
						.createRealLiteralExpression();
				stre.setValue(total);

				vpb.setValueExpression(stre);
				ram.getPropertyBinding().add(vpb);
			}
			if (prop.getDeclaredVariable().getName().equals(propName_ram_used)) {
				VariantPropertyBinding vpb = VariantFactory.eINSTANCE
						.createVariantPropertyBinding();
				vpb.setProperty(prop);

				RealLiteralExpression stre = LiteralsFactory.eINSTANCE
						.createRealLiteralExpression();
				stre.setValue(used);

				vpb.setValueExpression(stre);
				ram.getPropertyBinding().add(vpb);
			}
			if (prop.getDeclaredVariable().getName().equals(propName_ram_free)) {
				VariantPropertyBinding vpb = VariantFactory.eINSTANCE
						.createVariantPropertyBinding();
				vpb.setProperty(prop);

				RealLiteralExpression stre = LiteralsFactory.eINSTANCE
						.createRealLiteralExpression();
				stre.setValue(free);

				vpb.setValueExpression(stre);
				ram.getPropertyBinding().add(vpb);
			}
		}
		ret.getSubresources().add(ram);

		return ret;
	}

	private Resource getCPUInfos(Resource ret) {
		// get CPU infos
		try {
			System.out.println("CPU Infos\n=========");

			ResourceType cpuType = (ResourceType) dynamicValues.get(key_cpu_type);

			for (CpuInfo ci : s.getCpuInfoList()) {
				Resource cpu = VariantFactory.eINSTANCE.createResource();
				cpu.setName(ensureValidName(ci.getModel()));
				cpu.setSpecification(cpuType);
				//TODO set behavior!
				Behavior behavior = loadBehavior("CPU", cpu);

				for (Property prop : cpuType.getProperties()) {
					if (prop.getDeclaredVariable().getName()
							.equals(propName_cpu_freq)) {
						VariantPropertyBinding vpb = VariantFactory.eINSTANCE
								.createVariantPropertyBinding();
						vpb.setProperty(prop);

						RealLiteralExpression stre = LiteralsFactory.eINSTANCE
								.createRealLiteralExpression();
						stre.setValue(ci.getMhz());

						vpb.setValueExpression(stre);
						cpu.getPropertyBinding().add(vpb);
					}
				}

				ret.getSubresources().add(cpu);

				System.out.println(cpu.getName() + " :: " + ci.getMhz()
						+ " :: " + ci.getTotalCores());
				break;
			}
		} catch (SigarException e) {
			e.printStackTrace();
		}
		return ret;
	}

	/** Removes bad letters from the name. Could be used for all names, if so, introduce parameter for kind of name.<br>
	 * Bad letters are currently:
	 * <code>& # ;</code> */
	private String ensureValidName(String name) {
		return name.replaceAll("[&#;]", "_");
	}

	private Behavior loadBehavior(String string, Resource res) {
		Behavior ret = VariantFactory.eINSTANCE.createBehavior();
		ret.setComponent(res);
		
		try {
		URI ccmURI = URI.createPlatformPluginURI("org.coolsoftware.theatre.resourcemanager.lrm/models/"+string+".behavior", true);
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("behavior", new XMIResourceFactoryImpl());
		
		org.eclipse.emf.ecore.resource.Resource resource = rs.getResource(ccmURI, true);

		if (resource.getContents().size() > 0) {
			EObject contents = resource.getContents().get(0);

			if (contents instanceof StateMachine) {
				StateMachine sm = (StateMachine) contents;
				ret.setTemplate(sm);
				ret.setName(sm.getName());
				System.out.println("behavior template set");
			}
		}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return ret;
	}

	private Resource getNetInfos(Resource ret) {
		// get Net infos
		System.out.println("Net Infos\n=========");
		// get network resource type
		ResourceType netType = null;
		for (ResourceType sub : ((ResourceType) resourceTypes.getRoot())
				.getSubtypes()) {
			if (sub.getName().toLowerCase().equals("server")) {
				for (ResourceType dev : sub.getSubtypes()) {
					if (dev.getName().toLowerCase().equals("network")) {
						netType = dev;
					}
				}
			}
		}

		Enumeration<NetworkInterface> netDevs;
		try {
			netDevs = NetworkInterface.getNetworkInterfaces();

			while (netDevs.hasMoreElements()) {
				NetworkInterface ni = netDevs.nextElement();
				if (ni.getDisplayName() == null)
					continue;
				//
				if (!ni.getDisplayName().toLowerCase().contains("virtual")
						&& !ni.getDisplayName().toLowerCase()
								.contains("pseudo")
						&& !ni.getDisplayName().toLowerCase()
								.contains("adapter") && !ni.isLoopback()
						&& ni.getInterfaceAddresses().size() > 0) {

					if (ni.getDisplayName().toLowerCase().contains("ethernet"))
						System.out.println("[ethernet] ");

					if (ni.getDisplayName().toLowerCase().contains("wifi"))
						System.out.println("[wifi] ");

					System.out.print(ni.getName() + ": ");
					System.out.println(ni.getDisplayName());
					for (InterfaceAddress addr : ni.getInterfaceAddresses()) {
						if (addr.getAddress() != null
								&& addr.getAddress() instanceof Inet4Address) {
							System.out.println("["
									+ addr.getAddress().getClass().getName()
									+ "] " + addr.getAddress());
							// create net device
							Resource netResource = VariantFactory.eINSTANCE
									.createResource();
							netResource.setName(ni.getName());
							netResource.setSpecification(netType);

							for (Property prop : netType.getProperties()) {
								if (prop.getDeclaredVariable().getName()
										.equals("address")) {
									VariantPropertyBinding vpb = VariantFactory.eINSTANCE
											.createVariantPropertyBinding();
									vpb.setProperty(prop);
									String ip = addr.getAddress().toString();
									if (ip.startsWith("/192.168.") && !settings.getIpLan().get()) {
										ip = RemoteOsgiUtil.getExternalIp();
										System.out.println("["
												+ addr.getAddress().getClass()
														.getName()
												+ "] unNATed " + ip);
									}
									StringLiteralExpression stre = LiteralsFactory.eINSTANCE
											.createStringLiteralExpression();
									stre.setValue(ip);

									vpb.setValueExpression(stre);
									netResource.getPropertyBinding().add(vpb);
								}
								if (prop.getDeclaredVariable().getName()
										.equals("throughput")) {
									VariantPropertyBinding vpb2 = VariantFactory.eINSTANCE
											.createVariantPropertyBinding();
									vpb2.setProperty(prop);

									IntegerLiteralExpression stre = LiteralsFactory.eINSTANCE
											.createIntegerLiteralExpression();
									if (ni.getDisplayName().toLowerCase()
											.contains("gigabit")) {
										stre.setValue(1024 * 1024 * 1024); // 1
																			// GB/s
									} else if (ni.getDisplayName()
											.toLowerCase().contains("ethernet")) {
										stre.setValue(1024 * 1024 * 100); // 100
																			// MB/s
									} else if (ni.getDisplayName()
											.toLowerCase().contains("wifi")) {
										stre.setValue(1024 * 1024 * 54); // 54
																			// MB/s
									} else {
										stre.setValue(1024 * 1024 * 10); // 10
																			// MB/s
									}

									vpb2.setValueExpression(stre);
									netResource.getPropertyBinding().add(vpb2);
								}
							}
							// add to server
							ret.getSubresources().add(netResource);
						}
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return ret;
	}

	private Resource getDiskInfos(Resource ret) {
		FileReader fr = null;
		// get HDD infos
		try {
			ResourceType hddType = null;
			for (ResourceType sub : ((ResourceType) resourceTypes.getRoot())
					.getSubtypes()) {
				if (sub.getName().toLowerCase().equals("server")) {
					for (ResourceType dev : sub.getSubtypes()) {
						if (dev.getName().toLowerCase().equals("hdd")) {
							hddType = dev;
						}
					}
				}
			}
			System.out.println("HDD Infos\n=========");
			for (FileSystem fs : s.getFileSystemList()) {
				if (fs.getTypeName().equals("local")) {
					String devName = fs.getDevName();
					float total = (new File(devName).getTotalSpace() / 1024f / 1024f);
					float free = (new File(devName).getFreeSpace() / 1024f / 1024f);
					float used = total - free;

					System.out
							.println(devName
									+ " total: "
									+ (new File(devName).getTotalSpace() / 1024d / 1024d / 1024d)
									+ " GB");
					System.out
							.println(devName
									+ " free: "
									+ (new File(devName).getFreeSpace() / 1024d / 1024d / 1024d)
									+ " GB");

					float throughput_r = 0;
					float throughput_w = 0;
					boolean needToMeasure = true;
					if(settings.getLrmCacheStatics().get()) {
						// check for former values
						 if(staticValueCacheFile.exists()) {
							 needToMeasure = false;
							 Properties cache = new Properties();
							 cache.load(new FileInputStream(staticValueCacheFile));
							 // fill in former values
							 String tmp = cache.getProperty(devName + ".throughput_r");
							 if(tmp != null) {
								 throughput_r = Float.parseFloat(tmp);
							 }
							 tmp = cache.getProperty(devName + ".throughput_w");
							 if(tmp != null) {
								 throughput_w = Float.parseFloat(tmp);
							 }
						 }
					}
					if(needToMeasure) {
					// measure throughput
						String dir = settings.getLrmBenchmarkDirectory() + File.separator;
						String filename;
						if (System.getProperty("os.name").toLowerCase()
								.equals("linux"))
							filename = dir + "qbench.tmp";
						else
							filename = devName + dir + "qbench.tmp";
	
						File f = new File(filename);
						boolean fileOk = false;
						try {
							f.createNewFile();
							fileOk = true;
						} catch(IOException o) {
							System.out.println("cannot create test file here: "+f);
						}
						if (fileOk || f.exists()) {
							FileWriter fw = new FileWriter(f);
							long start = System.currentTimeMillis();
							for (int i = 0; i < (1024 * 1024 * 5); i++) { // 5MB ?!
								fw.write(1);
							}
							long duration = System.currentTimeMillis() - start;
							throughput_w = 5 / (duration / 1000f);
							fw.close();
	
							fr = new FileReader(f);
							start = System.currentTimeMillis();
							while (fr.read() != -1) {
							}
							duration = System.currentTimeMillis() - start;
							throughput_r = 5 / (duration / 1000f);
	
							f = null;
	
							// write result
							 Properties cache = new Properties();
							 if(!staticValueCacheFile.exists()) {
								staticValueCacheFile.createNewFile();
							 }
							 else {
								 FileInputStream fis = new FileInputStream(staticValueCacheFile);
								 cache.load(fis);
								 fis.close();
							 }
							 // fill in values
							 cache.setProperty(devName + ".throughput_r", Float.toString(throughput_r));
							 cache.setProperty(devName + ".throughput_w", Float.toString(throughput_w));
							 FileOutputStream fos = new FileOutputStream(staticValueCacheFile);
							 cache.store(fos, "Properties of selected static-instance values");
							 fos.close();
							
						}
						
						System.out.println(devName + " write throughput: "
								+ throughput_w + " MB/s");
						System.out.println(devName + " read throughput: "
								+ throughput_r + " MB/s");

						Resource hdd = VariantFactory.eINSTANCE
								.createResource();
						hdd.setName(devName.replace(":\\", ""));
						hdd.setSpecification(hddType);

						for (Property prop : hddType.getProperties()) {
							if (prop.getDeclaredVariable().getName()
									.equals("size")) {
								VariantPropertyBinding vpb = VariantFactory.eINSTANCE
										.createVariantPropertyBinding();
								vpb.setProperty(prop);

								RealLiteralExpression stre = LiteralsFactory.eINSTANCE
										.createRealLiteralExpression();
								stre.setValue(total);

								vpb.setValueExpression(stre);
								hdd.getPropertyBinding().add(vpb);
							}
							if (prop.getDeclaredVariable().getName()
									.equals(propName_ram_used)) {
								VariantPropertyBinding vpb = VariantFactory.eINSTANCE
										.createVariantPropertyBinding();
								vpb.setProperty(prop);

								RealLiteralExpression stre = LiteralsFactory.eINSTANCE
										.createRealLiteralExpression();
								stre.setValue(used);

								vpb.setValueExpression(stre);
								hdd.getPropertyBinding().add(vpb);
							}
							if (prop.getDeclaredVariable().getName()
									.equals("throughput_w")) {
								VariantPropertyBinding vpb = VariantFactory.eINSTANCE
										.createVariantPropertyBinding();
								vpb.setProperty(prop);

								RealLiteralExpression stre = LiteralsFactory.eINSTANCE
										.createRealLiteralExpression();
								stre.setValue(throughput_w);

								vpb.setValueExpression(stre);
								hdd.getPropertyBinding().add(vpb);
							}
							if (prop.getDeclaredVariable().getName()
									.equals("throughput_r")) {
								VariantPropertyBinding vpb = VariantFactory.eINSTANCE
										.createVariantPropertyBinding();
								vpb.setProperty(prop);

								RealLiteralExpression stre = LiteralsFactory.eINSTANCE
										.createRealLiteralExpression();
								stre.setValue(throughput_r);

								vpb.setValueExpression(stre);
								hdd.getPropertyBinding().add(vpb);
							}
						}
						

						ret.getSubresources().add(hdd);
						
					}
				}
			}

		} catch (SigarException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(fr != null) { try { fr.close(); } catch(IOException ignore) { } }
		}
		return ret;
	}

	/**
	 * Use CCMUtil instead
	 * 
	 * @deprecated
	 */
	private static StructuralModel deserializeStructModel(
			String serializedStructModel) throws IOException {
		// dump received model to disk
		// File tmp = dumpFile("lrmtmp.structure",serializedStructModel);
		ByteArrayInputStream bStruct = new ByteArrayInputStream(
				serializedStructModel.getBytes());

		// load it using emf + emftext factory
		EObject result = null;
		org.eclipse.emf.common.util.URI ccmURI = org.eclipse.emf.common.util.URI
				.createPlatformResourceURI("/theatre/ServerD.structure", false);
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry()
				.getExtensionToFactoryMap()
				.put(ResourceFactoryRegistryImpl.DEFAULT_EXTENSION,
						new EcoreResourceFactoryImpl());
		rs.getPackageRegistry()
				.put("structure", StructurePackageImpl.eINSTANCE);
		org.eclipse.emf.ecore.resource.Resource resource = rs.getResource(
				ccmURI, false);

		/* Check if the resource has already been created. */
		if (resource == null) {
			resource = rs.createResource(ccmURI);
		}
		// no else.

		if (!resource.isLoaded())
			resource.load(bStruct, null);
		// no else.

		if (resource.getContents().size() > 0) {
			result = resource.getContents().get(0);
		}

		return (StructuralModel) result;
	}

	@Deprecated
	private static File dumpFile(String fileName, String content)
			throws IOException {

		fileName = new File(".").getAbsolutePath() + "/" + fileName;
		System.out.println(fileName);
		File tmp = new File(fileName);
		if (content == null) {
			System.out.println("[LRM] content is null - i cannot dump nothing");
			return tmp;
		}
		if (!tmp.exists())
			tmp.createNewFile();
		FileWriter fw = new FileWriter(tmp, false);
		fw.write(content);
		fw.close();
		return tmp;
	}

	@Override
	public boolean reset(boolean full) {
		// nothing to be resetted
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return "Sigar-LRM@"+PlatformUtils.getHostName();
	}

	protected void setTheatreSettings(TheatreSettings ts) {
		this.settings = ts;
	}

	protected void unsetTheatreSettings(TheatreSettings ts) {
		this.settings = null;
	}
	
	protected void activate(ComponentContext ctx) {
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(ctx.getBundleContext());
		activate(RemoteOsgiUtil.getRemoteService(remote, settings.getGrmUri().get(),
				IGlobalResourceManager.class));
	}

	/**
	 * @param localGrm
	 */
	protected void activate(IGlobalResourceManager localGrm) {
		this.grm = localGrm;
		// ask global energy manager for resource types
		String resTypes = grm.getInfrastructureTypes();
		log.debug("Got resTypes from GRM: " + resTypes);
		try {
			resourceTypes = CCMUtil.deserializeHWStructModel(resTypes, false);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("Not able to deserialize the Structure Model from GRM");
		}
		
		createCacheRefresher();
		
		benchmarkAndregisterServer();
		
		startObserving();
		
	}

}
