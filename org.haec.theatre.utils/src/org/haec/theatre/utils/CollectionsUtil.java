/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;

/**
 * Utility class similar to Collections.
 * @author René Schöne */
public class CollectionsUtil {
	
	/**
	 * Calls the given function on each element of src.
	 * Preserves resulting <code>null</code> values.
	 * @param src the elements
	 * @param f the function
	 * @return the resulting list
	 * @see #map(Iterable, Function, boolean)
	 * @see #wrap(Object[])
	 */
	public static <S, T> List<T> map(S[] src, Function<S, T> f) {
		return map(wrap(src), f);
	}

	/**
	 * Calls the given function on each element of src.
	 * Preserves resulting <code>null</code> values.
	 * @param src the elements
	 * @param f the function
	 * @return the resulting list
	 * @see #map(Iterable, Function, boolean)
	 */
	public static <S, T> List<T> map(Iterable<S> src, Function<S, T> f) {
		return map(src, f, false);
	}
	
	/**
	 * Calls the given function on each element of src.
	 * Discards resulting <code>null</code> values if instructed to do so.
	 * @param src the elements
	 * @param f the function
	 * @param discardNull whether to discard <code>null</code> return values of the function application
	 * @return the resulting list
	 * @see #map(Iterable, Function, boolean)
	 * @see #wrap(Object[])
	 */
	public static <S, T> List<T> map(S[] src, Function<S, T> f, boolean discardNull) {
		return map(wrap(src), f, discardNull);
	}

	/**
	 * Calls the given function on each element of src.
	 * Discards resulting <code>null</code> values if instructed to do so.
	 * @param src the elements
	 * @param f the function
	 * @param discardNull whether to discard <code>null</code> return values of the function application
	 * @return the resulting list
	 */
	public static <S, T> List<T> map(Iterable<S> src, Function<S, T> f, boolean discardNull) {
		// find out type of list
		List<T> result;
		Class<?> srcClazz = src.getClass();
		if(srcClazz.equals(LinkedList.class)) {
			result = new LinkedList<T>();
		} else { // default
			result = new ArrayList<T>();
		}
		for(S s : src) {
			T t = f.apply(s);
			if(!discardNull || t != null) {
				result.add(t);
			}
		}
		return result;
	}
	
	/**
	 * Calls the given function on each value of src and returns a mapping of each key to the result of the function
	 * application of the corresponding value. Does not discard resulting <code>null</code> values.
	 * @param src the source map
	 * @param f the function to apply
	 * @return the resulting map
	 */
	public static <K, S, T> Map<K, T> map(Map<K, S> src, Function<S, T> f) {
		Map<K, T> result = new HashMap<>();
		for(Entry<K, S> e : src.entrySet()) {
			result.put(e.getKey(), f.apply(e.getValue()));
		}
		return result;
	}
	
	
	public static <S> Iterable<S> wrap(S[] s) {
		return Arrays.asList(s);
	}
	
	/**
	 * @return essentially {@link #get(Map, Object, Object)} with a new {@link HashMap} as defaultValue
	 */
	public static <K1, K2, V> Map<K2, V> ensureHashMap(Map<K1, Map<K2, V>> map,
			K1 key) {
		Map<K2, V> result = map.get(key);
		if(result == null) {
			result = new HashMap<>();
			map.put(key, result);
		}
		return result;
	}
	
	/**
	 * @return essentially {@link #get(Map, Object, Object)} with a new {@link HashSet} as defaultValue
	 */
	public static <K, V> Set<V> ensureHashSet(Map<K, Set<V>> map, K key) {
		Set<V> result = map.get(key);
		if(result == null) {
			result = new HashSet<>();
			map.put(key, result);
		}
		return result;
	}
	
	/**
	 * @return essentially {@link #get(Map, Object, Object)} with a new {@link ArrayList} as defaultValue
	 */
	public static <K, V> List<V> ensureArrayList(Map<K, List<V>> map, K key) {
		List<V> result = map.get(key);
		if(result == null) {
			result = new ArrayList<>();
			map.put(key, result);
		}
		return result;
	}
	
	/**
	 * map.get(key) with default value
	 * @param map the map to operate on
	 * @param key the given key
	 * @param defaultValue the defaultValue, returned if the value is <code>null</code>
	 * @return the value of the map, or defaultValue if no such value
	 */
	public static <K, S> S get(Map<K, ?> map, K key, S defaultValue) {
		Object value = map.get(key);
		@SuppressWarnings("unchecked")
		S result = value == null ? defaultValue : (S) value;
		return result;
	}
	
	/**
	 * map.get(key) with lazy default value
	 * @param map the map to operate on
	 * @param key the given key
	 * @param defaultValue the defaultValue, returned if the value is <code>null</code>
	 * @return the value of the map, or defaultValue if no such value
	 * @throws Exception if callable throws an exception
	 */
	public static <K, S> S get(Map<K, ?> map, K key, Callable<S> defaultValueCallable) throws Exception {
		Object value = map.get(key);
		@SuppressWarnings("unchecked")
		S result = value == null ? defaultValueCallable.call() : (S) value;
		return result;
	}
	
	/**
	 * Returns {@link #get(Map, Object, Callable)} ignoring exceptions
	 * @param map the map to operate on
	 * @param key the given key
	 * @param defaultValue the defaultValue, returned if the value is <code>null</code>
	 * @return the value of the map, or defaultValue if no such value,
	 *  or <code>null</code> on Exception of the callable
	 */
	public static <K, S> S getIgnoreException(Map<K, ?> map, K key, Callable<S> defaultValueCallable) {
		try { return get(map, key, defaultValueCallable); } catch (Exception e) { return null; }
	}
	
	/**
	 * Removes all mappings with the given keys.
	 * @param map the map to operate on
	 * @param keys the keys to remove
	 * @return the number of remove mappings
	 */
	@SafeVarargs
	public static <K> int removeAll(Map<K, ?> map, K... keys) {
		int removed = 0;
		for(K key : keys) {
			if(map.remove(key) != null) { removed++; }
		}
		return removed;
	}

	/**
	 * @return essentially {@link #get(Map, Object, Object)} with a new {@link TreeMap} as defaultValue
	 */
	public static <K1, K2, V, M  extends Map<K2, V>> Map<K2, V> ensureTreeMap(Map<K1, M> map,
			K1 key) {
		M result = map.get(key);
		if(result == null) {
			@SuppressWarnings("unchecked")
			M m = (M) new TreeMap<>();
			result = m;
			map.put(key, result);
		}
		return result;
	}

}
