/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.TheatreCommandProvider;
import org.coolsoftware.theatre.util.Reconnectable;
import org.coolsoftware.theatre.util.Reconnector;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Utility class providing methods to resolve and to start bundles by symbolic names.
 * @author René Schöne
 */
public class BundleUtils {
	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	public static SortedMap<String, Bundle> resolveBundles(BundleContext context, List<String> symbolicNames) {
		List<String> toSeekFor = new ArrayList<String>(symbolicNames);
		SortedMap<String,Bundle> resolvedBundles = new TreeMap<String, Bundle>();
		for(Bundle b : context.getBundles()) {
//			log.debug(String.format("id: %s, location: %s, class: %s, symName: %s, state: %d%n",
//					b.getBundleId(), b.getLocation(), b.getClass(),
//					b.getSymbolicName(), b.getState()));
			for(Iterator<String> iter = toSeekFor.iterator(); iter.hasNext();) {
				 String symName = iter.next();
//					log.debug(" " + symName);
				 if(symName.equals(b.getSymbolicName())) {
					resolvedBundles.put(symName, b);
					iter.remove();
				}
			}
			if(toSeekFor.isEmpty()) {
				break;
			}
		}
		for(String notFound : toSeekFor) {
			resolvedBundles.put(notFound, null);
		}
		return resolvedBundles;
	}
	
	public static SortedMap<String, Bundle> resolveBundles(BundleContext context, String... symbolicNames) {
		List<String> names = new ArrayList<String>(Arrays.asList(symbolicNames));
		return resolveBundles(context, names);
	}
	
	/**
	 * Starts given bundles.
	 * @param context the context for searching bundles
	 * @param bundleNames the symbolic names of bundles
	 * @return { bundleName : state }
	 */
	public static Map<String, Integer> startBundles(BundleContext context, List<String> bundleNames) {
		Map<String, Integer> bundleStates = new HashMap<>();
		try {
			log.debug("try starting bundles");
			log.debug("context=" + context + ", bundleNames=" + bundleNames);
			Map<String, Bundle> resolvedBundles;
			resolvedBundles = resolveBundles(context, bundleNames);
			log.debug("resolvedBundles="+ resolvedBundles);
			for(Entry<String,Bundle> entry : resolvedBundles.entrySet()) {
				String symName = entry.getKey();
				Bundle b = entry.getValue();
				try {
					log.info("Starting " + symName);
					b.start();
				} catch (BundleException e) {
					log.warn("Error while starting " + symName, e);
				}
				bundleStates.put(symName, b.getState());
			}
		} catch (Exception e) {
			log.warn("Error while starting bundles", e);
		}
		return bundleStates;
	}

	/** @return first bundle found with matching symbolic name, or <code>null</code> if not bundle matches */
	public static Bundle getBundle(BundleContext context, String symbolicName) {
		if(context == null || symbolicName == null)
			return null;
		for(Bundle b : context.getBundles()) {
			if(symbolicName.equals(b.getSymbolicName())) {
				return b;
			}
		}
		log.warn("No bundle found with symbolicname = '"+symbolicName+"'.");
		return null;
	}
	
	public static void lazyRegisterToReconnector(final BundleContext context, final Reconnectable instance) {
		try {
			context.addServiceListener(new ServiceListener() {
				@Override
				public void serviceChanged(ServiceEvent event) {
//				log.debug("event occurred: " + event.getType() + "|" + event.getServiceReference());
					switch(event.getType()) {
					case ServiceEvent.REGISTERED:
						ServiceReference<?> ref = event.getServiceReference();
						((Reconnector) context.getService(ref)).registerForReconnect(instance);
						break;
					}
				}
			}, "("+Constants.OBJECTCLASS+"=*"+Reconnector.class.getName()+"*)");
		} catch (InvalidSyntaxException e) {
			log.debug("Error installing service listener", e);
		}

	}
	
	public static ServiceRegistration<TheatreCommandProvider>
			register(TheatreCommandProvider instance, BundleContext context) {
		Dictionary<String, Object> properties = new Hashtable<>();
		properties.put(TheatreCommandProvider.PROPERTY_COMMAND_NAME, instance.getCommandName());
		return RemoteOsgiUtil.register(context, TheatreCommandProvider.class, instance, properties);
	}


}
