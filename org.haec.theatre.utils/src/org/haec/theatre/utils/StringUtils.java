/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

/** Utility class providing methods concerning strings and their manipulation.
 * @author René Schöne */
public class StringUtils {
	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	/** Simple join-Implementation. Returns an empty string, if <code>elements.length == 0</code>.
	 * Returns the element, if <code>elements.length == 1</code>.
	 * @param delimiter the delimiter sequence to put between the elements
	 * @param elements the elements to join together
	 * @return e[0] + delimiter + e[1] + ... + e[n-1] + delimiter + e[n] */
	public static String join(String delimiter, String... elements) {
		return join(delimiter, (Object[]) elements);
	}
	
	/** Simple join-Implementation. Returns an empty string, if <code>elements.length == 0</code>.
	 * Returns the element, if <code>elements.length == 1</code>.
	 * @param delimiter the delimiter sequence to put between the elements
	 * @param elements the elements to join together
	 * @return e[0] + delimiter + e[1] + ... + e[n-1] + delimiter + e[n] */
	public static String join(String delimiter, Object... elements) {
		boolean first = true;
		StringBuilder sb = new StringBuilder();
		for(Object e : elements) {
			if(first) {
				first = false;
			}
			else {
				sb.append(delimiter);
			}
			sb.append(e);
		}
		return sb.toString();
	}
	
	/** Simple join-Implementation. Returns an empty string, if <code>elements.length == 0</code>.
	 * Returns the element, if <code>elements.length == 1</code>.
	 * @param delimiter the delimiter sequence to put between the elements
	 * @param elements the elements to join together
	 * @return e[0] + delimiter + e[1] + ... + e[n-1] + delimiter + e[n] */
	public static String joinIterable(String delimiter, Iterable<?> elements) {
		boolean first = true;
		StringBuilder sb = new StringBuilder();
		for(Object e : elements) {
			if(first) {
				first = false;
			}
			else {
				sb.append(delimiter);
			}
			sb.append(e);
		}
		return sb.toString();
	}
	
	/**
	 * Does multiple replacements for one src-String. Runs in linear time.
	 * The target at index <i>i</i> is replaced by the replacement at index <i>i</i>.
	 * @param src the source string
	 * @param targets characters to replace
	 * @param replacements the replacements for one target
	 * @return the resulting string
	 */
	public static String replaceMultiple(String src, char[] targets, String[] replacements) {
		if(src == null) {
			log.warn("Src string was null.");
			return "null";
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < src.length(); i++) {
			char c = src.charAt(i);
			boolean found = false;
			for (int j = 0; j < targets.length; j++) {
				if(c == targets[j]) {
					sb.append(replacements[j]);
					found = true;
				}
			}
			if(!found) {
				sb.append(c);
			}
		}
		return sb.toString();
	}
	
	/**
	 * Splits the given string into token seperated by the given delimiter
	 * @param src the source string to divide
	 * @param delimiter the delimiter to search for as seperator
	 * @return an iterable sequence of tokens
	 * @see StringTokenizer
	 */
	public static Iterable<String> tokenize(String src, String delimiter) {
		if(src == null || delimiter == null) {
			log.warn("Passed " + StringUtils.cap(src, 50) + " and " + delimiter + ". Null not allowed.");
			return Collections.emptyList();
		}
		final StringTokenizer st = new StringTokenizer(src, delimiter);
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return new Iterator<String>() {

					@Override
					public boolean hasNext() {
						return st.hasMoreTokens();
					}

					@Override
					public String next() {
						return st.nextToken();
					}

					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	/**
	 * Parses the given line into an object. It is assumed, that the line has the format
	 * <pre>
	 * &lt;identifer&gt;,&lt;content&gt;
	 * </pre>
	 * Currently supported classes:
	 * <table>
	 * <tr><th>Class</th><th>identifier</th><th>default</th><th>Notes</th></tr>
	 * <tr><td>{@link File}</td><td>file</td><td></td><td>No default value</td></tr>
	 * <tr><td>{@link Integer}</td><td>int</td><td>0</td></tr>
	 * <tr><td>{@link Float}</td><td>float</td><td>0f</td></tr>
	 * <tr><td>{@link Boolean}</td><td>bool</td><td>false</td></tr>
	 * <tr><td>{@link String}</td><td><i>empty</i></td><td></td><td>No default value. Chosen by default.</td></tr>
	 * </table>
	 * @param line the given line to parsed
	 * @return the parsed object, or a default value
	 */
	public static Object parseStringParam(String line) {
		String[] tokElems = line.split(",");
		String type;
		String valueRepr;
		switch(tokElems.length) {
		case 1:
			type = "";
			valueRepr = tokElems[0];
			break;
		case 2:
		default:
			type = tokElems[0].toLowerCase();
			valueRepr = tokElems[1];
			break;
		}
		if(tokElems.length == 1) {
		}
		Object val;
		if(type.equals("file")) {
			val = new File(valueRepr);
		}
		else if(type.startsWith("int")) {
			try {
				val = Integer.parseInt(valueRepr);
			} catch (NumberFormatException e) {
				log.warn("Could not convert " + valueRepr + " to integer", e);
				val = 0;
			}
		}
		else if(type.equals("float")) {
			try {
				val = Float.parseFloat(valueRepr);
			} catch (NumberFormatException e) {
				log.warn("Could not convert " + valueRepr + " to float", e);
				val = 0f;
			}
		}
		else if(type.startsWith("bool")) {
			try {
				val = Boolean.parseBoolean(valueRepr);
			} catch (Exception e) {
				log.warn("Could not convert " + valueRepr + " to boolean", e);
				val = false;
			}
		}
		else {
			val = valueRepr;
		}
		return val;
	}

	/**
	 * Caps the string representation to maximum length of maxLength.
	 * Basically equals to <code>maxlength</code>-trimmed output of <code>o.toString()</code>.
	 * This is equivalent to <pre>cap(obj, 0, maxLength)</pre>
	 * @param obj the object to be representated
	 * @param maxLength the maximumLength. Has to be &ge; 4
	 * @return the capped string representation
	 * @see #cap(Object, int, int)
	 */
	public static String cap(Object obj, int maxLength) {
		return cap(obj, 0, maxLength);
	}

	/**
	 * Caps the string representation to maximum length of maxLength, beginning at character <code>start</code>.
	 * Basically equals to <code>maxlength</code>-trimmed output of <code>obj.toString().substring(start)</code>.
	 * If <code>obj.toString().length() < start</code>, the complete string is used to cap.
	 * @param obj the object to be representated
	 * @param start the character index to start at
	 * @param maxLength the maximumLength. Has to be &ge; 4
	 * @return the capped string representation
	 */
	public static String cap(Object obj, int start, int maxLength) {
		if(obj == null) {
			return "Null";
		}
		String s = obj.toString();
		if(0 <= start && start < s.length())
			s = s.substring(start);
		int length = s.length();
		if(length <= maxLength) {
			return s;
		}
		return s.substring(0, maxLength-3) + "...";
	}

	public static String i(Integer i, int defaultValue) {
		return i == null ? Integer.toString(defaultValue) : i.toString();
	}

	public static String d(Double d, double defaultValue) {
		return d == null ? Double.toString(defaultValue) : d.toString();
	}

	public static String l(Long l, long defaultValue) {
		return l == null ? Long.toString(defaultValue) : l.toString();
	}

}
