/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.settings;

/**
 * A key-value-pair with default value.
 * @author René Schöne
 * @param <S> the type of the value in this setting
 */
public interface Setting<S> {

	/**
	 * @return the current value, or the default value if the former is not set
	 */
	public S get();

	/**
	 * @return <code>true</code> if {@link #get()} will return the default value
	 */
	public boolean isDefault();

	/**
	 * @return the unique name of this setting
	 */
	public String getName();

}
