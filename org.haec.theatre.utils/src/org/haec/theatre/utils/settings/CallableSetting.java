/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.settings;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

/**
 * A key-value-pair with default value. Uses a {@link Callable} to set value.
 * @author René Schöne
 * @param <S> the type of the value in this setting
 */
public class CallableSetting<S> implements Setting<S> {
	
	private String name;
	private transient Callable<S> callable;
	private Logger log = Logger.getLogger(CallableSetting.class);

	/**
	 * Creates a new setting.
	 * If at any time the callable throws an exception, <code>null</code> will be returned.
	 * @param c the callable to use to get the value
	 */
	public CallableSetting(String name, Callable<S> c) {
		this.name = name;
		this.callable = c;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.settings.Setting#get()
	 */
	@Override
	public S get() {
		try {
			return callable.call();
		} catch (Exception e) {
			log.debug("Callable throws an error", e);
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.settings.Setting#isDefault()
	 */
	@Override
	public boolean isDefault() {
		// always default
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.settings.Setting#getName()
	 */
	public String getName() {
		return name;
	}

}
