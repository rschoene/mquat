/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.settings;

import java.util.Properties;

/**
 * A key-value-pair with default value. Uses {@link Properties} to set values.
 * @author René Schöne
 * @param <S> the type of the value in this setting
 */
public abstract class PropertiesSetting<S> implements Setting<S> {
	public final String key;
	private S defaultValue;
	private S currentValue;
	
	public PropertiesSetting(String key, S defaultValue) {
		this.key = key;
		this.defaultValue = defaultValue;
	}
	
	public S get() {
		return currentValue == null ? defaultValue : currentValue;
	}
	
	public boolean isDefault() {
		return (currentValue == null || currentValue.equals(defaultValue));
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.settings.Setting#getName()
	 */
	@Override
	public String getName() {
		return key;
	}
	
	/**
	 * Updates this setting
	 * @param newCurrentValue the new current value
	 */
	protected void set0(S newCurrentValue) {
		this.currentValue = newCurrentValue;
	}
	/**
	 * Updates this setting
	 * @param stringValue the string value read from the properties
	 */
	public abstract void set(String stringValue);
	
	/**
	 * Updates this setting
	 * @param p the properties to use
	 */
	public void set(Properties p) {
		String stringValue = p.getProperty(this.key);
		if(stringValue == null) {
			return;
		}
		set(stringValue);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getClass().getName()+" ("+key+"="+get()+")";
	}
}