/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.settings;

/**
 * A key-{@link Boolean}-pair.
 * @author René Schöne
 */
public class BooleanSetting extends PropertiesSetting<Boolean> {
	public BooleanSetting(String key, Boolean defaultValue) {super(key, defaultValue);}
	public void set(String stringValue) {try{set0(Boolean.parseBoolean(stringValue));}finally{}}
	
	public boolean isTrue() {
		return get().booleanValue();
	}
	
	public boolean isFalse() {
		return !get().booleanValue();
	}
}