/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.settings;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Helper class to store multiple settings and be able to reload them use a .properties file.
 * Subclasses are expected to have public static Setting fields, which will be updated then.
 * <b>Remark:</b> This only works if the subclass is public. If not public, use the methods with the List parameter instead.
 * @author René Schöne
 */
public class SettingHolder {

	private static Logger log = Logger.getLogger(SettingHolder.class);

	/**
	 * Get all setting-fields in the particular class.
	 * Uses reflection to find them, so the fields have to be visible.
	 * @param sh the setting holder to inspect
	 * @return a list of found settings
	 */
	public static List<Setting<?>> getSettings(final SettingHolder s) {
		final Class<? extends SettingHolder> clazz = s.getClass();
		List<Setting<?>> settings = new ArrayList<>();
		for(Field f : clazz.getFields()) {
			if(Setting.class.isAssignableFrom(f.getType())) {
				try {
					settings.add((Setting<?>) f.get(s));
				} catch (SecurityException | IllegalArgumentException
						| IllegalAccessException e) {
					log.warn("Could not add " + f.getName(), e);
				}
			}
		};
		return settings;
	}
	
	/**
	 * Loads the main settings from the <code>theatre/theatre.properties</code> file.
	 * The system property <i>theatre.properties</i> may be used to set another input file.
	 * @param sh the setting holder to load settings for
	 * @return the actual file path used to load the settings
	 */
	public static String loadTheatreProperties(SettingHolder sh) {
		String fname = System.getProperty("theatre.properties", "theatre/theatre.properties");
		load(fname, sh);
		return fname;
	}
	
	/**
	 * Loads the main settings from the <code>theatre/theatre.properties</code> file.
	 * The system property <i>theatre.properties</i> may be used to set another input file.
	 * @param settings the settings to load
	 * @return the actual file path used to load the settings
	 */
	public static String loadTheatreProperties(List<Setting<?>> settings) {
		String fname = System.getProperty("theatre.properties", "theatre/theatre.properties");
		load(fname, settings);
		return fname;
	}

	/**
	 * Loads settings into the given class using the properties file at the given path
	 * @param fname the path to load the properties
	 * @param sh the setting holder to load settings for
	 */
	public static void load(String fname, SettingHolder sh) {
		log.debug("Loading settings for " + sh);
		reload(loadProperties(fname), sh);
	}

	/**
	 * Loads settings using the properties file at the given path
	 * @param fname the path to load the properties
	 * @param settings the settings to load
	 */
	public static void load(String fname, List<Setting<?>> settings ) {
		log.debug("Loading " + settings.size() + " settings.");
		reload(loadProperties(fname), settings);
	}
	
	protected static Properties loadProperties(String fname) {
		Properties p = new Properties();
		try(FileReader fr = new FileReader(fname)) {
			p.load(fr);
		} catch (IOException e) {
			log.warn("Could not load settings at " + fname, e);
		}
		log.debug("Loaded " + p.size() + " settings from " + fname);
		return p;
	}

	/**
	 * Loads settings into the given class using an inputstream for loading the properties
	 * @param is the given inputstream
	 * @param sh the setting holder to load settings for
	 */
	public static void load(InputStream is, SettingHolder sh) {
		log.debug("Reloading settings for " + sh);
		reload(loadProperties(is), sh);
	}

	/**
	 * Loads settings using an inputstream for loading the properties
	 * @param is the given inputstream
	 * @param settings the settings to load
	 */
	public static void load(InputStream is, List<Setting<?>> settings) {
		log.debug("Reloading " + settings.size() + " settings.");
		reload(loadProperties(is), settings);
	}

	protected static Properties loadProperties(InputStream is) {
		Properties p = new Properties();
		try {
			p.load(is);
		} catch (IOException e) {
			log.warn("Could not load ilp settings", e);
		}
		return p;
	}

	/**
	 * Loads settings into the given class using the system properties
	 * @param clazz the class to load settings for
	 */
	public static void load(List<Setting<?>> settings) {
		log.debug("Reloading " + settings.size() + " settings from system properties.");
		reload(System.getProperties(), settings);
	}

	/**
	 * Loads settings using the system properties
	 * @param sh the setting holder to load settings for
	 */
	public static void load(SettingHolder sh) {
		log.debug("Reloading settings for " + sh + " settings from system properties.");
		reload(System.getProperties(), sh);
	}

	protected static void reload(Properties p, SettingHolder sh) {
		List<Setting<?>> settings = getSettings(sh);
		reload(p, settings);
	}
	
	protected static void reload(Properties p, List<Setting<?>> settings ) {
		for(Setting<?> s : settings) {
			if(s instanceof PropertiesSetting) {
				((PropertiesSetting<?>) s).set(p);
			}
		}
	}
	
	public static interface ChangeHandler {
		
		public void handleChange(List<Setting<?>> changedSettings);
	}
	
	private static class MyWatchQueueReader implements Runnable {

		private WatchService myWatcher;
		private Map<String, Object> values;
		private List<Setting<?>> settings;
		private Path dir;
		private String fileName;
		private ChangeHandler changeHandler;
		
		/**
		 * @param myWatcher watch service to poll events from
		 * @param handler the handler to call in case of updates
		 * @param dir the observed directory
		 * @param fname the file name including the directory
		 * @param settings the settings to update
		 */
		public MyWatchQueueReader(WatchService myWatcher, ChangeHandler handler,
				Path dir, String fname, List<Setting<?>> settings) {
			this.myWatcher = myWatcher;
			this.values = new HashMap<>();
			this.settings = settings;
			this.changeHandler = handler;
			update();
			this.dir = dir;
			this.fileName = fname;
		}

		private List<Setting<?>> update() {
			List<Setting<?>> changed = new ArrayList<>();
			for(Setting<?> s : settings) {
				if(s instanceof PropertiesSetting) {
					String key = ((PropertiesSetting<?>) s).key;
					Object formerValue = values.get(key);
					Object newValue = s.get();
					if(formerValue == null) {
						if(newValue != null) {
							changed.add(s);
						}
					} else {
						if(!formerValue.equals(newValue)) {
							changed.add(s);
						}
					}
					values.put(key, newValue);
				} else {
					log.warn("Unsupported setting: " + s);
				}
			}
			return changed;
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			try {
				// get the first event before looping
				WatchKey key = myWatcher.take();
				while(key != null) {
					// we have a polled event, now we traverse it and 
					// receive all the states from it
					for (WatchEvent<?> event : key.pollEvents()) {
						if(event.kind().equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
							// update settings
							Path p = (Path) event.context();
							if(p.endsWith(fileName)) {
								load(dir.resolve(p).toAbsolutePath().toString(), settings);
								List<Setting<?>> changedSettings = update();
								if(changeHandler != null) {
									changeHandler.handleChange(changedSettings);
								}
							}
						}
					}
					key.reset();
					key = myWatcher.take();
				}
			} catch (InterruptedException e) {
				log.debug("Interupted", e);
			} catch (ClosedWatchServiceException e) {
				log.debug("Watcher closed");
			}
			log.debug("Stopping");
		}
	}
	
	/** 
	 * Watches for file changes, updates the settings and calls the change handler.
	 * @param fname the path to the file to watch
	 * @param handler the handler to call
	 * @param settings the settings to update
	 * @throws IOException if an I/O error occurs
	 */
	public static void watchForChanges(String fname, ChangeHandler handler,
			List<Setting<?>> settings) throws IOException {
		Path p = Paths.get(fname);
		if(p == null) {
			log.error("Path not found: " + fname);
			return;
		}
		// follow symbolic link to watch real directory
		while(Files.isSymbolicLink(p)) {
			p = p.getParent().resolve(Files.readSymbolicLink(p));
		}
		Path dir = p.toAbsolutePath().getParent();
		log.debug("Watching: " + dir.toString());
		WatchService myWatcher = dir.getFileSystem().newWatchService();
		MyWatchQueueReader fileWatcher = new MyWatchQueueReader(myWatcher, handler, dir,
				p.getFileName().toString(), settings);
		dir.register(myWatcher, StandardWatchEventKinds.ENTRY_MODIFY);
		Thread th = new Thread(fileWatcher, "FileWatcher");
		th.setDaemon(true);
		th.start();
	}
	
	/**
	 * Watches for file changes, updates the settings of the given class and calls the change handler.
	 * @param fname the path to the file to watch
	 * @param handler the handler to call
	 * @param sh the setting holder to load settings for
	 * @throws IOException if an I/O error occurs
	 */
	public static void watchForChanges(String fname, ChangeHandler handler,
			SettingHolder sh) throws IOException {
		watchForChanges(fname, handler, getSettings(sh));
	}

}
