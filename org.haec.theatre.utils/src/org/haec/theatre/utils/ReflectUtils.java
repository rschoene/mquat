/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class providing methods using reflection.
 * @author René Schöne
 */
public class ReflectUtils {
	
	/**
	 * Try to execute every Method starting with "get" and has no parameters.
	 * Returns a map with method names as keys and results (or exception duing invokation) as values.
	 * @param o the object to inspect
	 * @return a map with results
	 */
	public static Map<String, Object> gatherGetMethodResults(Object o) {
		Class<?> clazz = o.getClass();
		Map<String, Object> result = new HashMap<String, Object>();
		for(Method m : clazz.getMethods()) {
			String name = m.getName();
			if(!name.startsWith("get")) {
				continue;
			}
			Object invRes;
			try {
				invRes = m.invoke(o);
			} catch (IllegalArgumentException e) {
				invRes = e;
			} catch (IllegalAccessException e) {
				invRes = e;
			} catch (InvocationTargetException e) {
				invRes = e;
			}
			result.put(name, invRes);
		}
		return result;
	}

}
