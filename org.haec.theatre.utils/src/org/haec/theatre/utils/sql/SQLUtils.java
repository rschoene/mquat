/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.haec.theatre.utils.LogUtils;
import org.haec.theatre.utils.UtilsSettings;

/**
 * Utility class for SQL in general and Dexter in particular.
 * @author René Schöne
 */
public class SQLUtils {
	
	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	/** @return b ? 1 : 0 */
	public static int boolToInt(boolean b) {
		return b ? 1 : 0;
	}
	
	/** Invokes {@link #runSql(Statement, String, boolean, Logger) runSql(stmt, sql, true, &lt;internal_logger&gt;)} */
	public static ResultSet runSql(Statement stmt, String sql) throws SQLException {
		return runSql(stmt, sql, true, null);
	}

	/** Invokes {@link #runSql(Statement, String, boolean, Logger) runSql(stmt, sql, shouldLog, &lt;internal_logger&gt;)} */
	public static ResultSet runSql(Statement stmt, String sql, boolean shouldLog) throws SQLException {
		return runSql(stmt, sql, shouldLog, null);
	}

	/** Invokes {@link Statement#executeQuery(String) Statement.executeQuery(sql)} and
	 *  {@link LogUtils#logMessage(String, boolean, Logger) LogUtils.logMessage(sql, shouldLog, log)} */
	public static ResultSet runSql(Statement stmt, String sql, boolean shouldLog, Logger log) throws SQLException {
		return stmt.executeQuery(LogUtils.logMessage(sql, shouldLog, log));
	}

	private static class ConnectionAndCreator {
		Connection c;
		Callable<Connection> creator;
		String source;
		private Runnable afterCreation;
		public ConnectionAndCreator(Connection c, Callable<Connection> creator,
				String source, Runnable afterCreation) {
			this.c = c;
			this.creator = creator;
			this.source = source;
			this.afterCreation = afterCreation;
		}
		public void runAfterCreation() {
			if(this.afterCreation != null) {
				this.afterCreation.run();
			}
		}
	}
	private static List<ConnectionAndCreator> connections = new ArrayList<>();
	
	/**
	 * Register new connection for an unknown source.
	 * @param createNewConnection createNewConnection to create a new connection
	 * @return the connectionIndex
	 * @throws Exception if the connection could not be created
	 */
	public static int registerConnection(Callable<Connection> createNewConnection) throws Exception {
		return registerConnection(createNewConnection, "unknown", null);
	}

	/**
	 * Register new connection.
	 * @param createNewConnection createNewConnection to create a new connection
	 * @param source the requesting object
	 * @return the connectionIndex
	 * @throws Exception if the connection could not be created
	 */
	public static int registerConnection(Callable<Connection> createNewConnection,
			String source) throws Exception {
		return registerConnection(createNewConnection, source, null);
	}

	/**
	 * Register new connection.
	 * @param createNewConnection createNewConnection to create a new connection
	 * @param source the requesting object
	 * @return the connectionIndex
	 * @throws Exception if the connection could not be created
	 */
	public static int registerConnection(Callable<Connection> createNewConnection,
			String source, Runnable afterCreation) {
		Connection c = null;
		try {
			c = createNewConnection.call();
		} catch (Exception e) {
			log.warn("Could not create connection", e);
		}
		ConnectionAndCreator cac = new ConnectionAndCreator(c, createNewConnection,
				source, afterCreation);
		connections.add(cac);
		cac.runAfterCreation();
		return connections.size() - 1;
	}
	
	/** @return formerly registered connection */
	public static Connection getConnection(int index) {
		try {
			return connections.get(index).c;
		} catch (IndexOutOfBoundsException e) {
			log.error("No such connection (" + index + ")");
			return null;
		}
	}
	
	/** @return whether successful */
	public static boolean recreateAllConnections() {
		boolean success = true;
		for(ConnectionAndCreator cac : connections) {
			try {
				cac.c = cac.creator.call();
				cac.runAfterCreation();
				log.info("Recreated connection for " + cac.source + ".");
			} catch (Exception e) {
				success = false;
				log.warn("Error while recreating connection for " + cac.source, e);
			}
		}
		return success;
	}
	
	/** @return whether successful */
	public static boolean recreateConnection(int index) {
		ConnectionAndCreator cac = connections.get(index);
		try {
			cac.c = cac.creator.call();
			cac.runAfterCreation();
		} catch (Exception e) {
			log.warn("Error while recreating connection for " + cac.source, e);
			return false;
		}
		return true;
	}

	/** Creates a creator which does nothing. */
	public static Callable<Connection> creatorForDummy() {
		return new Callable<Connection>() {
			@Override
			public Connection call() throws Exception {
				return new DummyConnection();
			}
		};
	}
}
