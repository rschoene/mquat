/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.io.File;
import java.io.Serializable;
import java.net.URI;

import org.apache.log4j.Logger;

/**
 * A wrapper for {@link File} also specifying the host of this file.
 * Further, it is possible to have non-resolved proxies by not specifying the host of the file.
 * @author René Schöne
 */
public class FileProxy implements Serializable {
	
	private static final long serialVersionUID = -6542094242745633422L;
	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	private File file;
	private URI lemUri;
	private String name;
	private boolean resolved;
	
	/** Do not use this.
	 * Used only by r-osgi */
	public FileProxy() { }
	
	public FileProxy(String name) {
		this.name = name;
		this.resolved = false;
	}
	
	public FileProxy(File file) {
		this(file, null);
	}
	
	public FileProxy(File file, URI lemUri) {
		this.file = file;
		this.lemUri = lemUri;
		this.name = null;
		this.resolved = true;
	}

	public File getFile() {
		return file;
	}
	
	public void resolve() {
		if(this.isResolved()) {
			return;
		}
		//TODO get possible files, choose one, set file and lemUri
	}
	
	/**
	 * Either returns {@link #getFile()} if local, or get the file from {@link #getLemUri() the source}.
	 * Also changes this proxy to point to the returned local file, if copied.
	 * @param thisUri the local uri
	 * @return the local file
	 */
	public File getFileFor(URI thisUri) {
		if(lemUri == null || lemUri.equals(thisUri)) {
			// null is considered to be "local"
			log.debug(thisUri + " is local.");
			return file;
		}
		else {
			// file is on a different host
			// copy the file
			log.debug("File will be copied to " + thisUri + ".");
			File thisFile = FileUtils.getFrom(lemUri, file);
			// change file to be local
			file = thisFile;
			lemUri = null;
			return file;
		}
	}
	
	public URI getLemUri() {
		return lemUri;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isResolved() {
		return resolved;
	}
	
	@Override
	public String toString() {
		return "FileProxy_" + hashCode() + "(" + (isResolved() ? file + "@" + lemUri : "name:" + name) + ")" ;
	}

}
