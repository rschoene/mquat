/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

import org.apache.log4j.Logger;

/**
 * List holding processes as weak references, providing the ability to kill all running processes.
 * @author René Schöne
 */
public class WeakProcessList {

	private WeakHashMap<Process, Void> _items = new WeakHashMap<>();
	private static final Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);

	public void add(Process p) {
		_items.put(p, null);
	}

	public Map<Process, Integer> printExitValues(boolean waitForProcess) {
		Iterator<Process> iterator = _items.keySet().iterator();
		Map<Process, Integer> result = new HashMap<>();
		while (iterator.hasNext()) {
			Process process = iterator.next();
			if (process != null) {
				try {
					int exitValue = waitForProcess ? process.waitFor() : process.exitValue();
					log.debug("exit value ("+process+") = " + exitValue);
					result.put(process, exitValue);
					iterator.remove();
				} catch (IllegalThreadStateException | InterruptedException e) {
					/* process not finished yet, or interrupted */
				}
			}
		}
		return result;
	}
	
	public void killAllRunning() {
		Iterator<Process> iterator = _items.keySet().iterator();
		while (iterator.hasNext()) {
			Process process = iterator.next();
			if (process != null) {
				process.destroy();
				log.warn("destroying("+process+")");
				iterator.remove();
			}
		}
	}
	
	public int size() {
		return _items.size();
	}
}
