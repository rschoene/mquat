/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;


/**
 * A simple cache using two functions, one to generate a value from a source, and
 * another to check if two sources differ. To benefit from the cache, it should be much cheaper to compare
 * the two sources than to create a new value from a source.
 * @author René Schöne
 *
 * @param <S> the type of the source
 * @param <T> the type of the values to cache
 */
public class Cache<S, T> {

	/**
	 * A simple checker to check whether to objects are not equals.
	 * Currently implemented as:
	 * <pre>
	 * if(k1 == null) {
	 * 	return k2 != null;
	 * }
	 * return !k1.equals(k2);
	 * </pre>
	 * @author René Schöne
	 *
	 * @param <K>
	 */
	public static class NotEqualsChecker<K> implements Function2<K, K, Boolean> {
		
		@Override
		public Boolean apply(K k1, K k2) {
			if(k1 == null) {
				return k2 != null;
			}
			return !k1.equals(k2);
		}
		
	}
	
	private S oldSource;
	private T cachedValue;
	private final Function<S, T> function;
	private final Function2<S, S, Boolean> checker;
	private final boolean allowNullValue;
	
	/**
	 * Creates a new cache using the normal {@link S#equals(Object) equals()} as checker.
	 * @param createValue the function to create a new value
	 * @param allowNullValue whether <code>null</code> as values should be kept or discarded
	 * @see NotEqualsChecker
	 */
	public Cache(Function<S, T> createValue, boolean allowNullValue) {
		this(createValue, new NotEqualsChecker<S>(), allowNullValue);
	}

	
	/**
	 * Creates a new cache.
	 * The checker function should return true, if two sources are <b>different</b>, i.e. if the
	 * cache should be updated with the new source.
	 * The createValue function should parse the source and create a new value.
	 * @param createValue the function to create a new value
	 * @param checker the function to check if two sources are different
	 * @param allowNullValue whether <code>null</code> as values should be kept or discarded
	 */
	public Cache(Function<S, T> createValue, Function2<S, S, Boolean> checker, boolean allowNullValue) {
		this.oldSource = null;
		this.cachedValue = null;
		this.function = createValue;
		this.checker = checker;
		this.allowNullValue = allowNullValue;
	}
	
	/**
	 * Sets a new source. If it is different from the last set source, the cache is refreshed
	 * and the source is set as source to check against next time.
	 * @param newSource the new source to set.
	 * @return <code>true</code> if the cache was re-computed
	 */
	public boolean set(S newSource) {
		if(oldSource == null || checker.apply(oldSource, newSource)) {
			T newValue = function.apply(newSource);
			if(newValue != null || allowNullValue) {
				cachedValue = newValue;
				oldSource = newSource;
			}
			return true;
		}
		return false;
	}
	
	/**
	 * @return the cached value
	 */
	public T get() {
		return cachedValue;
	}
	
	/**
	 * Combines {@link #set(S)} and {@link #get()}.
	 * The return value of set is ignored.
	 * @param newSource the new source to set.
	 * @return the cached value
	 */
	public T get(S newSource) {
		set(newSource);
		return get();
	}
}
