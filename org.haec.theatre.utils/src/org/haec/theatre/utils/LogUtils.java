/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import org.apache.log4j.Logger;

/** Utility class with methods concerning logging in general.
 * @author René Schöne */
public class LogUtils {

	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	/** Invokes {@link #logMessage(String, boolean, Logger) logMessage(message, true, &lt;internal_logger&gt;)} */
	public static String logMessage(String message) {
		return logMessage(message, true, log);
	}

	/** Invokes {@link #logMessage(String, boolean, Logger) logMessage(message, shouldLog, &lt;internal_logger&gt;)} */
	public static String logMessage(String message, boolean shouldLog) {
		return logMessage(message, shouldLog, log);
	}

	/**
	 * If shouldLog, calls log.info(message)
	 * @param message the message to be potentially be logged
	 * @param shouldLog if <code>true</code> logs the message, otherwise ignores it
	 * @param log the logger to use, may be <code>null</code> to use internal logger
	 * @return message
	 */
	public static String logMessage(String message, boolean shouldLog, Logger log) {
		if(shouldLog) {
			if(log == null) { log = LogUtils.log; }
			log.info(message);
		}
		return message;
	}

}
