/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import org.coolsoftware.theatre.TheatreCommandProvider;

/**
 * An abstract command provider to be used for convenience.
 * @author René Schöne
 */
public abstract class AbstractTheatreCommandProvider implements TheatreCommandProvider {

	private boolean checkForCommandName;
	private final String commandName;

	public AbstractTheatreCommandProvider(String commandName) { this(commandName, true); }

	public AbstractTheatreCommandProvider(String commandName, boolean checkForCommandName) {
		this.checkForCommandName = checkForCommandName;
		this.commandName = commandName;
	}
	
	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.TheatreCommandProvider#execute(java.lang.String, java.lang.String[])
	 */
	@Override
	public String execute(String command, String... params) {
		String result;
		if(checkForCommandName && !getCommandName().equals(command)) { result = "bad command name"; }
		else { result = execute0(params); }
		return PlatformUtils.getHostName() + HOST_DELIMITER + result;
	}
	
	protected abstract String execute0(String[] params);
	
	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.TheatreCommandProvider#getCommandName()
	 */
	@Override
	public String getCommandName() {
		return commandName;
	}

}
