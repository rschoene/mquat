/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.haec.theatre.utils.settings.BooleanSetting;
import org.haec.theatre.utils.settings.IntegerSetting;
import org.haec.theatre.utils.settings.Setting;
import org.haec.theatre.utils.settings.SettingHolder;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;
import ch.ethz.iks.r_osgi.URI;

/**
 * Utility class providing methods to handle R-OSGI specific stuff
 * 
 * @author Sebastian Cech
 * @author Sebastian Götz
 * @author René Schöne
 */
public class RemoteOsgiUtil {
	
	private static final Logger log = Logger.getLogger("Theatre.util");
	
	/** Do use the local ip address (192.168.*). Otherwise external ip is used. (default: false)*/
	public static Setting<Boolean> ipLan = new BooleanSetting("theatre.ip.lan", false);
	/** Cache the first found, valid ip address. Otherwise it is retrieved for each call (default: true)*/
	public static Setting<Boolean> ipcache = new BooleanSetting("theatre.ip.cache", true);
//	public static final String ROSGI_PORT_PROPERTY_KEY = "ch.ethz.iks.r_osgi.port";
//	public static Setting<Integer> rosgiPort = new IntegerSetting(ROSGI_PORT_PROPERTY_KEY, 9278);
	public static Setting<Integer> thisPort = new IntegerSetting("this.port", 9278);
	
	static {
		// load settings from properties file
		List<Setting<?>> settings = new ArrayList<>();
		settings.add(ipcache);
		settings.add(ipLan);
		settings.add(thisPort);
		SettingHolder.loadTheatreProperties(settings);
	}
	
	public static final String ROSGI_Prefix = "r-osgi://";
	
	public static final String PORTSEPARATOR = ":";

	private static final Function<String, URI> createRemoteOsgiUriFunction =
			new Function<String, URI>() {
		
		@Override
		public URI apply(String s) {
			return createRemoteOsgiUri0(s);
		}
	};
	private static final MapCache<String, URI> rosgiUriCache = new MapCache<>(createRemoteOsgiUriFunction);
	
	public static boolean isWindows = System.getProperty("os.name").contains("Windows");
	
	private static final int external = 1;
	private static final int platformIndependentExternal = 2;
	private static final int localhost = 3;
	private static final int lem = 4;
	private static Function<Integer, String> getIp = new Function<Integer, String>() {
		
		@Override
		public String apply(Integer s) {
			switch(s) {
			case external:
				try (BufferedReader br = new BufferedReader(new InputStreamReader(
							new URL("http://agentgatech.appspot.com").openStream()))) {
					return br.readLine();
				} catch (Exception e) {
					log.debug("Could not get external ip", e);
					return "";
				}
			case platformIndependentExternal:
				String pieIp;
				if(isWindows) {
					// XXX: Not tested under Windows
					pieIp = getLocalhostAddress();
					if((pieIp.startsWith("192.168.") || pieIp.startsWith("169.")) && !ipLan.get()) {
						pieIp = RemoteOsgiUtil.getExternalIp();
					}
				} else { //so far, only UNIX considered in else
					pieIp = RemoteOsgiUtil.getExternalIp();
				}
				return pieIp;
			case localhost:
				String locIp;
				try {
					locIp = InetAddress.getByName(null).getHostAddress();
				} catch (UnknownHostException e) {
					log.warn("unknown host: localhost, trying 127.0.0.1");
					try {
						locIp = InetAddress.getByAddress(new byte[]{127,0,0,1}).getHostAddress();
					} catch (UnknownHostException e1) {
						log.warn("unknown host: 127.0.0.1, returning \"127.0.0.1\"");
						locIp = "127.0.0.1";
					}
				}
				return locIp;
			case lem:
				String lemIp = null;
				if(ipLan.get()) {
					// use the first LAN ip address.
					// Src: http://stackoverflow.com/a/2845292/2493208
					boolean found = false;
					try {
						for (final Enumeration< NetworkInterface > interfaces = 
								NetworkInterface.getNetworkInterfaces( );
								interfaces.hasMoreElements( ); ) {
							if(found) { break; }
							final NetworkInterface cur = interfaces.nextElement( );

							if ( cur.isLoopback( ) ) {
								continue;
							}

							log.debug( "interface " + cur.getName( ) );

							for ( final InterfaceAddress addr : cur.getInterfaceAddresses( ) ) {
								final InetAddress inet_addr = addr.getAddress( );

								if ( !( inet_addr instanceof Inet4Address ) ) {
									continue;
								}
								lemIp = inet_addr.getHostAddress();
								found = true;
								break;
							}
						}
					} catch (SocketException e) { log.debug("Could not read network interfaces", e); }
				} else {
					// use external ip address
					lemIp = getPlatformIndependentExternalIp();
				}
				return lemIp;
			default:
				log.warn("Invalid index: " + s);
				return null;
			}
		}
	};
	private static MapCache<Integer, String> cacheOfIps = new MapCache<>(getIp);
	
	public static String getCacheAsString() {
		StringBuilder sb = new StringBuilder();
		sb.append("cacheExternalIp:").append(cacheOfIps.get(external)).append("\n");
		sb.append("cachePlatformIndependentExternalIp:").append(cacheOfIps.get(platformIndependentExternal)).append("\n");
		sb.append("cacheLocalHotsAddress:").append(cacheOfIps.get(localhost)).append("\n");
		sb.append("cacheLemIp:").append(cacheOfIps.get(lem)).append("\n");
		return sb.toString();
	}
	
	
	/**
	 * Creates from a String representation of an URI a {@link URI}
	 * that can be used for using  r-OSGI based Services. If no port is given, the default port
	 * ({@link UtilsActivator#rosgiPort}) is used. A specified scheme will be replaced
	 * by the default r-osgi scheme {@value #ROSGI_Prefix}.
	 * @param uri A string representaion of the uri (e.g. server ip or server name)
	 * @return An {@link URI} that can be used for R-osgi based communication
	 */
	public static URI createRemoteOsgiUri(String uri){
		return rosgiUriCache.get(uri);
	}

	private static URI createRemoteOsgiUri0(String uri){
		return new URI(createRemoteOsgiUriString(uri));
	}
	
	/**
	 * Creates from a String representation of an URI a valid r-osgi URI as a string.
	 * If no port is given, the default port ({@link UtilsActivator#rosgiPort}) is used.
	 * A specified scheme will be replaced by the default r-osgi scheme {@value #ROSGI_Prefix}.
	 * @param uri A string representaion of the uri (e.g. server ip or server name)
	 * @return A String that can be used as identifier or input for {@link URI#URI(String)}.
	 */
	public static String createRemoteOsgiUriString(String uri) {
		if(uri.contains("://")) {
			uri = uri.substring(uri.indexOf("://")+3);
		}
		if(!uri.contains(":")) {
			// add default port
			return ROSGI_Prefix + uri + PORTSEPARATOR + thisPort.get();
		} // else
		return ROSGI_Prefix + uri;
	}
	
	private static Pattern rosgiUriPattern = Pattern.compile(ROSGI_Prefix+"(.*?)"+PORTSEPARATOR+".*?");
	/**
	 * Expects an URI of the form {@value #ROSGI_Prefix}VALUE{@value #PORTSEPARATOR}PORT.
	 * @param rOsgiUri an URI describing an r-osgi resource
	 * @return the VALUE part, mostly the IP part
	 */
	public static String stripRosgiParts(String rOsgiUri) {
		if(rOsgiUri.startsWith(ROSGI_Prefix)) {
			Matcher m = rosgiUriPattern.matcher(rOsgiUri);
			if(m.matches()) {
				rOsgiUri = m.group(1);
			}
		}
//		else {
//			log.debug("No match for " + rOsgiUri);
//		}
		return rOsgiUri;
	}
	
	/**
	 * Helper method to get the {@link RemoteOSGiService}.
	 * @param context the {@link BundleContext}
	 * @return <code>null</code> in case of errors or the {@link RemoteOSGiService}.
	 */
	public static RemoteOSGiService getRemoteOSGiService(BundleContext context){
		if(context == null) {
			log.warn("Error: context is null!");
			return null;
		}
		ServiceReference<?> remoteRef = context.getServiceReference(RemoteOSGiService.class.getName());
		if (remoteRef == null) {
			log.error("Error: R-OSGi not found!");
			return null;
		}
		RemoteOSGiService remote = (RemoteOSGiService) context.getService(remoteRef);
		return remote;
	}
	
	/**
	 * Search and return a remote service.
	 * Calls {@link #createRemoteOsgiUri(String)} to get a correct rosgi-uri.
	 * @param remote the {@link RemoteOSGiService}
	 * @param uri the uri to search at
	 * @param clazz the expected clazz
	 * @return the proxy object of the correct class, or <code>null</code> if not found
	 * @throws ClassCastException if {@link Class#cast(Object)} fails
	 */
	public static <S> S getRemoteService(RemoteOSGiService remote, String uri, Class<S> clazz) {
		RemoteServiceReference[] refs = getRemoteServiceReferences(remote, uri, clazz.getName());
		if(refs == null || refs.length == 0) {
			return null;
		}
		if(refs.length > 1) {
			log.debug("Got multiple instances for " + clazz + " at " + uri);
		}
		return clazz.cast(remote.getRemoteService(refs[0]));
	}
	
	/**
	 * Search and return all remote services.
	 * Does not throw ClassCastException, if cast fails, instead the entries of the array are <code>null</code>.
	 * @param remote the {@link RemoteOSGiService}
	 * @param uri the uri to search at
	 * @param clazz the expected clazz
	 * @return an array of proxy objects of the correct class, or <code>null</code> if none found
	 */
	public static <S> S[] getRemoteServices(RemoteOSGiService remote, String uri, Class<S> clazz) {
		RemoteServiceReference[] refs = getRemoteServiceReferences(remote, uri, clazz.getName());
		if(refs == null || refs.length == 0) {
			return null;
		}
		@SuppressWarnings("unchecked")
		S[] result = (S[]) Array.newInstance(clazz, refs.length);
		for (int i = 0; i < refs.length; i++) {
			try {
				result[i] = clazz.cast(remote.getRemoteService(refs[i]));
			} catch (Exception e) {
				result[i] = null;
			}
		}
		return result;
	}

	/**
	 * Helper method to provide R-OSGi {@link ServiceReference}s.
	 * Calls {@link #createRemoteOsgiUri(String)} to get the URI.
	 * @param remote The {@link RemoteOSGiService}
	 * @param uri The uri of the target OSGi container
	 * @param clazz The name of the service interface
	 * @return An array of remoteServicereferences or <code>null</code>
	 */
	public static RemoteServiceReference[] getRemoteServiceReferences(RemoteOSGiService remote, String uri, String clazz) {
		return getRemoteServiceReferences(remote, uri, clazz, null);
	}

	/**
	 * Helper method to provide R-OSGi {@link ServiceReference}s.
	 * Calls {@link #createRemoteOsgiUri(String)} to get the URI.
	 * @param remote The {@link RemoteOSGiService}
	 * @param uri The uri of the target OSGi container
	 * @param clazz The name of the service interface
	 * @param filter filter to use, may be <code>null</code>
	 * @return An array of remoteServicereferences or <code>null</code>
	 */
	public static RemoteServiceReference[] getRemoteServiceReferences(RemoteOSGiService remote, String uri, String clazz, Filter filter) {
		if(remote!=null){
			RemoteServiceReference [] refs = remote.getRemoteServiceReferences(RemoteOsgiUtil.createRemoteOsgiUri(uri), clazz, filter);
			return refs;
		}
		return null;
	}
	
	public static String getExternalIp() {
		return cacheOfIps.get(external);
	}

	protected static void printCallers(int size) {
		if(size < 1)
			return;
		StackTraceElement[] ret = new StackTraceElement[size];
		// do not print element for this (printCallers) method
		System.arraycopy(new Exception().getStackTrace(), 1, ret, 0, size);
		for (int i = 1; i < ret.length; i++) {
			log.debug("\t- " + ret[i]);
		}
	}

	public static String getPlatformIndependentExternalIp() {
		return cacheOfIps.get(platformIndependentExternal);
	}
	
	public static String getLocalhostAddress() {
		return cacheOfIps.get(localhost);
	}

	public static String getLemIp() {
		return cacheOfIps.get(lem);
	}
	
	public static String serviceRefToString(RemoteServiceReference r) {
		StringBuilder sb = new StringBuilder(r.toString());
//		sb.append(Arrays.toString(r.getServiceInterfaces()));
		for(String key : r.getPropertyKeys()) {
			Object val = r.getProperty(key);
			sb.append(" " + key + ":" + (key.equals(Constants.OBJECTCLASS) ? Arrays.toString((String[]) val) : val));
		}
		return sb.toString();
	}
	
	/**
	 * Registeres an osgi service remotely.
	 * @param context the context to use for registration
	 * @param clazz the class with which the instance should registered
	 * @param instance the instance to register
	 * @param <S> The type of the instance
	 * @return the registration object
	 */
	public static <S> ServiceRegistration<S> register(BundleContext context, Class<S> clazz, S instance) {
		return register(context, clazz, instance, new Hashtable<String, Object>());
	}
	
	/**
	 * Registeres an osgi service remotely.
	 * @param context the context to use for registration
	 * @param clazz the class with which the instance should registered
	 * @param instance the instance to register
	 * @param properties the initial properties. Will be extended by the key-value pair
	 *  ({@link RemoteOSGiService#R_OSGi_REGISTRATION} : {@link Boolean#TRUE}).
	 * @param <S> The type of the instance
	 * @return the registration object
	 */
	public static <S> ServiceRegistration<S> register(BundleContext context, Class<S> clazz,
			S instance, Dictionary<String, Object> properties) {
		properties.put(RemoteOSGiService.R_OSGi_REGISTRATION, Boolean.TRUE);
		return context.registerService(clazz, instance, properties);
	}
	
	/**
	 * Registeres an osgi service remotely.
	 * @param context the context to use for registration
	 * @param instance the instance to register
	 * @param clazzes the classes with which the instance should registered
	 * @return the registration object
	 */
	public static ServiceRegistration<?> register(BundleContext context, Object instance, Class<?>... clazzes) {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put(RemoteOSGiService.R_OSGi_REGISTRATION, Boolean.TRUE);
		String[] classes = new String[clazzes.length];
		for (int i = 0; i < classes.length; i++) {
			classes[i] = clazzes[i].getName();
		}
		return context.registerService(classes, instance, properties);
	}

}
