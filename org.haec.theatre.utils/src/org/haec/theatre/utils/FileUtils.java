/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.ecore.resource.URIConverter;

/** Utility class with methods concerning Files (reading, writing, copying) and paths.
 * @author René Schöne */
public class FileUtils {

	public static final String TEMP_DIR = "/tmp";

	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	/** Seconds to wait before aborting an command. Type: Integer. Default: 0.
	 * Supported by: {@link #copyFile(URI, File, URI, Map) copyFile}.
	 * Use <code>0</code> to wait forever. */
	public static final String OPTION_TIMEOUT = "option.timeout";
	
	/** Number of times to repeat a failed command before giving up. Type: Integer. Default: 1.
	 * Supported by: {@link #copyFile(URI, File, URI, Map) copyFile}.
	 * Use <code>0</code> to only try once. */
	public static final String OPTION_TRIES = "option.tries";
	
	/** Port to use for scp. Type: Integer. Default: 22.
	 * Supported by: {@link #copyFile(URI, File, URI, Map) copyFile}. */
	public static final String OPTION_PORT = "option.src.port";
	
	/** Username to be used with scp. Type: String. Default: <i>local user</i>.
	 * Supported by: {@link #copyFile(URI, File, URI, Map) copyFile}. */
	public static final String OPTION_USER = "option.src.user";

	/** Port to use for ssh. Type: Integer. Default: 22.
	 * Supported by: {@link #copyFile(URI, File, URI, Map) copyFile}. */
	public static final String OPTION_TARGET_PORT = "option.target.port";
	
	/** Username to be used with ssh. Type: String. Default: <i>local user</i>.
	 * Supported by: {@link #copyFile(URI, File, URI, Map) copyFile}. */
	public static final String OPTION_TARGET_USER = "option.target.user";

	/**
	 * @return the directory of the current workspace; or current directory, if the workspace is not accessible
	 */
	public static String getWorkspaceDir() {
		String folder;
		try {
			IWorkspace workspace = ResourcesPlugin.getWorkspace(); 
			folder = workspace.getRoot().getLocation().toFile().getPath().toString();
		}
		catch(Exception e) {
			// fall back to current work dir
			folder = new File(".").getAbsolutePath();
		}
		if(!folder.endsWith("/")) folder = folder + "/";
		return folder;
	}

	/**
	 * Gets the path to a file in the workspace. Does not ensure that the file exists.
	 * @param filename the name of the file
	 * @return the path to the file within the workspace
	 */
	public static String getWorkspaceFile(String filename) {
		return join(getWorkspaceDir(), filename);
	}
	
	/**
	 * Joins the given paths to one path by appending them.
	 * If no path is given, an empty string will be returned.
	 * If only one path is given, this path will be returned.
	 * If exactly two paths are given, they are joined together.
	 * If more than two paths are given, they are joined together recursively.
	 * @param paths the paths to join
	 * @return the joined path
	 */
	public static String join(String... paths) {
		if(paths.length == 0) {
			return "";
		}
		if(paths.length == 1) {
			return paths[0];
		}
		if(paths.length == 2) {
			// just quickly join them
			return new File(paths[0], paths[1]).toString();
		}
		List<String> todo = new LinkedList<String>();
		for(String path : paths) {
			todo.add(path);
		}
		File current = new File(todo.remove(0));
		while(!todo.isEmpty()) {
			current = new File(current, todo.remove(0));
		}
		return current.toString();
	}

	/**
	 * Copies the the file at source to the target.
	 * The returned file will be relative to the target host.
	 * @param srcUri the URI of the source container
	 * @param srcFile the path of the file to copy, on the source host
	 * @param targetUri the URI of the target container, i.e. the destination
	 * @return a file object relative to the target container
	 */
	public static File copyFile(URI srcUri, File srcFile, URI targetUri) {
		return copyFile(srcUri, srcFile, targetUri, null);
	}
	
	/**
	 * Copies the the file at source to the target.
	 * The returned file will be relative to the target host.
	 * @param srcUri the URI of the source container
	 * @param srcFile the path of the file to copy, on the source host
	 * @param targetUri the URI of the target container, i.e. the destination
	 * @param options further options, for keys see Constants in {@link FileUtils}
	 * @return a file object relative to the target container
	 */
	public static File copyFile(URI srcUri, File srcFile, URI targetUri, Map<String, Object> options) {
		return copyFile(ipOf(srcUri), srcFile, ipOf(targetUri), options);
	}

	/**
	 * @return the host of the given uri
	 */
	private static String ipOf(URI uri) {
		String host = uri.getHost();
		return host != null ? host : uri.getPath();
	}
	
	private static final String p_scp = "scp";
	private static final String p_ssh_portFlag = "-p";
	private static final String p_scp_portFlag = "-P";
	private static final String p_timeoutFlag = "-oConnectTimeout";
	/**
	 * Copies a file from one host to another host.
	 * Options include timeout and tries to copy.
	 * @param srcIp the IP of the host storing the file
	 * @param srcFile the path of the file to copy, on the source host
	 * @param targetIp the IP of the destination host where the file shall be copied to
	 * @param options further options, for keys see Constants in {@link FileUtils}
	 * @return a file object relative to the target host
	 */
	public static File copyFile(String srcIp, File srcFile, String targetIp, Map<String, Object> options) {
//		log.warn("Actually, the file '" + srcFile + "' should have been copied from " + srcIp + " to " + targetIp);
		if(srcIp == null || srcIp.equals(targetIp)) {
			log.info("Source IP is null, assume local file");
			return srcFile;
		}
		// ssh to srcUri, issue scp with srcFile to targetUri
		if(options == null) {
			options = Collections.emptyMap();
		}
		File dir;
		if(!(dir = new File(TEMP_DIR)).exists()) {
			log.info("Creating " + dir.getAbsolutePath());
			if(!dir.mkdirs()) {
				log.error("Could not create " + dir.getAbsolutePath() + ". Return null.");
				return null;
			}
		}
		int triesLeft = CollectionsUtil.get(options, OPTION_TRIES, 1) - 1;
		int timeout = CollectionsUtil.get(options, OPTION_TIMEOUT, -1);
		int port = CollectionsUtil.get(options, OPTION_PORT, 22);
		String targetFileName = TEMP_DIR + File.separator + System.currentTimeMillis() + "." + splitExt(srcFile).extension;
		String userPart = userPart((String) options.get(OPTION_USER));
		String source = userPart + srcIp + ":" + srcFile;
		List<String> cmds = new ArrayList<String>();
		if(targetIp == null) {
			cmds.addAll(Arrays.asList(p_scp, p_scp_portFlag, Integer.toString(port)));
			if(timeout > 0) {
				cmds.add(p_timeoutFlag+"="+Integer.toString(timeout));
			}
			cmds.add(source);
			cmds.add(targetFileName);
		}
		else {
			String targetUserPart = userPart((String) options.get(OPTION_TARGET_USER));
			int targetPort = CollectionsUtil.get(options, OPTION_TARGET_PORT, 22);
			cmds.add("ssh");
			if(timeout > 0) {
				cmds.add(p_timeoutFlag+"="+Integer.toString(timeout));
			}
			cmds.add(p_ssh_portFlag);
			cmds.add(Integer.toString(targetPort));
			cmds.add(targetUserPart+targetIp);
			cmds.add(StringUtils.join(" ", p_scp, timeout > 0 ? p_timeoutFlag+"="+Integer.toString(timeout) : "",
					p_scp_portFlag, port, source, targetFileName));
		}
		log.debug("Issuing the cmd '" + cmds + "'");
		log.debug("> " + StringUtils.joinIterable(" ", new ArrayList<Object>(cmds)));
		boolean failed = false;
		do {
			ProcessBuilder pb = new ProcessBuilder(cmds);
			try {
				Process p = pb.start();
				handleStream(p.getInputStream(), LOG_DEBUG_STDOUT);
				handleStream(p.getErrorStream(), LOG_WARN_STDERR);
				p.waitFor();
			} catch (IOException e) {
				log.warn("I/O Error during copying", e);
				failed = true;
			} catch (InterruptedException e) {
				log.warn("Copying interupted (timeout was set to " + timeout + ")", e);
				failed = true;
			}
		} while(failed  && triesLeft-- > 0);
		if(failed) {
			log.error("Failed to copy the file. Giving up.");
		}
		return new File(targetFileName);
	}
	
	private interface StreamLineHandler { void handleLine(String line); }
	protected static StreamLineHandler LOG_DEBUG_STDOUT = new StreamLineHandler() {
		@Override
		public void handleLine(String line) {
			log.debug(line);
		}
	};
	protected static StreamLineHandler LOG_WARN_STDERR = new StreamLineHandler() {
		@Override
		public void handleLine(String line) {
			log.warn(line);
		}
	};
	protected static StreamLineHandler DEV_NULL = new StreamLineHandler() {
		@Override
		public void handleLine(String line) {
		}
	};

	private static void handleStream(InputStream is, StreamLineHandler handler) {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			while((line = br.readLine()) != null)
				handler.handleLine(line);
		} catch (IOException ignore) { }
		if(br != null) try { br.close(); } catch(IOException ignore) { };
	}
	
	private static String userPart(String user) {
		return user == null ? "" : user + "@";
	}

	/**
	 * Get a file from a remote machine.
	 * @param srcUri the uri of the hosting machine.
	 * @param srcFile the file, relative to the hosting machine
	 * @return a file stored in {@value #TEMP_DIR}.
	 */
	public static File getFrom(URI srcUri, File srcFile) {
		return copyFile(ipOf(srcUri), srcFile, null, null);
	}
	
	/**
	 * Get a file from a remote machine.
	 * @param srcUri the uri of the hosting machine.
	 * @param srcFile the file, relative to the hosting machine
	 * @return a file stored in {@value #TEMP_DIR}.
	 */
	public static File getFrom(String srcUri, File srcFile) {
		return copyFile(srcUri, srcFile, null, null);
	}
	
	/**
	 * Get a file from a remote machine.
	 * @param srcUri the uri of the hosting machine.
	 * @param srcFile the file, relative to the hosting machine
	 * @param options further options, for keys see Constants in {@link FileUtils}
	 * @return a file stored in {@value #TEMP_DIR}.
	 */
	public static File getFrom(String srcUri, File srcFile, Map<String, Object> options) {
		return copyFile(srcUri, srcFile, null, options);
	}

	public static String readContractAsText(org.eclipse.emf.common.util.URI fileURI) {
		StringBuilder ret = new StringBuilder();
		try {
			InputStream is = URIConverter.INSTANCE.createInputStream(fileURI);
			return readAsString(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret.toString();
	}

	/**
	 * Reads from the file given by its path and returns the result as a string.
	 * Does not preserve line breaks.
	 * @param fileName the path to the file
	 * @param includeLinebreaks whether to include read line breaks
	 * @return the result as a string
	 * @throws IOException if the could not be found or opened
	 * @see #readAsString(String, boolean)
	 */
	public static String readAsString(String fileName) throws IOException {
		return readAsString(fileName, false);
	}

	/**
	 * Reads from the file given by its path and returns the result as a string.
	 * @param fileName the path to the file
	 * @param includeLinebreaks whether to include read line breaks
	 * @return the result as a string
	 * @throws IOException if the could not be found or opened
	 */
	public static String readAsString(String fileName, boolean includeLinebreaks) throws IOException {
		try(FileReader fr = new FileReader(fileName)) {
			return readAsString0(fr, includeLinebreaks);
		}
	}
	
	/**
	 * Reads from the given inputstream and returns the result as a string.
	 * Does not preserve line breaks.
	 * The stream will be closed afterwards.
	 * @param is the given inputstream
	 * @return the result as a string
	 * @throws IOException if the stream could not be opened or closed
	 * @see #readAsString(InputStream, boolean)
	 */
	public static String readAsString(InputStream is) throws IOException {
		return readAsString(is, false);
	}
	
	/**
	 * Reads from the given inputstream and returns the result as a string.
	 * The stream will be closed afterwards.
	 * @param is the given inputstream
	 * @param includeLinebreaks whether to include read line breaks
	 * @return the result as a string
	 * @throws IOException if the stream could not be opened or closed
	 */
	public static String readAsString(InputStream is, boolean includeLinebreaks) throws IOException {
		try(InputStreamReader isr = new InputStreamReader(is)) {
			return readAsString0(isr, includeLinebreaks);
		}
	}
	
	private static String readAsString0(Reader r, boolean includeLinebreaks) {
		StringBuilder sb = new StringBuilder();
		try(BufferedReader br = new BufferedReader(r)) {
			String line;
			while((line = br.readLine()) != null) {
				sb.append(line);
				if(includeLinebreaks) {
					sb.append("\n");
				}
			}
		} catch (IOException ignore) {
//			System.out.println();
		}
		return sb.toString();
	}
	
	/**
	 * Write the given values to the file with the given name.
	 * @param fileName the path to the file
	 * @param values the values to write (for each toString() is called)
	 */
	public static void writeToCsv(String fileName, Object... values) {
		try(FileWriter fw = new FileWriter(fileName, true)) {
			writeToCsv(fw, values);
		} catch (IOException e) {
			log.warn("Could not write to " + fileName, e);
		}
	}

	/**
	 * Write the given values using the given writer.
	 * @param w the writer to use
	 * @param values the values to write (for each toString() is called)
	 */
	public static void writeToCsv(Writer w, Object... values)
			throws IOException {
		w.append(StringUtils.join(",", values)).append('\n');
	}
	
	/**
	 * @return the current time in the format <code>YYYY-MM-dd_HH-mm-ss</code>.
	 * @see SimpleDateFormat
	 */
	public static String getFileNameTimestamp() {
		return new SimpleDateFormat("YYYY-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
	}
	
	public static class FileName {
		public String basename;
		public String extension;
		public FileName() {
			this("", "");
		}
		public FileName(String basename, String extension) {
			this.basename = basename;
			this.extension = extension;
		}
	}
	
	/**
	 * @return Splits the given file into basename and extension. The extension will be empty, if not valid.
	 */
	public static FileName splitExt(File f) {
		return splitExt(f.getName());
	}
	
	/**
	 * @return Splits the given filename into basename and extension. The extension will be empty, if not valid.
	 */
	public static FileName splitExt(String fileName) {
		FileName result = new FileName();

		int i = fileName.lastIndexOf('.');
		int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));

		if (i > p) {
			result.basename  = fileName.substring(p+1, i);
		    result.extension = fileName.substring(i+1);
		}
		else {
			result.basename = fileName.substring(p+1);
		}
		return result;
	}

	/**
	 * Ensure, that the given directories exist. If not existing, attempts to create them.
	 * @param createParentDirs <code>true</code> to also create parent directory each time
	 * @param dirs the directories to create
	 * @return if all directories could be created
	 */
	public static boolean ensureDirectories(boolean createParentDirs, String... dirs) {
		boolean overallSuccess = true;
		for(String dir : dirs) {
			File f = new File(dir);
			if(f.exists() && f.isDirectory()) { continue; }
			overallSuccess |= createParentDirs ? f.mkdirs() : f.mkdir();
		}
		return overallSuccess;
	}
	
	/**
	 * Ensures the existence of a file.
	 * @param pathname the path to the file
	 * @param createParentDir <code>true</code> to create the parent directory
	 * @param createAllParentDirs <code>true</code> to create all parent directories
	 * @return the pathname
	 */
	public static String ensureFileExisting(String pathname, boolean createParentDir, boolean createAllParentDirs) throws IOException {
		File f = new File(pathname);
		if(!f.exists()) {
			File parentDir = f.getParentFile();
			if(!parentDir.exists() && createParentDir) {
				boolean createdDir = createAllParentDirs ? parentDir.mkdirs() : parentDir.mkdir();
				if(!createdDir) { log.warn("Directory " + parentDir.getAbsolutePath() + " was not created."); }
			}
			f.createNewFile();
		}
		return pathname;

	}

}
