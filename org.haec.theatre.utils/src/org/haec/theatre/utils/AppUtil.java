/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

/**
 * THEATRE-wide functionality used by both, THEATRE and components.
 * @author René Schöne
 */
public class AppUtil {

	/**
	 * @return gemIp + ":" + masterPort
	 */
	public static String getGEMUri() {
		return UtilsActivator.gemIp.get() + ":" + UtilsActivator.masterPort.get();
	}

	/**
	 * @return grmIp + ":" + masterPort
	 */
	public static String getGRMUri() {
		return UtilsActivator.grmIp.get() + ":" + UtilsActivator.masterPort.get();
	}

}
