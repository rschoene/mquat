/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author René Schöne
 */
public class MapCache<S, T> implements Map<S, T> {
	
	private Map<S,T> map = new HashMap<>();
	private Function<S, T> function;

	/**
	 * @param createValue
	 */
	public MapCache(Function<S, T> createValue) {
		this.function = createValue;
	}


	/* (non-Javadoc)
	 * @see java.util.Map#size()
	 */
	@Override
	public int size() {
		return map.size();
	}


	/* (non-Javadoc)
	 * @see java.util.Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}


	/* (non-Javadoc)
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		// keys will be generated on-demand
		return true;
	}


	/* (non-Javadoc)
	 * @see java.util.Map#containsValue(java.lang.Object)
	 */
	@Override
	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}


	/* (non-Javadoc)
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@Override
	public T get(Object key) {
		T value = map.get(key);
		if(value == null) {
			@SuppressWarnings("unchecked")
			S s = (S) key;
			value = function.apply(s);
			put(s, value);
		}
		return value;
	}


	/* (non-Javadoc)
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public T put(S key, T value) {
		return map.put(key, value);
	}


	/* (non-Javadoc)
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@Override
	public T remove(Object key) {
		return map.remove(key);
	}


	/* (non-Javadoc)
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map<? extends S, ? extends T> m) {
		map.putAll(m);
	}


	/* (non-Javadoc)
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {
		map.clear();
	}


	/* (non-Javadoc)
	 * @see java.util.Map#keySet()
	 */
	@Override
	public Set<S> keySet() {
		return map.keySet();
	}


	/* (non-Javadoc)
	 * @see java.util.Map#values()
	 */
	@Override
	public Collection<T> values() {
		return map.values();
	}


	/* (non-Javadoc)
	 * @see java.util.Map#entrySet()
	 */
	@Override
	public Set<java.util.Map.Entry<S, T>> entrySet() {
		return map.entrySet();
	}

}
