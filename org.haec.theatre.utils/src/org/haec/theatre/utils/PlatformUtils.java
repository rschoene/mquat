/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

import org.apache.log4j.Logger;

/**
 * Utility class providing methods to get the OS type, resolve hostnames and watch for file changes.
 * @author René Schöne
 */
public class PlatformUtils {
	
	private static class HostMapping {
		final Map<String, String> ip2Host;
		final Map<String, String> host2Ip;
		public HostMapping(String hostsContent) {
			ip2Host = new HashMap<>();
			host2Ip = new HashMap<>();
			for(String line : StringUtils.tokenize(hostsContent, "\n")) {
				String[] parts = line.split("\\s+");
				String ip = parts[0];
				String hostname = parts[1];
				host2Ip.put(hostname, ip);
				ip2Host.put(ip, hostname);
			}
			log.debug("Initialize: " + ip2Host);
		}
		public String getIpOfHost(String host) {
			return host2Ip.get(host);
		}
		public String getHostOfIp(String ip) {
			return ip2Host.get(ip);
		}
	}

	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static HostMapping hosts;
	private static final boolean OS_IS_SOLARIS = OS.indexOf("sunos") >= 0;
	private static final boolean OS_IS_UNIX = OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0;
	private static final boolean OS_IS_MAC = OS.indexOf("mac") >= 0;
	private static final boolean OS_IS_WINDOWS = OS.indexOf("win") >= 0;

	public static boolean isWindows() {
		return OS_IS_WINDOWS;
	}

	public static boolean isMac() {
		return OS_IS_MAC;
	}

	public static boolean isUnix() {
		return OS_IS_UNIX;
	}

	public static boolean isSolaris() {
		return OS_IS_SOLARIS;
	}
	
	/**
	 * @return the hostname of the current system
	 */
	public static String getHostName() {
		if(isUnix()) {
			InputStream is;
			try {
				is = Runtime.getRuntime().exec("hostname").getInputStream();
			} catch (IOException e) {
				return null;
			}
			try(java.util.Scanner s = new java.util.Scanner(is)) {
				return s.hasNextLine() ? s.nextLine() : "";
			}
		}
		return null;
	}
	
	/**
	 * Based on hosts-file, map IP to hostname.
	 * @param ip the given IP to map
	 * @return the hostname to the IP, or <code>null</code> on any failure
	 * @see #getIpOf(String)
	 */
	public static String getHostNameOf(String ip) {
		String result = getHostMapping().getHostOfIp(ip);
		if(result == null) {
			log.debug("Got null for ip " + ip);
		}
		return result;
	}
	
	/**
	 * Based on hosts-file, map hostname to ip.
	 * @param hostname the given hostname to map
	 * @return the IP to the hostname, or <code>null</code> on any failure
	 * @see #getHostNameOf(String)
	 */
	public static String getIpOf(String hostname) {
		String result = getHostMapping().getIpOfHost(hostname);
		if(result == null) {
			log.debug("Got null for hostname " + hostname);
		}
		return result;
	}

	private static HostMapping getHostMapping() {
		if(hosts == null) {
			String hostsContent = null;
			if(isUnix()) {
				try(FileInputStream fis = new FileInputStream("/etc/hosts")) {
					hostsContent = FileUtils.readAsString(fis, true);
				} catch (IOException e) {
					log.warn("Could not open /etc/hosts", e);
				}
			}
			// other systems should also be supported somewhen
			if(hostsContent == null) {
				log.error("Content of hosts is empty. Exiting.");
			}
			hosts = new HostMapping(hostsContent);
		}
		return hosts;
	}
	
	public static abstract class ProcessChangeHandler {
		/** Hook. Called if new process with the name is started */
		protected abstract void processStarted(String hostname, String processName);
		/** Hook. Called if former process with the name has not been found. */
		protected abstract void processEnded(String hostname, String processName);
		/** Hook. Called everytime, before any action is taken.
		 * Useful for error handling, e.g. if processStarted failed.
		 * Empty by default. */
		protected void beforeRun(String hostname, String processName) { };
	}

	public static TimerTask watchForProcessChange(final String processName,
			final ProcessChangeHandler changeHandler) {
		return watchForProcessChange(processName, null, changeHandler);
	}

	public static TimerTask watchForProcessChange(final String processName, final String hostname,
			final ProcessChangeHandler changeHandler) {
		TimerTask task = new TimerTask() {
			final int STARTED = 1;
			final int ENDED = 2;
			int lastStatus = ENDED;
			long lastPid = -1;
			
			@Override
			public void run() {
				changeHandler.beforeRun(hostname, processName);
//				log.debug("Checking for " + processName);
				long pid = getProcessPid(hostname, processName);
				if(pid == NO_SUCH_PROCESS && lastStatus == STARTED) {
					lastStatus = ENDED;
					changeHandler.processEnded(hostname, processName);
					return;
				}
				if(lastPid != pid && lastStatus == ENDED) {
					// changed process
					lastStatus = STARTED;
					lastPid = pid;
					changeHandler.processStarted(hostname, processName);
				}
				// else: Status quo
			}
		};
		return task;
	}
	
	/** No process found. */
	public static final long NO_SUCH_PROCESS = -1;

	/**
	 * @param processName the name of the process
	 * @return the pid of the (first) process with the given name, or {@link #NO_SUCH_PROCESS} if there is none
	 */
	public static long getProcessPid(String processName) {
		return getProcessPid(null, processName);
	}

	/**
	 * @param hostname the host to check at
	 * @param processName the name of the process
	 * @return the pid of the (first) process with the given name, or {@link #NO_SUCH_PROCESS} if there is none
	 */
	public static long getProcessPid(String hostname, String processName) {
		InputStream is;
		String content;
		long pid;
		if(isUnix()) {
			String[] cmdarray;
			if(hostname == null) {
				// run local
				cmdarray = new String[]{"pidof", processName};
			}
			else {
				// run remotely
				cmdarray = new String[]{"ssh", hostname, "/usr/sbin/pidof " + processName};
			}
			try {
				is = Runtime.getRuntime().exec(cmdarray).getInputStream();
			} catch (IOException e) {
				log.debug("Problems with pidof", e);
				return NO_SUCH_PROCESS;
			}
		}
		else { throw new UnsupportedOperationException("Only supported in unix systems."); }
		try { content = FileUtils.readAsString(is, false);
		} catch (IOException e) {
			log.debug("Problems with reading from pidof", e);
			return NO_SUCH_PROCESS;
		}
//		try(java.util.Scanner s = new java.util.Scanner(is)) {
//			content = s.hasNextLine() ? s.nextLine() : EMPTY;
//		}
		if(content.isEmpty()) { return NO_SUCH_PROCESS; }
		try { pid = Long.parseLong(content.trim());
		} catch (NumberFormatException ignore) {
			return NO_SUCH_PROCESS;
		}
		return pid;
	}

}
