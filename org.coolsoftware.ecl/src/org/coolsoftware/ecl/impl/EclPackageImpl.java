/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.impl;

import org.coolsoftware.coolcomponents.ccm.CcmPackage;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;
import org.coolsoftware.coolcomponents.expressions.ExpressionsPackage;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.coolsoftware.coolcomponents.types.DatatypesPackage;
import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.EclImport;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.HWComponentContract;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.HWContractClause;
import org.coolsoftware.ecl.HWContractMode;
import org.coolsoftware.ecl.Import;
import org.coolsoftware.ecl.Metaparameter;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.ecl.SelfRequirementClause;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EclPackageImpl extends EPackageImpl implements EclPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eclImportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmImportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swComponentContractEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eclFileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contractModeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass provisionClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swContractClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eclContractEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swComponentRequirementClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyRequirementClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hwComponentContractEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hwContractModeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swContractModeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hwContractClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hwComponentRequirementClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass selfRequirementClauseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass formulaTemplateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metaparameterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.coolsoftware.ecl.EclPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EclPackageImpl() {
		super(eNS_URI, EclFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link EclPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EclPackage init() {
		if (isInited) return (EclPackage)EPackage.Registry.INSTANCE.getEPackage(EclPackage.eNS_URI);

		// Obtain or create and register package
		EclPackageImpl theEclPackage = (EclPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof EclPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new EclPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CcmPackage.eINSTANCE.eClass();
		NameablePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theEclPackage.createPackageContents();

		// Initialize created meta-data
		theEclPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEclPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EclPackage.eNS_URI, theEclPackage);
		return theEclPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImport() {
		return importEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEclImport() {
		return eclImportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmImport() {
		return ccmImportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCcmImport_CcmStructuralModel() {
		return (EReference)ccmImportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSWComponentContract() {
		return swComponentContractEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSWComponentContract_Modes() {
		return (EReference)swComponentContractEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSWComponentContract_ComponentType() {
		return (EReference)swComponentContractEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSWComponentContract_Port() {
		return (EReference)swComponentContractEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEclFile() {
		return eclFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEclFile_Contracts() {
		return (EReference)eclFileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEclFile_Imports() {
		return (EReference)eclFileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContractMode() {
		return contractModeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getContractMode_Name() {
		return (EAttribute)contractModeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvisionClause() {
		return provisionClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvisionClause_ProvidedProperty() {
		return (EReference)provisionClauseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvisionClause_MinValue() {
		return (EReference)provisionClauseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvisionClause_MaxValue() {
		return (EReference)provisionClauseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvisionClause_Formula() {
		return (EReference)provisionClauseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSWContractClause() {
		return swContractClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEclContract() {
		return eclContractEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEclContract_Name() {
		return (EAttribute)eclContractEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEclContract_Metaparams() {
		return (EReference)eclContractEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSWComponentRequirementClause() {
		return swComponentRequirementClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSWComponentRequirementClause_RequiredComponentType() {
		return (EReference)swComponentRequirementClauseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSWComponentRequirementClause_RequiredProperties() {
		return (EReference)swComponentRequirementClauseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPropertyRequirementClause() {
		return propertyRequirementClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyRequirementClause_RequiredProperty() {
		return (EReference)propertyRequirementClauseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyRequirementClause_MaxValue() {
		return (EReference)propertyRequirementClauseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyRequirementClause_MinValue() {
		return (EReference)propertyRequirementClauseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPropertyRequirementClause_Formula() {
		return (EReference)propertyRequirementClauseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHWComponentContract() {
		return hwComponentContractEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHWComponentContract_ComponentType() {
		return (EReference)hwComponentContractEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHWComponentContract_Modes() {
		return (EReference)hwComponentContractEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHWContractMode() {
		return hwContractModeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHWContractMode_Clauses() {
		return (EReference)hwContractModeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSWContractMode() {
		return swContractModeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSWContractMode_Clauses() {
		return (EReference)swContractModeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHWContractClause() {
		return hwContractClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHWComponentRequirementClause() {
		return hwComponentRequirementClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHWComponentRequirementClause_RequiredProperties() {
		return (EReference)hwComponentRequirementClauseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHWComponentRequirementClause_RequiredResourceType() {
		return (EReference)hwComponentRequirementClauseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHWComponentRequirementClause_EnergyRate() {
		return (EAttribute)hwComponentRequirementClauseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSelfRequirementClause() {
		return selfRequirementClauseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelfRequirementClause_RequiredProperty() {
		return (EReference)selfRequirementClauseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFormulaTemplate() {
		return formulaTemplateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFormulaTemplate_Metaparameter() {
		return (EReference)formulaTemplateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetaparameter() {
		return metaparameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclFactory getEclFactory() {
		return (EclFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		importEClass = createEClass(IMPORT);

		eclImportEClass = createEClass(ECL_IMPORT);

		ccmImportEClass = createEClass(CCM_IMPORT);
		createEReference(ccmImportEClass, CCM_IMPORT__CCM_STRUCTURAL_MODEL);

		swComponentContractEClass = createEClass(SW_COMPONENT_CONTRACT);
		createEReference(swComponentContractEClass, SW_COMPONENT_CONTRACT__MODES);
		createEReference(swComponentContractEClass, SW_COMPONENT_CONTRACT__COMPONENT_TYPE);
		createEReference(swComponentContractEClass, SW_COMPONENT_CONTRACT__PORT);

		eclFileEClass = createEClass(ECL_FILE);
		createEReference(eclFileEClass, ECL_FILE__CONTRACTS);
		createEReference(eclFileEClass, ECL_FILE__IMPORTS);

		contractModeEClass = createEClass(CONTRACT_MODE);
		createEAttribute(contractModeEClass, CONTRACT_MODE__NAME);

		provisionClauseEClass = createEClass(PROVISION_CLAUSE);
		createEReference(provisionClauseEClass, PROVISION_CLAUSE__PROVIDED_PROPERTY);
		createEReference(provisionClauseEClass, PROVISION_CLAUSE__MIN_VALUE);
		createEReference(provisionClauseEClass, PROVISION_CLAUSE__MAX_VALUE);
		createEReference(provisionClauseEClass, PROVISION_CLAUSE__FORMULA);

		swContractClauseEClass = createEClass(SW_CONTRACT_CLAUSE);

		eclContractEClass = createEClass(ECL_CONTRACT);
		createEAttribute(eclContractEClass, ECL_CONTRACT__NAME);
		createEReference(eclContractEClass, ECL_CONTRACT__METAPARAMS);

		swComponentRequirementClauseEClass = createEClass(SW_COMPONENT_REQUIREMENT_CLAUSE);
		createEReference(swComponentRequirementClauseEClass, SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE);
		createEReference(swComponentRequirementClauseEClass, SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES);

		propertyRequirementClauseEClass = createEClass(PROPERTY_REQUIREMENT_CLAUSE);
		createEReference(propertyRequirementClauseEClass, PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY);
		createEReference(propertyRequirementClauseEClass, PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE);
		createEReference(propertyRequirementClauseEClass, PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE);
		createEReference(propertyRequirementClauseEClass, PROPERTY_REQUIREMENT_CLAUSE__FORMULA);

		hwComponentContractEClass = createEClass(HW_COMPONENT_CONTRACT);
		createEReference(hwComponentContractEClass, HW_COMPONENT_CONTRACT__COMPONENT_TYPE);
		createEReference(hwComponentContractEClass, HW_COMPONENT_CONTRACT__MODES);

		hwContractModeEClass = createEClass(HW_CONTRACT_MODE);
		createEReference(hwContractModeEClass, HW_CONTRACT_MODE__CLAUSES);

		swContractModeEClass = createEClass(SW_CONTRACT_MODE);
		createEReference(swContractModeEClass, SW_CONTRACT_MODE__CLAUSES);

		hwContractClauseEClass = createEClass(HW_CONTRACT_CLAUSE);

		hwComponentRequirementClauseEClass = createEClass(HW_COMPONENT_REQUIREMENT_CLAUSE);
		createEReference(hwComponentRequirementClauseEClass, HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES);
		createEReference(hwComponentRequirementClauseEClass, HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE);
		createEAttribute(hwComponentRequirementClauseEClass, HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE);

		selfRequirementClauseEClass = createEClass(SELF_REQUIREMENT_CLAUSE);
		createEReference(selfRequirementClauseEClass, SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY);

		formulaTemplateEClass = createEClass(FORMULA_TEMPLATE);
		createEReference(formulaTemplateEClass, FORMULA_TEMPLATE__METAPARAMETER);

		metaparameterEClass = createEClass(METAPARAMETER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StructurePackage theStructurePackage = (StructurePackage)EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);
		NameablePackage theNameablePackage = (NameablePackage)EPackage.Registry.INSTANCE.getEPackage(NameablePackage.eNS_URI);
		VariablesPackage theVariablesPackage = (VariablesPackage)EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		eclImportEClass.getESuperTypes().add(this.getImport());
		ccmImportEClass.getESuperTypes().add(this.getImport());
		swComponentContractEClass.getESuperTypes().add(this.getEclContract());
		provisionClauseEClass.getESuperTypes().add(this.getSWContractClause());
		provisionClauseEClass.getESuperTypes().add(this.getHWContractClause());
		swComponentRequirementClauseEClass.getESuperTypes().add(this.getSWContractClause());
		hwComponentContractEClass.getESuperTypes().add(this.getEclContract());
		hwContractModeEClass.getESuperTypes().add(this.getContractMode());
		swContractModeEClass.getESuperTypes().add(this.getContractMode());
		hwComponentRequirementClauseEClass.getESuperTypes().add(this.getHWContractClause());
		hwComponentRequirementClauseEClass.getESuperTypes().add(this.getSWContractClause());
		selfRequirementClauseEClass.getESuperTypes().add(this.getSWContractClause());
		formulaTemplateEClass.getESuperTypes().add(theNameablePackage.getNamedElement());
		metaparameterEClass.getESuperTypes().add(theVariablesPackage.getVariable());

		// Initialize classes and features; add operations and parameters
		initEClass(importEClass, Import.class, "Import", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eclImportEClass, EclImport.class, "EclImport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ccmImportEClass, CcmImport.class, "CcmImport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCcmImport_CcmStructuralModel(), theStructurePackage.getStructuralModel(), null, "ccmStructuralModel", null, 1, 1, CcmImport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(swComponentContractEClass, SWComponentContract.class, "SWComponentContract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSWComponentContract_Modes(), this.getSWContractMode(), null, "modes", null, 1, -1, SWComponentContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSWComponentContract_ComponentType(), theStructurePackage.getSWComponentType(), null, "componentType", null, 1, 1, SWComponentContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSWComponentContract_Port(), theStructurePackage.getPortType(), null, "port", null, 1, 1, SWComponentContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eclFileEClass, EclFile.class, "EclFile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEclFile_Contracts(), this.getEclContract(), null, "contracts", null, 0, -1, EclFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEclFile_Imports(), this.getImport(), null, "imports", null, 0, -1, EclFile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contractModeEClass, ContractMode.class, "ContractMode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getContractMode_Name(), ecorePackage.getEString(), "name", null, 1, 1, ContractMode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(provisionClauseEClass, ProvisionClause.class, "ProvisionClause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProvisionClause_ProvidedProperty(), theStructurePackage.getProperty(), null, "providedProperty", null, 1, 1, ProvisionClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProvisionClause_MinValue(), theExpressionsPackage.getStatement(), null, "minValue", null, 0, 1, ProvisionClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProvisionClause_MaxValue(), theExpressionsPackage.getStatement(), null, "maxValue", null, 0, 1, ProvisionClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProvisionClause_Formula(), this.getFormulaTemplate(), null, "formula", null, 0, 1, ProvisionClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(swContractClauseEClass, SWContractClause.class, "SWContractClause", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eclContractEClass, EclContract.class, "EclContract", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEclContract_Name(), ecorePackage.getEString(), "name", null, 1, 1, EclContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEclContract_Metaparams(), this.getMetaparameter(), null, "metaparams", null, 0, -1, EclContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(swComponentRequirementClauseEClass, SWComponentRequirementClause.class, "SWComponentRequirementClause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSWComponentRequirementClause_RequiredComponentType(), theStructurePackage.getSWComponentType(), null, "requiredComponentType", null, 1, 1, SWComponentRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSWComponentRequirementClause_RequiredProperties(), this.getPropertyRequirementClause(), null, "requiredProperties", null, 0, -1, SWComponentRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyRequirementClauseEClass, PropertyRequirementClause.class, "PropertyRequirementClause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPropertyRequirementClause_RequiredProperty(), theStructurePackage.getProperty(), null, "requiredProperty", null, 1, 1, PropertyRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyRequirementClause_MaxValue(), theExpressionsPackage.getStatement(), null, "maxValue", null, 0, 1, PropertyRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyRequirementClause_MinValue(), theExpressionsPackage.getStatement(), null, "minValue", null, 0, 1, PropertyRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPropertyRequirementClause_Formula(), this.getFormulaTemplate(), null, "formula", null, 0, 1, PropertyRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hwComponentContractEClass, HWComponentContract.class, "HWComponentContract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHWComponentContract_ComponentType(), theStructurePackage.getResourceType(), null, "componentType", null, 1, 1, HWComponentContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHWComponentContract_Modes(), this.getHWContractMode(), null, "modes", null, 1, -1, HWComponentContract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hwContractModeEClass, HWContractMode.class, "HWContractMode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHWContractMode_Clauses(), this.getHWContractClause(), null, "clauses", null, 1, -1, HWContractMode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(swContractModeEClass, SWContractMode.class, "SWContractMode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSWContractMode_Clauses(), this.getSWContractClause(), null, "clauses", null, 1, -1, SWContractMode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hwContractClauseEClass, HWContractClause.class, "HWContractClause", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hwComponentRequirementClauseEClass, HWComponentRequirementClause.class, "HWComponentRequirementClause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHWComponentRequirementClause_RequiredProperties(), this.getPropertyRequirementClause(), null, "requiredProperties", null, 1, -1, HWComponentRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHWComponentRequirementClause_RequiredResourceType(), theStructurePackage.getResourceType(), null, "requiredResourceType", null, 1, 1, HWComponentRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHWComponentRequirementClause_EnergyRate(), ecorePackage.getEDouble(), "energyRate", "0", 1, 1, HWComponentRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(selfRequirementClauseEClass, SelfRequirementClause.class, "SelfRequirementClause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSelfRequirementClause_RequiredProperty(), this.getPropertyRequirementClause(), null, "requiredProperty", null, 1, 1, SelfRequirementClause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(formulaTemplateEClass, FormulaTemplate.class, "FormulaTemplate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFormulaTemplate_Metaparameter(), theStructurePackage.getParameter(), null, "metaparameter", null, 0, -1, FormulaTemplate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(metaparameterEClass, Metaparameter.class, "Metaparameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //EclPackageImpl
