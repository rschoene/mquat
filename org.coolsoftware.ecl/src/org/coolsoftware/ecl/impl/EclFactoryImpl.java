/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.impl;

import org.coolsoftware.ecl.*;
import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.EclFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.EclImport;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.HWComponentContract;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.HWContractMode;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractMode;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EclFactoryImpl extends EFactoryImpl implements EclFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EclFactory init() {
		try {
			EclFactory theEclFactory = (EclFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.cool-software.org/ecl"); 
			if (theEclFactory != null) {
				return theEclFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EclFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case EclPackage.ECL_IMPORT: return createEclImport();
			case EclPackage.CCM_IMPORT: return createCcmImport();
			case EclPackage.SW_COMPONENT_CONTRACT: return createSWComponentContract();
			case EclPackage.ECL_FILE: return createEclFile();
			case EclPackage.PROVISION_CLAUSE: return createProvisionClause();
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE: return createSWComponentRequirementClause();
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE: return createPropertyRequirementClause();
			case EclPackage.HW_COMPONENT_CONTRACT: return createHWComponentContract();
			case EclPackage.HW_CONTRACT_MODE: return createHWContractMode();
			case EclPackage.SW_CONTRACT_MODE: return createSWContractMode();
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE: return createHWComponentRequirementClause();
			case EclPackage.SELF_REQUIREMENT_CLAUSE: return createSelfRequirementClause();
			case EclPackage.FORMULA_TEMPLATE: return createFormulaTemplate();
			case EclPackage.METAPARAMETER: return createMetaparameter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclImport createEclImport() {
		EclImportImpl eclImport = new EclImportImpl();
		return eclImport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmImport createCcmImport() {
		CcmImportImpl ccmImport = new CcmImportImpl();
		return ccmImport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponentContract createSWComponentContract() {
		SWComponentContractImpl swComponentContract = new SWComponentContractImpl();
		return swComponentContract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclFile createEclFile() {
		EclFileImpl eclFile = new EclFileImpl();
		return eclFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvisionClause createProvisionClause() {
		ProvisionClauseImpl provisionClause = new ProvisionClauseImpl();
		return provisionClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponentRequirementClause createSWComponentRequirementClause() {
		SWComponentRequirementClauseImpl swComponentRequirementClause = new SWComponentRequirementClauseImpl();
		return swComponentRequirementClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyRequirementClause createPropertyRequirementClause() {
		PropertyRequirementClauseImpl propertyRequirementClause = new PropertyRequirementClauseImpl();
		return propertyRequirementClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWComponentContract createHWComponentContract() {
		HWComponentContractImpl hwComponentContract = new HWComponentContractImpl();
		return hwComponentContract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWContractMode createHWContractMode() {
		HWContractModeImpl hwContractMode = new HWContractModeImpl();
		return hwContractMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWContractMode createSWContractMode() {
		SWContractModeImpl swContractMode = new SWContractModeImpl();
		return swContractMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HWComponentRequirementClause createHWComponentRequirementClause() {
		HWComponentRequirementClauseImpl hwComponentRequirementClause = new HWComponentRequirementClauseImpl();
		return hwComponentRequirementClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelfRequirementClause createSelfRequirementClause() {
		SelfRequirementClauseImpl selfRequirementClause = new SelfRequirementClauseImpl();
		return selfRequirementClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormulaTemplate createFormulaTemplate() {
		FormulaTemplateImpl formulaTemplate = new FormulaTemplateImpl();
		return formulaTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Metaparameter createMetaparameter() {
		MetaparameterImpl metaparameter = new MetaparameterImpl();
		return metaparameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclPackage getEclPackage() {
		return (EclPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EclPackage getPackage() {
		return EclPackage.eINSTANCE;
	}

} //EclFactoryImpl
