/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.impl;

import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.SelfRequirementClause;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Self Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.impl.SelfRequirementClauseImpl#getRequiredProperty <em>Required Property</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SelfRequirementClauseImpl extends EObjectImpl implements SelfRequirementClause {
	/**
	 * The cached value of the '{@link #getRequiredProperty() <em>Required Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredProperty()
	 * @generated
	 * @ordered
	 */
	protected PropertyRequirementClause requiredProperty;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SelfRequirementClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclPackage.Literals.SELF_REQUIREMENT_CLAUSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyRequirementClause getRequiredProperty() {
		return requiredProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequiredProperty(PropertyRequirementClause newRequiredProperty, NotificationChain msgs) {
		PropertyRequirementClause oldRequiredProperty = requiredProperty;
		requiredProperty = newRequiredProperty;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY, oldRequiredProperty, newRequiredProperty);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredProperty(PropertyRequirementClause newRequiredProperty) {
		if (newRequiredProperty != requiredProperty) {
			NotificationChain msgs = null;
			if (requiredProperty != null)
				msgs = ((InternalEObject)requiredProperty).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY, null, msgs);
			if (newRequiredProperty != null)
				msgs = ((InternalEObject)newRequiredProperty).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY, null, msgs);
			msgs = basicSetRequiredProperty(newRequiredProperty, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY, newRequiredProperty, newRequiredProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				return basicSetRequiredProperty(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				return getRequiredProperty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				setRequiredProperty((PropertyRequirementClause)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				setRequiredProperty((PropertyRequirementClause)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclPackage.SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				return requiredProperty != null;
		}
		return super.eIsSet(featureID);
	}

} //SelfRequirementClauseImpl
