/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.impl;

import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.EclPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ccm Import</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.impl.CcmImportImpl#getCcmStructuralModel <em>Ccm Structural Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CcmImportImpl extends ImportImpl implements CcmImport {
	/**
	 * The cached value of the '{@link #getCcmStructuralModel() <em>Ccm Structural Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCcmStructuralModel()
	 * @generated
	 * @ordered
	 */
	protected StructuralModel ccmStructuralModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CcmImportImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclPackage.Literals.CCM_IMPORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructuralModel getCcmStructuralModel() {
		if (ccmStructuralModel != null && ccmStructuralModel.eIsProxy()) {
			InternalEObject oldCcmStructuralModel = (InternalEObject)ccmStructuralModel;
			ccmStructuralModel = (StructuralModel)eResolveProxy(oldCcmStructuralModel);
			if (ccmStructuralModel != oldCcmStructuralModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL, oldCcmStructuralModel, ccmStructuralModel));
			}
		}
		return ccmStructuralModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructuralModel basicGetCcmStructuralModel() {
		return ccmStructuralModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCcmStructuralModel(StructuralModel newCcmStructuralModel) {
		StructuralModel oldCcmStructuralModel = ccmStructuralModel;
		ccmStructuralModel = newCcmStructuralModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL, oldCcmStructuralModel, ccmStructuralModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL:
				if (resolve) return getCcmStructuralModel();
				return basicGetCcmStructuralModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL:
				setCcmStructuralModel((StructuralModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL:
				setCcmStructuralModel((StructuralModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclPackage.CCM_IMPORT__CCM_STRUCTURAL_MODEL:
				return ccmStructuralModel != null;
		}
		return super.eIsSet(featureID);
	}

} //CcmImportImpl
