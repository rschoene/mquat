/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HW Component Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.impl.HWComponentRequirementClauseImpl#getRequiredProperties <em>Required Properties</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.impl.HWComponentRequirementClauseImpl#getRequiredResourceType <em>Required Resource Type</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.impl.HWComponentRequirementClauseImpl#getEnergyRate <em>Energy Rate</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class HWComponentRequirementClauseImpl extends EObjectImpl implements HWComponentRequirementClause {
	/**
	 * The cached value of the '{@link #getRequiredProperties() <em>Required Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyRequirementClause> requiredProperties;

	/**
	 * The cached value of the '{@link #getRequiredResourceType() <em>Required Resource Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredResourceType()
	 * @generated
	 * @ordered
	 */
	protected ResourceType requiredResourceType;

	/**
	 * The default value of the '{@link #getEnergyRate() <em>Energy Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnergyRate()
	 * @generated
	 * @ordered
	 */
	protected static final double ENERGY_RATE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getEnergyRate() <em>Energy Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnergyRate()
	 * @generated
	 * @ordered
	 */
	protected double energyRate = ENERGY_RATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HWComponentRequirementClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclPackage.Literals.HW_COMPONENT_REQUIREMENT_CLAUSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyRequirementClause> getRequiredProperties() {
		if (requiredProperties == null) {
			requiredProperties = new EObjectContainmentEList<PropertyRequirementClause>(PropertyRequirementClause.class, this, EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES);
		}
		return requiredProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceType getRequiredResourceType() {
		if (requiredResourceType != null && requiredResourceType.eIsProxy()) {
			InternalEObject oldRequiredResourceType = (InternalEObject)requiredResourceType;
			requiredResourceType = (ResourceType)eResolveProxy(oldRequiredResourceType);
			if (requiredResourceType != oldRequiredResourceType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE, oldRequiredResourceType, requiredResourceType));
			}
		}
		return requiredResourceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceType basicGetRequiredResourceType() {
		return requiredResourceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredResourceType(ResourceType newRequiredResourceType) {
		ResourceType oldRequiredResourceType = requiredResourceType;
		requiredResourceType = newRequiredResourceType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE, oldRequiredResourceType, requiredResourceType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getEnergyRate() {
		return energyRate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnergyRate(double newEnergyRate) {
		double oldEnergyRate = energyRate;
		energyRate = newEnergyRate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE, oldEnergyRate, energyRate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				return ((InternalEList<?>)getRequiredProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				return getRequiredProperties();
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE:
				if (resolve) return getRequiredResourceType();
				return basicGetRequiredResourceType();
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE:
				return getEnergyRate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				getRequiredProperties().clear();
				getRequiredProperties().addAll((Collection<? extends PropertyRequirementClause>)newValue);
				return;
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE:
				setRequiredResourceType((ResourceType)newValue);
				return;
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE:
				setEnergyRate((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				getRequiredProperties().clear();
				return;
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE:
				setRequiredResourceType((ResourceType)null);
				return;
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE:
				setEnergyRate(ENERGY_RATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				return requiredProperties != null && !requiredProperties.isEmpty();
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE:
				return requiredResourceType != null;
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE:
				return energyRate != ENERGY_RATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (energyRate: ");
		result.append(energyRate);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof HWComponentRequirementClause) {
			HWComponentRequirementClause ref = (HWComponentRequirementClause)obj;
			if(ref != null && ref.getEnergyRate() == this.getEnergyRate() && ref.getRequiredResourceType() != null && this.getRequiredResourceType() != null && ref.getRequiredResourceType().getName() != null && this.getRequiredResourceType().getName() != null&& ref.getRequiredResourceType().getName().equals(this.getRequiredResourceType().getName())) {
				boolean equal = true;
				List<String> propNames = new ArrayList<String>();
				for(PropertyRequirementClause tp : this.getRequiredProperties()) {
					propNames.add(tp.getRequiredProperty().getDeclaredVariable().getName());
				}
				for(PropertyRequirementClause prc : ref.getRequiredProperties()) {
					if(!propNames.contains(prc.getRequiredProperty().getDeclaredVariable().getName())) return false;
				}
				return equal;
			}
		}
		return super.equals(obj);
	}
} //HWComponentRequirementClauseImpl
