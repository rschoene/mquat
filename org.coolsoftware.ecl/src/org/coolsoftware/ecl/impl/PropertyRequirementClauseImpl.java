/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.impl;

import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl#getRequiredProperty <em>Required Property</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl#getMaxValue <em>Max Value</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl#getMinValue <em>Min Value</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl#getFormula <em>Formula</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertyRequirementClauseImpl extends EObjectImpl implements PropertyRequirementClause {
	/**
	 * The cached value of the '{@link #getRequiredProperty() <em>Required Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredProperty()
	 * @generated
	 * @ordered
	 */
	protected Property requiredProperty;

	/**
	 * The cached value of the '{@link #getMaxValue() <em>Max Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxValue()
	 * @generated
	 * @ordered
	 */
	protected Statement maxValue;

	/**
	 * The cached value of the '{@link #getMinValue() <em>Min Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinValue()
	 * @generated
	 * @ordered
	 */
	protected Statement minValue;

	/**
	 * The cached value of the '{@link #getFormula() <em>Formula</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormula()
	 * @generated
	 * @ordered
	 */
	protected FormulaTemplate formula;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyRequirementClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclPackage.Literals.PROPERTY_REQUIREMENT_CLAUSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getRequiredProperty() {
		if (requiredProperty != null && requiredProperty.eIsProxy()) {
			InternalEObject oldRequiredProperty = (InternalEObject)requiredProperty;
			requiredProperty = (Property)eResolveProxy(oldRequiredProperty);
			if (requiredProperty != oldRequiredProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY, oldRequiredProperty, requiredProperty));
			}
		}
		return requiredProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetRequiredProperty() {
		return requiredProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredProperty(Property newRequiredProperty) {
		Property oldRequiredProperty = requiredProperty;
		requiredProperty = newRequiredProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY, oldRequiredProperty, requiredProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Statement getMaxValue() {
		return maxValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxValue(Statement newMaxValue, NotificationChain msgs) {
		Statement oldMaxValue = maxValue;
		maxValue = newMaxValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE, oldMaxValue, newMaxValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxValue(Statement newMaxValue) {
		if (newMaxValue != maxValue) {
			NotificationChain msgs = null;
			if (maxValue != null)
				msgs = ((InternalEObject)maxValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE, null, msgs);
			if (newMaxValue != null)
				msgs = ((InternalEObject)newMaxValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE, null, msgs);
			msgs = basicSetMaxValue(newMaxValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE, newMaxValue, newMaxValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Statement getMinValue() {
		return minValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMinValue(Statement newMinValue, NotificationChain msgs) {
		Statement oldMinValue = minValue;
		minValue = newMinValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE, oldMinValue, newMinValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinValue(Statement newMinValue) {
		if (newMinValue != minValue) {
			NotificationChain msgs = null;
			if (minValue != null)
				msgs = ((InternalEObject)minValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE, null, msgs);
			if (newMinValue != null)
				msgs = ((InternalEObject)newMinValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE, null, msgs);
			msgs = basicSetMinValue(newMinValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE, newMinValue, newMinValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormulaTemplate getFormula() {
		return formula;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormula(FormulaTemplate newFormula, NotificationChain msgs) {
		FormulaTemplate oldFormula = formula;
		formula = newFormula;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA, oldFormula, newFormula);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormula(FormulaTemplate newFormula) {
		if (newFormula != formula) {
			NotificationChain msgs = null;
			if (formula != null)
				msgs = ((InternalEObject)formula).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA, null, msgs);
			if (newFormula != null)
				msgs = ((InternalEObject)newFormula).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA, null, msgs);
			msgs = basicSetFormula(newFormula, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA, newFormula, newFormula));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE:
				return basicSetMaxValue(null, msgs);
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE:
				return basicSetMinValue(null, msgs);
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA:
				return basicSetFormula(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				if (resolve) return getRequiredProperty();
				return basicGetRequiredProperty();
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE:
				return getMaxValue();
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE:
				return getMinValue();
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA:
				return getFormula();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				setRequiredProperty((Property)newValue);
				return;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE:
				setMaxValue((Statement)newValue);
				return;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE:
				setMinValue((Statement)newValue);
				return;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA:
				setFormula((FormulaTemplate)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				setRequiredProperty((Property)null);
				return;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE:
				setMaxValue((Statement)null);
				return;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE:
				setMinValue((Statement)null);
				return;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA:
				setFormula((FormulaTemplate)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY:
				return requiredProperty != null;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE:
				return maxValue != null;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE:
				return minValue != null;
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE__FORMULA:
				return formula != null;
		}
		return super.eIsSet(featureID);
	}

} //PropertyRequirementClauseImpl
