/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SW Component Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.impl.SWComponentRequirementClauseImpl#getRequiredComponentType <em>Required Component Type</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.impl.SWComponentRequirementClauseImpl#getRequiredProperties <em>Required Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SWComponentRequirementClauseImpl extends EObjectImpl implements SWComponentRequirementClause {
	/**
	 * The cached value of the '{@link #getRequiredComponentType() <em>Required Component Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredComponentType()
	 * @generated
	 * @ordered
	 */
	protected SWComponentType requiredComponentType;

	/**
	 * The cached value of the '{@link #getRequiredProperties() <em>Required Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyRequirementClause> requiredProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SWComponentRequirementClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EclPackage.Literals.SW_COMPONENT_REQUIREMENT_CLAUSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponentType getRequiredComponentType() {
		if (requiredComponentType != null && requiredComponentType.eIsProxy()) {
			InternalEObject oldRequiredComponentType = (InternalEObject)requiredComponentType;
			requiredComponentType = (SWComponentType)eResolveProxy(oldRequiredComponentType);
			if (requiredComponentType != oldRequiredComponentType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE, oldRequiredComponentType, requiredComponentType));
			}
		}
		return requiredComponentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponentType basicGetRequiredComponentType() {
		return requiredComponentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredComponentType(SWComponentType newRequiredComponentType) {
		SWComponentType oldRequiredComponentType = requiredComponentType;
		requiredComponentType = newRequiredComponentType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE, oldRequiredComponentType, requiredComponentType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyRequirementClause> getRequiredProperties() {
		if (requiredProperties == null) {
			requiredProperties = new EObjectContainmentEList<PropertyRequirementClause>(PropertyRequirementClause.class, this, EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES);
		}
		return requiredProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				return ((InternalEList<?>)getRequiredProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE:
				if (resolve) return getRequiredComponentType();
				return basicGetRequiredComponentType();
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				return getRequiredProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE:
				setRequiredComponentType((SWComponentType)newValue);
				return;
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				getRequiredProperties().clear();
				getRequiredProperties().addAll((Collection<? extends PropertyRequirementClause>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE:
				setRequiredComponentType((SWComponentType)null);
				return;
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				getRequiredProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE:
				return requiredComponentType != null;
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES:
				return requiredProperties != null && !requiredProperties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SWComponentRequirementClauseImpl
