/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.util;

import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.coolsoftware.ecl.*;
import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.EclImport;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.HWComponentContract;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.HWContractClause;
import org.coolsoftware.ecl.HWContractMode;
import org.coolsoftware.ecl.Import;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.ecl.EclPackage
 * @generated
 */
public class EclAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EclPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = EclPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EclSwitch<Adapter> modelSwitch =
		new EclSwitch<Adapter>() {
			@Override
			public Adapter caseImport(Import object) {
				return createImportAdapter();
			}
			@Override
			public Adapter caseEclImport(EclImport object) {
				return createEclImportAdapter();
			}
			@Override
			public Adapter caseCcmImport(CcmImport object) {
				return createCcmImportAdapter();
			}
			@Override
			public Adapter caseSWComponentContract(SWComponentContract object) {
				return createSWComponentContractAdapter();
			}
			@Override
			public Adapter caseEclFile(EclFile object) {
				return createEclFileAdapter();
			}
			@Override
			public Adapter caseContractMode(ContractMode object) {
				return createContractModeAdapter();
			}
			@Override
			public Adapter caseProvisionClause(ProvisionClause object) {
				return createProvisionClauseAdapter();
			}
			@Override
			public Adapter caseSWContractClause(SWContractClause object) {
				return createSWContractClauseAdapter();
			}
			@Override
			public Adapter caseEclContract(EclContract object) {
				return createEclContractAdapter();
			}
			@Override
			public Adapter caseSWComponentRequirementClause(SWComponentRequirementClause object) {
				return createSWComponentRequirementClauseAdapter();
			}
			@Override
			public Adapter casePropertyRequirementClause(PropertyRequirementClause object) {
				return createPropertyRequirementClauseAdapter();
			}
			@Override
			public Adapter caseHWComponentContract(HWComponentContract object) {
				return createHWComponentContractAdapter();
			}
			@Override
			public Adapter caseHWContractMode(HWContractMode object) {
				return createHWContractModeAdapter();
			}
			@Override
			public Adapter caseSWContractMode(SWContractMode object) {
				return createSWContractModeAdapter();
			}
			@Override
			public Adapter caseHWContractClause(HWContractClause object) {
				return createHWContractClauseAdapter();
			}
			@Override
			public Adapter caseHWComponentRequirementClause(HWComponentRequirementClause object) {
				return createHWComponentRequirementClauseAdapter();
			}
			@Override
			public Adapter caseSelfRequirementClause(SelfRequirementClause object) {
				return createSelfRequirementClauseAdapter();
			}
			@Override
			public Adapter caseFormulaTemplate(FormulaTemplate object) {
				return createFormulaTemplateAdapter();
			}
			@Override
			public Adapter caseMetaparameter(Metaparameter object) {
				return createMetaparameterAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseVariable(Variable object) {
				return createVariableAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.Import
	 * @generated
	 */
	public Adapter createImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.EclImport <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.EclImport
	 * @generated
	 */
	public Adapter createEclImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.CcmImport <em>Ccm Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.CcmImport
	 * @generated
	 */
	public Adapter createCcmImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.SWComponentContract <em>SW Component Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.SWComponentContract
	 * @generated
	 */
	public Adapter createSWComponentContractAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.EclFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.EclFile
	 * @generated
	 */
	public Adapter createEclFileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.ContractMode <em>Contract Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.ContractMode
	 * @generated
	 */
	public Adapter createContractModeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.ProvisionClause <em>Provision Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.ProvisionClause
	 * @generated
	 */
	public Adapter createProvisionClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.SWContractClause <em>SW Contract Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.SWContractClause
	 * @generated
	 */
	public Adapter createSWContractClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.EclContract <em>Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.EclContract
	 * @generated
	 */
	public Adapter createEclContractAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.SWComponentRequirementClause <em>SW Component Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.SWComponentRequirementClause
	 * @generated
	 */
	public Adapter createSWComponentRequirementClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.PropertyRequirementClause <em>Property Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.PropertyRequirementClause
	 * @generated
	 */
	public Adapter createPropertyRequirementClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.HWComponentContract <em>HW Component Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.HWComponentContract
	 * @generated
	 */
	public Adapter createHWComponentContractAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.HWContractMode <em>HW Contract Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.HWContractMode
	 * @generated
	 */
	public Adapter createHWContractModeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.SWContractMode <em>SW Contract Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.SWContractMode
	 * @generated
	 */
	public Adapter createSWContractModeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.HWContractClause <em>HW Contract Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.HWContractClause
	 * @generated
	 */
	public Adapter createHWContractClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.HWComponentRequirementClause <em>HW Component Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.HWComponentRequirementClause
	 * @generated
	 */
	public Adapter createHWComponentRequirementClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.SelfRequirementClause <em>Self Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.SelfRequirementClause
	 * @generated
	 */
	public Adapter createSelfRequirementClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.FormulaTemplate <em>Formula Template</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.FormulaTemplate
	 * @generated
	 */
	public Adapter createFormulaTemplateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.ecl.Metaparameter <em>Metaparameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.ecl.Metaparameter
	 * @generated
	 */
	public Adapter createMetaparameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.nameable.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.nameable.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.variables.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.variables.Variable
	 * @generated
	 */
	public Adapter createVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //EclAdapterFactory
