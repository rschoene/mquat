/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl.util;

import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.coolsoftware.ecl.*;
import java.util.List;

import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.ContractMode;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.EclImport;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.HWComponentContract;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.HWContractClause;
import org.coolsoftware.ecl.HWContractMode;
import org.coolsoftware.ecl.Import;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.ecl.EclPackage
 * @generated
 */
public class EclSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EclPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EclSwitch() {
		if (modelPackage == null) {
			modelPackage = EclPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case EclPackage.IMPORT: {
				Import import_ = (Import)theEObject;
				T result = caseImport(import_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.ECL_IMPORT: {
				EclImport eclImport = (EclImport)theEObject;
				T result = caseEclImport(eclImport);
				if (result == null) result = caseImport(eclImport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.CCM_IMPORT: {
				CcmImport ccmImport = (CcmImport)theEObject;
				T result = caseCcmImport(ccmImport);
				if (result == null) result = caseImport(ccmImport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.SW_COMPONENT_CONTRACT: {
				SWComponentContract swComponentContract = (SWComponentContract)theEObject;
				T result = caseSWComponentContract(swComponentContract);
				if (result == null) result = caseEclContract(swComponentContract);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.ECL_FILE: {
				EclFile eclFile = (EclFile)theEObject;
				T result = caseEclFile(eclFile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.CONTRACT_MODE: {
				ContractMode contractMode = (ContractMode)theEObject;
				T result = caseContractMode(contractMode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.PROVISION_CLAUSE: {
				ProvisionClause provisionClause = (ProvisionClause)theEObject;
				T result = caseProvisionClause(provisionClause);
				if (result == null) result = caseSWContractClause(provisionClause);
				if (result == null) result = caseHWContractClause(provisionClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.SW_CONTRACT_CLAUSE: {
				SWContractClause swContractClause = (SWContractClause)theEObject;
				T result = caseSWContractClause(swContractClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.ECL_CONTRACT: {
				EclContract eclContract = (EclContract)theEObject;
				T result = caseEclContract(eclContract);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.SW_COMPONENT_REQUIREMENT_CLAUSE: {
				SWComponentRequirementClause swComponentRequirementClause = (SWComponentRequirementClause)theEObject;
				T result = caseSWComponentRequirementClause(swComponentRequirementClause);
				if (result == null) result = caseSWContractClause(swComponentRequirementClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.PROPERTY_REQUIREMENT_CLAUSE: {
				PropertyRequirementClause propertyRequirementClause = (PropertyRequirementClause)theEObject;
				T result = casePropertyRequirementClause(propertyRequirementClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.HW_COMPONENT_CONTRACT: {
				HWComponentContract hwComponentContract = (HWComponentContract)theEObject;
				T result = caseHWComponentContract(hwComponentContract);
				if (result == null) result = caseEclContract(hwComponentContract);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.HW_CONTRACT_MODE: {
				HWContractMode hwContractMode = (HWContractMode)theEObject;
				T result = caseHWContractMode(hwContractMode);
				if (result == null) result = caseContractMode(hwContractMode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.SW_CONTRACT_MODE: {
				SWContractMode swContractMode = (SWContractMode)theEObject;
				T result = caseSWContractMode(swContractMode);
				if (result == null) result = caseContractMode(swContractMode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.HW_CONTRACT_CLAUSE: {
				HWContractClause hwContractClause = (HWContractClause)theEObject;
				T result = caseHWContractClause(hwContractClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.HW_COMPONENT_REQUIREMENT_CLAUSE: {
				HWComponentRequirementClause hwComponentRequirementClause = (HWComponentRequirementClause)theEObject;
				T result = caseHWComponentRequirementClause(hwComponentRequirementClause);
				if (result == null) result = caseHWContractClause(hwComponentRequirementClause);
				if (result == null) result = caseSWContractClause(hwComponentRequirementClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.SELF_REQUIREMENT_CLAUSE: {
				SelfRequirementClause selfRequirementClause = (SelfRequirementClause)theEObject;
				T result = caseSelfRequirementClause(selfRequirementClause);
				if (result == null) result = caseSWContractClause(selfRequirementClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.FORMULA_TEMPLATE: {
				FormulaTemplate formulaTemplate = (FormulaTemplate)theEObject;
				T result = caseFormulaTemplate(formulaTemplate);
				if (result == null) result = caseNamedElement(formulaTemplate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EclPackage.METAPARAMETER: {
				Metaparameter metaparameter = (Metaparameter)theEObject;
				T result = caseMetaparameter(metaparameter);
				if (result == null) result = caseVariable(metaparameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImport(Import object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEclImport(EclImport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmImport(CcmImport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SW Component Contract</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SW Component Contract</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSWComponentContract(SWComponentContract object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>File</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>File</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEclFile(EclFile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract Mode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract Mode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContractMode(ContractMode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provision Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provision Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvisionClause(ProvisionClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SW Contract Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SW Contract Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSWContractClause(SWContractClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contract</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contract</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEclContract(EclContract object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SW Component Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SW Component Requirement Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSWComponentRequirementClause(SWComponentRequirementClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property Requirement Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePropertyRequirementClause(PropertyRequirementClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HW Component Contract</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HW Component Contract</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHWComponentContract(HWComponentContract object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HW Contract Mode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HW Contract Mode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHWContractMode(HWContractMode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SW Contract Mode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SW Contract Mode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSWContractMode(SWContractMode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HW Contract Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HW Contract Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHWContractClause(HWContractClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HW Component Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HW Component Requirement Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHWComponentRequirementClause(HWComponentRequirementClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Self Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Self Requirement Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelfRequirementClause(SelfRequirementClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formula Template</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formula Template</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormulaTemplate(FormulaTemplate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Metaparameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Metaparameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetaparameter(Metaparameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //EclSwitch
