/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HW Component Contract</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.HWComponentContract#getComponentType <em>Component Type</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.HWComponentContract#getModes <em>Modes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.ecl.EclPackage#getHWComponentContract()
 * @model
 * @generated
 */
public interface HWComponentContract extends EclContract {
	/**
	 * Returns the value of the '<em><b>Component Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Type</em>' reference.
	 * @see #setComponentType(ResourceType)
	 * @see org.coolsoftware.ecl.EclPackage#getHWComponentContract_ComponentType()
	 * @model required="true"
	 * @generated
	 */
	ResourceType getComponentType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.HWComponentContract#getComponentType <em>Component Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Type</em>' reference.
	 * @see #getComponentType()
	 * @generated
	 */
	void setComponentType(ResourceType value);

	/**
	 * Returns the value of the '<em><b>Modes</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.ecl.HWContractMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modes</em>' containment reference list.
	 * @see org.coolsoftware.ecl.EclPackage#getHWComponentContract_Modes()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<HWContractMode> getModes();

} // HWComponentContract
