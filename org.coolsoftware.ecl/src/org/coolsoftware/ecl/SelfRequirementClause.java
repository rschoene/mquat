/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Self Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.SelfRequirementClause#getRequiredProperty <em>Required Property</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.ecl.EclPackage#getSelfRequirementClause()
 * @model
 * @generated
 */
public interface SelfRequirementClause extends SWContractClause {
	/**
	 * Returns the value of the '<em><b>Required Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Property</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Property</em>' containment reference.
	 * @see #setRequiredProperty(PropertyRequirementClause)
	 * @see org.coolsoftware.ecl.EclPackage#getSelfRequirementClause_RequiredProperty()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PropertyRequirementClause getRequiredProperty();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.SelfRequirementClause#getRequiredProperty <em>Required Property</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Property</em>' containment reference.
	 * @see #getRequiredProperty()
	 * @generated
	 */
	void setRequiredProperty(PropertyRequirementClause value);

} // SelfRequirementClause
