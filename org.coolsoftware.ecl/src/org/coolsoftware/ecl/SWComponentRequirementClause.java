/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SW Component Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.SWComponentRequirementClause#getRequiredComponentType <em>Required Component Type</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.SWComponentRequirementClause#getRequiredProperties <em>Required Properties</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.ecl.EclPackage#getSWComponentRequirementClause()
 * @model
 * @generated
 */
public interface SWComponentRequirementClause extends SWContractClause {
	/**
	 * Returns the value of the '<em><b>Required Component Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Component Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Component Type</em>' reference.
	 * @see #setRequiredComponentType(SWComponentType)
	 * @see org.coolsoftware.ecl.EclPackage#getSWComponentRequirementClause_RequiredComponentType()
	 * @model required="true"
	 * @generated
	 */
	SWComponentType getRequiredComponentType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.SWComponentRequirementClause#getRequiredComponentType <em>Required Component Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Component Type</em>' reference.
	 * @see #getRequiredComponentType()
	 * @generated
	 */
	void setRequiredComponentType(SWComponentType value);

	/**
	 * Returns the value of the '<em><b>Required Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.ecl.PropertyRequirementClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Properties</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Properties</em>' containment reference list.
	 * @see org.coolsoftware.ecl.EclPackage#getSWComponentRequirementClause_RequiredProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<PropertyRequirementClause> getRequiredProperties();

} // SWComponentRequirementClause
