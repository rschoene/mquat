/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.PropertyRequirementClause#getRequiredProperty <em>Required Property</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.PropertyRequirementClause#getMaxValue <em>Max Value</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.PropertyRequirementClause#getMinValue <em>Min Value</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.PropertyRequirementClause#getFormula <em>Formula</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.ecl.EclPackage#getPropertyRequirementClause()
 * @model
 * @generated
 */
public interface PropertyRequirementClause extends EObject {
	/**
	 * Returns the value of the '<em><b>Required Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Property</em>' reference.
	 * @see #setRequiredProperty(Property)
	 * @see org.coolsoftware.ecl.EclPackage#getPropertyRequirementClause_RequiredProperty()
	 * @model required="true"
	 * @generated
	 */
	Property getRequiredProperty();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.PropertyRequirementClause#getRequiredProperty <em>Required Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Property</em>' reference.
	 * @see #getRequiredProperty()
	 * @generated
	 */
	void setRequiredProperty(Property value);

	/**
	 * Returns the value of the '<em><b>Max Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Value</em>' containment reference.
	 * @see #setMaxValue(Statement)
	 * @see org.coolsoftware.ecl.EclPackage#getPropertyRequirementClause_MaxValue()
	 * @model containment="true"
	 * @generated
	 */
	Statement getMaxValue();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.PropertyRequirementClause#getMaxValue <em>Max Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Value</em>' containment reference.
	 * @see #getMaxValue()
	 * @generated
	 */
	void setMaxValue(Statement value);

	/**
	 * Returns the value of the '<em><b>Min Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Value</em>' containment reference.
	 * @see #setMinValue(Statement)
	 * @see org.coolsoftware.ecl.EclPackage#getPropertyRequirementClause_MinValue()
	 * @model containment="true"
	 * @generated
	 */
	Statement getMinValue();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.PropertyRequirementClause#getMinValue <em>Min Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Value</em>' containment reference.
	 * @see #getMinValue()
	 * @generated
	 */
	void setMinValue(Statement value);

	/**
	 * Returns the value of the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formula</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' containment reference.
	 * @see #setFormula(FormulaTemplate)
	 * @see org.coolsoftware.ecl.EclPackage#getPropertyRequirementClause_Formula()
	 * @model containment="true"
	 * @generated
	 */
	FormulaTemplate getFormula();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.PropertyRequirementClause#getFormula <em>Formula</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formula</em>' containment reference.
	 * @see #getFormula()
	 * @generated
	 */
	void setFormula(FormulaTemplate value);

} // PropertyRequirementClause
