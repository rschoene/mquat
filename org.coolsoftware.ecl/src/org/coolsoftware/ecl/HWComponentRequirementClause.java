/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HW Component Requirement Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.HWComponentRequirementClause#getRequiredProperties <em>Required Properties</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.HWComponentRequirementClause#getRequiredResourceType <em>Required Resource Type</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.HWComponentRequirementClause#getEnergyRate <em>Energy Rate</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.ecl.EclPackage#getHWComponentRequirementClause()
 * @model
 * @generated
 */
public interface HWComponentRequirementClause extends HWContractClause, SWContractClause {
	/**
	 * Returns the value of the '<em><b>Required Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.ecl.PropertyRequirementClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Properties</em>' containment reference list.
	 * @see org.coolsoftware.ecl.EclPackage#getHWComponentRequirementClause_RequiredProperties()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<PropertyRequirementClause> getRequiredProperties();

	/**
	 * Returns the value of the '<em><b>Required Resource Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Resource Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Resource Type</em>' reference.
	 * @see #setRequiredResourceType(ResourceType)
	 * @see org.coolsoftware.ecl.EclPackage#getHWComponentRequirementClause_RequiredResourceType()
	 * @model required="true"
	 * @generated
	 */
	ResourceType getRequiredResourceType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.HWComponentRequirementClause#getRequiredResourceType <em>Required Resource Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Resource Type</em>' reference.
	 * @see #getRequiredResourceType()
	 * @generated
	 */
	void setRequiredResourceType(ResourceType value);

	/**
	 * Returns the value of the '<em><b>Energy Rate</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Energy Rate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Energy Rate</em>' attribute.
	 * @see #setEnergyRate(double)
	 * @see org.coolsoftware.ecl.EclPackage#getHWComponentRequirementClause_EnergyRate()
	 * @model default="0" required="true"
	 * @generated
	 */
	double getEnergyRate();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.HWComponentRequirementClause#getEnergyRate <em>Energy Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Energy Rate</em>' attribute.
	 * @see #getEnergyRate()
	 * @generated
	 */
	void setEnergyRate(double value);

} // HWComponentRequirementClause
