/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.ecl.EclPackage
 * @generated
 */
public interface EclFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EclFactory eINSTANCE = org.coolsoftware.ecl.impl.EclFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Import</em>'.
	 * @generated
	 */
	EclImport createEclImport();

	/**
	 * Returns a new object of class '<em>Ccm Import</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ccm Import</em>'.
	 * @generated
	 */
	CcmImport createCcmImport();

	/**
	 * Returns a new object of class '<em>SW Component Contract</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SW Component Contract</em>'.
	 * @generated
	 */
	SWComponentContract createSWComponentContract();

	/**
	 * Returns a new object of class '<em>File</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>File</em>'.
	 * @generated
	 */
	EclFile createEclFile();

	/**
	 * Returns a new object of class '<em>Provision Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provision Clause</em>'.
	 * @generated
	 */
	ProvisionClause createProvisionClause();

	/**
	 * Returns a new object of class '<em>SW Component Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SW Component Requirement Clause</em>'.
	 * @generated
	 */
	SWComponentRequirementClause createSWComponentRequirementClause();

	/**
	 * Returns a new object of class '<em>Property Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property Requirement Clause</em>'.
	 * @generated
	 */
	PropertyRequirementClause createPropertyRequirementClause();

	/**
	 * Returns a new object of class '<em>HW Component Contract</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HW Component Contract</em>'.
	 * @generated
	 */
	HWComponentContract createHWComponentContract();

	/**
	 * Returns a new object of class '<em>HW Contract Mode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HW Contract Mode</em>'.
	 * @generated
	 */
	HWContractMode createHWContractMode();

	/**
	 * Returns a new object of class '<em>SW Contract Mode</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SW Contract Mode</em>'.
	 * @generated
	 */
	SWContractMode createSWContractMode();

	/**
	 * Returns a new object of class '<em>HW Component Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HW Component Requirement Clause</em>'.
	 * @generated
	 */
	HWComponentRequirementClause createHWComponentRequirementClause();

	/**
	 * Returns a new object of class '<em>Self Requirement Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Self Requirement Clause</em>'.
	 * @generated
	 */
	SelfRequirementClause createSelfRequirementClause();

	/**
	 * Returns a new object of class '<em>Formula Template</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formula Template</em>'.
	 * @generated
	 */
	FormulaTemplate createFormulaTemplate();

	/**
	 * Returns a new object of class '<em>Metaparameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Metaparameter</em>'.
	 * @generated
	 */
	Metaparameter createMetaparameter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EclPackage getEclPackage();

} //EclFactory
