/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.Expression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provision Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.ecl.ProvisionClause#getProvidedProperty <em>Provided Property</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.ProvisionClause#getMinValue <em>Min Value</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.ProvisionClause#getMaxValue <em>Max Value</em>}</li>
 *   <li>{@link org.coolsoftware.ecl.ProvisionClause#getFormula <em>Formula</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.ecl.EclPackage#getProvisionClause()
 * @model
 * @generated
 */
public interface ProvisionClause extends SWContractClause, HWContractClause {
	/**
	 * Returns the value of the '<em><b>Provided Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Property</em>' reference.
	 * @see #setProvidedProperty(Property)
	 * @see org.coolsoftware.ecl.EclPackage#getProvisionClause_ProvidedProperty()
	 * @model required="true"
	 * @generated
	 */
	Property getProvidedProperty();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.ProvisionClause#getProvidedProperty <em>Provided Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided Property</em>' reference.
	 * @see #getProvidedProperty()
	 * @generated
	 */
	void setProvidedProperty(Property value);

	/**
	 * Returns the value of the '<em><b>Min Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Value</em>' containment reference.
	 * @see #setMinValue(Statement)
	 * @see org.coolsoftware.ecl.EclPackage#getProvisionClause_MinValue()
	 * @model containment="true"
	 * @generated
	 */
	Statement getMinValue();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.ProvisionClause#getMinValue <em>Min Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Value</em>' containment reference.
	 * @see #getMinValue()
	 * @generated
	 */
	void setMinValue(Statement value);

	/**
	 * Returns the value of the '<em><b>Max Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Value</em>' containment reference.
	 * @see #setMaxValue(Statement)
	 * @see org.coolsoftware.ecl.EclPackage#getProvisionClause_MaxValue()
	 * @model containment="true"
	 * @generated
	 */
	Statement getMaxValue();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.ProvisionClause#getMaxValue <em>Max Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Value</em>' containment reference.
	 * @see #getMaxValue()
	 * @generated
	 */
	void setMaxValue(Statement value);

	/**
	 * Returns the value of the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formula</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' containment reference.
	 * @see #setFormula(FormulaTemplate)
	 * @see org.coolsoftware.ecl.EclPackage#getProvisionClause_Formula()
	 * @model containment="true"
	 * @generated
	 */
	FormulaTemplate getFormula();

	/**
	 * Sets the value of the '{@link org.coolsoftware.ecl.ProvisionClause#getFormula <em>Formula</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formula</em>' containment reference.
	 * @see #getFormula()
	 * @generated
	 */
	void setFormula(FormulaTemplate value);

} // ProvisionClause
