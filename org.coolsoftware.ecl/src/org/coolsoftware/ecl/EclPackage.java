/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.ecl.EclFactory
 * @model kind="package"
 * @generated
 */
public interface EclPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ecl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.cool-software.org/ecl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ecl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EclPackage eINSTANCE = org.coolsoftware.ecl.impl.EclPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.ImportImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.EclImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.EclImportImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getEclImport()
	 * @generated
	 */
	int ECL_IMPORT = 1;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECL_IMPORT_FEATURE_COUNT = IMPORT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.CcmImportImpl <em>Ccm Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.CcmImportImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getCcmImport()
	 * @generated
	 */
	int CCM_IMPORT = 2;

	/**
	 * The feature id for the '<em><b>Ccm Structural Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_IMPORT__CCM_STRUCTURAL_MODEL = IMPORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ccm Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_IMPORT_FEATURE_COUNT = IMPORT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.EclContractImpl <em>Contract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.EclContractImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getEclContract()
	 * @generated
	 */
	int ECL_CONTRACT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECL_CONTRACT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Metaparams</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECL_CONTRACT__METAPARAMS = 1;

	/**
	 * The number of structural features of the '<em>Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECL_CONTRACT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.SWComponentContractImpl <em>SW Component Contract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.SWComponentContractImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWComponentContract()
	 * @generated
	 */
	int SW_COMPONENT_CONTRACT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_CONTRACT__NAME = ECL_CONTRACT__NAME;

	/**
	 * The feature id for the '<em><b>Metaparams</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_CONTRACT__METAPARAMS = ECL_CONTRACT__METAPARAMS;

	/**
	 * The feature id for the '<em><b>Modes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_CONTRACT__MODES = ECL_CONTRACT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_CONTRACT__COMPONENT_TYPE = ECL_CONTRACT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_CONTRACT__PORT = ECL_CONTRACT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SW Component Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_CONTRACT_FEATURE_COUNT = ECL_CONTRACT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.EclFileImpl <em>File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.EclFileImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getEclFile()
	 * @generated
	 */
	int ECL_FILE = 4;

	/**
	 * The feature id for the '<em><b>Contracts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECL_FILE__CONTRACTS = 0;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECL_FILE__IMPORTS = 1;

	/**
	 * The number of structural features of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ECL_FILE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.ContractModeImpl <em>Contract Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.ContractModeImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getContractMode()
	 * @generated
	 */
	int CONTRACT_MODE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_MODE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Contract Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRACT_MODE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.SWContractClause <em>SW Contract Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.SWContractClause
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWContractClause()
	 * @generated
	 */
	int SW_CONTRACT_CLAUSE = 7;

	/**
	 * The number of structural features of the '<em>SW Contract Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONTRACT_CLAUSE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.ProvisionClauseImpl <em>Provision Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.ProvisionClauseImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getProvisionClause()
	 * @generated
	 */
	int PROVISION_CLAUSE = 6;

	/**
	 * The feature id for the '<em><b>Provided Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVISION_CLAUSE__PROVIDED_PROPERTY = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVISION_CLAUSE__MIN_VALUE = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVISION_CLAUSE__MAX_VALUE = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVISION_CLAUSE__FORMULA = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Provision Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVISION_CLAUSE_FEATURE_COUNT = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 4;


	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.SWComponentRequirementClauseImpl <em>SW Component Requirement Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.SWComponentRequirementClauseImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWComponentRequirementClause()
	 * @generated
	 */
	int SW_COMPONENT_REQUIREMENT_CLAUSE = 9;

	/**
	 * The feature id for the '<em><b>Required Component Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SW Component Requirement Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_REQUIREMENT_CLAUSE_FEATURE_COUNT = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl <em>Property Requirement Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getPropertyRequirementClause()
	 * @generated
	 */
	int PROPERTY_REQUIREMENT_CLAUSE = 10;

	/**
	 * The feature id for the '<em><b>Required Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY = 0;

	/**
	 * The feature id for the '<em><b>Max Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE = 1;

	/**
	 * The feature id for the '<em><b>Min Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE = 2;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_REQUIREMENT_CLAUSE__FORMULA = 3;

	/**
	 * The number of structural features of the '<em>Property Requirement Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_REQUIREMENT_CLAUSE_FEATURE_COUNT = 4;


	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.HWComponentContractImpl <em>HW Component Contract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.HWComponentContractImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWComponentContract()
	 * @generated
	 */
	int HW_COMPONENT_CONTRACT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_CONTRACT__NAME = ECL_CONTRACT__NAME;

	/**
	 * The feature id for the '<em><b>Metaparams</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_CONTRACT__METAPARAMS = ECL_CONTRACT__METAPARAMS;

	/**
	 * The feature id for the '<em><b>Component Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_CONTRACT__COMPONENT_TYPE = ECL_CONTRACT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Modes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_CONTRACT__MODES = ECL_CONTRACT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HW Component Contract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_CONTRACT_FEATURE_COUNT = ECL_CONTRACT_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.HWContractModeImpl <em>HW Contract Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.HWContractModeImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWContractMode()
	 * @generated
	 */
	int HW_CONTRACT_MODE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_CONTRACT_MODE__NAME = CONTRACT_MODE__NAME;

	/**
	 * The feature id for the '<em><b>Clauses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_CONTRACT_MODE__CLAUSES = CONTRACT_MODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>HW Contract Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_CONTRACT_MODE_FEATURE_COUNT = CONTRACT_MODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.SWContractModeImpl <em>SW Contract Mode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.SWContractModeImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWContractMode()
	 * @generated
	 */
	int SW_CONTRACT_MODE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONTRACT_MODE__NAME = CONTRACT_MODE__NAME;

	/**
	 * The feature id for the '<em><b>Clauses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONTRACT_MODE__CLAUSES = CONTRACT_MODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SW Contract Mode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONTRACT_MODE_FEATURE_COUNT = CONTRACT_MODE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.HWContractClause <em>HW Contract Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.HWContractClause
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWContractClause()
	 * @generated
	 */
	int HW_CONTRACT_CLAUSE = 14;

	/**
	 * The number of structural features of the '<em>HW Contract Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_CONTRACT_CLAUSE_FEATURE_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.HWComponentRequirementClauseImpl <em>HW Component Requirement Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.HWComponentRequirementClauseImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWComponentRequirementClause()
	 * @generated
	 */
	int HW_COMPONENT_REQUIREMENT_CLAUSE = 15;

	/**
	 * The feature id for the '<em><b>Required Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES = HW_CONTRACT_CLAUSE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Resource Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE = HW_CONTRACT_CLAUSE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Energy Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE = HW_CONTRACT_CLAUSE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HW Component Requirement Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMPONENT_REQUIREMENT_CLAUSE_FEATURE_COUNT = HW_CONTRACT_CLAUSE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.SelfRequirementClauseImpl <em>Self Requirement Clause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.SelfRequirementClauseImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSelfRequirementClause()
	 * @generated
	 */
	int SELF_REQUIREMENT_CLAUSE = 16;

	/**
	 * The feature id for the '<em><b>Required Property</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Self Requirement Clause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_REQUIREMENT_CLAUSE_FEATURE_COUNT = SW_CONTRACT_CLAUSE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.FormulaTemplateImpl <em>Formula Template</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.FormulaTemplateImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getFormulaTemplate()
	 * @generated
	 */
	int FORMULA_TEMPLATE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_TEMPLATE__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Metaparameter</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_TEMPLATE__METAPARAMETER = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Formula Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_TEMPLATE_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.ecl.impl.MetaparameterImpl <em>Metaparameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.ecl.impl.MetaparameterImpl
	 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getMetaparameter()
	 * @generated
	 */
	int METAPARAMETER = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METAPARAMETER__NAME = VariablesPackage.VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METAPARAMETER__TYPE = VariablesPackage.VARIABLE__TYPE;

	/**
	 * The feature id for the '<em><b>Initial Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METAPARAMETER__INITIAL_EXPRESSION = VariablesPackage.VARIABLE__INITIAL_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METAPARAMETER__VALUE = VariablesPackage.VARIABLE__VALUE;

	/**
	 * The number of structural features of the '<em>Metaparameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METAPARAMETER_FEATURE_COUNT = VariablesPackage.VARIABLE_FEATURE_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see org.coolsoftware.ecl.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.EclImport <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see org.coolsoftware.ecl.EclImport
	 * @generated
	 */
	EClass getEclImport();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.CcmImport <em>Ccm Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Import</em>'.
	 * @see org.coolsoftware.ecl.CcmImport
	 * @generated
	 */
	EClass getCcmImport();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.CcmImport#getCcmStructuralModel <em>Ccm Structural Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ccm Structural Model</em>'.
	 * @see org.coolsoftware.ecl.CcmImport#getCcmStructuralModel()
	 * @see #getCcmImport()
	 * @generated
	 */
	EReference getCcmImport_CcmStructuralModel();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.SWComponentContract <em>SW Component Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Component Contract</em>'.
	 * @see org.coolsoftware.ecl.SWComponentContract
	 * @generated
	 */
	EClass getSWComponentContract();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.SWComponentContract#getModes <em>Modes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Modes</em>'.
	 * @see org.coolsoftware.ecl.SWComponentContract#getModes()
	 * @see #getSWComponentContract()
	 * @generated
	 */
	EReference getSWComponentContract_Modes();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.SWComponentContract#getComponentType <em>Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Type</em>'.
	 * @see org.coolsoftware.ecl.SWComponentContract#getComponentType()
	 * @see #getSWComponentContract()
	 * @generated
	 */
	EReference getSWComponentContract_ComponentType();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.SWComponentContract#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see org.coolsoftware.ecl.SWComponentContract#getPort()
	 * @see #getSWComponentContract()
	 * @generated
	 */
	EReference getSWComponentContract_Port();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.EclFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File</em>'.
	 * @see org.coolsoftware.ecl.EclFile
	 * @generated
	 */
	EClass getEclFile();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.EclFile#getContracts <em>Contracts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contracts</em>'.
	 * @see org.coolsoftware.ecl.EclFile#getContracts()
	 * @see #getEclFile()
	 * @generated
	 */
	EReference getEclFile_Contracts();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.EclFile#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see org.coolsoftware.ecl.EclFile#getImports()
	 * @see #getEclFile()
	 * @generated
	 */
	EReference getEclFile_Imports();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.ContractMode <em>Contract Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract Mode</em>'.
	 * @see org.coolsoftware.ecl.ContractMode
	 * @generated
	 */
	EClass getContractMode();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.ecl.ContractMode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.coolsoftware.ecl.ContractMode#getName()
	 * @see #getContractMode()
	 * @generated
	 */
	EAttribute getContractMode_Name();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.ProvisionClause <em>Provision Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provision Clause</em>'.
	 * @see org.coolsoftware.ecl.ProvisionClause
	 * @generated
	 */
	EClass getProvisionClause();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.ProvisionClause#getProvidedProperty <em>Provided Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Property</em>'.
	 * @see org.coolsoftware.ecl.ProvisionClause#getProvidedProperty()
	 * @see #getProvisionClause()
	 * @generated
	 */
	EReference getProvisionClause_ProvidedProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.ecl.ProvisionClause#getMinValue <em>Min Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Min Value</em>'.
	 * @see org.coolsoftware.ecl.ProvisionClause#getMinValue()
	 * @see #getProvisionClause()
	 * @generated
	 */
	EReference getProvisionClause_MinValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.ecl.ProvisionClause#getMaxValue <em>Max Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Max Value</em>'.
	 * @see org.coolsoftware.ecl.ProvisionClause#getMaxValue()
	 * @see #getProvisionClause()
	 * @generated
	 */
	EReference getProvisionClause_MaxValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.ecl.ProvisionClause#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Formula</em>'.
	 * @see org.coolsoftware.ecl.ProvisionClause#getFormula()
	 * @see #getProvisionClause()
	 * @generated
	 */
	EReference getProvisionClause_Formula();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.SWContractClause <em>SW Contract Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Contract Clause</em>'.
	 * @see org.coolsoftware.ecl.SWContractClause
	 * @generated
	 */
	EClass getSWContractClause();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.EclContract <em>Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contract</em>'.
	 * @see org.coolsoftware.ecl.EclContract
	 * @generated
	 */
	EClass getEclContract();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.ecl.EclContract#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.coolsoftware.ecl.EclContract#getName()
	 * @see #getEclContract()
	 * @generated
	 */
	EAttribute getEclContract_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.EclContract#getMetaparams <em>Metaparams</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Metaparams</em>'.
	 * @see org.coolsoftware.ecl.EclContract#getMetaparams()
	 * @see #getEclContract()
	 * @generated
	 */
	EReference getEclContract_Metaparams();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.SWComponentRequirementClause <em>SW Component Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Component Requirement Clause</em>'.
	 * @see org.coolsoftware.ecl.SWComponentRequirementClause
	 * @generated
	 */
	EClass getSWComponentRequirementClause();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.SWComponentRequirementClause#getRequiredComponentType <em>Required Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Component Type</em>'.
	 * @see org.coolsoftware.ecl.SWComponentRequirementClause#getRequiredComponentType()
	 * @see #getSWComponentRequirementClause()
	 * @generated
	 */
	EReference getSWComponentRequirementClause_RequiredComponentType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.SWComponentRequirementClause#getRequiredProperties <em>Required Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Properties</em>'.
	 * @see org.coolsoftware.ecl.SWComponentRequirementClause#getRequiredProperties()
	 * @see #getSWComponentRequirementClause()
	 * @generated
	 */
	EReference getSWComponentRequirementClause_RequiredProperties();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.PropertyRequirementClause <em>Property Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Requirement Clause</em>'.
	 * @see org.coolsoftware.ecl.PropertyRequirementClause
	 * @generated
	 */
	EClass getPropertyRequirementClause();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.PropertyRequirementClause#getRequiredProperty <em>Required Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Property</em>'.
	 * @see org.coolsoftware.ecl.PropertyRequirementClause#getRequiredProperty()
	 * @see #getPropertyRequirementClause()
	 * @generated
	 */
	EReference getPropertyRequirementClause_RequiredProperty();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.ecl.PropertyRequirementClause#getMaxValue <em>Max Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Max Value</em>'.
	 * @see org.coolsoftware.ecl.PropertyRequirementClause#getMaxValue()
	 * @see #getPropertyRequirementClause()
	 * @generated
	 */
	EReference getPropertyRequirementClause_MaxValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.ecl.PropertyRequirementClause#getMinValue <em>Min Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Min Value</em>'.
	 * @see org.coolsoftware.ecl.PropertyRequirementClause#getMinValue()
	 * @see #getPropertyRequirementClause()
	 * @generated
	 */
	EReference getPropertyRequirementClause_MinValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.ecl.PropertyRequirementClause#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Formula</em>'.
	 * @see org.coolsoftware.ecl.PropertyRequirementClause#getFormula()
	 * @see #getPropertyRequirementClause()
	 * @generated
	 */
	EReference getPropertyRequirementClause_Formula();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.HWComponentContract <em>HW Component Contract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HW Component Contract</em>'.
	 * @see org.coolsoftware.ecl.HWComponentContract
	 * @generated
	 */
	EClass getHWComponentContract();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.HWComponentContract#getComponentType <em>Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Type</em>'.
	 * @see org.coolsoftware.ecl.HWComponentContract#getComponentType()
	 * @see #getHWComponentContract()
	 * @generated
	 */
	EReference getHWComponentContract_ComponentType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.HWComponentContract#getModes <em>Modes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Modes</em>'.
	 * @see org.coolsoftware.ecl.HWComponentContract#getModes()
	 * @see #getHWComponentContract()
	 * @generated
	 */
	EReference getHWComponentContract_Modes();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.HWContractMode <em>HW Contract Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HW Contract Mode</em>'.
	 * @see org.coolsoftware.ecl.HWContractMode
	 * @generated
	 */
	EClass getHWContractMode();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.HWContractMode#getClauses <em>Clauses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Clauses</em>'.
	 * @see org.coolsoftware.ecl.HWContractMode#getClauses()
	 * @see #getHWContractMode()
	 * @generated
	 */
	EReference getHWContractMode_Clauses();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.SWContractMode <em>SW Contract Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Contract Mode</em>'.
	 * @see org.coolsoftware.ecl.SWContractMode
	 * @generated
	 */
	EClass getSWContractMode();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.SWContractMode#getClauses <em>Clauses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Clauses</em>'.
	 * @see org.coolsoftware.ecl.SWContractMode#getClauses()
	 * @see #getSWContractMode()
	 * @generated
	 */
	EReference getSWContractMode_Clauses();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.HWContractClause <em>HW Contract Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HW Contract Clause</em>'.
	 * @see org.coolsoftware.ecl.HWContractClause
	 * @generated
	 */
	EClass getHWContractClause();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.HWComponentRequirementClause <em>HW Component Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HW Component Requirement Clause</em>'.
	 * @see org.coolsoftware.ecl.HWComponentRequirementClause
	 * @generated
	 */
	EClass getHWComponentRequirementClause();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.ecl.HWComponentRequirementClause#getRequiredProperties <em>Required Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Properties</em>'.
	 * @see org.coolsoftware.ecl.HWComponentRequirementClause#getRequiredProperties()
	 * @see #getHWComponentRequirementClause()
	 * @generated
	 */
	EReference getHWComponentRequirementClause_RequiredProperties();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.ecl.HWComponentRequirementClause#getRequiredResourceType <em>Required Resource Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Resource Type</em>'.
	 * @see org.coolsoftware.ecl.HWComponentRequirementClause#getRequiredResourceType()
	 * @see #getHWComponentRequirementClause()
	 * @generated
	 */
	EReference getHWComponentRequirementClause_RequiredResourceType();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.ecl.HWComponentRequirementClause#getEnergyRate <em>Energy Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Energy Rate</em>'.
	 * @see org.coolsoftware.ecl.HWComponentRequirementClause#getEnergyRate()
	 * @see #getHWComponentRequirementClause()
	 * @generated
	 */
	EAttribute getHWComponentRequirementClause_EnergyRate();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.SelfRequirementClause <em>Self Requirement Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Self Requirement Clause</em>'.
	 * @see org.coolsoftware.ecl.SelfRequirementClause
	 * @generated
	 */
	EClass getSelfRequirementClause();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.ecl.SelfRequirementClause#getRequiredProperty <em>Required Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Property</em>'.
	 * @see org.coolsoftware.ecl.SelfRequirementClause#getRequiredProperty()
	 * @see #getSelfRequirementClause()
	 * @generated
	 */
	EReference getSelfRequirementClause_RequiredProperty();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.FormulaTemplate <em>Formula Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula Template</em>'.
	 * @see org.coolsoftware.ecl.FormulaTemplate
	 * @generated
	 */
	EClass getFormulaTemplate();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.ecl.FormulaTemplate#getMetaparameter <em>Metaparameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Metaparameter</em>'.
	 * @see org.coolsoftware.ecl.FormulaTemplate#getMetaparameter()
	 * @see #getFormulaTemplate()
	 * @generated
	 */
	EReference getFormulaTemplate_Metaparameter();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.ecl.Metaparameter <em>Metaparameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Metaparameter</em>'.
	 * @see org.coolsoftware.ecl.Metaparameter
	 * @generated
	 */
	EClass getMetaparameter();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EclFactory getEclFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.ImportImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.EclImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.EclImportImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getEclImport()
		 * @generated
		 */
		EClass ECL_IMPORT = eINSTANCE.getEclImport();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.CcmImportImpl <em>Ccm Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.CcmImportImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getCcmImport()
		 * @generated
		 */
		EClass CCM_IMPORT = eINSTANCE.getCcmImport();

		/**
		 * The meta object literal for the '<em><b>Ccm Structural Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CCM_IMPORT__CCM_STRUCTURAL_MODEL = eINSTANCE.getCcmImport_CcmStructuralModel();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.SWComponentContractImpl <em>SW Component Contract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.SWComponentContractImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWComponentContract()
		 * @generated
		 */
		EClass SW_COMPONENT_CONTRACT = eINSTANCE.getSWComponentContract();

		/**
		 * The meta object literal for the '<em><b>Modes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT_CONTRACT__MODES = eINSTANCE.getSWComponentContract_Modes();

		/**
		 * The meta object literal for the '<em><b>Component Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT_CONTRACT__COMPONENT_TYPE = eINSTANCE.getSWComponentContract_ComponentType();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT_CONTRACT__PORT = eINSTANCE.getSWComponentContract_Port();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.EclFileImpl <em>File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.EclFileImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getEclFile()
		 * @generated
		 */
		EClass ECL_FILE = eINSTANCE.getEclFile();

		/**
		 * The meta object literal for the '<em><b>Contracts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECL_FILE__CONTRACTS = eINSTANCE.getEclFile_Contracts();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECL_FILE__IMPORTS = eINSTANCE.getEclFile_Imports();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.ContractModeImpl <em>Contract Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.ContractModeImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getContractMode()
		 * @generated
		 */
		EClass CONTRACT_MODE = eINSTANCE.getContractMode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTRACT_MODE__NAME = eINSTANCE.getContractMode_Name();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.ProvisionClauseImpl <em>Provision Clause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.ProvisionClauseImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getProvisionClause()
		 * @generated
		 */
		EClass PROVISION_CLAUSE = eINSTANCE.getProvisionClause();

		/**
		 * The meta object literal for the '<em><b>Provided Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVISION_CLAUSE__PROVIDED_PROPERTY = eINSTANCE.getProvisionClause_ProvidedProperty();

		/**
		 * The meta object literal for the '<em><b>Min Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVISION_CLAUSE__MIN_VALUE = eINSTANCE.getProvisionClause_MinValue();

		/**
		 * The meta object literal for the '<em><b>Max Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVISION_CLAUSE__MAX_VALUE = eINSTANCE.getProvisionClause_MaxValue();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVISION_CLAUSE__FORMULA = eINSTANCE.getProvisionClause_Formula();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.SWContractClause <em>SW Contract Clause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.SWContractClause
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWContractClause()
		 * @generated
		 */
		EClass SW_CONTRACT_CLAUSE = eINSTANCE.getSWContractClause();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.EclContractImpl <em>Contract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.EclContractImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getEclContract()
		 * @generated
		 */
		EClass ECL_CONTRACT = eINSTANCE.getEclContract();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ECL_CONTRACT__NAME = eINSTANCE.getEclContract_Name();

		/**
		 * The meta object literal for the '<em><b>Metaparams</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ECL_CONTRACT__METAPARAMS = eINSTANCE.getEclContract_Metaparams();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.SWComponentRequirementClauseImpl <em>SW Component Requirement Clause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.SWComponentRequirementClauseImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWComponentRequirementClause()
		 * @generated
		 */
		EClass SW_COMPONENT_REQUIREMENT_CLAUSE = eINSTANCE.getSWComponentRequirementClause();

		/**
		 * The meta object literal for the '<em><b>Required Component Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_COMPONENT_TYPE = eINSTANCE.getSWComponentRequirementClause_RequiredComponentType();

		/**
		 * The meta object literal for the '<em><b>Required Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES = eINSTANCE.getSWComponentRequirementClause_RequiredProperties();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl <em>Property Requirement Clause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getPropertyRequirementClause()
		 * @generated
		 */
		EClass PROPERTY_REQUIREMENT_CLAUSE = eINSTANCE.getPropertyRequirementClause();

		/**
		 * The meta object literal for the '<em><b>Required Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY = eINSTANCE.getPropertyRequirementClause_RequiredProperty();

		/**
		 * The meta object literal for the '<em><b>Max Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_REQUIREMENT_CLAUSE__MAX_VALUE = eINSTANCE.getPropertyRequirementClause_MaxValue();

		/**
		 * The meta object literal for the '<em><b>Min Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_REQUIREMENT_CLAUSE__MIN_VALUE = eINSTANCE.getPropertyRequirementClause_MinValue();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY_REQUIREMENT_CLAUSE__FORMULA = eINSTANCE.getPropertyRequirementClause_Formula();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.HWComponentContractImpl <em>HW Component Contract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.HWComponentContractImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWComponentContract()
		 * @generated
		 */
		EClass HW_COMPONENT_CONTRACT = eINSTANCE.getHWComponentContract();

		/**
		 * The meta object literal for the '<em><b>Component Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW_COMPONENT_CONTRACT__COMPONENT_TYPE = eINSTANCE.getHWComponentContract_ComponentType();

		/**
		 * The meta object literal for the '<em><b>Modes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW_COMPONENT_CONTRACT__MODES = eINSTANCE.getHWComponentContract_Modes();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.HWContractModeImpl <em>HW Contract Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.HWContractModeImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWContractMode()
		 * @generated
		 */
		EClass HW_CONTRACT_MODE = eINSTANCE.getHWContractMode();

		/**
		 * The meta object literal for the '<em><b>Clauses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW_CONTRACT_MODE__CLAUSES = eINSTANCE.getHWContractMode_Clauses();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.SWContractModeImpl <em>SW Contract Mode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.SWContractModeImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSWContractMode()
		 * @generated
		 */
		EClass SW_CONTRACT_MODE = eINSTANCE.getSWContractMode();

		/**
		 * The meta object literal for the '<em><b>Clauses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_CONTRACT_MODE__CLAUSES = eINSTANCE.getSWContractMode_Clauses();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.HWContractClause <em>HW Contract Clause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.HWContractClause
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWContractClause()
		 * @generated
		 */
		EClass HW_CONTRACT_CLAUSE = eINSTANCE.getHWContractClause();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.HWComponentRequirementClauseImpl <em>HW Component Requirement Clause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.HWComponentRequirementClauseImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getHWComponentRequirementClause()
		 * @generated
		 */
		EClass HW_COMPONENT_REQUIREMENT_CLAUSE = eINSTANCE.getHWComponentRequirementClause();

		/**
		 * The meta object literal for the '<em><b>Required Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_PROPERTIES = eINSTANCE.getHWComponentRequirementClause_RequiredProperties();

		/**
		 * The meta object literal for the '<em><b>Required Resource Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW_COMPONENT_REQUIREMENT_CLAUSE__REQUIRED_RESOURCE_TYPE = eINSTANCE.getHWComponentRequirementClause_RequiredResourceType();

		/**
		 * The meta object literal for the '<em><b>Energy Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW_COMPONENT_REQUIREMENT_CLAUSE__ENERGY_RATE = eINSTANCE.getHWComponentRequirementClause_EnergyRate();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.SelfRequirementClauseImpl <em>Self Requirement Clause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.SelfRequirementClauseImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getSelfRequirementClause()
		 * @generated
		 */
		EClass SELF_REQUIREMENT_CLAUSE = eINSTANCE.getSelfRequirementClause();

		/**
		 * The meta object literal for the '<em><b>Required Property</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELF_REQUIREMENT_CLAUSE__REQUIRED_PROPERTY = eINSTANCE.getSelfRequirementClause_RequiredProperty();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.FormulaTemplateImpl <em>Formula Template</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.FormulaTemplateImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getFormulaTemplate()
		 * @generated
		 */
		EClass FORMULA_TEMPLATE = eINSTANCE.getFormulaTemplate();

		/**
		 * The meta object literal for the '<em><b>Metaparameter</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA_TEMPLATE__METAPARAMETER = eINSTANCE.getFormulaTemplate_Metaparameter();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.ecl.impl.MetaparameterImpl <em>Metaparameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.ecl.impl.MetaparameterImpl
		 * @see org.coolsoftware.ecl.impl.EclPackageImpl#getMetaparameter()
		 * @generated
		 */
		EClass METAPARAMETER = eINSTANCE.getMetaparameter();

	}

} //EclPackage
