/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.ecl;

import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.nameable.NamedElement;

import org.coolsoftware.coolcomponents.types.CcmType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Metaparameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.coolsoftware.ecl.EclPackage#getMetaparameter()
 * @model
 * @generated
 */
public interface Metaparameter extends Variable {

} // Metaparameter
