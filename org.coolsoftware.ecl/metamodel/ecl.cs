SYNTAXDEF ecl
FOR <http://www.cool-software.org/ecl> <ecl.genmodel>
START EclFile

IMPORTS {
	exp : <http://www.cool-software.org/dimm/expressions> 
		<org.coolsoftware.coolcomponents.expressions/metamodel/expressions.genmodel>
		WITH SYNTAX exp <../../org.coolsoftware.coolcomponents.expressions/metamodel/expressions.cs>
}

OPTIONS {
	reloadGeneratorModel = "true";
	//overrideManifest = "false";
	disableDebugSupport = "true";
	disableLaunchSupport = "true";
	useClassicPrinter = "true";
}

RULES {

	// Top level container
	EclFile ::= 
		imports* !0 contracts* !0;
		
	
	// Import Statements
	CcmImport ::= 
		"import" #1 "ccm" #1 ccmStructuralModel['[', ']'] !0;
	
	
	// Contracts
	SWComponentContract ::= 
		"contract" #1 name[TEXT] #1 "implements" #1 "software" #1 componentType[TEXT] "." port[TEXT] "{" !1
			("metaparameter:" (metaparams #1)*)? !1
			(modes !0)+
		"}" !0; 
				
	HWComponentContract ::= 
		"contract" #1 name[TEXT] #1 "implements" #1 "hardware" #1 componentType[TEXT] #1 "{" !1
			("metaparameter:" (metaparams #1)*)? !1
			(modes !0)+
		"}";
		
		
	// Contract Modes	
	SWContractMode ::= 
		"mode" #1 name[TEXT] #1 "{" !1
			(clauses !0)+
		"}";
		
	HWContractMode ::= 
		"mode" #1 name[TEXT] #1 "{" !1
			(clauses !0)+
		"}";


	// Provision Clauses
	ProvisionClause ::= 
		"provides" #1 providedProperty[TEXT] #1
			("min:" #1 minValue:exp.Expression #1)? ("max:" #1 maxValue:exp.Expression #1)? formula?;
		
		
	// Requirement Clauses	
	SWComponentRequirementClause ::= 
		"requires" #1 "component" #1 requiredComponentType[TEXT] #1 (("{" !1 (requiredProperties !0)* "}") | ";");
		
	HWComponentRequirementClause ::= 
		"requires" #1 "resource" #1 requiredResourceType[TEXT] #1 "{" !1
			(requiredProperties !0)+ !1
			"energyRate: " energyRate[REAL_LITERAL] !1
		"}";
		
	SelfRequirementClause ::=
		"requires" #1 requiredProperty;

	PropertyRequirementClause ::=
		requiredProperty[TEXT] #1
			("min:" #1 minValue:exp.Expression #1)? ("max:" #1 maxValue:exp.Expression #1)? formula?;
	
	FormulaTemplate ::= "<" name[TEXT] "(" (metaparameter[] #1)* ")>";
	
	Metaparameter ::= name[TEXT];

}