/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.test.transcoder;

import org.haec.app.transcoder.ffmpeg.TranscoderFfmpeg;

/**
 * Transcoder test for ffmpeg.
 * @author René Schöne
 */
public class TestFfmpeg extends AbstractTranscoderTest {
	
	public TestFfmpeg() {
		super(new TranscoderFfmpeg() {
			@Override
			protected <S> S invoke(Class<S> clazz, String compName,
					String portName, Object... params) {
				return clazz.cast(params[0]);
			}
		});
	}

}
