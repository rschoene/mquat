/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.test.transcoder;

import java.io.File;
import java.util.List;

import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.utils.FileProxy;
import org.haec.videos.Video;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Abstract Test class testing different video and audio codecs as transcoding targets.
 * @author René Schöne
 */
public class AbstractTranscoderTest {
	
	public static final boolean constrainList = Boolean.getBoolean("constrainList");
	protected TranscoderTranscode t;
	static protected FileProxy fp1;
	static List<String> vCodecs = TranscodingServerUtil.getSupportedVideoCodecs();
	static List<String> aCodecs = TranscodingServerUtil.getSupportedAudioCodecs();
	
	public AbstractTranscoderTest(TranscoderTranscode toUse) {
		t = toUse;
	}
	
	@BeforeClass
	public static void constrainCodecs() {
		if(constrainList) {
			// only use first
			vCodecs = vCodecs.subList(0, 1);
			aCodecs = aCodecs.subList(0, 1);
		}
		// set file to use
//		File file = firstNonEmpty(VideoProviderFactory.getCurrentVideoProvider().getVideos()).get(0).file;
		File file = new File("/home/rschoene/Videos/Route66_0_1.avi");
		fp1 = new FileProxy(file);
	}
	
	protected static List<Video> firstNonEmpty(Iterable<List<Video>> videos) {
		for(List<Video> list : videos) {
			if(list != null && !list.isEmpty()) { return list; }
		}
		return null;
	}

	@Test
	public void onlyVideo() {
		for(String vCodec : vCodecs) {
			System.out.println(vCodec);
			File f = t.transcode(fp1, vCodec, null).getFile();
			assertNotEquals("File is empty for -vc " + vCodec, 0, f.length());
		}
	}

	@Test
	public void onlyAudio() {
		for(String aCodec : aCodecs) {
			File f = t.transcode(fp1, null, aCodec).getFile();
			assertNotEquals("File is empty for -ac " + aCodec, 0, f.length());
		}
	}

//	@Test
	public void bothVideoAndAudio() {
		for(String aCodec : aCodecs) {
			for(String vCodec : vCodecs) {
				File f = t.transcode(fp1, vCodec, aCodec).getFile();
				assertNotEquals("File is empty for -vc " + vCodec + " and -ac " + aCodec, 0, f.length());
			}
		}
	}

}
