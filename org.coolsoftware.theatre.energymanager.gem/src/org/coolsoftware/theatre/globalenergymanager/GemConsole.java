/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.globalenergymanager;

import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;
import ch.ethz.iks.r_osgi.URI;

/**
 * Default application starting off the GEM. To be revised.
 * @author Sebastian Götz
 */
public class GemConsole implements IApplication {

	@Override
	public Object start(IApplicationContext context) throws Exception {
		Bundle bundle = org.osgi.framework.FrameworkUtil.getBundle(this.getClass());
		BundleContext bcontext = bundle.getBundleContext();
		
		ServiceReference<?> remoteRef = bcontext.getServiceReference(RemoteOSGiService.class.getName());
		if(remoteRef == null) {
			 System.out.println("Error: R-OSGi not found!");
		}
		RemoteOSGiService remote = (RemoteOSGiService)bcontext.getService(remoteRef);
		final RemoteServiceReference[] srefs =
			remote.getRemoteServiceReferences(new URI("r-osgi://127.0.0.1:9278"),
				IGlobalEnergyManager.class.getName(), null);
		
		for(RemoteServiceReference ref : srefs)
			System.out.println("service ref: "+ref);
		
		IGlobalEnergyManager gem = (IGlobalEnergyManager)remote.getRemoteService(srefs[0]);
		
		gem.optimize(null, false);
		
		String app = "org.coolsoftware.app.video";
		
		System.out.println("[GEMConsole] Components and Properties of "+app+":");
		for(String c : gem.getComponentsOfApp(app)) {
			System.out.println("- "+c);
			for(String prop : gem.getComponentProperties(app, c)) {
				System.out.println("  # "+prop);
			}
		}
		
		return null;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

}
