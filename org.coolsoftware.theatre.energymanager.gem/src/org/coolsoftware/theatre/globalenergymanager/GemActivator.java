/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.globalenergymanager;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IComponentRepository;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.reconfiguration.ComponentRepository;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
//import org.haec.theatre.benchmark.IBenchmarkUtil;
//import org.haec.theatre.benchmark.LocalBenchmarkUtil;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * Activator to set up everything except the GEM.
 * @author Sebastian Götz
 * @author René Schöne
 * @see IGlobalEnergyManager
 */
public class GemActivator implements BundleActivator {
	
	private static BundleContext context;
	private static IComponentRepository rep;
//	private static IBenchmarkUtil benchUtilGem;
	private static Logger log = Logger.getLogger(GemActivator.class);

//	public static BundleContext getContext() {
//		return context;
//	}
	
//	public static IGlobalEnergyManager getGem() {
//		return gem;
//	}
//	
	public static IComponentRepository getRepository() {
		return rep;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		GemActivator.context = bundleContext;
		GemActivator.rep = new ComponentRepository();

//		benchUtilGem = new LocalBenchmarkUtil();
		
		Hashtable<String, Boolean> props = new Hashtable<String, Boolean>();
		props.put(RemoteOSGiService.R_OSGi_REGISTRATION, Boolean.TRUE);
		
		context.registerService(IComponentRepository.class.getName(), rep, props);
		log.debug("IComponentRepository Service registered (for remote access)");
		
//		context.registerService(IBenchmarkUtil.class.getName(), benchUtilGem, props);
//		log.debug("BenchmarkUtil Service registered (for remote access)");
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		GemActivator.context = null;
	}

	/*
	public static ILocalEnergyManager getLem(String uri) {
		ServiceReference<?> remoteRef = context.getServiceReference(RemoteOSGiService.class.getName());
		if (remoteRef == null) {
			log.warn("Error: R-OSGi not found!");
		}
		RemoteOSGiService remote = (RemoteOSGiService) context.getService(remoteRef);
		final RemoteServiceReference[] srefs = remote.getRemoteServiceReferences(new ch.ethz.iks.r_osgi.URI(uri), ILocalEnergyManager.class.getName(), null);

		ILocalEnergyManager lem = (ILocalEnergyManager) remote.getRemoteService(srefs[0]);

		return lem;
	}
	
	public static IGlobalEnergyManager getGlobalEnergyManager() throws IOException {
		RemoteOSGiService rosgi = getROSGiService(context);
		
		//HERE THEATRE needs to intervene - (which) impl of IVideoServer on which server to use?
		//gem.optimize(request) will reconfigure the system, but here we need a hard link for the entry point..
		RemoteServiceReference[] gemRefs = rosgi
				.getRemoteServiceReferences(RemoteOsgiUtil.createRemoteOsgiUri(AppUtil.getGEMUri()),
						IGlobalEnergyManager.class.getName(), null);
		IGlobalEnergyManager gem = (IGlobalEnergyManager)rosgi.getRemoteService(gemRefs[0]);
		return gem;
	}
	
	private static RemoteOSGiService getROSGiService(BundleContext context)
			throws IOException {
		// get the RemoteOSGiService
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);

		// connect
		remote.connect(RemoteOsgiUtil.createRemoteOsgiUri(AppUtil.getGEMUri()));

		return remote;
	}
	 */

}
