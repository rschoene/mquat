/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.globalenergymanager;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.jar.Manifest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.coolsoftware.coolcomponents.ccm.ParameterDirection;
import org.coolsoftware.coolcomponents.ccm.structure.CopyFileType;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.SWConnectorType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.Job;
import org.coolsoftware.coolcomponents.ccm.variant.JobStatus;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;
import org.coolsoftware.coolcomponents.ccm.variant.Schedule;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.reconfiguration.ReconfigurationPlan;
import org.coolsoftware.requests.Import;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.RequestsFactory;
import org.coolsoftware.theatre.PowerManager;
import org.coolsoftware.theatre.TheatreActivator;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.IJobPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.IJobResultNotifier;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.coolsoftware.theatre.energymanager.ITaskPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.ITaskResultNotifier;
import org.coolsoftware.theatre.energymanager.JobDescription;
import org.coolsoftware.theatre.energymanager.JobResultDescription;
import org.coolsoftware.theatre.energymanager.Optimizer;
import org.coolsoftware.theatre.energymanager.ResultDescription;
import org.coolsoftware.theatre.energymanager.TaskDescription;
import org.coolsoftware.theatre.energymanager.TaskResultDescription;
import org.coolsoftware.theatre.energymanager.Optimizer.Mapping;
import org.coolsoftware.theatre.energymanager.Optimizer.TimeInfo;
import org.coolsoftware.theatre.energymanager.ReconfigurationException;
import org.coolsoftware.theatre.energymanager.util.AsyncExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ErrorResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResultSerializer;
import org.coolsoftware.theatre.energymanager.util.PreOptimizeRunner;
import org.coolsoftware.theatre.energymanager.util.PreOptimizeRunner.RequestAndParams;
import org.coolsoftware.theatre.energymanager.util.SyncExecuteResult;
import org.coolsoftware.theatre.reconfiguration.ReconfigurationManager;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.JarUtils;
import org.coolsoftware.theatre.util.Persistator;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.haec.theatre.dao.ImplementationDAO;
import org.haec.theatre.dao.JobDAO;
import org.haec.theatre.dao.TaskDAO;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.task.ITask;
import org.haec.theatre.task.repository.ITaskRepository;
import org.haec.theatre.utils.Cache;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.ResourceResolver;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.AsyncRemoteCallCallback;
import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;
/**
 * The standard implementation of the GEM using r-OSGi.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class OsgiGem implements IGlobalEnergyManager {
	class StringWrapper {
		String s;
	}
	class MappingInfo {
		String appName; StringWrapper implName;
		String portName; Object[] newParams; String compName;
		VariantModel infraVM; Mapping mapping;
		String selectedImplOfRequestedComponent; String currentContainer;
		long calledJobId; long taskId; ITask task;
		public MappingInfo(String appName, StringWrapper implName,
				String portName, Object[] newParams, String compName,
				VariantModel infraVM, Mapping mapping,
				String selectedImplOfRequestedComponent,
				String currentContainer, long calledJobId, long taskId,
				ITask task) {
			super();
			this.appName = appName;
			this.implName = implName;
			this.portName = portName;
			this.newParams = newParams;
			this.compName = compName;
			this.infraVM = infraVM;
			this.mapping = mapping;
			this.selectedImplOfRequestedComponent = selectedImplOfRequestedComponent;
			this.currentContainer = currentContainer;
			this.calledJobId = calledJobId;
			this.taskId = taskId;
			this.task = task;
		}
	}

	public MappingInfo lastMapping;

	private final class OptimizeAndRun implements Callable<Serializable> {
		private Object[] params;
		private final long taskId;
		private final ITask task;
		private String userRequest;

		private OptimizeAndRun(Object[] params, long taskId, ITask task,
				String userRequest) {
			this.params = params; this.taskId = taskId;
			this.task = task; this.userRequest = userRequest;
		}

		@Override
		public Serializable call() throws Exception {
			RequestAndParams rap = new RequestAndParams();
			rap.params = this.params;
			rap.userRequest = this.userRequest;
			if(callPreOptimizer(rap)) {
				this.params = rap.params;
				this.userRequest = rap.userRequest;
			}
			String[] reqData = userRequest.split(MAIN_DELIMITER);
			log.debug("reqData: " + Arrays.toString(reqData));
			String appName = null;
			StringWrapper implName = new StringWrapper();
			String portName = null;
			try {
				if(reqData.length > 1) {
					appName = reqData[1];
				}
				else {
					throw new IllegalArgumentException("Could not find appName in request.");
				}
				log.debug(userRequest);
				log.info("Starting optimization");

				if (userRequest == null) {
					throw new NullPointerException("UserRequest is null.");
				}
				if(settings.getGemFailAtNoLemConnected().get() && localMgrs.size() == 0) {
					// no slaves connected, fail fast, because Optimize will fail anyway
					throw new IllegalStateException("No slaves connected.");
				}

				//			String[] fileNames;
				//			List<String> fileNamesTmp = new ArrayList<>();
				Map<String, List<String>> filenameMap = new HashMap<>();
				Object[] newParams = resolveResources(params, filenameMap);
				params = newParams;
				//			fileNames = fileNamesTmp.toArray(new String[0]);
				//			log.debug("FileNames = " + Arrays.toString(fileNames));
				log.debug("FilenameMap: " + filenameMap);

				String compName = reqData[2];
				portName = reqData[3];

				String mpName = ""; 
				String mpVal  = "";
				if(reqData.length > 5) { 
					String[] mp = reqData[5].split("=");
					if(mp.length==2) {
						mpName = mp[0];
						mpVal = mp[1];
					}
				}

				acquireReadLock();
				StructuralModel appModel;
				try {
					appModel = appModelsResolved.get(appName);
				} finally {
					releaseReadLock();
				}
				if (grm == null) {
					throw new IllegalStateException("global resource manager not available!");
				}
//				log.debug(getAppInfo(appName));
				String serializedStructModel = grm.getInfrastructureTypes();
				log.debug("serializedStructModel="+StringUtils.cap(serializedStructModel, 400));
				String serializedVarModel = grm.getInfrastructure();
				log.debug("using serializedVarModel="+StringUtils.cap(serializedVarModel, 400));
				VariantModel infraVM = CCMUtil.deserializeGlobalHWVariantModel(
						serializedStructModel, serializedVarModel, false);

				log.debug("infraVM="+StringUtils.cap(infraVM, 400));
				log.debug("Application: " + appName);
				if(appModel == null || appModel.getRoot() == null) {
					throw new IllegalStateException("App model is null for " + appName);
				}
				log.debug("App Model: " + appModel.getRoot().getName());

				log.debug("User Request: "+userRequest);

				Request req = createRequest(reqData, compName, portName,
						appModel, infraVM, mpName, mpVal);

				// invoke contract negotiation
				Set<StructuralModel> visiModels = new HashSet<StructuralModel>();
				visiModels.add(appModel);
				StructuralModel serverStruct = hwSmCache.get(serializedStructModel);
				visiModels.add(serverStruct);
				String uri = req.getComponent().getEclUri();
				int endIndex = uri.lastIndexOf(".");
				if(endIndex == -1)
					endIndex = uri.length();
				String eclId = uri.substring(uri.lastIndexOf("/") + 1,
						endIndex);
				log.debug("eclId="+eclId);

				Map<String,String> lemContractsText = getContractsForComponent(appName, compName);
				if(log.isDebugEnabled()) {
					log.debug("lemContractsText=" + StringUtils.cap(lemContractsText.keySet(), 1000));
				}

				Map<String, EclFile> lemContracts = new HashMap<String, EclFile>();

				for(String lemURI : lemContractsText.keySet()) {
					String lemContract = lemContractsText.get(lemURI);
					if(lemContract == null) {
						log.warn(String.format("Got no contract for %s on %s. Skipping.", compName, lemURI));
						continue;
					}
					EclFile eclFile = CCMUtil.deserializeECLContract(appModel,
							serverStruct, appName, eclId, lemContract, false);
					// strip r-osgi part from URI
					lemURI = RemoteOsgiUtil.stripRosgiParts(lemURI);
					if(!settings.getNoLocalManagers().get()) {
						// master and slave in same instance
						// use vm-container-ip as uri
						Resource root = (Resource) infraVM.getRoot();
						String containerName = root.getSubresources().get(0).getName();
						lemURI = containerName;
					}
					lemContracts.put(lemURI, eclFile);
				}
				log.debug("Got contracts for " +lemContracts.size() + " hosts.");

				// use first lemContract found, else use contract at gem
				EclFile eclFile;
				if(!lemContracts.isEmpty()) {
					// fetch first contract
					String firstLemUri = lemContracts.keySet().iterator().next();
					log.debug("Using contract of " + firstLemUri);
					eclFile = lemContracts.get(firstLemUri);
				}
				else {
					// fetch gem contract of component
					log.debug("Using contract of gem, i.e. " + getUri());
					String entryContract = getContractForComponentInContainer(appName, compName,
							RemoteOsgiUtil.createRemoteOsgiUri(getUri()).toString());
					if(entryContract == null) {
						throw new NullPointerException("Got empty contract. Exiting.");
					}
					eclFile = CCMUtil.deserializeECLContract(appModel,
							serverStruct, appName, eclId, entryContract, false);
				}

				if(settings.getGemSaveVisData().get() && persistator != null) {
					try {
						boolean success = persistator.persist(req);
						log.debug("Persisting request " + (success ? "successful" : "failed"));
						success = persistator.combineAndPersist(eclFile, req);
						log.debug("Persisting ecl file " + (success ? "successful" : "failed"));
					} catch (Throwable t) {
						log.warn("Error during persisting of request and ecl file", t);
					}
				}

				List<String> implsOfRequestedComponentType = new ArrayList<String>();
				for(EclContract ctr : eclFile.getContracts()) {
					implsOfRequestedComponentType.add(ctr.getName().replace("_","."));
				}
				log.debug("implsOfRequestedComponentType="+implsOfRequestedComponentType);

				String profile = getProfileFromRequest(reqData);
				Map<String, Object> options = new HashMap<>();
				options.put(Optimizer.OPTION_APPNAME, appName);
				options.put(Optimizer.OPTION_RESOURCENAMES, filenameMap);
				options.put(Optimizer.OPTION_VERBOSE, true);
				options.put(Optimizer.OPTION_PROFILE, profile);
				options.put(Optimizer.OPTION_PROBLEMNAME, compName + "_" + portName);
				Mapping mapping;
				if(settings.getGemFakeMapping().get()) {
					mapping = new Mapping();
					String scal = "org.haec.app.scaling.handbrake.ScalingHandbrake";
					String getFile = "org.haec.app.copyFile.scaling.CopyFileScalingImpl";
					mapping.put(scal, "r-osgi://192.168.0.211:9278");
					mapping.put(getFile, "");
					mapping.setStartTime(scal, 0);
					mapping.setRunTime(scal, 257.73);
					mapping.setResponseTime(scal, 257.73);
					mapping.setResponseTime(getFile, 1.0);
				} else {
					mapping = doOptimization(reqData[0], req, lemContracts, options);
				}
				if(mapping != null && mapping.hasErrors()) {
					throw(new IllegalStateException(mapping.getErrorMessage()));
				}

				if(mapping == null || mapping.size() == 0) {
					// no mapping found.
					if(req.getReqs().isEmpty()) {
						// there were no requirements. Give up.
						throw(new NullPointerException("Did not find a mapping (maybe because of a thrown exception)"));
					}
					else if(settings.getGemTryGracefulDegradation().get()){
						// there were requirements. Do "graceful degradation"
						// Naive approach: Remove requirements, do optimization again.
						log.info("Did not find a mapping. Doing graceful degradation by removing requirements.");
						req.getReqs().clear();
						mapping = doOptimization(reqData[0], req, lemContracts, options);
						if(mapping == null || mapping.size() == 0) {
							throw(new NullPointerException("Did not find a mapping even with graceful degradation"));
						}
					}
					else {
						throw(new NullPointerException("Did not find a mapping and disabled graceful degradation"));
					}
				}
				if(mapping.hasErrors()) {
					throw(new IllegalStateException(mapping.getErrorMessage()));
				}

				String selectedImplOfRequestedComponent = null;

//				changeMappingManually(mapping);

				for(Entry<String, String> e : mapping.entrySet()) {
					e.setValue(matchWithLocalMgrs(e.getValue()).toString());
//					e.setValue(RemoteOsgiUtil.createRemoteOsgiUriString(e.getValue()));
					String impl = e.getKey();
					if(implsOfRequestedComponentType.contains(impl.replace("_", "."))) {
						selectedImplOfRequestedComponent = impl;
					}
				}
				log.info("mapping="+mapping);

				//start app now by fetching container and sending execute command
				log.debug(String.format("selectedImplOfRequestedComponent=%s", selectedImplOfRequestedComponent));
				if(selectedImplOfRequestedComponent == null) {
					selectedImplOfRequestedComponent = implsOfRequestedComponentType.get(0);
					log.debug("fixing selectedImplOfRequestedComponent to " + selectedImplOfRequestedComponent);
				}

				String currentContainer = mapping.get(selectedImplOfRequestedComponent);
				log.debug("currentContainer="+currentContainer);

				long calledJobId = -1;
				lastMapping = new MappingInfo(appName, implName, portName, newParams, compName, infraVM,
						mapping, selectedImplOfRequestedComponent, currentContainer, calledJobId, taskId, task);
				return runMapping(lastMapping);
			} catch (Exception e) {
				// log and rethrow exception
				log.error("Exception during optimize:", e);
				// create taskFinished event with no success
				callTaskResultNotifiers(false, e, appName, implName.s, portName, taskId);
				throw e;
			}
		}

		private String getProfileFromRequest(String[] reqData) {
			if(reqData.length > 6) {
				String otherParams = reqData[6];
				for(String elem : StringUtils.tokenize(otherParams, ELEMENT_DELIMITER)) {
					int equalsIndex = elem.indexOf("=");
					if(equalsIndex == -1) { /* try with colon */ equalsIndex = elem.indexOf(":"); }
					if(equalsIndex > 0 && equalsIndex < elem.length() - 1) {
						String key = elem.substring(0, equalsIndex-1);
						if(key.trim().equals(SETTINGS_PROFILE)) {
							String p = elem.substring(equalsIndex+1);
							switch(p) {
							case Optimizer.PROFILE_ENERGY_SAVING: //$FALL-THROUGH$
							case Optimizer.PROFILE_NORMAL: //$FALL-THROUGH$
							case Optimizer.PROFILE_UTILITY: return p;
							}
							log.warn("Ignoring unknown profile " + p);
						}
					}
					else {
						log.warn("Illegal, thus ignored option " + elem);
					}
				}
			}
			// use default
			log.debug("Using normal profile as default");
			return Optimizer.PROFILE_NORMAL;
		}
	}

	/** Information class (appName, [Types]) */
	class App {
		private String appName;
		private List<Type> types;
		public App(String appName) {
			super();
			this.appName = appName;
			this.types = new ArrayList<OsgiGem.Type>();		
		}
		public void addType(Type t) {
			types.add(t);
		}
		public List<Type> getTypes() {
			return types;
		}
		/** @return {container -> Type}, where <code>Type.getTypeName.equals(name)</code> */
		public Map<String,Type> getTypesByName(String name) {
			Map<String,Type> ret = new HashMap<String, OsgiGem.Type>();
			for(Type t : types) {
				if(t.getTypeName().equals(name)) ret.put(t.getContainer(), t);
			}
			return ret;
		}
		/** @return Type t, where <code>t.getTypeName().equals(name) && t.getContainer().equals(lemURI)</code> */
		public Type getTypeByNameAndContainer(String name, String lemURI) {
			for(Type t : types) {
				if(t.getTypeName().equals(name) && t.getContainer().equals(lemURI)) return t;
			}
			return null;
		}
		public String getAppName() {
			return appName;
		}
		public void setAppName(String appName) {
			this.appName = appName;
		}
		/** @return {implName -> Type}, where 
		 * <code>Type.impls.contains(impl) && impl.getImplName.equals(implName) for all implNames</code> */
		public Map<String, Type> getTypeByImplName(String... implNames) {
			Map<String, Type> result = new HashMap<String, Type>();
			for(String implName : implNames) {
				for(Type t : types) {
					if(t.getImplByName(implName) != null) {
						result.put(implName, t);
					}
				}
				if(!result.containsKey(implName)) {
					log.debug(implName + " not found");
				}
				
			}
			return result;
		}
	}

	/** Information class (typeName, contract, [Impls], container)  */
	class Type {
		private String typeName;
		private String contract;
		private List<Impl> impls;
		private String container;
		private boolean isConnector;
		private boolean isCopyFile;
		
		public Type(String typeName, String contract) {
			super();
			this.typeName = typeName;
			this.contract = contract;
			this.impls = new ArrayList<OsgiGem.Impl>();
			isConnector = false;
			isCopyFile = false;
		}
		public void addImpl(Impl impl) {
			this.impls.add(impl);
		}
		public String getTypeName() {
			return typeName;
		}
		public void setTypeName(String typeName) {
			this.typeName = typeName;
		}
		public String getContract() {
			return contract;
		}
		public void setContract(String contract) {
			this.contract = contract;
		}
		public List<Impl> getImpls() {
			return impls;
		}
		/** @return first impl matching given name, or <code>null</code> if not found */
		public Impl getImplByName(String name) {
			for(Impl i : impls) {
				if(i.getImplName().equals(name) || i.getPluginName().equals(name)) { return i; }
			}
			return null;
		}
		
		public String getContainer() {
			return container;
		}
		public void setContainer(String container) {
			this.container = container;
		}
	}

	/** Information class  (implName, benchName, finished?, pluginName) */
	class Impl {
		private String implName;
		private String benchName;
		private boolean finished;
		private String pluginName;
		
		public Impl(String implName, String benchName, boolean finished) {
			super();
			this.implName = implName;
			this.benchName = benchName;
			this.finished = finished;
		}
		public String getImplName() {
			return implName;
		}
		public void setImplName(String implName) {
			this.implName = implName;
		}
		public String getBenchName() {
			return benchName;
		}
		public void setBenchName(String benchName) {
			this.benchName = benchName;
		}
		public boolean isFinished() {
			return finished;
		}
		public void setFinished(boolean finished) {
			this.finished = finished;
		}
		public String getPluginName() {
			return pluginName;
		}
		public void setPluginName(String pluginName) {
			this.pluginName = pluginName;
		}	
	}

	private final Logger log = Logger.getLogger(OsgiGem.class);

	private String uri;
	
	/** Locks {@link #appModels}, {@link #appContracts}, {@link #appConfigurations}, {@link #appLocations},
	 * {@link #appModelsResolved} */
	private final ReadWriteLock appLock;
	
	/** appName - structuralModelSw
	 * @lockedBy {@link #appLock} */
	private Map<String, String> appModels; // String value represents structure
											// model
	/** appName - lemUri
	 * @lockedBy {@link #appLock} */
	private Map<String, URI> appLocations; // String value represents structure
											// model

	/** appName - typeName - contract - impl - LEM 
	 * @lockedBy {@link #appLock} */
	private Map<String, App> appContracts;
	
	/** appName - SW_Variant_Model
	 * @lockedBy {@link #appLock} */
	private Map<String, VariantModel> appConfigurations;

	/** registered LEMs */
	private List<URI> localMgrs;

	/** registered and ready LEMs */
	private List<URI> localReadyMgrs;

	/** The global resource manager */
	private IGlobalResourceManager grm;

	/** appName - SW_structure_model
	 * @lockedBy {@link #appLock} */
	private Map<String, StructuralModel> appModelsResolved;
	
//	/** comptype - implname - benchname */
//	private Map<String, Map<String, String>> typeImplBench;

	/** lemURI - list of benchmarks (represented as parameters of {@link #asyncComputeContractAtLEM(URI, Object[])}) */
	private Map<URI, List<Object[]>> scheduledBenchmarks;

	/** lock of {@link #scheduledBenchmarks} */
	private Lock benchmarkLock; 
	
	/** condition fired at the end of {@link #asyncComputeContractAtLEM(URI, Object[])}.
	 * Used for waiting at {@link #waitForCurrentBenchmarksToFinish()} */
	private Condition benchmarkFinished;
	
	/** Use the database connection or not. */
	private boolean useDatabase;
	/** Connection index to dexter with jdbc driver */
//	private int connectionIndex;
	
	private Cache<String, StructuralModel> hwSmCache = new Cache<>(new Function<String, StructuralModel>() {

		@Override
		public StructuralModel apply(String s) {
			try {
				return CCMUtil.deserializeHWStructModel(s, false);
			} catch (IOException e) {
				log.warn("Could not deserialize hw-sm", e);
			}
			return null;
		}
	}, false);

	// private boolean useILP = false;

	public OsgiGem() {
		appLock = new ReentrantReadWriteLock(true);
		localMgrs = new ArrayList<URI>();
		localReadyMgrs = new ArrayList<URI>();
		appContracts = new HashMap<String, App>();
		appConfigurations = new HashMap<String, VariantModel>();
		appLocations = new HashMap<String, URI>();
		appModels = new HashMap<String, String>();
		appModelsResolved = new HashMap<String, StructuralModel>();
		scheduledBenchmarks = new HashMap<URI, List<Object[]>>();
//		typeImplBench = new HashMap<String, Map<String,String>>();
		results = new TreeMap<>();
		benchmarkLock = new ReentrantLock();
		benchmarkFinished = benchmarkLock.newCondition();
	}

	/** For testing purposes only. Manually change the optimal mapping */
	protected void changeMappingManually(Mapping mapping) {
		if(mapping == null) { return; }
		if(!settings.getGemTestChangeMapping().get()) { return; }
		// chenk for cpScaling
//		String implName = "org.haec.app.copyFile.scaling.CopyFileScalingImpl";
		String implName = "org.haec.app.scaling.handbrake.ScalingHandbrake";
		String host = mapping.get(implName);
		if(host == null) {
			log.warn("Could not find "+implName+" in mapping.");
			return;
		}
		String newHost;
		switch(host) {
		case "192.168.0.211": newHost = "192.168.0.210"; break;
		case "192.168.0.210": //$FALL-THROUGH$
		default: newHost = "192.168.0.211";
		}
		mapping.put(implName, newHost);
		log.info("Changed host for "+implName+" from "+host+" to "+newHost);
	}

//	private String handleIPProblems(String ip) {
//		if(ip.startsWith("192.168.") || ip.startsWith("169.")) { //unNAT!
//			ip = RemoteOsgiUtil.getExternalIp();
//		} 
//		return ip;
//	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public List<URI> getLocalMgrs() {
		return localMgrs;
	}

	@Override
	public boolean registerLEM(URI uri) {
		initMDC(uri);
		try {
			if(localMgrs.contains(uri)) {
				// reconnect of LEM
				tryUnregisterExistingLEM(uri);
				getLocalEnergyManager(uri);
				log.info("LEM with uri = "+uriAndHostname(uri)+" reconnected.");
			}
			else {
				ILocalEnergyManager lem = getLocalEnergyManager(uri);
				if (lem == null)
					throw new Exception("Invalid LEM");
				localMgrs.add(uri);
				log.debug("successfully registered LEM: " + uri+" - now start benchmarkings applications");
			}
		} catch (Exception e) {
			log.error("Error: LEM with uri = "+uri+" could not be registered. ", e);
			return false;
		}
		if(settings.getGemAppsForceRetrieval().get()) {
			acquireReadLock();
			try {
				for(String appName : appModels.keySet()) {
					getLocalEnergyManager(uri).retrieveAndStartBundle(appName.toLowerCase());
					for(Type type : appContracts.get(appName).getTypes()) {
						for(Impl i : type.getImpls()) {
							getLocalEnergyManager(uri).retrieveAndStartBundle(
									appName.toLowerCase()+"."+i.implName.toLowerCase());
						}				
					}
				}
			} catch(Exception e) {
				log.warn("Exception while starting bundles", e);
				//maybe return false?
				//return false;
			}
			finally {
				releaseReadLock();
			}
		}
		return true;
	}

	private void initMDC(URI uri) {
		String ip = uri.getHost();
		String hostname;
		if(ip != null) {
			hostname = PlatformUtils.getHostNameOf(ip);
		}
		else {
			hostname = uri.getPath();
		}
		if(hostname != null) {
			initMDC(hostname);
		}
		else {
			log.debug("Could not get hostname for uri: " + uri);
		}
	}

	private void initMDC(String hostname) {
		MDC.put(TheatreActivator.KEY_HOSTNAME, hostname);
	}

	private void tryUnregisterExistingLEM(URI uri) {
		RemoteOSGiService remote = getRemoteOSGiService();
		final RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
				remote, uri.toString(), ILocalEnergyManager.class.getName());
		if(srefs == null || srefs.length == 0) {
			// no LEM to unregister
			return;
		}
		for(RemoteServiceReference ref : srefs) {
			try {
				remote.ungetRemoteService(ref);
				log.debug("Ungetted " + ref);
			} catch (Exception e) {
				log.debug("ingored error while ungetting " + ref, e);
			}
		}
	}

	/** Fetches context of this bundle and return RemoeOSGiServer */
	protected RemoteOSGiService getRemoteOSGiService() {
		return RemoteOsgiUtil.getRemoteOSGiService(FrameworkUtil.getBundle(
				this.getClass()).getBundleContext());
	}

	/** Uses r-osgi to get reference of LEM with given URI */
	private ILocalEnergyManager getLocalEnergyManager(URI container) {
		RemoteOSGiService remote = getRemoteOSGiService();
		ILocalEnergyManager lem = RemoteOsgiUtil.getRemoteService(remote, container.toString(), ILocalEnergyManager.class);
		if(lem == null) {
			log.warn("Got no no service reference for LEM at " + container + ". Returning null.");
		}
		return lem;
	}

	/**<ul> 
	 * <li>Stores the parameters in {@link #scheduledBenchmarks}, locking {@link #benchmarkLock} first </li>
	 * <li>Prepares callback, storing result in {@link #appContracts} and updates {@link #scheduledBenchmarks}</li>
	 * <li>Calls {@link ILocalEnergyManager#computeContract(String, String, String, String, String, String, String, String)
	 *  ILocalEnergyManager.computeContract}</li>
	 * </ul>
	 * @param args appModel, hwModel, appName, eclURI, contract, implName, benchmark, pluginName */
	private void asyncComputeContractAtLEM(final URI lemURI, final Object[] args) {
//		Object[] args = new Object[] { appModel, hwModel, appName, typeName,
//				contract, implName, benchName, pluginName };

		// <appName>:<typeName>.<implName>
		log.debug("entry "+args[5]+"@"+uriAndHostname(lemURI));
		RemoteOSGiService remote = getRemoteOSGiService();
		final RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
				remote, lemURI.toString(), ILocalEnergyManager.class.getName());
		
		//ILocalEnergyManager lem = getLocalEnergyManager(lemURI);
		
		//String retContract = lem.computeContract(args[0].toString(), args[1].toString(), args[2].toString(),
		//	args[3].toString(), args[4].toString(), args[5].toString(), args[6].toString(), args[7].toString());
		//log.warn(">>> Returned Contract:\n"+retContract);
		//appContracts.get(args[2]).getTypesByName((String) args[3]).get(lemURI.toString()).setContract(retContract);
		
		benchmarkLock.lock();
		List<Object[]> tmp = scheduledBenchmarks.get(lemURI);
		if(tmp == null) {
			tmp = new ArrayList<Object[]>();
			scheduledBenchmarks.put(lemURI, tmp);
		}
		final List<Object[]> list = tmp;
		list.add(args);
		benchmarkLock.unlock();
		
		remote.asyncRemoteCall(
				srefs[0].getURI(),
				"computeContract(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;" +
				"Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;" +
				"Ljava/lang/String;)Ljava/lang/String;",
				args, new AsyncRemoteCallCallback() {
					@Override
					public void remoteCallResult(boolean success, Object o) {
						if(o == null) { success = false; }
						if(success) {
							String contract = (String)o;
							String appName = (String) args[2];
							String typeName = (String) args[3];
							log.debug("finished registering the variant "+typeName+"("
									+ success + ") for " + uriAndHostname(lemURI));
							log.debug("Returned Contract: "+StringUtils.cap(contract, 186, 100));
							//update appContracts
							try {
								acquireWriteLock();
								appContracts.get(appName).getTypeByNameAndContainer(typeName,
										lemURI.toString()).setContract(contract);
							} finally {
								releaseWriteLock();
							}
						}
						else {
							if(o instanceof Throwable) {
								log.warn("compute contract, failure:", (Throwable) o);
							}
							else {
								log.warn("compute contract, got object: " + o);
							}
						}
						benchmarkLock.lock();
						list.remove(args);
						benchmarkFinished.signalAll();
						benchmarkLock.unlock();
					}
				});
//		log.debug("############# END "+lemURI+" asyncComputeContract" + args[2] + ":" + args[3] + "." + args[5]);
	}
	
	/** Inserts a new available implementation along with used frequency and power. What is power? */
	public void updateKnowledePlane(String implName, long frequency, double power) {
		implementationDAO.updateImplementation(implName, frequency, power);
	}

//	/** Map {@link #getLocalEnergyManager(URI)} on {@link #localMgrs} */
//	public List<ILocalEnergyManager> getLocalEnergyManagers() {
//		List<ILocalEnergyManager> ret = new ArrayList<ILocalEnergyManager>();
//		for (URI container : localMgrs) {
//			ILocalEnergyManager lem = getLocalEnergyManager(container);
//			ret.add(lem);
//		}
//		return ret;
//	}

	@Override
	public boolean unregisterLem(URI container) {
		boolean known = localMgrs.remove(container);
		boolean ready = localReadyMgrs.remove(container);
		log.info("Unregistering "+(known ? "known" : "unknown")+","+
				(ready ? "ready" : "unfinished") +
				" LEM at " + uriAndHostname(container));
		return known;
	}

	@Override
	public Object getGlobalUserManager() {
		return null;
	}

	@Override
	public IGlobalResourceManager getGlobalResourceManager() {
		return grm;
	}

	public void setGlobalResourceManager(IGlobalResourceManager grm) {
		this.grm = grm;
	}

	public void unsetGlobalResourceManager(IGlobalResourceManager grm) {
		this.grm = null;
	}

	@Override
	public boolean registerApp(String appName, URI containerURI,
			String structureModel, Map<String, String> contracts) {
		initMDC(containerURI);
		log.info("Registering " + appName + " for " + uriAndHostname(containerURI));
		log.debug(StringUtils.cap(structureModel, 200));

		acquireWriteLock();
		boolean newAppModel;
		boolean newAppLocation;
		try {
			//check if already registered
			if(appModelsResolved.get(appName) != null) {
				log.debug(appName + " is already registered");
			}

			// resolve structural model and ECL contracts

			// caching of deserialized ECL contracts for initial variantmodel
			// generation
			Map<SWComponentType, EclFile> variants = new HashMap<SWComponentType, EclFile>();
			StructuralModel appModel;
			try {
				appModel = CCMUtil.deserializeSWStructModel(
						appName, structureModel, true);
			} catch(IOException e) {
				log.debug("Could not load and persist sw-model", e);
			}
			try {
				appModel = CCMUtil.deserializeSWStructModel(
						appName, structureModel, false);
				ResourceSet rs = appModel.eResource().getResourceSet();
				StructuralModel hwModel = CCMUtil.deserializeHWStructModel(rs,
						grm.getInfrastructureTypes(), false);
				appModelsResolved.put(appName, appModel);
				App contractHere = appContracts.get(appName);
//				boolean newApp = false;
				if(contractHere == null) {
					contractHere = new App(appName);
//					newApp = true;
				}
				//TODO RS: use value of newApp, i.e. maybe don't override App?
				for(String type : contracts.keySet()) {
					if(contractHere.getTypeByNameAndContainer(type, containerURI.toString()) == null) {
						String ctr = contracts.get(type);
						Type t = new Type(type, ctr);
						t.setContainer(containerURI.toString());
						// check if type already exists with this name
						contractHere.addType(t);
					}
					else {
						log.debug(String.format("Type %s already known at %s", type, containerURI));
					}
				}
				appContracts.put(appName, contractHere);

				// find contract uris
				SWComponentType swt = (SWComponentType) appModel.getRoot();
				for (SWComponentType sub : swt.getSubtypes()) {
					String uri = sub.getEclUri();
					String eclUri = uri.substring(uri.lastIndexOf("/") + 1,
							uri.lastIndexOf("."));
					String compName = sub.getName();
					if (compName.contains("_")) {
						compName = compName.substring(compName.lastIndexOf("_") + 1);
					}
					String contract = contracts.get(compName);

					if (uri.startsWith("/")) {
						uri = uri.substring(1);
					}
					EclFile ecl = CCMUtil.deserializeECLContract(appModel, hwModel,
							appName, eclUri, contract, true);
					// rewrite the ECL URI in the structuremodel
					sub.setEclUri(ecl.eResource().getURI().toString());

					// update the contract (contract template)
					contracts.put(sub.getName(), CCMUtil.serializeEObject(ecl));
					// add deserialized contract to the cache
					variants.put(sub, ecl);

					log.debug("dumped: " + sub.getEclUri() + " :: " + StringUtils.cap(contract, 40));
					
					// add info on whether swConnectorType
					for(Type type : contractHere.getTypesByName(compName).values()) {
						type.isConnector = sub instanceof SWConnectorType;
						type.isCopyFile  = sub instanceof CopyFileType;
					}
				}
				CCMUtil.persistResource(appModel.eResource());

			} catch (IOException e) {
				log.error("Error while resolving structural model.", e);
			}

			newAppModel = appModels.put(appName, structureModel) == null;
			newAppLocation = appLocations.put(appName, containerURI) != null;

			log.debug("[GEM] registered app: " + appName);

			// create default variant model for this application, which contains all
			// instances but no mapping
			StructuralModel appStruct = appModelsResolved.get(appName);
			SWComponentType rootType = (SWComponentType) appStruct.getRoot();

			// create a new VariantModel as part of a Resource
			VariantModel vm = CCMUtil.createSWVariantModel(appStruct.eResource()
					.getResourceSet(), appName);
			SWComponent root = VariantFactory.eINSTANCE.createSWComponent();
			root.setName("Root");
			root.setSpecification(rootType);

			// for each contract (= impl) create a(n unmapped) component
			Set<StructuralModel> visiModels = new HashSet<StructuralModel>();
			visiModels.add(appStruct);
			// TODO SG: allow structure models with a hierarchy
			for (EclFile file : variants.values()) {
				for (EclContract c : file.getContracts()) {
					if (c instanceof SWComponentContract) {
						SWComponentType t = ((SWComponentContract) c)
								.getComponentType();
						String implName = ((SWComponentContract) c).getName();
						implName = implName.replaceAll("_", ".");
						SWComponent impl = VariantFactory.eINSTANCE
								.createSWComponent();
						impl.setSpecification(t);
						impl.setName(implName);
						root.getSubtypes().add(impl);
						log.debug("added " + implName + " [" + t.getName() + "]");
					}
				}
			}

			vm.setRoot(root);
			try {
				CCMUtil.persistResource(vm.eResource());
			} catch (IOException e) {
				log.error("Not able to persist the VariantModel for application "
						+ appName, e);
			}

			appConfigurations.put(appName, vm);
		} finally {
			releaseWriteLock();
		}

		if (newAppModel && newAppLocation) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void registerVariant(String appName, String typeName,
			String implName, String benchName, String lemURI, String pluginName) {
		initMDC(URI.create(lemURI));
		// SG: uncomment for productive use. RS: Does not work anymore.
//		if(!lemURI.equals(RemoteOsgiUtil.createRemoteOsgiUriString(getUri()))) {
//			log.warn("will happen another way ;-) ("+getUri()+" || "+lemURI+")");
//			return;
//		}
		implName = implName.replaceAll("_",".");
		acquireWriteLock();
		//only run when not already registered or is currently registering..
		String appModel;
		String hwModel;
		String contract;
		try {
			if(appContracts.get(appName) != null) {
				App app = appContracts.get(appName);
				for(Entry<String, Type> e : app.getTypesByName(typeName).entrySet()) {
					String container = e.getKey();
					Type t = e.getValue();
					Impl impl = t.getImplByName(implName);
					//TODO check if impl generation is really working correct with 2+ LEMs
					if(impl != null) {
						if(container.equals(lemURI)) {
							log.info(String.format("Impl: %s already registered for %s.", implName, container));
							// uncomment return for productive use.
							// Returning here means not computing the contract again
							// if variant is already registered for the LEM
//							return;
						}
					}
					else {
						// impl not known, create new impl object
						Impl newImpl = new Impl(implName, benchName, false);
						newImpl.setPluginName(pluginName);
						t.addImpl(newImpl);
					}
				}
			}
			
			log.debug(typeName + " | " + implName + " | " + benchName+" @ "+lemURI);
			appModel = getApplicationModel(appName);
			hwModel = grm.getInfrastructureTypes();
			contract = getContractForComponentInContainer(appName, typeName, lemURI);
		} finally {
			releaseWriteLock();
		}
		if(contract == null) {
			// TODO better error handling. With this the registration process for this impl breaks. (contract is not computed)
			throw new NullPointerException("Got empty contract for "+implName+". Exiting.");
		}
		
		if(settings.getGemSaveVisData().get() && persistator != null) {
			// save hw-variant model
			log.debug("Persisting hw model before benchmarking");
			persistator.persistModelToJson(grm.getInfrastructure(), grm.getInfrastructureTypes(), implName + "-bench-hw.json");
		}

		/*XXX under the assumption that the application is identified by the appName,
		/* only the LEM with lemUri has to computeContract */
		Object[] args = new Object[] { appModel, hwModel, appName, typeName,
				contract, implName, benchName, pluginName };

		if(settings.getGemUpdateContractsUponNewLem().get()) {
			for (URI aLemURI : localMgrs) {
				asyncComputeContractAtLEM(aLemURI, args);
			}
		}
		else {
			asyncComputeContractAtLEM(URI.create(lemURI), args);
		}
	}

	@Override
	public boolean unregister(String appName, URI container) {

		return false;
	}

	@Override
	public Map<String, String> getRegisteredApplications() {
		return appModels;
	}
	
	@Override
	public String getApplicationModel(String appName) {
		acquireReadLock();
		String structModel;
		try {
			structModel = appModels.get(appName);
		} finally {
			releaseReadLock();
		}
		return structModel;
	}

	@Override
	public Map<String, VariantModel> getCurrentSystemConfigurations() {
		return appConfigurations;
	}
	
	@Override
	public String getCurrentSystemConfiguration(String appUri) {
		acquireReadLock();
		log.debug(appModelsResolved);
		ResourceSet rs;
		VariantModel conf;
		try {
			SWComponentType rootType = (SWComponentType) appModelsResolved.get(
					appUri).getRoot();
			rs = rootType.eResource().getResourceSet();
			conf = appConfigurations.get(appUri);
		} finally { releaseReadLock(); }
		VariantModel infra = null;
		try {
			String infraTypes = grm.getInfrastructureTypes();
			String infrastruct = grm.getInfrastructure();
			CCMUtil.deserializeHWStructModel(rs, infraTypes, false);
			infra = CCMUtil.deserializeGlobalHWVariantModel(rs, infraTypes,
					infrastruct, false);
		} catch (IOException e) {
			log.debug("Error while getting current system config", e);
		}
		if (infra == null) {
			return "";
		}

		String result = "";
		try {
//			//XXX rschoene: why persisting resource here?
//			CCMUtil.persistResource(conf.eResource());
			result = CCMUtil.serializeEObject(conf);

		} catch (IOException e) {
			log.debug("Error while serializing current system config", e);
		}
		return result;

	}

	@Override
	public Map<String, List<String>> getAllApplicationPropertyNames(
			String appName) {
		Map<String, List<String>> ret = new HashMap<String, List<String>>();
		acquireReadLock();
		try {
			if (appModelsResolved == null)
				return null;
			StructuralModel sm = appModelsResolved.get(appName);
			if (sm.getRoot() instanceof SWComponentType) {
				SWComponentType root = (SWComponentType) sm.getRoot();
				for (SWComponentType sub : root.getSubtypes()) {
					String compName = sub.getName();
					List<String> props = new ArrayList<String>();
					for (Property prop : sub.getProperties()) {
						props.add(prop.getDeclaredVariable().getName() + " {"
								+ prop.getValOrder().getLiteral() + "} <"
								+ prop.getKind().getLiteral() + "> ["
								+ prop.getUnit().getName() + "]");
					}
					ret.put(compName, props);
				}
			}
			return ret;
		} finally {
			releaseReadLock();
		}
	}

	@Override
	public List<String> getComponentsOfApp(String appName) {
		List<String> ret = new ArrayList<String>();
		acquireReadLock();
		try {
			if (appModelsResolved == null)
				return null;
			StructuralModel sm = appModelsResolved.get(appName);
			if (sm.getRoot() instanceof SWComponentType) {
				SWComponentType root = (SWComponentType) sm.getRoot();
				for (SWComponentType sub : root.getSubtypes()) {
					ret.add(sub.getName());
				}
			}
			return ret;
		} finally {
			releaseReadLock();
		}
	}

	@Override
	public List<String> getComponentPorts(String appName, String compId) {
		List<String> ret = new ArrayList<String>();
		acquireReadLock();
		try {
			if (appModelsResolved == null)
				return null;
			StructuralModel sm = appModelsResolved.get(appName);
			if (sm.getRoot() instanceof SWComponentType) {
				SWComponentType root = (SWComponentType) sm.getRoot();
				for (SWComponentType sub : root.getSubtypes()) {
					if (sub.getName().equals(compId)) {
						for (PortType pt : sub.getPorttypes()) {
							ret.add(pt.getName());
						}
					}
				}
			}
			return ret;
		} finally {
			releaseReadLock();
		}
	}
	
	/** -todo- */
	public List<String> getPortParameters(String appName, String compId, String portName) {
		List<String> ret = new ArrayList<String>();
		acquireReadLock();
		try {
			if (appModelsResolved == null)
				return null;
			StructuralModel sm = appModelsResolved.get(appName);
			if (sm.getRoot() instanceof SWComponentType) {
				SWComponentType root = (SWComponentType) sm.getRoot();
				for (SWComponentType sub : root.getSubtypes()) {
					if (sub.getName().equals(compId)) {
						for (PortType pt : sub.getPorttypes()) {
							if(pt.getName().equals(portName)) {
								for(Parameter p : pt.getParameter()) {
									if(p.getDirection().equals(ParameterDirection.IN))
										ret.add(p.getName()+" : "+p.getType());
								}
							}
						}
					}
				}
			}
			return ret;
		} finally {
			releaseReadLock();
		}
	}

	@Override
	public List<String> getComponentProperties(String appName, String compId) {
		List<String> ret = new ArrayList<String>();
		acquireReadLock();
		try {
			if (appModelsResolved == null)
				return null;
			StructuralModel sm = appModelsResolved.get(appName);
			if (sm.getRoot() instanceof SWComponentType) {
				SWComponentType root = (SWComponentType) sm.getRoot();
				for (SWComponentType sub : root.getSubtypes()) {
					if (sub.getName().equals(compId)) {
						for (Property prop : sub.getProperties()) {
							ret.add(prop.getDeclaredVariable().getName());
						}
					}
				}
			}
			return ret;
		} finally {
			releaseReadLock();
		}
	}
	
	@Override
	public List<String> getPortsMetaParameters(String appName, String compId,
			String portName) {
		List<String> ret = new ArrayList<String>();
		acquireReadLock();
		try {
			if (appModelsResolved == null)
				return null;
			StructuralModel sm = appModelsResolved.get(appName);
			if (sm.getRoot() instanceof SWComponentType) {
				SWComponentType root = (SWComponentType) sm.getRoot();
				for (SWComponentType sub : root.getSubtypes()) {
					if (sub.getName().equals(compId)) {
						for(PortType pt : sub.getPorttypes()) {
							if(pt.getName().equals(portName)) {
								for(Parameter p : pt.getMetaparameter()) {
									ret.add(p.getName());
								}
							}
						}
					}
				}
			}
			return ret;
		} finally {
			releaseReadLock();
		}
	}
	
	@Override
	public Serializable run(String userRequest, boolean returnTaskId, boolean optimize, Object... params) {
		class JustRun implements Callable<Serializable> {
			private MappingInfo mi;

			public JustRun(MappingInfo mi) {
				this.mi = mi;
			}

			/* (non-Javadoc)
			 * @see java.util.concurrent.Callable#call()
			 */
			@Override
			public Serializable call() throws Exception {
				return runMapping(mi);
			}
			
		}
		log.debug("run() is called");
		final ITask task = createNewTask();
		final long taskId = task.getTaskId();
		if(!optimize && lastMapping != null) {
//			RequestAndParams rap = new RequestAndParams();
//			rap.params = params;
//			rap.userRequest = userRequest;
//			if(callPreOptimizer(rap)) {
//				params = rap.params;
//				userRequest = rap.userRequest;
//			}
			params = resolveResources(params, new HashMap<String, List<String>>());
			log.info("Do not optimize, just run");	
			lastMapping.task = task;
			lastMapping.taskId = taskId;
			lastMapping.newParams = params;
			Callable<Serializable> jr = new JustRun(lastMapping);
			if(returnTaskId) {
				results.put(taskId, pool.submit(jr));
				return taskId;
			}
			// else execute it normally
			try {
				return jr.call();
			} catch (Exception e) {
				return ExecuteResultSerializer.serialize(new ErrorResult(e));
			}
		}
		else {
			// with optimize
			Callable<Serializable> theRest = new OptimizeAndRun(params, taskId, task, userRequest);
			if(returnTaskId) {
				results.put(taskId, pool.submit(theRest));
				return taskId;
			}
			// else execute it normally
			try {
				return theRest.call();
			}
			catch(Exception e) {
				return ExecuteResultSerializer.serialize(new ErrorResult(e));
			}
		}
	}

	@Override
	public Serializable optimize(final String userRequest, boolean returnTaskId, final Object... params) {
		final ITask task = createNewTask();
		final long taskId = task.getTaskId();
		Callable<Serializable> theRest = new OptimizeAndRun(params, taskId, task, userRequest);
		if(returnTaskId) {
			results.put(taskId, pool.submit(theRest));
			return taskId;
		}
		// else execute it normally
		try {
			return theRest.call();
		}
		catch(Exception e) {
			return ExecuteResultSerializer.serialize(new ErrorResult(e));
		}
	}
	
	private void updateGemJobsToGrm(String appName, long taskId) {
		List<Job> jobList = getJobs(appName, taskId);
		// create partial model with virtual root directly containing all resources with changes
		VariantModel partialModel = VariantFactory.eINSTANCE.createVariantModel();
		final Resource root = VariantFactory.eINSTANCE.createResource();
		partialModel.setRoot(root);
		Callable<ContainerProvider> createResource = new Callable<ContainerProvider>() {
			@Override
			public ContainerProvider call() throws Exception {
				ContainerProvider cp = VariantFactory.eINSTANCE.createContainerProvider();
				root.getSubresources().add(cp);
				cp.setSchedule(VariantFactory.eINSTANCE.createSchedule());
				return cp;
			}
		};
		Map<String, ContainerProvider> createdResources = new HashMap<>();
		for(Job job : jobList) {
			String hostname = job.getHost();
			ContainerProvider cp = CollectionsUtil.getIgnoreException(createdResources, hostname, createResource);
			cp.setName(hostname);
			cp.getSchedule().getJobs().add(job);
		}
		updateGrm(partialModel);
	}
	
	protected void updateGrm(VariantModel partialModel) {
		// create resource and add tmpForGrm to it
		org.eclipse.emf.common.util.URI varURI = org.eclipse.emf.common.util.URI.
				createPlatformResourceURI("theatre/changedHw_VM.variant", false);
		org.eclipse.emf.ecore.resource.Resource r = CCMUtil.createResourceSet().createResource(varURI);
		r.getContents().add(partialModel);
		// serialize model and send to grm
		try {
			grm.updateWithModel(CCMUtil.serializeEObject(partialModel));
		} catch (IOException e) {
			log.warn("Problems with serializing partial model", e);
		}
	}

	ExecutorService pool = Executors.newCachedThreadPool();
	final Map<Long, Future<Serializable>> results;
	
	@Override
	public Long[] getTaskIds() {
		return results.keySet().toArray(new Long[results.size()]);
	}
	
	@Override
	public Serializable getResultOfTask(long taskId, long timeout, TimeUnit unit) {
		Future<Serializable> future = results.get(taskId);
		if(future != null) {
			try { return future.get(timeout, unit);	}
			catch (InterruptedException | TimeoutException ignore) { }
			catch (ExecutionException e) {
				return ExecuteResultSerializer.serialize(new ErrorResult(e));
			}
		}
		return null;
	}
	
	/**
	 * Creates a newly created task with a fresh task-id.
	 * @return the string representation of the new task
	 */
	protected ITask createNewTask() {
		if(useDatabase) {
			long taskId = taskDAO.createNewTask();
			if(taskId != TaskDAO.OPERATION_FAILED) {
				return getTaskRepository().createNewTask(taskId);
			}
		}
		// if database not used or failed, use default
		return getTaskRepository().createNewTask();
	}
	
	private Schedule ensureSchedule(ContainerProvider cp) {
		Schedule schedule = cp.getSchedule();
		if(schedule == null) {
			schedule = VariantFactory.eINSTANCE.createSchedule();
			cp.setSchedule(schedule);
		}
		return schedule;
	}

	private Resource cloneRes(Resource r, Job... newJobs) {
		Resource clone;
		if(r instanceof ContainerProvider) {
			ContainerProvider cp = VariantFactory.eINSTANCE.createContainerProvider();
			for(Job j : newJobs) {
				ensureSchedule(cp).getJobs().add(j);
			}
			clone = cp;
		}
		else {
			clone = VariantFactory.eINSTANCE.createResource();
		}
		clone.setName(r.getName());
		clone.setSpecification(r.getSpecification());
		return clone;
	}

	protected Mapping doOptimization(String solverId, Request req,
			Map<String, EclFile> lemContracts, Map<String, Object> options) {
		Optimizer theOneToUse = null;
		for(Optimizer o : optimizers) {
			if(o.getId().equals(solverId)) {
				theOneToUse = o;
			}
		}
		if(theOneToUse == null) { // use first as per default
			theOneToUse = optimizers.get(0);
		}
		
		Mapping mapping = new Mapping();
		Object[] args = new Object[4];
		args[0] = req;
		args[1] = lemContracts;
		args[2] = this;
		args[3] = options;
		try {
			mapping = theOneToUse.optimize(args);
		} catch (Exception e) {
			log.debug("Error while using optimizer " + solverId, e);
			mapping.setErrorMessage(e.getMessage());
		}
		
		return mapping;
	}

	private AtomicLong localJobIdCounter = new AtomicLong(System.currentTimeMillis());

	private PowerManager powerManager;
	private long getNextJobId() {
		if(useDatabase) {
			long jobId = jobDAO.createNewJob();
			if(jobId != JobDAO.OPERATION_FAILED) {
				return jobId;
			}
		}
		// if database not used or failed, use default
		return localJobIdCounter.incrementAndGet();
	}

//	private String getAppInfo(String appName) {
//		final String nl = "\n";
//		StringBuilder sb = new StringBuilder("App: ");
//		sb.append(appName).append(nl);
//		Map<String, List<String>> containerToImpls = new HashMap<>();
//		acquireReadLock();
//		try {
//			App a = appContracts.get(appName);
//			if(a == null) {
//				return null;
//			}
//			for(Type t : a.getTypes()) {
////				sb.append(String.format(" t.container=%s, t.typeName=%s", t.container, t.typeName)).append(nl);
//				for(Impl i : t.getImpls()) {
////					sb.append(String.format("  i.implName=%s, i.pluginName=%s, i.benchName=%s, i.finished=%s",
////							i.implName, i.pluginName, i.benchName, i.finished)).append(nl);
//					CollectionsUtil.ensureArrayList(containerToImpls, t.container).add(i.implName);
//				}
//			}
//		} finally {
//			releaseReadLock();
//		}
//		for(Entry<String, List<String>> e : containerToImpls.entrySet()) {
//			sb.append(e.getKey()).append(":").append(e.getValue()).append(nl);
//		}
//		return sb.toString();
//	}

	protected ITaskRepository getTaskRepository() {
		return taskRepo;
//		// using gem uri may be not valid in any case
//		return TaskRepository.getRepo(getBundleContext(), uri);
//		BundleContext context = getBundleContext();
//		ServiceReference<ITaskRepository> ref = context.getServiceReference(ITaskRepository.class);
//		if(ref == null) {
//			log.debug("TaskRepository was not found.");
//			// try remote
//			RemoteOSGiService remote = getRemoteOSGiService();
//			String repoUri = uri; 
//			log.debug("Try to find repo remotely using uri=" + repoUri);
//			RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
//					remote, repoUri, ITaskRepository.class.getName());
//			if(srefs == null || srefs.length == 0) {
//				// not successfull
//				log.warn("TaskRepository was not found remotely. Returning null");
//				return null;
//			}
//			return (ITaskRepository) remote.getRemoteService(srefs[0]);
//		}
//		return context.getService(ref);
	}

	/**
	 * Get all preExecuteNotifiers and for each invoke
	 *  {@link IJobPreExecuteNotifier#beforeJobExecute(JobDescription)}.
	 * @param jd the job description
	 */
	protected void callPreJobExecuteNotifiers(JobDescription jd) {
		for(IJobPreExecuteNotifier notifier : jobPreExecuteNotifiers) {
			try {
				notifier.beforeJobExecute(jd);
			} catch (Exception e) {
				log.warn("Error in " + notifier, e);
			}
		}
	}

	/**
	 * Get all preExecuteNotifiers and for each invoke
	 *  {@link ITaskPreExecuteNotifier#beforeTaskExecute(TaskDescription)}.
	 * @param td the task description
	 */
	protected void callPreTaskExecuteNotifiers(TaskDescription td) {
		for(ITaskPreExecuteNotifier notifier : taskPreExecuteNotifiers) {
			try {
				notifier.beforeTaskExecute(td);
			} catch (Exception e) {
				log.warn("Error in " + notifier, e);
			}
		}
	}
	
	/** Replaces dots with underscores. */
	private String prepareName(String name) {
		name = name.replaceAll("\\.", "_");
		return name;
	}
	
	@Override
	public Serializable executeAtLem(final URI lemURI, Map<String, Object> options,
			String portName, String appName, final String compName, Object... params) {
		if(options == null) { options = Collections.emptyMap(); }
		// check if host of LEM is ready, and invoke PowerManager if not
		if(!localReadyMgrs.contains(lemURI)) {
			try { powerOnBlocking(lemURI, appName, compName, null);
			} catch(Exception e) {
				return new ErrorResult(e);
			}
		}
		
//		String info = getAppInfo(appName);
//		log.debug(info);
		String impl = CollectionsUtil.getIgnoreException(options, OPTION_IMPL_NAME, new Callable<String>() {
			public String call() throws Exception {
				// fallback, grab first impl
				ILocalEnergyManager lem = getLocalEnergyManager(lemURI);
				List<String> listOfImpls = lem.getDeployedComponents(compName);
				return listOfImpls.get(0);
			}
		});
		long taskId = CollectionsUtil.get(options, OPTION_TASK_ID, ITask.ID_NOT_SET);
		long jobId = CollectionsUtil.get(options, OPTION_JOB_ID, ITask.ID_NOT_SET);
		if(taskId == ITask.ID_NOT_SET || jobId == ITask.ID_NOT_SET) {
			log.warn("Options map does not contain valid taskId ("+taskId+") or jobID ("+jobId+").");
		}
		String currentContainer = lemURI.toString();
		acquireReadLock();
		Impl i = null;
		boolean compIsConnector;
		boolean compIsCopyType;
		try {
			App a = appContracts.get(appName);
			Type t;
			if(currentContainer != null) {
				t = a.getTypeByNameAndContainer(compName, currentContainer);
			}
			else {
				t = a.getTypesByName(compName).values().iterator().next();
			}
			i = t.getImplByName(impl);
			compIsConnector = t.isConnector;
			compIsCopyType = t.isCopyFile;
			a = null; t = null;
		} finally {
			releaseReadLock();
		}
		
//		Serializable returnValue = /*null;*/ lem.execute(i.getPluginName(),
//				i.getImplName(), portName, appName, compName,
//				getPortParameters(appName, compName, portName), params);
		// begin async call
		if(i == null) {
			return new ErrorResult(new NullPointerException("No impl found for " + impl));
		}
		String pluginName = i.getPluginName();
		String implName = i.getImplName();
		List<String> portTypes = getPortParameters(appName, compName, portName);
		boolean syncExecute = CollectionsUtil.get(options, IGlobalEnergyManager.OPTION_SYNC_CALL, false);

		Job job = getJob(appName, jobId);
		if(job != null) {
			job.setStatus(JobStatus.STARTED);
		}
		else {
			log.warn("Job is null for app=" + appName + ", jodId=" + jobId);
		}

		ExecuteResult ee;
		if(syncExecute) {
			ee = syncExecute(lemURI, pluginName, implName, portName,
					appName, compIsConnector, compIsCopyType, taskId, jobId, portTypes, params);
		}
		else {
			ee = asyncExecute(lemURI, pluginName, implName, portName,
					appName, compIsConnector, compIsCopyType, taskId, jobId, false, portTypes, params);
		}
		
		return ExecuteResultSerializer.serialize(ee);
	}
	
	private void powerOnBlocking(URI lemURI, Object appName, String compName, String implName)
			throws IllegalStateException, InterruptedException {
		if(compName == null && implName == null) {
			throw new NullPointerException("CompName and ImplName are both null");
		}
		if(powerManager == null) { return; }
		log.debug("Waiting to power on " + lemURI);
		boolean success = powerManager.powerOnBlocking(RemoteOsgiUtil.stripRosgiParts(lemURI.toString()));
		String name = uriAndHostname(lemURI);
		if(!success) {
			log.fatal("Could not power on host " + name + ".");
			throw new IllegalStateException(
					"Slave" + name + " could not be switched on.");
		}
		else {
			log.debug("Powered on " + name);
		}

		// now wait until impl is registered (or timeout)
		
		long waitUntil = System.currentTimeMillis() + settings.getGemTimeToWaitForRegisterImpl().get();
		boolean found = false;
		while(System.currentTimeMillis() < waitUntil && !found) {
			log.debug("Wait for LEM to be ready.");
			acquireReadLock();
			try {
				found = localReadyMgrs.contains(lemURI);
			} finally {
				releaseReadLock();
			}
			Thread.sleep(1000);
		}
		if(!found) {
			// timeout before registered
			throw new IllegalStateException("Impl " + implName + " (" + compName + ":" + appName
					+") not registered in time at " + uriAndHostname(lemURI));
		}
	}

	private ExecuteResult syncExecute(URI lemURI, String pluginName, String implName,
			String portName, String appName, Boolean compIsConnector, Boolean compIsCopyType, Long taskId, Long jobId,
			List<String> portTypes, Object... params) {
		log.debug("begin, lemURI="+uriAndHostname(lemURI) + ", args=" + Arrays.deepToString(
				new Object[]{pluginName, implName, portName, appName, compIsConnector, compIsCopyType,
						taskId, portTypes, params}));
		// check if host of LEM is ready, and invoke PowerManager if not
		if(!localReadyMgrs.contains(lemURI)) {
			try { powerOnBlocking(lemURI, appName, null, implName);
			} catch(Exception e) {
				return new ErrorResult(e);
			}
		}
		ILocalEnergyManager lem = getLocalEnergyManager(lemURI);
		Serializable result;
		boolean success;
		try {
			result = lem.execute(pluginName, implName, portName, appName, compIsConnector, compIsCopyType,
				taskId, jobId, portTypes, params);
			success = true;
		} catch(Exception e) {
			log.warn("Got exception on execute", e);
			result = e;
			success = false;
		}
		log.debug("result = " + result);
		boolean successNotification = success && result != null && !(result instanceof Exception);
		callJobResultNotifiers(successNotification, result, appName, jobId);
		return new SyncExecuteResult(result);
	}

	@Override
	public Serializable executeCurrentImpl(String appName, String compName, String portName,
			Map<String, Object> options, Object... params) {
		if(options == null) {
			options = Collections.emptyMap();
			log.debug("Using empty options map");
		}
		URI lemUri = null;
		Object optionsLemUri = options.get(IGlobalEnergyManager.OPTION_LEM_URI);
		if(optionsLemUri != null) {
			if(optionsLemUri instanceof String) {
				lemUri = URI.create((String) optionsLemUri);
			}
			else if(optionsLemUri instanceof URI) {
				lemUri = (URI) optionsLemUri;
			}
			else {
				log.debug("Ignoring set uri: " + optionsLemUri);
			}
		}
		if(lemUri == null) {
			// search for a lem which has the component deployed
			List<URI> lemURIs = getLocalMgrs();
			for(URI candidateUri : lemURIs) {
				ILocalEnergyManager lem = getLocalEnergyManager(candidateUri);
				if(lem.isComponentTypeDeployed(compName)) {
					lemUri = candidateUri;
					break;
				}
			}
			// no LEM found which has the given component deployed
			return new SyncExecuteResult(new IllegalStateException("Component not deployed at any LEM"));
		}
		Serializable result = executeAtLem(lemUri, options, portName, appName, compName, params);
		log.debug("result="+result);
		return result;
	}
	
	/**
	 * Calls {@link ILocalEnergyManager#execute(String, String, String, String, Boolean, Boolea, Long, Integer, List, Object...)}
	 * asynchronously.
	 * @param lemURI the uri of the LEM to invoke execute on
	 * @param pluginName the name of the plugin containing the implementation
	 * @param implName the name of the implementation class
	 * @param portName the name of the method to invoke
	 * @param appName the name of the application
	 * @param compIsConnector whether the invoked component is a connector
	 * @param taskId the identifier of the task
	 * @param jobId the identifier of the job of this execute
	 * @param informTaskNotifier whether this is the starting job of the task
	 * @param portTypes a list describing the parameter types, form of each element:
	 * <code>"$NAME : $TYPE"</code>
	 * @param params the actual parameters to pass to the invoked method
	 * @return a wrapper containing either the result or an exception
	 */
	private ExecuteResult asyncExecute(final URI lemURI, final String pluginName, final String implName,
			final String portName, final String appName, final Boolean compIsConnector, final Boolean compIsCopyType,
			final Long taskId, final Long jobId,
			final boolean informTaskNotifier, final List<String> portTypes, final Object... params) {
		if(log.isDebugEnabled()) {
			log.debug("begin, lemURI="+uriAndHostname(lemURI) + ", args=" + Arrays.deepToString(
				new Object[]{pluginName, implName, portName, appName, compIsConnector, taskId,
						jobId, informTaskNotifier, portTypes, params}));
		}

		// run in seperate thread to not block the thread,as this is asyncExecute
		Runnable r = new Runnable() {
			
			@Override
			public void run() {
				// check if host of LEM is ready, and invoke PowerManager if not
				if(!localReadyMgrs.contains(lemURI)) {
					try { powerOnBlocking(lemURI, appName, null, implName);
					} catch(Exception e) {
						callTaskResultNotifiers(false, e, appName, implName, portName, taskId);
						return;
					}
				}
				RemoteOSGiService remote = getRemoteOSGiService();
				final RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
						remote, lemURI.toString(), ILocalEnergyManager.class.getName());
				final Object[] args = new Object[] { pluginName,
						implName, portName, appName, compIsConnector, compIsCopyType,
						taskId, jobId, portTypes, params };
				remote.asyncRemoteCall(
						srefs[0].getURI(),
						"execute("
						+ "Ljava/lang/String;"
						+ "Ljava/lang/String;"
						+ "Ljava/lang/String;"
						+ "Ljava/lang/String;"
						+ "Ljava/lang/Boolean;"
						+ "Ljava/lang/Boolean;"
						+ "Ljava/lang/Long;"
						+ "Ljava/lang/Long;"
						+ "Ljava/util/List;"
						+ "[Ljava/lang/Object;)"
						+ "Ljava/io/Serializable;",
//						+ "Ljava/lang/Integer;",
						args, new AsyncRemoteCallCallback() {
							@Override
							public void remoteCallResult(boolean success, Object o) {
								boolean successNotification;
								if(!success || o == null || (o instanceof Exception)) {
									log.warn("No success, got success=" + success + " and o=" + o);
									successNotification = false;
								}
								else {
									log.debug("Success, got success=" + success + " and o=" + o);
									successNotification = true;
								}
								callJobResultNotifiers(successNotification, o, appName, jobId);
								if(informTaskNotifier) {
									callTaskResultNotifiers(successNotification, o, appName, implName, portName, taskId);
								}
							}
						});
				log.debug("#!#!# end "+lemURI);
			}
		};
		pool.submit(r);
		return new AsyncExecuteResult(taskId, jobId, lemURI.toString());
	}
	
	/**
	 * Get all jobResultNotifier and for each invoke {@link IJobResultNotifier#afterJobFinish(boolean, Object, long) onNotify}.
	 * @param success Was call successful?
	 * @param o the result of the call
	 * @param appName 
	 * @param jobId the id of the job which was finished
	 */
	protected void callJobResultNotifiers(boolean success, Object o, String appName, long jobId) {
		JobResultDescription jrd = new JobResultDescription();
		jrd.success = success;
		jrd.appName = appName;
		jrd.jobId = jobId;
		jrd.endTime = System.currentTimeMillis();
		setRawObject(jrd, o);
		updateInternalJobList(jrd);
		for(IJobResultNotifier notifier : jobResultNotifiers) {
			try {
				notifier.afterJobFinish(jrd);
			} catch (Exception e) {
				log.warn("Error in " + notifier, e);
			}
		}
	}

	private void updateInternalJobList(JobResultDescription jrd) {
		Job job = getJob(jrd.appName, jrd.jobId);
		if(job == null) {
			log.warn("No job found for app " + jrd.appName +", id = " + jrd.jobId);
			return;
		}
		job.setStatus(jrd.success ? JobStatus.FINISHED : JobStatus.FAILED);
		job.setRawResult(StringUtils.replaceMultiple(StringUtils.cap(jrd.rawObject, 200),
				new char[]{'[',']','{','}'}, new String[]{"(",")","(",")"}));
		job.setEndTime(jrd.endTime);
	}

	/**
	 * Get all taskResultNotifier and for each invoke {@link ITaskResultNotifier#afterJobFinish(boolean, Object, int) onNotify}.
	 * @param success Was call successful?
	 * @param o the result of the call
	 * @param appName
	 * @param portName 
	 * @param implName 
	 * @param taskId the id of the task which was finished
	 */
	protected void callTaskResultNotifiers(boolean success, Object o, String appName, String implName, String portName, long taskId) {
		TaskResultDescription trd = new TaskResultDescription();
		trd.success = success;
		trd.appName = appName;
		trd.taskId = taskId;
		trd.endTime = System.currentTimeMillis();
		setRawObject(trd, o);
		for(ITaskResultNotifier notifier : taskResultNotifiers) {
			try {
				notifier.afterTaskFinish(trd);
			} catch (Exception e) {
				log.warn("Error in " + notifier.getClass().getName(), e);
			}
		}
		List<Job> jobs = getJobs(appName, taskId);
		if(jobs.isEmpty()) {
			log.warn("No jobs found for app " + appName + " and taskId " + taskId);
		}
		else {
			// FIXME also call jobResultNotifiers
			for(Job job : jobs) {
				// assume jobs only created or started by now have failed
				if(job.getStatus() == JobStatus.CREATED || job.getStatus() == JobStatus.STARTED) {
					job.setStatus(JobStatus.FAILED);
				}
			}
			updateGemJobsToGrm(appName, taskId);
			if(settings.getGemSaveVisData().get() && persistator != null) {
				// save hw-variant model
				log.debug("Persisting jobs after task-finish");
				persistator.persistJobsToJson(grm.getInfrastructure(), grm.getInfrastructureTypes(),
						implName + "_" + portName + "-jobs.json", taskId);
			}
		}
	}
	
	private void setRawObject(ResultDescription rd, Object rawObject) {
		rd.rawObject = rawObject;
		if(rawObject instanceof FileProxy) {
			FileProxy fp = (FileProxy) rawObject;
			rd.filePath = fp.getFile().getAbsolutePath();
			rd.hostname = hostnameOf(fp.getLemUri());
		}
	}
	
	private String uriAndHostname(URI u) {
		if(u == null) { return "uri=null!"; }
		return u.toString() + "(" + hostnameOf(u) + ")";
	}

	protected String hostnameOf(URI u) {
		return safeHostnameOf(u.toString());
	}

	private String safeHostnameOf(String uriString) {
		uriString = RemoteOsgiUtil.stripRosgiParts(uriString);
		String hostname = PlatformUtils.getHostNameOf(uriString);
		if(hostname == null) {
			return "IP:"+uriString;
		}
		return hostname;
	}

	/**
	 * Search for the contract of a component on the server at the given lemUri.
	 * @param appName name of application
	 * @param compName name of component type
	 * @param lemURI uri of the LEM
	 * @return the contract for this component-LEM-pair, or <code>null</code> on failure
	 */
	private String getContractForComponentInContainer(String appName, String compName, String lemURI) {
		acquireReadLock();
		try {
			App app = appContracts.get(appName);
			log.debug(compName+"@"+safeHostnameOf(lemURI));
			if(app != null) {
				Type t = app.getTypeByNameAndContainer(compName, lemURI);
				if(t != null) {
					return t.getContract();
				}
				if(log.isDebugEnabled()) {
					StringBuilder sb = new StringBuilder("Types exist only for:");
					for(Type type : app.getTypes()) {
						sb.append(type.getTypeName()).append(" @ ").append(type.getContainer()).append(",");
					}
					log.debug(sb.toString());
				}
				log.error("App registration didn't work out. (contract template has not been registered for compName="+compName+")");
				return null;
			}
		} finally {
			releaseReadLock();
		}
		log.error("A variant bundle registered before component bundle, compName=" + compName);
		return null;
	}

	@Override
	public Map<String, Map<String, String>> getContractsForApp(String appName, Map<String, List<String>> nameMap) {
		Map<String, Map<String, String>> ret = new HashMap<>();
		App app = appContracts.get(appName);
		Cache<String, List<String>> resolvedCache = new Cache<>(new Function<String, List<String>>() {

			@Override
			public List<String> apply(String s) {
				List<FileProxy> list = resolveResource(s);
				if(list.isEmpty() || list.get(0) == null) {
					log.fatal("Eh, got an empty or bad list ("+list+"). Hack: add first LEM as match.");
					for(Iterator<FileProxy> it = list.iterator(); it.hasNext(); ) {
						FileProxy fp = it.next();
						if(fp == null) {
							it.remove();
						}
					}
					list.add(new FileProxy(null, localMgrs.get(0)));
				}
				List<String> result = new ArrayList<>();
				for(FileProxy fp : list) {
					if(fp == null) { continue; }
					String host = fp.getLemUri().getHost();
					result.add(host != null ? host : fp.getLemUri().getPath());
				}
				log.debug("Recomputed list of hosts for " + s + " and got:" + result);
				return result;
			}
		}, false);
		Map<String, String> seenCopyFiles = new HashMap<>();
		if(app != null) {
			List<Type> types = app.getTypes();
			log.debug("Using nameMap="+nameMap);
			for(Type t : types) {
				String containerIp = RemoteOsgiUtil.stripRosgiParts(t.container);
//				log.debug("processing type " + t.typeName + " on " + containerIp);
				if(t.isCopyFile && nameMap.containsKey(t.typeName)) {
					String name;
					if(seenCopyFiles.containsKey(t.typeName)) {
						name = seenCopyFiles.get(t.typeName);
					}
					else {
						List<String> nameList = nameMap.get(t.typeName);
						if(nameList == null || nameList.isEmpty()) {
							log.error("Got null or empty list for " + t.typeName + ". Skipping.");
							continue;
						}
						// take first.
						name = nameList.get(0);
						seenCopyFiles.put(t.typeName, name);
					}
					List<String> list = resolvedCache.get(name);
//					log.debug("Got the list: " + list);
					if(list.contains(containerIp)) {
						// TODO check if t.contract contains correct contract
						// FIXME here the contract needs to be rewritten to include boot-time
						CollectionsUtil.ensureHashMap(ret, t.typeName).put(containerIp, t.contract);
					}
					else {
//						log.debug("Container " + containerIp + " not found in list of resolved hosts.");
					}
				}
				else {
					CollectionsUtil.ensureHashMap(ret, t.typeName).put(containerIp, t.contract);
				}
			}
		}
		return ret;
	}
	
	/**
	 * Try to resolve a resource with the given name.
	 * Uses all {@link ResourceResolver} registered at the same URI as the GEM.
	 * @param name the name of the resource to resolve
	 * @return a list of resolved FileProxies, or a list containing only an unresolved
	 * FileProxy. Never returns <code>null</code>.
	 */
	private List<FileProxy> resolveResource(String name) {
			for(ResourceResolver rr : resolvers) {
				try {
					List<FileProxy> list = rr.resolve(name);
					log.debug("got: " + list);
					return list;
				} catch (Exception e) {
					log.warn(rr.getClass() + " threw an exception. Trying next.", e);
				}
			}
		log.error("No resource resolver returned a suitable result. Returning unresolved FileProxy.");
		return Arrays.asList(new FileProxy(name));
	}

	@Override
	public Map<String,String> getContractsForComponent(String appName, String compName) {
		Map<String, String> ret = new HashMap<String, String>();
		App app = appContracts.get(appName);
		if(app != null) {
			Map<String,Type> lemTypes = app.getTypesByName(compName);
			if(lemTypes != null) {
				for(String uri : lemTypes.keySet()) {
					ret.put(RemoteOsgiUtil.stripRosgiParts(uri), lemTypes.get(uri).getContract());
				}
				return ret;
			}
			throw new RuntimeException("App registration didn't work out. (contract template has not been registered)");
		}
		throw new RuntimeException("A variant bundle should not be registered before its component bundle.");
	}

	/**
	 * -todo-
	 * @param reqData
	 * @param compName
	 * @param portName
	 * @param numNFRs
	 * @param appModel
	 * @param infraVM
	 * @return
	 */
	private Request createRequest(String[] reqData, String compName,
			String portName, StructuralModel appModel,
			VariantModel infraVM, String mpName, String mpVal) {
		Request req = RequestsFactory.eINSTANCE.createRequest();
		Platform platform = RequestsFactory.eINSTANCE.createPlatform();
		platform.setHwmodel(infraVM);
		Parameter mp = null;

		req.setHardware(platform);
		SWComponentType swt = (SWComponentType) appModel.getRoot();
		SWComponentType reqSWT = null;
		for (SWComponentType subt : swt.getSubtypes()) {
			if (subt.getName().equals(compName)) {
				req.setComponent(subt);
				reqSWT = subt;
				for (PortType pt : subt.getPorttypes()) {
					if (pt.getName().equals(portName)) {
						for(Parameter p : pt.getMetaparameter()) {
							if(p.getName().equals(mpName)) {
								mp = p;
							}
						}
						req.setTarget(pt);
					}
				}
			}
		}
		if(reqData.length > 4 && !reqData[4].trim().isEmpty() && reqSWT != null) {
//			String[] nfrs = reqData[4].split("\\|");
//			int numNFRs = nfrs.length;
//			for (int i = 0; i < numNFRs; i++) {
			for(String nfr : StringUtils.tokenize(reqData[4], ELEMENT_DELIMITER)) {
				PropertyRequirementClause prc = EclFactory.eINSTANCE
						.createPropertyRequirementClause();
//				String nfr = nfrs[i];
				log.debug("Parsing nfr '" + nfr + "'");
				if (nfr.contains(" min: ")) {
					String[] x = nfr.split(" min: ");
					String propName = x[0];
					for (Property p : reqSWT.getProperties()) {
						if (p.getDeclaredVariable().getName().equals(propName)) {
							prc.setRequiredProperty(p);
						}
					}
					String minValue = x[1];
					RealLiteralExpression rle = LiteralsFactory.eINSTANCE
							.createRealLiteralExpression();
					rle.setValue(Float.parseFloat(minValue));
					prc.setMinValue(rle);
				} else {
					String[] x = nfr.split(" max: ");
					String propName = x[0];
					for (Property p : reqSWT.getProperties()) {
						if (p.getDeclaredVariable().getName().equals(propName)) {
							prc.setRequiredProperty(p);
						}
					}
					String maxValue = x[1];
					RealLiteralExpression rle = LiteralsFactory.eINSTANCE
							.createRealLiteralExpression();
					rle.setValue(Float.parseFloat(maxValue));
					prc.setMaxValue(rle);
					if(prc.getRequiredProperty() == null) {
						log.warn("Could not find requested property for " + propName + " in " + reqSWT);
					}
				}
	
				req.getReqs().add(prc);
			}
		}

		Import imp = RequestsFactory.eINSTANCE.createImport();
		imp.setModel(appModel);
		req.setImport(imp);
		
		if(mpVal != "") {
			MetaParamValue mpv = RequestsFactory.eINSTANCE.createMetaParamValue();
			mpv.setMetaparam(mp);
			mpv.setValue(Integer.parseInt(mpVal));
			req.getMetaParamValues().add(mpv);
		}

		log.debug(req);
		return req;
	}
	
	@Override
	public void reconfigure(String plan, String appName) {
		// execute the derived reconfiguration plan
		printCurrentConfig(appName);
		ReconfigurationManager confManager = new ReconfigurationManager();
		try {
			ReconfigurationPlan confPlan = CCMUtil
					.deserializeReconfigurationPlan(plan, appName);
			confManager.execute(confPlan);
		} catch (ReconfigurationException e) {
			log.error("Not able to reconfigure " + appName, e);
		} catch (IOException e) {
			log.error(
					"Can not deserialize the reconfiguration plan. "
							+ e.getMessage(), e);
		}
		getCurrentSystemConfiguration(appName);
		printCurrentConfig(appName);
	}

	/**
	 * Prints the current config. Does <b>not</b> acquire {@link #appLock},
	 * instead assumes the lock already acquired.
	 * @param appName name of application
	 */
	private void printCurrentConfig(String appName) {
		if(!log.isDebugEnabled() || !settings.getGemVerboseConfig().get()) {
			// since everything shall be printed to log.debug, we are done here
			return;
		}

		log.debug("Current Configuration for '" + appName + "':");

		VariantModel vm = appConfigurations.get(appName);

		SWComponent root = (SWComponent) vm.getRoot();
		Set<Schedule> schedules = new HashSet<Schedule>();
		for (SWComponent sub : root.getSubtypes()) {
			for(Job job : sub.getJobs()) {
				EObject schedule = job.eContainer();
				if(schedule != null) {
					schedules.add((Schedule) schedule);
				}
			}
		}
		for(Schedule schedule : schedules) {
			EObject container = schedule.eContainer();
			String containerName = "UnknownContainer";
			if(container != null) {
				containerName = ((ContainerProvider) container).getName();
			}
			String msg = String.format("%s: %s", containerName, schedule.getJobs());
			log.debug(msg);
		}
	}
	
	/** helper method to get a bundle by its symbolic name **/
	private Bundle getBundle(BundleContext bundleContext, String symbolicName) {
		Bundle result = null;
		for (Bundle candidate : bundleContext.getBundles()) {
			if (candidate.getSymbolicName() != null
					&& candidate.getSymbolicName().equals(symbolicName)) {
				if (result == null
						|| result.getVersion()
								.compareTo(candidate.getVersion()) < 0) {
					result = candidate;
				}
			}
		}
		return result;
	}
	
	private BundleContext getBundleContext() {
		return FrameworkUtil.getBundle(OsgiGem.class).getBundleContext();
	}

	@Override
	public InputStream getBundleBySymbolicName(String name) {
		Bundle variantBundle = getBundle(getBundleContext(), name);
		
		if(variantBundle == null) {
			log.warn("Could not find bundle with name " + name);
			return null;
		}
		String bundleLoc = variantBundle.getLocation();
		log.debug("Bundle Location: "+bundleLoc);
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			File f = FileLocator.getBundleFile(variantBundle);
			File jar = f;
			if(f.isDirectory()) {
				jar = new File(name+".jar");
				fis = new FileInputStream(new File(f.getAbsolutePath()+"/META-INF/MANIFEST.MF"));
				Manifest mf = new Manifest(fis);
				FileFilter filter = new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						if(pathname.getName().equals("MANIFEST.MF")) return false;
						return true;
					}
				};
				fos = new FileOutputStream(jar);
				JarUtils.jar(fos, f.listFiles(), filter, null, mf);
			}
			log.debug(jar.getAbsolutePath());
			InputStream is = new FileInputStream(jar);
			return is;
		} catch (IOException e) {
			log.debug("Error while getting bundle with name="+name, e);
		}
		finally {
			try { if(fis != null) fis.close(); } catch (IOException ignore) { }
			try { if(fos != null) fos.close(); } catch (IOException ignore) { }
		}
		return null;
	}

	@Override
	public void updateContract(String appName, String eclURI,
			String newSerializedContract) {
		// update contracts at all LEMs
		for(URI uri : localMgrs) {
			ILocalEnergyManager lem = getLocalEnergyManager(uri);
			lem.updateContract(getApplicationModel(appName), grm.getInfrastructureTypes(),
					appName, eclURI, newSerializedContract);
			acquireWriteLock();
			try {
				appContracts.get(appName).getTypeByNameAndContainer(eclURI, lem.getContainerId()).
					setContract(newSerializedContract);
			} finally {
				releaseWriteLock();
			}
		}
	}
	
	@Override
	public void waitForCurrentBenchmarksToFinish() {
		benchmarkLock.lock();
		Map<URI,List<Object[]>> toWaitFor = new HashMap<URI, List<Object[]>>(scheduledBenchmarks);
		int size = 0;
		for(Entry<URI, List<Object[]>> entry : toWaitFor.entrySet()) { // reusing entrySet later
			size += entry.getValue().size();
		}
		while(size > 0) {
			log.debug("Waiting for " + size + " benchmarks to finish.");
			try {
				benchmarkFinished.await();
			} catch (InterruptedException e) {
				log.debug("Interupted while waiting for benchmarks.", e);
			}
			// compute new size
			size = 0;
			for(Entry<URI, List<Object[]>> entry : toWaitFor.entrySet()) {
				List<Object[]> currentObjects = scheduledBenchmarks.get(entry.getKey());
				entry.getValue().retainAll(currentObjects);
				size += entry.getValue().size();
			}
		}
		benchmarkLock.unlock();
	}
	
//	IBenchmarkUtil gemBenchmarkUtil;
//	
//	@Override
//	public IBenchmarkUtil getBenchmarkUtil() {
//		if(gemBenchmarkUtil == null) {
//			gemBenchmarkUtil = new BenchmarkUtilProxy();
//		}
//		return gemBenchmarkUtil;
//	}

	@Override
	public boolean reset(boolean full) {
		log.info("Resetting now.");
		localJobIdCounter.set(System.currentTimeMillis());
//		getTaskRepository().reset(full);
		List<Runnable> cancelled = pool.shutdownNow();
		log.warn("Cancelled tasks: " + cancelled);
		pool = Executors.newCachedThreadPool();
		if(useDatabase) {
			taskDAO.clearTasks();
			jobDAO.clearJobs();
			implementationDAO.clearImplementations();
		}
		if(full) {
			// also forget about LEMs and their components (quite dangerous)
			acquireWriteLock();
			try {
				appConfigurations.clear();
				appContracts.clear();
				appModels.clear();
				appModelsResolved.clear();
				scheduledBenchmarks.clear();
			} catch(Exception e) {
				log.warn("Problems during full reset", e);
			} finally {
				releaseWriteLock();
			}
			localMgrs.clear();
			localReadyMgrs.clear();
		}
		return true;
	}

	private void releaseReadLock() {
		appLock.readLock().unlock();
		if(settings.getGemLogLock().get()) log.debug(Thread.currentThread().getName() + " release readlock");
	}

	private void releaseWriteLock() {
		appLock.writeLock().unlock();
		if(settings.getGemLogLock().get()) log.debug(Thread.currentThread().getName() + " release writelock");
	}

	private void acquireReadLock() {
		if(settings.getGemLogLock().get()) log.debug(Thread.currentThread().getName() + " wants readlock for "
				+ new Exception().getStackTrace()[1].getMethodName());
		appLock.readLock().lock();
		if(settings.getGemLogLock().get()) log.debug(Thread.currentThread().getName() + " got readlock");
	}

	private void acquireWriteLock() {
		if(settings.getGemLogLock().get()) log.debug(Thread.currentThread().getName() + " wants writelock for "
				+ new Exception().getStackTrace()[1].getMethodName());
		appLock.writeLock().lock();
		if(settings.getGemLogLock().get()) log.debug(Thread.currentThread().getName() + " got writelock");
	}

	@Override
	public Serializable getResultOfJob(String appName, long jobId) {
		Job job = getJob(appName, jobId);
		if(job == null) {
			return null;
		}
		return (Serializable) job.getRawResult();
	}
	
	/**
	 * Find jobs with the given taskId in the given app.
	 * @param appName the given app
	 * @param taskId the given taskId
	 * @return the found jobs, or an empty list if no such job
	 */
	private List<Job> getJobs(String appName, long taskId) {
		return getJobs(appName, null, taskId);
	}
	
	/**
	 * Find job with the given jobId in the given app.
	 * @param appName the given app
	 * @param jobId the given jobId
	 * @return the found job, or <code>null</code> if no such job
	 */
	private Job getJob(String appName, long jobId) {
		List<Job> list = getJobs(appName, jobId, null);
		if(list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	private List<Job> getJobs(String appName, Long jobId, Long taskId) {
		List<Job> result = new ArrayList<>();
		acquireReadLock();
		VariantModel vm;
		try {
			vm = appConfigurations.get(appName);
		} catch (Exception e) {
			log.warn("Could not get variant model for " + appName, e);
			return result;
		}
		finally {
			releaseReadLock();
		}
		if(vm == null) {
			log.warn("Did not found variant model for " + appName);
			return result;
		}
		// scan through components to find jobs matching the given ids
		SWComponent root = (SWComponent) vm.getRoot();
		List<SWComponent> todo = new ArrayList<>();
		todo.add(root);
		while(!todo.isEmpty()) {
			SWComponent current = todo.remove(0);
			for(Job job : current.getJobs()) {
				if((jobId != null && job.getJobId() == jobId)
						|| (taskId != null && job.getTaskId() == taskId)) {
					result.add(job);
				}
			}
			todo.addAll(current.getSubtypes());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return OsgiGem.class.getName();
	}
	
	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public void setReady(URI lemURI) {
		localReadyMgrs.add(lemURI);
		log.info(uriAndHostname(lemURI) + " is now ready");
	}
	

	protected Serializable runMapping(MappingInfo mi) throws IOException {
		ExecuteResult er;
		String pluginName;
		boolean compIsConnector;
		boolean compIsCopyType;
		URI lemURI;
		Map<String, Type> taskMapping;
		try {
			acquireWriteLock();
			App a = appContracts.get(mi.appName);
			Type t = mi.currentContainer == null ? a.getTypeByNameAndContainer(mi.compName, mi.currentContainer) :
				a.getTypesByName(mi.compName).values().iterator().next();

			Impl i = t.getImplByName(mi.selectedImplOfRequestedComponent);
			if(i == null) {
				throw new IllegalStateException("No impl found for " + mi.selectedImplOfRequestedComponent);
			}
			mi.implName.s = i.implName;
			pluginName = i.pluginName;
			compIsConnector = t.isConnector;
			compIsCopyType = t.isCopyFile;
			t = null;
			i = null;

			//lemURI = localMgrs.get(0);
			lemURI = matchWithLocalMgrs(mi.currentContainer);

			Resource rootInfra = (Resource) mi.infraVM.getRoot();
			printCurrentConfig(mi.appName);

			VariantModel curVM = appConfigurations.get(mi.appName);
			SWComponent root = (SWComponent) curVM.getRoot();

			taskMapping = a.getTypeByImplName(mi.mapping.keySet().toArray(
					new String[mi.mapping.size()]));
			
			// -- begin job creation --
			JobDescription jd = new JobDescription();
			jd.taskId = mi.taskId;

			TaskDescription td = new TaskDescription();
			td.predictedResponseTime = 0.0;
			td.taskId = mi.taskId;

			VariantModel tmpForGrm = VariantFactory.eINSTANCE.createVariantModel();
			tmpForGrm.setRoot(cloneRes(rootInfra));
			// apply mapping by appending new jobs of the mapping
			for (SWComponent sub : root.getSubtypes()) {
				String swCompName = sub.getName();
				String targetServer = mi.mapping.get(swCompName);
				if(targetServer == null) {
					// there is no mapping for this sw-component
					log.debug(swCompName + " not in mapping.");
					continue;
				}
				targetServer = RemoteOsgiUtil.stripRosgiParts(targetServer);
				// search for resource with the name of targetServer
				List<Resource> subTypes = ((Resource) tmpForGrm.getRoot()).getSubresources();
				for (Resource r : rootInfra.getSubresources()) {
					String rUri = r.getName();
					//log.debug(swCompName + ":" + rUri + " ?= " + targetServer);
					if (rUri.equals(targetServer)) {
						if (r instanceof ContainerProvider) {
							long jobId = getNextJobId();
							String swCompType = sub.getSpecification().getName();
							Job newJob = VariantFactory.eINSTANCE.createJob();
							newJob.setImpl(sub);
							Double runTime = mi.mapping.getRunTime(swCompType);
							if(runTime != null) {
								newJob.setPredictedRuntime(runTime);
							}
							else {
								log.warn(String.format("Got no run-time for job(%s on %s)",
										swCompType, targetServer));
							}
							Long startTime = mi.mapping.getStartTime(swCompType);
							if(startTime == null) {
								log.warn(String.format("Got no start-time for job(%s on %s). Using 0.",
										swCompType, targetServer));
								startTime = Long.valueOf(0);
							}
							startTime = System.currentTimeMillis() + startTime;
							newJob.setStartTime(startTime);
							newJob.setJobId(jobId);
							newJob.setTaskId(mi.taskId);
							newJob.setStatus(JobStatus.CREATED);
							newJob.setHost(targetServer);
							//Save schedules in appConfigurations (sw-vm) and not in hw-vm (stored in GRM). Should be done.
							ensureSchedule((ContainerProvider) r).getJobs().add(newJob);
							subTypes.add(cloneRes(r, newJob));
							sub.getJobs().add(newJob);

							String jobTypeName = sub.getSpecification().getName();
							String jobImplName = sub.getName();
							String jobContainer = mi.mapping.get(jobImplName);
							TimeInfo times = mi.mapping.getTimes(jobTypeName);
							double runtime = times.runTime == null ? 0 : times.runTime;
							mi.task.addMapping(mi.appName, jobTypeName, jobImplName, jobContainer, jobId, startTime);
							log.info(String.format("New Task mapping, id=%d, appName=%s, type=%s, impl=%s, container=%s",
									mi.taskId, mi.appName, jobTypeName, jobImplName, jobContainer));
							jd.appName = mi.appName;
							jd.implName = prepareName(jobImplName);
							jd.jobId = jobId;
							jd.containerName = RemoteOsgiUtil.stripRosgiParts(jobContainer);
							jd.predicted = runtime;
							jd.startTime = startTime;

							callPreJobExecuteNotifiers(jd);
							if(mi.implName.s.equals(jobImplName)) {
								mi.calledJobId = jobId;
							}
							td.predictedResponseTime += runtime;
							td.hosts.add(jd.containerName);

							break;
						}
						log.debug("Nothing to do because Server "
								+ targetServer
								+ " has to be a Container Provider!");
					} // no else
				} // for resources
			} // for sw-components
			callPreTaskExecuteNotifiers(td);

			// -- end job creation --
			
			// create resource and add tmpForGrm to it
			org.eclipse.emf.common.util.URI varURI = org.eclipse.emf.common.util.URI.
					createPlatformResourceURI("theatre/changedHw_VM.variant", false);
			org.eclipse.emf.ecore.resource.Resource r = CCMUtil.createResourceSet().createResource(varURI);
			r.getContents().add(tmpForGrm);
			// serialize model and send to grm
			grm.updateWithModel(CCMUtil.serializeEObject(tmpForGrm));
			// maybe not even necessary
			appConfigurations.put(mi.appName, curVM);

			printCurrentConfig(mi.appName);

			log.debug("taskMapping=" + taskMapping);
			a = null;
		} finally {
			releaseWriteLock();
		}
		getTaskRepository().updateTask(mi.taskId, mi.task);

		log.info("Calling " + mi.compName + " at " + uriAndHostname(lemURI));
		// finally the call
		er = asyncExecute(lemURI, pluginName, mi.implName.s,
				mi.portName, mi.appName, compIsConnector, compIsCopyType, mi.taskId, mi.calledJobId, true,
				getPortParameters(mi.appName, mi.compName, mi.portName), mi.newParams);
		return ExecuteResultSerializer.serialize(er);
	}

	/**
	 * @param ip the IP to search with
	 * @return the URI of the first existing, known and ready LEM having this IP
	 */
	private URI matchWithLocalMgrs(String ip) {
		for(URI uri : localReadyMgrs) {
			if(uri.getHost().equals(ip)) {
				return uri;
			}
		}
		// default, make an URI out of the IP
		log.warn("No matching LEM found for ip = " + ip);
		return URI.create(ip);
	}

	protected boolean callPreOptimizer(RequestAndParams rap) {
		// check for pre-optimize-runners
		BundleContext context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		Collection<ServiceReference<PreOptimizeRunner>> refs;
		try {
			refs = context.getServiceReferences(PreOptimizeRunner.class, null);
		} catch (Exception impossible) { /* filter is null */ return false; }
		if(!refs.isEmpty()) {
			for(ServiceReference<PreOptimizeRunner> ref : refs) {
				PreOptimizeRunner por = context.getService(ref);
				rap = por.update(rap);
			}
			return true;
		}
		return false;
	}

	protected Object[] resolveResources(Object[] params, 
			Map<String, List<String>> filenameMap) {
		Object[] newParams = new Object[params.length];
		for (int i = 0; i < params.length; i++) {
			newParams[i] = params[i];
			// Look out for resource describing strings
			if(params[i] instanceof String) {
				String p = (String) params[i];
				if(p.startsWith(RESOURCE_IDENTIFIER)) {
					// gotcha
					p = p.substring(RESOURCE_IDENTIFIER.length());
					String[] parts = p.split(RESOURCE_TYPE_DELIMITER);
					if(parts.length != 2) {
						throw new IllegalArgumentException("Wrong format of resource spec."
								+ " Expected TYPE"+RESOURCE_TYPE_DELIMITER+"NAME. Got " + p);
					}
					String typeName = parts[0];
					String resourceName = parts[1];
					String basename = FileUtils.splitExt(resourceName).basename;
					//						fileNamesTmp.add(basename);
					CollectionsUtil.ensureArrayList(filenameMap, typeName).add(basename);
					// resolved filename.
					String pathname;
					List<FileProxy> proxies = resolveResource(basename);
					if(proxies == null || proxies.size() == 0) {
						log.error("Got empty resolved list for " + basename + ". Using this unresolved name.");
						pathname = basename;
					}
					else {
						// take first. FIXME there should be a better way to select files.
						pathname = proxies.get(0).getFile().getAbsolutePath();
						if(settings.getGemValidResources().get()) {
							// check whether set(lemUris) and set(proxyUris) are not disjunct
							// i.e. if at least one server hosts the given resource
							boolean found = false;
							for(FileProxy fp : proxies) {
								for(URI lemUri : localMgrs) {
									String lemIp = RemoteOsgiUtil.stripRosgiParts(lemUri.toString());
									if(fp.getLemUri().toString().equals(lemIp)) {
										found = true;
										break;
									}
								}
								if(found) { break; }
							}
							if(!found) {
								if(log.isDebugEnabled()) {
									log.debug("localMgrs: " + localMgrs);
									log.debug("proxies: " + proxies);
								}
								throw new IllegalStateException("No connected host found for resource " + params[i]);
							}
						}
					}
					newParams[i] = new File(pathname);
					log.debug("Changing param[" + i + "] to " + newParams[i] +
							(newParams[i] == null ? "" : " of class " + newParams[i].getClass()));
					continue;
				}
			}
			log.debug("param[" + i + "] stays " + newParams[i] +
					(newParams[i] == null ? "" : " of class " + newParams[i].getClass()));
		}
		return newParams;
	}

	// --- OSGi DS methods ---

	private TheatreSettings settings;
	private JobDAO jobDAO;
	private TaskDAO taskDAO;
	private ImplementationDAO implementationDAO;
	private ITaskRepository taskRepo;
	private final List<IJobPreExecuteNotifier> jobPreExecuteNotifiers = new ArrayList<>();
	private final List<IJobResultNotifier> jobResultNotifiers = new ArrayList<>();
	private final List<ITaskPreExecuteNotifier> taskPreExecuteNotifiers = new ArrayList<>();
	private final List<ITaskResultNotifier> taskResultNotifiers = new ArrayList<>();
	private final List<Optimizer> optimizers = new ArrayList<>();
	private final List<ResourceResolver> resolvers = new ArrayList<>();
	private Persistator persistator;

	private ServiceRegistration<IGlobalEnergyManager> reg;
	
	protected void setTheatreSettings(TheatreSettings ts) {
		this.settings = ts;
		String hi;
		if(Boolean.getBoolean("disableColor")) {
			hi = "** Hello World! **";
		}
		else {
			// write with green ink
			hi = "\033[1;32mHello World!\033[0m";
		}
		log.info(hi);
		try {
			uri = settings.getGemUri().get();
		} catch (Exception e) {
			uri = "localhost";
			log.warn("Not able to set the uri of the GEM", e);
		}
		
		// lock the value for this GEM
		useDatabase = settings.getDatabaseUse().get();
	}

	protected void unsetTheatreSettings(TheatreSettings ts) {
		this.settings = null;
	}
	
	protected void setJobDAO(JobDAO jd) {
		this.jobDAO = jd;
		this.jobDAO.insertDummyJobEntry();
	}
	
	protected void unsetJobDAO(JobDAO jd) {
		this.jobDAO = null;
	}

	protected void setTaskDAO(TaskDAO td) {
		this.taskDAO = td;
		this.taskDAO.insertDummyTaskEntry();
	}

	protected void unsetTaskDAO(TaskDAO td) {
		this.taskDAO = null;
	}

	protected void setImplementationDAO(ImplementationDAO id) {
		this.implementationDAO = id;
	}

	protected void unsetImplementationDAO(ImplementationDAO id) {
		this.implementationDAO = null;
	}

	protected void setTaskRepo(ITaskRepository tr) {
		this.taskRepo = tr;
	}

	protected void unsetTaskRepo(ITaskRepository tr) {
		this.taskRepo = null;
	}
	
	protected void addJobPreExecute(IJobPreExecuteNotifier jpen) {
		this.jobPreExecuteNotifiers.add(jpen);
	}
	
	protected void removeJobPreExecute(IJobPreExecuteNotifier jpen) {
		this.jobPreExecuteNotifiers.remove(jpen);
	}
	
	protected void addJobResult(IJobResultNotifier jrn) {
		this.jobResultNotifiers.add(jrn);
	}
	
	protected void removeJobResult(IJobResultNotifier jrn) {
		this.jobResultNotifiers.remove(jrn);
	}
	
	protected void addTaskPreExecute(ITaskPreExecuteNotifier tpen) {
		this.taskPreExecuteNotifiers.add(tpen);
	}
	
	protected void removeTaskPreExecute(ITaskPreExecuteNotifier tpen) {
		this.taskPreExecuteNotifiers.remove(tpen);
	}
	
	protected void addTaskResult(ITaskResultNotifier trn) {
		this.taskResultNotifiers.add(trn);
	}
	
	protected void removeTaskResult(ITaskResultNotifier trn) {
		this.taskResultNotifiers.remove(trn);
	}
	
	protected void setPowerManager(PowerManager pm) {
		log.debug("Powermanager set to " + pm);
		this.powerManager = pm;
		this.powerManager.setHostnameEqualIp(true);
	}
	
	protected void unsetPowerManager(PowerManager pm) {
		this.powerManager = null;
	}
	
	protected void setPersistator(Persistator p) {
		this.persistator = p;
	}
	
	protected void unsetPersistator(Persistator p) {
		this.persistator = null;
	}
	
	protected void addOptimizer(Optimizer o) {
		this.optimizers.add(o);
	}
	
	protected void removeOptimizer(Optimizer o) {
		this.optimizers.remove(o);
	}
	
	protected void addResourceResolver(ResourceResolver rr) {
		this.resolvers.add(rr);
	}
	
	protected void removeResourceResolver(ResourceResolver rr) {
		this.resolvers.remove(rr);
	}

	protected void activate(ComponentContext ctx) {
		log.info("Registering GEM remotely");
		reg = RemoteOsgiUtil.register(ctx.getBundleContext(), IGlobalEnergyManager.class, this);
	}
	
	protected void deactivate(ComponentContext ctx) {
		log.info("Unregistering GEM");
		if(reg != null) {
			reg.unregister();
		}
	}

}
