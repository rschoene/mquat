/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.reconfiguration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IComponentRepository;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.FrameworkUtil;

/**
 * Global repository for components.
 * @author Sebastian Götz
 */
public class ComponentRepository implements IComponentRepository {
	
	private static final String VARIANT_OF = "variantOf_";

	private static final String TYPEIDENTIFIER = "type_";
	
	private Map<String, String> registeredComponentTypes; // K,V = TypeName, BundleId
	private Map<String, Map<String, String>> registeredComponentImpls; // K,V = TypeName, Map<ImplName, BundleId>
	private String server;
	private Logger log = Logger.getLogger(ComponentRepository.class);
	
	public ComponentRepository() throws UnknownHostException{
		registeredComponentTypes = new HashMap<String,String>();
		registeredComponentImpls = new HashMap<String, Map<String,String>>();
		server = "http://" + RemoteOsgiUtil.getPlatformIndependentExternalIp();
		if(System.getProperty("org.eclipse.equinox.http.jetty.http.port")!=null){
			server += ":"+System.getProperty("org.eclipse.equinox.http.jetty.http.port");
		}
		server += "/";
		initialize();
	}
	
	private void initialize() {
		log.info("*** Initializing the component repository ***");
		Properties props = new Properties();
		
		URL propUrl = FrameworkUtil.getBundle(this.getClass()).getResource("repository/repository.properties");
		try {
			props.load(propUrl.openConnection().getInputStream());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Set<String> implKeys = new HashSet<String>();		
		
		for (Object key : props.keySet()) {
			// handle type infos
			if(key.toString().contains(TYPEIDENTIFIER)){
				String typeName = key.toString().replace(TYPEIDENTIFIER, "");
				registeredComponentTypes.put(typeName, props.getProperty(key.toString()).toString());
				log.debug("Component Type Bundle: " + typeName + " added");
			} else if(key.toString().contains(VARIANT_OF)){
				// handle variant names
				
				implKeys.add(key.toString());
			}// no else
		}
		for (String string : registeredComponentTypes.keySet()) {
			registeredComponentImpls.put(string, new HashMap<String, String>());
		}
		for (String key : implKeys) {
			String type = key.replace(VARIANT_OF, "");
			String[] implNames = props.getProperty(key).split(",");
			Map<String, String> variants =  null;
			if(registeredComponentImpls.containsKey(type)){
				variants = registeredComponentImpls.get(type);
			} else {
				variants =  new HashMap<String, String>();
			}
			registeredComponentImpls.put(type,variants);
			for (String implName : implNames) {
				if(props.containsKey(implName)){
					log.debug("Component Variant Bundle " + implName + " added");
					variants.put(implName, props.get(implName).toString());
				}
			}
		}
		log.info("*** Component repository initialization completed. ***");
		
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.reconfiguration.IComponentRepository#getComponentTypeLocation(java.lang.String)
	 */
	@Override
	public URL getComponentTypeLocation (String compType) throws MalformedURLException{
		return getComponentLocation(compType, null);
	}
	
	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.reconfiguration.IComponentRepository#getComponentLocation(java.lang.String, java.lang.String)
	 */
	@Override
	public URL getComponentLocation(String compType, String impl) throws MalformedURLException{
		URL compLocation = null;
		if((impl==null) && (registeredComponentTypes.containsKey(compType))){
			compLocation = new URL( server +  registeredComponentTypes.get(compType));
		}
		else if (compType ==null){
			for (Map val : registeredComponentImpls.values()) {
				if(((Map<String, String>) val).containsKey(impl)){
					compLocation = new URL(server + ((Map<String, String>) val).get(impl));
					break;
				}
			}
		}
		else {
			// TODO: does not work!!
			Map<String, String> impls = registeredComponentImpls.get(compType);
			if(impls!=null){
				if (impls.containsKey(impl)){
					compLocation = new URL(server + impls.get(impls));
				}
			}
		}
		return compLocation;
	}
	
	public Map<String, Map<String, String>> getAll(){
		return registeredComponentImpls;
	}

}
