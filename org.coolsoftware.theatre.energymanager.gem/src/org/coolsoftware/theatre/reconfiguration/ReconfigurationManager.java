/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/** * 
 */
package org.coolsoftware.theatre.reconfiguration;

import java.net.URI;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.reconfiguration.Deployment;
import org.coolsoftware.reconfiguration.DistributedReplacement;
import org.coolsoftware.reconfiguration.LocalReplacement;
import org.coolsoftware.reconfiguration.Migration;
import org.coolsoftware.reconfiguration.ReconfigurationActivity;
import org.coolsoftware.reconfiguration.ReconfigurationPlan;
import org.coolsoftware.reconfiguration.ReconfigurationStep;
import org.coolsoftware.reconfiguration.UnDeployment;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.coolsoftware.theatre.energymanager.ReconfigurationException;
import org.coolsoftware.theatre.globalenergymanager.GemActivator;
import org.coolsoftware.theatre.globalenergymanager.OsgiGem;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

/**
 * A {@link ReconfigurationManager} is responsible to execute the {@link ReconfigurationPlan} derived by the {@link ReconfigurationPlanner.
 * 
 * @author Sebastian Cech
 *
 */
public class ReconfigurationManager {
	private static final Logger log = Logger.getLogger(ReconfigurationManager.class);
	
	
	public void execute(ReconfigurationPlan plan) throws ReconfigurationException{
		for (ReconfigurationActivity act : plan.getActivities()) {
			log.debug("Reconfiguration for Software Component Type " + act.getComponentType() + " started.");
			for (ReconfigurationStep step : act.getSteps()) {
				if (step instanceof Deployment) {
					executeDeployment((Deployment) step, act.getComponentType());
				} 
				else if (step instanceof Migration) {
					executeMigration((Migration) step, act.getComponentType());
				} 
				else if (step instanceof DistributedReplacement) {
					executeDistributedReplacement((DistributedReplacement) step, act.getComponentType());
				} 
				else if (step instanceof LocalReplacement) {
					executeLocalReplacement((LocalReplacement) step, act.getComponentType());
				} 
				else if (step instanceof UnDeployment){
					executeUndeployment((UnDeployment)step, act.getComponentType());
				}
			}
			log.debug("Reconfiguration for Software Component Type " + act.getComponentType() + " successful completed."); //$NON-NLS-1$ //$NON-NLS-2$
		}
		
	}

	private void executeUndeployment(UnDeployment step, String type) throws ReconfigurationException {
		String containerId = step.getSourceContainer();
		String compToUnDeploy = step.getSourceComponent();
		if(isRegisteredContainer(containerId)){
			ILocalEnergyManager lem = getRemoteLem(containerId);
			lem.undeploy(type, compToUnDeploy);					
		} else 
			throw new ReconfigurationException("Can not Deploy " + compToUnDeploy + " on Container " 
					+ containerId + ". Reason: Container is not registered.");
		
	}

	private void executeLocalReplacement(LocalReplacement step, String SwComponentType) throws ReconfigurationException {
		String containerId = step.getSourceContainer();
		String srcComp = step.getSourceComponent();
		String trgtComp = step.getTargetComponent();
		if(isRegisteredContainer(containerId)){
			ILocalEnergyManager lem = getRemoteLem(containerId);
			lem.replace(SwComponentType, srcComp, trgtComp);
		} else {
			throw new ReconfigurationException("Can not perform Local Replacement of " + srcComp +" by "
					+ trgtComp +" on Container " + containerId + ". Reason: Container is not registered.");
		}
		
	}

	private void executeDistributedReplacement(DistributedReplacement step, String type) throws ReconfigurationException {
		String srcCont = step.getSourceContainer();
		String tgtCont = step.getTargetContainer();
		String srcComp = step.getSourceComponent();
		String tgtComp = step.getTargetComponent();
		if(isRegisteredContainer(srcCont) && isRegisteredContainer(tgtCont)){
			ILocalEnergyManager lem = getRemoteLem(srcCont);
			lem.replace(type, srcComp, tgtComp, tgtCont);
		} else {
			throw new ReconfigurationException("Can not perform Distrubuted Replacement of " + srcComp +" by "
					+ tgtComp +" on Container " + srcCont + "to "+ tgtCont + ". Reason: Container is not registered.");
		}
	}

	private void executeMigration(Migration step, String type) throws ReconfigurationException {
		String srcContainer = step.getSourceContainer();
		String tgtContainer = step.getTargetContainer();
		String comp = step.getSourceComponent();
		if(isRegisteredContainer(srcContainer)){
			ILocalEnergyManager lem = getRemoteLem(srcContainer);
			lem.migrate(type, comp, tgtContainer);				
		} else 
			throw new ReconfigurationException("Can not Deploy " + comp + " on Container " 
					+ tgtContainer + ". Reason: Container is not registered.");
		
		log.debug("Execute Component migration"); //$NON-NLS-1$
		
	}

	private void executeDeployment(Deployment step, String type) throws ReconfigurationException {
		String containerId = step.getTargetContainer();
		String compToDeploy = step.getSourceComponent();
		if(isRegisteredContainer(containerId)){
			ILocalEnergyManager lem = getRemoteLem(containerId);
			lem.deploy(type, compToDeploy);					
		} else 
			throw new ReconfigurationException("Can not Deploy " + compToDeploy + " on Container " 
					+ containerId + ". Reason: Container is not registered.");
	}

	private BundleContext getBundleContext() {
		return FrameworkUtil.getBundle(ReconfigurationManager.class).getBundleContext();
	}

	private ILocalEnergyManager getRemoteLem(String uri){
		ILocalEnergyManager lem = null;
		BundleContext context = getBundleContext();

		ServiceReference<?> remoteRef = context.getServiceReference(RemoteOSGiService.class.getName());
		if(remoteRef == null) {
			 log.error("Error: R-OSGi not found!");
		}
		RemoteOSGiService remote = (RemoteOSGiService)context.getService(remoteRef);
		final RemoteServiceReference[] srefs =
			remote.getRemoteServiceReferences(new ch.ethz.iks.r_osgi.URI(uri),
			ILocalEnergyManager.class.getName(), null);
		
		
		lem = (ILocalEnergyManager)remote.getRemoteService(srefs[0]);
		return lem;
	}

	private boolean isRegisteredContainer(String containerUri){
		List<URI> lems = getGem().getLocalMgrs();
		for (URI uri : lems) {
			if(uri.toString().equals(containerUri)){
				return true;
			}
		}
		return false;
	}

	/**
	 * @return the current gem
	 */
	private IGlobalEnergyManager getGem() {
		return RemoteOsgiUtil.getRemoteService(RemoteOsgiUtil.getRemoteOSGiService(getBundleContext()),
				"localhost", IGlobalEnergyManager.class);
	}
}
