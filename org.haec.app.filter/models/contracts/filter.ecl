import ccm [../Server.structure]
import ccm [../filter.structure]

contract UnsortedFilter implements software Filter.filter { 
	mode fast {		
		requires resource CPU {			
			frequency min: 1500			
			cpu_time <f_cpu_time(list_size)>		
		}		
		provides response_time <f_wtime_fast(list_size)> 	
	}	
	mode slow {		
		requires resource CPU {			
			frequency min: 900			
			cpu_time <f_cpu_time(list_size)>		
		}		
		provides response_time <f_wtime_slow(list_size)>
	}
}
