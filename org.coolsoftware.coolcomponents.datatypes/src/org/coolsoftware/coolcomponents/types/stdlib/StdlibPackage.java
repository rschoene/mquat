/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory
 * @model kind="package"
 * @generated
 */
public interface StdlibPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "stdlib";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.cool-software.org/dimm/types/stdlib";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "stdlib";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StdlibPackage eINSTANCE = org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmValueImpl <em>Ccm Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmValueImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmValue()
	 * @generated
	 */
	int CCM_VALUE = 6;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_VALUE__ASCENDING = 0;

	/**
	 * The number of structural features of the '<em>Ccm Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_VALUE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmBooleanImpl <em>Ccm Boolean</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmBooleanImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmBoolean()
	 * @generated
	 */
	int CCM_BOOLEAN = 0;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_BOOLEAN__ASCENDING = CCM_VALUE__ASCENDING;

	/**
	 * The feature id for the '<em><b>Boolean Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_BOOLEAN__BOOLEAN_VALUE = CCM_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ccm Boolean</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_BOOLEAN_FEATURE_COUNT = CCM_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmComparableImpl <em>Ccm Comparable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmComparableImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmComparable()
	 * @generated
	 */
	int CCM_COMPARABLE = 1;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_COMPARABLE__ASCENDING = CCM_VALUE__ASCENDING;

	/**
	 * The number of structural features of the '<em>Ccm Comparable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_COMPARABLE_FEATURE_COUNT = CCM_VALUE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmRealImpl <em>Ccm Real</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmRealImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmReal()
	 * @generated
	 */
	int CCM_REAL = 3;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_REAL__ASCENDING = CCM_COMPARABLE__ASCENDING;

	/**
	 * The feature id for the '<em><b>Real Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_REAL__REAL_VALUE = CCM_COMPARABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ccm Real</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_REAL_FEATURE_COUNT = CCM_COMPARABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmIntegerImpl <em>Ccm Integer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmIntegerImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmInteger()
	 * @generated
	 */
	int CCM_INTEGER = 2;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_INTEGER__ASCENDING = CCM_REAL__ASCENDING;

	/**
	 * The feature id for the '<em><b>Real Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_INTEGER__REAL_VALUE = CCM_REAL__REAL_VALUE;

	/**
	 * The feature id for the '<em><b>Integer Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_INTEGER__INTEGER_VALUE = CCM_REAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ccm Integer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_INTEGER_FEATURE_COUNT = CCM_REAL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStringImpl <em>Ccm String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStringImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmString()
	 * @generated
	 */
	int CCM_STRING = 4;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STRING__ASCENDING = CCM_COMPARABLE__ASCENDING;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STRING__STRING_VALUE = CCM_COMPARABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ccm String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STRING_FEATURE_COUNT = CCM_COMPARABLE_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl <em>Ccm Standard Library Type Factory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmStandardLibraryTypeFactory()
	 * @generated
	 */
	int CCM_STANDARD_LIBRARY_TYPE_FACTORY = 5;

	/**
	 * The feature id for the '<em><b>Boolean Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Integer Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Real Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE = 2;

	/**
	 * The feature id for the '<em><b>String Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE = 4;

	/**
	 * The number of structural features of the '<em>Ccm Standard Library Type Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_STANDARD_LIBRARY_TYPE_FACTORY_FEATURE_COUNT = 5;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmVoidImpl <em>Ccm Void</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmVoidImpl
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmVoid()
	 * @generated
	 */
	int CCM_VOID = 7;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_VOID__ASCENDING = CCM_VALUE__ASCENDING;

	/**
	 * The number of structural features of the '<em>Ccm Void</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_VOID_FEATURE_COUNT = CCM_VALUE_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral <em>Type Literal</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral
	 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getTypeLiteral()
	 * @generated
	 */
	int TYPE_LITERAL = 8;

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean <em>Ccm Boolean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Boolean</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean
	 * @generated
	 */
	EClass getCcmBoolean();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean#isBooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Value</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean#isBooleanValue()
	 * @see #getCcmBoolean()
	 * @generated
	 */
	EAttribute getCcmBoolean_BooleanValue();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmComparable <em>Ccm Comparable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Comparable</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmComparable
	 * @generated
	 */
	EClass getCcmComparable();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmInteger <em>Ccm Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Integer</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmInteger
	 * @generated
	 */
	EClass getCcmInteger();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmInteger#getIntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Value</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmInteger#getIntegerValue()
	 * @see #getCcmInteger()
	 * @generated
	 */
	EAttribute getCcmInteger_IntegerValue();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmReal <em>Ccm Real</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Real</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmReal
	 * @generated
	 */
	EClass getCcmReal();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmReal#getRealValue <em>Real Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Real Value</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmReal#getRealValue()
	 * @see #getCcmReal()
	 * @generated
	 */
	EAttribute getCcmReal_RealValue();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmString <em>Ccm String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm String</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmString
	 * @generated
	 */
	EClass getCcmString();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmString#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmString#getStringValue()
	 * @see #getCcmString()
	 * @generated
	 */
	EAttribute getCcmString_StringValue();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory <em>Ccm Standard Library Type Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Standard Library Type Factory</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory
	 * @generated
	 */
	EClass getCcmStandardLibraryTypeFactory();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getBooleanType <em>Boolean Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Boolean Type</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getBooleanType()
	 * @see #getCcmStandardLibraryTypeFactory()
	 * @generated
	 */
	EReference getCcmStandardLibraryTypeFactory_BooleanType();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getIntegerType <em>Integer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Integer Type</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getIntegerType()
	 * @see #getCcmStandardLibraryTypeFactory()
	 * @generated
	 */
	EReference getCcmStandardLibraryTypeFactory_IntegerType();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getRealType <em>Real Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Real Type</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getRealType()
	 * @see #getCcmStandardLibraryTypeFactory()
	 * @generated
	 */
	EReference getCcmStandardLibraryTypeFactory_RealType();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getStringType <em>String Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>String Type</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getStringType()
	 * @see #getCcmStandardLibraryTypeFactory()
	 * @generated
	 */
	EReference getCcmStandardLibraryTypeFactory_StringType();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value Type</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getValueType()
	 * @see #getCcmStandardLibraryTypeFactory()
	 * @generated
	 */
	EReference getCcmStandardLibraryTypeFactory_ValueType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmValue <em>Ccm Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Value</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmValue
	 * @generated
	 */
	EClass getCcmValue();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmValue#isAscending <em>Ascending</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ascending</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmValue#isAscending()
	 * @see #getCcmValue()
	 * @generated
	 */
	EAttribute getCcmValue_Ascending();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmVoid <em>Ccm Void</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Void</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.CcmVoid
	 * @generated
	 */
	EClass getCcmVoid();

	/**
	 * Returns the meta object for enum '{@link org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral <em>Type Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type Literal</em>'.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral
	 * @generated
	 */
	EEnum getTypeLiteral();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StdlibFactory getStdlibFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmBooleanImpl <em>Ccm Boolean</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmBooleanImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmBoolean()
		 * @generated
		 */
		EClass CCM_BOOLEAN = eINSTANCE.getCcmBoolean();

		/**
		 * The meta object literal for the '<em><b>Boolean Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CCM_BOOLEAN__BOOLEAN_VALUE = eINSTANCE.getCcmBoolean_BooleanValue();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmComparableImpl <em>Ccm Comparable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmComparableImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmComparable()
		 * @generated
		 */
		EClass CCM_COMPARABLE = eINSTANCE.getCcmComparable();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmIntegerImpl <em>Ccm Integer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmIntegerImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmInteger()
		 * @generated
		 */
		EClass CCM_INTEGER = eINSTANCE.getCcmInteger();

		/**
		 * The meta object literal for the '<em><b>Integer Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CCM_INTEGER__INTEGER_VALUE = eINSTANCE.getCcmInteger_IntegerValue();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmRealImpl <em>Ccm Real</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmRealImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmReal()
		 * @generated
		 */
		EClass CCM_REAL = eINSTANCE.getCcmReal();

		/**
		 * The meta object literal for the '<em><b>Real Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CCM_REAL__REAL_VALUE = eINSTANCE.getCcmReal_RealValue();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStringImpl <em>Ccm String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStringImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmString()
		 * @generated
		 */
		EClass CCM_STRING = eINSTANCE.getCcmString();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CCM_STRING__STRING_VALUE = eINSTANCE.getCcmString_StringValue();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl <em>Ccm Standard Library Type Factory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmStandardLibraryTypeFactory()
		 * @generated
		 */
		EClass CCM_STANDARD_LIBRARY_TYPE_FACTORY = eINSTANCE.getCcmStandardLibraryTypeFactory();

		/**
		 * The meta object literal for the '<em><b>Boolean Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE = eINSTANCE.getCcmStandardLibraryTypeFactory_BooleanType();

		/**
		 * The meta object literal for the '<em><b>Integer Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE = eINSTANCE.getCcmStandardLibraryTypeFactory_IntegerType();

		/**
		 * The meta object literal for the '<em><b>Real Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE = eINSTANCE.getCcmStandardLibraryTypeFactory_RealType();

		/**
		 * The meta object literal for the '<em><b>String Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE = eINSTANCE.getCcmStandardLibraryTypeFactory_StringType();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE = eINSTANCE.getCcmStandardLibraryTypeFactory_ValueType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmValueImpl <em>Ccm Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmValueImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmValue()
		 * @generated
		 */
		EClass CCM_VALUE = eINSTANCE.getCcmValue();

		/**
		 * The meta object literal for the '<em><b>Ascending</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CCM_VALUE__ASCENDING = eINSTANCE.getCcmValue_Ascending();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmVoidImpl <em>Ccm Void</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.CcmVoidImpl
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getCcmVoid()
		 * @generated
		 */
		EClass CCM_VOID = eINSTANCE.getCcmVoid();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral <em>Type Literal</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral
		 * @see org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibPackageImpl#getTypeLiteral()
		 * @generated
		 */
		EEnum TYPE_LITERAL = eINSTANCE.getTypeLiteral();

	}

} //StdlibPackage
