/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.impl;

import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Ccm Integer</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmIntegerImpl#getIntegerValue <em>Integer Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CcmIntegerImpl extends CcmRealImpl implements CcmInteger {
	/**
	 * The default value of the '{@link #getIntegerValue() <em>Integer Value</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIntegerValue()
	 * @generated
	 * @ordered
	 */
	protected static final long INTEGER_VALUE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getIntegerValue() <em>Integer Value</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIntegerValue()
	 * @generated
	 * @ordered
	 */
	protected long integerValue = INTEGER_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CcmIntegerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StdlibPackage.Literals.CCM_INTEGER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public long getIntegerValue() {
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerValue(long newIntegerValue) {
		long oldIntegerValue = integerValue;
		integerValue = newIntegerValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_INTEGER__INTEGER_VALUE, oldIntegerValue, integerValue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setIntegerValue(int newIntegerValue) {
		long oldIntegerValue = integerValue;
		integerValue = newIntegerValue;
		realValue = newIntegerValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					StdlibPackage.CCM_INTEGER__INTEGER_VALUE,
					oldIntegerValue, integerValue));

		this.setRealValue(newIntegerValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmInteger add(CcmInteger in1) {
		CcmInteger result = StdlibFactory.eINSTANCE.createCcmInteger();
		result.setIntegerValue(this.integerValue + in1.getIntegerValue());

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmInteger divide(CcmInteger in1) {
		CcmInteger result = StdlibFactory.eINSTANCE.createCcmInteger();
		result.setIntegerValue(this.integerValue / in1.getIntegerValue());

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmInteger multiply(CcmInteger in1) {
		CcmInteger result = StdlibFactory.eINSTANCE.createCcmInteger();
		result.setIntegerValue(this.integerValue * in1.getIntegerValue());

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmInteger negate() {
		CcmInteger result = StdlibFactory.eINSTANCE.createCcmInteger();
		result.setIntegerValue(-this.integerValue);

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmInteger subtract(CcmInteger in1) {
		CcmInteger result = StdlibFactory.eINSTANCE.createCcmInteger();
		result.setIntegerValue(this.integerValue - in1.getIntegerValue());

		return result;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CcmInteger power(CcmInteger in1) {
		CcmInteger result = StdlibFactory.eINSTANCE.createCcmInteger();
		if(in1.getIntegerValue() == 0) {
			result.setIntegerValue(1);
		} else {
			long a = this.integerValue;
			long b = this.integerValue;
			for(long i = in1.getIntegerValue(); i > 1; i--) {
					b *= a;
			}
			result.setIntegerValue(b);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StdlibPackage.CCM_INTEGER__INTEGER_VALUE:
				return getIntegerValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StdlibPackage.CCM_INTEGER__INTEGER_VALUE:
				setIntegerValue((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StdlibPackage.CCM_INTEGER__INTEGER_VALUE:
				setIntegerValue(INTEGER_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StdlibPackage.CCM_INTEGER__INTEGER_VALUE:
				return integerValue != INTEGER_VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (integerValue: ");
		result.append(integerValue);
		result.append(')');
		return result.toString();
	}

} // CcmIntegerImpl
