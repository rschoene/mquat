/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.impl;

import org.coolsoftware.coolcomponents.types.DatatypesPackage;
import org.coolsoftware.coolcomponents.types.impl.DatatypesPackageImpl;
import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmComparable;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.CcmVoid;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage;
import org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StdlibPackageImpl extends EPackageImpl implements StdlibPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmBooleanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmComparableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmIntegerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmRealEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmStandardLibraryTypeFactoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ccmVoidEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum typeLiteralEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StdlibPackageImpl() {
		super(eNS_URI, StdlibFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link StdlibPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StdlibPackage init() {
		if (isInited) return (StdlibPackage)EPackage.Registry.INSTANCE.getEPackage(StdlibPackage.eNS_URI);

		// Obtain or create and register package
		StdlibPackageImpl theStdlibPackage = (StdlibPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StdlibPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new StdlibPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		DatatypesPackageImpl theDatatypesPackage = (DatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) instanceof DatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI) : DatatypesPackage.eINSTANCE);

		// Create package meta-data objects
		theStdlibPackage.createPackageContents();
		theDatatypesPackage.createPackageContents();

		// Initialize created meta-data
		theStdlibPackage.initializePackageContents();
		theDatatypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStdlibPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StdlibPackage.eNS_URI, theStdlibPackage);
		return theStdlibPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmBoolean() {
		return ccmBooleanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCcmBoolean_BooleanValue() {
		return (EAttribute)ccmBooleanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmComparable() {
		return ccmComparableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmInteger() {
		return ccmIntegerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCcmInteger_IntegerValue() {
		return (EAttribute)ccmIntegerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmReal() {
		return ccmRealEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCcmReal_RealValue() {
		return (EAttribute)ccmRealEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmString() {
		return ccmStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCcmString_StringValue() {
		return (EAttribute)ccmStringEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmStandardLibraryTypeFactory() {
		return ccmStandardLibraryTypeFactoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCcmStandardLibraryTypeFactory_BooleanType() {
		return (EReference)ccmStandardLibraryTypeFactoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCcmStandardLibraryTypeFactory_IntegerType() {
		return (EReference)ccmStandardLibraryTypeFactoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCcmStandardLibraryTypeFactory_RealType() {
		return (EReference)ccmStandardLibraryTypeFactoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCcmStandardLibraryTypeFactory_StringType() {
		return (EReference)ccmStandardLibraryTypeFactoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCcmStandardLibraryTypeFactory_ValueType() {
		return (EReference)ccmStandardLibraryTypeFactoryEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmValue() {
		return ccmValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCcmValue_Ascending() {
		return (EAttribute)ccmValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCcmVoid() {
		return ccmVoidEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTypeLiteral() {
		return typeLiteralEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StdlibFactory getStdlibFactory() {
		return (StdlibFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		ccmBooleanEClass = createEClass(CCM_BOOLEAN);
		createEAttribute(ccmBooleanEClass, CCM_BOOLEAN__BOOLEAN_VALUE);

		ccmComparableEClass = createEClass(CCM_COMPARABLE);

		ccmIntegerEClass = createEClass(CCM_INTEGER);
		createEAttribute(ccmIntegerEClass, CCM_INTEGER__INTEGER_VALUE);

		ccmRealEClass = createEClass(CCM_REAL);
		createEAttribute(ccmRealEClass, CCM_REAL__REAL_VALUE);

		ccmStringEClass = createEClass(CCM_STRING);
		createEAttribute(ccmStringEClass, CCM_STRING__STRING_VALUE);

		ccmStandardLibraryTypeFactoryEClass = createEClass(CCM_STANDARD_LIBRARY_TYPE_FACTORY);
		createEReference(ccmStandardLibraryTypeFactoryEClass, CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE);
		createEReference(ccmStandardLibraryTypeFactoryEClass, CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE);
		createEReference(ccmStandardLibraryTypeFactoryEClass, CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE);
		createEReference(ccmStandardLibraryTypeFactoryEClass, CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE);
		createEReference(ccmStandardLibraryTypeFactoryEClass, CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE);

		ccmValueEClass = createEClass(CCM_VALUE);
		createEAttribute(ccmValueEClass, CCM_VALUE__ASCENDING);

		ccmVoidEClass = createEClass(CCM_VOID);

		// Create enums
		typeLiteralEEnum = createEEnum(TYPE_LITERAL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DatatypesPackage theDatatypesPackage = (DatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		ccmBooleanEClass.getESuperTypes().add(this.getCcmValue());
		ccmComparableEClass.getESuperTypes().add(this.getCcmValue());
		ccmIntegerEClass.getESuperTypes().add(this.getCcmReal());
		ccmRealEClass.getESuperTypes().add(this.getCcmComparable());
		ccmStringEClass.getESuperTypes().add(this.getCcmComparable());
		ccmVoidEClass.getESuperTypes().add(this.getCcmValue());

		// Initialize classes and features; add operations and parameters
		initEClass(ccmBooleanEClass, CcmBoolean.class, "CcmBoolean", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCcmBoolean_BooleanValue(), ecorePackage.getEBoolean(), "booleanValue", null, 1, 1, CcmBoolean.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(ccmBooleanEClass, this.getCcmBoolean(), "and", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmBoolean(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmBooleanEClass, this.getCcmBoolean(), "implies", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmBoolean(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(ccmBooleanEClass, this.getCcmBoolean(), "not", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmBooleanEClass, this.getCcmBoolean(), "or", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmBoolean(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ccmComparableEClass, CcmComparable.class, "CcmComparable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = addEOperation(ccmComparableEClass, this.getCcmBoolean(), "greaterThan", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmValue(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmComparableEClass, this.getCcmBoolean(), "greaterThanEquals", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmValue(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmComparableEClass, this.getCcmBoolean(), "lessThan", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmValue(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmComparableEClass, this.getCcmBoolean(), "lessThanEquals", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmValue(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ccmIntegerEClass, CcmInteger.class, "CcmInteger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCcmInteger_IntegerValue(), ecorePackage.getELong(), "integerValue", null, 1, 1, CcmInteger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ccmIntegerEClass, this.getCcmInteger(), "add", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmInteger(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmIntegerEClass, this.getCcmInteger(), "divide", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmInteger(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmIntegerEClass, this.getCcmInteger(), "multiply", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmInteger(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(ccmIntegerEClass, this.getCcmInteger(), "negate", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmIntegerEClass, this.getCcmInteger(), "subtract", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmInteger(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmIntegerEClass, this.getCcmInteger(), "power", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmInteger(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ccmRealEClass, CcmReal.class, "CcmReal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCcmReal_RealValue(), ecorePackage.getEDouble(), "realValue", null, 1, 1, CcmReal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ccmRealEClass, this.getCcmReal(), "add", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmReal(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmRealEClass, this.getCcmReal(), "divide", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmReal(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmRealEClass, this.getCcmReal(), "multiply", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmReal(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(ccmRealEClass, this.getCcmReal(), "negate", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmRealEClass, this.getCcmReal(), "subtract", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmReal(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmRealEClass, this.getCcmReal(), "power", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmReal(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(ccmRealEClass, this.getCcmReal(), "sin", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(ccmRealEClass, this.getCcmReal(), "cos", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ccmStringEClass, CcmString.class, "CcmString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCcmString_StringValue(), ecorePackage.getEString(), "stringValue", null, 1, 1, CcmString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ccmStringEClass, this.getCcmString(), "concat", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmString(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ccmStandardLibraryTypeFactoryEClass, CcmStandardLibraryTypeFactory.class, "CcmStandardLibraryTypeFactory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCcmStandardLibraryTypeFactory_BooleanType(), theDatatypesPackage.getCcmType(), null, "booleanType", null, 1, 1, CcmStandardLibraryTypeFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCcmStandardLibraryTypeFactory_IntegerType(), theDatatypesPackage.getCcmType(), null, "integerType", null, 1, 1, CcmStandardLibraryTypeFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCcmStandardLibraryTypeFactory_RealType(), theDatatypesPackage.getCcmType(), null, "realType", null, 1, 1, CcmStandardLibraryTypeFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCcmStandardLibraryTypeFactory_StringType(), theDatatypesPackage.getCcmType(), null, "stringType", null, 1, 1, CcmStandardLibraryTypeFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCcmStandardLibraryTypeFactory_ValueType(), theDatatypesPackage.getCcmType(), null, "valueType", null, 1, 1, CcmStandardLibraryTypeFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ccmValueEClass, CcmValue.class, "CcmValue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCcmValue_Ascending(), ecorePackage.getEBoolean(), "ascending", "true", 0, 1, CcmValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ccmValueEClass, this.getCcmBoolean(), "equals", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmValue(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(ccmValueEClass, this.getCcmBoolean(), "notEquals", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCcmValue(), "in1", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ccmVoidEClass, CcmVoid.class, "CcmVoid", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(typeLiteralEEnum, TypeLiteral.class, "TypeLiteral");
		addEEnumLiteral(typeLiteralEEnum, TypeLiteral.VALUE);
		addEEnumLiteral(typeLiteralEEnum, TypeLiteral.BOOLEAN);
		addEEnumLiteral(typeLiteralEEnum, TypeLiteral.INTEGER);
		addEEnumLiteral(typeLiteralEEnum, TypeLiteral.REAL);
		addEEnumLiteral(typeLiteralEEnum, TypeLiteral.STRING);
	}

} //StdlibPackageImpl
