/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.impl;

import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmComparable;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Ccm Comparable</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class CcmComparableImpl extends CcmValueImpl implements
		CcmComparable {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CcmComparableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StdlibPackage.Literals.CCM_COMPARABLE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmBoolean greaterThan(CcmValue in1) {
		CcmBoolean result = StdlibFactory.eINSTANCE.createCcmBoolean();
		result.setBooleanValue(this.isGreaterThan(in1));

		return result;
	}

	/** @generated NOT */
	protected abstract boolean isGreaterThan(CcmValue in1);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmBoolean greaterThanEquals(CcmValue in1) {
		CcmBoolean result = StdlibFactory.eINSTANCE.createCcmBoolean();
		result.setBooleanValue(this.isGreaterThanEquals(in1));

		return result;
	}

	/** @generated NOT */
	protected abstract boolean isGreaterThanEquals(CcmValue in1);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmBoolean lessThan(CcmValue in1) {
		CcmBoolean result = StdlibFactory.eINSTANCE.createCcmBoolean();
		result.setBooleanValue(this.isLessThan(in1));

		return result;
	}

	/** @generated NOT */
	protected abstract boolean isLessThan(CcmValue in1);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmBoolean lessThanEquals(CcmValue in1) {
		CcmBoolean result = StdlibFactory.eINSTANCE.createCcmBoolean();
		result.setBooleanValue(this.isLessThanEquals(in1));

		return result;
	}

	/** @generated NOT */
	protected abstract boolean isLessThanEquals(CcmValue in1);

} // CcmComparableImpl
