/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.impl;

import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Ccm Real</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmRealImpl#getRealValue <em>Real Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CcmRealImpl extends CcmComparableImpl implements CcmReal {
	/**
	 * The default value of the '{@link #getRealValue() <em>Real Value</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRealValue()
	 * @generated
	 * @ordered
	 */
	protected static final double REAL_VALUE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getRealValue() <em>Real Value</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRealValue()
	 * @generated
	 * @ordered
	 */
	protected double realValue = REAL_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected CcmRealImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StdlibPackage.Literals.CCM_REAL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public double getRealValue() {
		return realValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealValue(double newRealValue) {
		double oldRealValue = realValue;
		realValue = newRealValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_REAL__REAL_VALUE, oldRealValue, realValue));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmReal add(CcmReal in1) {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		result.setRealValue(this.realValue + in1.getRealValue());

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmReal divide(CcmReal in1) {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		result.setRealValue(this.realValue / in1.getRealValue());

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmReal multiply(CcmReal in1) {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		result.setRealValue(this.realValue * in1.getRealValue());

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmReal negate() {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		result.setRealValue(-this.realValue);

		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmReal subtract(CcmReal in1) {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		result.setRealValue(this.realValue - in1.getRealValue());

		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CcmReal power(CcmReal in1) {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmInteger();
		result.setRealValue((float)Math.pow(this.realValue, in1.getRealValue()));
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CcmReal sin() {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		result.setRealValue((double)Math.sin(this.realValue));
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CcmReal cos() {
		CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
		
		result.setRealValue((double)Math.cos(this.realValue));
		return result;
	}

	/** @generated NOT */
	protected boolean isEqual(CcmValue in1) {
		if (in1 instanceof CcmReal) {
			return this.realValue == ((CcmReal) in1).getRealValue();
		}

		else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StdlibPackage.CCM_REAL__REAL_VALUE:
				return getRealValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StdlibPackage.CCM_REAL__REAL_VALUE:
				setRealValue((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StdlibPackage.CCM_REAL__REAL_VALUE:
				setRealValue(REAL_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StdlibPackage.CCM_REAL__REAL_VALUE:
				return realValue != REAL_VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (realValue: ");
		result.append(realValue);
		result.append(')');
		return result.toString();
	}

	/** @generate NOT */
	@Override
	protected boolean isGreaterThan(CcmValue in1) {
		if (in1 instanceof CcmReal) {
			return this.realValue > ((CcmReal) in1).getRealValue();
		}

		else {
			return false;
		}
	}

	/** @generate NOT */
	@Override
	protected boolean isGreaterThanEquals(CcmValue in1) {
		if (in1 instanceof CcmReal) {
			return this.realValue >= ((CcmReal) in1).getRealValue();
		}

		else {
			return false;
		}
	}

	/** @generate NOT */
	@Override
	protected boolean isLessThan(CcmValue in1) {
		if (in1 instanceof CcmReal) {
			return this.realValue < ((CcmReal) in1).getRealValue();
		}

		else {
			return false;
		}
	}

	/** @generate NOT */
	@Override
	protected boolean isLessThanEquals(CcmValue in1) {
		if (in1 instanceof CcmReal) {
			return this.realValue <= ((CcmReal) in1).getRealValue();
		}

		else {
			return false;
		}
	}

} // CcmRealImpl
