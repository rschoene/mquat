/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.impl;

import org.coolsoftware.coolcomponents.types.stdlib.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StdlibFactoryImpl extends EFactoryImpl implements StdlibFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StdlibFactory init() {
		try {
			StdlibFactory theStdlibFactory = (StdlibFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.cool-software.org/dimm/types/stdlib"); 
			if (theStdlibFactory != null) {
				return theStdlibFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StdlibFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StdlibFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StdlibPackage.CCM_BOOLEAN: return createCcmBoolean();
			case StdlibPackage.CCM_INTEGER: return createCcmInteger();
			case StdlibPackage.CCM_REAL: return createCcmReal();
			case StdlibPackage.CCM_STRING: return createCcmString();
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY: return createCcmStandardLibraryTypeFactory();
			case StdlibPackage.CCM_VOID: return createCcmVoid();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case StdlibPackage.TYPE_LITERAL:
				return createTypeLiteralFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case StdlibPackage.TYPE_LITERAL:
				return convertTypeLiteralToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmBoolean createCcmBoolean() {
		CcmBooleanImpl ccmBoolean = new CcmBooleanImpl();
		return ccmBoolean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmInteger createCcmInteger() {
		CcmIntegerImpl ccmInteger = new CcmIntegerImpl();
		return ccmInteger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmReal createCcmReal() {
		CcmRealImpl ccmReal = new CcmRealImpl();
		return ccmReal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmString createCcmString() {
		CcmStringImpl ccmString = new CcmStringImpl();
		return ccmString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmStandardLibraryTypeFactory createCcmStandardLibraryTypeFactory() {
		CcmStandardLibraryTypeFactoryImpl ccmStandardLibraryTypeFactory = new CcmStandardLibraryTypeFactoryImpl();
		return ccmStandardLibraryTypeFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmVoid createCcmVoid() {
		CcmVoidImpl ccmVoid = new CcmVoidImpl();
		return ccmVoid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeLiteral createTypeLiteralFromString(EDataType eDataType, String initialValue) {
		TypeLiteral result = TypeLiteral.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTypeLiteralToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StdlibPackage getStdlibPackage() {
		return (StdlibPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StdlibPackage getPackage() {
		return StdlibPackage.eINSTANCE;
	}

} //StdlibFactoryImpl
