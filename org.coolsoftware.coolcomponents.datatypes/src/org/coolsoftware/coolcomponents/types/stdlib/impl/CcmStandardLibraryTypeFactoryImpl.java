/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.impl;

import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.DatatypesFactory;

import org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Ccm Standard Library Type Factory</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl#getBooleanType <em>Boolean Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl#getIntegerType <em>Integer Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl#getRealType <em>Real Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl#getStringType <em>String Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.impl.CcmStandardLibraryTypeFactoryImpl#getValueType <em>Value Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CcmStandardLibraryTypeFactoryImpl extends EObjectImpl implements
		CcmStandardLibraryTypeFactory {
	/**
	 * The cached value of the '{@link #getBooleanType() <em>Boolean Type</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBooleanType()
	 * @generated
	 * @ordered
	 */
	protected CcmType booleanType;

	/**
	 * The cached value of the '{@link #getIntegerType() <em>Integer Type</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIntegerType()
	 * @generated
	 * @ordered
	 */
	protected CcmType integerType;

	/**
	 * The cached value of the '{@link #getRealType() <em>Real Type</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getRealType()
	 * @generated
	 * @ordered
	 */
	protected CcmType realType;

	/**
	 * The cached value of the '{@link #getStringType() <em>String Type</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getStringType()
	 * @generated
	 * @ordered
	 */
	protected CcmType stringType;

	/**
	 * The cached value of the '{@link #getValueType() <em>Value Type</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getValueType()
	 * @generated
	 * @ordered
	 */
	protected CcmType valueType;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected CcmStandardLibraryTypeFactoryImpl() {
		super();

		valueType = DatatypesFactory.eINSTANCE.createCcmType();
		valueType.setName("CcmValue");

		booleanType = DatatypesFactory.eINSTANCE.createCcmType();
		booleanType.setName("CcmBoolean");
		booleanType.setSuperType(valueType);

		realType = DatatypesFactory.eINSTANCE.createCcmType();
		realType.setName("CcmReal");
		realType.setSuperType(valueType);

		integerType = DatatypesFactory.eINSTANCE.createCcmType();
		integerType.setName("CcmInteger");
		integerType.setSuperType(realType);

		stringType = DatatypesFactory.eINSTANCE.createCcmType();
		stringType.setName("CcmString");
		stringType.setSuperType(valueType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StdlibPackage.Literals.CCM_STANDARD_LIBRARY_TYPE_FACTORY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CcmType getBooleanType() {
		return booleanType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBooleanType(CcmType newBooleanType,
			NotificationChain msgs) {
		CcmType oldBooleanType = booleanType;
		booleanType = newBooleanType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE, oldBooleanType, newBooleanType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setBooleanType(CcmType newBooleanType) {
		if (newBooleanType != booleanType) {
			NotificationChain msgs = null;
			if (booleanType != null)
				msgs = ((InternalEObject)booleanType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE, null, msgs);
			if (newBooleanType != null)
				msgs = ((InternalEObject)newBooleanType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE, null, msgs);
			msgs = basicSetBooleanType(newBooleanType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE, newBooleanType, newBooleanType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CcmType getIntegerType() {
		return integerType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegerType(CcmType newIntegerType,
			NotificationChain msgs) {
		CcmType oldIntegerType = integerType;
		integerType = newIntegerType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE, oldIntegerType, newIntegerType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerType(CcmType newIntegerType) {
		if (newIntegerType != integerType) {
			NotificationChain msgs = null;
			if (integerType != null)
				msgs = ((InternalEObject)integerType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE, null, msgs);
			if (newIntegerType != null)
				msgs = ((InternalEObject)newIntegerType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE, null, msgs);
			msgs = basicSetIntegerType(newIntegerType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE, newIntegerType, newIntegerType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CcmType getRealType() {
		return realType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRealType(CcmType newRealType,
			NotificationChain msgs) {
		CcmType oldRealType = realType;
		realType = newRealType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE, oldRealType, newRealType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealType(CcmType newRealType) {
		if (newRealType != realType) {
			NotificationChain msgs = null;
			if (realType != null)
				msgs = ((InternalEObject)realType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE, null, msgs);
			if (newRealType != null)
				msgs = ((InternalEObject)newRealType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE, null, msgs);
			msgs = basicSetRealType(newRealType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE, newRealType, newRealType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CcmType getStringType() {
		return stringType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStringType(CcmType newStringType,
			NotificationChain msgs) {
		CcmType oldStringType = stringType;
		stringType = newStringType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE, oldStringType, newStringType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setStringType(CcmType newStringType) {
		if (newStringType != stringType) {
			NotificationChain msgs = null;
			if (stringType != null)
				msgs = ((InternalEObject)stringType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE, null, msgs);
			if (newStringType != null)
				msgs = ((InternalEObject)newStringType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE, null, msgs);
			msgs = basicSetStringType(newStringType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE, newStringType, newStringType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CcmType getValueType() {
		return valueType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueType(CcmType newValueType,
			NotificationChain msgs) {
		CcmType oldValueType = valueType;
		valueType = newValueType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE, oldValueType, newValueType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueType(CcmType newValueType) {
		if (newValueType != valueType) {
			NotificationChain msgs = null;
			if (valueType != null)
				msgs = ((InternalEObject)valueType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE, null, msgs);
			if (newValueType != null)
				msgs = ((InternalEObject)newValueType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE, null, msgs);
			msgs = basicSetValueType(newValueType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE, newValueType, newValueType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE:
				return basicSetBooleanType(null, msgs);
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE:
				return basicSetIntegerType(null, msgs);
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE:
				return basicSetRealType(null, msgs);
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE:
				return basicSetStringType(null, msgs);
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE:
				return basicSetValueType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE:
				return getBooleanType();
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE:
				return getIntegerType();
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE:
				return getRealType();
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE:
				return getStringType();
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE:
				return getValueType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE:
				setBooleanType((CcmType)newValue);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE:
				setIntegerType((CcmType)newValue);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE:
				setRealType((CcmType)newValue);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE:
				setStringType((CcmType)newValue);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE:
				setValueType((CcmType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE:
				setBooleanType((CcmType)null);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE:
				setIntegerType((CcmType)null);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE:
				setRealType((CcmType)null);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE:
				setStringType((CcmType)null);
				return;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE:
				setValueType((CcmType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__BOOLEAN_TYPE:
				return booleanType != null;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__INTEGER_TYPE:
				return integerType != null;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__REAL_TYPE:
				return realType != null;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__STRING_TYPE:
				return stringType != null;
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY__VALUE_TYPE:
				return valueType != null;
		}
		return super.eIsSet(featureID);
	}

} // CcmStandardLibraryTypeFactoryImpl
