/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.util;

import java.util.List;

import org.coolsoftware.coolcomponents.types.stdlib.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage
 * @generated
 */
public class StdlibSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StdlibPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StdlibSwitch() {
		if (modelPackage == null) {
			modelPackage = StdlibPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
		return ePackage == modelPackage;
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
  protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case StdlibPackage.CCM_BOOLEAN: {
				CcmBoolean ccmBoolean = (CcmBoolean)theEObject;
				T result = caseCcmBoolean(ccmBoolean);
				if (result == null) result = caseCcmValue(ccmBoolean);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StdlibPackage.CCM_COMPARABLE: {
				CcmComparable ccmComparable = (CcmComparable)theEObject;
				T result = caseCcmComparable(ccmComparable);
				if (result == null) result = caseCcmValue(ccmComparable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StdlibPackage.CCM_INTEGER: {
				CcmInteger ccmInteger = (CcmInteger)theEObject;
				T result = caseCcmInteger(ccmInteger);
				if (result == null) result = caseCcmReal(ccmInteger);
				if (result == null) result = caseCcmComparable(ccmInteger);
				if (result == null) result = caseCcmValue(ccmInteger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StdlibPackage.CCM_REAL: {
				CcmReal ccmReal = (CcmReal)theEObject;
				T result = caseCcmReal(ccmReal);
				if (result == null) result = caseCcmComparable(ccmReal);
				if (result == null) result = caseCcmValue(ccmReal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StdlibPackage.CCM_STRING: {
				CcmString ccmString = (CcmString)theEObject;
				T result = caseCcmString(ccmString);
				if (result == null) result = caseCcmComparable(ccmString);
				if (result == null) result = caseCcmValue(ccmString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StdlibPackage.CCM_STANDARD_LIBRARY_TYPE_FACTORY: {
				CcmStandardLibraryTypeFactory ccmStandardLibraryTypeFactory = (CcmStandardLibraryTypeFactory)theEObject;
				T result = caseCcmStandardLibraryTypeFactory(ccmStandardLibraryTypeFactory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StdlibPackage.CCM_VALUE: {
				CcmValue ccmValue = (CcmValue)theEObject;
				T result = caseCcmValue(ccmValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StdlibPackage.CCM_VOID: {
				CcmVoid ccmVoid = (CcmVoid)theEObject;
				T result = caseCcmVoid(ccmVoid);
				if (result == null) result = caseCcmValue(ccmVoid);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Boolean</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Boolean</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmBoolean(CcmBoolean object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Comparable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Comparable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmComparable(CcmComparable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Integer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Integer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmInteger(CcmInteger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Real</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Real</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmReal(CcmReal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmString(CcmString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Standard Library Type Factory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Standard Library Type Factory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmStandardLibraryTypeFactory(CcmStandardLibraryTypeFactory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmValue(CcmValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ccm Void</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ccm Void</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCcmVoid(CcmVoid object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
  public T defaultCase(EObject object) {
		return null;
	}

} //StdlibSwitch
