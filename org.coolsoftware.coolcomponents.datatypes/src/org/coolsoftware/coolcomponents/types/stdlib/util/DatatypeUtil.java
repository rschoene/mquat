/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib.util;

import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;

/**
 * Utility class providing methods to print types.
 * @author Sebastian Götz
 */
public class DatatypeUtil {
	public static String formatVarValue(CcmValue v) {
		if(v instanceof CcmInteger) return ((CcmInteger)v).getIntegerValue()+"";
		if(v instanceof CcmReal) return ((CcmReal)v).getRealValue()+"";
		if(v instanceof CcmBoolean) return ((CcmBoolean)v).isBooleanValue() ? "true" : "false";
		if(v instanceof CcmString) return ((CcmString)v).getStringValue();
		
		return "";
	}
}
