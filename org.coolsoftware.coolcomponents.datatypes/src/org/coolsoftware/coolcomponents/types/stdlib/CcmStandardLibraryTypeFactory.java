/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib;

import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Ccm Standard Library Type Factory</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getBooleanType <em>Boolean Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getIntegerType <em>Integer Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getRealType <em>Real Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getStringType <em>String Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getValueType <em>Value Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage#getCcmStandardLibraryTypeFactory()
 * @model
 * @generated
 */
public interface CcmStandardLibraryTypeFactory extends EObject {

	/** @generated NOT */
	public static CcmStandardLibraryTypeFactory eINSTANCE = StdlibFactoryImpl.eINSTANCE
			.createCcmStandardLibraryTypeFactory();

	/**
	 * Returns the value of the '<em><b>Boolean Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Boolean Type</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Boolean Type</em>' containment reference.
	 * @see #setBooleanType(CcmType)
	 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage#getCcmStandardLibraryTypeFactory_BooleanType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CcmType getBooleanType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getBooleanType <em>Boolean Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Boolean Type</em>' containment reference.
	 * @see #getBooleanType()
	 * @generated
	 */
	void setBooleanType(CcmType value);

	/**
	 * Returns the value of the '<em><b>Integer Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Type</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Type</em>' containment reference.
	 * @see #setIntegerType(CcmType)
	 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage#getCcmStandardLibraryTypeFactory_IntegerType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CcmType getIntegerType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getIntegerType <em>Integer Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Type</em>' containment reference.
	 * @see #getIntegerType()
	 * @generated
	 */
	void setIntegerType(CcmType value);

	/**
	 * Returns the value of the '<em><b>Real Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Type</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Type</em>' containment reference.
	 * @see #setRealType(CcmType)
	 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage#getCcmStandardLibraryTypeFactory_RealType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CcmType getRealType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getRealType <em>Real Type</em>}' containment reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Real Type</em>' containment reference.
	 * @see #getRealType()
	 * @generated
	 */
	void setRealType(CcmType value);

	/**
	 * Returns the value of the '<em><b>String Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Type</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Type</em>' containment reference.
	 * @see #setStringType(CcmType)
	 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage#getCcmStandardLibraryTypeFactory_StringType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CcmType getStringType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getStringType <em>String Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Type</em>' containment reference.
	 * @see #getStringType()
	 * @generated
	 */
	void setStringType(CcmType value);

	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' containment reference.
	 * @see #setValueType(CcmType)
	 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage#getCcmStandardLibraryTypeFactory_ValueType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CcmType getValueType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory#getValueType <em>Value Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' containment reference.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(CcmType value);

} // CcmStandardLibraryTypeFactory
