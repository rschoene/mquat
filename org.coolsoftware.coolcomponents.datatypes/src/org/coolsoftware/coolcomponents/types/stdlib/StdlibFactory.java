/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types.stdlib;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.types.stdlib.StdlibPackage
 * @generated
 */
public interface StdlibFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StdlibFactory eINSTANCE = org.coolsoftware.coolcomponents.types.stdlib.impl.StdlibFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Ccm Boolean</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ccm Boolean</em>'.
	 * @generated
	 */
	CcmBoolean createCcmBoolean();

	/**
	 * Returns a new object of class '<em>Ccm Integer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ccm Integer</em>'.
	 * @generated
	 */
	CcmInteger createCcmInteger();

	/**
	 * Returns a new object of class '<em>Ccm Real</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ccm Real</em>'.
	 * @generated
	 */
	CcmReal createCcmReal();

	/**
	 * Returns a new object of class '<em>Ccm String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ccm String</em>'.
	 * @generated
	 */
	CcmString createCcmString();

	/**
	 * Returns a new object of class '<em>Ccm Standard Library Type Factory</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ccm Standard Library Type Factory</em>'.
	 * @generated
	 */
	CcmStandardLibraryTypeFactory createCcmStandardLibraryTypeFactory();

	/**
	 * Returns a new object of class '<em>Ccm Void</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ccm Void</em>'.
	 * @generated
	 */
	CcmVoid createCcmVoid();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StdlibPackage getStdlibPackage();

} //StdlibFactory
