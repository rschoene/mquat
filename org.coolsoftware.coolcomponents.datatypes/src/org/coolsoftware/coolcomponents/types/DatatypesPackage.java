/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.types;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.types.DatatypesFactory
 * @model kind="package"
 * @generated
 */
public interface DatatypesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "types";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.cool-software.org/dimm/types";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "types";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DatatypesPackage eINSTANCE = org.coolsoftware.coolcomponents.types.impl.DatatypesPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.impl.CcmTypeImpl <em>Ccm Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.impl.CcmTypeImpl
	 * @see org.coolsoftware.coolcomponents.types.impl.DatatypesPackageImpl#getCcmType()
	 * @generated
	 */
	int CCM_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Super Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_TYPE__SUPER_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Ccm Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCM_TYPE_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.types.impl.TypeLibraryImpl <em>Type Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.types.impl.TypeLibraryImpl
	 * @see org.coolsoftware.coolcomponents.types.impl.DatatypesPackageImpl#getTypeLibrary()
	 * @generated
	 */
	int TYPE_LIBRARY = 1;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_LIBRARY__TYPES = 0;

	/**
	 * The number of structural features of the '<em>Type Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_LIBRARY_FEATURE_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.CcmType <em>Ccm Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ccm Type</em>'.
	 * @see org.coolsoftware.coolcomponents.types.CcmType
	 * @generated
	 */
	EClass getCcmType();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.types.CcmType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.coolsoftware.coolcomponents.types.CcmType#getName()
	 * @see #getCcmType()
	 * @generated
	 */
	EAttribute getCcmType_Name();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.types.CcmType#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Type</em>'.
	 * @see org.coolsoftware.coolcomponents.types.CcmType#getSuperType()
	 * @see #getCcmType()
	 * @generated
	 */
	EReference getCcmType_SuperType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.types.TypeLibrary <em>Type Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Library</em>'.
	 * @see org.coolsoftware.coolcomponents.types.TypeLibrary
	 * @generated
	 */
	EClass getTypeLibrary();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.types.TypeLibrary#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see org.coolsoftware.coolcomponents.types.TypeLibrary#getTypes()
	 * @see #getTypeLibrary()
	 * @generated
	 */
	EReference getTypeLibrary_Types();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DatatypesFactory getDatatypesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.impl.CcmTypeImpl <em>Ccm Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.impl.CcmTypeImpl
		 * @see org.coolsoftware.coolcomponents.types.impl.DatatypesPackageImpl#getCcmType()
		 * @generated
		 */
		EClass CCM_TYPE = eINSTANCE.getCcmType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CCM_TYPE__NAME = eINSTANCE.getCcmType_Name();

		/**
		 * The meta object literal for the '<em><b>Super Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CCM_TYPE__SUPER_TYPE = eINSTANCE.getCcmType_SuperType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.types.impl.TypeLibraryImpl <em>Type Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.types.impl.TypeLibraryImpl
		 * @see org.coolsoftware.coolcomponents.types.impl.DatatypesPackageImpl#getTypeLibrary()
		 * @generated
		 */
		EClass TYPE_LIBRARY = eINSTANCE.getTypeLibrary();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_LIBRARY__TYPES = eINSTANCE.getTypeLibrary_Types();

	}

} //DatatypesPackage
