/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao.dexter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.haec.theatre.dao.CpuCoreInfo;
import org.haec.theatre.dao.ImplementationDAO;
import org.haec.theatre.dao.JobDAO;
import org.haec.theatre.dao.JobInfo;
import org.haec.theatre.dao.LoadInfo;
import org.haec.theatre.dao.RamInfo;
import org.haec.theatre.dao.ResourceDAO;
import org.haec.theatre.dao.SynopseDAO;
import org.haec.theatre.dao.TaskDAO;
import org.haec.theatre.dao.TaskInfo;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.PlatformUtils.ProcessChangeHandler;
import org.haec.theatre.utils.sql.SQLUtils;

/**
 * Database access object using dexter.
 * @author René Schöne
 */
public class DexterDAO implements JobDAO, TaskDAO, ImplementationDAO, ResourceDAO, SynopseDAO {
	class Init implements Runnable {

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			int tries = settings.getDatabaseStartupTries().get();
			connectionIndex = -1;
			while(tries-- > 0 && connectionIndex == -1) {
				try {
					connectionIndex = SQLUtils.registerConnection(creatorForDexter(), "DexterDAO");
				} catch (Exception e) {
					log.warn("Could not connect", e);
					try { Thread.sleep(settings.getDatabaseStartupInterval().get());
					} catch (InterruptedException ignore) { }
				}
			}
			if(connectionIndex == -1) {
				log.fatal("Connection to Dexter@"+settings.getDatabaseUrl().get()+" not possible");
				return;
			}

			Timer timer = new Timer();
			ProcessChangeHandler changeHandler = new ProcessChangeHandler() {
				
				private boolean recreateSuccessful = true;

				@Override
				protected void processStarted(String hostname, String processName) {
					recreateSuccessful = SQLUtils.recreateAllConnections();
				}
				
				@Override
				protected void processEnded(String hostname, String processName) {
					log.info("Dexter terminated. Waiting for new instance.");
				}
				
				@Override
				protected void beforeRun(String hostname, String processName) {
					if(!recreateSuccessful) { /* try again */
						processStarted(hostname, processName);
					}
				}
			};
			if(settings.getDatabaseUrl().isDefault()) {
				// runs locally
				timer.scheduleAtFixedRate(PlatformUtils.watchForProcessChange(
						PROCESS_NAME_DEXTER,
						changeHandler),
						settings.getDatabaseStartupInterval().get(), settings.getDatabaseCheckRunningRate().get());
			} else {
				// runs remotely
				timer.scheduleAtFixedRate(PlatformUtils.watchForProcessChange(
						PROCESS_NAME_DEXTER,
						settings.getDatabaseUrl().get(), changeHandler),
						settings.getDatabaseStartupInterval().get(), settings.getDatabaseCheckRunningRate().get());
			}
			ready = true;
		}
		
	}

	public final static String PROCESS_NAME_DEXTER = "DexterServer";

	private Logger log = Logger.getLogger(DexterDAO.class);
	private TheatreSettings settings;
	private int connectionIndex;
	
	private AtomicLong lastCpuUpdate = new AtomicLong(0);

	private AtomicLong lastRamUpdate = new AtomicLong(0);

	private AtomicLong lastLoadUpdate = new AtomicLong(0);

	private boolean ready = false;

	/** Creates a creator for a connection the DEXTER database running at the given url. */
	public Callable<Connection> creatorForDexter() {
		return new Callable<Connection>() {
			@Override
			public Connection call() throws Exception {
				String url = settings.getDatabaseUrl().get();
				log.debug(String.format("Connecting to Dexter@%s ...", url));
				Class.forName("de.tu_dresden.inf.dexterdb.jdbc.JDBCDriver");
				DriverManager.setLoginTimeout(5);
				return DriverManager.getConnection("jdbc:dexterdb://"+url,"dexter-user","M8ukagVd");
			}
		};
	}

	protected void setTheatreSettings(TheatreSettings ts) {
		this.settings = ts;
		new Thread(new Init()).start();
	}

	protected void unsetTheatreSettings(TheatreSettings ts) {
		this.settings = null;
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.JobDAO#getJobs(java.lang.String, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public List<JobInfo> getJobs(String appName, Long jobIdRequest,	Long taskIdRequest) {
		List<JobInfo> result = new ArrayList<>();
		try(Statement stmt = getConnection().createStatement()) {
			String sql = SQLQueries.makeJobQuery(jobIdRequest, taskIdRequest);
			ResultSet rs = SQLUtils.runSql(stmt, sql);
			while(rs != null && rs.next()) {
				long jobId = rs.getLong(SQLQueries.index_job_id);
				long taskId = rs.getLong(SQLQueries.index_task_id);
				long startTime = rs.getLong(SQLQueries.index_start);
				double pred_duration = rs.getDouble(SQLQueries.index_pred_duration);
				long endTime = rs.getLong(SQLQueries.index_end);
				int statusCode = rs.getInt(SQLQueries.index_status);
				String status = JobInfo.statusCodeToName(statusCode);
				String implName = rs.getString(SQLQueries.index_implName);
				String hostName = rs.getString(SQLQueries.index_hostName);
				JobInfo info = new JobInfo(taskId, jobId, startTime,
						pred_duration, endTime, status, implName, hostName);
				result.add(info);
			}
		} catch (SQLException e) {
			log.error("Error while retrieving jobs from Dexter.", e);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.TaskDAO#getTasks(java.lang.Long)
	 */
	@Override
	public List<TaskInfo> getTasks(Long taskIdRequest) {
		List<TaskInfo> result = new ArrayList<>();
		try(Statement stmt = getConnection().createStatement()) {
			ResultSet rs = SQLUtils.runSql(stmt, SQLQueries.makeTaskQuery(taskIdRequest), true);
			while(rs.next()) {
				long taskId = rs.getLong(1);
				int statusCode = rs.getInt(2);
				result.add(new TaskInfo(taskId, TaskInfo.statusCodeToName(statusCode)));
			}
		} catch (SQLException e) {
			log.error("Error while retrieving tasks from Dexter.", e);
		}
		return result;
	}

	/**
	 * @return the connection to dexter
	 */
	private Connection getConnection() {
		return SQLUtils.getConnection(connectionIndex);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.JobDAO#insertDummyEntry()
	 */
	@Override
	public void insertDummyJobEntry() {
		try(Statement stmt = getConnection().createStatement()) {
			// job_id, task_id, impl_id, server_id, start, duration, end, status
			long jobId = System.currentTimeMillis();
			SQLUtils.runSql(stmt, String.format("INSERT INTO discovery.theatre_jobs VALUES(%d,0,0,0,0,0,0,%d)",
					jobId, JobInfo.STATUS_DUMMY_CODE));
		} catch (SQLException e) {
			log.error("Error while inserting dummy entries", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.TaskDAO#insertDummyTaskEntry()
	 */
	@Override
	public void insertDummyTaskEntry() {
		try(Statement stmt = getConnection().createStatement()) {
			// task_id, status
			long taskId = System.currentTimeMillis();
			SQLUtils.runSql(stmt, String.format("INSERT INTO discovery.theatre_tasks VALUES (%d,%d)",
					taskId, TaskInfo.STATUS_DUMMY_CODE), true);
		} catch (SQLException e) {
			log.error("Error while inserting dummy entries", e);
		}

	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.ImplementationDAO#updateImplementation(java.lang.String, long, double)
	 */
	@Override
	public void updateImplementation(String implName, long frequency,
			double power) {
		log.debug("storing info for new impl: "+implName+" @"+frequency+" power: "+power+"W");
		try(Statement stmt = getConnection().createStatement()) {
			String sql = String.format("SELECT id, frequency, power from discovery.theatre_implementations WHERE name = '%s'", implName);
			ResultSet rs = SQLUtils.runSql(stmt, sql, true, log);
			if(rs.next()) {
				// impl already exists in database, so ignore information? or update?
				int id = rs.getInt(1);
				long freqStored = rs.getLong(2);
				if(freqStored == 0) {
					// update
					sql = String.format("UPDATE discovery.theatre_implementations SET frequency = %s WHERE id = %d", frequency, id);
					SQLUtils.runSql(stmt, sql, true, log);
				}
			}
			else {
				//insert information
				//				sql = "INSERT INTO discovery.theatre_implementations VALUES("+id+",'"+prepareName(implName)+"',"+frequency+","+power+")";
				sql = String.format(Locale.US, "INSERT INTO discovery.theatre_implementations VALUES('%s',%d,%f)",
						prepareName(implName), frequency, power);
				SQLUtils.runSql(stmt, sql, true, log);
			}
		} catch (SQLException e) {
			log.debug("Error while updating knowledge", e);
		}
	}

	/** Replaces dots with underscores. */
	private String prepareName(String name) {
		name = name.replaceAll("\\.", "_");
		return name;
	}
	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.TaskDAO#createNewTask()
	 */
	@Override
	public long createNewTask() {
		try(Statement stmt = getConnection().createStatement()) {
			// this will result in a Dexter-Crash, if the table is empty
			ResultSet rs = SQLUtils.runSql(stmt, "SELECT max(task_id) FROM discovery.theatre_tasks");
			if(rs != null && rs.next()) {
				long taskId = rs.getLong(1)+1;
				// create new task (not started yet)
				SQLUtils.runSql(stmt, "INSERT INTO discovery.theatre_tasks VALUES ("+taskId+", " + TaskInfo.STATUS_CREATED_CODE+")",
						true, log);
				return taskId;
			}
			log.warn("Didn't get a valid result set. Using default.");
			// Either got some database error, or empty result set. Use default.
		} catch (SQLException e) {
			log.error("Error while getting next task id. Using default.", e);
		}
		return TaskDAO.OPERATION_FAILED;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.JobDAO#createNewJob()
	 */
	@Override
	public long createNewJob() {
		long jobId;
		try(Statement stmt = getConnection().createStatement()) {
			// this will result in a Dexter-Crash, if table theatre_jobs is empty
			ResultSet rs = SQLUtils.runSql(stmt, "SELECT max(job_id) FROM discovery.theatre_jobs");
			if(rs.next()) { jobId = rs.getLong(1)+1; }
			else { // if no rows are returned (i.e. first query ever)
				jobId = 1;
			}
			// job_id, task_id, impl_id, server_id, start, pred_duration, end, status
			String sql = String.format("INSERT INTO discovery.theatre_jobs VALUES(%d,0,0,0,0,0,0,%d)",
					jobId, JobInfo.STATUS_CREATED_CODE);
			SQLUtils.runSql(stmt, sql);
			return jobId;
		} catch (SQLException e) {
			log.error("Error while getting next job id", e);
		}
		return JobDAO.OPERATION_FAILED;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.ImplementationDAO#clearImplementations()
	 */
	@Override
	public void clearImplementations() {
		try(Statement stmt = getConnection().createStatement()) {
			SQLUtils.runSql(stmt, SQLQueries.clearImplementations);
		} catch (SQLException e) {
			log.error("Error while deleting implementations", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.TaskDAO#clearTasks()
	 */
	@Override
	public void clearTasks() {
		try(Statement stmt = getConnection().createStatement()) {
			SQLUtils.runSql(stmt, SQLQueries.clearTasks);
		} catch (SQLException e) {
			log.error("Error while deleting tasks", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.JobDAO#clearJobs()
	 */
	@Override
	public void clearJobs() {
		try(Statement stmt = getConnection().createStatement()) {
			SQLUtils.runSql(stmt, SQLQueries.clearJobs);
		} catch (SQLException e) {
			log.error("Error while deleting jobs", e);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.ResourceDAO#updateCpuCores(java.util.Map)
	 */
	@Override
	public int updateCpuCores(Map<String, List<CpuCoreInfo>> info) {
		String sql = String.format(SQLQueries.getCpuCoresUpdate, updateLastUpdate(lastCpuUpdate));
		int cpuCore = 1;
		int sensorValue = 2;
		int nicIp = 3;
		// from discovery.cores c, discovery.sockets soc, discovery.boards b, discovery.servers serv
		// where sens.core = c.id and c.socket = soc.id and soc.board = b.id and b.server = serv.id and nics.server = serv.id
		/* select sens.core, v.value, nics.ip from discovery.sensors sens, discovery.sensorclasses cla, discovery.sensorvalues v, discovery.nics nics, discovery.cores cor, discovery.sockets soc, discovery.boards b, discovery.servers serv where sens.class = cor.id and sens.core > 0 and sens.core = cor.id and cor.socket = soc.id and soc.board = b.id and b.server = serv.id and nics.server = serv.id and cla.name = 'frequency' and sens.id = v.id */
//		@Override
//		protected String getErrorMessage() {
//			return "Exception while updating CPU properties with " + sql;
//		}
//		@Override
//		public void doWork(Statement stmt) throws Exception {
		int nCount = 0;
		try(Statement stmt = getConnection().createStatement()) {
			// seems that, sensors.host really matches nics.server
			log.debug(sql);
			ResultSet rs = SQLUtils.runSql(stmt, sql, false);
			if(rs == null) {
//				log.info("Stopping timer (success=" + cancel()+")");
				return RESOURCE_UPDATE_FAILED;
			}
			double freq = 0;
			while(rs.next()) {
				String ip = rs.getString(nicIp);
				if(ip.startsWith(LOCALHOST_PREFIX)) {
					continue;
				}
				int coreId = rs.getInt(cpuCore);
				freq = rs.getLong(sensorValue) / 1000.0;
				List<CpuCoreInfo> cores = info.get(ip);
				if(cores == null) {
					cores = new ArrayList<>();
					info.put(ip, cores);
				}
				boolean createNeeded = true;
				for(CpuCoreInfo core : cores) {
					if(core.coreId == coreId) {
						createNeeded = false;
						if(core.frequency != freq) {
							core.frequency = freq;
							core.changed = true;
							nCount++;
						}
					}
				}
				if(createNeeded) {
					CpuCoreInfo core = new CpuCoreInfo();
					core.coreId = coreId;
					core.changed = true;
					core.frequency = freq;
					core.host = ip;
					cores.add(core);
					nCount++;
				}
			}
		} catch (SQLException e) {
			log.error("Exception while updating CPU properties with " + sql, e);
		}
		return nCount;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.ResourceDAO#updateRAM(java.util.Map)
	 */
	@Override
	public int updateRAM(Map<String, RamInfo> info) {
		String sql = String.format(SQLQueries.ramFreeUpdate, updateLastUpdate(lastRamUpdate));
		int nicIp = 1;
		int sensorValue = 2;
		int nCount = 0;
		try(Statement stmt = getConnection().createStatement()) {
			log.debug(sql);
			ResultSet rs = SQLUtils.runSql(stmt, sql, false);
			if(rs == null) {
//				log.info("Stopping timer (success=" + cancel()+")");
				return RESOURCE_UPDATE_FAILED;
			}
			String serverIp = null;
			while(rs.next()) {
				serverIp = rs.getString(nicIp);
				if(serverIp.startsWith(LOCALHOST_PREFIX)) {
					continue;
				}
				double free = rs.getLong(sensorValue) / 1024;
				RamInfo ram = info.get(serverIp);
				if(ram == null) {
					ram = new RamInfo();
					ram.free = -1;
					ram.host = serverIp;
					ram.name = "RAM";
					info.put(serverIp, ram);
				}
				if(ram.free != free) {
					ram.free = free;
					ram.changed = true;
					nCount++;
				}
			}
			rs.close();
		} catch (SQLException e) {
			log.error("Error while updating RAM with " + sql, e);
		}
		return nCount;
	}

	/**
	 * Updates the value for a last update (special care for Dexter, i.e. time stored in microseconds)
	 * @param lastUpdate the atomicLong holding the last update
	 * @return the last update in microseconds
	 */
	private long updateLastUpdate(AtomicLong lastUpdate) {
		return lastUpdate.getAndSet(System.currentTimeMillis())/1000;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.ResourceDAO#updateLoad(java.util.Map)
	 */
	@Override
	public int updateLoad(Map<String, LoadInfo> info) {
		String sql = String.format(SQLQueries.loadUpdate, updateLastUpdate(lastLoadUpdate));
		int sensorClassName = 1;
		int sensorValue = 2;
		int nicIp = 3;
		int nCount = 0;
		try(Statement stmt = getConnection().createStatement()) {
			log.debug(sql);
			ResultSet rs = SQLUtils.runSql(stmt, sql, false);
			if(rs == null) {
//				log.info("Stopping timer (success=" + cancel()+")");
				return RESOURCE_UPDATE_FAILED;
			}
			while(rs.next()) {
				String ip = rs.getString(nicIp);
				if(ip.startsWith(LOCALHOST_PREFIX)) {
					continue;
				}
				LoadInfo load = info.get(ip);
				if(load == null) {
					load = new LoadInfo();
					load.host = ip;
					load.changed = true;
					info.put(ip, load);
				}
				String lx = rs.getString(sensorClassName);
				int value = rs.getInt(sensorValue);
				switch(lx) {
				case "load1": if(value != load.load1) {
					load.load1 = rs.getInt(sensorValue);
					load.changed = true; nCount++; } break;
				case "load5": if(value != load.load5) {
					load.load5 = rs.getInt(sensorValue);
					load.changed = true; nCount++; } break;
				case "load15": if(value != load.load15) {
					load.load15 = rs.getInt(sensorValue);
					load.changed = true; nCount++; } break;
				}
			}
			rs.close();
		} catch (SQLException e) {
			log.error("Exception while updating server load with " + sql, e);
		}
		return nCount;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.SynopseDAO#createSynopse(java.lang.String, java.lang.String, java.lang.String, int, int)
	 */
	@Override
	public long createSynopse(String synopseTableName, String sensorClassName,
			String hostIp, int windowValue, int frequencyValue) {
		String sql = "select s.id from discovery.sensors s, discovery.nics n, discovery.sensorclasses c"
				+ " where s.class = c.id and c.name = '" + sensorClassName
				+ "' and s.host = n.server and n.ip = '" + hostIp + "'";
		long synopseId = SynopseDAO.OPERATION_FAILED;
		try(Statement stmt = getConnection().createStatement()) {
			long sensorId;
			ResultSet rs = SQLUtils.runSql(stmt, sql);
			if(rs.next()) {
				sensorId = rs.getLong(1);
			} else {
				log.error("Could not find sensor-id for sensorClassName " + sensorClassName + " and host-ip " + hostIp);
				return SynopseDAO.OPERATION_FAILED;
			}
			if(synopseTableName == null) {
				synopseTableName = getNewRandomName(sensorClassName);
			}
			SQLUtils.runSql(stmt, "create synopse " + synopseTableName + " on " + sensorId + " window " + windowValue
					+ " frequency " + frequencyValue);
			// ^ further options: quality <q> model maintainance <m> horizon <h> confidence <c>
			rs = SQLUtils.runSql(stmt, "select id from discovery.synopses where name = " + synopseTableName);
			if(rs.next()) { synopseId = rs.getLong(1); }
			else { log.error("Could not find synopse " + synopseTableName); }
		} catch (SQLException e) {
			log.error("Could not create synopse for sensorClassName " + sensorClassName + " and host-ip " + hostIp);
			return SynopseDAO.OPERATION_FAILED;
		}
		return synopseId;
	}

	/**
	 * @param sensorClassName the class of the sensor
	 * @return <code>sensorClassName + System.currentTimeMillis()</code>
	 */
	private String getNewRandomName(String sensorClassName) {
		return sensorClassName + System.currentTimeMillis();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.SynopseDAO#deleteSynopse(long)
	 */
	@Override
	public void deleteSynopse(long synopseId) {
		try(Statement stmt = getConnection().createStatement()) {
			ResultSet rs = SQLUtils.runSql(stmt, "select name from discovery.synopses where id = " + synopseId);
			String synopseTableName;
			if(rs.next()) {
				synopseTableName = rs.getString(1);
			}
			else {
				log.error("Could not find synopse with id " + synopseId);
				return;
			}
			SQLUtils.runSql(stmt, "drop synopse " + synopseTableName);
		} catch (SQLException e) {
			log.error("Could not delete synopse with id " + synopseId);
		}
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.SynopseDAO#getResult(long)
	 */
	@Override
	public List<Double> getResult(long synopseId) {
		String sql = "select timestamp, value from discovery.synopsevalues where id = " + synopseId;
		List<Double> result = new ArrayList<>();
		try(Statement stmt = getConnection().createStatement()) {
			ResultSet rs = SQLUtils.runSql(stmt, sql);
			if(rs.next()) {
				result.add(rs.getDouble(2));
			}
			
		} catch (SQLException e) {
			log.error("Could not get result of synopse with id " + synopseId, e);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.ImplementationDAO#getOrCreateImplementation(java.lang.String)
	 */
	@Override
	public long getOrCreateImplementation(String implName) {
		String querySql = "SELECT id FROM discovery.theatre_implementations WHERE name = '"+implName+"'";
		String insertSql = String.format(Locale.US, 
				"INSERT INTO discovery.theatre_implementations VALUES ('%s', 0, 0.0)",
				implName);
		long implId;
		ResultSet rs;
		try(Statement stmt = getConnection().createStatement()) {
			rs = SQLUtils.runSql(stmt, querySql);
			if(rs.next()) {
				implId = rs.getLong(1);
				return implId;
			}
			else {
				// create new
				SQLUtils.runSql(stmt, insertSql);
				// and query again
				rs = SQLUtils.runSql(stmt, querySql);
				if(rs.next()) {
					implId = rs.getLong(1);
					return implId;
				}
				else {
					// newly created tuple not in db. very strange.
					log.error("Could not find implId from just added impl " + implName);
					return ImplementationDAO.OPERATION_FAILED;
				}
			}
		} catch (SQLException e) {
			log.error("Could not get implementation id for " + implName, e);
			return ImplementationDAO.OPERATION_FAILED;
		}
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.TaskDAO#updateTask(long, java.lang.String)
	 */
	@Override
	public boolean updateTask(long taskId, String status) {
		if(status == null) { return true; }
		String sql = "UPDATE discovery.theatre_tasks SET status = " + TaskInfo.nameToStatusCode(status) +
				" WHERE task_id = " + taskId;
		try(Statement stmt = getConnection().createStatement()) {
			SQLUtils.runSql(stmt, sql);
		} catch (SQLException e) {
			log.error("Could not update task with id " + taskId, e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.JobDAO#updateJob(long, java.lang.Long, java.lang.Long, java.lang.String, java.lang.Long, java.lang.Double, java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean updateJob(long jobId, Long taskId, Long implId,
			String hostIp, Long start, Double predDuration, Long end,
			String status, String result) {
		try(Statement stmt = getConnection().createStatement()) {
			// ignore result parameter
			String sql = "UPDATE  discovery.theatre_jobs SET "
					+ (taskId != null ? "task_id="+taskId : "")
					+ (implId != null ? "impl_id="+implId : "")
					+ (hostIp != null ? "server_id="+getHostIp(stmt, hostIp) : "")
					+ (start != null ? "start="+start : "")
					+ (predDuration != null ? "pred_duration="+predDuration : "")
					+ (end != null ? "end="+end : "")
					+ (status != null ? "status="+JobInfo.nameToStatusCode(status) : "")
					+ " WHERE job_id=" + jobId;
			SQLUtils.runSql(stmt, sql);
		} catch (SQLException e) {
			log.error("Could not update job with id " + jobId, e);
			return false;
		}
		return true;
	}

	/**
	 * @param stmt an existing statmement
	 * @param hostIp the ip to query
	 * @return the id of the host
	 */
	private int getHostIp(Statement stmt, String hostIp) throws SQLException {
		//get server id
		int hostId = 0;
		//rs = stmt.executeQuery("SELECT c.id FROM cores c, sockets s, boards b, servers srv"
		//+ " WHERE srv.name='server 1' AND b.server=srv.id AND s.board=b.id AND c.socket=s.id");
		ResultSet rs = SQLUtils.runSql(stmt, String.format("SELECT min(s.id), count(1) FROM discovery.servers s, discovery.nics n"
				+ " WHERE n.ip='%s' and s.id = n.server", hostIp));
		if(rs.next()) {
			hostId = rs.getInt(1);
			int count = rs.getInt(2);
			if(count > 1) {
				log.warn(String.format("Got multiple (%d) servers with the name '%s'", count, hostIp));
			}
		}
		else {
			log.error(String.format("Could not retrieve id for server '%s'. Exiting", hostIp));
			return 0;
		}
		return hostId;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.dao.AbstractDAO#isReady()
	 */
	@Override
	public boolean isReady() {
		return ready;
	}

	/* Prototype for querying the database
		try(Statement stmt = getConnection().createStatement()) {
			
		} catch (SQLException e) {
			log.error("for " + null, e);
			return false;
		}
	 */
}
