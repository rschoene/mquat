/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.dao.dexter;

import org.haec.theatre.dao.JobInfo;
import org.haec.theatre.dao.TaskInfo;

/**
 * 
 * @author René Schöne
 */
public class SQLQueries {

	static final int index_job_id = 1;
	static final int index_task_id = 2;
	static final int index_start = 3;
	static final int index_pred_duration = 4;
	static final int index_end = 5;
	static final int index_status = 6;
	static final int index_implName = 7;
	static final int index_hostName = 8;

	public static final String clearImplementations = "DELETE FROM discovery.theatre_implementations";

	static String makeJobQuery(Long jobIdRequest, Long taskIdRequest) {
		String sql = "SELECT j.job_id, j.task_id, j.start, j.pred_duration, j.end, j.status, i.name, s.name"
				+ " FROM discovery.theatre_jobs j, discovery.theatre_implementations i, discovery.servers s"
				+ " WHERE j.status > " + JobInfo.STATUS_DUMMY_CODE + " AND j.impl_id = i.id AND j.server_id = s.id";
		if(jobIdRequest != null) {
			sql += " AND job_id =" + jobIdRequest.longValue();
		}
		if(taskIdRequest != null) {
			sql += " AND task_id =" + taskIdRequest.longValue();
		}
		return sql;
	}
	
	static String makeTaskQuery(Long taskIdRequest) {
		String sql = "SELECT task_id, status FROM discovery.theatre_tasks WHERE status > " + TaskInfo.STATUS_CREATED_CODE;
		if(taskIdRequest != null) { sql += " AND task_id = " + taskIdRequest.longValue(); }
		return sql;
	}

	/**
	 * 
	 */
	static final String loadUpdate = "select c.name, v.value, nics.ip"
			+ " from discovery.sensors s, discovery.sensorvalues v, discovery.sensorclasses c, discovery.nics nics"
			+ " where s.id = v.id and s.server > 0 and s.server = nics.server and s.class = c.id and v.timestamp > %s"
			+ " and v.value > 0 order by nics.ip";
	/**
	 * 
	 */
	static final String ramFreeUpdate = "select nics.ip, v.value"
			+ " from discovery.sensors s, discovery.sensorvalues v, discovery.sensorclasses c,"
			+ " discovery.sockets soc, discovery.boards boards, discovery.nics nics"
			+ " where s.id = v.id and s.memory = soc.memory and soc.board = boards.id and boards.server = nics.server"
			+ " and s.class = c.id and c.name = 'free' and s.memory > 0 and v.timestamp > %s";
	/**
	 * 
	 */
	static final String getCpuCoresUpdate = "select sens.core, v.value, nics.ip"
			+ " from discovery.sensors sens, discovery.sensorclasses c, discovery.sensorvalues v, discovery.nics nics"
			+ " where sens.class = c.id and sens.core > 0 and c.name = 'frequency' and sens.id = v.id"
			+ " and sens.host = nics.server and v.timestamp > %s";
	/**
	 * 
	 */
	static final String clearJobs = "DELETE FROM discovery.theatre_jobs";
	/**
	 * 
	 */
	static final String clearTasks = "DELETE FROM discovery.theatre_tasks";

}
