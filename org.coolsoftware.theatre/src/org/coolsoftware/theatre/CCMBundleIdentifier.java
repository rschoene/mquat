/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre;

import java.awt.Component;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.ecl.EclContract;


/**
 * This interface contains {@link String} identifiers for the CCM to Bundle Mapping. It can be used to access to CCM information of a certain Bundle. It is assumed that each 
 * {@link ComponentType} and each {@link Component} is represented as an individual OSGi Bundle with an extended
 * Manifest:
 * 
 * The Manifest.MF of ComponentType Bundles are extended with {@value #CCM_TYPE} and the name of the {@link ComponentType} as specified in the {@link StructuralModel}. 
 * 
 * The Manifest.MF of Component Bundles (Variants) are extended with {@value #CCM_VARIANT} and {@link #CCM_VARIANT_TYPE}. 
 * {@value #CCM_VARIANT} corresponds with the name of the {@link EclContract} whereas 
 * {@link #CCM_VARIANT_TYPE} as specifies the {@link ComponentType} of the variant as indicated in the {@link StructuralModel}.
 * 
 * 
 * @author Sebastian Cech
 *
 */

public interface CCMBundleIdentifier {

	/**
	 * Identifies a {@link ComponentType} Bundle
	 */
	public static final String CCM_TYPE = "CCM-SWComponentType";
	
	/**
	 * Identifies a {@link Component} Bundle of a specific {@link ComponentType}. The {@link org.coolsoftware.coolcomponents.ccm.variant.Component#getSpecification()}
	 * relation is stored using {@link #CCM_VARIANT_TYPE}
	 */
	public static final String CCM_VARIANT = "CCM-Variant";
	
	/**
	 * Specifies the Variant-Type relationship for a Bundle ({@link org.coolsoftware.coolcomponents.ccm.variant.Component#getSpecification()}).
	 * 
	 */
	public static final String CCM_VARIANT_TYPE = "CCM-VariantType";

}