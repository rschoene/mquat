/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre;

import java.io.File;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * 
 * @author René Schöne
 */
public class EmfFileSetup implements TheatreStartupHook {

	Logger log = Logger.getLogger(EmfFileSetup.class);
	private boolean init;
	
	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.TheatreStartupHook#runSetup()
	 */
	@Override
	public boolean runSetup() {
		if(init) {
			return true;
		}
		try {
			org.eclipse.emf.common.util.URI platform = org.eclipse.emf.common.util.URI.createURI("theatre/").resolve(
					org.eclipse.emf.common.util.URI.createFileURI((new File(".")).getAbsolutePath()));
			EcorePlugin.getPlatformResourceMap().put("theatre", platform);
			init = true;
		} catch (Exception e) {
			log.error("Not good.", e);
			return false;
		}
		return true;
	}

}
