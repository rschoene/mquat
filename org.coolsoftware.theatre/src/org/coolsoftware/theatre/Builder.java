/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * Small application to copy plugins contained in the config.ini
 * @author Sebastian Götz
 */
public class Builder {
	public static void main(String[] args) {
		String strDestDir = "C:/dev/coolsw-distrib/theatre";
		
		Properties props = new Properties();
		try {
			props.load(new FileReader(new File("config.ini")));
			String bundles = props.get("osgi.bundles").toString();
			for(String str : bundles.split("reference\\:file\\:")) {
				if(str.length() > 0) {
					if(str.indexOf(",") != -1) str = str.substring(0,str.length()-1);
					String[] bdl = str.split("@");
					if(bdl.length == 2)
						System.out.println("## "+bdl[0]+" || "+bdl[1]);
					else 
						System.out.println("## "+bdl[0]);
					
					if(bdl[0].endsWith(".jar")) {
						//copy file
						File src = new File(bdl[0]);
						File dest = new File(strDestDir+bdl[0].substring(bdl[0].indexOf("/plugins/")));
						
						//if(!dest.exists()) dest.createNewFile();
						
						System.out.println(">> "+dest);
						FileReader fr = new FileReader(src);
						FileWriter fw = new FileWriter(dest,true);
						
						int c = 0;
						while((c = fr.read()) != -1) {
							fw.write(c);
						}
						fr.close();
						fw.close();
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
