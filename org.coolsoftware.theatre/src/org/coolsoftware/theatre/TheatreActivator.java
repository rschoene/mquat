/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.apache.log4j.MDC;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator of THEATRE setting logging hostname and emf resource mappings.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class TheatreActivator implements BundleActivator {

	/** Key used by log4j to print hostname */
	public static final String KEY_HOSTNAME = "hostname";
	/** Constant to used if fetching of hostname failed */
	private static final String UNKNOWN_HOSTNAME = "unknown";

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		TheatreActivator.context = bundleContext;
		/* Get hostname and set it in logging facility */
		String hostname = getHostName();
		MDC.put(KEY_HOSTNAME, hostname);
		/*  Register a mapping between emf platform resources and its physical location.
		 * 	This mapping makes it possible to use the method
		 *  URI.createPlatformResourceURI("/theatre/filename.extension", false) to write emf models
		 *  instead absolute URIs. Further, it hopefully avoids absolute references in emf models.
		 *  This location is mapped to: <rootFolderOfRuntime>/theatre (e.g. c:\dev\eclipse\theatre)
		 */
		org.eclipse.emf.common.util.URI platform = org.eclipse.emf.common.util.URI.createURI("theatre/").resolve(
				org.eclipse.emf.common.util.URI.createFileURI((new File(".")).getAbsolutePath()));
		EcorePlugin.getPlatformResourceMap().put("theatre", platform);
	}

	private String getHostName() {
		String hostname = System.getenv().get("HOSTNAME");
		if (hostname!=null && !hostname.trim().equals("")) return hostname;

		// support windows server 2003
		hostname = System.getenv().get("COMPUTERNAME");
		if (hostname!=null && !hostname.trim().equals("")) return hostname;

		// try to run "hostname" command
		InputStream is;
		try {
			is = Runtime.getRuntime().exec("hostname").getInputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
			return UNKNOWN_HOSTNAME;
		}
		try(Scanner s = new Scanner(is)) {
			if (s.useDelimiter("\\A").hasNext()) {
				hostname = s.next().trim();
			}
		}
		return hostname;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		TheatreActivator.context = null;
	}

}
