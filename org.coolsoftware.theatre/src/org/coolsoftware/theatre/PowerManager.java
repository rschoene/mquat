/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre;

/**
 * An entity to power on and off hosts.
 * @author René Schöne
 */
public interface PowerManager {
	
	/** Unknown state. */
	public static final int UNKNOWN = 0;
	/** Host is up and running. */
	public static final int ONLINE = 1;
	/** Host is currently booting. */
	public static final int BOOTING = 2;
	/** Host is offline. */
	public static final int OFFLINE = 3;
	/** Host is shutting down. */
	public static final int SHUTING_DOWN = 4;
	/** Value returned for measurement information upon previous errors */
	public static final int ERROR_VALUE = -1;

	/**
	 * Powers on the given host. Blocks until powered on completely.
	 * If successful, the host will be in state {@link #ONLINE}.
	 * @param hostname the hostname to power on
	 * @return whether successful
	 */
	public boolean powerOnBlocking(String hostname);

	/**
	 * Powers on the given host asynchronously, i.e. returns immediately.
	 * If successful, the host will be in state {@link #BOOTING}.
	 * @param hostname the hostname to power on
	 * @return whether successful
	 */
	public boolean powerOnAsynchronous(String hostname);

	/**
	 * Shut down the given host. Blocks until powered off completely.
	 * If successful, the host will be in state {@link #OFFLINE}.
	 * @param hostname the hostname to shut down
	 * @return whether successful
	 */
	public boolean shutdownBlocking(String hostname);

	/**
	 * Shut down the given host asynchronously, i.e. returns immediately.
	 * If successful, the host will be in state {@link #SHUTING_DOWN}.
	 * @param hostname the hostname to shut down
	 * @return whether successful
	 */
	public boolean shutdownAsynchronous(String hostname);
	
	/**
	 * @param hostname the hostname to inspect
	 * @return the current state, as described by the constants in {@link PowerManager}.
	 * @see #UNKNOWN
	 * @see #OFFLINE
	 * @see #BOOTING
	 * @see #ONLINE
	 * @see #SHUTING_DOWN
	 */
	public int getState(String hostname);
	
	/**
	 * @param hostname the affected hostname
	 * @return currently drawn current in mA, or {@link #ERROR_VALUE}
	 */
	public int getDrawnCurrent(String hostname);
	
	/**
	 * @param hostname the affected hostname
	 * @return total energy used since start in Wh, or {@link #ERROR_VALUE}
	 */
	public float getTotalConsumedEnergy(String hostname);
	
	/**
	 * @param hostname the affected hostname
	 * @return time since connection in seconds, or {@link #ERROR_VALUE}
	 */
	public int getUptime(String hostname);
	
	/**
	 * @param hostnameEqualIp
	 */
	public void setHostnameEqualIp(boolean hostnameEqualIp);

	/**
	 * @return hostnameEqualIp
	 */
	public boolean isHostnameEqualIp();

}
