/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * 
 */
package org.coolsoftware.theatre.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;
import org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPackage;
import org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl;
import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.CcmImport;
import org.coolsoftware.ecl.EclFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.EclPackage;
import org.coolsoftware.ecl.Import;
import org.coolsoftware.ecl.resource.ecl.EclParserFacade;
import org.coolsoftware.ecl.resource.ecl.mopp.EclResource;
import org.coolsoftware.reconfiguration.ReconfigurationPackage;
import org.coolsoftware.reconfiguration.ReconfigurationPlan;
import org.coolsoftware.requests.RequestsPackage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryRegistryImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

/**
 * Provides helper methods to handle CCM models at runtime.
 * 
 * @author Sebastian Cech
 */
public class CCMUtil {
	public static final String APP_STRUCTURE_PREFIX_PATH = "/theatre/Apps/";
	// PATH for URI construction
	public static final String CURRENT_INFRASTRUCTURE_VARIANT_PATH = "/theatre/currentInfrastructure.variant";
	public static final String RESOURCESTRUCTURE_PATH = "/theatre/ServerD.structure";
	public static final String SENT_SERVER_DATA_VARIANT_PATH = "/theatre/SentServerData.variant";
	public static final String THIS_SERVER_VARIANT_PATH = "/theatre/thisserver.variant";
	/**
	 * Create a new {@link ResourceSet} with registered CCM related packages, i.e. 
	 * <li> {@link StructurePackage}</li>
	 * <li> {@link VariantPackage} </li> 
	 * <li> {@link BehaviorPackage} </li>
	 * <li> {@link EclPackage} </li>
	 * 
	 * @return A {@link ResourceSet} that is prepared to process CCM models
	 */
	public static ResourceSet createResourceSet(){
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
				ResourceFactoryRegistryImpl.DEFAULT_EXTENSION, new EcoreResourceFactoryImpl());
		rs.getPackageRegistry().put("structure", StructurePackageImpl.eINSTANCE);
		rs.getPackageRegistry().put("variant", VariantPackageImpl.eINSTANCE);
		rs.getPackageRegistry().put("behavior", VariantPackageImpl.eINSTANCE);
		rs.getPackageRegistry().put("ecl", EclPackage.eINSTANCE);
		rs.getPackageRegistry().put("request", RequestsPackage.eINSTANCE);
		rs.getPackageRegistry().put("config", ReconfigurationPackage.eINSTANCE);
		return rs;
	}
	
	
	/**
	 * Create an empty {@link VariantModel} with a {@link Resource} as container in order to allow EMF based serialization. 
	 * The {@link Resource} is added to the same  {@link ResourceSet} as the {@link StructuralModel} for the application.
	 * @param rs The {@link ResourceSet} containing the {@link StructuralModel} of the application
	 * @param appName The application name that is used to create the {@link Resource} URI
	 * @return An empty {@link VariantModel}.
	 */
	public static VariantModel createSWVariantModel(ResourceSet rs, String appName){
//		URI varURI = URI.createPlatformResourceURI(APP_STRUCTURE_PREFIX_PATH + appName + ".variant", false);
//		URI varURI = URI.createPlatformResourceURI(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName +".variant", false);
		URI varURI = EcorePlugin.resolvePlatformResourcePath(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName +".variant");
		Resource res = rs.createResource(varURI);
		VariantModel vm = VariantFactory.eINSTANCE.createVariantModel();
		res.getContents().add(vm);
		return vm;
	}
	
	public static EclFile deserializeECLContract(StructuralModel appModel, StructuralModel hwModel, String appName,
			String contractId, String serializedContract, boolean persist) throws IOException{
		Set<StructuralModel> visibleStructuralModels = new HashSet<StructuralModel>();
		visibleStructuralModels.add(appModel);
		visibleStructuralModels.add(hwModel);
		EclResource eclres = EclParserFacade.parseEclFile(serializedContract, visibleStructuralModels);
//		eclres.setURI(eclUri);
		eclres.setURI(URI.createURI(contractId));
		
		// rewrite imports
		EclFile eclFile = (EclFile) eclres.getContents().get(0);
		EList<Import> impList = eclFile.getImports();
		impList.clear();
		
		CcmImport hwimp = EclFactory.eINSTANCE.createCcmImport();
		hwimp.setCcmStructuralModel(hwModel);
		impList.add(hwimp);
		
		CcmImport appimp = EclFactory.eINSTANCE.createCcmImport();
		appimp.setCcmStructuralModel(appModel);
		impList.add(appimp);
		
		if(persist)
			persistResource(eclres);
		
		return eclFile;
	}
	
	/**
	 * Deserializes a VariantModel representing a concrete IT infrastructure with the {@link URI} 
	 *  plattform://resource/theatre/currentInfrastructure.variant. As the elements of the {@link VariantModel}
	 *  hold references to a {@link StructuralModel} the serialized {@link StructuralModel} is deserialized and persisted
	 *  at first. Thereafter, the {@link VariantModel} deserialized.
	 * @param rs The common {@link ResourceSet}
	 * @param serializedStructModel The serialized {@link StructuralModel}
	 * @param serializedVariantModel The serialized {@link VariantModel}
	 * @param persist must be <code>true</code> if the model should be saved. 
	 * @return the deserialized {@link VariantModel}
	 * @throws IOException
	 */
	public static VariantModel deserializeGlobalHWVariantModel(ResourceSet rs, String serializedStructModel, String serializedVariantModel, boolean persist) throws IOException{
//		URI ccmVarURI = URI.createPlatformResourceURI(CURRENT_INFRASTRUCTURE_VARIANT_PATH, false);
		URI ccmVarURI = EcorePlugin.resolvePlatformResourcePath(CURRENT_INFRASTRUCTURE_VARIANT_PATH);
		VariantModel  varModel = deserializeVariantModel(rs, serializedStructModel, serializedVariantModel, ccmVarURI); 
		// persist as file if needed
		if(persist)
			persistResource(varModel.eResource());
		return varModel;
	}
	/**
	 * Invokes {@link #deserializeGlobalHWVariantModel(ResourceSet, String, String, boolean)} without any {@link ResourceSet} (<code>null</code>),
	 * which means a new {@link ResourceSet} is created. 
	 * @param rs The common {@link ResourceSet}
	 * @param serializedStructModel The serialized {@link StructuralModel}
	 * @param serializedVariantModel The serialized {@link VariantModel}
	 * @param persist must be <code>true</code> if the model should be saved. 
	 * @return the deserialized {@link VariantModel}
	 * @throws IOException
	 */
	public static VariantModel deserializeGlobalHWVariantModel(String serializedStructModel, String serializedVariantModel, boolean persist) throws IOException{
		return deserializeGlobalHWVariantModel(null, serializedStructModel, serializedVariantModel, persist);
	}

	/**
	 * Deserializes a {@link StructuralModel} representing {@link ResourceType}s of an IT infrastructure with the 
	 * {@link URI} plattform://resource/theatre/ServerD.structure.
	 * This method provides only deserialization functionality and does not store anything to the HDD. For persisting 
	 * the {@link StructuralModel} the method {@link #persistResource(Resource)} should be used.
	 * This method invokes at first the {@link #deserializeStructModel(ResourceSet, URI, String)} method and therafer the
	 * {@link #persistResource(Resource)} method, which despends on the persist attribtue.
	 * 
	 * @param rs The {@link ResourceSet} where the {@link StructuralModel} should be collected.
	 * @param serializedStructModel the serialized {@link StructuralModel}
	 * @param persist if persist is <code>true</code> the model is saved to the disk
	 * @return the deserialized {@link StructuralModel}.
	 * @see #deserializeStructModel(ResourceSet, URI, String)
	 * @throws IOException
	 */
	public static StructuralModel deserializeHWStructModel(ResourceSet rs, String serializedStructModel, boolean persist) throws IOException {
		// create a URI for the serialized model 
//		URI ccmURI = URI.createPlatformResourceURI(RESOURCESTRUCTURE_PATH, false);
		URI ccmURI = EcorePlugin.resolvePlatformResourcePath(RESOURCESTRUCTURE_PATH);
		StructuralModel model = deserializeStructModel(rs, ccmURI, serializedStructModel);
		
		if(persist){
			persistResource(model.eResource());
		}
		
		return model;
	}
	
	/**
	 * Invokes the method {@link #deserializeHWStructModel(ResourceSet, String, boolean)}
	 * @see #deserializeHWStructModel(ResourceSet, String, boolean)
	 * @throws IOException
	 */
	public static StructuralModel deserializeHWStructModel(String serializedStructModel, boolean persist) throws IOException {
		return deserializeHWStructModel(null, serializedStructModel, persist);
	}
	
	/**
	 * Deserializes a VariantModel representing a concrete IT infrastructure with the {@link URI} 
	 *  plattform://resource/theatre/currentInfrastructure.variant. As the elements of the {@link VariantModel}
	 *  hold references to a {@link StructuralModel} the serialized {@link StructuralModel} is deserialized and persisted
	 *  at first. Thereafter, the {@link VariantModel} deserialized. 
	 * @param serializedStructModel The serialized {@link StructuralModel}
	 * @param serializedVariantModel The serialized {@link VariantModel}
	 * @param persist must be <code>true</code> if the model should be saved.
	 * @return the deserialized {@link VariantModel}
	 * @throws IOException
	 */
	public static VariantModel deserializePartialHWVariantModel(String serializedStructModel, String serializedVariantModel, boolean persist) throws IOException{
//		URI ccmVarURI = URI.createPlatformResourceURI(SENT_SERVER_DATA_VARIANT_PATH, false);
		URI ccmVarURI = EcorePlugin.resolvePlatformResourcePath(SENT_SERVER_DATA_VARIANT_PATH);
		VariantModel  varModel = deserializeVariantModel(null, serializedStructModel, serializedVariantModel, ccmVarURI);
		if(persist)
			persistResource(varModel.eResource());
		return varModel;
	}

	/**
	 * Deserialize a String representation of a {@link StructuralModel} to the given URI. 
	 * This method provides only deserialization functionality and does not store anything to the HDD.
	 * @param rs The {@link ResourceSet} where models should be collected. If rs is <code>null</code> a new {@link ResourceSet} is created
	 * @param targetURI The target {@link URI} of the {@link StructuralModel}
	 * @param serializedStructModel The {@link String} representation of the {@link StructuralModel}
	 * @return A {@link StructuralModel} that can be accessed using the targetURI.
	 * @throws IOException
	 */
	private static StructuralModel deserializeStructModel(ResourceSet rs, URI targetURI, String serializedStructModel) throws IOException {
		//create an inputstream of the serialized model
		ByteArrayInputStream bStruct = new ByteArrayInputStream(serializedStructModel.getBytes());
		//load it using emf + emftext factory
		EObject result = null;
		if(rs==null)
			rs = createResourceSet();
		
		
		org.eclipse.emf.ecore.resource.Resource resource = rs.getResource(targetURI, false);
		/* Check if the resource has already been created. */
		if (resource == null) {
			resource = rs.createResource(targetURI);
		}
		// no else.
	
		if (!resource.isLoaded())
			resource.load(bStruct, null);
		// no else.
	
		if (resource.getContents().size() > 0) {
			result = resource.getContents().get(0);
		}
		
		return (StructuralModel)result;
	}
	
	public static StructuralModel deserializeSWStructModel(String appName, String serializedStructModel, boolean persist) throws IOException {
		return deserializeSWStructModel(null, appName, serializedStructModel, persist);
	}

	public static StructuralModel deserializeSWStructModel(ResourceSet rs, String appName, String serializedStructModel, boolean persist) throws IOException {
//		URI appURI = URI.createPlatformResourceURI(APP_STRUCTURE_PREFIX_PATH + appName + ".structure", false);
		URI appURI = EcorePlugin.resolvePlatformResourcePath(APP_STRUCTURE_PREFIX_PATH + appName + ".structure");
		StructuralModel struct = deserializeStructModel(rs, appURI, serializedStructModel);
//		appURI = URI.createPlatformResourceURI(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName +".structure", false);
		appURI = EcorePlugin.resolvePlatformResourcePath(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName +".structure");
		struct.eResource().setURI(appURI);
		if(persist)
			persistResource(struct.eResource());
		return struct;
	}
	
	public static VariantModel deserializeSWVariantModel(ResourceSet rs, String appName, String serializedVariantModel, boolean persist) throws IOException{
		return deserializeSWVariantModel(rs, appName, "", serializedVariantModel, persist);
	}
	public static VariantModel deserializeSWVariantModel(ResourceSet rs, String appName, String uriSuffix,  String serializedVariantModel, boolean persist) throws IOException{
//		URI varURI = URI.createPlatformResourceURI(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName + uriSuffix +".variant", false);
		URI varURI = EcorePlugin.resolvePlatformResourcePath(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName + uriSuffix +".variant");
		VariantModel vm = deserializeVariantModel(rs, null, serializedVariantModel, varURI);
		if(persist)
			vm.eResource().save(null);
		return vm;
	}

	/**
	 * Deserialize a String representation of a {@link VariantModel} to the given URI. 
	 * As the elements of the {@link VariantModel}
	 *  hold references to a {@link StructuralModel} the serialized {@link StructuralModel} is deserialized and persisted
	 *  at first. Thereafter, the {@link VariantModel} deserialized.
	 * This method provides only deserialization functionality and does not store anything to the HDD.
	 * @param rs The {@link ResourceSet} where models should be collected. If rs is <code>null</code> a new {@link ResourceSet} is created
	 * @param targetURI The target {@link URI} of the {@link StructuralModel}
	 * @param serializedStructModel The {@link String} representation of the {@link StructuralModel}
	 * @return A {@link StructuralModel} that can be accessed using the targetURI.
	 * @throws IOException
	 */
	private static VariantModel deserializeVariantModel(ResourceSet rs, String serializedStructModel, String serializedVariantModel, URI varModelUri) throws IOException{
		if(rs==null){
			StructuralModel struct = deserializeHWStructModel(serializedStructModel, false);
			rs = struct.eResource().getResourceSet();
		}
		
		//create an inputstream of the serialized model
		ByteArrayInputStream bStruct = new ByteArrayInputStream(serializedVariantModel.getBytes());
		//load it using emf + emftext factory
		EObject result = null;
		Resource resource = rs.createResource(varModelUri);
		if (!resource.isLoaded())
			resource.load(bStruct, null);
		// no else.
	
		if (resource.getContents().size() > 0) {
			result = resource.getContents().get(0);
		}
		
		return (VariantModel) result;
	}

	/**
	 * Save an {@link Resource} to the disk with its internal {@link URI}
	 * @param eResource The resource to be persisted.
	 * @throws IOException
	 */
	public static void persistResource(Resource eResource) throws IOException {
		eResource.save(null);
		
	}

	/**
	 * A helper that serializes an {@link EObject} by using the {@link #serializeResource(Resource)} method.
	 * @param obj The {@link EObject} of a {@link Resource} that should be serialized
	 * @return The serialized {@link Resource}.
	 * @throws IOException
	 */
	public static String serializeEObject(EObject obj) throws IOException{
		return serializeResource(obj.eResource());
	}

	/**
	 * Serialize a {@link Resource} into a String by using a {@link ByteArrayOutputStream}
	 * @param res The {@link Resource} to be serialized.
	 * @return A {@link String} representation of the {@link Resource}.
	 * @throws IOException
	 */
	public static String serializeResource(Resource res) throws IOException{
		String serializedResource = "";
		ByteArrayOutputStream out = new ExcludeBadCharsStream();
		res.save(out, null);
		serializedResource = out.toString();
		
		return serializedResource;
	}
	
	private static class ExcludeBadCharsStream extends ByteArrayOutputStream {

		@Override
		public synchronized void write(byte[] b, int off, int len) {
			for (int i = 0; i < len; i++) {
				byte by = b[off+i];
				//Basically, all characters below 0x20 is disallowed, except 0x9 (TAB), 0xA (CR?), 0xD (LF?)
				if(by < 0x20 && by != 0x9 && by != 0xA && by != 0xD) {
					b[off+i] = 0x20; // set to SPACE
				}
			}
			super.write(b, off, len);
		}
	}

	/**
	 * Deserialize a {@link String} representation of a {@link ReconfigurationPlan}. So far, this method is tested XMI deserialization.
	 * @param plan The {@link ReconfigurationPlan} as {@link String} (XMI)
	 * @param appName The application identifier (used for URI creation)
	 * @return The {@link ReconfigurationPlan}
	 * @throws IOException
	 */
	public static ReconfigurationPlan deserializeReconfigurationPlan(String plan, String appName) throws IOException {
//		URI planURI = URI.createPlatformResourceURI(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName + ".config", false);
		URI planURI = EcorePlugin.resolvePlatformResourcePath(APP_STRUCTURE_PREFIX_PATH + appName +"/" + appName + ".config");
		ResourceSet rs = createResourceSet();
		//create an inputstream of the serialized model
		ByteArrayInputStream bPlan = new ByteArrayInputStream(plan.getBytes());
		//load it using emf + emftext factory
		EObject result = null;
		Resource resource = rs.createResource(planURI);
		if (!resource.isLoaded())
			resource.load(bPlan, null);
		// no else.
	
		if (resource.getContents().size() > 0) {
			result = resource.getContents().get(0);
		}
		persistResource(resource);
		
		return (ReconfigurationPlan) result;
	}
	
	/** @see #getLongValue(CcmValue, boolean) */
	public static long getLongValue(CcmValue value) {
		return getLongValue(value, true);
	}

	/** @return {@link CcmInteger#getIntegerValue()} -> {@link CcmReal#getRealValue()} -> 
	 * ({@link CcmBoolean#isBooleanValue()} ? 1 : 0) ->
	 * {@link Integer#parseInt(String)} for {@link CcmString#getStringValue()} -> -1 */
	public static long getLongValue(CcmValue value, boolean ignoreErrors) {
		long result = -1;
		if(value instanceof CcmInteger) {
			result = ((CcmInteger) value).getIntegerValue();
		}
		else if(value instanceof CcmReal) {
			// cast double to long
			result = (long) ((CcmReal) value).getRealValue();
		}
		else if(value instanceof CcmBoolean) {
			if(((CcmBoolean) value).isBooleanValue()) {
				result = 1;
			}
			else {
				result = 0;
			}
		}
		else if(value instanceof CcmString) {
			// try to parse
			try {
				result = Integer.parseInt(((CcmString) value).getStringValue());
			} catch (NumberFormatException e) {
				if(!ignoreErrors) {
					e.printStackTrace();
				}
			}
		}
		if(!ignoreErrors) {
			System.err.println("[CCMUtil] Unknown subclass: " + value.getClass().getCanonicalName());
		}
		return result;
	}

	/** @see #getRealValue(CcmValue, boolean) */
	public static double getRealValue(CcmValue value) {
		return getRealValue(value, true);
	}

	/** @return {@link CcmInteger#getIntegerValue()} [!= 0] -> {@link CcmInteger#getRealValue()} -> {@link CcmReal#getRealValue()}
	 * -> -1 */
	public static double getRealValue(CcmValue value, boolean ignoreErrors) {
		double result = -1;
		if(value != null) {
			if(value instanceof CcmInteger) {
				long intValue = ((CcmInteger) value).getIntegerValue();
				if(intValue != 0)
					result = intValue;
				else
					result = ((CcmInteger) value).getRealValue();
			}
			else if(value instanceof CcmReal) {
				result = ((CcmReal) value).getRealValue();
			}
			else {
				if(!ignoreErrors) {
					System.err.println("[CCMUtil] Neither integer nor real: " + value);
				}
			}
		}
		return result;
	}

}
