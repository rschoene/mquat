/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.util;

import java.io.IOException;
import java.util.List;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment;
import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.resource.ecl.IEclTextResource;
import org.coolsoftware.ecl.resource.ecl.mopp.EclPrinter2;
import org.coolsoftware.requests.MetaParamValue;

/**
 * Utility class providing methods to get CcmValue from a Statement and processing this value further.
 * @author Sebastian Götz
 */
public class GeneratorUtil {

	public static CcmValue evalFormula(Statement stmt,
			List<MetaParamValue> metaParamValues) {
		FunctionExpression fe = (FunctionExpression)stmt;
		if(fe.getSourceExp() instanceof VariableAssignment) {
			VariableAssignment va = (VariableAssignment)fe.getSourceExp();
			Expression e = va.getValueExpression();
//			System.out.print(">>> Formula: ");
//			try {
//				EclPrinter2 printer = new EclPrinter2(System.out, (IEclTextResource) e.eResource());
//				printer.print(e);
//				System.out.flush();
//				System.out.println();
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
			CcmValue v = new CcmExpressionInterpreter().interpret(e);
//			System.out.println("<<<"+ v);
			return v;
		} else {
			System.out.println("curious function expression: ");
			EclPrinter2 printer = new EclPrinter2(System.out, (IEclTextResource) fe.eResource());
			try {
				printer.print(fe);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.flush();
			System.out.println();
		}
		return null;
		
	}

	public static String cleanString(String orig) {
		String ret = orig.replace(" ", "");
		ret = ret.replace("(", "[");
		ret = ret.replace(")", "]");
		return ret;
	}

	public static String formatCcmValue(CcmValue v) {
		if (v instanceof CcmInteger) {
			long val = ((CcmInteger) v).getIntegerValue();
			if(val == 0) {
				return "" + ((CcmInteger) v).getRealValue();
			} else {
				return "" + ((CcmInteger) v).getIntegerValue();
			}
		} else if (v instanceof CcmReal) {
			return "" + ((CcmReal) v).getRealValue();
		} else if (v instanceof CcmBoolean) {
			return (((CcmBoolean) v).isBooleanValue() ? "1" : "0");
		} else {
			return v.toString();
		}
	}

	public static long getCcmValueAsInt(CcmValue v) {
		if (v instanceof CcmReal) {
			if(((CcmReal) v).getRealValue() == 0) return ((CcmInteger) v).getIntegerValue();
			else return Math.round(((CcmReal) v).getRealValue());
		} else if (v instanceof CcmInteger) {
			return ((CcmInteger) v).getIntegerValue();
		} else if (v instanceof CcmBoolean) {
			return (((CcmBoolean) v).isBooleanValue() ? 1 : 0);
		} else {
			return 0;
		}
	}

}
