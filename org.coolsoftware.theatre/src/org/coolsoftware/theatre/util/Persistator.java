/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.util;

import java.util.List;

import org.coolsoftware.coolcomponents.ccm.variant.Job;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.requests.Request;

/**
 * Helper class to persist several types within the CoolComponentModel.
 * @author René Schöne
 */
public interface Persistator {

	/**
	 * Takes a contract file and persists it.
	 * @param eclFile the contract file, possibly still containing formulas
	 * @return whether successful
	 */
	public boolean persist(Request request);
	
	/**
	 * Takes a given request and persists it.
	 * @param request the request to be persisted
	 * @return whether successful
	 */
	public boolean persist(EclFile eclFile);
	
	/**
	 * Takes a contract file, computes the concrete values with the values of the
	 * given request and persists the new file.
	 * @param eclFile the contract file, possibly still containing formulas
	 * @param request the request with values for the metaparameters
	 * @return whether successful
	 */
	public boolean combineAndPersist(EclFile eclFile, Request request);
	
	/**
	 * Takes a given variant and persists it.
	 * @param serializedVariantModel the variant model to be persisted
	 * @param serializedStructModel the structural model (to deserialize the variant model)
	 * @param filename a description where to put the output
	 * @return whether successful
	 */
	public boolean persistModelToJson(String serializedVariantModel, String serializedStructModel,
			String filename);

	/**
	 * Takes a given variant and persists only the jobs, possibly even only the jobs with the given taskId.
	 * @param serializedVariantModel the variant model to be persisted
	 * @param serializedStructModel the structural model (to deserialize the variant model)
	 * @param filename a description where to put the output
	 * @param taskId the taskId to search for, or <code>null</code> to take all jobs
	 * @return whether successful
	 */
	public boolean persistJobsToJson(String serializedVariantModel, String serializedStructModel,
			String filename, Long taskId);

	/**
	 * Returns all jobs contained in the given variant model (software or hardware).
	 * May be constrained by passing non-<code>null</code> value for taskId.
	 * @param model the model to inspect
	 * @param taskId the taskId of all jobs, or <code>null</code> for all tasks
	 * @return those jobs with the given taskId, or all jobs, if taskId is <code>null</code>
	 */
	public abstract List<Job> getJobs(VariantModel model, final Long taskId);

}
