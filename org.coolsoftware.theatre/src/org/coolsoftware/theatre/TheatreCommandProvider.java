/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * An entity able to execute commands.
 * For each TheatreCommandProvider, the {@link RemoteOSGiService#connect(ch.ethz.iks.r_osgi.URI)} method should be invoked
 * to ensure a proxy service is created.
 * @author René Schöne
 */
public interface TheatreCommandProvider {
	
	/** The property name to filter for providers offering a command. */
	public static final String PROPERTY_COMMAND_NAME = "org.coolsoftware.theatre.command.name";
	
	/** The property name to filter for providers offering a command. */
	public static final String PROPERTY_DESCRIPTION = "org.coolsoftware.theatre.command.description";
	
	/** The delimiter to seperate executed host and result description. */
	public static final String HOST_DELIMITER = ":";

	/**
	 * Executes the given command and return a description of the result.
	 * @param command the command to execute
	 * @param params optional parameters
	 * @return a result description in the form <code>HOST {@link #HOST_DELIMITER} RESULT</code>
	 */
	public abstract String execute(String command, String... params);
	
	public abstract String getCommandName();
	
	public abstract String getDescription();

}
