/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.simpleApp.rest;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.haec.theatre.rest.GemCallOptimizeResource;

/**
 * REST service to access the simpleApp.
 * @author René Schöne
 */
@Path("simpleApp/simpleComp")
public class SimpleAppService extends GemCallOptimizeResource {

	private static final List<String> max = Arrays.asList("max");
	private static final String APP_NAME = "org.haec.app.simpleApp";
	private static final String COMPONENT = "SimpleComponent";
	private static final String PORT = "doWork";
	private static final List<String> particle_count = Arrays.asList("particle_count");
	private Logger log = Logger.getLogger(SimpleAppService.class);

	@Path("doWork")
	@GET
	@Produces( {MediaType.TEXT_PLAIN} )
	public Response doWork(@QueryParam("input") int input,
			@QueryParam("objectiveName") String objectiveName,
			@QueryParam("objectiveMaxValue") int objectiveMaxValue ) {
		String request = makeRequest("ilp", APP_NAME, COMPONENT, PORT,
				makeObjectivName(objectiveName), max, Arrays.asList(Integer.toString(objectiveMaxValue)),
				particle_count, makeParticleCount(input));
		Object[] userParams = new Object[]{input};
		return fire(request, false, true, userParams);
	}
	
	@Override
	protected Response processResult(String request, Object[] userParams,
			Serializable result) {
		if(result == null || !(result instanceof Integer)) {
			Response r;
			if(result instanceof Throwable) {
				r = Response.status(Status.INTERNAL_SERVER_ERROR).entity("Got exception: " + ((Throwable) result).getMessage()).build();
			}
			else {
				r = Response.status(Status.INTERNAL_SERVER_ERROR).entity("Got not Integer as execution result.").build();
			}
			log.debug(loggingContent(r));
			return r;
		}
		// expect some double number as output
		return Response.ok(Double.toString((Integer) result)).build();
	}

	/**
	 * @param objectiveName
	 * @return
	 */
	private List<String> makeObjectivName(String objectiveName) {
		return Collections.singletonList(objectiveName);
	}

	/**
	 * @param input the input value
	 * @return the list of metaparameters
	 */
	private List<String> makeParticleCount(double input) {
		return Arrays.asList(Integer.toString((int) input));
	}

}
