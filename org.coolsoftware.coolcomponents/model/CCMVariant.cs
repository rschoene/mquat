SYNTAXDEF varianttext
FOR <http://cool-software.org/ccm/1.0> <CCM.genmodel>
START ccm.variant.PhysicalResource

IMPORTS {
	ccm.structure:<http://cool-software.org/ccm/1.0/typesystem> <CCM.genmodel>
	ccm.variant:<http://cool-software.org/ccm/1.0/variant> <CCM.genmodel>
	ccm.behavior:<http://cool-software.org/ccm/1.0/behavior> <CCM.genmodel>
	nameable:<http://www.cool-software.org/dimm/nameable> <CCM.genmodel>
	datatype:<http://www.cool-software.org/dimm/types> <CCM.genmodel>
	expression:<http://www.cool-software.org/dimm/expressions> <CCM.genmodel>
}


TOKENS {

	DEFINE COMMENT $'//'(~('\n'|'\r'|'\uffff'))*$;
		DEFINE INTEGER $('-')?('1'..'9')('0'..'9')*|'0'$;
			DEFINE FLOAT $('-')?(('1'..'9') ('0'..'9')* | '0') '.' ('0'..'9')+ $;
				DEFINE MYTEXT TEXT + $'.'$ + TEXT;
			
			}
			
			
			TOKENSTYLES {
				"name" COLOR #7F0055, BOLD;
				"Binding for" COLOR #7F0055, BOLD;
				"is type of" COLOR #7F0055, BOLD;
				"porttypes" COLOR #7F0055, BOLD;
				"source" COLOR #7F0055, BOLD;
				"target" COLOR #7F0055, BOLD;
				"PhysicalResourceType" COLOR #7F0055, BOLD;
				"SWComponentType" COLOR #7F0055, BOLD;
				"UserType" COLOR #7F0055, BOLD;
				"ResourceType" COLOR #7F0055, BOLD;
				"ContainerProviderType" COLOR #7F0055, BOLD;
				"Port" COLOR #7F0055, BOLD;
				"specification" COLOR #7F0055, BOLD;
				"PortConnector" COLOR #7F0055, BOLD;
				"PhysicalResource" COLOR #7F0055, BOLD;
				"ports" COLOR #7F0055, BOLD;
				"SWComponent" COLOR #7F0055, BOLD;
				"User" COLOR #7F0055, BOLD;
				"ContainerProvider" COLOR #7F0055, BOLD;
				"uses" COLOR #7F0055, BOLD;
				"Behavior" COLOR #7F0055, BOLD;
				"parameterBinding" COLOR #7F0055, BOLD;
				"template" COLOR #7F0055, BOLD;
				"links" COLOR #7F0055, BOLD;
				"component" COLOR #7F0055, BOLD;
				"Resource" COLOR #7F0055, BOLD;
				"Link" COLOR #7F0055, BOLD;
				"port" COLOR #7F0055, BOLD;
				"pin" COLOR #7F0055, BOLD;
				
			}
			
			
			RULES {
				ccm.variant.Port ::= "Port" #1  name[] #1  "is type of" #1 specification[] ;
				ccm.variant.PhysicalResource ::= "PhysicalResource" #1  name['"','"'] #1 "is type of" #1 specification [] #1 "{" !1 #2 ((ports)* !1  behavior ) !1"}" !2 ;
				ccm.variant.Behavior ::= "Behavior" #1 name[] #1 "usesTemplate" #1 template[] #1 "withCostParameterBindings" #1 "{"!1 #2 (parameterBinding!1)* !1 "}" ;
				ccm.variant.ParameterValue ::= "Binding" #1 parameter [MYTEXT] #1 "=" #1 value[FLOAT] !1; 
				ccm.behavior.StateMachine ::= "StateMachine"  name['"','"'];
				ccm.behavior.Transition ::= "Transition" name['"','"'] ;
				ccm.behavior.State ::= "State"  name['"','"'] ;
			}