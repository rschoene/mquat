/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm;

import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.ccm.CcmFactory
 * @model kind="package"
 * @generated
 */
public interface CcmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ccm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://cool-software.org/ccm/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ccm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CcmPackage eINSTANCE = org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl <em>Cool Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl
	 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getCoolEnvironment()
	 * @generated
	 */
	int COOL_ENVIRONMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__NAME = NameablePackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__DESCRIPTION = NameablePackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Software Type System</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource Type System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>User Type System</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__USER_TYPE_SYSTEM = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Components</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__COMPONENTS = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Resourcemodel</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__RESOURCEMODEL = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Users</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT__USERS = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Cool Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COOL_ENVIRONMENT_FEATURE_COUNT = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.Visibility <em>Visibility</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.Visibility
	 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getVisibility()
	 * @generated
	 */
	int VISIBILITY = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.Direction <em>Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.Direction
	 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getDirection()
	 * @generated
	 */
	int DIRECTION = 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.ParameterDirection <em>Parameter Direction</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.ParameterDirection
	 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getParameterDirection()
	 * @generated
	 */
	int PARAMETER_DIRECTION = 3;


	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment <em>Cool Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cool Environment</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.CoolEnvironment
	 * @generated
	 */
	EClass getCoolEnvironment();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getSoftwareTypeSystem <em>Software Type System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Software Type System</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getSoftwareTypeSystem()
	 * @see #getCoolEnvironment()
	 * @generated
	 */
	EReference getCoolEnvironment_SoftwareTypeSystem();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getResourceTypeSystem <em>Resource Type System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource Type System</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getResourceTypeSystem()
	 * @see #getCoolEnvironment()
	 * @generated
	 */
	EReference getCoolEnvironment_ResourceTypeSystem();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getUserTypeSystem <em>User Type System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>User Type System</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getUserTypeSystem()
	 * @see #getCoolEnvironment()
	 * @generated
	 */
	EReference getCoolEnvironment_UserTypeSystem();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Components</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getComponents()
	 * @see #getCoolEnvironment()
	 * @generated
	 */
	EReference getCoolEnvironment_Components();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getResourcemodel <em>Resourcemodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Resourcemodel</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getResourcemodel()
	 * @see #getCoolEnvironment()
	 * @generated
	 */
	EReference getCoolEnvironment_Resourcemodel();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getUsers <em>Users</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Users</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getUsers()
	 * @see #getCoolEnvironment()
	 * @generated
	 */
	EReference getCoolEnvironment_Users();

	/**
	 * Returns the meta object for enum '{@link org.coolsoftware.coolcomponents.ccm.Visibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Visibility</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.Visibility
	 * @generated
	 */
	EEnum getVisibility();

	/**
	 * Returns the meta object for enum '{@link org.coolsoftware.coolcomponents.ccm.Direction <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Direction</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.Direction
	 * @generated
	 */
	EEnum getDirection();

	/**
	 * Returns the meta object for enum '{@link org.coolsoftware.coolcomponents.ccm.ParameterDirection <em>Parameter Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Parameter Direction</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.ParameterDirection
	 * @generated
	 */
	EEnum getParameterDirection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CcmFactory getCcmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl <em>Cool Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl
		 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getCoolEnvironment()
		 * @generated
		 */
		EClass COOL_ENVIRONMENT = eINSTANCE.getCoolEnvironment();

		/**
		 * The meta object literal for the '<em><b>Software Type System</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM = eINSTANCE.getCoolEnvironment_SoftwareTypeSystem();

		/**
		 * The meta object literal for the '<em><b>Resource Type System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM = eINSTANCE.getCoolEnvironment_ResourceTypeSystem();

		/**
		 * The meta object literal for the '<em><b>User Type System</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COOL_ENVIRONMENT__USER_TYPE_SYSTEM = eINSTANCE.getCoolEnvironment_UserTypeSystem();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COOL_ENVIRONMENT__COMPONENTS = eINSTANCE.getCoolEnvironment_Components();

		/**
		 * The meta object literal for the '<em><b>Resourcemodel</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COOL_ENVIRONMENT__RESOURCEMODEL = eINSTANCE.getCoolEnvironment_Resourcemodel();

		/**
		 * The meta object literal for the '<em><b>Users</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COOL_ENVIRONMENT__USERS = eINSTANCE.getCoolEnvironment_Users();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.Visibility <em>Visibility</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.Visibility
		 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getVisibility()
		 * @generated
		 */
		EEnum VISIBILITY = eINSTANCE.getVisibility();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.Direction <em>Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.Direction
		 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getDirection()
		 * @generated
		 */
		EEnum DIRECTION = eINSTANCE.getDirection();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.ParameterDirection <em>Parameter Direction</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.ParameterDirection
		 * @see org.coolsoftware.coolcomponents.ccm.impl.CcmPackageImpl#getParameterDirection()
		 * @generated
		 */
		EEnum PARAMETER_DIRECTION = eINSTANCE.getParameterDirection();

	}

} //CcmPackage
