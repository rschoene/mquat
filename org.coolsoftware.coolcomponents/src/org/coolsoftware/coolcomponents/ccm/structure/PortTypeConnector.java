/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Type Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeOffer <em>Port Type Offer</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeRequirement <em>Port Type Requirement</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortTypeConnector()
 * @model
 * @generated
 */
public interface PortTypeConnector extends EObject {
	/**
	 * Returns the value of the '<em><b>Port Type Offer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Type Offer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Type Offer</em>' reference.
	 * @see #setPortTypeOffer(PortTypeOffer)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortTypeConnector_PortTypeOffer()
	 * @model required="true"
	 * @generated
	 */
	PortTypeOffer getPortTypeOffer();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeOffer <em>Port Type Offer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Type Offer</em>' reference.
	 * @see #getPortTypeOffer()
	 * @generated
	 */
	void setPortTypeOffer(PortTypeOffer value);

	/**
	 * Returns the value of the '<em><b>Port Type Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Type Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Type Requirement</em>' reference.
	 * @see #setPortTypeRequirement(PortTypeRequirement)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortTypeConnector_PortTypeRequirement()
	 * @model required="true"
	 * @generated
	 */
	PortTypeRequirement getPortTypeRequirement();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeRequirement <em>Port Type Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Type Requirement</em>' reference.
	 * @see #getPortTypeRequirement()
	 * @generated
	 */
	void setPortTypeRequirement(PortTypeRequirement value);

} // PortTypeConnector
