/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.coolsoftware.coolcomponents.nameable.NamedElement;

import org.coolsoftware.coolcomponents.types.CcmType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.Operation#getReturnType <em>Return Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getOperation()
 * @model
 * @generated
 */
public interface Operation extends NamedElement, ParameterizableElement {
	/**
	 * Returns the value of the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Type</em>' reference.
	 * @see #setReturnType(CcmType)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getOperation_ReturnType()
	 * @model
	 * @generated
	 */
	CcmType getReturnType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.Operation#getReturnType <em>Return Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Type</em>' reference.
	 * @see #getReturnType()
	 * @generated
	 */
	void setReturnType(CcmType value);

} // Operation
