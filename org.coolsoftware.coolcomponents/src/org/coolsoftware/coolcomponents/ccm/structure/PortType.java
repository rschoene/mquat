/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.coolsoftware.coolcomponents.ccm.Direction;
import org.coolsoftware.coolcomponents.ccm.Visibility;
import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getDirection <em>Direction</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getIn <em>In</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getOut <em>Out</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getSharedMemory <em>Shared Memory</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getRequiredPortTypes <em>Required Port Types</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getOperations <em>Operations</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getMetaparameter <em>Metaparameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType()
 * @model annotation="gmf.node label='name'"
 * @generated
 */
public interface PortType extends NamedElement, ParameterizableElement {
	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The literals are from the enumeration {@link org.coolsoftware.coolcomponents.ccm.Direction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.Direction
	 * @see #setDirection(Direction)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_Direction()
	 * @model
	 * @generated
	 */
	Direction getDirection();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.Direction
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(Direction value);

	/**
	 * Returns the value of the '<em><b>Visibility</b></em>' attribute.
	 * The default value is <code>"public"</code>.
	 * The literals are from the enumeration {@link org.coolsoftware.coolcomponents.ccm.Visibility}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visibility</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.Visibility
	 * @see #setVisibility(Visibility)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_Visibility()
	 * @model default="public"
	 * @generated
	 */
	Visibility getVisibility();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getVisibility <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visibility</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.Visibility
	 * @see #getVisibility()
	 * @generated
	 */
	void setVisibility(Visibility value);

	/**
	 * Returns the value of the '<em><b>In</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType}.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_In()
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<PortConnectorType> getIn();

	/**
	 * Returns the value of the '<em><b>Out</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType}.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_Out()
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<PortConnectorType> getOut();

	/**
	 * Returns the value of the '<em><b>Shared Memory</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Memory</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Memory</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_SharedMemory()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getSharedMemory();

	/**
	 * Returns the value of the '<em><b>Required Port Types</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.PortType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Port Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Port Types</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_RequiredPortTypes()
	 * @model
	 * @generated
	 */
	EList<PortType> getRequiredPortTypes();

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_Operations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Operation> getOperations();

	/**
	 * Returns the value of the '<em><b>Metaparameter</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.Parameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metaparameter</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metaparameter</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortType_Metaparameter()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getMetaparameter();

} // PortType
