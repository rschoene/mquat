/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructureFactory
 * @model kind="package"
 *        annotation="gmf model='StructureModel'"
 * @generated
 */
public interface StructurePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "structure";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://cool-software.org/ccm/1.0/typesystem";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "type";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StructurePackage eINSTANCE = org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl <em>Structural Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getStructuralModel()
	 * @generated
	 */
	int STRUCTURAL_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_MODEL__ROOT = 0;

	/**
	 * The feature id for the '<em><b>Type Libraries</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_MODEL__TYPE_LIBRARIES = 1;

	/**
	 * The feature id for the '<em><b>Referenced Type Libraries</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_MODEL__REFERENCED_TYPE_LIBRARIES = 2;

	/**
	 * The feature id for the '<em><b>Component Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_MODEL__COMPONENT_TYPES = 3;

	/**
	 * The feature id for the '<em><b>Port Type Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_MODEL__PORT_TYPE_CONTAINER = 4;

	/**
	 * The feature id for the '<em><b>Data Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_MODEL__DATA_TYPES = 5;

	/**
	 * The number of structural features of the '<em>Structural Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRUCTURAL_MODEL_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl <em>Component Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getComponentType()
	 * @generated
	 */
	int COMPONENT_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__NAME = NameablePackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__DESCRIPTION = NameablePackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__ID = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__PORTTYPES = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__CONNECTORS = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__PROPERTIES = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__ECL_URI = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__PORT_TYPE_OFFERS = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE_FEATURE_COUNT = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl <em>Port Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortType()
	 * @generated
	 */
	int PORT_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__PARAMETER = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__DIRECTION = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__VISIBILITY = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__IN = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__OUT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Shared Memory</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__SHARED_MEMORY = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Required Port Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__REQUIRED_PORT_TYPES = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__OPERATIONS = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Metaparameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE__METAPARAMETER = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Port Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortConnectorTypeImpl <em>Port Connector Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortConnectorTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortConnectorType()
	 * @generated
	 */
	int PORT_CONNECTOR_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR_TYPE__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR_TYPE__SOURCE = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR_TYPE__TARGET = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Port Connector Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR_TYPE_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ResourceTypeImpl <em>Resource Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ResourceTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getResourceType()
	 * @generated
	 */
	int RESOURCE_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__NAME = COMPONENT_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__DESCRIPTION = COMPONENT_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__ID = COMPONENT_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__PORTTYPES = COMPONENT_TYPE__PORTTYPES;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__CONNECTORS = COMPONENT_TYPE__CONNECTORS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__PROPERTIES = COMPONENT_TYPE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__ECL_URI = COMPONENT_TYPE__ECL_URI;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__PORT_TYPE_OFFERS = COMPONENT_TYPE__PORT_TYPE_OFFERS;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__PORT_TYPE_REQUIREMENTS = COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Subtypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__SUBTYPES = COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__LOWER_BOUND = COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__UPPER_BOUND = COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>CSuper Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE__CSUPER_TYPE = COMPONENT_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Resource Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_TYPE_FEATURE_COUNT = COMPONENT_TYPE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PhysicalResourceTypeImpl <em>Physical Resource Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PhysicalResourceTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPhysicalResourceType()
	 * @generated
	 */
	int PHYSICAL_RESOURCE_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__NAME = RESOURCE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__DESCRIPTION = RESOURCE_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__ID = RESOURCE_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__PORTTYPES = RESOURCE_TYPE__PORTTYPES;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__CONNECTORS = RESOURCE_TYPE__CONNECTORS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__PROPERTIES = RESOURCE_TYPE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__ECL_URI = RESOURCE_TYPE__ECL_URI;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__PORT_TYPE_OFFERS = RESOURCE_TYPE__PORT_TYPE_OFFERS;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__PORT_TYPE_REQUIREMENTS = RESOURCE_TYPE__PORT_TYPE_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Subtypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__SUBTYPES = RESOURCE_TYPE__SUBTYPES;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__LOWER_BOUND = RESOURCE_TYPE__LOWER_BOUND;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__UPPER_BOUND = RESOURCE_TYPE__UPPER_BOUND;

	/**
	 * The feature id for the '<em><b>CSuper Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__CSUPER_TYPE = RESOURCE_TYPE__CSUPER_TYPE;

	/**
	 * The feature id for the '<em><b>Is Shared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE__IS_SHARED = RESOURCE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Resource Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_TYPE_FEATURE_COUNT = RESOURCE_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.SWComponentTypeImpl <em>SW Component Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.SWComponentTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getSWComponentType()
	 * @generated
	 */
	int SW_COMPONENT_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__NAME = COMPONENT_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__DESCRIPTION = COMPONENT_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__ID = COMPONENT_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__PORTTYPES = COMPONENT_TYPE__PORTTYPES;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__CONNECTORS = COMPONENT_TYPE__CONNECTORS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__PROPERTIES = COMPONENT_TYPE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__ECL_URI = COMPONENT_TYPE__ECL_URI;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__PORT_TYPE_OFFERS = COMPONENT_TYPE__PORT_TYPE_OFFERS;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS = COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Subtypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE__SUBTYPES = COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SW Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_TYPE_FEATURE_COUNT = COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.UserTypeImpl <em>User Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.UserTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getUserType()
	 * @generated
	 */
	int USER_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__NAME = COMPONENT_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__DESCRIPTION = COMPONENT_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__ID = COMPONENT_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__PORTTYPES = COMPONENT_TYPE__PORTTYPES;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__CONNECTORS = COMPONENT_TYPE__CONNECTORS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__PROPERTIES = COMPONENT_TYPE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__ECL_URI = COMPONENT_TYPE__ECL_URI;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__PORT_TYPE_OFFERS = COMPONENT_TYPE__PORT_TYPE_OFFERS;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE__PORT_TYPE_REQUIREMENTS = COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS;

	/**
	 * The number of structural features of the '<em>User Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_TYPE_FEATURE_COUNT = COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ContainerProviderTypeImpl <em>Container Provider Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ContainerProviderTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getContainerProviderType()
	 * @generated
	 */
	int CONTAINER_PROVIDER_TYPE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__NAME = RESOURCE_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__DESCRIPTION = RESOURCE_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__ID = RESOURCE_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__PORTTYPES = RESOURCE_TYPE__PORTTYPES;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__CONNECTORS = RESOURCE_TYPE__CONNECTORS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__PROPERTIES = RESOURCE_TYPE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__ECL_URI = RESOURCE_TYPE__ECL_URI;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__PORT_TYPE_OFFERS = RESOURCE_TYPE__PORT_TYPE_OFFERS;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__PORT_TYPE_REQUIREMENTS = RESOURCE_TYPE__PORT_TYPE_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Subtypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__SUBTYPES = RESOURCE_TYPE__SUBTYPES;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__LOWER_BOUND = RESOURCE_TYPE__LOWER_BOUND;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__UPPER_BOUND = RESOURCE_TYPE__UPPER_BOUND;

	/**
	 * The feature id for the '<em><b>CSuper Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE__CSUPER_TYPE = RESOURCE_TYPE__CSUPER_TYPE;

	/**
	 * The number of structural features of the '<em>Container Provider Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_TYPE_FEATURE_COUNT = RESOURCE_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PropertyImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 9;

	/**
	 * The feature id for the '<em><b>Declared Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DECLARED_VARIABLE = VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__UNIT = VariablesPackage.VARIABLE_DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__KIND = VariablesPackage.VARIABLE_DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Val Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VAL_ORDER = VariablesPackage.VARIABLE_DECLARATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = VariablesPackage.VARIABLE_DECLARATION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeContainerImpl <em>Port Type Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeContainerImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeContainer()
	 * @generated
	 */
	int PORT_TYPE_CONTAINER = 10;

	/**
	 * The feature id for the '<em><b>Port Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_CONTAINER__PORT_TYPES = 0;

	/**
	 * The number of structural features of the '<em>Port Type Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = VariablesPackage.VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__TYPE = VariablesPackage.VARIABLE__TYPE;

	/**
	 * The feature id for the '<em><b>Initial Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__INITIAL_EXPRESSION = VariablesPackage.VARIABLE__INITIAL_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__VALUE = VariablesPackage.VARIABLE__VALUE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__DIRECTION = VariablesPackage.VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = VariablesPackage.VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.OperationImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__PARAMETER = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__RETURN_TYPE = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterizableElementImpl <em>Parameterizable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterizableElementImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getParameterizableElement()
	 * @generated
	 */
	int PARAMETERIZABLE_ELEMENT = 13;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERIZABLE_ELEMENT__PARAMETER = 0;

	/**
	 * The number of structural features of the '<em>Parameterizable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETERIZABLE_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeOfferImpl <em>Port Type Offer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeOfferImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeOffer()
	 * @generated
	 */
	int PORT_TYPE_OFFER = 14;

	/**
	 * The feature id for the '<em><b>Port Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_OFFER__PORT_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Port Type Offer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_OFFER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeRequirementImpl <em>Port Type Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeRequirementImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeRequirement()
	 * @generated
	 */
	int PORT_TYPE_REQUIREMENT = 15;

	/**
	 * The feature id for the '<em><b>Port Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_REQUIREMENT__PORT_TYPE = 0;

	/**
	 * The number of structural features of the '<em>Port Type Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_REQUIREMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeConnectorImpl <em>Port Type Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeConnectorImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeConnector()
	 * @generated
	 */
	int PORT_TYPE_CONNECTOR = 16;

	/**
	 * The feature id for the '<em><b>Port Type Offer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER = 0;

	/**
	 * The feature id for the '<em><b>Port Type Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT = 1;

	/**
	 * The number of structural features of the '<em>Port Type Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_TYPE_CONNECTOR_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.DataTypeImpl <em>Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.DataTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE__PROPERTIES = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_TYPE_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.SWConnectorTypeImpl <em>SW Connector Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.SWConnectorTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getSWConnectorType()
	 * @generated
	 */
	int SW_CONNECTOR_TYPE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__NAME = SW_COMPONENT_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__DESCRIPTION = SW_COMPONENT_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__ID = SW_COMPONENT_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__PORTTYPES = SW_COMPONENT_TYPE__PORTTYPES;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__CONNECTORS = SW_COMPONENT_TYPE__CONNECTORS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__PROPERTIES = SW_COMPONENT_TYPE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__ECL_URI = SW_COMPONENT_TYPE__ECL_URI;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__PORT_TYPE_OFFERS = SW_COMPONENT_TYPE__PORT_TYPE_OFFERS;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__PORT_TYPE_REQUIREMENTS = SW_COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Subtypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE__SUBTYPES = SW_COMPONENT_TYPE__SUBTYPES;

	/**
	 * The number of structural features of the '<em>SW Connector Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_CONNECTOR_TYPE_FEATURE_COUNT = SW_COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.CopyFileTypeImpl <em>Copy File Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.CopyFileTypeImpl
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getCopyFileType()
	 * @generated
	 */
	int COPY_FILE_TYPE = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__NAME = SW_COMPONENT_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__DESCRIPTION = SW_COMPONENT_TYPE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__ID = SW_COMPONENT_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Porttypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__PORTTYPES = SW_COMPONENT_TYPE__PORTTYPES;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__CONNECTORS = SW_COMPONENT_TYPE__CONNECTORS;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__PROPERTIES = SW_COMPONENT_TYPE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__ECL_URI = SW_COMPONENT_TYPE__ECL_URI;

	/**
	 * The feature id for the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__PORT_TYPE_OFFERS = SW_COMPONENT_TYPE__PORT_TYPE_OFFERS;

	/**
	 * The feature id for the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__PORT_TYPE_REQUIREMENTS = SW_COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS;

	/**
	 * The feature id for the '<em><b>Subtypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__SUBTYPES = SW_COMPONENT_TYPE__SUBTYPES;

	/**
	 * The feature id for the '<em><b>File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__FILE_NAME = SW_COMPONENT_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE__PARAMETER = SW_COMPONENT_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Copy File Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_TYPE_FEATURE_COUNT = SW_COMPONENT_TYPE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.PropertyKind <em>Property Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PropertyKind
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPropertyKind()
	 * @generated
	 */
	int PROPERTY_KIND = 20;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.structure.Order <em>Order</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Order
	 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getOrder()
	 * @generated
	 */
	int ORDER = 21;

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel <em>Structural Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Structural Model</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructuralModel
	 * @generated
	 */
	EClass getStructuralModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getRoot()
	 * @see #getStructuralModel()
	 * @generated
	 */
	EReference getStructuralModel_Root();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getTypeLibraries <em>Type Libraries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Type Libraries</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getTypeLibraries()
	 * @see #getStructuralModel()
	 * @generated
	 */
	EReference getStructuralModel_TypeLibraries();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getReferencedTypeLibraries <em>Referenced Type Libraries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Referenced Type Libraries</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getReferencedTypeLibraries()
	 * @see #getStructuralModel()
	 * @generated
	 */
	EReference getStructuralModel_ReferencedTypeLibraries();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getComponentTypes <em>Component Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component Types</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getComponentTypes()
	 * @see #getStructuralModel()
	 * @generated
	 */
	EReference getStructuralModel_ComponentTypes();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getPortTypeContainer <em>Port Type Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port Type Container</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getPortTypeContainer()
	 * @see #getStructuralModel()
	 * @generated
	 */
	EReference getStructuralModel_PortTypeContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getDataTypes <em>Data Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Types</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getDataTypes()
	 * @see #getStructuralModel()
	 * @generated
	 */
	EReference getStructuralModel_DataTypes();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType <em>Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ComponentType
	 * @generated
	 */
	EClass getComponentType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPorttypes <em>Porttypes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Porttypes</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPorttypes()
	 * @see #getComponentType()
	 * @generated
	 */
	EReference getComponentType_Porttypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getConnectors <em>Connectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connectors</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getConnectors()
	 * @see #getComponentType()
	 * @generated
	 */
	EReference getComponentType_Connectors();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getProperties()
	 * @see #getComponentType()
	 * @generated
	 */
	EReference getComponentType_Properties();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getEclUri <em>Ecl Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ecl Uri</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getEclUri()
	 * @see #getComponentType()
	 * @generated
	 */
	EAttribute getComponentType_EclUri();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPortTypeOffers <em>Port Type Offers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Type Offers</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPortTypeOffers()
	 * @see #getComponentType()
	 * @generated
	 */
	EReference getComponentType_PortTypeOffers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPortTypeRequirements <em>Port Type Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Type Requirements</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPortTypeRequirements()
	 * @see #getComponentType()
	 * @generated
	 */
	EReference getComponentType_PortTypeRequirements();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType <em>Port Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType
	 * @generated
	 */
	EClass getPortType();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getDirection()
	 * @see #getPortType()
	 * @generated
	 */
	EAttribute getPortType_Direction();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getVisibility()
	 * @see #getPortType()
	 * @generated
	 */
	EAttribute getPortType_Visibility();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>In</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getIn()
	 * @see #getPortType()
	 * @generated
	 */
	EReference getPortType_In();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getOut <em>Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Out</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getOut()
	 * @see #getPortType()
	 * @generated
	 */
	EReference getPortType_Out();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getSharedMemory <em>Shared Memory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Shared Memory</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getSharedMemory()
	 * @see #getPortType()
	 * @generated
	 */
	EReference getPortType_SharedMemory();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getRequiredPortTypes <em>Required Port Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Port Types</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getRequiredPortTypes()
	 * @see #getPortType()
	 * @generated
	 */
	EReference getPortType_RequiredPortTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getOperations()
	 * @see #getPortType()
	 * @generated
	 */
	EReference getPortType_Operations();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getMetaparameter <em>Metaparameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Metaparameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getMetaparameter()
	 * @see #getPortType()
	 * @generated
	 */
	EReference getPortType_Metaparameter();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType <em>Port Connector Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connector Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType
	 * @generated
	 */
	EClass getPortConnectorType();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getSource()
	 * @see #getPortConnectorType()
	 * @generated
	 */
	EReference getPortConnectorType_Source();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getTarget()
	 * @see #getPortConnectorType()
	 * @generated
	 */
	EReference getPortConnectorType_Target();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.PhysicalResourceType <em>Physical Resource Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Resource Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PhysicalResourceType
	 * @generated
	 */
	EClass getPhysicalResourceType();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.PhysicalResourceType#isIsShared <em>Is Shared</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Shared</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PhysicalResourceType#isIsShared()
	 * @see #getPhysicalResourceType()
	 * @generated
	 */
	EAttribute getPhysicalResourceType_IsShared();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.SWComponentType <em>SW Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Component Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.SWComponentType
	 * @generated
	 */
	EClass getSWComponentType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.SWComponentType#getSubtypes <em>Subtypes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtypes</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.SWComponentType#getSubtypes()
	 * @see #getSWComponentType()
	 * @generated
	 */
	EReference getSWComponentType_Subtypes();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.UserType <em>User Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.UserType
	 * @generated
	 */
	EClass getUserType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.ResourceType <em>Resource Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ResourceType
	 * @generated
	 */
	EClass getResourceType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getSubtypes <em>Subtypes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtypes</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getSubtypes()
	 * @see #getResourceType()
	 * @generated
	 */
	EReference getResourceType_Subtypes();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getLowerBound <em>Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Bound</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getLowerBound()
	 * @see #getResourceType()
	 * @generated
	 */
	EAttribute getResourceType_LowerBound();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getUpperBound <em>Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Bound</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getUpperBound()
	 * @see #getResourceType()
	 * @generated
	 */
	EAttribute getResourceType_UpperBound();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getCSuperType <em>CSuper Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>CSuper Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ResourceType#getCSuperType()
	 * @see #getResourceType()
	 * @generated
	 */
	EReference getResourceType_CSuperType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.ContainerProviderType <em>Container Provider Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container Provider Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ContainerProviderType
	 * @generated
	 */
	EClass getContainerProviderType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Property#getUnit()
	 * @see #getProperty()
	 * @generated
	 */
	EReference getProperty_Unit();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Property#getKind()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Kind();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getValOrder <em>Val Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Val Order</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Property#getValOrder()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_ValOrder();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeContainer <em>Port Type Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Type Container</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeContainer
	 * @generated
	 */
	EClass getPortTypeContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeContainer#getPortTypes <em>Port Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Types</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeContainer#getPortTypes()
	 * @see #getPortTypeContainer()
	 * @generated
	 */
	EReference getPortTypeContainer_PortTypes();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.Parameter#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Parameter#getDirection()
	 * @see #getParameter()
	 * @generated
	 */
	EAttribute getParameter_Direction();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.Operation#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Return Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Operation#getReturnType()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_ReturnType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.ParameterizableElement <em>Parameterizable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameterizable Element</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ParameterizableElement
	 * @generated
	 */
	EClass getParameterizableElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.ParameterizableElement#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.ParameterizableElement#getParameter()
	 * @see #getParameterizableElement()
	 * @generated
	 */
	EReference getParameterizableElement_Parameter();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeOffer <em>Port Type Offer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Type Offer</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeOffer
	 * @generated
	 */
	EClass getPortTypeOffer();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeOffer#getPortType <em>Port Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeOffer#getPortType()
	 * @see #getPortTypeOffer()
	 * @generated
	 */
	EReference getPortTypeOffer_PortType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeRequirement <em>Port Type Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Type Requirement</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeRequirement
	 * @generated
	 */
	EClass getPortTypeRequirement();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeRequirement#getPortType <em>Port Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeRequirement#getPortType()
	 * @see #getPortTypeRequirement()
	 * @generated
	 */
	EReference getPortTypeRequirement_PortType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector <em>Port Type Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Type Connector</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector
	 * @generated
	 */
	EClass getPortTypeConnector();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeOffer <em>Port Type Offer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Type Offer</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeOffer()
	 * @see #getPortTypeConnector()
	 * @generated
	 */
	EReference getPortTypeConnector_PortTypeOffer();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeRequirement <em>Port Type Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port Type Requirement</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector#getPortTypeRequirement()
	 * @see #getPortTypeConnector()
	 * @generated
	 */
	EReference getPortTypeConnector_PortTypeRequirement();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.DataType
	 * @generated
	 */
	EClass getDataType();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.structure.DataType#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.DataType#getProperties()
	 * @see #getDataType()
	 * @generated
	 */
	EReference getDataType_Properties();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.SWConnectorType <em>SW Connector Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Connector Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.SWConnectorType
	 * @generated
	 */
	EClass getSWConnectorType();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.structure.CopyFileType <em>Copy File Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy File Type</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.CopyFileType
	 * @generated
	 */
	EClass getCopyFileType();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.structure.CopyFileType#getFileName <em>File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>File Name</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.CopyFileType#getFileName()
	 * @see #getCopyFileType()
	 * @generated
	 */
	EAttribute getCopyFileType_FileName();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.structure.CopyFileType#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.CopyFileType#getParameter()
	 * @see #getCopyFileType()
	 * @generated
	 */
	EReference getCopyFileType_Parameter();

	/**
	 * Returns the meta object for enum '{@link org.coolsoftware.coolcomponents.ccm.structure.PropertyKind <em>Property Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Property Kind</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PropertyKind
	 * @generated
	 */
	EEnum getPropertyKind();

	/**
	 * Returns the meta object for enum '{@link org.coolsoftware.coolcomponents.ccm.structure.Order <em>Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Order</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Order
	 * @generated
	 */
	EEnum getOrder();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StructureFactory getStructureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl <em>Structural Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getStructuralModel()
		 * @generated
		 */
		EClass STRUCTURAL_MODEL = eINSTANCE.getStructuralModel();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURAL_MODEL__ROOT = eINSTANCE.getStructuralModel_Root();

		/**
		 * The meta object literal for the '<em><b>Type Libraries</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURAL_MODEL__TYPE_LIBRARIES = eINSTANCE.getStructuralModel_TypeLibraries();

		/**
		 * The meta object literal for the '<em><b>Referenced Type Libraries</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURAL_MODEL__REFERENCED_TYPE_LIBRARIES = eINSTANCE.getStructuralModel_ReferencedTypeLibraries();

		/**
		 * The meta object literal for the '<em><b>Component Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURAL_MODEL__COMPONENT_TYPES = eINSTANCE.getStructuralModel_ComponentTypes();

		/**
		 * The meta object literal for the '<em><b>Port Type Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURAL_MODEL__PORT_TYPE_CONTAINER = eINSTANCE.getStructuralModel_PortTypeContainer();

		/**
		 * The meta object literal for the '<em><b>Data Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRUCTURAL_MODEL__DATA_TYPES = eINSTANCE.getStructuralModel_DataTypes();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl <em>Component Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getComponentType()
		 * @generated
		 */
		EClass COMPONENT_TYPE = eINSTANCE.getComponentType();

		/**
		 * The meta object literal for the '<em><b>Porttypes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TYPE__PORTTYPES = eINSTANCE.getComponentType_Porttypes();

		/**
		 * The meta object literal for the '<em><b>Connectors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TYPE__CONNECTORS = eINSTANCE.getComponentType_Connectors();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TYPE__PROPERTIES = eINSTANCE.getComponentType_Properties();

		/**
		 * The meta object literal for the '<em><b>Ecl Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_TYPE__ECL_URI = eINSTANCE.getComponentType_EclUri();

		/**
		 * The meta object literal for the '<em><b>Port Type Offers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TYPE__PORT_TYPE_OFFERS = eINSTANCE.getComponentType_PortTypeOffers();

		/**
		 * The meta object literal for the '<em><b>Port Type Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS = eINSTANCE.getComponentType_PortTypeRequirements();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl <em>Port Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortType()
		 * @generated
		 */
		EClass PORT_TYPE = eINSTANCE.getPortType();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_TYPE__DIRECTION = eINSTANCE.getPortType_Direction();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_TYPE__VISIBILITY = eINSTANCE.getPortType_Visibility();

		/**
		 * The meta object literal for the '<em><b>In</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE__IN = eINSTANCE.getPortType_In();

		/**
		 * The meta object literal for the '<em><b>Out</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE__OUT = eINSTANCE.getPortType_Out();

		/**
		 * The meta object literal for the '<em><b>Shared Memory</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE__SHARED_MEMORY = eINSTANCE.getPortType_SharedMemory();

		/**
		 * The meta object literal for the '<em><b>Required Port Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE__REQUIRED_PORT_TYPES = eINSTANCE.getPortType_RequiredPortTypes();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE__OPERATIONS = eINSTANCE.getPortType_Operations();

		/**
		 * The meta object literal for the '<em><b>Metaparameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE__METAPARAMETER = eINSTANCE.getPortType_Metaparameter();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortConnectorTypeImpl <em>Port Connector Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortConnectorTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortConnectorType()
		 * @generated
		 */
		EClass PORT_CONNECTOR_TYPE = eINSTANCE.getPortConnectorType();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_CONNECTOR_TYPE__SOURCE = eINSTANCE.getPortConnectorType_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_CONNECTOR_TYPE__TARGET = eINSTANCE.getPortConnectorType_Target();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PhysicalResourceTypeImpl <em>Physical Resource Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PhysicalResourceTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPhysicalResourceType()
		 * @generated
		 */
		EClass PHYSICAL_RESOURCE_TYPE = eINSTANCE.getPhysicalResourceType();

		/**
		 * The meta object literal for the '<em><b>Is Shared</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHYSICAL_RESOURCE_TYPE__IS_SHARED = eINSTANCE.getPhysicalResourceType_IsShared();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.SWComponentTypeImpl <em>SW Component Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.SWComponentTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getSWComponentType()
		 * @generated
		 */
		EClass SW_COMPONENT_TYPE = eINSTANCE.getSWComponentType();

		/**
		 * The meta object literal for the '<em><b>Subtypes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT_TYPE__SUBTYPES = eINSTANCE.getSWComponentType_Subtypes();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.UserTypeImpl <em>User Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.UserTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getUserType()
		 * @generated
		 */
		EClass USER_TYPE = eINSTANCE.getUserType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ResourceTypeImpl <em>Resource Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ResourceTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getResourceType()
		 * @generated
		 */
		EClass RESOURCE_TYPE = eINSTANCE.getResourceType();

		/**
		 * The meta object literal for the '<em><b>Subtypes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_TYPE__SUBTYPES = eINSTANCE.getResourceType_Subtypes();

		/**
		 * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_TYPE__LOWER_BOUND = eINSTANCE.getResourceType_LowerBound();

		/**
		 * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_TYPE__UPPER_BOUND = eINSTANCE.getResourceType_UpperBound();

		/**
		 * The meta object literal for the '<em><b>CSuper Type</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE_TYPE__CSUPER_TYPE = eINSTANCE.getResourceType_CSuperType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ContainerProviderTypeImpl <em>Container Provider Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ContainerProviderTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getContainerProviderType()
		 * @generated
		 */
		EClass CONTAINER_PROVIDER_TYPE = eINSTANCE.getContainerProviderType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PropertyImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPERTY__UNIT = eINSTANCE.getProperty_Unit();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__KIND = eINSTANCE.getProperty_Kind();

		/**
		 * The meta object literal for the '<em><b>Val Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VAL_ORDER = eINSTANCE.getProperty_ValOrder();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeContainerImpl <em>Port Type Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeContainerImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeContainer()
		 * @generated
		 */
		EClass PORT_TYPE_CONTAINER = eINSTANCE.getPortTypeContainer();

		/**
		 * The meta object literal for the '<em><b>Port Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE_CONTAINER__PORT_TYPES = eINSTANCE.getPortTypeContainer_PortTypes();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER__DIRECTION = eINSTANCE.getParameter_Direction();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.OperationImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__RETURN_TYPE = eINSTANCE.getOperation_ReturnType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterizableElementImpl <em>Parameterizable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.ParameterizableElementImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getParameterizableElement()
		 * @generated
		 */
		EClass PARAMETERIZABLE_ELEMENT = eINSTANCE.getParameterizableElement();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETERIZABLE_ELEMENT__PARAMETER = eINSTANCE.getParameterizableElement_Parameter();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeOfferImpl <em>Port Type Offer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeOfferImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeOffer()
		 * @generated
		 */
		EClass PORT_TYPE_OFFER = eINSTANCE.getPortTypeOffer();

		/**
		 * The meta object literal for the '<em><b>Port Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE_OFFER__PORT_TYPE = eINSTANCE.getPortTypeOffer_PortType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeRequirementImpl <em>Port Type Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeRequirementImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeRequirement()
		 * @generated
		 */
		EClass PORT_TYPE_REQUIREMENT = eINSTANCE.getPortTypeRequirement();

		/**
		 * The meta object literal for the '<em><b>Port Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE_REQUIREMENT__PORT_TYPE = eINSTANCE.getPortTypeRequirement_PortType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeConnectorImpl <em>Port Type Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeConnectorImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPortTypeConnector()
		 * @generated
		 */
		EClass PORT_TYPE_CONNECTOR = eINSTANCE.getPortTypeConnector();

		/**
		 * The meta object literal for the '<em><b>Port Type Offer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER = eINSTANCE.getPortTypeConnector_PortTypeOffer();

		/**
		 * The meta object literal for the '<em><b>Port Type Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT = eINSTANCE.getPortTypeConnector_PortTypeRequirement();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.DataTypeImpl <em>Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.DataTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getDataType()
		 * @generated
		 */
		EClass DATA_TYPE = eINSTANCE.getDataType();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_TYPE__PROPERTIES = eINSTANCE.getDataType_Properties();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.SWConnectorTypeImpl <em>SW Connector Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.SWConnectorTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getSWConnectorType()
		 * @generated
		 */
		EClass SW_CONNECTOR_TYPE = eINSTANCE.getSWConnectorType();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.impl.CopyFileTypeImpl <em>Copy File Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.CopyFileTypeImpl
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getCopyFileType()
		 * @generated
		 */
		EClass COPY_FILE_TYPE = eINSTANCE.getCopyFileType();

		/**
		 * The meta object literal for the '<em><b>File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_FILE_TYPE__FILE_NAME = eINSTANCE.getCopyFileType_FileName();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COPY_FILE_TYPE__PARAMETER = eINSTANCE.getCopyFileType_Parameter();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.PropertyKind <em>Property Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.PropertyKind
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getPropertyKind()
		 * @generated
		 */
		EEnum PROPERTY_KIND = eINSTANCE.getPropertyKind();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.structure.Order <em>Order</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.structure.Order
		 * @see org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl#getOrder()
		 * @generated
		 */
		EEnum ORDER = eINSTANCE.getOrder();

	}

} //StructurePackage
