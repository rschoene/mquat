/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.coolsoftware.coolcomponents.nameable.DescribableElement;
import org.coolsoftware.coolcomponents.nameable.IdentifyableElement;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * ComponentType represent an abstract type specifying arbitrary components of QoS aware auto tuning systems
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPorttypes <em>Porttypes</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getConnectors <em>Connectors</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getEclUri <em>Ecl Uri</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPortTypeOffers <em>Port Type Offers</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getPortTypeRequirements <em>Port Type Requirements</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getComponentType()
 * @model
 * @generated
 */
public interface ComponentType extends DescribableElement, IdentifyableElement {
	/**
	 * Returns the value of the '<em><b>Porttypes</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.PortType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Porttypes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Porttypes</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getComponentType_Porttypes()
	 * @model containment="true"
	 *        annotation="gmf.affixed foo='bar'"
	 * @generated
	 */
	EList<PortType> getPorttypes();

	/**
	 * Returns the value of the '<em><b>Connectors</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connectors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connectors</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getComponentType_Connectors()
	 * @model containment="true"
	 * @generated
	 */
	EList<PortConnectorType> getConnectors();

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getComponentType_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getProperties();

	/**
	 * Returns the value of the '<em><b>Ecl Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ecl Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ecl Uri</em>' attribute.
	 * @see #setEclUri(String)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getComponentType_EclUri()
	 * @model required="true"
	 * @generated
	 */
	String getEclUri();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.ComponentType#getEclUri <em>Ecl Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ecl Uri</em>' attribute.
	 * @see #getEclUri()
	 * @generated
	 */
	void setEclUri(String value);

	/**
	 * Returns the value of the '<em><b>Port Type Offers</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeOffer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Type Offers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Type Offers</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getComponentType_PortTypeOffers()
	 * @model containment="true"
	 * @generated
	 */
	EList<PortTypeOffer> getPortTypeOffers();

	/**
	 * Returns the value of the '<em><b>Port Type Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.PortTypeRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Type Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Type Requirements</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getComponentType_PortTypeRequirements()
	 * @model containment="true"
	 * @generated
	 */
	EList<PortTypeRequirement> getPortTypeRequirements();

} // ComponentType
