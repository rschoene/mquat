/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Resource Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PhysicalResourceType#isIsShared <em>Is Shared</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPhysicalResourceType()
 * @model annotation="gmf.node label='name'"
 * @generated
 */
public interface PhysicalResourceType extends ResourceType {
	/**
	 * Returns the value of the '<em><b>Is Shared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Shared</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Shared</em>' attribute.
	 * @see #setIsShared(boolean)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPhysicalResourceType_IsShared()
	 * @model
	 * @generated
	 */
	boolean isIsShared();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.PhysicalResourceType#isIsShared <em>Is Shared</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Shared</em>' attribute.
	 * @see #isIsShared()
	 * @generated
	 */
	void setIsShared(boolean value);

} // PhysicalResourceType
