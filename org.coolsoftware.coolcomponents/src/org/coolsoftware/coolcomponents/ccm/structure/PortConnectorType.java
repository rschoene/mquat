/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.coolsoftware.coolcomponents.nameable.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port Connector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getSource <em>Source</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortConnectorType()
 * @model annotation="gmf.link label='name' source='source' target='target' target.decoration='arrow'"
 * @generated
 */
public interface PortConnectorType extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getOut <em>Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(PortType)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortConnectorType_Source()
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getOut
	 * @model opposite="out" required="true"
	 * @generated
	 */
	PortType getSource();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(PortType value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.structure.PortType#getIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(PortType)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getPortConnectorType_Target()
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PortType#getIn
	 * @model opposite="in" required="true"
	 * @generated
	 */
	PortType getTarget();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(PortType value);

} // PortConnectorType
