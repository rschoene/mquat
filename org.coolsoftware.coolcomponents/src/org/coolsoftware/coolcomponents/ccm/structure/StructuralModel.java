/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.coolsoftware.coolcomponents.types.TypeLibrary;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structural Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getRoot <em>Root</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getTypeLibraries <em>Type Libraries</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getReferencedTypeLibraries <em>Referenced Type Libraries</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getComponentTypes <em>Component Types</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getPortTypeContainer <em>Port Type Container</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getDataTypes <em>Data Types</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getStructuralModel()
 * @model annotation="gmf.diagram x='x'"
 * @generated
 */
public interface StructuralModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root</em>' containment reference.
	 * @see #setRoot(ComponentType)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getStructuralModel_Root()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ComponentType getRoot();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getRoot <em>Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root</em>' containment reference.
	 * @see #getRoot()
	 * @generated
	 */
	void setRoot(ComponentType value);

	/**
	 * Returns the value of the '<em><b>Type Libraries</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.types.TypeLibrary}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Libraries</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Libraries</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getStructuralModel_TypeLibraries()
	 * @model containment="true"
	 * @generated
	 */
	EList<TypeLibrary> getTypeLibraries();

	/**
	 * Returns the value of the '<em><b>Referenced Type Libraries</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.types.TypeLibrary}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Type Libraries</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Type Libraries</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getStructuralModel_ReferencedTypeLibraries()
	 * @model
	 * @generated
	 */
	EList<TypeLibrary> getReferencedTypeLibraries();

	/**
	 * Returns the value of the '<em><b>Component Types</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.SWComponentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Types</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getStructuralModel_ComponentTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<SWComponentType> getComponentTypes();

	/**
	 * Returns the value of the '<em><b>Port Type Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Type Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Type Container</em>' containment reference.
	 * @see #setPortTypeContainer(PortTypeContainer)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getStructuralModel_PortTypeContainer()
	 * @model containment="true"
	 * @generated
	 */
	PortTypeContainer getPortTypeContainer();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.StructuralModel#getPortTypeContainer <em>Port Type Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Type Container</em>' containment reference.
	 * @see #getPortTypeContainer()
	 * @generated
	 */
	void setPortTypeContainer(PortTypeContainer value);

	/**
	 * Returns the value of the '<em><b>Data Types</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.DataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Types</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getStructuralModel_DataTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataType> getDataTypes();

} // StructuralModel
