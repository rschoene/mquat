/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.PortTypeOffer;
import org.coolsoftware.coolcomponents.ccm.structure.PortTypeRequirement;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;
import org.coolsoftware.coolcomponents.nameable.IdentifyableElement;
import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.coolsoftware.coolcomponents.nameable.impl.DescribableElementImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl#getPorttypes <em>Porttypes</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl#getConnectors <em>Connectors</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl#getEclUri <em>Ecl Uri</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl#getPortTypeOffers <em>Port Type Offers</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.ComponentTypeImpl#getPortTypeRequirements <em>Port Type Requirements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentTypeImpl extends DescribableElementImpl implements ComponentType {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPorttypes() <em>Porttypes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPorttypes()
	 * @generated
	 * @ordered
	 */
	protected EList<PortType> porttypes;

	/**
	 * The cached value of the '{@link #getConnectors() <em>Connectors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectors()
	 * @generated
	 * @ordered
	 */
	protected EList<PortConnectorType> connectors;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<Property> properties;

	/**
	 * The default value of the '{@link #getEclUri() <em>Ecl Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEclUri()
	 * @generated
	 * @ordered
	 */
	protected static final String ECL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEclUri() <em>Ecl Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEclUri()
	 * @generated
	 * @ordered
	 */
	protected String eclUri = ECL_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortTypeOffers() <em>Port Type Offers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortTypeOffers()
	 * @generated
	 * @ordered
	 */
	protected EList<PortTypeOffer> portTypeOffers;

	/**
	 * The cached value of the '{@link #getPortTypeRequirements() <em>Port Type Requirements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortTypeRequirements()
	 * @generated
	 * @ordered
	 */
	protected EList<PortTypeRequirement> portTypeRequirements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructurePackage.Literals.COMPONENT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.COMPONENT_TYPE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortType> getPorttypes() {
		if (porttypes == null) {
			porttypes = new EObjectContainmentEList<PortType>(PortType.class, this, StructurePackage.COMPONENT_TYPE__PORTTYPES);
		}
		return porttypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortConnectorType> getConnectors() {
		if (connectors == null) {
			connectors = new EObjectContainmentEList<PortConnectorType>(PortConnectorType.class, this, StructurePackage.COMPONENT_TYPE__CONNECTORS);
		}
		return connectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Property> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<Property>(Property.class, this, StructurePackage.COMPONENT_TYPE__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEclUri() {
		return eclUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEclUri(String newEclUri) {
		String oldEclUri = eclUri;
		eclUri = newEclUri;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.COMPONENT_TYPE__ECL_URI, oldEclUri, eclUri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortTypeOffer> getPortTypeOffers() {
		if (portTypeOffers == null) {
			portTypeOffers = new EObjectContainmentEList<PortTypeOffer>(PortTypeOffer.class, this, StructurePackage.COMPONENT_TYPE__PORT_TYPE_OFFERS);
		}
		return portTypeOffers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortTypeRequirement> getPortTypeRequirements() {
		if (portTypeRequirements == null) {
			portTypeRequirements = new EObjectContainmentEList<PortTypeRequirement>(PortTypeRequirement.class, this, StructurePackage.COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS);
		}
		return portTypeRequirements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructurePackage.COMPONENT_TYPE__PORTTYPES:
				return ((InternalEList<?>)getPorttypes()).basicRemove(otherEnd, msgs);
			case StructurePackage.COMPONENT_TYPE__CONNECTORS:
				return ((InternalEList<?>)getConnectors()).basicRemove(otherEnd, msgs);
			case StructurePackage.COMPONENT_TYPE__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_OFFERS:
				return ((InternalEList<?>)getPortTypeOffers()).basicRemove(otherEnd, msgs);
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS:
				return ((InternalEList<?>)getPortTypeRequirements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructurePackage.COMPONENT_TYPE__ID:
				return getId();
			case StructurePackage.COMPONENT_TYPE__PORTTYPES:
				return getPorttypes();
			case StructurePackage.COMPONENT_TYPE__CONNECTORS:
				return getConnectors();
			case StructurePackage.COMPONENT_TYPE__PROPERTIES:
				return getProperties();
			case StructurePackage.COMPONENT_TYPE__ECL_URI:
				return getEclUri();
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_OFFERS:
				return getPortTypeOffers();
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS:
				return getPortTypeRequirements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructurePackage.COMPONENT_TYPE__ID:
				setId((String)newValue);
				return;
			case StructurePackage.COMPONENT_TYPE__PORTTYPES:
				getPorttypes().clear();
				getPorttypes().addAll((Collection<? extends PortType>)newValue);
				return;
			case StructurePackage.COMPONENT_TYPE__CONNECTORS:
				getConnectors().clear();
				getConnectors().addAll((Collection<? extends PortConnectorType>)newValue);
				return;
			case StructurePackage.COMPONENT_TYPE__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends Property>)newValue);
				return;
			case StructurePackage.COMPONENT_TYPE__ECL_URI:
				setEclUri((String)newValue);
				return;
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_OFFERS:
				getPortTypeOffers().clear();
				getPortTypeOffers().addAll((Collection<? extends PortTypeOffer>)newValue);
				return;
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS:
				getPortTypeRequirements().clear();
				getPortTypeRequirements().addAll((Collection<? extends PortTypeRequirement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructurePackage.COMPONENT_TYPE__ID:
				setId(ID_EDEFAULT);
				return;
			case StructurePackage.COMPONENT_TYPE__PORTTYPES:
				getPorttypes().clear();
				return;
			case StructurePackage.COMPONENT_TYPE__CONNECTORS:
				getConnectors().clear();
				return;
			case StructurePackage.COMPONENT_TYPE__PROPERTIES:
				getProperties().clear();
				return;
			case StructurePackage.COMPONENT_TYPE__ECL_URI:
				setEclUri(ECL_URI_EDEFAULT);
				return;
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_OFFERS:
				getPortTypeOffers().clear();
				return;
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS:
				getPortTypeRequirements().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructurePackage.COMPONENT_TYPE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case StructurePackage.COMPONENT_TYPE__PORTTYPES:
				return porttypes != null && !porttypes.isEmpty();
			case StructurePackage.COMPONENT_TYPE__CONNECTORS:
				return connectors != null && !connectors.isEmpty();
			case StructurePackage.COMPONENT_TYPE__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case StructurePackage.COMPONENT_TYPE__ECL_URI:
				return ECL_URI_EDEFAULT == null ? eclUri != null : !ECL_URI_EDEFAULT.equals(eclUri);
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_OFFERS:
				return portTypeOffers != null && !portTypeOffers.isEmpty();
			case StructurePackage.COMPONENT_TYPE__PORT_TYPE_REQUIREMENTS:
				return portTypeRequirements != null && !portTypeRequirements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IdentifyableElement.class) {
			switch (derivedFeatureID) {
				case StructurePackage.COMPONENT_TYPE__ID: return NameablePackage.IDENTIFYABLE_ELEMENT__ID;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IdentifyableElement.class) {
			switch (baseFeatureID) {
				case NameablePackage.IDENTIFYABLE_ELEMENT__ID: return StructurePackage.COMPONENT_TYPE__ID;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", eclUri: ");
		result.append(eclUri);
		result.append(')');
		return result.toString();
	}

} //ComponentTypeImpl
