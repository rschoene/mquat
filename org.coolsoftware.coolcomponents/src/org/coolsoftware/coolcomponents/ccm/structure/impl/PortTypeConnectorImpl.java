/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure.impl;

import org.coolsoftware.coolcomponents.ccm.structure.PortTypeConnector;
import org.coolsoftware.coolcomponents.ccm.structure.PortTypeOffer;
import org.coolsoftware.coolcomponents.ccm.structure.PortTypeRequirement;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Type Connector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeConnectorImpl#getPortTypeOffer <em>Port Type Offer</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeConnectorImpl#getPortTypeRequirement <em>Port Type Requirement</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PortTypeConnectorImpl extends EObjectImpl implements PortTypeConnector {
	/**
	 * The cached value of the '{@link #getPortTypeOffer() <em>Port Type Offer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortTypeOffer()
	 * @generated
	 * @ordered
	 */
	protected PortTypeOffer portTypeOffer;

	/**
	 * The cached value of the '{@link #getPortTypeRequirement() <em>Port Type Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortTypeRequirement()
	 * @generated
	 * @ordered
	 */
	protected PortTypeRequirement portTypeRequirement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortTypeConnectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructurePackage.Literals.PORT_TYPE_CONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortTypeOffer getPortTypeOffer() {
		if (portTypeOffer != null && portTypeOffer.eIsProxy()) {
			InternalEObject oldPortTypeOffer = (InternalEObject)portTypeOffer;
			portTypeOffer = (PortTypeOffer)eResolveProxy(oldPortTypeOffer);
			if (portTypeOffer != oldPortTypeOffer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER, oldPortTypeOffer, portTypeOffer));
			}
		}
		return portTypeOffer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortTypeOffer basicGetPortTypeOffer() {
		return portTypeOffer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortTypeOffer(PortTypeOffer newPortTypeOffer) {
		PortTypeOffer oldPortTypeOffer = portTypeOffer;
		portTypeOffer = newPortTypeOffer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER, oldPortTypeOffer, portTypeOffer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortTypeRequirement getPortTypeRequirement() {
		if (portTypeRequirement != null && portTypeRequirement.eIsProxy()) {
			InternalEObject oldPortTypeRequirement = (InternalEObject)portTypeRequirement;
			portTypeRequirement = (PortTypeRequirement)eResolveProxy(oldPortTypeRequirement);
			if (portTypeRequirement != oldPortTypeRequirement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT, oldPortTypeRequirement, portTypeRequirement));
			}
		}
		return portTypeRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortTypeRequirement basicGetPortTypeRequirement() {
		return portTypeRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortTypeRequirement(PortTypeRequirement newPortTypeRequirement) {
		PortTypeRequirement oldPortTypeRequirement = portTypeRequirement;
		portTypeRequirement = newPortTypeRequirement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT, oldPortTypeRequirement, portTypeRequirement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER:
				if (resolve) return getPortTypeOffer();
				return basicGetPortTypeOffer();
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT:
				if (resolve) return getPortTypeRequirement();
				return basicGetPortTypeRequirement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER:
				setPortTypeOffer((PortTypeOffer)newValue);
				return;
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT:
				setPortTypeRequirement((PortTypeRequirement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER:
				setPortTypeOffer((PortTypeOffer)null);
				return;
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT:
				setPortTypeRequirement((PortTypeRequirement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_OFFER:
				return portTypeOffer != null;
			case StructurePackage.PORT_TYPE_CONNECTOR__PORT_TYPE_REQUIREMENT:
				return portTypeRequirement != null;
		}
		return super.eIsSet(featureID);
	}

} //PortTypeConnectorImpl
