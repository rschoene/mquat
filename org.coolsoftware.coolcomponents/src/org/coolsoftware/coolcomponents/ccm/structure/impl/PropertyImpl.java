/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure.impl;

import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.PropertyKind;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;
import org.coolsoftware.coolcomponents.expressions.variables.impl.VariableDeclarationImpl;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.nameable.impl.NamedElementImpl;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.units.Unit;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PropertyImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PropertyImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PropertyImpl#getValOrder <em>Val Order</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropertyImpl extends VariableDeclarationImpl implements Property {
	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected Unit unit;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final PropertyKind KIND_EDEFAULT = PropertyKind.STATIC_INSTANCE;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected PropertyKind kind = KIND_EDEFAULT;
	/**
	 * The default value of the '{@link #getValOrder() <em>Val Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValOrder()
	 * @generated
	 * @ordered
	 */
	protected static final Order VAL_ORDER_EDEFAULT = Order.INCREASING;

	/**
	 * The cached value of the '{@link #getValOrder() <em>Val Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValOrder()
	 * @generated
	 * @ordered
	 */
	protected Order valOrder = VAL_ORDER_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructurePackage.Literals.PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit getUnit() {
		if (unit != null && unit.eIsProxy()) {
			InternalEObject oldUnit = (InternalEObject)unit;
			unit = (Unit)eResolveProxy(oldUnit);
			if (unit != oldUnit) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StructurePackage.PROPERTY__UNIT, oldUnit, unit));
			}
		}
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit basicGetUnit() {
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnit(Unit newUnit) {
		Unit oldUnit = unit;
		unit = newUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.PROPERTY__UNIT, oldUnit, unit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(PropertyKind newKind) {
		PropertyKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.PROPERTY__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Order getValOrder() {
		return valOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValOrder(Order newValOrder) {
		Order oldValOrder = valOrder;
		valOrder = newValOrder == null ? VAL_ORDER_EDEFAULT : newValOrder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.PROPERTY__VAL_ORDER, oldValOrder, valOrder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructurePackage.PROPERTY__UNIT:
				if (resolve) return getUnit();
				return basicGetUnit();
			case StructurePackage.PROPERTY__KIND:
				return getKind();
			case StructurePackage.PROPERTY__VAL_ORDER:
				return getValOrder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructurePackage.PROPERTY__UNIT:
				setUnit((Unit)newValue);
				return;
			case StructurePackage.PROPERTY__KIND:
				setKind((PropertyKind)newValue);
				return;
			case StructurePackage.PROPERTY__VAL_ORDER:
				setValOrder((Order)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructurePackage.PROPERTY__UNIT:
				setUnit((Unit)null);
				return;
			case StructurePackage.PROPERTY__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case StructurePackage.PROPERTY__VAL_ORDER:
				setValOrder(VAL_ORDER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructurePackage.PROPERTY__UNIT:
				return unit != null;
			case StructurePackage.PROPERTY__KIND:
				return kind != KIND_EDEFAULT;
			case StructurePackage.PROPERTY__VAL_ORDER:
				return valOrder != VAL_ORDER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (kind: ");
		result.append(kind);
		result.append(", valOrder: ");
		result.append(valOrder);
		result.append(')');
		return result.toString();
	}

} //PropertyImpl
