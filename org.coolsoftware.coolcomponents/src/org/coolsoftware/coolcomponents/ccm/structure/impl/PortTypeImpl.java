/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.Direction;
import org.coolsoftware.coolcomponents.ccm.Visibility;
import org.coolsoftware.coolcomponents.ccm.structure.Operation;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.ParameterizableElement;
import org.coolsoftware.coolcomponents.ccm.structure.PortConnectorType;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;
import org.coolsoftware.coolcomponents.nameable.impl.NamedElementImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Port Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getDirection <em>Direction</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getVisibility <em>Visibility</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getIn <em>In</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getOut <em>Out</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getSharedMemory <em>Shared Memory</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getRequiredPortTypes <em>Required Port Types</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getOperations <em>Operations</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.PortTypeImpl#getMetaparameter <em>Metaparameter</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PortTypeImpl extends NamedElementImpl implements PortType {
	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> parameter;

	/**
	 * The default value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected static final Direction DIRECTION_EDEFAULT = Direction.PROVIDED;

	/**
	 * The cached value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected Direction direction = DIRECTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected static final Visibility VISIBILITY_EDEFAULT = Visibility.PUBLIC;

	/**
	 * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibility()
	 * @generated
	 * @ordered
	 */
	protected Visibility visibility = VISIBILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<PortConnectorType> in;

	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<PortConnectorType> out;

	/**
	 * The cached value of the '{@link #getSharedMemory() <em>Shared Memory</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharedMemory()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> sharedMemory;

	/**
	 * The cached value of the '{@link #getRequiredPortTypes() <em>Required Port Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredPortTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<PortType> requiredPortTypes;

	/**
	 * The cached value of the '{@link #getOperations() <em>Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<Operation> operations;

	/**
	 * The cached value of the '{@link #getMetaparameter() <em>Metaparameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaparameter()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> metaparameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructurePackage.Literals.PORT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirection(Direction newDirection) {
		Direction oldDirection = direction;
		direction = newDirection == null ? DIRECTION_EDEFAULT : newDirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.PORT_TYPE__DIRECTION, oldDirection, direction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getParameter() {
		if (parameter == null) {
			parameter = new EObjectContainmentEList<Parameter>(Parameter.class, this, StructurePackage.PORT_TYPE__PARAMETER);
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Visibility getVisibility() {
		return visibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisibility(Visibility newVisibility) {
		Visibility oldVisibility = visibility;
		visibility = newVisibility == null ? VISIBILITY_EDEFAULT : newVisibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.PORT_TYPE__VISIBILITY, oldVisibility, visibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortConnectorType> getIn() {
		if (in == null) {
			in = new EObjectWithInverseResolvingEList<PortConnectorType>(PortConnectorType.class, this, StructurePackage.PORT_TYPE__IN, StructurePackage.PORT_CONNECTOR_TYPE__TARGET);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortConnectorType> getOut() {
		if (out == null) {
			out = new EObjectWithInverseResolvingEList<PortConnectorType>(PortConnectorType.class, this, StructurePackage.PORT_TYPE__OUT, StructurePackage.PORT_CONNECTOR_TYPE__SOURCE);
		}
		return out;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getSharedMemory() {
		if (sharedMemory == null) {
			sharedMemory = new EObjectContainmentEList<Parameter>(Parameter.class, this, StructurePackage.PORT_TYPE__SHARED_MEMORY);
		}
		return sharedMemory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortType> getRequiredPortTypes() {
		if (requiredPortTypes == null) {
			requiredPortTypes = new EObjectResolvingEList<PortType>(PortType.class, this, StructurePackage.PORT_TYPE__REQUIRED_PORT_TYPES);
		}
		return requiredPortTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation> getOperations() {
		if (operations == null) {
			operations = new EObjectContainmentEList<Operation>(Operation.class, this, StructurePackage.PORT_TYPE__OPERATIONS);
		}
		return operations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getMetaparameter() {
		if (metaparameter == null) {
			metaparameter = new EObjectContainmentEList<Parameter>(Parameter.class, this, StructurePackage.PORT_TYPE__METAPARAMETER);
		}
		return metaparameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE__IN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIn()).basicAdd(otherEnd, msgs);
			case StructurePackage.PORT_TYPE__OUT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOut()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE__PARAMETER:
				return ((InternalEList<?>)getParameter()).basicRemove(otherEnd, msgs);
			case StructurePackage.PORT_TYPE__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case StructurePackage.PORT_TYPE__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
			case StructurePackage.PORT_TYPE__SHARED_MEMORY:
				return ((InternalEList<?>)getSharedMemory()).basicRemove(otherEnd, msgs);
			case StructurePackage.PORT_TYPE__OPERATIONS:
				return ((InternalEList<?>)getOperations()).basicRemove(otherEnd, msgs);
			case StructurePackage.PORT_TYPE__METAPARAMETER:
				return ((InternalEList<?>)getMetaparameter()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE__PARAMETER:
				return getParameter();
			case StructurePackage.PORT_TYPE__DIRECTION:
				return getDirection();
			case StructurePackage.PORT_TYPE__VISIBILITY:
				return getVisibility();
			case StructurePackage.PORT_TYPE__IN:
				return getIn();
			case StructurePackage.PORT_TYPE__OUT:
				return getOut();
			case StructurePackage.PORT_TYPE__SHARED_MEMORY:
				return getSharedMemory();
			case StructurePackage.PORT_TYPE__REQUIRED_PORT_TYPES:
				return getRequiredPortTypes();
			case StructurePackage.PORT_TYPE__OPERATIONS:
				return getOperations();
			case StructurePackage.PORT_TYPE__METAPARAMETER:
				return getMetaparameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE__PARAMETER:
				getParameter().clear();
				getParameter().addAll((Collection<? extends Parameter>)newValue);
				return;
			case StructurePackage.PORT_TYPE__DIRECTION:
				setDirection((Direction)newValue);
				return;
			case StructurePackage.PORT_TYPE__VISIBILITY:
				setVisibility((Visibility)newValue);
				return;
			case StructurePackage.PORT_TYPE__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends PortConnectorType>)newValue);
				return;
			case StructurePackage.PORT_TYPE__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends PortConnectorType>)newValue);
				return;
			case StructurePackage.PORT_TYPE__SHARED_MEMORY:
				getSharedMemory().clear();
				getSharedMemory().addAll((Collection<? extends Parameter>)newValue);
				return;
			case StructurePackage.PORT_TYPE__REQUIRED_PORT_TYPES:
				getRequiredPortTypes().clear();
				getRequiredPortTypes().addAll((Collection<? extends PortType>)newValue);
				return;
			case StructurePackage.PORT_TYPE__OPERATIONS:
				getOperations().clear();
				getOperations().addAll((Collection<? extends Operation>)newValue);
				return;
			case StructurePackage.PORT_TYPE__METAPARAMETER:
				getMetaparameter().clear();
				getMetaparameter().addAll((Collection<? extends Parameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE__PARAMETER:
				getParameter().clear();
				return;
			case StructurePackage.PORT_TYPE__DIRECTION:
				setDirection(DIRECTION_EDEFAULT);
				return;
			case StructurePackage.PORT_TYPE__VISIBILITY:
				setVisibility(VISIBILITY_EDEFAULT);
				return;
			case StructurePackage.PORT_TYPE__IN:
				getIn().clear();
				return;
			case StructurePackage.PORT_TYPE__OUT:
				getOut().clear();
				return;
			case StructurePackage.PORT_TYPE__SHARED_MEMORY:
				getSharedMemory().clear();
				return;
			case StructurePackage.PORT_TYPE__REQUIRED_PORT_TYPES:
				getRequiredPortTypes().clear();
				return;
			case StructurePackage.PORT_TYPE__OPERATIONS:
				getOperations().clear();
				return;
			case StructurePackage.PORT_TYPE__METAPARAMETER:
				getMetaparameter().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructurePackage.PORT_TYPE__PARAMETER:
				return parameter != null && !parameter.isEmpty();
			case StructurePackage.PORT_TYPE__DIRECTION:
				return direction != DIRECTION_EDEFAULT;
			case StructurePackage.PORT_TYPE__VISIBILITY:
				return visibility != VISIBILITY_EDEFAULT;
			case StructurePackage.PORT_TYPE__IN:
				return in != null && !in.isEmpty();
			case StructurePackage.PORT_TYPE__OUT:
				return out != null && !out.isEmpty();
			case StructurePackage.PORT_TYPE__SHARED_MEMORY:
				return sharedMemory != null && !sharedMemory.isEmpty();
			case StructurePackage.PORT_TYPE__REQUIRED_PORT_TYPES:
				return requiredPortTypes != null && !requiredPortTypes.isEmpty();
			case StructurePackage.PORT_TYPE__OPERATIONS:
				return operations != null && !operations.isEmpty();
			case StructurePackage.PORT_TYPE__METAPARAMETER:
				return metaparameter != null && !metaparameter.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ParameterizableElement.class) {
			switch (derivedFeatureID) {
				case StructurePackage.PORT_TYPE__PARAMETER: return StructurePackage.PARAMETERIZABLE_ELEMENT__PARAMETER;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ParameterizableElement.class) {
			switch (baseFeatureID) {
				case StructurePackage.PARAMETERIZABLE_ELEMENT__PARAMETER: return StructurePackage.PORT_TYPE__PARAMETER;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (direction: ");
		result.append(direction);
		result.append(", visibility: ");
		result.append(visibility);
		result.append(')');
		return result.toString();
	}

} //PortTypeImpl
