/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure.impl;

import java.util.Collection;
import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.DataType;
import org.coolsoftware.coolcomponents.ccm.structure.PortTypeContainer;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;
import org.coolsoftware.coolcomponents.types.TypeLibrary;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Structural Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl#getRoot <em>Root</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl#getTypeLibraries <em>Type Libraries</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl#getReferencedTypeLibraries <em>Referenced Type Libraries</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl#getComponentTypes <em>Component Types</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl#getPortTypeContainer <em>Port Type Container</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.impl.StructuralModelImpl#getDataTypes <em>Data Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StructuralModelImpl extends EObjectImpl implements StructuralModel {
	/**
	 * The cached value of the '{@link #getRoot() <em>Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoot()
	 * @generated
	 * @ordered
	 */
	protected ComponentType root;

	/**
	 * The cached value of the '{@link #getTypeLibraries() <em>Type Libraries</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeLibraries()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeLibrary> typeLibraries;
	/**
	 * The cached value of the '{@link #getReferencedTypeLibraries() <em>Referenced Type Libraries</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedTypeLibraries()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeLibrary> referencedTypeLibraries;
	/**
	 * The cached value of the '{@link #getComponentTypes() <em>Component Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<SWComponentType> componentTypes;
	/**
	 * The cached value of the '{@link #getPortTypeContainer() <em>Port Type Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortTypeContainer()
	 * @generated
	 * @ordered
	 */
	protected PortTypeContainer portTypeContainer;

	/**
	 * The cached value of the '{@link #getDataTypes() <em>Data Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<DataType> dataTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StructuralModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StructurePackage.Literals.STRUCTURAL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType getRoot() {
		return root;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoot(ComponentType newRoot, NotificationChain msgs) {
		ComponentType oldRoot = root;
		root = newRoot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructurePackage.STRUCTURAL_MODEL__ROOT, oldRoot, newRoot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoot(ComponentType newRoot) {
		if (newRoot != root) {
			NotificationChain msgs = null;
			if (root != null)
				msgs = ((InternalEObject)root).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructurePackage.STRUCTURAL_MODEL__ROOT, null, msgs);
			if (newRoot != null)
				msgs = ((InternalEObject)newRoot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructurePackage.STRUCTURAL_MODEL__ROOT, null, msgs);
			msgs = basicSetRoot(newRoot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.STRUCTURAL_MODEL__ROOT, newRoot, newRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeLibrary> getTypeLibraries() {
		if (typeLibraries == null) {
			typeLibraries = new EObjectContainmentEList<TypeLibrary>(TypeLibrary.class, this, StructurePackage.STRUCTURAL_MODEL__TYPE_LIBRARIES);
		}
		return typeLibraries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeLibrary> getReferencedTypeLibraries() {
		if (referencedTypeLibraries == null) {
			referencedTypeLibraries = new EObjectResolvingEList<TypeLibrary>(TypeLibrary.class, this, StructurePackage.STRUCTURAL_MODEL__REFERENCED_TYPE_LIBRARIES);
		}
		return referencedTypeLibraries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SWComponentType> getComponentTypes() {
		if (componentTypes == null) {
			componentTypes = new EObjectContainmentEList<SWComponentType>(SWComponentType.class, this, StructurePackage.STRUCTURAL_MODEL__COMPONENT_TYPES);
		}
		return componentTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortTypeContainer getPortTypeContainer() {
		return portTypeContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPortTypeContainer(PortTypeContainer newPortTypeContainer, NotificationChain msgs) {
		PortTypeContainer oldPortTypeContainer = portTypeContainer;
		portTypeContainer = newPortTypeContainer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER, oldPortTypeContainer, newPortTypeContainer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortTypeContainer(PortTypeContainer newPortTypeContainer) {
		if (newPortTypeContainer != portTypeContainer) {
			NotificationChain msgs = null;
			if (portTypeContainer != null)
				msgs = ((InternalEObject)portTypeContainer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER, null, msgs);
			if (newPortTypeContainer != null)
				msgs = ((InternalEObject)newPortTypeContainer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER, null, msgs);
			msgs = basicSetPortTypeContainer(newPortTypeContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER, newPortTypeContainer, newPortTypeContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataType> getDataTypes() {
		if (dataTypes == null) {
			dataTypes = new EObjectContainmentEList<DataType>(DataType.class, this, StructurePackage.STRUCTURAL_MODEL__DATA_TYPES);
		}
		return dataTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StructurePackage.STRUCTURAL_MODEL__ROOT:
				return basicSetRoot(null, msgs);
			case StructurePackage.STRUCTURAL_MODEL__TYPE_LIBRARIES:
				return ((InternalEList<?>)getTypeLibraries()).basicRemove(otherEnd, msgs);
			case StructurePackage.STRUCTURAL_MODEL__COMPONENT_TYPES:
				return ((InternalEList<?>)getComponentTypes()).basicRemove(otherEnd, msgs);
			case StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER:
				return basicSetPortTypeContainer(null, msgs);
			case StructurePackage.STRUCTURAL_MODEL__DATA_TYPES:
				return ((InternalEList<?>)getDataTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StructurePackage.STRUCTURAL_MODEL__ROOT:
				return getRoot();
			case StructurePackage.STRUCTURAL_MODEL__TYPE_LIBRARIES:
				return getTypeLibraries();
			case StructurePackage.STRUCTURAL_MODEL__REFERENCED_TYPE_LIBRARIES:
				return getReferencedTypeLibraries();
			case StructurePackage.STRUCTURAL_MODEL__COMPONENT_TYPES:
				return getComponentTypes();
			case StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER:
				return getPortTypeContainer();
			case StructurePackage.STRUCTURAL_MODEL__DATA_TYPES:
				return getDataTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StructurePackage.STRUCTURAL_MODEL__ROOT:
				setRoot((ComponentType)newValue);
				return;
			case StructurePackage.STRUCTURAL_MODEL__TYPE_LIBRARIES:
				getTypeLibraries().clear();
				getTypeLibraries().addAll((Collection<? extends TypeLibrary>)newValue);
				return;
			case StructurePackage.STRUCTURAL_MODEL__REFERENCED_TYPE_LIBRARIES:
				getReferencedTypeLibraries().clear();
				getReferencedTypeLibraries().addAll((Collection<? extends TypeLibrary>)newValue);
				return;
			case StructurePackage.STRUCTURAL_MODEL__COMPONENT_TYPES:
				getComponentTypes().clear();
				getComponentTypes().addAll((Collection<? extends SWComponentType>)newValue);
				return;
			case StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER:
				setPortTypeContainer((PortTypeContainer)newValue);
				return;
			case StructurePackage.STRUCTURAL_MODEL__DATA_TYPES:
				getDataTypes().clear();
				getDataTypes().addAll((Collection<? extends DataType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StructurePackage.STRUCTURAL_MODEL__ROOT:
				setRoot((ComponentType)null);
				return;
			case StructurePackage.STRUCTURAL_MODEL__TYPE_LIBRARIES:
				getTypeLibraries().clear();
				return;
			case StructurePackage.STRUCTURAL_MODEL__REFERENCED_TYPE_LIBRARIES:
				getReferencedTypeLibraries().clear();
				return;
			case StructurePackage.STRUCTURAL_MODEL__COMPONENT_TYPES:
				getComponentTypes().clear();
				return;
			case StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER:
				setPortTypeContainer((PortTypeContainer)null);
				return;
			case StructurePackage.STRUCTURAL_MODEL__DATA_TYPES:
				getDataTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StructurePackage.STRUCTURAL_MODEL__ROOT:
				return root != null;
			case StructurePackage.STRUCTURAL_MODEL__TYPE_LIBRARIES:
				return typeLibraries != null && !typeLibraries.isEmpty();
			case StructurePackage.STRUCTURAL_MODEL__REFERENCED_TYPE_LIBRARIES:
				return referencedTypeLibraries != null && !referencedTypeLibraries.isEmpty();
			case StructurePackage.STRUCTURAL_MODEL__COMPONENT_TYPES:
				return componentTypes != null && !componentTypes.isEmpty();
			case StructurePackage.STRUCTURAL_MODEL__PORT_TYPE_CONTAINER:
				return portTypeContainer != null;
			case StructurePackage.STRUCTURAL_MODEL__DATA_TYPES:
				return dataTypes != null && !dataTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //StructuralModelImpl
