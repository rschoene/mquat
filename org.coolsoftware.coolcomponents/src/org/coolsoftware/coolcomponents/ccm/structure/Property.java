/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.units.Unit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getUnit <em>Unit</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getKind <em>Kind</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getValOrder <em>Val Order</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getProperty()
 * @model
 * @generated
 */
public interface Property extends VariableDeclaration {
	/**
	 * Returns the value of the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' reference.
	 * @see #setUnit(Unit)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getProperty_Unit()
	 * @model required="true"
	 * @generated
	 */
	Unit getUnit();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getUnit <em>Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' reference.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(Unit value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.coolsoftware.coolcomponents.ccm.structure.PropertyKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PropertyKind
	 * @see #setKind(PropertyKind)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getProperty_Kind()
	 * @model required="true"
	 * @generated
	 */
	PropertyKind getKind();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.PropertyKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(PropertyKind value);

	/**
	 * Returns the value of the '<em><b>Val Order</b></em>' attribute.
	 * The literals are from the enumeration {@link org.coolsoftware.coolcomponents.ccm.structure.Order}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Order</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Order
	 * @see #setValOrder(Order)
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getProperty_ValOrder()
	 * @model required="true"
	 * @generated
	 */
	Order getValOrder();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.structure.Property#getValOrder <em>Val Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Val Order</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.Order
	 * @see #getValOrder()
	 * @generated
	 */
	void setValOrder(Order value);

} // Property
