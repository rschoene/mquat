/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.structure;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SW Component Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.structure.SWComponentType#getSubtypes <em>Subtypes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getSWComponentType()
 * @model annotation="gmf.node label='name'"
 * @generated
 */
public interface SWComponentType extends ComponentType {
	/**
	 * Returns the value of the '<em><b>Subtypes</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.SWComponentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtypes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtypes</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.structure.StructurePackage#getSWComponentType_Subtypes()
	 * @model containment="true"
	 *        annotation="gmf.compartment collapsible='true'"
	 * @generated
	 */
	EList<SWComponentType> getSubtypes();

} // SWComponentType
