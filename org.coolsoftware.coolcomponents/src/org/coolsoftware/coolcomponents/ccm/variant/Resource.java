/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.variant;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Resource#getSubresources <em>Subresources</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Resource#getLastChanged <em>Last Changed</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getResource()
 * @model
 * @generated
 */
public interface Resource extends Component {
	/**
	 * Returns the value of the '<em><b>Subresources</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subresources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subresources</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getResource_Subresources()
	 * @model containment="true"
	 * @generated
	 */
	EList<Resource> getSubresources();

	/**
	 * Returns the value of the '<em><b>Last Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Changed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Changed</em>' attribute.
	 * @see #setLastChanged(Long)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getResource_LastChanged()
	 * @model
	 * @generated
	 */
	Long getLastChanged();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Resource#getLastChanged <em>Last Changed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Changed</em>' attribute.
	 * @see #getLastChanged()
	 * @generated
	 */
	void setLastChanged(Long value);

} // Resource
