/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.variant;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getPorts <em>Ports</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getPortconnectors <em>Portconnectors</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getPropertyBinding <em>Property Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getComponent()
 * @model abstract="true"
 * @generated
 */
public interface Component extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Ports</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.Port}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ports</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getComponent_Ports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Port> getPorts();

	/**
	 * Returns the value of the '<em><b>Behavior</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.variant.Behavior#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behavior</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavior</em>' containment reference.
	 * @see #setBehavior(Behavior)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getComponent_Behavior()
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Behavior#getComponent
	 * @model opposite="component" containment="true" required="true"
	 * @generated
	 */
	Behavior getBehavior();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getBehavior <em>Behavior</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Behavior</em>' containment reference.
	 * @see #getBehavior()
	 * @generated
	 */
	void setBehavior(Behavior value);

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' reference.
	 * @see #setSpecification(ComponentType)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getComponent_Specification()
	 * @model required="true"
	 * @generated
	 */
	ComponentType getSpecification();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getSpecification <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' reference.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(ComponentType value);

	/**
	 * Returns the value of the '<em><b>Portconnectors</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.PortConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Portconnectors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Portconnectors</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getComponent_Portconnectors()
	 * @model containment="true"
	 * @generated
	 */
	EList<PortConnector> getPortconnectors();

	/**
	 * Returns the value of the '<em><b>Property Binding</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Binding</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Binding</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getComponent_PropertyBinding()
	 * @model containment="true"
	 * @generated
	 */
	EList<VariantPropertyBinding> getPropertyBinding();

} // Component
