/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.variant;

import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantFactory
 * @model kind="package"
 * @generated
 */
public interface VariantPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "variant";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://cool-software.org/ccm/1.0/variant";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "variant";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VariantPackage eINSTANCE = org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.BehaviorImpl <em>Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.BehaviorImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getBehavior()
	 * @generated
	 */
	int BEHAVIOR = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR__PARAMETER_BINDING = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR__TEMPLATE = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR__LINKS = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Component</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR__COMPONENT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PORTS = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__BEHAVIOR = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__SPECIFICATION = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Portconnectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PORTCONNECTORS = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Property Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PROPERTY_BINDING = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ResourceImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__PORTS = COMPONENT__PORTS;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__BEHAVIOR = COMPONENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__SPECIFICATION = COMPONENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Portconnectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__PORTCONNECTORS = COMPONENT__PORTCONNECTORS;

	/**
	 * The feature id for the '<em><b>Property Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__PROPERTY_BINDING = COMPONENT__PROPERTY_BINDING;

	/**
	 * The feature id for the '<em><b>Subresources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__SUBRESOURCES = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Last Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__LAST_CHANGED = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ContainerProviderImpl <em>Container Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ContainerProviderImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getContainerProvider()
	 * @generated
	 */
	int CONTAINER_PROVIDER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__NAME = RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__PORTS = RESOURCE__PORTS;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__BEHAVIOR = RESOURCE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__SPECIFICATION = RESOURCE__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Portconnectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__PORTCONNECTORS = RESOURCE__PORTCONNECTORS;

	/**
	 * The feature id for the '<em><b>Property Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__PROPERTY_BINDING = RESOURCE__PROPERTY_BINDING;

	/**
	 * The feature id for the '<em><b>Subresources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__SUBRESOURCES = RESOURCE__SUBRESOURCES;

	/**
	 * The feature id for the '<em><b>Last Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__LAST_CHANGED = RESOURCE__LAST_CHANGED;

	/**
	 * The feature id for the '<em><b>Uses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__USES = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER__SCHEDULE = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Container Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_PROVIDER_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.CostParameterBindingImpl <em>Cost Parameter Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.CostParameterBindingImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getCostParameterBinding()
	 * @generated
	 */
	int COST_PARAMETER_BINDING = 3;

	/**
	 * The feature id for the '<em><b>Declared Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COST_PARAMETER_BINDING__DECLARED_VARIABLE = VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COST_PARAMETER_BINDING__PARAMETER = VariablesPackage.VARIABLE_DECLARATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cost Parameter Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COST_PARAMETER_BINDING_FEATURE_COUNT = VariablesPackage.VARIABLE_DECLARATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.PortImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__SPECIFICATION = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.PortConnectorImpl <em>Port Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.PortConnectorImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getPortConnector()
	 * @generated
	 */
	int PORT_CONNECTOR = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR__SOURCE = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR__TARGET = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Port Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_CONNECTOR_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.PhysicalResourceImpl <em>Physical Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.PhysicalResourceImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getPhysicalResource()
	 * @generated
	 */
	int PHYSICAL_RESOURCE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__NAME = RESOURCE__NAME;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__PORTS = RESOURCE__PORTS;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__BEHAVIOR = RESOURCE__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__SPECIFICATION = RESOURCE__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Portconnectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__PORTCONNECTORS = RESOURCE__PORTCONNECTORS;

	/**
	 * The feature id for the '<em><b>Property Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__PROPERTY_BINDING = RESOURCE__PROPERTY_BINDING;

	/**
	 * The feature id for the '<em><b>Subresources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__SUBRESOURCES = RESOURCE__SUBRESOURCES;

	/**
	 * The feature id for the '<em><b>Last Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__LAST_CHANGED = RESOURCE__LAST_CHANGED;

	/**
	 * The feature id for the '<em><b>Used By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE__USED_BY = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHYSICAL_RESOURCE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.SWComponentImpl <em>SW Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.SWComponentImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getSWComponent()
	 * @generated
	 */
	int SW_COMPONENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__PORTS = COMPONENT__PORTS;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__BEHAVIOR = COMPONENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__SPECIFICATION = COMPONENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Portconnectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__PORTCONNECTORS = COMPONENT__PORTCONNECTORS;

	/**
	 * The feature id for the '<em><b>Property Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__PROPERTY_BINDING = COMPONENT__PROPERTY_BINDING;

	/**
	 * The feature id for the '<em><b>Subtypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__SUBTYPES = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Jobs</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT__JOBS = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SW Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.UserImpl <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.UserImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getUser()
	 * @generated
	 */
	int USER = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PORTS = COMPONENT__PORTS;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__BEHAVIOR = COMPONENT__BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__SPECIFICATION = COMPONENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Portconnectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PORTCONNECTORS = COMPONENT__PORTCONNECTORS;

	/**
	 * The feature id for the '<em><b>Property Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PROPERTY_BINDING = COMPONENT__PROPERTY_BINDING;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.LinkImpl <em>Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.LinkImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getLink()
	 * @generated
	 */
	int LINK = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__PORT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__PIN = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.VariantModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantModelImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getVariantModel()
	 * @generated
	 */
	int VARIANT_MODEL = 11;

	/**
	 * The feature id for the '<em><b>Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIANT_MODEL__ROOT = 0;

	/**
	 * The feature id for the '<em><b>Data Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIANT_MODEL__DATA_ELEMENTS = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIANT_MODEL_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPropertyBindingImpl <em>Property Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPropertyBindingImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getVariantPropertyBinding()
	 * @generated
	 */
	int VARIANT_PROPERTY_BINDING = 12;

	/**
	 * The feature id for the '<em><b>Referred Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIANT_PROPERTY_BINDING__REFERRED_VARIABLE = VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE;

	/**
	 * The feature id for the '<em><b>Value Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIANT_PROPERTY_BINDING__VALUE_EXPRESSION = VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIANT_PROPERTY_BINDING__PROPERTY = VariablesPackage.VARIABLE_ASSIGNMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Property Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIANT_PROPERTY_BINDING_FEATURE_COUNT = VariablesPackage.VARIABLE_ASSIGNMENT_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.DataElementImpl <em>Data Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.DataElementImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getDataElement()
	 * @generated
	 */
	int DATA_ELEMENT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__SPECIFICATION = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Deployed On</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__DEPLOYED_ON = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Property Binding</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT__PROPERTY_BINDING = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Data Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_ELEMENT_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ScheduleImpl <em>Schedule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ScheduleImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getSchedule()
	 * @generated
	 */
	int SCHEDULE = 14;

	/**
	 * The feature id for the '<em><b>Jobs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__JOBS = 0;

	/**
	 * The number of structural features of the '<em>Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl <em>Job</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getJob()
	 * @generated
	 */
	int JOB = 15;

	/**
	 * The feature id for the '<em><b>Impl</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__IMPL = 0;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__START_TIME = 1;

	/**
	 * The feature id for the '<em><b>Predicted Runtime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__PREDICTED_RUNTIME = 2;

	/**
	 * The feature id for the '<em><b>Job Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__JOB_ID = 3;

	/**
	 * The feature id for the '<em><b>Raw Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__RAW_RESULT = 4;

	/**
	 * The feature id for the '<em><b>Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__TASK_ID = 5;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__END_TIME = 6;

	/**
	 * The feature id for the '<em><b>Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__HOST = 7;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB__STATUS = 8;

	/**
	 * The number of structural features of the '<em>Job</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_FEATURE_COUNT = 9;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.variant.JobStatus <em>Job Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.variant.JobStatus
	 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getJobStatus()
	 * @generated
	 */
	int JOB_STATUS = 16;


	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.Behavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behavior</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Behavior
	 * @generated
	 */
	EClass getBehavior();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.Behavior#getParameterBinding <em>Parameter Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Binding</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Behavior#getParameterBinding()
	 * @see #getBehavior()
	 * @generated
	 */
	EReference getBehavior_ParameterBinding();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Behavior#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Template</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Behavior#getTemplate()
	 * @see #getBehavior()
	 * @generated
	 */
	EReference getBehavior_Template();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.Behavior#getLinks <em>Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Links</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Behavior#getLinks()
	 * @see #getBehavior()
	 * @generated
	 */
	EReference getBehavior_Links();

	/**
	 * Returns the meta object for the container reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Behavior#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Component</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Behavior#getComponent()
	 * @see #getBehavior()
	 * @generated
	 */
	EReference getBehavior_Component();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getPorts <em>Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ports</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Component#getPorts()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Ports();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getBehavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Behavior</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Component#getBehavior()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Behavior();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Component#getSpecification()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Specification();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getPortconnectors <em>Portconnectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Portconnectors</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Component#getPortconnectors()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Portconnectors();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.Component#getPropertyBinding <em>Property Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Binding</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Component#getPropertyBinding()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_PropertyBinding();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider <em>Container Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container Provider</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider
	 * @generated
	 */
	EClass getContainerProvider();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider#getUses <em>Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uses</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider#getUses()
	 * @see #getContainerProvider()
	 * @generated
	 */
	EReference getContainerProvider_Uses();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Schedule</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider#getSchedule()
	 * @see #getContainerProvider()
	 * @generated
	 */
	EReference getContainerProvider_Schedule();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.CostParameterBinding <em>Cost Parameter Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cost Parameter Binding</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.CostParameterBinding
	 * @generated
	 */
	EClass getCostParameterBinding();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.CostParameterBinding#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.CostParameterBinding#getParameter()
	 * @see #getCostParameterBinding()
	 * @generated
	 */
	EReference getCostParameterBinding_Parameter();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Port#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Port#getSpecification()
	 * @see #getPort()
	 * @generated
	 */
	EReference getPort_Specification();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.PortConnector <em>Port Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port Connector</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.PortConnector
	 * @generated
	 */
	EClass getPortConnector();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.PortConnector#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.PortConnector#getSource()
	 * @see #getPortConnector()
	 * @generated
	 */
	EReference getPortConnector_Source();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.PortConnector#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.PortConnector#getTarget()
	 * @see #getPortConnector()
	 * @generated
	 */
	EReference getPortConnector_Target();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.PhysicalResource <em>Physical Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Resource</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.PhysicalResource
	 * @generated
	 */
	EClass getPhysicalResource();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.PhysicalResource#getUsedBy <em>Used By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Used By</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.PhysicalResource#getUsedBy()
	 * @see #getPhysicalResource()
	 * @generated
	 */
	EReference getPhysicalResource_UsedBy();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.Resource#getSubresources <em>Subresources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subresources</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Resource#getSubresources()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Subresources();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Resource#getLastChanged <em>Last Changed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Changed</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Resource#getLastChanged()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_LastChanged();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent <em>SW Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Component</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.SWComponent
	 * @generated
	 */
	EClass getSWComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getSubtypes <em>Subtypes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subtypes</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getSubtypes()
	 * @see #getSWComponent()
	 * @generated
	 */
	EReference getSWComponent_Subtypes();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getJobs <em>Jobs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Jobs</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getJobs()
	 * @see #getSWComponent()
	 * @generated
	 */
	EReference getSWComponent_Jobs();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.Link <em>Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Link
	 * @generated
	 */
	EClass getLink();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Link#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Link#getPort()
	 * @see #getLink()
	 * @generated
	 */
	EReference getLink_Port();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Link#getPin <em>Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Pin</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Link#getPin()
	 * @see #getLink()
	 * @generated
	 */
	EReference getLink_Pin();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.VariantModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantModel
	 * @generated
	 */
	EClass getVariantModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.variant.VariantModel#getRoot <em>Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantModel#getRoot()
	 * @see #getVariantModel()
	 * @generated
	 */
	EReference getVariantModel_Root();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.VariantModel#getDataElements <em>Data Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Elements</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantModel#getDataElements()
	 * @see #getVariantModel()
	 * @generated
	 */
	EReference getVariantModel_DataElements();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding <em>Property Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Binding</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding
	 * @generated
	 */
	EClass getVariantPropertyBinding();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding#getProperty()
	 * @see #getVariantPropertyBinding()
	 * @generated
	 */
	EReference getVariantPropertyBinding_Property();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement <em>Data Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Element</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.DataElement
	 * @generated
	 */
	EClass getDataElement();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Specification</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.DataElement#getSpecification()
	 * @see #getDataElement()
	 * @generated
	 */
	EReference getDataElement_Specification();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getDeployedOn <em>Deployed On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Deployed On</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.DataElement#getDeployedOn()
	 * @see #getDataElement()
	 * @generated
	 */
	EReference getDataElement_DeployedOn();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getPropertyBinding <em>Property Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Binding</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.DataElement#getPropertyBinding()
	 * @see #getDataElement()
	 * @generated
	 */
	EReference getDataElement_PropertyBinding();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.Schedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Schedule
	 * @generated
	 */
	EClass getSchedule();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.variant.Schedule#getJobs <em>Jobs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Jobs</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Schedule#getJobs()
	 * @see #getSchedule()
	 * @generated
	 */
	EReference getSchedule_Jobs();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.variant.Job <em>Job</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Job</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job
	 * @generated
	 */
	EClass getJob();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getImpl <em>Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Impl</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getImpl()
	 * @see #getJob()
	 * @generated
	 */
	EReference getJob_Impl();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getStartTime()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getPredictedRuntime <em>Predicted Runtime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Predicted Runtime</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getPredictedRuntime()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_PredictedRuntime();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getJobId <em>Job Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Job Id</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getJobId()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_JobId();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getRawResult <em>Raw Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Raw Result</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getRawResult()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_RawResult();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getTaskId <em>Task Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Task Id</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getTaskId()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_TaskId();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getEndTime()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getHost <em>Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Host</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getHost()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_Host();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getStatus()
	 * @see #getJob()
	 * @generated
	 */
	EAttribute getJob_Status();

	/**
	 * Returns the meta object for enum '{@link org.coolsoftware.coolcomponents.ccm.variant.JobStatus <em>Job Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Job Status</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.JobStatus
	 * @generated
	 */
	EEnum getJobStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VariantFactory getVariantFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.BehaviorImpl <em>Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.BehaviorImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getBehavior()
		 * @generated
		 */
		EClass BEHAVIOR = eINSTANCE.getBehavior();

		/**
		 * The meta object literal for the '<em><b>Parameter Binding</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR__PARAMETER_BINDING = eINSTANCE.getBehavior_ParameterBinding();

		/**
		 * The meta object literal for the '<em><b>Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR__TEMPLATE = eINSTANCE.getBehavior_Template();

		/**
		 * The meta object literal for the '<em><b>Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR__LINKS = eINSTANCE.getBehavior_Links();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR__COMPONENT = eINSTANCE.getBehavior_Component();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PORTS = eINSTANCE.getComponent_Ports();

		/**
		 * The meta object literal for the '<em><b>Behavior</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__BEHAVIOR = eINSTANCE.getComponent_Behavior();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__SPECIFICATION = eINSTANCE.getComponent_Specification();

		/**
		 * The meta object literal for the '<em><b>Portconnectors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PORTCONNECTORS = eINSTANCE.getComponent_Portconnectors();

		/**
		 * The meta object literal for the '<em><b>Property Binding</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PROPERTY_BINDING = eINSTANCE.getComponent_PropertyBinding();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ContainerProviderImpl <em>Container Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ContainerProviderImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getContainerProvider()
		 * @generated
		 */
		EClass CONTAINER_PROVIDER = eINSTANCE.getContainerProvider();

		/**
		 * The meta object literal for the '<em><b>Uses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER_PROVIDER__USES = eINSTANCE.getContainerProvider_Uses();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER_PROVIDER__SCHEDULE = eINSTANCE.getContainerProvider_Schedule();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.CostParameterBindingImpl <em>Cost Parameter Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.CostParameterBindingImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getCostParameterBinding()
		 * @generated
		 */
		EClass COST_PARAMETER_BINDING = eINSTANCE.getCostParameterBinding();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COST_PARAMETER_BINDING__PARAMETER = eINSTANCE.getCostParameterBinding_Parameter();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.PortImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT__SPECIFICATION = eINSTANCE.getPort_Specification();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.PortConnectorImpl <em>Port Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.PortConnectorImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getPortConnector()
		 * @generated
		 */
		EClass PORT_CONNECTOR = eINSTANCE.getPortConnector();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_CONNECTOR__SOURCE = eINSTANCE.getPortConnector_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_CONNECTOR__TARGET = eINSTANCE.getPortConnector_Target();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.PhysicalResourceImpl <em>Physical Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.PhysicalResourceImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getPhysicalResource()
		 * @generated
		 */
		EClass PHYSICAL_RESOURCE = eINSTANCE.getPhysicalResource();

		/**
		 * The meta object literal for the '<em><b>Used By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHYSICAL_RESOURCE__USED_BY = eINSTANCE.getPhysicalResource_UsedBy();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ResourceImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
		 * The meta object literal for the '<em><b>Subresources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__SUBRESOURCES = eINSTANCE.getResource_Subresources();

		/**
		 * The meta object literal for the '<em><b>Last Changed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__LAST_CHANGED = eINSTANCE.getResource_LastChanged();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.SWComponentImpl <em>SW Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.SWComponentImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getSWComponent()
		 * @generated
		 */
		EClass SW_COMPONENT = eINSTANCE.getSWComponent();

		/**
		 * The meta object literal for the '<em><b>Subtypes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT__SUBTYPES = eINSTANCE.getSWComponent_Subtypes();

		/**
		 * The meta object literal for the '<em><b>Jobs</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMPONENT__JOBS = eINSTANCE.getSWComponent_Jobs();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.UserImpl <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.UserImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.LinkImpl <em>Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.LinkImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getLink()
		 * @generated
		 */
		EClass LINK = eINSTANCE.getLink();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK__PORT = eINSTANCE.getLink_Port();

		/**
		 * The meta object literal for the '<em><b>Pin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK__PIN = eINSTANCE.getLink_Pin();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.VariantModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantModelImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getVariantModel()
		 * @generated
		 */
		EClass VARIANT_MODEL = eINSTANCE.getVariantModel();

		/**
		 * The meta object literal for the '<em><b>Root</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIANT_MODEL__ROOT = eINSTANCE.getVariantModel_Root();

		/**
		 * The meta object literal for the '<em><b>Data Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIANT_MODEL__DATA_ELEMENTS = eINSTANCE.getVariantModel_DataElements();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPropertyBindingImpl <em>Property Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPropertyBindingImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getVariantPropertyBinding()
		 * @generated
		 */
		EClass VARIANT_PROPERTY_BINDING = eINSTANCE.getVariantPropertyBinding();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIANT_PROPERTY_BINDING__PROPERTY = eINSTANCE.getVariantPropertyBinding_Property();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.DataElementImpl <em>Data Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.DataElementImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getDataElement()
		 * @generated
		 */
		EClass DATA_ELEMENT = eINSTANCE.getDataElement();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT__SPECIFICATION = eINSTANCE.getDataElement_Specification();

		/**
		 * The meta object literal for the '<em><b>Deployed On</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT__DEPLOYED_ON = eINSTANCE.getDataElement_DeployedOn();

		/**
		 * The meta object literal for the '<em><b>Property Binding</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_ELEMENT__PROPERTY_BINDING = eINSTANCE.getDataElement_PropertyBinding();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ScheduleImpl <em>Schedule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.ScheduleImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getSchedule()
		 * @generated
		 */
		EClass SCHEDULE = eINSTANCE.getSchedule();

		/**
		 * The meta object literal for the '<em><b>Jobs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE__JOBS = eINSTANCE.getSchedule_Jobs();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl <em>Job</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getJob()
		 * @generated
		 */
		EClass JOB = eINSTANCE.getJob();

		/**
		 * The meta object literal for the '<em><b>Impl</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JOB__IMPL = eINSTANCE.getJob_Impl();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__START_TIME = eINSTANCE.getJob_StartTime();

		/**
		 * The meta object literal for the '<em><b>Predicted Runtime</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__PREDICTED_RUNTIME = eINSTANCE.getJob_PredictedRuntime();

		/**
		 * The meta object literal for the '<em><b>Job Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__JOB_ID = eINSTANCE.getJob_JobId();

		/**
		 * The meta object literal for the '<em><b>Raw Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__RAW_RESULT = eINSTANCE.getJob_RawResult();

		/**
		 * The meta object literal for the '<em><b>Task Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__TASK_ID = eINSTANCE.getJob_TaskId();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__END_TIME = eINSTANCE.getJob_EndTime();

		/**
		 * The meta object literal for the '<em><b>Host</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__HOST = eINSTANCE.getJob_Host();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB__STATUS = eINSTANCE.getJob_Status();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.variant.JobStatus <em>Job Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.variant.JobStatus
		 * @see org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl#getJobStatus()
		 * @generated
		 */
		EEnum JOB_STATUS = eINSTANCE.getJobStatus();

	}

} //VariantPackage
