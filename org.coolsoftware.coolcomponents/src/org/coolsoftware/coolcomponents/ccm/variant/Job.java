/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.coolsoftware.coolcomponents.ccm.variant;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getImpl <em>Impl</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getPredictedRuntime <em>Predicted Runtime</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getJobId <em>Job Id</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getRawResult <em>Raw Result</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getTaskId <em>Task Id</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getHost <em>Host</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getStatus <em>Status</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob()
 * @model
 * @generated
 */
public interface Job extends EObject {
	/**
	 * Returns the value of the '<em><b>Impl</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getJobs <em>Jobs</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impl</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impl</em>' reference.
	 * @see #setImpl(SWComponent)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_Impl()
	 * @see org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getJobs
	 * @model opposite="jobs" required="true"
	 * @generated
	 */
	SWComponent getImpl();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getImpl <em>Impl</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impl</em>' reference.
	 * @see #getImpl()
	 * @generated
	 */
	void setImpl(SWComponent value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Long)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_StartTime()
	 * @model
	 * @generated
	 */
	Long getStartTime();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Long value);

	/**
	 * Returns the value of the '<em><b>Predicted Runtime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predicted Runtime</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicted Runtime</em>' attribute.
	 * @see #setPredictedRuntime(double)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_PredictedRuntime()
	 * @model
	 * @generated
	 */
	double getPredictedRuntime();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getPredictedRuntime <em>Predicted Runtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predicted Runtime</em>' attribute.
	 * @see #getPredictedRuntime()
	 * @generated
	 */
	void setPredictedRuntime(double value);

	/**
	 * Returns the value of the '<em><b>Job Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Job Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Job Id</em>' attribute.
	 * @see #setJobId(long)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_JobId()
	 * @model required="true"
	 * @generated
	 */
	long getJobId();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getJobId <em>Job Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Job Id</em>' attribute.
	 * @see #getJobId()
	 * @generated
	 */
	void setJobId(long value);

	/**
	 * Returns the value of the '<em><b>Raw Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raw Result</em>' attribute.
	 * @see #setRawResult(Object)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_RawResult()
	 * @model
	 * @generated
	 */
	Object getRawResult();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getRawResult <em>Raw Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Raw Result</em>' attribute.
	 * @see #getRawResult()
	 * @generated
	 */
	void setRawResult(Object value);

	/**
	 * Returns the value of the '<em><b>Task Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Id</em>' attribute.
	 * @see #setTaskId(long)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_TaskId()
	 * @model required="true"
	 * @generated
	 */
	long getTaskId();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getTaskId <em>Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Id</em>' attribute.
	 * @see #getTaskId()
	 * @generated
	 */
	void setTaskId(long value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Long)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_EndTime()
	 * @model
	 * @generated
	 */
	Long getEndTime();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Long value);

	/**
	 * Returns the value of the '<em><b>Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Host</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Host</em>' attribute.
	 * @see #setHost(String)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_Host()
	 * @model
	 * @generated
	 */
	String getHost();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getHost <em>Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Host</em>' attribute.
	 * @see #getHost()
	 * @generated
	 */
	void setHost(String value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link org.coolsoftware.coolcomponents.ccm.variant.JobStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.JobStatus
	 * @see #setStatus(JobStatus)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getJob_Status()
	 * @model
	 * @generated
	 */
	JobStatus getStatus();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.JobStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(JobStatus value);

} // Job
