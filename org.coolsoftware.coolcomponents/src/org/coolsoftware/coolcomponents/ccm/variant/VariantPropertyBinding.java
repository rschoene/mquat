/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.variant;

import org.coolsoftware.coolcomponents.ccm.structure.Property;

import org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralExpression;

import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding#getProperty <em>Property</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getVariantPropertyBinding()
 * @model
 * @generated
 */
public interface VariantPropertyBinding extends VariableAssignment {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference.
	 * @see #setProperty(Property)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getVariantPropertyBinding_Property()
	 * @model required="true"
	 * @generated
	 */
	Property getProperty();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' reference.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(Property value);

} // VariantPropertyBinding
