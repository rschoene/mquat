/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.coolsoftware.coolcomponents.ccm.variant;

import org.coolsoftware.coolcomponents.ccm.structure.DataType;

import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getDeployedOn <em>Deployed On</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getPropertyBinding <em>Property Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getDataElement()
 * @model
 * @generated
 */
public interface DataElement extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' reference.
	 * @see #setSpecification(DataType)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getDataElement_Specification()
	 * @model required="true"
	 * @generated
	 */
	DataType getSpecification();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getSpecification <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' reference.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(DataType value);

	/**
	 * Returns the value of the '<em><b>Deployed On</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployed On</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployed On</em>' reference.
	 * @see #setDeployedOn(Resource)
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getDataElement_DeployedOn()
	 * @model
	 * @generated
	 */
	Resource getDeployedOn();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.variant.DataElement#getDeployedOn <em>Deployed On</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deployed On</em>' reference.
	 * @see #getDeployedOn()
	 * @generated
	 */
	void setDeployedOn(Resource value);

	/**
	 * Returns the value of the '<em><b>Property Binding</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Binding</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Binding</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getDataElement_PropertyBinding()
	 * @model containment="true"
	 * @generated
	 */
	EList<VariantPropertyBinding> getPropertyBinding();

} // DataElement
