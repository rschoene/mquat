/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.variant.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.variant.Behavior;
import org.coolsoftware.coolcomponents.ccm.variant.Component;
import org.coolsoftware.coolcomponents.ccm.variant.Port;
import org.coolsoftware.coolcomponents.ccm.variant.PortConnector;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPackage;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.nameable.impl.NamedElementImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl#getPorts <em>Ports</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl#getBehavior <em>Behavior</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl#getPortconnectors <em>Portconnectors</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.ComponentImpl#getPropertyBinding <em>Property Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ComponentImpl extends NamedElementImpl implements Component {
	/**
	 * The cached value of the '{@link #getPorts() <em>Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<Port> ports;

	/**
	 * The cached value of the '{@link #getBehavior() <em>Behavior</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBehavior()
	 * @generated
	 * @ordered
	 */
	protected Behavior behavior;

	/**
	 * The cached value of the '{@link #getSpecification() <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected ComponentType specification;

	/**
	 * The cached value of the '{@link #getPortconnectors() <em>Portconnectors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortconnectors()
	 * @generated
	 * @ordered
	 */
	protected EList<PortConnector> portconnectors;

	/**
	 * The cached value of the '{@link #getPropertyBinding() <em>Property Binding</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyBinding()
	 * @generated
	 * @ordered
	 */
	protected EList<VariantPropertyBinding> propertyBinding;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariantPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Port> getPorts() {
		if (ports == null) {
			ports = new EObjectContainmentEList<Port>(Port.class, this, VariantPackage.COMPONENT__PORTS);
		}
		return ports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Behavior getBehavior() {
		return behavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBehavior(Behavior newBehavior, NotificationChain msgs) {
		Behavior oldBehavior = behavior;
		behavior = newBehavior;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VariantPackage.COMPONENT__BEHAVIOR, oldBehavior, newBehavior);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBehavior(Behavior newBehavior) {
		if (newBehavior != behavior) {
			NotificationChain msgs = null;
			if (behavior != null)
				msgs = ((InternalEObject)behavior).eInverseRemove(this, VariantPackage.BEHAVIOR__COMPONENT, Behavior.class, msgs);
			if (newBehavior != null)
				msgs = ((InternalEObject)newBehavior).eInverseAdd(this, VariantPackage.BEHAVIOR__COMPONENT, Behavior.class, msgs);
			msgs = basicSetBehavior(newBehavior, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.COMPONENT__BEHAVIOR, newBehavior, newBehavior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType getSpecification() {
		if (specification != null && specification.eIsProxy()) {
			InternalEObject oldSpecification = (InternalEObject)specification;
			specification = (ComponentType)eResolveProxy(oldSpecification);
			if (specification != oldSpecification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariantPackage.COMPONENT__SPECIFICATION, oldSpecification, specification));
			}
		}
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType basicGetSpecification() {
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(ComponentType newSpecification) {
		ComponentType oldSpecification = specification;
		specification = newSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.COMPONENT__SPECIFICATION, oldSpecification, specification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortConnector> getPortconnectors() {
		if (portconnectors == null) {
			portconnectors = new EObjectContainmentEList<PortConnector>(PortConnector.class, this, VariantPackage.COMPONENT__PORTCONNECTORS);
		}
		return portconnectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariantPropertyBinding> getPropertyBinding() {
		if (propertyBinding == null) {
			propertyBinding = new EObjectContainmentEList<VariantPropertyBinding>(VariantPropertyBinding.class, this, VariantPackage.COMPONENT__PROPERTY_BINDING);
		}
		return propertyBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariantPackage.COMPONENT__BEHAVIOR:
				if (behavior != null)
					msgs = ((InternalEObject)behavior).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VariantPackage.COMPONENT__BEHAVIOR, null, msgs);
				return basicSetBehavior((Behavior)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariantPackage.COMPONENT__PORTS:
				return ((InternalEList<?>)getPorts()).basicRemove(otherEnd, msgs);
			case VariantPackage.COMPONENT__BEHAVIOR:
				return basicSetBehavior(null, msgs);
			case VariantPackage.COMPONENT__PORTCONNECTORS:
				return ((InternalEList<?>)getPortconnectors()).basicRemove(otherEnd, msgs);
			case VariantPackage.COMPONENT__PROPERTY_BINDING:
				return ((InternalEList<?>)getPropertyBinding()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariantPackage.COMPONENT__PORTS:
				return getPorts();
			case VariantPackage.COMPONENT__BEHAVIOR:
				return getBehavior();
			case VariantPackage.COMPONENT__SPECIFICATION:
				if (resolve) return getSpecification();
				return basicGetSpecification();
			case VariantPackage.COMPONENT__PORTCONNECTORS:
				return getPortconnectors();
			case VariantPackage.COMPONENT__PROPERTY_BINDING:
				return getPropertyBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariantPackage.COMPONENT__PORTS:
				getPorts().clear();
				getPorts().addAll((Collection<? extends Port>)newValue);
				return;
			case VariantPackage.COMPONENT__BEHAVIOR:
				setBehavior((Behavior)newValue);
				return;
			case VariantPackage.COMPONENT__SPECIFICATION:
				setSpecification((ComponentType)newValue);
				return;
			case VariantPackage.COMPONENT__PORTCONNECTORS:
				getPortconnectors().clear();
				getPortconnectors().addAll((Collection<? extends PortConnector>)newValue);
				return;
			case VariantPackage.COMPONENT__PROPERTY_BINDING:
				getPropertyBinding().clear();
				getPropertyBinding().addAll((Collection<? extends VariantPropertyBinding>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariantPackage.COMPONENT__PORTS:
				getPorts().clear();
				return;
			case VariantPackage.COMPONENT__BEHAVIOR:
				setBehavior((Behavior)null);
				return;
			case VariantPackage.COMPONENT__SPECIFICATION:
				setSpecification((ComponentType)null);
				return;
			case VariantPackage.COMPONENT__PORTCONNECTORS:
				getPortconnectors().clear();
				return;
			case VariantPackage.COMPONENT__PROPERTY_BINDING:
				getPropertyBinding().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariantPackage.COMPONENT__PORTS:
				return ports != null && !ports.isEmpty();
			case VariantPackage.COMPONENT__BEHAVIOR:
				return behavior != null;
			case VariantPackage.COMPONENT__SPECIFICATION:
				return specification != null;
			case VariantPackage.COMPONENT__PORTCONNECTORS:
				return portconnectors != null && !portconnectors.isEmpty();
			case VariantPackage.COMPONENT__PROPERTY_BINDING:
				return propertyBinding != null && !propertyBinding.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComponentImpl
