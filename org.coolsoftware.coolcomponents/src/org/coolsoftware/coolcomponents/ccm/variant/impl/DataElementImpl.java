/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.coolsoftware.coolcomponents.ccm.variant.impl;

import java.util.Collection;
import org.coolsoftware.coolcomponents.ccm.structure.DataType;

import org.coolsoftware.coolcomponents.ccm.variant.DataElement;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPackage;

import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.nameable.impl.NamedElementImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.DataElementImpl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.DataElementImpl#getDeployedOn <em>Deployed On</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.DataElementImpl#getPropertyBinding <em>Property Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DataElementImpl extends NamedElementImpl implements DataElement {
	/**
	 * The cached value of the '{@link #getSpecification() <em>Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecification()
	 * @generated
	 * @ordered
	 */
	protected DataType specification;

	/**
	 * The cached value of the '{@link #getDeployedOn() <em>Deployed On</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeployedOn()
	 * @generated
	 * @ordered
	 */
	protected Resource deployedOn;

	/**
	 * The cached value of the '{@link #getPropertyBinding() <em>Property Binding</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyBinding()
	 * @generated
	 * @ordered
	 */
	protected EList<VariantPropertyBinding> propertyBinding;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariantPackage.Literals.DATA_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType getSpecification() {
		if (specification != null && specification.eIsProxy()) {
			InternalEObject oldSpecification = (InternalEObject)specification;
			specification = (DataType)eResolveProxy(oldSpecification);
			if (specification != oldSpecification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariantPackage.DATA_ELEMENT__SPECIFICATION, oldSpecification, specification));
			}
		}
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType basicGetSpecification() {
		return specification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(DataType newSpecification) {
		DataType oldSpecification = specification;
		specification = newSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.DATA_ELEMENT__SPECIFICATION, oldSpecification, specification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getDeployedOn() {
		if (deployedOn != null && deployedOn.eIsProxy()) {
			InternalEObject oldDeployedOn = (InternalEObject)deployedOn;
			deployedOn = (Resource)eResolveProxy(oldDeployedOn);
			if (deployedOn != oldDeployedOn) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariantPackage.DATA_ELEMENT__DEPLOYED_ON, oldDeployedOn, deployedOn));
			}
		}
		return deployedOn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource basicGetDeployedOn() {
		return deployedOn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeployedOn(Resource newDeployedOn) {
		Resource oldDeployedOn = deployedOn;
		deployedOn = newDeployedOn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.DATA_ELEMENT__DEPLOYED_ON, oldDeployedOn, deployedOn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariantPropertyBinding> getPropertyBinding() {
		if (propertyBinding == null) {
			propertyBinding = new EObjectContainmentEList<VariantPropertyBinding>(VariantPropertyBinding.class, this, VariantPackage.DATA_ELEMENT__PROPERTY_BINDING);
		}
		return propertyBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariantPackage.DATA_ELEMENT__PROPERTY_BINDING:
				return ((InternalEList<?>)getPropertyBinding()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariantPackage.DATA_ELEMENT__SPECIFICATION:
				if (resolve) return getSpecification();
				return basicGetSpecification();
			case VariantPackage.DATA_ELEMENT__DEPLOYED_ON:
				if (resolve) return getDeployedOn();
				return basicGetDeployedOn();
			case VariantPackage.DATA_ELEMENT__PROPERTY_BINDING:
				return getPropertyBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariantPackage.DATA_ELEMENT__SPECIFICATION:
				setSpecification((DataType)newValue);
				return;
			case VariantPackage.DATA_ELEMENT__DEPLOYED_ON:
				setDeployedOn((Resource)newValue);
				return;
			case VariantPackage.DATA_ELEMENT__PROPERTY_BINDING:
				getPropertyBinding().clear();
				getPropertyBinding().addAll((Collection<? extends VariantPropertyBinding>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariantPackage.DATA_ELEMENT__SPECIFICATION:
				setSpecification((DataType)null);
				return;
			case VariantPackage.DATA_ELEMENT__DEPLOYED_ON:
				setDeployedOn((Resource)null);
				return;
			case VariantPackage.DATA_ELEMENT__PROPERTY_BINDING:
				getPropertyBinding().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariantPackage.DATA_ELEMENT__SPECIFICATION:
				return specification != null;
			case VariantPackage.DATA_ELEMENT__DEPLOYED_ON:
				return deployedOn != null;
			case VariantPackage.DATA_ELEMENT__PROPERTY_BINDING:
				return propertyBinding != null && !propertyBinding.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataElementImpl
