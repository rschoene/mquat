/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.coolsoftware.coolcomponents.ccm.variant.impl;

import org.coolsoftware.coolcomponents.ccm.variant.Job;
import org.coolsoftware.coolcomponents.ccm.variant.JobStatus;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Job</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getImpl <em>Impl</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getPredictedRuntime <em>Predicted Runtime</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getJobId <em>Job Id</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getRawResult <em>Raw Result</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getTaskId <em>Task Id</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getHost <em>Host</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.impl.JobImpl#getStatus <em>Status</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class JobImpl extends EObjectImpl implements Job {
	/**
	 * The cached value of the '{@link #getImpl() <em>Impl</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImpl()
	 * @generated
	 * @ordered
	 */
	protected SWComponent impl;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final Long START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected Long startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPredictedRuntime() <em>Predicted Runtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredictedRuntime()
	 * @generated
	 * @ordered
	 */
	protected static final double PREDICTED_RUNTIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPredictedRuntime() <em>Predicted Runtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredictedRuntime()
	 * @generated
	 * @ordered
	 */
	protected double predictedRuntime = PREDICTED_RUNTIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getJobId() <em>Job Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJobId()
	 * @generated
	 * @ordered
	 */
	protected static final long JOB_ID_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getJobId() <em>Job Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJobId()
	 * @generated
	 * @ordered
	 */
	protected long jobId = JOB_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getRawResult() <em>Raw Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRawResult()
	 * @generated
	 * @ordered
	 */
	protected static final Object RAW_RESULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRawResult() <em>Raw Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRawResult()
	 * @generated
	 * @ordered
	 */
	protected Object rawResult = RAW_RESULT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTaskId() <em>Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskId()
	 * @generated
	 * @ordered
	 */
	protected static final long TASK_ID_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getTaskId() <em>Task Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskId()
	 * @generated
	 * @ordered
	 */
	protected long taskId = TASK_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final Long END_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected Long endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getHost() <em>Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHost()
	 * @generated
	 * @ordered
	 */
	protected static final String HOST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHost() <em>Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHost()
	 * @generated
	 * @ordered
	 */
	protected String host = HOST_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final JobStatus STATUS_EDEFAULT = JobStatus.CREATED;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected JobStatus status = STATUS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JobImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariantPackage.Literals.JOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponent getImpl() {
		if (impl != null && impl.eIsProxy()) {
			InternalEObject oldImpl = (InternalEObject)impl;
			impl = (SWComponent)eResolveProxy(oldImpl);
			if (impl != oldImpl) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariantPackage.JOB__IMPL, oldImpl, impl));
			}
		}
		return impl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponent basicGetImpl() {
		return impl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImpl(SWComponent newImpl, NotificationChain msgs) {
		SWComponent oldImpl = impl;
		impl = newImpl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__IMPL, oldImpl, newImpl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImpl(SWComponent newImpl) {
		if (newImpl != impl) {
			NotificationChain msgs = null;
			if (impl != null)
				msgs = ((InternalEObject)impl).eInverseRemove(this, VariantPackage.SW_COMPONENT__JOBS, SWComponent.class, msgs);
			if (newImpl != null)
				msgs = ((InternalEObject)newImpl).eInverseAdd(this, VariantPackage.SW_COMPONENT__JOBS, SWComponent.class, msgs);
			msgs = basicSetImpl(newImpl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__IMPL, newImpl, newImpl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Long newStartTime) {
		Long oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__START_TIME, oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPredictedRuntime() {
		return predictedRuntime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredictedRuntime(double newPredictedRuntime) {
		double oldPredictedRuntime = predictedRuntime;
		predictedRuntime = newPredictedRuntime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__PREDICTED_RUNTIME, oldPredictedRuntime, predictedRuntime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getJobId() {
		return jobId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJobId(long newJobId) {
		long oldJobId = jobId;
		jobId = newJobId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__JOB_ID, oldJobId, jobId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getRawResult() {
		return rawResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRawResult(Object newRawResult) {
		Object oldRawResult = rawResult;
		rawResult = newRawResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__RAW_RESULT, oldRawResult, rawResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getTaskId() {
		return taskId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskId(long newTaskId) {
		long oldTaskId = taskId;
		taskId = newTaskId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__TASK_ID, oldTaskId, taskId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(Long newEndTime) {
		Long oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__END_TIME, oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHost() {
		return host;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHost(String newHost) {
		String oldHost = host;
		host = newHost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__HOST, oldHost, host));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JobStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(JobStatus newStatus) {
		JobStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariantPackage.JOB__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariantPackage.JOB__IMPL:
				if (impl != null)
					msgs = ((InternalEObject)impl).eInverseRemove(this, VariantPackage.SW_COMPONENT__JOBS, SWComponent.class, msgs);
				return basicSetImpl((SWComponent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariantPackage.JOB__IMPL:
				return basicSetImpl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariantPackage.JOB__IMPL:
				if (resolve) return getImpl();
				return basicGetImpl();
			case VariantPackage.JOB__START_TIME:
				return getStartTime();
			case VariantPackage.JOB__PREDICTED_RUNTIME:
				return getPredictedRuntime();
			case VariantPackage.JOB__JOB_ID:
				return getJobId();
			case VariantPackage.JOB__RAW_RESULT:
				return getRawResult();
			case VariantPackage.JOB__TASK_ID:
				return getTaskId();
			case VariantPackage.JOB__END_TIME:
				return getEndTime();
			case VariantPackage.JOB__HOST:
				return getHost();
			case VariantPackage.JOB__STATUS:
				return getStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariantPackage.JOB__IMPL:
				setImpl((SWComponent)newValue);
				return;
			case VariantPackage.JOB__START_TIME:
				setStartTime((Long)newValue);
				return;
			case VariantPackage.JOB__PREDICTED_RUNTIME:
				setPredictedRuntime((Double)newValue);
				return;
			case VariantPackage.JOB__JOB_ID:
				setJobId((Long)newValue);
				return;
			case VariantPackage.JOB__RAW_RESULT:
				setRawResult(newValue);
				return;
			case VariantPackage.JOB__TASK_ID:
				setTaskId((Long)newValue);
				return;
			case VariantPackage.JOB__END_TIME:
				setEndTime((Long)newValue);
				return;
			case VariantPackage.JOB__HOST:
				setHost((String)newValue);
				return;
			case VariantPackage.JOB__STATUS:
				setStatus((JobStatus)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariantPackage.JOB__IMPL:
				setImpl((SWComponent)null);
				return;
			case VariantPackage.JOB__START_TIME:
				setStartTime(START_TIME_EDEFAULT);
				return;
			case VariantPackage.JOB__PREDICTED_RUNTIME:
				setPredictedRuntime(PREDICTED_RUNTIME_EDEFAULT);
				return;
			case VariantPackage.JOB__JOB_ID:
				setJobId(JOB_ID_EDEFAULT);
				return;
			case VariantPackage.JOB__RAW_RESULT:
				setRawResult(RAW_RESULT_EDEFAULT);
				return;
			case VariantPackage.JOB__TASK_ID:
				setTaskId(TASK_ID_EDEFAULT);
				return;
			case VariantPackage.JOB__END_TIME:
				setEndTime(END_TIME_EDEFAULT);
				return;
			case VariantPackage.JOB__HOST:
				setHost(HOST_EDEFAULT);
				return;
			case VariantPackage.JOB__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariantPackage.JOB__IMPL:
				return impl != null;
			case VariantPackage.JOB__START_TIME:
				return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
			case VariantPackage.JOB__PREDICTED_RUNTIME:
				return predictedRuntime != PREDICTED_RUNTIME_EDEFAULT;
			case VariantPackage.JOB__JOB_ID:
				return jobId != JOB_ID_EDEFAULT;
			case VariantPackage.JOB__RAW_RESULT:
				return RAW_RESULT_EDEFAULT == null ? rawResult != null : !RAW_RESULT_EDEFAULT.equals(rawResult);
			case VariantPackage.JOB__TASK_ID:
				return taskId != TASK_ID_EDEFAULT;
			case VariantPackage.JOB__END_TIME:
				return END_TIME_EDEFAULT == null ? endTime != null : !END_TIME_EDEFAULT.equals(endTime);
			case VariantPackage.JOB__HOST:
				return HOST_EDEFAULT == null ? host != null : !HOST_EDEFAULT.equals(host);
			case VariantPackage.JOB__STATUS:
				return status != STATUS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (startTime: ");
		result.append(startTime);
		result.append(", predictedRuntime: ");
		result.append(predictedRuntime);
		result.append(", jobId: ");
		result.append(jobId);
		result.append(", rawResult: ");
		result.append(rawResult);
		result.append(", taskId: ");
		result.append(taskId);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", host: ");
		result.append(host);
		result.append(", status: ");
		result.append(status);
		result.append(')');
		return result.toString();
	}

} //JobImpl
