/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.variant;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SW Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getSubtypes <em>Subtypes</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent#getJobs <em>Jobs</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getSWComponent()
 * @model
 * @generated
 */
public interface SWComponent extends Component {
	/**
	 * Returns the value of the '<em><b>Subtypes</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtypes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtypes</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getSWComponent_Subtypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<SWComponent> getSubtypes();

	/**
	 * Returns the value of the '<em><b>Jobs</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.Job}.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.variant.Job#getImpl <em>Impl</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Jobs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jobs</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.variant.VariantPackage#getSWComponent_Jobs()
	 * @see org.coolsoftware.coolcomponents.ccm.variant.Job#getImpl
	 * @model opposite="impl"
	 * @generated
	 */
	EList<Job> getJobs();

} // SWComponent
