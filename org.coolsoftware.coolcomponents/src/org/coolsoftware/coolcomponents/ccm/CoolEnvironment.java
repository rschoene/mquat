/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm;

import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.UserType;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;
import org.coolsoftware.coolcomponents.ccm.variant.User;
import org.coolsoftware.coolcomponents.nameable.DescribableElement;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cool Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getSoftwareTypeSystem <em>Software Type System</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getResourceTypeSystem <em>Resource Type System</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getUserTypeSystem <em>User Type System</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getComponents <em>Components</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getResourcemodel <em>Resourcemodel</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getUsers <em>Users</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#getCoolEnvironment()
 * @model
 * @generated
 */
public interface CoolEnvironment extends DescribableElement {
	/**
	 * Returns the value of the '<em><b>Software Type System</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.SWComponentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Type System</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Type System</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#getCoolEnvironment_SoftwareTypeSystem()
	 * @model
	 * @generated
	 */
	EList<SWComponentType> getSoftwareTypeSystem();

	/**
	 * Returns the value of the '<em><b>Resource Type System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Type System</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Type System</em>' reference.
	 * @see #setResourceTypeSystem(ResourceType)
	 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#getCoolEnvironment_ResourceTypeSystem()
	 * @model
	 * @generated
	 */
	ResourceType getResourceTypeSystem();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.CoolEnvironment#getResourceTypeSystem <em>Resource Type System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Type System</em>' reference.
	 * @see #getResourceTypeSystem()
	 * @generated
	 */
	void setResourceTypeSystem(ResourceType value);

	/**
	 * Returns the value of the '<em><b>User Type System</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.structure.UserType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Type System</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Type System</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#getCoolEnvironment_UserTypeSystem()
	 * @model
	 * @generated
	 */
	EList<UserType> getUserTypeSystem();

	/**
	 * Returns the value of the '<em><b>Components</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.SWComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Components</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#getCoolEnvironment_Components()
	 * @model
	 * @generated
	 */
	EList<SWComponent> getComponents();

	/**
	 * Returns the value of the '<em><b>Resourcemodel</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resourcemodel</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resourcemodel</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#getCoolEnvironment_Resourcemodel()
	 * @model
	 * @generated
	 */
	EList<Resource> getResourcemodel();

	/**
	 * Returns the value of the '<em><b>Users</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.variant.User}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Users</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Users</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#getCoolEnvironment_Users()
	 * @model
	 * @generated
	 */
	EList<User> getUsers();

} // CoolEnvironment
