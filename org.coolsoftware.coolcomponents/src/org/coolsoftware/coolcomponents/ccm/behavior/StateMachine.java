/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.behavior;

import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getStates <em>States</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getInitialState <em>Initial State</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getCurrentState <em>Current State</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getTimer <em>Timer</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getPower <em>Power</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getVariables <em>Variables</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getStepInfos <em>Step Infos</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine()
 * @model annotation="gmfB.diagram diagram='BehaviorTemplate' model.extension='behavior'"
 * @generated
 */
public interface StateMachine extends ComposedBehavior {
	
	
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.behavior.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_States()
	 * @model containment="true"
	 *        annotation="gmfB.compartment comp='states'"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.behavior.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial State</em>' reference.
	 * @see #setInitialState(State)
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_InitialState()
	 * @model required="true"
	 * @generated
	 */
	State getInitialState();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getInitialState <em>Initial State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial State</em>' reference.
	 * @see #getInitialState()
	 * @generated
	 */
	void setInitialState(State value);

	/**
	 * Returns the value of the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current State</em>' reference.
	 * @see #setCurrentState(State)
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_CurrentState()
	 * @model transient="true"
	 * @generated
	 */
	State getCurrentState();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getCurrentState <em>Current State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current State</em>' reference.
	 * @see #getCurrentState()
	 * @generated
	 */
	void setCurrentState(State value);

	/**
	 * Returns the value of the '<em><b>Timer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timer</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timer</em>' containment reference.
	 * @see #setTimer(VariableDeclaration)
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_Timer()
	 * @model containment="true" required="true"
	 * @generated
	 */
	VariableDeclaration getTimer();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getTimer <em>Timer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timer</em>' containment reference.
	 * @see #getTimer()
	 * @generated
	 */
	void setTimer(VariableDeclaration value);

	/**
	 * Returns the value of the '<em><b>Power</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Power</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Power</em>' containment reference.
	 * @see #setPower(VariableDeclaration)
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_Power()
	 * @model containment="true" required="true"
	 * @generated
	 */
	VariableDeclaration getPower();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getPower <em>Power</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Power</em>' containment reference.
	 * @see #getPower()
	 * @generated
	 */
	void setPower(VariableDeclaration value);

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_Variables()
	 * @model containment="true"
	 *        annotation="gmfB.compartment x='y'"
	 * @generated
	 */
	EList<VariableDeclaration> getVariables();
	
	/**
	 * Returns the value of the '<em><b>Step Infos</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.behavior.StepInfo}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Infos</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Infos</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getStateMachine_StepInfos()
	 * @model containment="true"
	 * @generated
	 */
	EList<StepInfo> getStepInfos();
	
	public boolean addStepInfo(StepInfo si);

	public void updateVariable(org.coolsoftware.coolcomponents.expressions.variables.Variable var);

	/**
	 * @generated NOT
	 */
	void reset();

} // StateMachine
