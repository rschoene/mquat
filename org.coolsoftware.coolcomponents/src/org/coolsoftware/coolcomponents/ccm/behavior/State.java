/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.behavior;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.State#getFanout <em>Fanout</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getState()
 * @model annotation="gmfB.node label='name' border.color='0,0,0'"
 * @generated
 */
public interface State extends StmStructuralElement {
	/**
	 * Returns the value of the '<em><b>Fanout</b></em>' reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.behavior.Transition}.
	 * It is bidirectional and its opposite is '{@link org.coolsoftware.coolcomponents.ccm.behavior.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fanout</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fanout</em>' reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getState_Fanout()
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Transition#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Transition> getFanout();

} // State
