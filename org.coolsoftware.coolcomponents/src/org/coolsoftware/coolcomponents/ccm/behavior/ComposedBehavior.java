/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.behavior;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composed Behavior</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior#getSubBehavior <em>Sub Behavior</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior#getConnectors <em>Connectors</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getComposedBehavior()
 * @model
 * @generated
 */
public interface ComposedBehavior extends BehaviorTemplate {
	/**
	 * Returns the value of the '<em><b>Sub Behavior</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Behavior</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Behavior</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getComposedBehavior_SubBehavior()
	 * @model containment="true"
	 * @generated
	 */
	EList<BehaviorTemplate> getSubBehavior();

	/**
	 * Returns the value of the '<em><b>Connectors</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.ccm.behavior.PinConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connectors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connectors</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getComposedBehavior_Connectors()
	 * @model containment="true"
	 * @generated
	 */
	EList<PinConnector> getConnectors();

} // ComposedBehavior
