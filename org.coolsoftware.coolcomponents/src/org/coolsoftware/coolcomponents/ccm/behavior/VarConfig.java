/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.behavior;

import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Var Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * describes the current variable configuration for a single execution step
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarName <em>Var Name</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarValue <em>Var Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getVarConfig()
 * @model
 * @generated
 */
public interface VarConfig extends EObject {
	/**
	 * Returns the value of the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Name</em>' attribute.
	 * @see #setVarName(String)
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getVarConfig_VarName()
	 * @model required="true"
	 * @generated
	 */
	String getVarName();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarName <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Name</em>' attribute.
	 * @see #getVarName()
	 * @generated
	 */
	void setVarName(String value);

	/**
	 * Returns the value of the '<em><b>Var Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Value</em>' reference.
	 * @see #setVarValue(CcmValue)
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage#getVarConfig_VarValue()
	 * @model required="true"
	 * @generated
	 */
	CcmValue getVarValue();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarValue <em>Var Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Value</em>' reference.
	 * @see #getVarValue()
	 * @generated
	 */
	void setVarValue(CcmValue value);

} // VarConfig
