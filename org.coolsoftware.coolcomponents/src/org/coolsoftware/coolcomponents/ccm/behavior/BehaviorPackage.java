/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.behavior;

import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorFactory
 * @model kind="package"
 *        annotation="gmfB model='BehaviorModel'"
 * @generated
 */
public interface BehaviorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "behavior";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://cool-software.org/ccm/1.0/behavior";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "behavior";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BehaviorPackage eINSTANCE = org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorModelImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getBehaviorModel()
	 * @generated
	 */
	int BEHAVIOR_MODEL = 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorTemplateImpl <em>Template</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorTemplateImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getBehaviorTemplate()
	 * @generated
	 */
	int BEHAVIOR_TEMPLATE = 2;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.ComposedBehaviorImpl <em>Composed Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.ComposedBehaviorImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getComposedBehavior()
	 * @generated
	 */
	int COMPOSED_BEHAVIOR = 3;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.CostParameterImpl <em>Cost Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.CostParameterImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getCostParameter()
	 * @generated
	 */
	int COST_PARAMETER = 4;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.NavigatingElementImpl <em>Navigating Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.NavigatingElementImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getNavigatingElement()
	 * @generated
	 */
	int NAVIGATING_ELEMENT = 7;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.EventImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 5;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.GuardImpl <em>Guard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.GuardImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getGuard()
	 * @generated
	 */
	int GUARD = 6;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.OccurrenceImpl <em>Occurrence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.OccurrenceImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getOccurrence()
	 * @generated
	 */
	int OCCURRENCE = 8;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.PinImpl <em>Pin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.PinImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getPin()
	 * @generated
	 */
	int PIN = 9;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.PinConnectorImpl <em>Pin Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.PinConnectorImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getPinConnector()
	 * @generated
	 */
	int PIN_CONNECTOR = 10;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StmStructuralElementImpl <em>Stm Structural Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StmStructuralElementImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getStmStructuralElement()
	 * @generated
	 */
	int STM_STRUCTURAL_ELEMENT = 13;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StateImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getState()
	 * @generated
	 */
	int STATE = 11;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl <em>State Machine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getStateMachine()
	 * @generated
	 */
	int STATE_MACHINE = 12;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.TransitionImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 14;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.WorkloadImpl <em>Workload</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.WorkloadImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getWorkload()
	 * @generated
	 */
	int WORKLOAD = 15;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StepInfoImpl <em>Step Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StepInfoImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getStepInfo()
	 * @generated
	 */
	int STEP_INFO = 16;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.VarConfigImpl <em>Var Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.VarConfigImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getVarConfig()
	 * @generated
	 */
	int VAR_CONFIG = 17;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.ActionImpl
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 0;

	/**
	 * The feature id for the '<em><b>Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__STATEMENT = 0;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_MODEL__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Templates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_MODEL__TEMPLATES = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_MODEL_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TEMPLATE__NAME = NameablePackage.DESCRIBABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TEMPLATE__DESCRIPTION = NameablePackage.DESCRIBABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Pins</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TEMPLATE__PINS = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TEMPLATE__PARAMETER = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_TEMPLATE_FEATURE_COUNT = NameablePackage.DESCRIBABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_BEHAVIOR__NAME = BEHAVIOR_TEMPLATE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_BEHAVIOR__DESCRIPTION = BEHAVIOR_TEMPLATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Pins</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_BEHAVIOR__PINS = BEHAVIOR_TEMPLATE__PINS;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_BEHAVIOR__PARAMETER = BEHAVIOR_TEMPLATE__PARAMETER;

	/**
	 * The feature id for the '<em><b>Sub Behavior</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_BEHAVIOR__SUB_BEHAVIOR = BEHAVIOR_TEMPLATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_BEHAVIOR__CONNECTORS = BEHAVIOR_TEMPLATE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composed Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_BEHAVIOR_FEATURE_COUNT = BEHAVIOR_TEMPLATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COST_PARAMETER__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COST_PARAMETER__UNIT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cost Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COST_PARAMETER_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Pin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATING_ELEMENT__PIN = 0;

	/**
	 * The number of structural features of the '<em>Navigating Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAVIGATING_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Pin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PIN = NAVIGATING_ELEMENT__PIN;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = NAVIGATING_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Boolean Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD__BOOLEAN_EXPRESSION = 0;

	/**
	 * The number of structural features of the '<em>Guard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUARD_FEATURE_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Pin</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCURRENCE__PIN = 0;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCURRENCE__TIME = 1;

	/**
	 * The feature id for the '<em><b>Load</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCURRENCE__LOAD = 2;

	/**
	 * The number of structural features of the '<em>Occurrence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCCURRENCE_FEATURE_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__DIRECTION = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__PARAMETER = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Visibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN__VISIBILITY = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Pin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_CONNECTOR__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_CONNECTOR__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Pin Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIN_CONNECTOR_FEATURE_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STM_STRUCTURAL_ELEMENT__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STM_STRUCTURAL_ELEMENT__PARAMETER = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STM_STRUCTURAL_ELEMENT__ACTION = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Stm Structural Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STM_STRUCTURAL_ELEMENT_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = STM_STRUCTURAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__PARAMETER = STM_STRUCTURAL_ELEMENT__PARAMETER;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ACTION = STM_STRUCTURAL_ELEMENT__ACTION;

	/**
	 * The feature id for the '<em><b>Fanout</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__FANOUT = STM_STRUCTURAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = STM_STRUCTURAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__NAME = COMPOSED_BEHAVIOR__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__DESCRIPTION = COMPOSED_BEHAVIOR__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Pins</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__PINS = COMPOSED_BEHAVIOR__PINS;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__PARAMETER = COMPOSED_BEHAVIOR__PARAMETER;

	/**
	 * The feature id for the '<em><b>Sub Behavior</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__SUB_BEHAVIOR = COMPOSED_BEHAVIOR__SUB_BEHAVIOR;

	/**
	 * The feature id for the '<em><b>Connectors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__CONNECTORS = COMPOSED_BEHAVIOR__CONNECTORS;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__STATES = COMPOSED_BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TRANSITIONS = COMPOSED_BEHAVIOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__INITIAL_STATE = COMPOSED_BEHAVIOR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__CURRENT_STATE = COMPOSED_BEHAVIOR_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Timer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TIMER = COMPOSED_BEHAVIOR_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Power</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__POWER = COMPOSED_BEHAVIOR_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__VARIABLES = COMPOSED_BEHAVIOR_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Step Infos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__STEP_INFOS = COMPOSED_BEHAVIOR_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>State Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_FEATURE_COUNT = COMPOSED_BEHAVIOR_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = STM_STRUCTURAL_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__PARAMETER = STM_STRUCTURAL_ELEMENT__PARAMETER;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ACTION = STM_STRUCTURAL_ELEMENT__ACTION;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TARGET = STM_STRUCTURAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE = STM_STRUCTURAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EVENT = STM_STRUCTURAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__GUARD = STM_STRUCTURAL_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = STM_STRUCTURAL_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKLOAD__NAME = NameablePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Items</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKLOAD__ITEMS = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKLOAD__RESOURCE = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Workload</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKLOAD_FEATURE_COUNT = NameablePackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_INFO__STATE = 0;

	/**
	 * The feature id for the '<em><b>Config</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_INFO__CONFIG = 1;

	/**
	 * The feature id for the '<em><b>Clock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_INFO__CLOCK = 2;

	/**
	 * The feature id for the '<em><b>Power</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_INFO__POWER = 3;

	/**
	 * The number of structural features of the '<em>Step Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_INFO_FEATURE_COUNT = 4;

	/**
	 * The feature id for the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_CONFIG__VAR_NAME = 0;

	/**
	 * The feature id for the '<em><b>Var Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_CONFIG__VAR_VALUE = 1;

	/**
	 * The number of structural features of the '<em>Var Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VAR_CONFIG_FEATURE_COUNT = 2;

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.BehaviorModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorModel
	 * @generated
	 */
	EClass getBehaviorModel();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.BehaviorModel#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Templates</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorModel#getTemplates()
	 * @see #getBehaviorModel()
	 * @generated
	 */
	EReference getBehaviorModel_Templates();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate
	 * @generated
	 */
	EClass getBehaviorTemplate();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate#getPins <em>Pins</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pins</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate#getPins()
	 * @see #getBehaviorTemplate()
	 * @generated
	 */
	EReference getBehaviorTemplate_Pins();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate#getParameter()
	 * @see #getBehaviorTemplate()
	 * @generated
	 */
	EReference getBehaviorTemplate_Parameter();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior <em>Composed Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composed Behavior</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior
	 * @generated
	 */
	EClass getComposedBehavior();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior#getSubBehavior <em>Sub Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Behavior</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior#getSubBehavior()
	 * @see #getComposedBehavior()
	 * @generated
	 */
	EReference getComposedBehavior_SubBehavior();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior#getConnectors <em>Connectors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Connectors</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior#getConnectors()
	 * @see #getComposedBehavior()
	 * @generated
	 */
	EReference getComposedBehavior_Connectors();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.CostParameter <em>Cost Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cost Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.CostParameter
	 * @generated
	 */
	EClass getCostParameter();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.CostParameter#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.CostParameter#getUnit()
	 * @see #getCostParameter()
	 * @generated
	 */
	EReference getCostParameter_Unit();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.Guard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guard</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Guard
	 * @generated
	 */
	EClass getGuard();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Guard#getBooleanExpression <em>Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Boolean Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Guard#getBooleanExpression()
	 * @see #getGuard()
	 * @generated
	 */
	EReference getGuard_BooleanExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.NavigatingElement <em>Navigating Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Navigating Element</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.NavigatingElement
	 * @generated
	 */
	EClass getNavigatingElement();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.NavigatingElement#getPin <em>Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Pin</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.NavigatingElement#getPin()
	 * @see #getNavigatingElement()
	 * @generated
	 */
	EReference getNavigatingElement_Pin();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.Occurrence <em>Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Occurrence</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Occurrence
	 * @generated
	 */
	EClass getOccurrence();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Occurrence#getPin <em>Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Pin</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Occurrence#getPin()
	 * @see #getOccurrence()
	 * @generated
	 */
	EReference getOccurrence_Pin();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.behavior.Occurrence#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Occurrence#getTime()
	 * @see #getOccurrence()
	 * @generated
	 */
	EAttribute getOccurrence_Time();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.behavior.Occurrence#getLoad <em>Load</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Occurrence#getLoad()
	 * @see #getOccurrence()
	 * @generated
	 */
	EAttribute getOccurrence_Load();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.Pin <em>Pin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Pin
	 * @generated
	 */
	EClass getPin();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.behavior.Pin#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Pin#getDirection()
	 * @see #getPin()
	 * @generated
	 */
	EAttribute getPin_Direction();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.Pin#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Pin#getParameter()
	 * @see #getPin()
	 * @generated
	 */
	EReference getPin_Parameter();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.behavior.Pin#getVisibility <em>Visibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Visibility</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Pin#getVisibility()
	 * @see #getPin()
	 * @generated
	 */
	EAttribute getPin_Visibility();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.PinConnector <em>Pin Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pin Connector</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.PinConnector
	 * @generated
	 */
	EClass getPinConnector();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.PinConnector#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.PinConnector#getSource()
	 * @see #getPinConnector()
	 * @generated
	 */
	EReference getPinConnector_Source();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.PinConnector#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.PinConnector#getTarget()
	 * @see #getPinConnector()
	 * @generated
	 */
	EReference getPinConnector_Target();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.State#getFanout <em>Fanout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Fanout</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.State#getFanout()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Fanout();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine
	 * @generated
	 */
	EClass getStateMachine();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getStates()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_States();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getTransitions()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Transitions();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getInitialState <em>Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial State</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getInitialState()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_InitialState();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getCurrentState <em>Current State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current State</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getCurrentState()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_CurrentState();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getTimer <em>Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timer</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getTimer()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Timer();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getPower <em>Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Power</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getPower()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Power();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getVariables()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Variables();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getStepInfos <em>Step Infos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Step Infos</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StateMachine#getStepInfos()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_StepInfos();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.StmStructuralElement <em>Stm Structural Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stm Structural Element</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StmStructuralElement
	 * @generated
	 */
	EClass getStmStructuralElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.StmStructuralElement#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StmStructuralElement#getParameter()
	 * @see #getStmStructuralElement()
	 * @generated
	 */
	EReference getStmStructuralElement_Parameter();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.StmStructuralElement#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StmStructuralElement#getAction()
	 * @see #getStmStructuralElement()
	 * @generated
	 */
	EReference getStmStructuralElement_Action();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Transition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Transition#getTarget()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Target();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Transition#getSource()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Source();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Transition#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Transition#getEvent()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Event();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Transition#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guard</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Transition#getGuard()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Guard();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.Workload <em>Workload</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Workload</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Workload
	 * @generated
	 */
	EClass getWorkload();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.Workload#getItems <em>Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Items</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Workload#getItems()
	 * @see #getWorkload()
	 * @generated
	 */
	EReference getWorkload_Items();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Workload#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Workload#getResource()
	 * @see #getWorkload()
	 * @generated
	 */
	EReference getWorkload_Resource();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.StepInfo <em>Step Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Step Info</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StepInfo
	 * @generated
	 */
	EClass getStepInfo();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getState()
	 * @see #getStepInfo()
	 * @generated
	 */
	EReference getStepInfo_State();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getConfig <em>Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Config</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getConfig()
	 * @see #getStepInfo()
	 * @generated
	 */
	EReference getStepInfo_Config();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getClock <em>Clock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Clock</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getClock()
	 * @see #getStepInfo()
	 * @generated
	 */
	EAttribute getStepInfo_Clock();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getPower <em>Power</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Power</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.StepInfo#getPower()
	 * @see #getStepInfo()
	 * @generated
	 */
	EAttribute getStepInfo_Power();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.VarConfig <em>Var Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Var Config</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.VarConfig
	 * @generated
	 */
	EClass getVarConfig();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarName <em>Var Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Name</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarName()
	 * @see #getVarConfig()
	 * @generated
	 */
	EAttribute getVarConfig_VarName();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarValue <em>Var Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Var Value</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.VarConfig#getVarValue()
	 * @see #getVarConfig()
	 * @generated
	 */
	EReference getVarConfig_VarValue();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.ccm.behavior.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.ccm.behavior.Action#getStatement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Statement</em>'.
	 * @see org.coolsoftware.coolcomponents.ccm.behavior.Action#getStatement()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Statement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BehaviorFactory getBehaviorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorModelImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getBehaviorModel()
		 * @generated
		 */
		EClass BEHAVIOR_MODEL = eINSTANCE.getBehaviorModel();

		/**
		 * The meta object literal for the '<em><b>Templates</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_MODEL__TEMPLATES = eINSTANCE.getBehaviorModel_Templates();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorTemplateImpl <em>Template</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorTemplateImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getBehaviorTemplate()
		 * @generated
		 */
		EClass BEHAVIOR_TEMPLATE = eINSTANCE.getBehaviorTemplate();

		/**
		 * The meta object literal for the '<em><b>Pins</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TEMPLATE__PINS = eINSTANCE.getBehaviorTemplate_Pins();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BEHAVIOR_TEMPLATE__PARAMETER = eINSTANCE.getBehaviorTemplate_Parameter();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.ComposedBehaviorImpl <em>Composed Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.ComposedBehaviorImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getComposedBehavior()
		 * @generated
		 */
		EClass COMPOSED_BEHAVIOR = eINSTANCE.getComposedBehavior();

		/**
		 * The meta object literal for the '<em><b>Sub Behavior</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_BEHAVIOR__SUB_BEHAVIOR = eINSTANCE.getComposedBehavior_SubBehavior();

		/**
		 * The meta object literal for the '<em><b>Connectors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_BEHAVIOR__CONNECTORS = eINSTANCE.getComposedBehavior_Connectors();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.CostParameterImpl <em>Cost Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.CostParameterImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getCostParameter()
		 * @generated
		 */
		EClass COST_PARAMETER = eINSTANCE.getCostParameter();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COST_PARAMETER__UNIT = eINSTANCE.getCostParameter_Unit();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.EventImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.GuardImpl <em>Guard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.GuardImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getGuard()
		 * @generated
		 */
		EClass GUARD = eINSTANCE.getGuard();

		/**
		 * The meta object literal for the '<em><b>Boolean Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GUARD__BOOLEAN_EXPRESSION = eINSTANCE.getGuard_BooleanExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.NavigatingElementImpl <em>Navigating Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.NavigatingElementImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getNavigatingElement()
		 * @generated
		 */
		EClass NAVIGATING_ELEMENT = eINSTANCE.getNavigatingElement();

		/**
		 * The meta object literal for the '<em><b>Pin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAVIGATING_ELEMENT__PIN = eINSTANCE.getNavigatingElement_Pin();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.OccurrenceImpl <em>Occurrence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.OccurrenceImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getOccurrence()
		 * @generated
		 */
		EClass OCCURRENCE = eINSTANCE.getOccurrence();

		/**
		 * The meta object literal for the '<em><b>Pin</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OCCURRENCE__PIN = eINSTANCE.getOccurrence_Pin();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCCURRENCE__TIME = eINSTANCE.getOccurrence_Time();

		/**
		 * The meta object literal for the '<em><b>Load</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCCURRENCE__LOAD = eINSTANCE.getOccurrence_Load();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.PinImpl <em>Pin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.PinImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getPin()
		 * @generated
		 */
		EClass PIN = eINSTANCE.getPin();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN__DIRECTION = eINSTANCE.getPin_Direction();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN__PARAMETER = eINSTANCE.getPin_Parameter();

		/**
		 * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIN__VISIBILITY = eINSTANCE.getPin_Visibility();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.PinConnectorImpl <em>Pin Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.PinConnectorImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getPinConnector()
		 * @generated
		 */
		EClass PIN_CONNECTOR = eINSTANCE.getPinConnector();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN_CONNECTOR__SOURCE = eINSTANCE.getPinConnector_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIN_CONNECTOR__TARGET = eINSTANCE.getPinConnector_Target();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StateImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Fanout</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__FANOUT = eINSTANCE.getState_Fanout();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl <em>State Machine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getStateMachine()
		 * @generated
		 */
		EClass STATE_MACHINE = eINSTANCE.getStateMachine();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__STATES = eINSTANCE.getStateMachine_States();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__TRANSITIONS = eINSTANCE.getStateMachine_Transitions();

		/**
		 * The meta object literal for the '<em><b>Initial State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__INITIAL_STATE = eINSTANCE.getStateMachine_InitialState();

		/**
		 * The meta object literal for the '<em><b>Current State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__CURRENT_STATE = eINSTANCE.getStateMachine_CurrentState();

		/**
		 * The meta object literal for the '<em><b>Timer</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__TIMER = eINSTANCE.getStateMachine_Timer();

		/**
		 * The meta object literal for the '<em><b>Power</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__POWER = eINSTANCE.getStateMachine_Power();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__VARIABLES = eINSTANCE.getStateMachine_Variables();

		/**
		 * The meta object literal for the '<em><b>Step Infos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__STEP_INFOS = eINSTANCE.getStateMachine_StepInfos();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StmStructuralElementImpl <em>Stm Structural Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StmStructuralElementImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getStmStructuralElement()
		 * @generated
		 */
		EClass STM_STRUCTURAL_ELEMENT = eINSTANCE.getStmStructuralElement();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STM_STRUCTURAL_ELEMENT__PARAMETER = eINSTANCE.getStmStructuralElement_Parameter();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STM_STRUCTURAL_ELEMENT__ACTION = eINSTANCE.getStmStructuralElement_Action();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.TransitionImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TARGET = eINSTANCE.getTransition_Target();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SOURCE = eINSTANCE.getTransition_Source();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EVENT = eINSTANCE.getTransition_Event();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__GUARD = eINSTANCE.getTransition_Guard();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.WorkloadImpl <em>Workload</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.WorkloadImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getWorkload()
		 * @generated
		 */
		EClass WORKLOAD = eINSTANCE.getWorkload();

		/**
		 * The meta object literal for the '<em><b>Items</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORKLOAD__ITEMS = eINSTANCE.getWorkload_Items();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WORKLOAD__RESOURCE = eINSTANCE.getWorkload_Resource();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StepInfoImpl <em>Step Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.StepInfoImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getStepInfo()
		 * @generated
		 */
		EClass STEP_INFO = eINSTANCE.getStepInfo();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STEP_INFO__STATE = eINSTANCE.getStepInfo_State();

		/**
		 * The meta object literal for the '<em><b>Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STEP_INFO__CONFIG = eINSTANCE.getStepInfo_Config();

		/**
		 * The meta object literal for the '<em><b>Clock</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STEP_INFO__CLOCK = eINSTANCE.getStepInfo_Clock();

		/**
		 * The meta object literal for the '<em><b>Power</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STEP_INFO__POWER = eINSTANCE.getStepInfo_Power();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.VarConfigImpl <em>Var Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.VarConfigImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getVarConfig()
		 * @generated
		 */
		EClass VAR_CONFIG = eINSTANCE.getVarConfig();

		/**
		 * The meta object literal for the '<em><b>Var Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VAR_CONFIG__VAR_NAME = eINSTANCE.getVarConfig_VarName();

		/**
		 * The meta object literal for the '<em><b>Var Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VAR_CONFIG__VAR_VALUE = eINSTANCE.getVarConfig_VarValue();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.ActionImpl
		 * @see org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Statement</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__STATEMENT = eINSTANCE.getAction_Statement();

	}

} //BehaviorPackage
