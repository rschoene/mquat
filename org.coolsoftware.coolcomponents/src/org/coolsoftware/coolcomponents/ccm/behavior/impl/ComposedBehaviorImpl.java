/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.behavior.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage;
import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorTemplate;
import org.coolsoftware.coolcomponents.ccm.behavior.ComposedBehavior;
import org.coolsoftware.coolcomponents.ccm.behavior.PinConnector;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composed Behavior</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.ComposedBehaviorImpl#getSubBehavior <em>Sub Behavior</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.ComposedBehaviorImpl#getConnectors <em>Connectors</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComposedBehaviorImpl extends BehaviorTemplateImpl implements ComposedBehavior {
	/**
	 * The cached value of the '{@link #getSubBehavior() <em>Sub Behavior</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubBehavior()
	 * @generated
	 * @ordered
	 */
	protected EList<BehaviorTemplate> subBehavior;

	/**
	 * The cached value of the '{@link #getConnectors() <em>Connectors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectors()
	 * @generated
	 * @ordered
	 */
	protected EList<PinConnector> connectors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedBehaviorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BehaviorPackage.Literals.COMPOSED_BEHAVIOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BehaviorTemplate> getSubBehavior() {
		if (subBehavior == null) {
			subBehavior = new EObjectContainmentEList<BehaviorTemplate>(BehaviorTemplate.class, this, BehaviorPackage.COMPOSED_BEHAVIOR__SUB_BEHAVIOR);
		}
		return subBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PinConnector> getConnectors() {
		if (connectors == null) {
			connectors = new EObjectContainmentEList<PinConnector>(PinConnector.class, this, BehaviorPackage.COMPOSED_BEHAVIOR__CONNECTORS);
		}
		return connectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BehaviorPackage.COMPOSED_BEHAVIOR__SUB_BEHAVIOR:
				return ((InternalEList<?>)getSubBehavior()).basicRemove(otherEnd, msgs);
			case BehaviorPackage.COMPOSED_BEHAVIOR__CONNECTORS:
				return ((InternalEList<?>)getConnectors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BehaviorPackage.COMPOSED_BEHAVIOR__SUB_BEHAVIOR:
				return getSubBehavior();
			case BehaviorPackage.COMPOSED_BEHAVIOR__CONNECTORS:
				return getConnectors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BehaviorPackage.COMPOSED_BEHAVIOR__SUB_BEHAVIOR:
				getSubBehavior().clear();
				getSubBehavior().addAll((Collection<? extends BehaviorTemplate>)newValue);
				return;
			case BehaviorPackage.COMPOSED_BEHAVIOR__CONNECTORS:
				getConnectors().clear();
				getConnectors().addAll((Collection<? extends PinConnector>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BehaviorPackage.COMPOSED_BEHAVIOR__SUB_BEHAVIOR:
				getSubBehavior().clear();
				return;
			case BehaviorPackage.COMPOSED_BEHAVIOR__CONNECTORS:
				getConnectors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BehaviorPackage.COMPOSED_BEHAVIOR__SUB_BEHAVIOR:
				return subBehavior != null && !subBehavior.isEmpty();
			case BehaviorPackage.COMPOSED_BEHAVIOR__CONNECTORS:
				return connectors != null && !connectors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComposedBehaviorImpl
