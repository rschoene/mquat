/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.behavior.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage;
import org.coolsoftware.coolcomponents.ccm.behavior.State;
import org.coolsoftware.coolcomponents.ccm.behavior.StateMachine;
import org.coolsoftware.coolcomponents.ccm.behavior.StepInfo;
import org.coolsoftware.coolcomponents.ccm.behavior.Transition;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getStates <em>States</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getInitialState <em>Initial State</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getCurrentState <em>Current State</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getTimer <em>Timer</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getPower <em>Power</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.behavior.impl.StateMachineImpl#getStepInfos <em>Step Infos</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StateMachineImpl extends ComposedBehaviorImpl implements StateMachine {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getInitialState() <em>Initial State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialState()
	 * @generated
	 * @ordered
	 */
	protected State initialState;

	/**
	 * The cached value of the '{@link #getCurrentState() <em>Current State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentState()
	 * @generated
	 * @ordered
	 */
	protected State currentState;

	/**
	 * The cached value of the '{@link #getTimer() <em>Timer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimer()
	 * @generated
	 * @ordered
	 */
	protected VariableDeclaration timer;

	/**
	 * The cached value of the '{@link #getPower() <em>Power</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPower()
	 * @generated
	 * @ordered
	 */
	protected VariableDeclaration power;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<VariableDeclaration> variables;

	/**
	 * The cached value of the '{@link #getStepInfos() <em>Step Infos</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepInfos()
	 * @generated
	 * @ordered
	 */
	protected EList<StepInfo> stepInfos;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateMachineImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BehaviorPackage.Literals.STATE_MACHINE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, BehaviorPackage.STATE_MACHINE__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this, BehaviorPackage.STATE_MACHINE__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getInitialState() {
		if (initialState != null && initialState.eIsProxy()) {
			InternalEObject oldInitialState = (InternalEObject)initialState;
			initialState = (State)eResolveProxy(oldInitialState);
			if (initialState != oldInitialState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BehaviorPackage.STATE_MACHINE__INITIAL_STATE, oldInitialState, initialState));
			}
		}
		return initialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetInitialState() {
		return initialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialState(State newInitialState) {
		State oldInitialState = initialState;
		initialState = newInitialState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviorPackage.STATE_MACHINE__INITIAL_STATE, oldInitialState, initialState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getCurrentState() {
		if (currentState != null && currentState.eIsProxy()) {
			InternalEObject oldCurrentState = (InternalEObject)currentState;
			currentState = (State)eResolveProxy(oldCurrentState);
			if (currentState != oldCurrentState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BehaviorPackage.STATE_MACHINE__CURRENT_STATE, oldCurrentState, currentState));
			}
		}
		return currentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetCurrentState() {
		return currentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentState(State newCurrentState) {
		State oldCurrentState = currentState;
		currentState = newCurrentState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviorPackage.STATE_MACHINE__CURRENT_STATE, oldCurrentState, currentState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclaration getTimer() {
		return timer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimer(VariableDeclaration newTimer, NotificationChain msgs) {
		VariableDeclaration oldTimer = timer;
		timer = newTimer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviorPackage.STATE_MACHINE__TIMER, oldTimer, newTimer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimer(VariableDeclaration newTimer) {
		if (newTimer != timer) {
			NotificationChain msgs = null;
			if (timer != null)
				msgs = ((InternalEObject)timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviorPackage.STATE_MACHINE__TIMER, null, msgs);
			if (newTimer != null)
				msgs = ((InternalEObject)newTimer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviorPackage.STATE_MACHINE__TIMER, null, msgs);
			msgs = basicSetTimer(newTimer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviorPackage.STATE_MACHINE__TIMER, newTimer, newTimer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclaration getPower() {
		return power;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPower(VariableDeclaration newPower, NotificationChain msgs) {
		VariableDeclaration oldPower = power;
		power = newPower;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BehaviorPackage.STATE_MACHINE__POWER, oldPower, newPower);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPower(VariableDeclaration newPower) {
		if (newPower != power) {
			NotificationChain msgs = null;
			if (power != null)
				msgs = ((InternalEObject)power).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BehaviorPackage.STATE_MACHINE__POWER, null, msgs);
			if (newPower != null)
				msgs = ((InternalEObject)newPower).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BehaviorPackage.STATE_MACHINE__POWER, null, msgs);
			msgs = basicSetPower(newPower, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BehaviorPackage.STATE_MACHINE__POWER, newPower, newPower));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariableDeclaration> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList<VariableDeclaration>(VariableDeclaration.class, this, BehaviorPackage.STATE_MACHINE__VARIABLES);
		}
		return variables;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StepInfo> getStepInfos() {
		if (stepInfos == null) {
			stepInfos = new EObjectContainmentEList<StepInfo>(StepInfo.class, this, BehaviorPackage.STATE_MACHINE__STEP_INFOS);
		}
		return stepInfos;
	}
	
	public boolean addStepInfo(StepInfo si) {
		if(stepInfos == null) stepInfos = new BasicEList<StepInfo>();
		return stepInfos.add(si);
	}

	public void updateVariable(Variable var) {
		throw new UnsupportedOperationException("This method has to be reimplmented due to the revised expression package");
		// FIXME: REIMPLMENT THE OLD IMPLEMENTATION
//		EList<Variable> ret = new BasicEList<Variable>();
//		for(Variable v : variables) {
//			if(v.getName().equals(var.getName())) {
//				ret.add(var);
//			} else {
//				ret.add(v);
//			}
//		}
//		variables = ret;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BehaviorPackage.STATE_MACHINE__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case BehaviorPackage.STATE_MACHINE__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
			case BehaviorPackage.STATE_MACHINE__TIMER:
				return basicSetTimer(null, msgs);
			case BehaviorPackage.STATE_MACHINE__POWER:
				return basicSetPower(null, msgs);
			case BehaviorPackage.STATE_MACHINE__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case BehaviorPackage.STATE_MACHINE__STEP_INFOS:
				return ((InternalEList<?>)getStepInfos()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BehaviorPackage.STATE_MACHINE__STATES:
				return getStates();
			case BehaviorPackage.STATE_MACHINE__TRANSITIONS:
				return getTransitions();
			case BehaviorPackage.STATE_MACHINE__INITIAL_STATE:
				if (resolve) return getInitialState();
				return basicGetInitialState();
			case BehaviorPackage.STATE_MACHINE__CURRENT_STATE:
				if (resolve) return getCurrentState();
				return basicGetCurrentState();
			case BehaviorPackage.STATE_MACHINE__TIMER:
				return getTimer();
			case BehaviorPackage.STATE_MACHINE__POWER:
				return getPower();
			case BehaviorPackage.STATE_MACHINE__VARIABLES:
				return getVariables();
			case BehaviorPackage.STATE_MACHINE__STEP_INFOS:
				return getStepInfos();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BehaviorPackage.STATE_MACHINE__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case BehaviorPackage.STATE_MACHINE__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case BehaviorPackage.STATE_MACHINE__INITIAL_STATE:
				setInitialState((State)newValue);
				return;
			case BehaviorPackage.STATE_MACHINE__CURRENT_STATE:
				setCurrentState((State)newValue);
				return;
			case BehaviorPackage.STATE_MACHINE__TIMER:
				setTimer((VariableDeclaration)newValue);
				return;
			case BehaviorPackage.STATE_MACHINE__POWER:
				setPower((VariableDeclaration)newValue);
				return;
			case BehaviorPackage.STATE_MACHINE__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends VariableDeclaration>)newValue);
				return;
			case BehaviorPackage.STATE_MACHINE__STEP_INFOS:
				getStepInfos().clear();
				getStepInfos().addAll((Collection<? extends StepInfo>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BehaviorPackage.STATE_MACHINE__STATES:
				getStates().clear();
				return;
			case BehaviorPackage.STATE_MACHINE__TRANSITIONS:
				getTransitions().clear();
				return;
			case BehaviorPackage.STATE_MACHINE__INITIAL_STATE:
				setInitialState((State)null);
				return;
			case BehaviorPackage.STATE_MACHINE__CURRENT_STATE:
				setCurrentState((State)null);
				return;
			case BehaviorPackage.STATE_MACHINE__TIMER:
				setTimer((VariableDeclaration)null);
				return;
			case BehaviorPackage.STATE_MACHINE__POWER:
				setPower((VariableDeclaration)null);
				return;
			case BehaviorPackage.STATE_MACHINE__VARIABLES:
				getVariables().clear();
				return;
			case BehaviorPackage.STATE_MACHINE__STEP_INFOS:
				getStepInfos().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BehaviorPackage.STATE_MACHINE__STATES:
				return states != null && !states.isEmpty();
			case BehaviorPackage.STATE_MACHINE__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
			case BehaviorPackage.STATE_MACHINE__INITIAL_STATE:
				return initialState != null;
			case BehaviorPackage.STATE_MACHINE__CURRENT_STATE:
				return currentState != null;
			case BehaviorPackage.STATE_MACHINE__TIMER:
				return timer != null;
			case BehaviorPackage.STATE_MACHINE__POWER:
				return power != null;
			case BehaviorPackage.STATE_MACHINE__VARIABLES:
				return variables != null && !variables.isEmpty();
			case BehaviorPackage.STATE_MACHINE__STEP_INFOS:
				return stepInfos != null && !stepInfos.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void reset() {
		CcmInteger zero = StdlibFactory.eINSTANCE.createCcmInteger();
		zero.setIntegerValue(0);
		zero.setRealValue(0);
		CcmReal rzero = StdlibFactory.eINSTANCE.createCcmReal();
		rzero.setRealValue(0);
		getTimer().getDeclaredVariable().setValue(zero);
		getPower().getDeclaredVariable().setValue(rzero);
		setCurrentState(getInitialState());
	}

} //StateMachineImpl
