/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.CcmPackage;
import org.coolsoftware.coolcomponents.ccm.CoolEnvironment;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.UserType;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;
import org.coolsoftware.coolcomponents.ccm.variant.User;
import org.coolsoftware.coolcomponents.nameable.impl.DescribableElementImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cool Environment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl#getSoftwareTypeSystem <em>Software Type System</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl#getResourceTypeSystem <em>Resource Type System</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl#getUserTypeSystem <em>User Type System</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl#getComponents <em>Components</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl#getResourcemodel <em>Resourcemodel</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.ccm.impl.CoolEnvironmentImpl#getUsers <em>Users</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CoolEnvironmentImpl extends DescribableElementImpl implements CoolEnvironment {
	/**
	 * The cached value of the '{@link #getSoftwareTypeSystem() <em>Software Type System</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftwareTypeSystem()
	 * @generated
	 * @ordered
	 */
	protected EList<SWComponentType> softwareTypeSystem;

	/**
	 * The cached value of the '{@link #getResourceTypeSystem() <em>Resource Type System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceTypeSystem()
	 * @generated
	 * @ordered
	 */
	protected ResourceType resourceTypeSystem;

	/**
	 * The cached value of the '{@link #getUserTypeSystem() <em>User Type System</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserTypeSystem()
	 * @generated
	 * @ordered
	 */
	protected EList<UserType> userTypeSystem;

	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<SWComponent> components;

	/**
	 * The cached value of the '{@link #getResourcemodel() <em>Resourcemodel</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourcemodel()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> resourcemodel;

	/**
	 * The cached value of the '{@link #getUsers() <em>Users</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsers()
	 * @generated
	 * @ordered
	 */
	protected EList<User> users;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoolEnvironmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CcmPackage.Literals.COOL_ENVIRONMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SWComponentType> getSoftwareTypeSystem() {
		if (softwareTypeSystem == null) {
			softwareTypeSystem = new EObjectResolvingEList<SWComponentType>(SWComponentType.class, this, CcmPackage.COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM);
		}
		return softwareTypeSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceType getResourceTypeSystem() {
		if (resourceTypeSystem != null && resourceTypeSystem.eIsProxy()) {
			InternalEObject oldResourceTypeSystem = (InternalEObject)resourceTypeSystem;
			resourceTypeSystem = (ResourceType)eResolveProxy(oldResourceTypeSystem);
			if (resourceTypeSystem != oldResourceTypeSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CcmPackage.COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM, oldResourceTypeSystem, resourceTypeSystem));
			}
		}
		return resourceTypeSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceType basicGetResourceTypeSystem() {
		return resourceTypeSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceTypeSystem(ResourceType newResourceTypeSystem) {
		ResourceType oldResourceTypeSystem = resourceTypeSystem;
		resourceTypeSystem = newResourceTypeSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CcmPackage.COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM, oldResourceTypeSystem, resourceTypeSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UserType> getUserTypeSystem() {
		if (userTypeSystem == null) {
			userTypeSystem = new EObjectResolvingEList<UserType>(UserType.class, this, CcmPackage.COOL_ENVIRONMENT__USER_TYPE_SYSTEM);
		}
		return userTypeSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SWComponent> getComponents() {
		if (components == null) {
			components = new EObjectResolvingEList<SWComponent>(SWComponent.class, this, CcmPackage.COOL_ENVIRONMENT__COMPONENTS);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getResourcemodel() {
		if (resourcemodel == null) {
			resourcemodel = new EObjectResolvingEList<Resource>(Resource.class, this, CcmPackage.COOL_ENVIRONMENT__RESOURCEMODEL);
		}
		return resourcemodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<User> getUsers() {
		if (users == null) {
			users = new EObjectResolvingEList<User>(User.class, this, CcmPackage.COOL_ENVIRONMENT__USERS);
		}
		return users;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CcmPackage.COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM:
				return getSoftwareTypeSystem();
			case CcmPackage.COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM:
				if (resolve) return getResourceTypeSystem();
				return basicGetResourceTypeSystem();
			case CcmPackage.COOL_ENVIRONMENT__USER_TYPE_SYSTEM:
				return getUserTypeSystem();
			case CcmPackage.COOL_ENVIRONMENT__COMPONENTS:
				return getComponents();
			case CcmPackage.COOL_ENVIRONMENT__RESOURCEMODEL:
				return getResourcemodel();
			case CcmPackage.COOL_ENVIRONMENT__USERS:
				return getUsers();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CcmPackage.COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM:
				getSoftwareTypeSystem().clear();
				getSoftwareTypeSystem().addAll((Collection<? extends SWComponentType>)newValue);
				return;
			case CcmPackage.COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM:
				setResourceTypeSystem((ResourceType)newValue);
				return;
			case CcmPackage.COOL_ENVIRONMENT__USER_TYPE_SYSTEM:
				getUserTypeSystem().clear();
				getUserTypeSystem().addAll((Collection<? extends UserType>)newValue);
				return;
			case CcmPackage.COOL_ENVIRONMENT__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends SWComponent>)newValue);
				return;
			case CcmPackage.COOL_ENVIRONMENT__RESOURCEMODEL:
				getResourcemodel().clear();
				getResourcemodel().addAll((Collection<? extends Resource>)newValue);
				return;
			case CcmPackage.COOL_ENVIRONMENT__USERS:
				getUsers().clear();
				getUsers().addAll((Collection<? extends User>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CcmPackage.COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM:
				getSoftwareTypeSystem().clear();
				return;
			case CcmPackage.COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM:
				setResourceTypeSystem((ResourceType)null);
				return;
			case CcmPackage.COOL_ENVIRONMENT__USER_TYPE_SYSTEM:
				getUserTypeSystem().clear();
				return;
			case CcmPackage.COOL_ENVIRONMENT__COMPONENTS:
				getComponents().clear();
				return;
			case CcmPackage.COOL_ENVIRONMENT__RESOURCEMODEL:
				getResourcemodel().clear();
				return;
			case CcmPackage.COOL_ENVIRONMENT__USERS:
				getUsers().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CcmPackage.COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM:
				return softwareTypeSystem != null && !softwareTypeSystem.isEmpty();
			case CcmPackage.COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM:
				return resourceTypeSystem != null;
			case CcmPackage.COOL_ENVIRONMENT__USER_TYPE_SYSTEM:
				return userTypeSystem != null && !userTypeSystem.isEmpty();
			case CcmPackage.COOL_ENVIRONMENT__COMPONENTS:
				return components != null && !components.isEmpty();
			case CcmPackage.COOL_ENVIRONMENT__RESOURCEMODEL:
				return resourcemodel != null && !resourcemodel.isEmpty();
			case CcmPackage.COOL_ENVIRONMENT__USERS:
				return users != null && !users.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CoolEnvironmentImpl
