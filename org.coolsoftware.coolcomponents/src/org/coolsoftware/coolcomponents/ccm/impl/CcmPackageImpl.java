/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.ccm.impl;

import org.coolsoftware.coolcomponents.ccm.CcmFactory;
import org.coolsoftware.coolcomponents.ccm.CcmPackage;
import org.coolsoftware.coolcomponents.ccm.CoolEnvironment;
import org.coolsoftware.coolcomponents.ccm.Direction;
import org.coolsoftware.coolcomponents.ccm.ParameterDirection;
import org.coolsoftware.coolcomponents.ccm.Visibility;
import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage;
import org.coolsoftware.coolcomponents.ccm.behavior.impl.BehaviorPackageImpl;
import org.coolsoftware.coolcomponents.ccm.structure.StructurePackage;
import org.coolsoftware.coolcomponents.ccm.structure.impl.StructurePackageImpl;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPackage;
import org.coolsoftware.coolcomponents.ccm.variant.impl.VariantPackageImpl;
import org.coolsoftware.coolcomponents.expressions.ExpressionsPackage;
import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.coolsoftware.coolcomponents.nameable.impl.NameablePackageImpl;
import org.coolsoftware.coolcomponents.units.UnitsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CcmPackageImpl extends EPackageImpl implements CcmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass coolEnvironmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum visibilityEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum directionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum parameterDirectionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.coolsoftware.coolcomponents.ccm.CcmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CcmPackageImpl() {
		super(eNS_URI, CcmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CcmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CcmPackage init() {
		if (isInited) return (CcmPackage)EPackage.Registry.INSTANCE.getEPackage(CcmPackage.eNS_URI);

		// Obtain or create and register package
		CcmPackageImpl theCcmPackage = (CcmPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CcmPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CcmPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		UnitsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		StructurePackageImpl theStructurePackage = (StructurePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI) instanceof StructurePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI) : StructurePackage.eINSTANCE);
		VariantPackageImpl theVariantPackage = (VariantPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(VariantPackage.eNS_URI) instanceof VariantPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(VariantPackage.eNS_URI) : VariantPackage.eINSTANCE);
		BehaviorPackageImpl theBehaviorPackage = (BehaviorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI) instanceof BehaviorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI) : BehaviorPackage.eINSTANCE);
		NameablePackageImpl theNameablePackage = (NameablePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(NameablePackage.eNS_URI) instanceof NameablePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(NameablePackage.eNS_URI) : NameablePackage.eINSTANCE);

		// Create package meta-data objects
		theCcmPackage.createPackageContents();
		theStructurePackage.createPackageContents();
		theVariantPackage.createPackageContents();
		theBehaviorPackage.createPackageContents();
		theNameablePackage.createPackageContents();

		// Initialize created meta-data
		theCcmPackage.initializePackageContents();
		theStructurePackage.initializePackageContents();
		theVariantPackage.initializePackageContents();
		theBehaviorPackage.initializePackageContents();
		theNameablePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCcmPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CcmPackage.eNS_URI, theCcmPackage);
		return theCcmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCoolEnvironment() {
		return coolEnvironmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoolEnvironment_SoftwareTypeSystem() {
		return (EReference)coolEnvironmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoolEnvironment_ResourceTypeSystem() {
		return (EReference)coolEnvironmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoolEnvironment_UserTypeSystem() {
		return (EReference)coolEnvironmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoolEnvironment_Components() {
		return (EReference)coolEnvironmentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoolEnvironment_Resourcemodel() {
		return (EReference)coolEnvironmentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCoolEnvironment_Users() {
		return (EReference)coolEnvironmentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getVisibility() {
		return visibilityEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDirection() {
		return directionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getParameterDirection() {
		return parameterDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CcmFactory getCcmFactory() {
		return (CcmFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		coolEnvironmentEClass = createEClass(COOL_ENVIRONMENT);
		createEReference(coolEnvironmentEClass, COOL_ENVIRONMENT__SOFTWARE_TYPE_SYSTEM);
		createEReference(coolEnvironmentEClass, COOL_ENVIRONMENT__RESOURCE_TYPE_SYSTEM);
		createEReference(coolEnvironmentEClass, COOL_ENVIRONMENT__USER_TYPE_SYSTEM);
		createEReference(coolEnvironmentEClass, COOL_ENVIRONMENT__COMPONENTS);
		createEReference(coolEnvironmentEClass, COOL_ENVIRONMENT__RESOURCEMODEL);
		createEReference(coolEnvironmentEClass, COOL_ENVIRONMENT__USERS);

		// Create enums
		visibilityEEnum = createEEnum(VISIBILITY);
		directionEEnum = createEEnum(DIRECTION);
		parameterDirectionEEnum = createEEnum(PARAMETER_DIRECTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		StructurePackage theStructurePackage = (StructurePackage)EPackage.Registry.INSTANCE.getEPackage(StructurePackage.eNS_URI);
		VariantPackage theVariantPackage = (VariantPackage)EPackage.Registry.INSTANCE.getEPackage(VariantPackage.eNS_URI);
		BehaviorPackage theBehaviorPackage = (BehaviorPackage)EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
		NameablePackage theNameablePackage = (NameablePackage)EPackage.Registry.INSTANCE.getEPackage(NameablePackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theStructurePackage);
		getESubpackages().add(theVariantPackage);
		getESubpackages().add(theBehaviorPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		coolEnvironmentEClass.getESuperTypes().add(theNameablePackage.getDescribableElement());

		// Initialize classes and features; add operations and parameters
		initEClass(coolEnvironmentEClass, CoolEnvironment.class, "CoolEnvironment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCoolEnvironment_SoftwareTypeSystem(), theStructurePackage.getSWComponentType(), null, "softwareTypeSystem", null, 0, -1, CoolEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCoolEnvironment_ResourceTypeSystem(), theStructurePackage.getResourceType(), null, "resourceTypeSystem", null, 0, 1, CoolEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCoolEnvironment_UserTypeSystem(), theStructurePackage.getUserType(), null, "userTypeSystem", null, 0, -1, CoolEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCoolEnvironment_Components(), theVariantPackage.getSWComponent(), null, "components", null, 0, -1, CoolEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCoolEnvironment_Resourcemodel(), theVariantPackage.getResource(), null, "resourcemodel", null, 0, -1, CoolEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCoolEnvironment_Users(), theVariantPackage.getUser(), null, "users", null, 0, -1, CoolEnvironment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(visibilityEEnum, Visibility.class, "Visibility");
		addEEnumLiteral(visibilityEEnum, Visibility.PRIVATE);
		addEEnumLiteral(visibilityEEnum, Visibility.PUBLIC);
		addEEnumLiteral(visibilityEEnum, Visibility.PUBLIC_ALL);

		initEEnum(directionEEnum, Direction.class, "Direction");
		addEEnumLiteral(directionEEnum, Direction.PROVIDED);
		addEEnumLiteral(directionEEnum, Direction.REQUIRED);

		initEEnum(parameterDirectionEEnum, ParameterDirection.class, "ParameterDirection");
		addEEnumLiteral(parameterDirectionEEnum, ParameterDirection.IN);
		addEEnumLiteral(parameterDirectionEEnum, ParameterDirection.OUT);
		addEEnumLiteral(parameterDirectionEEnum, ParameterDirection.INOUT);

		// Create resource
		createResource(eNS_URI);
	}

} //CcmPackageImpl
