/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.censor.ffmpeg;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.CensorCensor;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;
import org.haec.videos.Video;
import org.haec.videoprovider.IVideoProviderFactory;
import org.haec.videoprovider.VideoProviderFactory;

class CensorData implements BenchmarkData {
	File in;
	int mvl;

	public CensorData(File in, int mvl) {
		super();
		this.in = in;
		this.mvl = mvl;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mvl;
	}

}

/**
 * Benchmark for org.haec.app.censor.ffmpeg.CensorFfmpeg
 * @author René Schöne
 */
public class CensorFfmpegBenchmark implements Benchmark {
	
	private CensorCensor comp = new CensorFfmpeg();
	private IVideoProviderFactory fac;
	protected Logger log = Logger.getLogger(CensorFfmpegBenchmark.class);
	
	public CensorFfmpegBenchmark(IVideoProviderFactory fac) {
		this.fac = fac;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>();
		log.info("[CEN Bench] Started");
		for(Video video: fac.getCurrentVideoProvider().getOneVideoOfEachLength()) {
			int mpValue = TranscodingServerUtil.getMetaparameterValue(video.length, video.resX, video.resY);
			result.add(new CensorData(video.file, mpValue));
		}
		log.info("[CEN Bench] Finished");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		CensorData d = (CensorData) data;
		log.debug("[CEN Bench] - " + d.in.getName());
		return comp.censor(d.in);
	}

}
