/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.censor.ffmpeg;

import java.io.File;

import org.haec.app.videotranscodingserver.CensorCensor;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.videoprovider.IVideoProviderFactory;

/**
 * Implementation of org.haec.app.censor.ffmpeg.CensorFfmpeg
 * @author René Schöne
 */
public class CensorFfmpeg extends TaskBasedImpl implements CensorCensor {
	
	private IVideoProviderFactory fac;

	/* (non-Javadoc)
	 * @see org.haec.app.censor.censorffmpeg.CensorCensor#censor(java.io.File)
	 */
	@Override
	public File censor(File fileVideo) {
		//TODO Implement censor
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return TranscodingServerUtil.APP_NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "Censor";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.censor.ffmpeg.CensorFfmpeg";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new CensorFfmpegBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.censor.ffmpeg";
	}
	
	protected void setVideoProviderFactory(IVideoProviderFactory fac) {
		this.fac = fac;
	}

}
