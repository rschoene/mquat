/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.theatre.settings.TheatreSettingsImpl;
import org.haec.theatre.utils.settings.BooleanSetting;
import org.haec.theatre.utils.settings.IntegerSetting;
import org.haec.theatre.utils.settings.Setting;
import org.haec.theatre.utils.settings.StringSetting;

/**
 * Impl of extended settings for video stuff
 * @author René Schöne
 */
public class VideoSettingsImpl extends TheatreSettingsImpl implements
		VideoSettings {

	// transcoding-stuff
	public Setting<Boolean> encodeNoSound = new BooleanSetting("haec.videoutil.encode.nosound", false);
	public Setting<Boolean> uiDebug = new BooleanSetting("org.haec.app.videotranscodingserver.ui.debug", true);
	public Setting<Boolean> appsDebug = new BooleanSetting("org.haec.apps.debug", false);
	public Setting<String> cuiVideo1 = new StringSetting("org.haec.app.videoTranscodingServer.cui.video1", "/home/linaro/store/videos/sintel2048_7_1.mp4");
	public Setting<String> cuiVideo2 = new StringSetting("org.haec.app.videoTranscodingServer.cui.video2", "/home/linaro/store/videos/Route66_27_1.avi");
	public Setting<String> cui2TaskConfigPath = new StringSetting("org.haec.cui2.taskConfigPath", "/home/linaro/store/theatre-slave/cui2TaskConfig.txt");
	public Setting<String> cui2CallConfigPath = new StringSetting("org.haec.cui2.callConfigPath", "/home/linaro/store/theatre-slave/cui2CallConfig.properties");
	public Setting<String> videoProvidername = new StringSetting("theatre.videoProvider", "proxy");

	// videoplatform-stuff
	public Setting<String> vpHost = new StringSetting("vp.host", getGemIp().get());
	public Setting<Integer> vpPort = new IntegerSetting("vp.port", 8980);
	public Setting<String> vpPathFinished = new StringSetting("vp.path.finished", "tasks/finished");
	public Setting<String> vpPathNew = new StringSetting("vp.path.started", "tasks/started");
	public Setting<Boolean> updateConfigOnNotify = new BooleanSetting("vp.update.config.onNotify", true);
	public Setting<Boolean> vpNotifierEnabled = new BooleanSetting("vp.enabled.notifier", false);
	public Setting<Boolean> vpResolverEnabled = new BooleanSetting("vp.enabled.resolver", true);
	public Setting<String> vpPathVideoByName = new StringSetting("vp.path.videoByName", "tm/videoByName");

	// ffmpeg
	public Setting<String> ffbin = new StringSetting("haec.ffmpeg.bin", null);
	public Setting<Boolean> ffmpegtemp = new BooleanSetting("haec.ffmpeg.temporary", false);
	public Setting<Boolean> ffmpegmergeerror = new BooleanSetting("haec.ffmpeg.redirect.error", false);
	public Setting<Boolean> ffmpegpass = new BooleanSetting("haec.ffmpeg.pass", false);

	// mencoder/mplayer
	public Setting<String> mencoderbin = new StringSetting("haec.mencoder.bin", null);
	public Setting<String> mplayerbin = new StringSetting("haec.mplayer.bin", null);
	public Setting<Boolean> mencodertemp = new BooleanSetting("haec.mencoder.temporary", false);
	public Setting<Boolean> mencodermergeerror = new BooleanSetting("haec.mencoder.redirect.error", false);
	public Setting<Boolean> mencoderpass = new BooleanSetting("haec.mencoder.pass", false);
	
	// handbrake
	public Setting<String> handbrakebin = new StringSetting("haec.handbrake.bin", null);
	public Setting<Boolean> handbraketemp = new BooleanSetting("haec.handbrake.temporary", false);
	public Setting<Boolean> handbrakemergeerror = new BooleanSetting("haec.handbrake.redirect.error", false);
	public Setting<Boolean> handbrakepass = new BooleanSetting("haec.handbrake.pass", false);

	// slave-test
	public Setting<Boolean> runScalingFfmepg = new BooleanSetting("scaling.ffmpeg", false);
	public Setting<Boolean> runScalingMencoder = new BooleanSetting("scaling.mencoder", false);
	public Setting<Boolean> runScalingHandbrake = new BooleanSetting("scaling.handbrake", false);

	public Setting<Boolean> runTranscoderFfmepg = new BooleanSetting("transcoder.ffmpeg", false);
	public Setting<Boolean> runTranscoderMencoder = new BooleanSetting("transcoder.mencoder", false);
	public Setting<Boolean> runTranscoderHandbrake = new BooleanSetting("transcoder.handbrake", false);

	public Setting<Boolean> runPipFfmepg = new BooleanSetting("pip.ffmpeg", false);

	public Setting<String> videoRegexName = new StringSetting("param.video.baseName", "Route*_1.avi");
	public Setting<String> firstVideoName = new StringSetting("param.video.first", null);
	public Setting<String> secondVideoName = new StringSetting("param.video.second", null);

	public Setting<Integer> scalingWidth = new IntegerSetting("param.scaling.width", 320);
	public Setting<Integer> scalingHeight = new IntegerSetting("param.scaling.height", 240);
	public Setting<String> transcoderVCodec = new StringSetting("param.transcoder.vcodec", "h264");
	public Setting<String> transcoderACodec = new StringSetting("param.transcoder.acodec", "aac");
	public Setting<Integer> pipPos = new IntegerSetting("param.pip.pos", PipDoPip.POS_CENTER);
	
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoutilMergeError()
	 */
	@Override
	public Setting<Boolean> getVideoutilMergeError(String id) {
		switch(id) {
		case FFMPEG: return ffmpegmergeerror;
		case MENCODER: //$FALL-THROUGH$
		case MPLAYER: return mencodermergeerror;
		case HANDBRAKE: return handbrakemergeerror;
		default: return null;
		}
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getFmpegCmdDir()
	 */
	@Override
	public Setting<String> getVideoUtilBin(String id) {
		switch(id) {
		case FFMPEG: return ffbin;
		case MENCODER: return mencoderbin;
		case MPLAYER: return mplayerbin;
		case HANDBRAKE: return handbrakebin;
		default: return null;
		}
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoutilPass(java.lang.String)
	 */
	@Override
	public Setting<Boolean> getVideoutilPass(String id) {
		switch(id) {
		case FFMPEG: return ffmpegpass;
		case MENCODER: //$FALL-THROUGH$
		case MPLAYER: return mencoderpass;
		case HANDBRAKE: return handbrakepass;
		default: return null;
		}
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoutilTemporaryFiles(java.lang.String)
	 */
	@Override
	public Setting<Boolean> getVideoutilTemporaryFiles(String id) {
		switch(id) {
		case FFMPEG: return ffmpegtemp;
		case MENCODER: //$FALL-THROUGH$
		case MPLAYER: return mencodertemp;
		case HANDBRAKE: return handbraketemp;
		default: return null;
		}
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoUtilEncodeNoSound(java.lang.String)
	 */
	@Override
	public Setting<Boolean> getVideoUtilEncodeNoSound(String id) {
		return encodeNoSound;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getUiDebug()
	 */
	@Override
	public Setting<Boolean> getUiDebug() {
		return uiDebug;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getAppsDebug()
	 */
	@Override
	public Setting<Boolean> getAppsDebug() {
		return appsDebug;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCuiVideo1()
	 */
	@Override
	public Setting<String> getCuiVideo1() {
		return cuiVideo1;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCuiVideo2()
	 */
	@Override
	public Setting<String> getCuiVideo2() {
		return cuiVideo2;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCui2TaskConfigPath()
	 */
	@Override
	public Setting<String> getCui2TaskConfigPath() {
		return cui2TaskConfigPath;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getCui2CallConfigPath()
	 */
	@Override
	public Setting<String> getCui2CallConfigPath() {
		return cui2CallConfigPath;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoProviderName()
	 */
	@Override
	public Setting<String> getVideoProviderName() {
		return videoProvidername;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoplatformHost()
	 */
	@Override
	public Setting<String> getVideoplatformHost() {
		return vpHost;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPort()
	 */
	@Override
	public Setting<Integer> getVideoPlatformPort() {
		return vpPort;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPathFinished()
	 */
	@Override
	public Setting<String> getVideoPlatformPathFinished() {
		return vpPathFinished;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPathNew()
	 */
	@Override
	public Setting<String> getVideoPlatformPathNew() {
		return vpPathNew;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoPlatformPathVideoByName()
	 */
	@Override
	public Setting<String> getVideoPlatformPathVideoByName() {
		return vpPathVideoByName;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoplatformNotifierEnabled()
	 */
	@Override
	public Setting<Boolean> getVideoplatformNotifierEnabled() {
		return vpNotifierEnabled;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#getVideoplatformResolverEnabled()
	 */
	@Override
	public Setting<Boolean> getVideoplatformResolverEnabled() {
		return vpResolverEnabled;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runScalingFfmepg()
	 */
	@Override
	public Setting<Boolean> runScalingFfmepg() {
		return runScalingFfmepg;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runScalingMencoder()
	 */
	@Override
	public Setting<Boolean> runScalingMencoder() {
		return runScalingMencoder;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runScalingHandbrake()
	 */
	@Override
	public Setting<Boolean> runScalingHandbrake() {
		return runScalingHandbrake;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runTranscoderFfmepg()
	 */
	@Override
	public Setting<Boolean> runTranscoderFfmepg() {
		return runTranscoderFfmepg;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runTranscoderMencoder()
	 */
	@Override
	public Setting<Boolean> runTranscoderMencoder() {
		return runTranscoderMencoder;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runTranscoderHandbrake()
	 */
	@Override
	public Setting<Boolean> runTranscoderHandbrake() {
		return runTranscoderHandbrake;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#runPipFfmepg()
	 */
	@Override
	public Setting<Boolean> runPipFfmepg() {
		return runPipFfmepg;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#videoRegexName()
	 */
	@Override
	public Setting<String> videoRegexName() {
		return videoRegexName;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#firstVideoName()
	 */
	@Override
	public Setting<String> firstVideoName() {
		return firstVideoName;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#secondVideoName()
	 */
	@Override
	public Setting<String> secondVideoName() {
		return secondVideoName;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#scalingWidth()
	 */
	@Override
	public Setting<Integer> scalingWidth() {
		return scalingWidth;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#scalingHeight()
	 */
	@Override
	public Setting<Integer> scalingHeight() {
		return scalingHeight;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#transcoderVCodec()
	 */
	@Override
	public Setting<String> transcoderVCodec() {
		return transcoderVCodec;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#transcoderACodec()
	 */
	@Override
	public Setting<String> transcoderACodec() {
		return transcoderACodec;
	}
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoSettings#pipPos()
	 */
	@Override
	public Setting<Integer> pipPos() {
		return pipPos;
	}

}
