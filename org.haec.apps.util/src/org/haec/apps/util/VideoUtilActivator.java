/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import org.coolsoftware.theatre.Resettable;
import org.coolsoftware.theatre.util.Reconnectable;
import org.haec.theatre.utils.BundleUtils;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * Abstract activator for bundles providing VideoUtils.
 * @author René Schöne
 */
public abstract class VideoUtilActivator implements BundleActivator {

	private List<ServiceRegistration<?>> regs = new LinkedList<ServiceRegistration<?>>();

	@Override
	public void start(BundleContext context) throws Exception {
		Hashtable<String, Object> props = new Hashtable<String, Object>();
//		props.put(RemoteOSGiService.R_OSGi_REGISTRATION, Boolean.TRUE);
		for(Service service : getServicesToRegister()) {
			regs.add(context.registerService(service.getClassNames(), service.getInstance(), props));
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		if(!regs.isEmpty()) {
			for(ServiceRegistration<?> reg : regs) {
				reg.unregister();
			}
			regs.clear();
		}
	}
	
	/**
	 * Subclasses should return the services to register at the activator here.
	 * Always registers the instances as resettable.
	 * @return the services to register
	 */
	protected abstract Service[] getServicesToRegister();
	
	protected class Service {
		public Object instance;
		public Class<?>[] clazzes;
		public Service(Object instance, Class<?>... clazzes) {
			this.instance = instance;
			this.clazzes = clazzes;
		}
		public String[] getClassNames() {
			List<String> result = new ArrayList<String>(clazzes.length+1);
			for (int i = 0; i < clazzes.length; i++) {
				result.add(clazzes[i].getName());
			}
			result.add(Resettable.class.getName());
			return result.toArray(new String[result.size()]);
		}
		public Object getInstance() {
			return instance;
		}
	}

}
