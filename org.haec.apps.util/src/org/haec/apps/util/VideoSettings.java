/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.settings.Setting;

/**
 * More settings specific for video transcoding
 * @author René Schöne
 */
public interface VideoSettings extends TheatreSettings {
	
	// general
	/** Name of the video provider to use (choices: haec-box, proxy, example) (default: "proxy")*/
	public abstract Setting<String> getVideoProviderName();
	
	// video-util
	public final String FFMPEG = "ffmpeg";
	public final String MENCODER = "mencoder";
	public final String MPLAYER = "mplayer";
	public final String HANDBRAKE = "handbrake";
	
	/** Directory of a videoUtil command. (no default)*/
	public abstract Setting<String> getVideoUtilBin(String id);
	/** Use temporary files for a videoUtil (default: true)*/
	public abstract Setting<Boolean> getVideoutilTemporaryFiles(String id);
	/** Ignore requests and return first input for a videoUtil (default: true)*/
	public abstract Setting<Boolean> getVideoutilPass(String id);
	/** Merge error into output for a videoUtil (concerning started transcoder processes) (default: true)*/
	public abstract Setting<Boolean> getVideoutilMergeError(String id);
	/** Encode without sound using a videoUtil command. (default: false)*/
	public abstract Setting<Boolean> getVideoUtilEncodeNoSound(String id);
	/** Print debug messages of appsUtil (default: false)*/
	public abstract Setting<Boolean> getAppsDebug();
	
	// videoplatform
	/** Host to connect (default: gem-uri) */
	public abstract Setting<String> getVideoplatformHost();
	/** Port to use at host (default: 8980) */
	public abstract Setting<Integer> getVideoPlatformPort();
	/** Path to use for finished task (default: "tasks/finished") */
	public abstract Setting<String> getVideoPlatformPathFinished();
	/** Path to use for new task (default: "tasks/started") */
	public abstract Setting<String> getVideoPlatformPathNew();
	/** Path to use for resolving video by name (default: "tm/videoByName")*/
	public abstract Setting<String> getVideoPlatformPathVideoByName();
	/** Videoplatform Notifier enabled (default: false) */
	public abstract Setting<Boolean> getVideoplatformNotifierEnabled();
	/** Videoplatform Resolver enabled (default: true) */
	public abstract Setting<Boolean> getVideoplatformResolverEnabled();
	
	// command ui
	/** Print debug messages of videoTranscodingserver.ui (default: true)*/
	public abstract Setting<Boolean> getUiDebug();
	/** Path to first video to use in cui (default: "/home/linaro/store/videos/sintel2048_7_1.mp4")*/
	public abstract Setting<String> getCuiVideo1();
	/** Path to second video to use in cui (default: "/home/linaro/store/videos/Route66_27_1.avi")*/
	public abstract Setting<String> getCuiVideo2();
	/** Path to a file containing a serialized form a task for the call of cui2 (default: "/home/linaro/store/theatre-slave/cui2TaskConfig.txt")*/
	public abstract Setting<String> getCui2TaskConfigPath();
	/** Path to a file containing the properties for the call of cui2 (default: "/home/linaro/store/theatre-slave/cui2CallConfig.properties")*/
	public abstract Setting<String> getCui2CallConfigPath();
	
	// slave-test
	public abstract Setting<Boolean> runScalingFfmepg();
	public abstract Setting<Boolean> runScalingMencoder();
	public abstract Setting<Boolean> runScalingHandbrake();

	public abstract Setting<Boolean> runTranscoderFfmepg();
	public abstract Setting<Boolean> runTranscoderMencoder();
	public abstract Setting<Boolean> runTranscoderHandbrake();

	public abstract Setting<Boolean> runPipFfmepg();

	public abstract Setting<String> videoRegexName();
	public abstract Setting<String> firstVideoName();
	public abstract Setting<String> secondVideoName();

	public abstract Setting<Integer> scalingWidth();
	public abstract Setting<Integer> scalingHeight();
	public abstract Setting<String> transcoderVCodec();
	public abstract Setting<String> transcoderACodec();
	public abstract Setting<Integer> pipPos();
	
}
