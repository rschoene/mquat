/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util.monitor;

import org.haec.theatre.monitor.MonitorRepository;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Adapter for a real monitor measuring "cpu_time"
 * @author René Schöne
 */
public class LocalTimeMonitor extends LocalMonitor {
	String diff;
	
	public LocalTimeMonitor(BundleContext context) {
		ServiceReference<MonitorRepository> ref = context.getServiceReference(MonitorRepository.class);
		if(ref == null) {
			throw new RuntimeException("Monitor-Repository unavailable");
		}
		MonitorRepository repo = context.getService(ref);
		delegatee = repo.getMonitor("cpu_time");
	}
	
	@Override
	public void before() {
		delegatee.collectBefore();
	}

	@Override
	public void after() {
		delegatee.collectAfter(null, null);
		delegatee.compute();
		diff = getDelegateeDiff();
	}

	@Override
	public String getDiff() {
		return diff;
	}

	@Override
	public String getParameterName() {
		return "cpu_time";
	}
}