/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util.monitor;

/**
 * Composite monitor, i.e. a monitor having child monitors.
 * @author René Schöne
 */
public class LocalCompositeMonitor extends LocalMonitor {

	LocalMonitor[] monitors;
	int currentSize;
	String delimiter;
	private String pnDelimiter;
	
	private LocalCompositeMonitor(String delimiter) {
		setDelimiter(delimiter);
		pnDelimiter = ";"; // default
	}
	
	public LocalCompositeMonitor(String delimiter, LocalMonitor... fixedMonitors) {
		this(delimiter);
		monitors = fixedMonitors;
		currentSize = fixedMonitors.length;
	}
	
	public LocalCompositeMonitor(String delimiter, int size) {
		this(delimiter);
		monitors = new LocalEnergyMonitor[size];
		currentSize = 0;
	}
	
	/** @return <code>true</code> iff the monitor has been added */
	public boolean addMonitor(LocalMonitor monitor) {
		if(currentSize == monitors.length)
			return false;
		monitors[currentSize++] = monitor;
		return true;
	}
	
	public String getDelimiter() {
		return delimiter;
	}
	
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	
	public String getParameterNameDelimiter() {
		return pnDelimiter;
	}
	
	public void setParameterNameDelimiter(String pnDelimiter) {
		this.pnDelimiter = pnDelimiter;
	}
	
	@Override
	public void before() {
		for(LocalMonitor m : monitors)
			m.before();
	}

	@Override
	public void after() {
		for(LocalMonitor m : monitors)
			m.after();
	}

	@Override
	public String getDiff() {
		StringBuilder sb = new StringBuilder();
		for(LocalMonitor m : monitors)
			sb.append(m.getDiff()).append(getDelimiter());
		// cut last delimiter
		return sb.substring(0, sb.length()-getDelimiter().length());
	}

	@Override
	public String getParameterName() {
		StringBuilder sb = new StringBuilder();
		for(LocalMonitor m : monitors)
			sb.append(m.getParameterName()).append(getParameterNameDelimiter());
		// cut last delimiter
		return sb.substring(0, sb.length()-getParameterNameDelimiter().length());
	}
	
}