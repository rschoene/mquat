/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

/**
 * Simple representation for the resolution of a video.
 * @author René Schöne
 */
public class VideoResolution {
	
	public final int width;
	public final int height;
	
	public VideoResolution(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}
	
	@Override
	public String toString() {
		return this.width + "x" + this.height;
	}
	/** new VideoResolution(-1, -1) */
	public static final VideoResolution UNKNOWN = new VideoResolution(-1, -1);
	/** new VideoResolution(1024, 768) */
	public static final VideoResolution DVD = new VideoResolution(1024, 768);
	/** new VideoResolution(1920, 1080) */
	public static final VideoResolution HD = new VideoResolution(1920, 1080);

}
