/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import java.io.File;

/**
 * Utility class able to compute the duration of a video.
 * @author René Schöne
 */
public interface VideoDurationCalculator {

	/**
	 * Takes a video file as input and returns its length as VideoDuration object.
	 * @param video the input video file
	 * @return the duration in millisecond precision
	 */
	public abstract VideoDuration getDurationOfVideo(File video);

}
