/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.coolsoftware.theatre.Resettable;

/**
 * Utility class able to create processes and handle their streams in the context of video processing.
 * @author René Schöne
 */
public interface VideoUtil {
	
	/** Use temporary files (i.e. with deleteOnExit) for all video utils? */
	public static final String TEMPORARY_FILES = "haec.videoutil.temporary";
	
	/** Merge error and output stream from all video utils processes? */
	public static final String MERGE_ERROR = "haec.videoutil.merge.error";
	
	/** Whether to ignore requests to all video utils and return first input file. */
	public static final String PASS = "haec.videoutil.pass";
	
	/** Whether to ignore audio in all video utils requests (May be not supported by every subclass). */
	public static final String NOSOUND = "haec.videoutil.nosound";
	
	/** Use temporary files (i.e. with deleteOnExit)? */
	public boolean shouldUseTemporaryFiles();
	
	/** Merge error and output stream? */
	public boolean shouldMergeError();
	
	/** Whether to ignore requests and return first input file. */
	public boolean shouldPass();
	
	/** Whether to ignore audio requests (May be not supported by every subclass). */
	public boolean shouldEncodeNoSound();

	/**
	 * Returns a newly created temporary file in a subfolder of the workspace directory.
	 * The subfolder is called <i>haec</i>.
	 * The default extension (tmp) is used.
	 * @return the new fily
	 * @throws IOException If the file could not be created
	 * @see File#createTempFile(String, String, File)
	 */
	public File getTemporaryFile() throws IOException;

	/**
	 * Returns a newly created temporary file in a subfolder of the workspace directory.
	 * The subfolder is called <i>haec</i>.
	 * The extension of the given file is used.
	 * @return the new fily
	 * @throws IOException If the file could not be created
	 * @see File#createTempFile(String, String, File)
	 */
	public File getTemporaryFile(File input) throws IOException;

	/**
	 * Returns a newly created temporary file in a subfolder of the workspace directory
	 * with the given extension.
	 * The subfolder is called <i>haec</i>.
	 * @param extension the file extension
	 * @return the new fily
	 * @throws IOException If the file could not be created
	 * @see File#createTempFile(String, String, File)
	 */
	public File getTemporaryFile(String extension) throws IOException;

	/**
	 * Creates and starts a new Process with the given arguments. The first argument passed (or the first
	 * value of the field) should be a valid command name. The global value for merging streams is used.
	 * @param args the arguments to use including the command name
	 * @return the started process
	 * @throws IOException if an I/O Error occurs
	 * @see ProcessBuilder#start()
	 */
	public Process createProcess(String... args) throws IOException;

	/**
	 * Creates and starts a new Process with the given arguments. The first argument passed (or the first
	 * value of the field) should be a valid command name. The global value for merging streams is used.
	 * @param args the arguments to use including the command name
	 * @return the started process
	 * @throws IOException if an I/O Error occurs
	 * @see ProcessBuilder#start()
	 */
	public Process createProcess(List<String> args) throws IOException;

	/**
	 * Creates and starts a new Process with the given arguments. The first argument passed (or the first
	 * value of the field) should be a valid command name.
	 * @param args the arguments to use including the command name
	 * @param mergeError <code>true</code> to merge output and error stream
	 * @return the started process
	 * @throws IOException if an I/O Error occurs
	 * @see ProcessBuilder#start()
	 */
	public Process createProcess(boolean mergeError, String... args)
			throws IOException;

	/**
	 * Waits for the given process to end. Restarts waiting if interrupted.
	 * @param p the process to wait for
	 * @param handleStreams should the streams be handled?
	 * @return the exit value returned by {@link Process#waitFor()}.
	 */
	public int waitForever(Process p, boolean handleStreams);

	/**
	 * Prints the given stream to {@link System#out}.
	 * @param is the input stream to print
	 */
	public void printStream(InputStream is);

	/**
	 * Logs the given stream to DEBUG.
	 * @param is the input stream to log
	 */
	public void logStream(InputStream is);
	
	/**TODO*/
	public ErrorState createErrorState();

	/**
	 * @return <code>true</code> if this utility is available, e.g. if all libraries are available
	 */
	public boolean isAvailable();
	
	/**
	 * Kills all process created by this util and are still running.
	 */
	public void killRemainingProcesses();
}