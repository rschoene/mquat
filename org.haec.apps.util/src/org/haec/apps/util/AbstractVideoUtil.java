/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.StringUtils;
import org.haec.theatre.utils.WeakProcessList;
import org.haec.theatre.utils.settings.Setting;
import org.osgi.service.component.ComponentContext;

/**
 * Abstract superclass for each VideoUtil providing process and process stream handling methods.
 * @author René Schöne
 */
public abstract class AbstractVideoUtil implements VideoUtil {

//	public final File tempDir;
	private Setting<Boolean> globalMergeError;
	private Setting<Boolean> useTemporaryFiles;
	private Setting<Boolean> pass;
	private Setting<Boolean> encodeNoSound;
	
	private final Logger log = Logger.getLogger(AbstractVideoUtil.class);
	
	protected String command;
	/** bin + command */
	protected String processCommand;
	protected final WeakProcessList processList;
	private VideoSettings settings;
	private String commandId;
	
	/**
	 * Creates a new Utility object to create processes. Subclasses should provide a shared
	 * instance static field to ease access and avoid unnecessary creation of such utility
	 * objects.
	 * @param command the command to invoke, i.e. the binary name
	 * @param bin directory of the binary
	 * @param useTemporaryFiles delete created files on exit
	 * @param globalMergeError merge error and output stream
	 * @param pass ignore requests and just return the first input file
	 */
	public AbstractVideoUtil(String commandId) {
		this.commandId = commandId;
		this.processList = new WeakProcessList();
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				// don't use logging, as may already be shut down
				try {
					System.out.println("Kill all remaining processes");
					killRemainingProcesses();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}
	
	@Override
	public boolean shouldEncodeNoSound() {
		return encodeNoSound.get();
	}
	
	@Override
	public boolean shouldMergeError() {
		return globalMergeError.get();
	}
	
	@Override
	public boolean shouldPass() {
		return pass.get();
	}
	
	@Override
	public boolean shouldUseTemporaryFiles() {
		return useTemporaryFiles.get();
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#getTemporaryFile()
	 */
	@Override
	public File getTemporaryFile() throws IOException {
		return getTemporaryFile("tmp");
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#getTemporaryFile(java.io.File)
	 */
	@Override
	public File getTemporaryFile(File input) throws IOException {
		String name = input.getName();
		String extension = name.substring(name.lastIndexOf('.')+1);
		return getTemporaryFile(extension);
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#getTemporaryFile(java.lang.String)
	 */
	@Override
	public File getTemporaryFile(String extension) throws IOException {
		File result = File.createTempFile("haec.tmpVideo.", "."+extension);
		if(shouldUseTemporaryFiles())
			result.deleteOnExit();
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#createProcess(java.lang.String)
	 */
	@Override
	public Process createProcess(String... args) throws IOException {
		return createProcess(shouldMergeError(), args);
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#createProcess(java.util.List)
	 */
	@Override
	public Process createProcess(List<String> args) throws IOException {
		return createProcess(shouldMergeError(), args.toArray(new String[args.size()]));
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#createProcess(boolean, java.lang.String)
	 */
	@Override
	public Process createProcess(boolean mergeError, String... args) throws IOException {
		String[] command;
		if(args == null || args.length == 0)
			command = new String[1];
		else {
			String[] additionalCommands = getAdditionalCommands();
			command = new String[args.length+1+additionalCommands.length];
			System.arraycopy(args, 0, command, 1, args.length);
			int additionalLength = additionalCommands.length;
			System.arraycopy(additionalCommands, 0, command, command.length-additionalLength, additionalLength);
		}
		command[0] = processCommand;
		log.info("command: " + StringUtils.join(" ", command));
		ProcessBuilder pb = new ProcessBuilder(command);
		Process p = pb.redirectErrorStream(mergeError).start();
		processList.add(p);
		return p;
	}
	
	/**
	 * @return an array of additional commands appended at the end of each process invocation.
	 * Subclasses may override this to provide such commands. Defaults to a zero-length array.
	 */
	protected String[] getAdditionalCommands() {
		return new String[0];
	}
	
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#waitForever(java.lang.Process, boolean)
	 */
	@Override
	public int waitForever(Process p, boolean handleStreams) {
		boolean stillRunning = true;
		int result = 0;
		while(stillRunning) {
			try {
				Thread.sleep(20); // let process start
				log.info("Waiting for " + p);
				if(handleStreams) {
					handleInputStream(p.getInputStream());
					handleErrorStream(p.getErrorStream());
				}
				result = p.waitFor();
				stillRunning = false;
			} catch (InterruptedException ignore) { }
		}
		FileUtils.writeToCsv("logs/return-codes.csv",
				new SimpleDateFormat().format(Calendar.getInstance().getTime()), processCommand, result);
		return result;
	}
	
	/**
	 * Processes the error stream after {@link #waitForever(Process, boolean)}. Subclasses
	 * may override this. Default implementation consumes the stream content silently.
	 * @param error the error stream
	 */
	protected void handleErrorStream(InputStream error) { 
		consumeStream(error);
	}
	
	/**
	 * Processes the input stream after {@link #waitForever(Process, boolean)}. Subclasses
	 * may override this. Default implementation consumes the stream content silently.
	 * @param input the input stream
	 */
	protected void handleInputStream(InputStream input) { 
		consumeStream(input);
	}

	private void consumeStream(InputStream is) {
	    try { 
	        while(is.read() != -1) { }
	    } catch(IOException ignore) { }
	}
	
	interface StreamLineHandler {
		public void handleLine(String line);
	}
	class LogDebugHandler implements StreamLineHandler {

		@Override
		public void handleLine(String line) {
			L.g(line);
		}
	}
	class SysoHandler implements StreamLineHandler {
		@Override
		public void handleLine(String line) {
			System.out.println(line);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#printStream(java.io.InputStream)
	 */
	@Override
	public void printStream(InputStream is) {
		handleStream(is, new SysoHandler());
	}
	
	public void logStream(InputStream is) {
		handleStream(is, new LogDebugHandler());
	}
	
	private void handleStream(InputStream is, StreamLineHandler handler) {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			while((line = br.readLine()) != null)
				handler.handleLine(line);
		} catch (IOException ignore) { }
		if(br != null) try { br.close(); } catch(IOException ignore) { };
	}

	protected final static String IDENTIFY_KEY_OUTPUT = "!output";
	
	protected String getIdentityOutput(Map<String, List<String>> result) {
		return result.get(IDENTIFY_KEY_OUTPUT).get(0);
	}

	/**
	 * Calls mplayer with optimized options for the given file. Searches the
	 * output for a given string and returns a map containing the last match for each string
	 * (just the content of the line after the searchString), or <code>null</code> if there was
	 * no such line.
	 * @param pb process builder to start process
	 * @param searchString the string to search for in the result
	 * @return a map containing the rest of the first line matching the searchString,
	 *  or <code>null</code> if no such line, for each searchString
	 */
	protected Map<String, List<String>> identify(ProcessBuilder pb, final String... searchStrings)
			throws IOException {
		Map<String, List<String>> result = new HashMap<String, List<String>>(searchStrings.length);
		Process p;
		p = pb.start();
		InputStream lsOut = p.getInputStream();
		InputStreamReader isr = new InputStreamReader(lsOut);
		BufferedReader in = new BufferedReader(isr);
		boolean emptyStream = false;

		StringBuilder sb = new StringBuilder();
		try {
			// parsing output to find info for duration:
			String line;
			while ((line = in.readLine()) != null)
			{
				emptyStream = false;
				for(String searchString : searchStrings) {
					int index = line.indexOf(searchString);
					if(index != -1)
					{
						addToMap(result, searchString, line.substring(index + searchString.length()));
					}
				}
				sb.append(line);
				sb.append('\n');
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(emptyStream) {
			System.err.println("Empty stream for video while identifying.");
		}
		result.put(IDENTIFY_KEY_OUTPUT, Collections.singletonList(sb.toString()));
		return result;
	}

	private void addToMap(Map<String, List<String>> result, String searchString,
			String line) {
		List<String> list = result.get(searchString);
		if(list == null) {
			list = new ArrayList<String>();
			result.put(searchString, list);
		}
		list.add(line);
	}

	protected static <T extends VideoResolutionCalculator & VideoDurationCalculator> void test(
			T object, File file) {
				VideoResolution resolution = object.getVideoResolution(file);
				VideoDuration duration = object.getDurationOfVideo(file);
				System.out.println("Res: " + resolution);
				System.out.println("Dur: " + duration);
			}
	
	@Override
	public boolean isAvailable() {
		// quick and dirty test with "which" command
		ProcessBuilder pb = new ProcessBuilder("which", this.processCommand);
		Process p;
		try {
			p = pb.start();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		int nextOutputByte;
		int nextErrorByte;
		try {
			nextOutputByte = p.getInputStream().read();
		} catch (IOException e1) {
			e1.printStackTrace();
			// problems reading input indicate that which is not working properly
			// no information on availability of command, assume false
			return false;
		}
		try {
			nextErrorByte = p.getErrorStream().read();
		} catch (IOException e) {
			e.printStackTrace();
			// problems reading error indicate that which is not working properly
			// no information on availability of command, assume false
			return false;
		}
		return nextOutputByte != -1 && nextErrorByte == -1;
	}

	/* (non-Javadoc)
	 * @see org.haec.apps.util.VideoUtil#killRemainingProcesses()
	 */
	@Override
	public void killRemainingProcesses() {
		processList.killAllRunning();
	}
	
	protected void setVideoSettings(VideoSettings vs) {
		this.settings = vs;
		this.useTemporaryFiles = settings.getVideoutilTemporaryFiles(commandId);
		this.globalMergeError  = settings.getVideoutilMergeError(commandId);
		this.pass = settings.getVideoutilPass(commandId);
		this.encodeNoSound = settings.getVideoUtilEncodeNoSound(commandId);
		this.processCommand = settings.getVideoUtilBin(commandId).get();
	}

	protected void unsetVideoSettings(VideoSettings vs) {
		this.settings = null;
	}

	
	protected void activate(ComponentContext ctx) {
		
	}
}
