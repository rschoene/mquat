/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import java.util.Collections;
import java.util.List;

import org.haec.theatre.api.Benchmark;
import org.haec.theatre.utils.UtilsActivator;

/**
 * Helper class simulating benchmarks by simply waiting a pre-specified time.
 * @author René Schöne
 */
public abstract class FakeBenchmark implements Benchmark {
	
	private static final int DEFAUT_MIN_VALUE_METAPARAM = 1;
	
	private static final int DEFAUT_MAX_VALUE_METAPARAM = 6;
	
	private static final int DEFAUT_MILLISECONDS_TO_WAIT_PER_VALUE = 500;
	
	protected final boolean fake;
	protected int metaparam;
	protected int minValueMetaparam = DEFAUT_MIN_VALUE_METAPARAM;
	protected int maxValueMetaparam = DEFAUT_MAX_VALUE_METAPARAM;
	protected long milliseondsToWaitPerValue = DEFAUT_MILLISECONDS_TO_WAIT_PER_VALUE;

	public FakeBenchmark() {
		fake = UtilsActivator.fakeBenchmark.get();
	}
	
	public FakeBenchmark(long milliseondsToWaitPerValue) {
		this();
		this.milliseondsToWaitPerValue = milliseondsToWaitPerValue;
	}
	
	public FakeBenchmark(int minValueMetaparam, int maxValueMetaparam) {
		this();
		this.minValueMetaparam = minValueMetaparam;
		this.maxValueMetaparam = maxValueMetaparam;
	}
	
	public FakeBenchmark(long milliseondsToWaitPerValue, int minValueMetparam, int maxValueMetparam) {
		this();
		this.milliseondsToWaitPerValue = milliseondsToWaitPerValue;
		this.minValueMetaparam = minValueMetparam;
		this.maxValueMetaparam = maxValueMetparam;
	}
	
	@Override
	public void benchmark() {
		if(fake) {
			System.out.println(getClass().getName() + " = Faking benchmarks");
			for(metaparam = minValueMetaparam; metaparam < maxValueMetaparam; metaparam++)
				runIteration(getMetaparams(), metaparam == 0);
		}
		else {
			realBenchmark();
		}
	}

	/**
	 * Subclasses may override this to set the "correct" value of the meta parameters.
	 * @return a list containing the metaparameters for the current {@link #metaparam}
	 */
	protected List<Integer> getMetaparams() {
		return Collections.singletonList(metaparam);
	}
	
	/**
	 * @see #benchmark()
	 */
	protected abstract void realBenchmark();

	@Override
	protected void iteration() {
		if(fake) {
			// use busy waiting to actually spend time in the thread
			long waitUntil = System.currentTimeMillis() + metaparam*milliseondsToWaitPerValue;
			double tmp;
			while(System.currentTimeMillis() < waitUntil) {
				// just random computation
				tmp = Math.random();
				tmp *= tmp;
			}
		}
		else {
			realIteration();
		}
	}
	
	/**
	 * @see #iteration()
	 */
	protected abstract void realIteration();

}
