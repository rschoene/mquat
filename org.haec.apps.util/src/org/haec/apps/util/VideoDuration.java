/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import java.util.concurrent.TimeUnit;

/**
 * Simple representation for the duration of a video.
 * @author René Schöne
 */
public class VideoDuration {
	
	public int hours;
	public int minutes;
	public double seconds;
	
	public VideoDuration() {
	}
	
	public VideoDuration(int hours, int minutes, double seconds) {
		this.hours   = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
	
	@Override
	public String toString() {
		return hours + ":" + minutes + ":" + seconds;
	}
	
	public long roundToSeconds() {
		return (long) seconds
				+ TimeUnit.MINUTES.toSeconds(minutes)
				+ TimeUnit.HOURS.toSeconds(hours);
	}
	
	public static VideoDuration getMax(VideoDuration duration1, VideoDuration duration2) {
		if(duration1.hours > duration2.hours)
			return duration1;
		if(duration1.hours == duration2.hours) {
			if(duration1.minutes > duration2.minutes)
				return duration1;
			if(duration1.minutes == duration2.minutes
					&& duration1.seconds > duration2.seconds)
				return duration1;
			else
				return duration2;
		}
		return duration2;
		
	}
	
	public static final VideoDuration UNKNOWN = new VideoDuration(0, 0, 0f);
}