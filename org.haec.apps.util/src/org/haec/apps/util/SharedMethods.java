/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.apps.util;

import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.haec.theatre.utils.StringUtils;

/**
 * Utility class providing methods to compute video resolution and duration assuming simliar output as ffmpeg.
 * @author René Schöne
 */
public class SharedMethods {
	
	private final static String searchVideo  = ": Video:";
	private final static String searchAudio  = ": Audio:";
	private static Logger log = Logger.getLogger(SharedMethods.class);
	
	public static VideoResolution getVideoResolutionFfmpegHandbrake(List<String> list) {
		VideoResolution result = VideoResolution.UNKNOWN;
		if(list == null) {
			// return unknown
			return result;
		}
		for(String line: list) {
			int index = line.indexOf(searchVideo);
			if(index > -1) {
				String subString = line.substring(index + searchVideo.length());
				boolean skip = true; /* skip first token (contains codec and maybe also "x") */
				for(String tok : StringUtils.tokenize(subString, ",")) {
					if(skip) { skip = false; continue; }
					log.debug("tok = '" + tok + "'");
					int xIndex = tok.indexOf('x');
					if(xIndex > -1) {
						String width  = toNumberString(tok.substring(0, xIndex));
						String height = toNumberString(tok.substring(xIndex+1));
						log.debug("width = '" + width + "', height = '" + height + "'");
						result = new VideoResolution(Integer.parseInt(width), Integer.parseInt(height));
						break;
					}
				}
				if(result != VideoResolution.UNKNOWN) {
					break;
				}
			}
		}
		return result;
	}

    public static String toNumberString(String str) {
        System.out.println("str= '" + str + "'");
		int length = str.length();
		int cutOffFirst = 0, cutOffLast = str.length();
		char c;
		boolean digitFound = false;
		for(int i=0; i<length; i++) {
			c = str.charAt(i);
			if(!Character.isDigit(c)) {
				if(digitFound) {
					// found a non-digit after a number -> cut off rest
					cutOffLast = i;
					break;
				}
			}
			else {
                if(!digitFound) {
                    cutOffFirst = i;
	    			digitFound = true;
                }
			}
		}
		return str.substring(cutOffFirst, cutOffLast);
	}

	public static VideoDuration getVideoDurationFfmpegHandbrake(List<String> list) {
		VideoDuration result = VideoDuration.UNKNOWN;
		if(list == null) {
			// return unknown
			return result;
		}
		for(String line : list) {
			// format e.g. "Duration: 00:00:03.16, start: 0.000000, bitrate: 717 kb/s" without the matched "Duration: "
			line = line.substring(0, line.indexOf(','));
			StringTokenizer st = new StringTokenizer(line, ":");
			if(st.countTokens() == 3) {
				String hour = st.nextToken();
				String min  = st.nextToken();
				String sec  = st.nextToken();
				result = new VideoDuration(Integer.parseInt(hour), Integer.parseInt(min), Float.parseFloat(sec));
				break;
			}
		}
		return result;
	}
	
	public static String getVideoCodec(List<String> list) {
		if(list == null) {
			return "null";
		}
		for(String line: list) {
			int index = line.indexOf(searchVideo);
			if(index > -1) {
				int commaIndex = line.indexOf(",", index);
				if(commaIndex == -1) { commaIndex = line.length(); }
				String codecString = line.substring(index + searchVideo.length(), commaIndex);
				return codecString.trim();
			}
		}
		return "not found";
	}
	
	public static String getAudioCodec(List<String> list) {
		if(list == null) {
			return "null";
		}
		for(String line: list) {
			int index = line.indexOf(searchAudio);
			if(index > -1) {
				int commaIndex = line.indexOf(",", index);
				if(commaIndex == -1) { commaIndex = line.length(); }
				String codecString = line.substring(index + searchAudio.length(), commaIndex);
				return codecString.trim();
			}
		}
		return "not found";
	}

}
