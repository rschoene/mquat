package org.haec.app.sort;

import org.haec.theatre.api.App;

/**
 * "Shortcut application" only containing the sort component.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class SortApp implements App {

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getName()
	 */
	@Override
	public String getName() {
		return "org.haec.app.composedsort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getApplicationModelPath()
	 */
	@Override
	public String getApplicationModelPath() {
		return "models/sort.structure";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getContractsPath()
	 */
	@Override
	public String getContractsPath() {
		return "models/contracts";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.App#getBundleSymbolicName()
	 */
	@Override
	public String getBundleSymbolicName() {
		return "org.haec.app.sort";
	}

}
