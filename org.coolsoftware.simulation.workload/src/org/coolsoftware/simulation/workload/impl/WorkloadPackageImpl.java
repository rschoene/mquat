/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.simulation.workload.impl;

import org.coolsoftware.coolcomponents.ccm.CcmPackage;
import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorPackage;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPackage;
import org.coolsoftware.coolcomponents.nameable.NameablePackage;
import org.coolsoftware.simulation.workload.CcmImport;
import org.coolsoftware.simulation.workload.WorkloadDescription;
import org.coolsoftware.simulation.workload.WorkloadFactory;
import org.coolsoftware.simulation.workload.WorkloadPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WorkloadPackageImpl extends EPackageImpl implements WorkloadPackage {
	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass workloadDescriptionEClass = null;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private EClass ccmImportEClass = null;

	/**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.coolsoftware.simulation.workload.WorkloadPackage#eNS_URI
   * @see #init()
   * @generated
   */
	private WorkloadPackageImpl() {
    super(eNS_URI, WorkloadFactory.eINSTANCE);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private static boolean isInited = false;

	/**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link WorkloadPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
	public static WorkloadPackage init() {
    if (isInited) return (WorkloadPackage)EPackage.Registry.INSTANCE.getEPackage(WorkloadPackage.eNS_URI);

    // Obtain or create and register package
    WorkloadPackageImpl theWorkloadPackage = (WorkloadPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof WorkloadPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new WorkloadPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    CcmPackage.eINSTANCE.eClass();
    NameablePackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theWorkloadPackage.createPackageContents();

    // Initialize created meta-data
    theWorkloadPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theWorkloadPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(WorkloadPackage.eNS_URI, theWorkloadPackage);
    return theWorkloadPackage;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getWorkloadDescription() {
    return workloadDescriptionEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EReference getWorkloadDescription_Workload() {
    return (EReference)workloadDescriptionEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EReference getWorkloadDescription_Imports() {
    return (EReference)workloadDescriptionEClass.getEStructuralFeatures().get(1);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EClass getCcmImport() {
    return ccmImportEClass;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EReference getCcmImport_Resref() {
    return (EReference)ccmImportEClass.getEStructuralFeatures().get(0);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public WorkloadFactory getWorkloadFactory() {
    return (WorkloadFactory)getEFactoryInstance();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private boolean isCreated = false;

	/**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void createPackageContents() {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    workloadDescriptionEClass = createEClass(WORKLOAD_DESCRIPTION);
    createEReference(workloadDescriptionEClass, WORKLOAD_DESCRIPTION__WORKLOAD);
    createEReference(workloadDescriptionEClass, WORKLOAD_DESCRIPTION__IMPORTS);

    ccmImportEClass = createEClass(CCM_IMPORT);
    createEReference(ccmImportEClass, CCM_IMPORT__RESREF);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	private boolean isInitialized = false;

	/**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void initializePackageContents() {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    BehaviorPackage theBehaviorPackage = (BehaviorPackage)EPackage.Registry.INSTANCE.getEPackage(BehaviorPackage.eNS_URI);
    VariantPackage theVariantPackage = (VariantPackage)EPackage.Registry.INSTANCE.getEPackage(VariantPackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(workloadDescriptionEClass, WorkloadDescription.class, "WorkloadDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getWorkloadDescription_Workload(), theBehaviorPackage.getWorkload(), null, "workload", null, 1, 1, WorkloadDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getWorkloadDescription_Imports(), this.getCcmImport(), null, "imports", null, 0, -1, WorkloadDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ccmImportEClass, CcmImport.class, "CcmImport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCcmImport_Resref(), theVariantPackage.getResource(), null, "resref", null, 1, 1, CcmImport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //WorkloadPackageImpl
