/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.simulation.workload.impl;

import org.coolsoftware.coolcomponents.ccm.variant.Resource;

import org.coolsoftware.simulation.workload.CcmImport;
import org.coolsoftware.simulation.workload.WorkloadPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ccm Import</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.simulation.workload.impl.CcmImportImpl#getResref <em>Resref</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CcmImportImpl extends EObjectImpl implements CcmImport {
	/**
   * The cached value of the '{@link #getResref() <em>Resref</em>}' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getResref()
   * @generated
   * @ordered
   */
	protected Resource resref;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected CcmImportImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	protected EClass eStaticClass() {
    return WorkloadPackage.Literals.CCM_IMPORT;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public Resource getResref() {
    if (resref != null && resref.eIsProxy())
    {
      InternalEObject oldResref = (InternalEObject)resref;
      resref = (Resource)eResolveProxy(oldResref);
      if (resref != oldResref)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, WorkloadPackage.CCM_IMPORT__RESREF, oldResref, resref));
      }
    }
    return resref;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public Resource basicGetResref() {
    return resref;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setResref(Resource newResref) {
    Resource oldResref = resref;
    resref = newResref;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.CCM_IMPORT__RESREF, oldResref, resref));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID)
    {
      case WorkloadPackage.CCM_IMPORT__RESREF:
        if (resolve) return getResref();
        return basicGetResref();
    }
    return super.eGet(featureID, resolve, coreType);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eSet(int featureID, Object newValue) {
    switch (featureID)
    {
      case WorkloadPackage.CCM_IMPORT__RESREF:
        setResref((Resource)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eUnset(int featureID) {
    switch (featureID)
    {
      case WorkloadPackage.CCM_IMPORT__RESREF:
        setResref((Resource)null);
        return;
    }
    super.eUnset(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public boolean eIsSet(int featureID) {
    switch (featureID)
    {
      case WorkloadPackage.CCM_IMPORT__RESREF:
        return resref != null;
    }
    return super.eIsSet(featureID);
  }

} //CcmImportImpl
