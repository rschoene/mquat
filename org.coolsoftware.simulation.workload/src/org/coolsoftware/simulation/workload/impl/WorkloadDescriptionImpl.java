/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.simulation.workload.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.ccm.behavior.Pin;
import org.coolsoftware.coolcomponents.ccm.behavior.Workload;

import org.coolsoftware.simulation.workload.CcmImport;
import org.coolsoftware.simulation.workload.WorkloadDescription;
import org.coolsoftware.simulation.workload.WorkloadPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.simulation.workload.impl.WorkloadDescriptionImpl#getWorkload <em>Workload</em>}</li>
 *   <li>{@link org.coolsoftware.simulation.workload.impl.WorkloadDescriptionImpl#getImports <em>Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class WorkloadDescriptionImpl extends EObjectImpl implements WorkloadDescription {
	/**
   * The cached value of the '{@link #getWorkload() <em>Workload</em>}' containment reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getWorkload()
   * @generated
   * @ordered
   */
	protected Workload workload;

	/**
   * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getImports()
   * @generated
   * @ordered
   */
	protected EList<CcmImport> imports;

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected WorkloadDescriptionImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	protected EClass eStaticClass() {
    return WorkloadPackage.Literals.WORKLOAD_DESCRIPTION;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public Workload getWorkload() {
    return workload;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public NotificationChain basicSetWorkload(Workload newWorkload, NotificationChain msgs) {
    Workload oldWorkload = workload;
    workload = newWorkload;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD, oldWorkload, newWorkload);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setWorkload(Workload newWorkload) {
    if (newWorkload != workload)
    {
      NotificationChain msgs = null;
      if (workload != null)
        msgs = ((InternalEObject)workload).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD, null, msgs);
      if (newWorkload != null)
        msgs = ((InternalEObject)newWorkload).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD, null, msgs);
      msgs = basicSetWorkload(newWorkload, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD, newWorkload, newWorkload));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EList<CcmImport> getImports() {
    if (imports == null)
    {
      imports = new EObjectContainmentEList<CcmImport>(CcmImport.class, this, WorkloadPackage.WORKLOAD_DESCRIPTION__IMPORTS);
    }
    return imports;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID)
    {
      case WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD:
        return basicSetWorkload(null, msgs);
      case WorkloadPackage.WORKLOAD_DESCRIPTION__IMPORTS:
        return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID)
    {
      case WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD:
        return getWorkload();
      case WorkloadPackage.WORKLOAD_DESCRIPTION__IMPORTS:
        return getImports();
    }
    return super.eGet(featureID, resolve, coreType);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
    switch (featureID)
    {
      case WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD:
        setWorkload((Workload)newValue);
        return;
      case WorkloadPackage.WORKLOAD_DESCRIPTION__IMPORTS:
        getImports().clear();
        getImports().addAll((Collection<? extends CcmImport>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eUnset(int featureID) {
    switch (featureID)
    {
      case WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD:
        setWorkload((Workload)null);
        return;
      case WorkloadPackage.WORKLOAD_DESCRIPTION__IMPORTS:
        getImports().clear();
        return;
    }
    super.eUnset(featureID);
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public boolean eIsSet(int featureID) {
    switch (featureID)
    {
      case WorkloadPackage.WORKLOAD_DESCRIPTION__WORKLOAD:
        return workload != null;
      case WorkloadPackage.WORKLOAD_DESCRIPTION__IMPORTS:
        return imports != null && !imports.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //WorkloadDescriptionImpl
