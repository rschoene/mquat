/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.simulation.workload;

import org.coolsoftware.coolcomponents.ccm.variant.Resource;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ccm Import</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.simulation.workload.CcmImport#getResref <em>Resref</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.simulation.workload.WorkloadPackage#getCcmImport()
 * @model
 * @generated
 */
public interface CcmImport extends EObject {
	/**
   * Returns the value of the '<em><b>Resref</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Resref</em>' reference.
   * @see #setResref(Resource)
   * @see org.coolsoftware.simulation.workload.WorkloadPackage#getCcmImport_Resref()
   * @model required="true"
   * @generated
   */
	Resource getResref();

	/**
   * Sets the value of the '{@link org.coolsoftware.simulation.workload.CcmImport#getResref <em>Resref</em>}' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Resref</em>' reference.
   * @see #getResref()
   * @generated
   */
	void setResref(Resource value);

} // CcmImport
