/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.simulation.workload;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.simulation.workload.WorkloadFactory
 * @model kind="package"
 * @generated
 */
public interface WorkloadPackage extends EPackage {
	/**
   * The package name.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNAME = "workload";

	/**
   * The package namespace URI.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNS_URI = "http://www.cool-software.org/workload";

	/**
   * The package namespace name.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNS_PREFIX = "org.coolsoftware";

	/**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	WorkloadPackage eINSTANCE = org.coolsoftware.simulation.workload.impl.WorkloadPackageImpl.init();

	/**
   * The meta object id for the '{@link org.coolsoftware.simulation.workload.impl.WorkloadDescriptionImpl <em>Description</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.simulation.workload.impl.WorkloadDescriptionImpl
   * @see org.coolsoftware.simulation.workload.impl.WorkloadPackageImpl#getWorkloadDescription()
   * @generated
   */
	int WORKLOAD_DESCRIPTION = 0;

	/**
   * The feature id for the '<em><b>Workload</b></em>' containment reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int WORKLOAD_DESCRIPTION__WORKLOAD = 0;

	/**
   * The feature id for the '<em><b>Imports</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int WORKLOAD_DESCRIPTION__IMPORTS = 1;

	/**
   * The number of structural features of the '<em>Description</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int WORKLOAD_DESCRIPTION_FEATURE_COUNT = 2;

	/**
   * The meta object id for the '{@link org.coolsoftware.simulation.workload.impl.CcmImportImpl <em>Ccm Import</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.simulation.workload.impl.CcmImportImpl
   * @see org.coolsoftware.simulation.workload.impl.WorkloadPackageImpl#getCcmImport()
   * @generated
   */
	int CCM_IMPORT = 1;

	/**
   * The feature id for the '<em><b>Resref</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int CCM_IMPORT__RESREF = 0;

	/**
   * The number of structural features of the '<em>Ccm Import</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int CCM_IMPORT_FEATURE_COUNT = 1;


	/**
   * Returns the meta object for class '{@link org.coolsoftware.simulation.workload.WorkloadDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Description</em>'.
   * @see org.coolsoftware.simulation.workload.WorkloadDescription
   * @generated
   */
	EClass getWorkloadDescription();

	/**
   * Returns the meta object for the containment reference '{@link org.coolsoftware.simulation.workload.WorkloadDescription#getWorkload <em>Workload</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Workload</em>'.
   * @see org.coolsoftware.simulation.workload.WorkloadDescription#getWorkload()
   * @see #getWorkloadDescription()
   * @generated
   */
	EReference getWorkloadDescription_Workload();

	/**
   * Returns the meta object for the containment reference list '{@link org.coolsoftware.simulation.workload.WorkloadDescription#getImports <em>Imports</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Imports</em>'.
   * @see org.coolsoftware.simulation.workload.WorkloadDescription#getImports()
   * @see #getWorkloadDescription()
   * @generated
   */
	EReference getWorkloadDescription_Imports();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.simulation.workload.CcmImport <em>Ccm Import</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Ccm Import</em>'.
   * @see org.coolsoftware.simulation.workload.CcmImport
   * @generated
   */
	EClass getCcmImport();

	/**
   * Returns the meta object for the reference '{@link org.coolsoftware.simulation.workload.CcmImport#getResref <em>Resref</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Resref</em>'.
   * @see org.coolsoftware.simulation.workload.CcmImport#getResref()
   * @see #getCcmImport()
   * @generated
   */
	EReference getCcmImport_Resref();

	/**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
	WorkloadFactory getWorkloadFactory();

	/**
   * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
   * @generated
   */
	interface Literals {
		/**
     * The meta object literal for the '{@link org.coolsoftware.simulation.workload.impl.WorkloadDescriptionImpl <em>Description</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.simulation.workload.impl.WorkloadDescriptionImpl
     * @see org.coolsoftware.simulation.workload.impl.WorkloadPackageImpl#getWorkloadDescription()
     * @generated
     */
		EClass WORKLOAD_DESCRIPTION = eINSTANCE.getWorkloadDescription();

		/**
     * The meta object literal for the '<em><b>Workload</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference WORKLOAD_DESCRIPTION__WORKLOAD = eINSTANCE.getWorkloadDescription_Workload();

		/**
     * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference WORKLOAD_DESCRIPTION__IMPORTS = eINSTANCE.getWorkloadDescription_Imports();

		/**
     * The meta object literal for the '{@link org.coolsoftware.simulation.workload.impl.CcmImportImpl <em>Ccm Import</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.simulation.workload.impl.CcmImportImpl
     * @see org.coolsoftware.simulation.workload.impl.WorkloadPackageImpl#getCcmImport()
     * @generated
     */
		EClass CCM_IMPORT = eINSTANCE.getCcmImport();

		/**
     * The meta object literal for the '<em><b>Resref</b></em>' reference feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference CCM_IMPORT__RESREF = eINSTANCE.getCcmImport_Resref();

	}

} //WorkloadPackage
