/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.simulation.workload;

import org.coolsoftware.coolcomponents.ccm.behavior.Pin;
import org.coolsoftware.coolcomponents.ccm.behavior.Workload;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.simulation.workload.WorkloadDescription#getWorkload <em>Workload</em>}</li>
 *   <li>{@link org.coolsoftware.simulation.workload.WorkloadDescription#getImports <em>Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.simulation.workload.WorkloadPackage#getWorkloadDescription()
 * @model
 * @generated
 */
public interface WorkloadDescription extends EObject {
	/**
   * Returns the value of the '<em><b>Workload</b></em>' containment reference.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Workload</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Workload</em>' containment reference.
   * @see #setWorkload(Workload)
   * @see org.coolsoftware.simulation.workload.WorkloadPackage#getWorkloadDescription_Workload()
   * @model containment="true" required="true"
   * @generated
   */
	Workload getWorkload();

	/**
   * Sets the value of the '{@link org.coolsoftware.simulation.workload.WorkloadDescription#getWorkload <em>Workload</em>}' containment reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Workload</em>' containment reference.
   * @see #getWorkload()
   * @generated
   */
	void setWorkload(Workload value);

	/**
   * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
   * The list contents are of type {@link org.coolsoftware.simulation.workload.CcmImport}.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Imports</em>' containment reference list.
   * @see org.coolsoftware.simulation.workload.WorkloadPackage#getWorkloadDescription_Imports()
   * @model containment="true"
   * @generated
   */
	EList<CcmImport> getImports();

} // WorkloadDescription
