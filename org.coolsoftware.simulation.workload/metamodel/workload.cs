SYNTAXDEF workload
FOR <http://www.cool-software.org/workload> <workload.genmodel>
START WorkloadDescription

OPTIONS {
	reloadGeneratorModel = "true";
	//overrideManifest = "false";
	additionalDependencies = "org.eclipse.swt,org.eclipse.jface,org.coolsoftware.coolcomponents.expressions.interpreter,org.eclipse.ui";
	overridePluginXML = "false"; // is needed for popup menus and stuff
	//overrideProjectFile = "false";
	//overrideBuildProperties = "false";
	//overrideClasspath = "false";
	additionalLibraries = "lib/jcommon-1.0.16.jar,lib/jfreechart-1.0.13-swt.jar,lib/jfreechart-1.0.13.jar";
	disableDebugSupport = "true";
	disableLaunchSupport = "true";
}

TOKENS {
	DEFINE SL_COMMENT				$ '--'(~('\n'|'\r'|'\uffff'))* $ COLLECT IN comments;
	DEFINE ML_COMMENT				$ '/*'.*'*/'$ COLLECT IN comments;
}

TOKENSTYLES {
	"ML_COMMENT" COLOR #008000, ITALIC;
	"SL_COMMENT" COLOR #008000, ITALIC;
}

RULES {

	// Simple Container Element to provide container for Pins not defined in any model.
	WorkloadDescription	::= 
		imports* !0
		workload;

	CcmImport ::= 
		"import" #1 "ccm" #1 resref['[', ']'] !0; 

	// TODO Lower bound of name and items is 0!
	Behavior.Workload ::= 
		"Workload" #1 name[] #1 "for" #1 resource[] #1 "{" !1
			items+ !1
		"}";

	Behavior.Occurrence	::= 
		pin[] #1 "at" #1 time[] (#1 "for" #1 load[])?;
}