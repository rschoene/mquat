/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.task;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResultSerializer;
import org.haec.theatre.settings.TheatreSettingsImpl;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

/**
 * Simple in-memory implementation of the task interface.
 * @author René Schöne
 */
public class TaskImpl implements ITask { //, Serializable {

	/** String container; String implName; */
	private class MappingInfo { //implements Serializable {
		public final String container;
		public final String implName;
		public final long jobId;
		public final long startAtMillis;
		public MappingInfo(String container, String implName, long jobId, long startAtMillis) {
			this.container = container;
			this.implName = implName;
			this.jobId = jobId;
			this.startAtMillis = startAtMillis;
		}
	}

	public static final int SUCCESS = 0;
	public static final int ADD_MAPPING_COMP_ALREADY_REGISTRED = 1;

	private final boolean REMOVE_EMPTY_APP_MAP = true; //maybe provide system property for this

	/** appName - compName - container*/
	private final Map<String, Map<String, MappingInfo>> acc;
	private long taskId;
	private transient static Logger log = Logger.getLogger(TaskImpl.class);

	public TaskImpl() {
		acc = new HashMap<String, Map<String, MappingInfo>>();
	}

	@Override
	public int addMapping(String appName, String compName, String implName, String container, long jobId, long startAtMillis) {
		log.debug(StringUtils.join(",", appName, compName, implName, container, jobId));
		Map<String, MappingInfo> compToContainer = acc.get(appName);
		if(compToContainer == null) {
			compToContainer = new HashMap<String, MappingInfo>();
			acc.put(appName, compToContainer);
		}
		if(compToContainer.containsKey(compName)) {
			return ADD_MAPPING_COMP_ALREADY_REGISTRED;
		}
		compToContainer.put(compName, new MappingInfo(container, implName, jobId, startAtMillis));
		return SUCCESS;
	}

	@Override
	public String removeMapping(String appName, String compName) {
		Map<String, MappingInfo> compToContainer = acc.get(appName);
		if(compToContainer == null) {
			// nothing to remove
			return null;
		}
		MappingInfo mi = compToContainer.remove(compName);
		String result = mi != null ? mi.container : null;
		if(REMOVE_EMPTY_APP_MAP) {
			if(compToContainer.isEmpty()) {
				acc.remove(appName);
			}
		}
		return result;
	}

	private MappingInfo miOrNull(String appName, String compName) {
		Map<String, MappingInfo> compToContainer = acc.get(appName);
		if(compToContainer == null) {
			return null;
		}
		return compToContainer.get(compName);
	}

	@Override
	public String getContainer(String appName, String compName) {
		MappingInfo mi = miOrNull(appName, compName);
		if(mi == null) {
			return null;
		}
		return mi.container;
	}

	@Override
	public String getImplName(String appName, String compName) {
		MappingInfo mi = miOrNull(appName, compName);
		if(mi == null) {
			return null;
		}
		return mi.implName;
	}

	@Override
	public String[] getAppNames() {
		return acc.keySet().toArray(new String[acc.size()]);
	}

	@Override
	public String[] getComponentNames(String appName) {
		Map<String, MappingInfo> compToContainer = acc.get(appName);
		if(compToContainer == null) {
			return new String[0];
		}
		else {
			return compToContainer.keySet().toArray(new String[compToContainer.size()]);
		}
	}

	@Override
	public <S> S execute(Class<S> clazz, String appName, String compName, String portName, Object... params) {
		log.debug("starting");
		MappingInfo mi = miOrNull(appName, compName);
		if(mi == null) {
			errorExecute(appName, compName, "No registered components.");
			return null;
		}
		IGlobalEnergyManager gem = getGem();
		Map<String, Object> options = new HashMap<String, Object>();
		options.put(IGlobalEnergyManager.OPTION_LEM_URI, mi.container);
		options.put(IGlobalEnergyManager.OPTION_IMPL_NAME, mi.implName);
		// id is passed for propagation to called components
		options.put(IGlobalEnergyManager.OPTION_TASK_ID, getTaskId());
		options.put(IGlobalEnergyManager.OPTION_JOB_ID, mi.jobId);
		options.put(IGlobalEnergyManager.OPTION_SYNC_CALL, Boolean.TRUE);
		log.debug("before getPortobject");
		Serializable serExecResult = gem.executeCurrentImpl(appName, compName, portName, options, params);
		log.debug(serExecResult);
		ExecuteResult ee = ExecuteResultSerializer.deserialize(serExecResult);
		// assume sync execute result
		@SuppressWarnings("unchecked")
		S result = (S) ee.getResult();
		return result;
	}

	private IGlobalEnergyManager getGem() {
		String uri = TheatreSettingsImpl.currentInstance.getGemUri().get();
		BundleContext context = FrameworkUtil.getBundle(TaskImpl.class).getBundleContext();;
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
		RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
				remote, uri, IGlobalEnergyManager.class.getName());
		if(srefs != null && srefs.length == 1) {
			IGlobalEnergyManager gem = (IGlobalEnergyManager) remote.getRemoteService(srefs[0]);
			return gem;
		}
		log.error("Could not connect to GEM using uri=" + uri);
		return null;
	}

	private void errorExecute(String appName, String compName, String errorMessage) {
		log.error("Failed to execute " + appName + " " + compName + ": " + errorMessage);
	}

	public void setTaskId(long id) {
		this.taskId = id;
	}

	@Override
	public long getTaskId() {
		return taskId;
	}

	private static final String delim = ";";
	private static final String delimMap = "#";

	@Override
	public void loadFromString(String serializedForm) {
		acc.clear();
		boolean first = true;
		for(String token : StringUtils.tokenize(serializedForm, delim)) {
			if(first) {
				// load id
				setTaskId(Long.parseLong(unmask(token)));
				first = false;
			}
			else {
				// load part of acc
				int i = 0;
				String[] tmp = new String[6];
				for(String mapToken : StringUtils.tokenize(token, delimMap)) {
					tmp[i++] = unmask(mapToken);
				}
				addMapping(tmp[0], tmp[1], tmp[2], tmp[3], Long.parseLong(tmp[4]), Long.parseLong(tmp[5]));
			}
		}
		log.debug("Task loaded from " + serializedForm);
	}

	/*
	 * Serialized form = <id><delim><map>
	 * The map is String -> String -> MappingInfo (which is pair of two Strings, i.e. (String, String))
	 * <map> = appName<delimMap><compoName><delimMap><implName><delimMap><container>
	 * (non-Javadoc)
	 * @see org.haec.theatre.task.ITask#saveToString()
	 */
	@Override
	public String saveToString() {
		StringBuilder sb = new StringBuilder();
		sb.append(mask(Long.toString(this.taskId)));
		for(Entry<String, Map<String, MappingInfo>> eApp : acc.entrySet()) {
			String appName = eApp.getKey();
			for(Entry<String, MappingInfo> eComp : eApp.getValue().entrySet()) {
				String compName = eComp.getKey();
				MappingInfo mi = eComp.getValue();
				sb.append(delim).append(mask(appName))
				.append(delimMap).append(mask(compName))
				.append(delimMap).append(mask(mi.implName))
				.append(delimMap).append(mask(mi.container))
				.append(delimMap).append(mask(Long.toString(mi.jobId)))
				.append(delimMap).append(mask(Long.toString(mi.startAtMillis)));
			}
		}
		log.debug("Task serialized to " + sb.toString());
		return sb.toString();
	}

	private String unmask(String string) {
		// XXX currently no masking done
		return string;
	}

	private String mask(String string) {
		// XXX currently no masking done
		return string;
	}

	@Override
	public long getJobId(String appName, String compName) {
		MappingInfo mi = miOrNull(appName, compName);
		if(mi == null) {
			return ID_NOT_SET;
		}
		return mi.jobId;
	}

	@Override
	public long getStartTime(String appName, String compName) {
		MappingInfo mi = miOrNull(appName, compName);
		if(mi == null) {
			return START_TIME_NOT_SET;
		}
		return mi.startAtMillis;
	}
}
