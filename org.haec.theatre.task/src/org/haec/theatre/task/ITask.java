/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.task;

/**
 * The description of a task and its jobs.
 * A job is a mapping of a implementation to a container along with some information like
 * the id of the job and its start time.
 * @author René Schöne
 */
public interface ITask {
	
	public static long ID_NOT_SET = -1;
	public static long START_TIME_NOT_SET = -1;

	public abstract void setTaskId(long id);

	public abstract long getTaskId();

	/**
	 * Executes the method given by the portName for the given component on the container the component
	 * is mapped to. Params are passed the the method.
	 * @param <S> the result type
	 * @param clazz the class of the result type
	 * @param appName the name of the application
	 * @param compName the name of the component
	 * @param portName the name of the port, i.e. the method name
	 * @param params the parameters to pass to the method
	 * @return the result
	 */
	public abstract <S> S execute(Class<S> clazz, String appName, String compName, String portName,
			Object... params);

	public abstract String[] getComponentNames(String appName);

	public abstract String[] getAppNames();

	public abstract String getImplName(String appName, String compName);

	public abstract String getContainer(String appName, String compName);

	/**
	 * Removes the mapping for the given component and returns the container it was mapped to.
	 * @param appName the name of the application
	 * @param compName the name of the component to remove from the mappings
	 * @return the formerly mapped container, or <code>null</code> if there was no mapping for the component
	 */
	public abstract String removeMapping(String appName, String compName);

	/**
	 * Adds a mapping for the given component to the given container
	 * @param appName the name of the application
	 * @param compName the name of the component
	 * @param implName the name of the implementation to use
	 * @param container the uri of the container
	 * @param jobId 
	 * @return either {@link #SUCCESS} or {@link #ADD_MAPPING_COMP_ALREADY_REGISTRED} if there is already
	 * a mapping for the component
	 */
	public abstract int addMapping(String appName, String compName, String implName,
			String container, long jobId, long startAtMillis);
	
	public void loadFromString(String serializedForm);
	
	public String saveToString();
	
	public long getJobId(String appName, String compName);
	
	public long getStartTime(String appName, String compName);

}
