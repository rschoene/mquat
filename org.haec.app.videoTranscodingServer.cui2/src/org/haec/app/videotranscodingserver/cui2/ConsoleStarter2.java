/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.cui2;

import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResultSerializer;
import org.haec.app.videotranscodingserver.ui.BundleNames;
import org.haec.theatre.task.ITask;
import org.haec.theatre.task.repository.ITaskRepository;
import org.haec.theatre.utils.BundleUtils;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

/**
 * A configurable starter calling implementations directly (without former optimization).
 * @author René Schöne
 */
public class ConsoleStarter2 {
	
	private static Logger log = Logger.getLogger(ConsoleStarter2.class);

	private BundleContext context;

	public ConsoleStarter2(BundleContext context) {
		this.context = context;
		log.debug("start bundles");
		BundleUtils.startBundles(context, BundleNames.getAllBundleNames());
	}

	public ExecuteResult startRequest() {
		ITask task;
		String taskConfig;
		Properties callConfig = new Properties();
		FileReader fr = null;
		try {
			taskConfig = FileUtils.readAsString(Cui2Activator.taskConfig.get());
			fr = new FileReader(Cui2Activator.callConfig.get());
			callConfig.load(fr);
		} catch (IOException e) {
			log.warn("Error while reading configs", e);
			return null;
		}
		finally {
			if(fr != null) try { fr.close(); } catch(IOException ignore) {};
		}
		// fetch repo and create new task
		ServiceReference<ITaskRepository> ref = context.getServiceReference(ITaskRepository.class);
		ITaskRepository repo = context.getService(ref);
		task = repo.createNewTask();
		// remember id
		long id = task.getTaskId();
		
		// load config and set id
		task.loadFromString(taskConfig);
		task.setTaskId(id);
		
		// update task at repo
		repo.updateTask(id, task);
		
		callConfig.list(System.out);
		// get values for the parameters of the call
		Map<String, Object> options = new HashMap<String, Object>();
		options.put(IGlobalEnergyManager.OPTION_TASK_ID, id);
		options.put(IGlobalEnergyManager.OPTION_SYNC_CALL, Boolean.TRUE);
		String implName = checkNull(callConfig.getProperty("implName"));
		options.put(IGlobalEnergyManager.OPTION_IMPL_NAME, implName);
		
		String portName = checkNull(callConfig.getProperty("portName"));
		String appName = checkNull(callConfig.getProperty("appName"));
		String compName = checkNull(callConfig.getProperty("compName"));
		String paramsValue = checkNull(callConfig.getProperty("params"));
		String container = task.getContainer(appName, compName);
		List<Object> paramList = new ArrayList<Object>();
		for(String tok : StringUtils.tokenize(paramsValue, ";")) {
			Object val = StringUtils.parseStringParam(tok);
			paramList.add(val);
		}
		URI lemUri = URI.create(RemoteOsgiUtil.createRemoteOsgiUri(container).toString());
		log.debug(String.format("lemUri=%s, options=%s, portName=%s, appName=%s, compName=%s, paramList=%s",
				lemUri , options , portName, appName, compName, paramList));
		IGlobalEnergyManager gem = getGEM();
		Serializable result = gem.executeAtLem(lemUri , options , portName, appName, compName,
				paramList.toArray(new Object[paramList.size()]));
		
		return ExecuteResultSerializer.deserialize(result);
	}

	private <S> S checkNull(S value) {
		if(value == null) {
			throw new NullPointerException();
		}
		return value;
	}

	protected IGlobalEnergyManager getGEM() {
		IGlobalEnergyManager ret = null;
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
		
		String ip = "127.0.0.1";
		final RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
				remote, ip, IGlobalEnergyManager.class.getName());
		
		ret = (IGlobalEnergyManager)remote.getRemoteService(srefs[0]);
		return ret;
	}

}
