/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.cui2;

import java.io.File;
import java.io.Serializable;
import java.net.URI;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.settings.Setting;
import org.haec.theatre.utils.settings.SettingHolder;
import org.haec.theatre.utils.settings.StringSetting;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator to start the ConsoleStarter2.
 * @author René Schöne
 */
public class Cui2Activator extends SettingHolder implements BundleActivator {
	public static final String KEY_TASK_CONFIG_PATH = "org.haec.cui2.taskConfigPath";
	public static final String KEY_CALL_CONFIG_PATH = "org.haec.cui2.callConfigPath";
	/** Path to task config (default: "task.properties") */
	public static Setting<String> taskConfig = new StringSetting("cui2.config.task", "task.properties");
	/** Path to call config (default: "call.properties") */
	public static Setting<String> callConfig = new StringSetting("cui2.config.call", "call.properties");
	/** Global repositories in the form <code>&lt;HOST_ADDRESS&gt;(","&lt;HOST_ADDRESS&gt;)*</code> (default: true)*/
	public static Setting<String> globalUris = new StringSetting("theatre.taskRepository.globalUris", null);

	Logger log = Logger.getLogger(ConsoleStarter2.class);

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		try {
			String version = context.getBundle().getVersion().toString();
			log.info("cui " + version + " started");
			ConsoleStarter2 cs = new ConsoleStarter2(context);
			ExecuteResult ee = cs.startRequest();
			log.debug("ee="+ee);
			Serializable result = ee.getResult();
			log.debug("result="+result);
			// maybe copy it
			if(result instanceof FileProxy) {
				URI thisUri = URI.create(RemoteOsgiUtil.getLemIp());
				File localFile = ((FileProxy) result).getFileFor(thisUri);
				log.info("result after unproxy as a file: " + localFile);
			}
			else if(result instanceof File) {
				log.info("result as a file: " + (File) result);
			}
		} catch (Throwable e) {
			log.warn("Got exception", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		log.info("Cui2 stopped");
	}

}
