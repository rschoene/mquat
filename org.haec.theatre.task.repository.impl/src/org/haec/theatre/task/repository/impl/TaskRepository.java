/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.task.repository.impl;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.haec.theatre.task.ITask;
import org.haec.theatre.task.TaskImpl;
import org.haec.theatre.task.repository.ITaskRepository;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

/**
 * Central repository for tasks.
 * TODO Needs enhancement regarding memory footprint (delete old tasks).
 * @author René Schöne
 */
public class TaskRepository implements ITaskRepository {
	
	private AtomicLong taskCounter = new AtomicLong(System.currentTimeMillis());
	private Map<Long, ITask> tasks;
	private ServiceRegistration<ITaskRepository> reg;
	private transient static Logger log = Logger.getLogger(TaskRepository.class);
	
	public TaskRepository() {
		tasks = new TreeMap<>();
	}

	@Override
	public ITask createNewTask(long taskId) {
		ITask task = new TaskImpl();
		task.setTaskId(taskId);
		return task;
	}

	@Override
	public ITask createNewTask() {
		return createNewTask(getNextTaskId());
	}
	
	@Override
	public void updateTask(long taskId, ITask task) {
		if(hasTaskWithId(taskId)) {
			tasks.put(taskId, task);
		}
		else {
			log.warn("Can not update task with id=" + taskId + ". Not found in repo.");
		}
	}
	
	private long getNextTaskId() {
		return taskCounter.incrementAndGet();
	}

	@Override
	public ITask getTask(long taskId) {
		return tasks.get(taskId);
	}
	
	private boolean hasTaskWithId(long taskId) {
		return tasks.containsKey(taskId);
	}
	
	@Override
	public String toString() {
		return super.toString() + "(counter="+taskCounter+")";
	}
	
	public static ITaskRepository getRepo(BundleContext context, String repoUri) {
		ServiceReference<ITaskRepository> ref = context.getServiceReference(ITaskRepository.class);
		if(ref == null) {
			log.debug("TaskRepository was not found.");
			// try remote
			log.debug("Try to find repo remotely using uri=" + repoUri);
			RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
			RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(
					remote, repoUri, ITaskRepository.class.getName());
			if(srefs == null || srefs.length == 0) {
				// not successfull
				log.warn("TaskRepository was not found remotely. Returning null");
				return null;
			}
			return (ITaskRepository) remote.getRemoteService(srefs[0]);
		}
		return context.getService(ref);
		
	}

	@Override
	public boolean reset(boolean full) {
		taskCounter.set(System.currentTimeMillis());
		tasks.clear();
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return TaskRepository.class.getName();
	}
	
	protected void activate(ComponentContext ctx) {
		log.info("Activating task repository");
		BundleContext context = ctx.getBundleContext();
		try {
			reg = RemoteOsgiUtil.register(context, ITaskRepository.class, this);
		} catch(Exception e) {
			log.error("Could not register task repository remotely", e);
		}
	}
	
	protected void deactivate(ComponentContext ctx) {
		if(reg != null) { reg.unregister(); }
	}

}
