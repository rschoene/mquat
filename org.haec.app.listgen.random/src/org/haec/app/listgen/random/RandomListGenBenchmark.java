/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.listgen.random;

import java.util.ArrayList;
import java.util.List;

import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;

class ListGenBenchmarkData implements BenchmarkData {

	int mp;
	int cur_list_size = 0;
	
	public ListGenBenchmarkData(int mp, int cur_list_size) {
		super();
		this.mp = mp;
		this.cur_list_size = cur_list_size;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mp;
	}
	
}

/**
 * Benchmark of org.haec.app.listgen.random.Random
 * @author Sebastian Götz
 */
public class RandomListGenBenchmark implements Benchmark {
	
	private Random gen = new Random();
	
//	public static void main(String[] args) {
//		RandomListGenBenchmark b = new RandomListGenBenchmark();
//		b.addMonitor(MonitorRepository.shared.getMonitor("cpu_time"));
//		b.addMonitor(MonitorRepository.shared.getMonitor("cpu_energy"));
//		b.addMonitor(MonitorRepository.shared.getMonitor("response_time"));
//		b.setBenchData(new File("test.bench"));
//		b.benchmark();
//	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>();
		// metaparameters: list_size
		for (int list_size = 0; list_size <= 2000000; list_size += 250000) {
			List<Integer> mpv = new ArrayList<Integer>();
			mpv.add(list_size);
			result.add(new ListGenBenchmarkData(list_size, list_size));
			//System.out.print(cur_list_size+";");
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		ListGenBenchmarkData lgbd = (ListGenBenchmarkData) data;
		return gen.generate(lgbd.cur_list_size);
	}

}
