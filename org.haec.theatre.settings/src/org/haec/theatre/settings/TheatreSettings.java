/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.settings;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.haec.theatre.utils.settings.Setting;
import org.haec.theatre.utils.settings.SettingHolder.ChangeHandler;

/**
 * 
 * @author René Schöne
 */
public interface TheatreSettings {

	/** Reloads the properties */
	public void reload();
	
	/** @return the path to the properties file used to (re)load this */
	public String getPropertiesFilePath();
	
	/** Watches for file changes, updates the settings and calls the change handler 
	 * @throws IOException if an I/O error occurs 
	 */
	public void watchForChanges(ChangeHandler handler) throws IOException;

	// theatre-general
	/** Do not to use LEM, LRM nor any variants on this machine.
	 * Default: false. */
	public abstract Setting<Boolean> getNoLocalManagers();
	/** Trigger things only relevant for the master.
	 * Default: false. */
	public abstract Setting<Boolean> isMaster();
	/** Do use the local ip address (192.168.*). Otherwise external ip is used.
	 * Default: false. */
	public abstract Setting<Boolean> getIpLan();
	/** Cache IPs at RemoteOsgiUtil
	 * Default: true. */
	public abstract Setting<Boolean> getIpCache();
	/** Create a new global task repository at startup (usually only done at the GEM)
	 * Default: false. */
	public abstract Setting<Boolean> getTaskRepositoryCreateGlobal();
	/** URI (or possibly URIs) of the global task repository
	 * Default: "192.168.0.202". */
	public abstract Setting<String> getTaskRepositoryGlobalUris();
	/** Path to Rscript-command
	 * Default: "/usr/bin/Rscript". */
	public abstract Setting<String> getRExe();
	/** Port for r-osgi to be used for GEM,GRM and other master-related URIs
	 * Default: 9278. */
	public abstract Setting<Integer> getMasterPort();
	/** Port for r-osgi to be used for URIs referencing oneself
	 * Default: 9278. */
	public abstract Setting<Integer> getThisPort();
	/** Directory of resources
	 * Default: ".". */
	public abstract Setting<String> getResourceDir();

	// database
	/** URL of where the database is running on
	 * Default: "localhost". */
	public abstract Setting<String> getDatabaseUrl();
	/** Update rate for synchronizing variant model with the database in milliseconds
	 * Default: 25000. */
	public abstract Setting<Integer> getDatabaseUpdaterate();
	/** Period to check, if the database is still running (default: 5000) */
	public abstract Setting<Integer> getDatabaseCheckRunningRate();
	/** Whether to use the database
	 * Default: true. */
	public abstract Setting<Boolean> getDatabaseUse();
	/** Number of tries to connect to the database
	 * Default: 5. */
	public abstract Setting<Integer> getDatabaseStartupTries();
	/** Milliseconds to wait between each startup connection try
	 * Default: 5000. */
	public abstract Setting<Long> getDatabaseStartupInterval();
	/** Whether to use a dummy connection instead of the database.
	 * Default: false. */
	public abstract Setting<Boolean> getDatabaseDummy();
	/** Enable logging of each issued sql statement.
	 * Default: false. */
	public abstract Setting<Boolean> getDatabaseLogSql();

	// gem
	/** IP of the GEM
	 * Default: external ip. */
	public abstract Setting<String> getGemIp();
	/** URI of the GEM
	 * Default: GEM.IP + ":" + master.port. */
	public abstract Setting<String> getGemUri();
	/** Forces registrating LEMs to retrieve and start all app bundles
	 * Default: false. */
	public abstract Setting<Boolean> getGemAppsForceRetrieval();
	/** Force recompution of contracts in all LEMs if a LEM registers a new variant.
	 * Default: false. */
	public abstract Setting<Boolean> getGemUpdateContractsUponNewLem();
	/** Whether to log attempts of locking and unlocking in GEM.
	 * Default: false. */
	public abstract Setting<Boolean> getGemLogLock();
	/** For testing file copy. Changes mapping of scaling.handbrake from 210 to 211 and vice versa.
	 * Default: false. */
	public abstract Setting<Boolean> getGemTestChangeMapping();
	/** Check valid resolved resources. For testing purposes.
	 * Default: true. */
	public abstract Setting<Boolean> getGemValidResources();
	/** For testing mapping without the database. Use a predefined mapping.
	 * Default: false. */
	public abstract Setting<Boolean> getGemFakeMapping();
	/** Fail at optimize, if no LEM is connected.
	 * Default: true. */
	public abstract Setting<Boolean> getGemFailAtNoLemConnected();
	/** Time in ms to wait after powering on a host until its implementation are registered.
	 * Default: 10000. */
	public abstract Setting<Long> getGemTimeToWaitForRegisterImpl();
	/** Persist some data used later for visualization.
	 * Default: false. */
	public abstract Setting<Boolean> getGemSaveVisData();
	/** Try graceful degradation if the optimizer fails to compute an optimal configuration.
	 * Default: false. */
	public abstract Setting<Boolean> getGemTryGracefulDegradation();
	/** Print current config upon optimization.
	 * Default: false. */
	public abstract Setting<Boolean> getGemVerboseConfig();

	// grm
	/** IP of the GRM
	 * Default: external ip. */
	public abstract Setting<String> getGrmIp();
	/** URI of the GRM
	 * Default: GRM.IP + ":" + master.port. */
	public abstract Setting<String> getGrmUri();
	/** Enforce logging of each single notification. Otherwise only the notification count is logged. */
	public abstract Setting<Boolean> getGrmLogSingleNotification();
	/** Enforce logging of each single ip mismatch. Otherwise these are not logged. */
	public abstract Setting<Boolean> getGrmLogIpMismatch();

	// lem
	/** Force computing contracts, even if computed contracts are already available
	 * Default: false. */
	public abstract Setting<Boolean> getLemForceContracts();
	/** Do not compute contracts on its own, but instead let GEM compute it.
	 * Default: false. */
	public abstract Setting<Boolean> getLemContractsAtGem();
	/** Period in ms after which the LEM should check, if the GEM is alive, -1 means never check.
	 * Default: -1. */
	public abstract Setting<Long> getLemCheckForMasterPeriod();
	/** [Testing only!] Use System.nanoTime() for time monitoring. ThreadMxBeans otherwise. */
	public abstract Setting<Boolean> getLemSimpleTimeMeasure();
	/** [Testing only!] [Currently unsupported] Test plugins upon system startup.
	 * Will not be initiated by THEATRE, but has to be done by plugins themselves. */
	public abstract Setting<String> getLemTestPlugins();
	/** [Testing only!] [Currently unsupported] Fake benchmarks (wait some milliseconds).
	 * Only affects benchmarks subclassing org.haec.apps.util.FakeBenchmark */
	public abstract Setting<Boolean> getLemFakeBenchmarks();
	
	// lrm/sigar
	/** The period in ms of resource monitoring updates using Sigar.
	 * Default: 5000. */
	public abstract Setting<Long> getLrmUpdateRate();
	/** Delay in ms before starting resource monitoring using Sigar.
	 * Default: 0. */
	public abstract Setting<Long> getLrmInitialDelay();
	/** Do cache the values of properties with abstract-instance type. Otherwise such values are measured on start-up.
	 * Default: false. */
	public abstract Setting<Boolean> getLrmCacheStatics();
	/** Directory that is used to store hardware benchmarks.
	 * Default: current directory. */
	public abstract Setting<String> getLrmBenchmarkDirectory();

	// misc
	/** Comma-seperated classes to use as REST resources (default: "") */
	public abstract Setting<String> getRestClasses();
	
	// powermanager
	/** Time in ms to wait before aborting operation (default: 60000) */
	public abstract Setting<Long> getPowermanagerMaxWait();
	/** Time in ms between updates of measured values (default: 1000) */
	public abstract Setting<Long> getPowermanagerUpdateRate();
	/** Print additional messages (default: false) */
	public abstract Setting<Boolean> getPowermanagerVerbose();

	// ilp
	/** Log factors in objective,
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogObjectiveSettings();
	/** Log single constraint creations,
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogConstraints();
	/** Log single indicator variable constraint creation,
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogSingleIndicator();
	/** Log modeMismatch,
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogModeServerMismatch();
	/** Log missing ecls,
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogMissingEcl();
	/** Log creation of usage variables
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogUsageVar();
	/** Log processing of resources
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogResourceProcessing();
	/** Log processing of resources,
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogProcessingResource();
	/** Log elimination of duplicate constraints,
	 * Default: false. */
	public abstract Setting<Boolean> getILPlogDuplicateElimination();
	/** Use response_time*energy_rate. Otherwise simulate time.
	 * Default: false. */
	public abstract Setting<Boolean> getILPobjectiveSimpleWeight();
	/** Use glp_exact. Otherwise use glp_simplex.
	 * Default: false. */
	public abstract Setting<Boolean> getILPsolverExact();
	/** Minimize response_time additionally to energy.
	 * Default: false. */
	public abstract Setting<Boolean> getILPobjectiveIncludeResponseTime();
	/** Force sequential execution of required components of connectors.
	 * Default: false. */
	public abstract Setting<Boolean> getILPfeatureSequentialConstraint();
	/** Penalize cross-server mappings of required components or of components required by same connector.
	 * Default: false. */
	public abstract Setting<Boolean> getILPfeaturePenalties();
	/** Ignore unknown properties by setting usageVar >= 0, thus always satisfying it.
	 * Default: false. */
	public abstract Setting<Boolean> getILPfeatureIgnoreUnknownProperties();
	/** Include the properties of servers.
	 * Default: false. */
	public abstract Setting<Boolean> getILPfeatureIncludeTopLevelProperties();
	/** Regard values with value equal to zero as unknown properties.
	 * Default: false. */
	public abstract Setting<Boolean> getILPfeatureZeroIsUnknown();
	/** Timeout in seconds to wait for init thread to finish,
	 * Default: 20. */
	public abstract Setting<Integer> getILPsetupInitTimeout();
	/** Experimental: Threat binary values as 1 if greater than 0.5.
	 * Default: false. */
	public abstract Setting<Boolean> getILPfeatureFuzzy();
	/** Experimental: Only update ILP, instead of regeneration
	 * Default: false. */
	public abstract Setting<Boolean> getILPfeatureOnlyUpdate();
	/** Time to regard resources as offline
	 * Default: 60000. */
	public abstract Setting<Integer> getILPresourceOfflineTimeout();
	/** Reset state of optimizer for each request. Should not be used,
	 *  if {@link #getILPfeatureOnlyUpdate()} is <code>true</code>.
	 * Default: true. */
	public Setting<Boolean> getILPcleanStart();

}