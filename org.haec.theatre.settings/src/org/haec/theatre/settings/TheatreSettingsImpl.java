/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.settings;

import java.io.IOException;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.settings.BooleanSetting;
import org.haec.theatre.utils.settings.CallableSetting;
import org.haec.theatre.utils.settings.IntegerSetting;
import org.haec.theatre.utils.settings.LongSetting;
import org.haec.theatre.utils.settings.Setting;
import org.haec.theatre.utils.settings.SettingHolder;
import org.haec.theatre.utils.settings.StringSetting;

/**
 * Utility class encapsulating all settings of THEATRE.
 * @author René Schöne
 */
public class TheatreSettingsImpl extends SettingHolder implements TheatreSettings {

	public static TheatreSettings currentInstance;
	private Logger log = Logger.getLogger(TheatreSettingsImpl.class);
	
	// theatre-general
	public Setting<Boolean> noLocalManagers = new BooleanSetting("theatre.nolocal", false);
	public Setting<Boolean> master = new BooleanSetting("theatre.isMaster", false);
	public Setting<Boolean> ipLan = new BooleanSetting("theatre.ip.lan", false);
	public Setting<Boolean> ipcache = new BooleanSetting("theatre.ip.cache", true);
	public Setting<Boolean> taskRepositorycreateglobal = new BooleanSetting("theatre.taskRepository.createglobal", false);
	public Setting<String> taskRepositoryglobalUris = new StringSetting("theatre.taskRepository.globalUris", "192.168.0.202");
	public Setting<String> rexe = new StringSetting("theatre.r.exe", "/usr/bin/Rscript");
	public Setting<Integer> masterPort = new IntegerSetting("master.port", 9278);
	public Setting<Integer> thisPort = new IntegerSetting("this.port", 9278);
	public Setting<String> resourceDir = new StringSetting("theatre.resources.dir", ".");

	// database
	public Setting<String> databaseurl = new StringSetting("database.url", "localhost");
	public Setting<Integer> databaseupdaterate = new IntegerSetting("database.rate.update", 25000);
	public Setting<Integer> databasecheckrate = new IntegerSetting("database.rate.checkRunning", 5000);
	public Setting<Boolean> databaseuse = new BooleanSetting("database.use", true);
	public Setting<Integer> databasestartupTries = new IntegerSetting("database.startupTries", 5);
	public Setting<Long> databasestartupInterval = new LongSetting("database.startupInterval", 5000);
	public Setting<Boolean> databasedummy = new BooleanSetting("database.dummy", false);
	public Setting<Boolean> databaselogSql = new BooleanSetting("database.logSql", false);

	// gem theatre.gem.ip = 127.0.0.1
	public Setting<String> gemIp = new StringSetting("theatre.gem.ip", RemoteOsgiUtil.getExternalIp());
	public Setting<String> gemUri = new CallableSetting<>("gem.uri", new Callable<String>() {
		@Override
		public String call() throws Exception {
			return gemIp.get() + ":" + masterPort.get();
		}
	});
	public Setting<Boolean> gemappsForceRetrieval = new BooleanSetting("theatre.gem.apps.forceRetrieval", false);
	public Setting<Boolean> gemupdateContractsUponNewLem = new BooleanSetting("theatre.gem.updateContractsUponNewLem", false);
	public Setting<Boolean> gemlogLock = new BooleanSetting("theatre.gem.logLock", false);
	public Setting<Boolean> gemtestChangeMapping = new BooleanSetting("theatre.gem.test.changeMapping", false);
	public Setting<Boolean> gemvalidResources = new BooleanSetting("theatre.gem.validResources", true);
	public Setting<Boolean> gemfakeMapping = new BooleanSetting("theatre.gem.fakeMapping", false);
	public Setting<Boolean> gemfailAtNoLemConnected = new BooleanSetting("theatre.gem.failAtNoLemConnected", false);
	public Setting<Long> gemtimeToWaitForRegisterImpl = new LongSetting("theatre.gem.timeToWaitForRegisterImpl", 10000);
	public Setting<Boolean> gemsaveVisData = new BooleanSetting("theatre.gem.save.visData", false);
	public Setting<Boolean> gemtryGracefulDegradation = new BooleanSetting("theatre.gem.graceful.degradation", false);
	public Setting<Boolean> gemverboseConfig = new BooleanSetting("theatre.gem.verboseConfig", false);

	// grm
	public Setting<String> grmIp = new StringSetting("theatre.grm.ip", RemoteOsgiUtil.getExternalIp());
	public Setting<String> grmUri = new CallableSetting<>("grm.uri", new Callable<String>() {
		@Override
		public String call() throws Exception {
			return grmIp.get() + ":" + masterPort.get();
		}
	});
	public Setting<Boolean> grmlogsinglenotification = new BooleanSetting("theatre.grm.log.singleNotification", false);
	public Setting<Boolean> grmlogipmismatch = new BooleanSetting("theatre.grm.log.ipMismatch", false);
	
	// lem
	public Setting<Boolean> lemforceContracts = new BooleanSetting("theatre.lem.contracts.force", false);
	public Setting<Boolean> lemcontractsAtGem = new BooleanSetting("theatre.lem.contracts.atgem", false);
	public Setting<Long> lemcheckForMasterPeriod = new LongSetting("theatre.lem.periods.checkForMaster", -1);
	public Setting<Boolean> lemsimpleTime = new BooleanSetting("theatre.lem.simpleTime", false);
	public Setting<String> lemtestplugin = new StringSetting("theatre.lem.pluginTest", "");
	public Setting<Boolean> lemfakebenchmarks = new BooleanSetting("theatre.lem.benchmark.fake", false);

	// lrm
	public Setting<Long> lrmupdateRate = new LongSetting("lrm.update.rate", 5000);
	public Setting<Long> lrminitialDelay = new LongSetting("lrm.initialDelay", 0);
	public Setting<Boolean> lrmcacheStatics = new BooleanSetting("theatre.lrm.cache.statics", false);
	public Setting<String> lrmbenchmarkDirectory = new StringSetting("theatre.lrm.benchmarkDir", ".");
	
	// misc
	public Setting<String> restClasses = new StringSetting("theatre.rest.classes", "");

	// powermanager
	public Setting<Long> pmmaxWait = new LongSetting("powermanager.maxWait", 60000L);
	public Setting<Long> pmupdateRate = new LongSetting("powermanager.rate.update", 1000L);
	public Setting<Boolean> pmverbose = new BooleanSetting("powermanager.verbose", false);

	// ilp
	public Setting<Boolean> ilplogobjectiveSettings = new BooleanSetting("ilp.log.objectiveSettings", false);
	public Setting<Boolean> ilplogconstraints = new BooleanSetting("ilp.log.constraints", false);
	public Setting<Boolean> ilplogsingleIndicator = new BooleanSetting("ilp.log.singleIndicator", false);
	public Setting<Boolean> ilplogmodeServerMismatch = new BooleanSetting("ilp.log.modeServerMismatch", false);
	public Setting<Boolean> ilplogUsageVar = new BooleanSetting("ilp.log.usageVar", false);
	public Setting<Boolean> ilplogmissingEcl = new BooleanSetting("ilp.log.missingEcl", false);
	public Setting<Boolean> ilplogResourceProcessing = new BooleanSetting("ilp.log.resourceProcessing", false);
	public Setting<Boolean> ilplogprocessingResource = new BooleanSetting("ilp.log.processingResource", false);
	public Setting<Boolean> ilplogilpLogDuplicateElimination = new BooleanSetting("ilp.log.ilp.log.duplicateElimination", false);
	public Setting<Boolean> ilpobjectivesimpleWeight = new BooleanSetting("ilp.objective.simpleWeight", false);
	public Setting<Boolean> ilpsolverexact = new BooleanSetting("ilp.solver.exact", false);
	public Setting<Boolean> ilpobjectiveincludeResponseTime = new BooleanSetting("ilp.objective.includeResponseTime", false);
	public Setting<Boolean> ilpfeaturesequentialConstraint = new BooleanSetting("ilp.feature.sequentialConstraint", false);
	public Setting<Boolean> ilpfeaturepenalties = new BooleanSetting("ilp.feature.penalties", false);
	public Setting<Boolean> ilpfeatureignoreUnknownProperties = new BooleanSetting("ilp.feature.ignoreUnknownProperties", false);
	public Setting<Boolean> ilpfeatureilpFeatureIncludeTopLevelProperties = new BooleanSetting("ilp.feature.ilp.feature.includeTopLevelProperties", false);
	public Setting<Boolean> ilpfeaturezeroIsUnknown = new BooleanSetting("ilp.feature.zeroIsUnknown", false);
	public Setting<Integer> ilpsetupinitTimeout = new IntegerSetting("ilp.setup.initTimeout", 20);
	public Setting<Boolean> ilpfeatureexperimentalFuzzy = new BooleanSetting("ilp.feature.experimental.fuzzy", false);
	public Setting<Boolean> ilponlyUpdate = new BooleanSetting("ilp.feature.experimental.onlyUpdate", false);
	public Setting<Integer> ilpresourceOfflineTimeout = new IntegerSetting("ilp.grm.resourceOfflineTimeout", 60000);
	public Setting<Boolean> ilpclean = new BooleanSetting("ilp.feature.cleanState", true);
	
	private String fname;
	
	public TheatreSettingsImpl() {
		currentInstance = this;
	}

	@Override
	public void reload() {
		long before = System.currentTimeMillis();
		fname = loadTheatreProperties(this);
		if(log.isDebugEnabled()) {
			log.debug("Reloading settings took " + (System.currentTimeMillis() - before) + "ms.");
		}
	}

	protected void activate() {
		log.debug("Activating settings");
		reload();
	}
	
	@Override
	public String getPropertiesFilePath() {
		return fname;
	}
	
	@Override
	public void watchForChanges(ChangeHandler handler) throws IOException {
		watchForChanges(getPropertiesFilePath(), handler, this);
	}
	
	@Override
	public Setting<Boolean> getNoLocalManagers() {
		return noLocalManagers;
	}
	
	@Override
	public Setting<Boolean> isMaster() {
		return master;
	}

	@Override
	public Setting<String> getResourceDir() {
		return resourceDir;
	}

	@Override
	public Setting<Boolean> getGemAppsForceRetrieval() {
		return gemappsForceRetrieval;
	}

	@Override
	public Setting<Boolean> getGemUpdateContractsUponNewLem() {
		return gemupdateContractsUponNewLem;
	}

	@Override
	public Setting<Boolean> getGemLogLock() {
		return gemlogLock;
	}

	@Override
	public Setting<Boolean> getGemTestChangeMapping() {
		return gemtestChangeMapping;
	}

	@Override
	public Setting<Boolean> getGemValidResources() {
		return gemvalidResources;
	}

	@Override
	public Setting<Boolean> getGemFakeMapping() {
		return gemfakeMapping;
	}

	@Override
	public Setting<Boolean> getGemFailAtNoLemConnected() {
		return gemfailAtNoLemConnected;
	}

	@Override
	public Setting<Long> getGemTimeToWaitForRegisterImpl() {
		return gemtimeToWaitForRegisterImpl;
	}

	@Override
	public Setting<Boolean> getGemSaveVisData() {
		return gemsaveVisData;
	}

	@Override
	public Setting<Boolean> getGemTryGracefulDegradation() {
		return gemtryGracefulDegradation;
	}

	@Override
	public Setting<Boolean> getGemVerboseConfig() {
		return gemverboseConfig;
	}

	@Override
	public Setting<String> getDatabaseUrl() {
		return databaseurl;
	}

	@Override
	public Setting<Integer> getDatabaseUpdaterate() {
		return databaseupdaterate;
	}

	@Override
	public Setting<Integer> getDatabaseCheckRunningRate() {
		return databasecheckrate;
	}

	@Override
	public Setting<Boolean> getDatabaseUse() {
		return databaseuse;
	}

	@Override
	public Setting<Integer> getDatabaseStartupTries() {
		return databasestartupTries;
	}

	@Override
	public Setting<Long> getDatabaseStartupInterval() {
		return databasestartupInterval;
	}

	@Override
	public Setting<Boolean> getDatabaseDummy() {
		return databasedummy;
	}

	@Override
	public Setting<Boolean> getDatabaseLogSql() {
		return databaselogSql;
	}

	@Override
	public Setting<Boolean> getLemForceContracts() {
		return lemforceContracts;
	}

	@Override
	public Setting<Boolean> getLemContractsAtGem() {
		return lemcontractsAtGem;
	}

	@Override
	public Setting<Long> getLemCheckForMasterPeriod() {
		return lemcheckForMasterPeriod;
	}

	@Override
	public Setting<Boolean> getIpLan() {
		return ipLan;
	}

	@Override
	public Setting<Long> getLrmUpdateRate() {
		return lrmupdateRate;
	}

	@Override
	public Setting<Long> getLrmInitialDelay() {
		return lrminitialDelay;
	}

	@Override
	public Setting<Boolean> getLrmCacheStatics() {
		return lrmcacheStatics;
	}

	@Override
	public Setting<String> getLrmBenchmarkDirectory() {
		return lrmbenchmarkDirectory;
	}

	@Override
	public Setting<String> getRestClasses() {
		return restClasses;
	}

	@Override
	public Setting<Boolean> getIpCache() {
		return ipcache;
	}

	@Override
	public Setting<Boolean> getTaskRepositoryCreateGlobal() {
		return taskRepositorycreateglobal;
	}

	@Override
	public Setting<String> getTaskRepositoryGlobalUris() {
		return taskRepositoryglobalUris;
	}

	@Override
	public Setting<String> getRExe() {
		return rexe;
	}

	@Override
	public Setting<Boolean> getGrmLogSingleNotification() {
		return grmlogsinglenotification;
	}

	@Override
	public Setting<Boolean> getGrmLogIpMismatch() {
		return grmlogipmismatch;
	}

	@Override
	public Setting<Boolean> getLemSimpleTimeMeasure() {
		return lemsimpleTime;
	}

	@Override
	public Setting<String> getLemTestPlugins() {
		return lemtestplugin;
	}

	@Override
	public Setting<Boolean> getLemFakeBenchmarks() {
		return lemfakebenchmarks;
	}

	@Override
	public Setting<Long> getPowermanagerMaxWait() {
		return pmmaxWait;
	}

	@Override
	public Setting<Long> getPowermanagerUpdateRate() {
		return pmupdateRate;
	}

	@Override
	public Setting<Boolean> getPowermanagerVerbose() {
		return pmverbose;
	}

	@Override
	public Setting<Integer> getMasterPort() {
		return masterPort;
	}

	@Override
	public Setting<Integer> getThisPort() {
		return thisPort;
	}

	@Override
	public Setting<String> getGemIp() {
		return gemIp;
	}

	@Override
	public Setting<String> getGrmIp() {
		return grmIp;
	}

	@Override
	public Setting<Boolean> getILPlogObjectiveSettings() {
		return ilplogobjectiveSettings;
	}

	@Override
	public Setting<Boolean> getILPlogConstraints() {
		return ilplogconstraints;
	}

	@Override
	public Setting<Boolean> getILPlogSingleIndicator() {
		return ilplogsingleIndicator;
	}

	@Override
	public Setting<Boolean> getILPlogModeServerMismatch() {
		return ilplogmodeServerMismatch;
	}
	
	@Override
	public Setting<Boolean> getILPlogUsageVar() {
		return ilplogUsageVar;
	}

	@Override
	public Setting<Boolean> getILPlogMissingEcl() {
		return ilplogmissingEcl;
	}

	@Override
	public Setting<Boolean> getILPlogResourceProcessing() {
		return ilplogResourceProcessing;
	}

	@Override
	public Setting<Boolean> getILPlogProcessingResource() {
		return ilplogprocessingResource;
	}

	@Override
	public Setting<Boolean> getILPlogDuplicateElimination() {
		return ilplogilpLogDuplicateElimination;
	}

	@Override
	public Setting<Boolean> getILPobjectiveSimpleWeight() {
		return ilpobjectivesimpleWeight;
	}

	@Override
	public Setting<Boolean> getILPsolverExact() {
		return ilpsolverexact;
	}

	@Override
	public Setting<Boolean> getILPobjectiveIncludeResponseTime() {
		return ilpobjectiveincludeResponseTime;
	}

	@Override
	public Setting<Boolean> getILPfeatureSequentialConstraint() {
		return ilpfeaturesequentialConstraint;
	}

	@Override
	public Setting<Boolean> getILPfeaturePenalties() {
		return ilpfeaturepenalties;
	}

	@Override
	public Setting<Boolean> getILPfeatureIgnoreUnknownProperties() {
		return ilpfeatureignoreUnknownProperties;
	}

	@Override
	public Setting<Boolean> getILPfeatureIncludeTopLevelProperties() {
		return ilpfeatureilpFeatureIncludeTopLevelProperties;
	}

	@Override
	public Setting<Boolean> getILPfeatureZeroIsUnknown() {
		return ilpfeaturezeroIsUnknown;
	}

	@Override
	public Setting<Integer> getILPsetupInitTimeout() {
		return ilpsetupinitTimeout;
	}

	@Override
	public Setting<Boolean> getILPfeatureFuzzy() {
		return ilpfeatureexperimentalFuzzy;
	}

	@Override
	public Setting<Boolean> getILPfeatureOnlyUpdate() {
		return ilponlyUpdate;
	}

	@Override
	public Setting<Integer> getILPresourceOfflineTimeout() {
		return ilpresourceOfflineTimeout;
	}

	@Override
	public Setting<Boolean> getILPcleanStart() {
		return ilpclean;
	}

	@Override
	public Setting<String> getGemUri() {
		return gemUri;
	}

	@Override
	public Setting<String> getGrmUri() {
		return grmUri;
	}

}
