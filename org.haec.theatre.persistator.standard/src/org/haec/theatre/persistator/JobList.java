/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.persistator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Simple representation of a list of {@link SimpleJob}s.
 * @author René Schöne
 */
@XmlRootElement
public class JobList implements Iterable<SimpleJob> {
	
	@XmlElements({ @XmlElement(name = "jobs", type = SimpleJob.class) })
	List<SimpleJob> infos;
	
	public JobList() {
		infos = new ArrayList<>();
	}
	
	public void add(SimpleJob tri) {
		infos.add(tri);
	}

	@Override
	public String toString() {
		return "AllTasksResponseInfo [infos=" + infos + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<SimpleJob> iterator() {
		return infos.iterator();
	}
}