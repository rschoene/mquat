/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.persistator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.coolsoftware.coolcomponents.ccm.variant.JobStatus;

/**
 * Simple representation of a job.
 * @author René Schöne
 */
@XmlRootElement
public class SimpleJob {
	@XmlElement(name="taskId") Long taskId;
	@XmlElement(name="jobId") Long jobId;
	@XmlElement(name="startTime") Long startTime;
	@XmlElement(name="pred_duration") Double pred_duration;
	@XmlElement(name="endTime") Long endTime;
	@XmlElement(name="status", defaultValue = "unknown") String status;
	@XmlElement(name="result", defaultValue = "unknown") String result;
	@XmlElement(name="existsAtGem") boolean existsAtGem;
	@XmlElement(name="host") String host;
	@XmlElement(name="implName", defaultValue = "unknown") String implName;
	
	public SimpleJob() { } // needed by JAXB
	public SimpleJob(Long taskId, Long jobId, Long startTime, Double pred_duration,
			Long endTime, JobStatus jobStatus, String implName, String host, String result) {
		this.taskId = taskId;
		this.jobId = jobId;
		this.startTime = startTime;
		this.pred_duration = pred_duration;
		this.endTime = endTime;
		this.existsAtGem = false;
		this.status = jobStatus.toString();
		this.implName = implName;
		this.host = host;
		this.result = result;
	}
}