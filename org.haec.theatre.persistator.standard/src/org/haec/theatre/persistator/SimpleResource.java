/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.persistator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

/**
 * Simple representation of a hardware resource.
 * @author René Schöne
 */
public class SimpleResource {
	@XmlElements({ @XmlElement(name = "children", type = SimpleResource.class) })
	List<SimpleResource> subresources;
	@XmlElement String name;
	@XmlElement String type;
	@XmlElement String lastChanged;
	@XmlElement Map<String, String> attributes;
	@XmlElements({ @XmlElement(name = "jobs", type = SimpleJob.class) })
	List<SimpleJob> jobs;
	public SimpleResource() {
		attributes = new HashMap<String, String>();
		subresources = new ArrayList<>();
		jobs = new ArrayList<>();
	}
}