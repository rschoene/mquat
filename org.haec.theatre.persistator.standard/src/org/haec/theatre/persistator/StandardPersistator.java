/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.persistator;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.variant.Component;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.Job;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;
import org.coolsoftware.coolcomponents.ccm.variant.Schedule;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment;
import org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression;
import org.coolsoftware.coolcomponents.nameable.NamedElement;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.ecl.resource.ecl.mopp.EclPrinter;
import org.coolsoftware.ecl.resource.ecl.mopp.EclResource;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.resource.request.mopp.RequestPrinter2;
import org.coolsoftware.requests.resource.request.mopp.RequestResource;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.Persistator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.StringUtils;

import com.google.gson.Gson;

/**
 * The standard implementation of a persistator storing the files in the workspace.
 * @author René Schöne
 */
public class StandardPersistator implements Persistator {
	
	private static Logger log = Logger.getLogger(StandardPersistator.class);

	public static SimpleHardwareModel parse(VariantModel vm) {
		SimpleHardwareModel result = new SimpleHardwareModel();
		Component root = vm.getRoot();
		result.root = new SimpleResource();
		parse(root, result.root);
		return result;
	}

	private static void parse(Component comp, SimpleResource simple) {
		// set name
		if(comp instanceof ContainerProvider) {
			String ip = comp.getName();
			simple.attributes.put("IP", ip);
			String name = PlatformUtils.getHostNameOf(ip);
			if(name == null) {
				name = "(" + ip + ")";
			}
			simple.name = name;
		}
		else {
			simple.name = comp.getName();
		}
		simple.type = comp.getSpecification() != null ? comp.getSpecification().getName() : "unknown";
		// set property-value pairs
		for(VariantPropertyBinding vpb : comp.getPropertyBinding()) {
			if(vpb == null) {
				continue;
			}
			String name;
			Property property = vpb.getProperty();
			if(property != null) {
				Variable declaredVariable = property.getDeclaredVariable();
				if(declaredVariable != null) {
					name = declaredVariable.getName();
				} else { name = "nullVariable in " + property; }
			} else { name = "nullProperty in " + comp; }
			String value = getStringValue(vpb.getValueExpression());
			if(value != null) {
				simple.attributes.put(name, value);
			}
		}
		// recursive calls
		if(comp instanceof SWComponent) {
			for(Job job : ((SWComponent) comp).getJobs()) {
				SimpleJob simpleJob = parse(job);
				simple.jobs.add(simpleJob);
			}
			for(SWComponent subtype : ((SWComponent) comp).getSubtypes()) {
				SimpleResource simpleSub = new SimpleResource();
				simple.subresources.add(simpleSub);
				parse(subtype, simpleSub);
			}
		}
		else if(comp instanceof Resource) {
			Long lastChanged = ((Resource) comp).getLastChanged();
			simple.lastChanged = lastChanged == null ? "never" : lastChanged.toString();
			if(comp instanceof ContainerProvider) {
				Schedule schedule = ((ContainerProvider) comp).getSchedule();
				if(schedule != null) { for(Job job : schedule.getJobs()) {
					SimpleJob simpleJob = parse(job);
					simple.jobs.add(simpleJob);
				}}
			}
			for(Resource subResource : ((Resource) comp).getSubresources()) {
				SimpleResource simpleSub = new SimpleResource();
				simple.subresources.add(simpleSub);
				parse(subResource, simpleSub);
			}
		} else { log.debug(comp + " has no subresouces."); }
	}

	protected static SimpleJob parse(Job job) {
		SimpleJob simpleJob = new SimpleJob(job.getTaskId(), job.getJobId(),
				job.getStartTime(), job.getPredictedRuntime(),
				job.getEndTime(), job.getStatus(), job.getImpl().getName(),
				job.getHost(), StringUtils.cap(job.getRawResult(), 100));
		return simpleJob;
	}

	private static String getStringValue(Expression exp) {
		if(exp == null) { return null; }
		if(exp instanceof BooleanLiteralExpression) { return
				Boolean.toString(((BooleanLiteralExpression) exp).isValue()); }
		if(exp instanceof IntegerLiteralExpression) { return
				Long.toString(((IntegerLiteralExpression) exp).getValue()); }
		if(exp instanceof RealLiteralExpression) { return
				Double.toString(((RealLiteralExpression) exp).getValue()); }
		if(exp instanceof StringLiteralExpression) { return
				((StringLiteralExpression) exp).getValue(); }
		// no literal expression
		return exp.toString();
	}


	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Persistator#persist(org.coolsoftware.requests.Request)
	 */
	@Override
	public boolean persist(Request request) {
		if(request == null) {
			log.warn("The given request is null.");
			return false;
		}
		try {

			RequestResource reqRes = (RequestResource) request.eResource();
			OutputStream out = getRequestOutputStream(request);
			if(out == null) { return false; }
			RequestPrinter2 printer = new RequestPrinter2(out, reqRes);
			printer.print(request);
		} catch (IOException ioe) {
			log.warn("IO exception during request persisting", ioe);
			return false;
		}
		return true;
	}
	
	protected OutputStream getRequestOutputStream(Request request) throws IOException {
		String path = safeGetName(request.getComponent()) + "_" + safeGetName(request.getTarget()) + ".request";
		URI uri = constructFileUri(FileUtils.getWorkspaceDir(), path);
		FileOutputStream fos = new FileOutputStream(uri.toFileString());
		log.debug("Using " + uri.toFileString());
		return fos;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Persistator#persist(org.coolsoftware.ecl.EclFile)
	 */
	@Override
	public boolean persist(EclFile eclFile) {
		if(eclFile == null || eclFile.getContracts().isEmpty()) {
			log.warn("The given ECL file is null or contains no contracts.");
			return false;
		}
		try {
			EclResource eclRes = (EclResource) eclFile.eResource();
			OutputStream out = getEclOutputStream(eclFile);
			if(out == null) { return false; }
			EclPrinter printer = new EclPrinter(out , eclRes);
			printer.print(eclFile);

		} catch (IOException ioe) {
			log.warn("IO exception during ECLFile persisting", ioe);
			return false;
		}
		return true;
	}
	
	protected OutputStream getEclOutputStream(EclFile eclFile) throws IOException {
		EclContract contract = eclFile.getContracts().get(0);
		if(!(contract instanceof SWComponentContract)) {
			log.fatal("Only software contracts are supported, but got " + contract);
			return null;
		}
		SWComponentContract swContract = (SWComponentContract) contract;
		String path = safeGetName(swContract.getComponentType()) + "_" + safeGetName(swContract.getPort()) + ".ecl";
		URI uri = constructFileUri(FileUtils.getWorkspaceDir(), path);
		FileOutputStream fos = new FileOutputStream(uri.toFileString());
		log.debug("Using " + uri.toFileString());
		return fos;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Persistator#combineAndPersist(
	 * org.coolsoftware.ecl.EclFile, org.coolsoftware.requests.Request)
	 */
	@Override
	public boolean combineAndPersist(EclFile eclFile, Request request) {
		for(EclContract contract : eclFile.getContracts()) {
			if(!(contract instanceof SWComponentContract)) {
				log.info("Not a software contract, skipping " + safeGetName(contract));
				continue;
			}
			SWComponentContract swcc = (SWComponentContract) contract;
			for(SWContractMode mode : swcc.getModes()) { // e.g. mode scaleFfmpeg1
				for(SWContractClause clause : mode.getClauses()) { // e.g. requires CPU { ... }
					// classes of clause can be either hw-req, sw-req, or provision (self-req not used yet)
					if(clause instanceof SWComponentRequirementClause) {
						for(PropertyRequirementClause prc : ((SWComponentRequirementClause) clause)
								.getRequiredProperties()) { // e.g. min utility
							evalFormula(prc, request, true);
							evalFormula(prc, request, false);
						}
					}
					else if(clause instanceof HWComponentRequirementClause) {
						for(PropertyRequirementClause prc : ((HWComponentRequirementClause) clause)
								.getRequiredProperties()) { // e.g. min frequency
							evalFormula(prc, request, true);
							evalFormula(prc, request, false);
						}
					}
					else if (clause instanceof ProvisionClause) {
						evalFormula((ProvisionClause) clause, request, true);
						evalFormula((ProvisionClause) clause, request, false);
					}
					else {
						log.warn("Ignoring clause " + clause);
					}
				}
			}
		}
		// now go and persist the file
		return persist(eclFile);
	}
	
	/**
	 * Duplicated code from {@link #evalFormula(ProvisionClause, Request, boolean)}
	 * @param prc the clause to inspect
	 * @param request the request to compute the formula with
	 * @param min wether to check for minimum or maximum value
	 */
	private void evalFormula(PropertyRequirementClause prc, Request request,
			boolean min) {
		Statement possibleFormula = min ? prc.getMinValue() : prc.getMaxValue();
		if(possibleFormula instanceof FunctionExpression) {
			CcmReal val = (CcmReal)evalFormula(possibleFormula, request.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min) { prc.setMinValue(rle); }
			else { prc.setMaxValue(rle); }
		}
	}
	
	/**
	 * Duplicated code from {@link #evalFormula(PropertyRequirementClause, Request, boolean)}
	 * @param pc the clause to inspect
	 * @param request the request to compute the formula with
	 * @param min wether to check for minimum or maximum value
	 */
	private void evalFormula(ProvisionClause pc, Request request, boolean min) {
		Statement possibleFormula = min ? pc.getMinValue() : pc.getMaxValue();
		if(possibleFormula instanceof FunctionExpression) {
			CcmReal val = (CcmReal)evalFormula(possibleFormula, request.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(min) { pc.setMinValue(rle); }
			else { pc.setMaxValue(rle); }
		}
	}

	/**
	 * @param stmt the statement, i.e. formula, to evaluate
	 * @param metaParamValues the metaparameters and their values
	 * @return the concrete value
	 */
	private CcmValue evalFormula(Statement stmt,
			EList<MetaParamValue> metaParamValues) {
		final Map<String, Integer> metaparamMap = new HashMap<>();
		for(MetaParamValue mpv : metaParamValues) {
			metaparamMap.put(mpv.getMetaparam().getName(), mpv.getValue());
		}
		FunctionExpression fe = (FunctionExpression)stmt;
		if(fe.getSourceExp() instanceof VariableAssignment) {
			VariableAssignment va = (VariableAssignment)fe.getSourceExp();
			Expression e = va.getValueExpression();
			CcmValue v = new CcmExpressionInterpreter(){
				@Override
				protected CcmValue interpretVariableCallExp(
						VariableCallExpression exp) {
					Variable refVar = exp.getReferredVariable();
					if(refVar == null) { throw new NullPointerException("The referred variable is null"); }
					Integer value = metaparamMap.get(refVar.getName());
					if(value != null) {
						CcmReal result = StdlibFactory.eINSTANCE.createCcmReal();
						result.setRealValue(value);
						return result;
					}
					log.warn("No metaparam value found for " + safeGetName(refVar));
					return super.interpretVariableCallExp(exp);
				}
			}.interpret(e);
			return v;
		}
		log.fatal("Function expression with source expression no variable assignment: " + fe);
		return null;
	}

	private String safeGetName(NamedElement e) {
		return e == null ? "unkownElement" : e.getName();
	}
	private String safeGetName(EclContract c) {
		return c == null ? "unkownContract" : c.getName();
	}
	private String safeGetName(Variable v) {
		return v == null ? "unkownVar" : v.getName();
	}
	
	private URI constructFileUri(String... segments) {
		String path = FileUtils.join(segments);
		return URI.createFileURI(path);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Persistator#persistModelToJson(String, String, String)
	 */
	@Override
	public boolean persistModelToJson(String serializedVariantModel, String serializedStructModel,
			String filename) {
		return persistToJson(serializedVariantModel, serializedStructModel, filename, false, null);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Persistator#persistJobsToJson(String, String, String, Long)
	 */
	@Override
	public boolean persistJobsToJson(String serializedVariantModel, String serializedStructModel,
			String filename, Long taskId) {
		return persistToJson(serializedVariantModel, serializedStructModel, filename, true, taskId);
	}

	protected boolean persistToJson(String serializedVariantModel, String serializedStructModel,
			String filename, boolean onlyJobs, Long taskId) {
		VariantModel model;
		try {
			model = deserializeModel(serializedVariantModel, serializedStructModel);
		} catch (IOException e) {
			log.error("Exception during " + (onlyJobs ? "job" : "variant model") + " deserialization", e);
			return false;
		}
		return persistToJson(model, filename, onlyJobs, taskId);
	}
	
	public boolean persistToJson(VariantModel model, String filename, boolean onlyJobs, Long taskId) {
		try(FileWriter fw = new FileWriter(filename)) {
			Object toSerialize = onlyJobs ? getJobsForJson(model, taskId) : parse(model);
			new Gson().toJson(toSerialize, fw);
		} catch (Exception e) {
			log.error("Exception during " + (onlyJobs ? "job" : "variant model") + " persisting to " + filename, e);
			return false;
		}
		return true;
	}

	@Override
	public List<Job> getJobs(VariantModel model, final Long taskId) {
		Component root = model.getRoot();
		final List<Job> list = new ArrayList<Job>();
		List<Component> todo = new ArrayList<>();
		todo.add(root);
		while(!todo.isEmpty()) {
			Component current = todo.remove(0);
			if(current instanceof SWComponent) {
				SWComponent currentsw = (SWComponent) current;
				list.addAll(currentsw.getJobs());
				todo.addAll(currentsw.getSubtypes());
			} else if(current instanceof Resource) {
				if(current instanceof ContainerProvider) {
					ContainerProvider currentcontainer = (ContainerProvider) current;
					Schedule schedule = currentcontainer.getSchedule();
					if(schedule != null) {
						list.addAll(schedule.getJobs());
					}
				}
				todo.addAll(((Resource) current).getSubresources());
			}
		}
		return list;
	}
	
	public JobList getJobsForJson(VariantModel model, final Long taskId) {
		final JobList list = new JobList();
		Function<Job, Void> f = new Function<Job, Void>() {
			@Override
			public Void apply(Job s) {
				SimpleJob simpleJob = parse(s);
				list.add(simpleJob);
				return null;
			}
		};
		CollectionsUtil.map(getJobs(model, taskId), f, true);
		return list;
	}

	/**
	 * @param serializedVariantModel
	 * @param serializedStructModel
	 * @return the deserialized variant model
	 * @throws IOException if something has gone wrong
	 */
	protected VariantModel deserializeModel(String serializedVariantModel,
			String serializedStructModel) throws IOException {
		ResourceSet rs = CCMUtil.createResourceSet();
		return CCMUtil.deserializeGlobalHWVariantModel(rs, serializedStructModel, serializedVariantModel, false);
	}

	protected OutputStream getModelOutputStream(VariantModel model, String filename) throws IOException {
		URI uri = constructFileUri(FileUtils.getWorkspaceDir(), filename);
		FileOutputStream fos = new FileOutputStream(uri.toFileString());
		log.debug("Using " + uri.toFileString());
		return fos;
	}

	protected OutputStream getJobsOutputStream(VariantModel model, String filename) throws IOException {
		URI uri = constructFileUri(FileUtils.getWorkspaceDir(), filename);
		FileOutputStream fos = new FileOutputStream(uri.toFileString());
		log.debug("Using " + uri.toFileString());
		return fos;
	}

}
