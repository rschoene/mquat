/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.haec.theatre.dao.JobDAO;
import org.haec.theatre.dao.JobInfo;
import org.haec.theatre.persistator.JobList;
import org.haec.theatre.persistator.StandardPersistator;
import org.haec.theatre.rest.constants.PathConstants;
import org.haec.theatre.rest.util.AbstractGemResource;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;

@XmlRootElement
class AllJobsResponseInfo {
	
	@XmlElements({ @XmlElement(name = "jobs", type = JobInfo.class) })
	List<JobInfo> infos;
	
	public AllJobsResponseInfo() {
		infos = new ArrayList<>();
	}
	
	public void add(JobInfo tri) {
		infos.add(tri);
	}

	@Override
	public String toString() {
		return "AllTasksResponseInfo [infos=" + infos + "]";
	}
}


/**
 * Resource providing information about jobs in JSON format.
 * @author René Schöne
 */
@Path(PathConstants.pathGem)
public class JobInfoResource extends AbstractGemResource {

	static final Logger log = Logger.getLogger(JobInfoResource.class);
	private JobDAO jobDAO;
	private IGlobalEnergyManager gem;
	
	public JobInfoResource() {
		super("JobInfo");
	}

	@Path("jobs")
	@GET
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response jobs(
			@QueryParam(PathConstants.KEY_APP_NAME) String appName,
			@QueryParam(PathConstants.KEY_TASK_ID) Long taskIdRequest,
			@QueryParam(PathConstants.KEY_JOB_ID) Long jobIdRequest,
			@QueryParam(PathConstants.KEY_URI) String uriRequest,
			@QueryParam(PathConstants.KEY_HOSTNAME) String hostnameRequest
			) {
		AllJobsResponseInfo result = new AllJobsResponseInfo();
		List<JobInfo> jobList = jobDAO.
				getJobs(appName, jobIdRequest, taskIdRequest);
		for(JobInfo info : jobList) {
			Serializable rawResult = gem.getResultOfJob(appName, info.jobId);
			if(rawResult == null) {
				info.result = "null result";
//				info.existsAtGem = false;
			} else if(rawResult instanceof FileProxy) {
				FileProxy proxy = (FileProxy) rawResult;
				File f = proxy.getFile();
				String lemUri = proxy.getLemUri().toString();
				if(uriRequest != null || hostnameRequest != null &&
						!matches(lemUri, uriRequest, hostnameRequest)) {
					continue;
				}
				info.result = f.getAbsolutePath();
				info.host = toHostName(lemUri);
//				info.existsAtGem = true;
			} else {
				info.result = StringUtils.cap(rawResult, 100);
				info.host = "unknown";
//				info.existsAtGem = true;
			}
			result.add(info);
		}
		return build(result);
	}

	private String toHostName(String lemUri) {
		String ip = RemoteOsgiUtil.stripRosgiParts(lemUri);
		String hostname = PlatformUtils.getHostNameOf(ip);
		return hostname != null ? hostname : ip;
	}

	private boolean matches(String actualUri, String uriRequest,
			String hostnameRequest) {
		if(uriRequest == null && hostnameRequest == null) { return true; }
		String ipRequest;
		if(uriRequest == null) {
			ipRequest = PlatformUtils.getIpOf(hostnameRequest);
		}
		else {
			ipRequest = RemoteOsgiUtil.stripRosgiParts(uriRequest);
		}
		String actualIp = RemoteOsgiUtil.stripRosgiParts(actualUri);
		log.debug("request: " + ipRequest + ", actual:" + actualIp);
		return ipRequest.equals(actualIp);
	}

	
	@Path("jobs-local")
	@GET
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response jobsLocal(
			@QueryParam(PathConstants.KEY_APP_NAME) String appName,
			@QueryParam(PathConstants.KEY_TASK_ID) Long taskIdRequest
			) {
		String sw_vm = gem.getCurrentSystemConfiguration(appName);
		ResourceSet rs = CCMUtil.createResourceSet();
		VariantModel model;
		try {
			model = CCMUtil.deserializeSWVariantModel(rs, appName, sw_vm, false);
		} catch (IOException e) {
			log.warn("Problems while deserializing variant model", e);
			return Response.serverError().entity(e).build();
		}
		JobList jobs = new StandardPersistator().getJobsForJson(model, taskIdRequest);
		return Response.ok(jobs).build();
	}
	
	protected void setJobDAO(JobDAO jd) {
		this.jobDAO = jd;
	}
	
	protected void unsetJobDAO(JobDAO jd) {
		this.jobDAO = null;
	}
	
	protected void setGem(IGlobalEnergyManager gem) {
		this.gem = gem;
	}
	
	protected void unsetGem(IGlobalEnergyManager gem) {
		this.gem = null;
	}

}
