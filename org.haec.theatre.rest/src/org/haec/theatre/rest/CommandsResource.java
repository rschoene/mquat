/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;
import org.coolsoftware.theatre.Resettable;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.haec.theatre.utils.BundleUtils;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.sql.SQLUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;

@XmlRootElement
class ResetResult {
	private static final String ERROR_KEY = "error";
	private static final String ERROR_DELIMITER = ";";
	private static final String SUCCESS_MESSAGE = "success";
	private static final String FAILURE_MESSAGE = "failed";
	@XmlElement Map<String, String> result;
	public ResetResult() {
		result = new HashMap<>();
	}
	public void addError(String message) {
		String formerErrors = CollectionsUtil.get(result, ERROR_KEY, "");
		result.put(ERROR_KEY, formerErrors + ERROR_DELIMITER + message);
	}
	public void addResult(Resettable r, boolean success) {
		result.put(r.getName(), success ? SUCCESS_MESSAGE : FAILURE_MESSAGE);
	}
}

/**
 * Resource for commands to control parts of THEATRE.
 * @author René Schöne
 */
@Path("commands")
public class CommandsResource {
	
	public static final String consoleLoggerName = "stdout";
	public static final String dexterGrmName = "DexterGRM";
	private Logger log = Logger.getLogger(CommandsResource.class);
	private BundleContext context;
	
	@PUT
	@Path("dexter")
	@Produces(MediaType.TEXT_PLAIN)
	public Response resetDexterConnections() {
		log.info("Received dexter.");
		boolean success = SQLUtils.recreateAllConnections();
		return success ? Response.ok("success").build() : Response.serverError().build();
	}
	
	@PUT
	@Path("silence")
	@Produces(MediaType.TEXT_PLAIN)
	public Response silenceGrm() {
		log.info("Received silence.");
		Appender appender = Logger.getRootLogger().getAppender(consoleLoggerName);
		Filter f = appender.getFilter();
		String output;
		if(f == null) {
			output = "Silencing " + dexterGrmName + ".";
			appender.addFilter(new Filter() {
				
				@Override
				public int decide(LoggingEvent event) {
					return event.getLocationInformation().getClassName().contains(
							dexterGrmName) ? Filter.DENY : Filter.ACCEPT;
				}
			});
		}
		else {
			output = "Removing all filters for " + dexterGrmName + ".";
			appender.clearFilters();
		}
		return Response.ok(output).build();
	}
	
	@PUT
	@Path("exit")
	@Produces(MediaType.TEXT_PLAIN)
	public Response exitFramework() {
		log.info("Received exit. Shutting down now stopping system bundle.");
		try {
			context.getBundle(0).stop();
		} catch (BundleException e) {
			return Response.serverError().entity(e).build();
		}
		// a bit hacky, wait some reasonable time, until "most" bundles are shut down, then call System.exit()
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// not sure, whether log4j bundle is still running, therefore use use System.out
			e.printStackTrace(System.out);
		}
		System.exit(0);
		return Response.ok().build();
	}
	
	@PUT
	@Path("exit2")
	public void systemExit() {
		log.info("Received exit2. Shutting down now with System.exit().");
		System.exit(0);
	}
	
	@PUT
	@Path("reset")
	@Produces(MediaType.APPLICATION_JSON)
	public Response resetTheatre(@QueryParam("full") boolean full) {
		log.debug("entry");
		ResetResult result = new ResetResult();
		ServiceReference<?>[] refs = null;
		try {
			refs = context.getServiceReferences(Resettable.class.getName(), null);
		} catch (InvalidSyntaxException e) {
			result.addError("weird: " + e.getMessage());
		}
		if(refs != null) { for(ServiceReference<?> ref : refs) {
			Resettable instance = (Resettable) context.getService(ref);
			if(instance != null) {
				log.debug("Resetting instance " + instance);
				result.addResult(instance, instance.reset(full));
			}
			else { log.warn("Instance for " + ref + " is null."); }
		}}
		return Response.ok(result).build();
	}
	
	@PUT
	@Path("grmStop")
	@Produces(MediaType.APPLICATION_JSON)
	public Response grmStop() {
		ServiceReference<?>[] refs;
		try {
			refs = context.getAllServiceReferences(IGlobalResourceManager.class.getName(), null);
		} catch (InvalidSyntaxException e) {
			return Response.serverError().build();
		}
		for(ServiceReference<?> ref : refs) {
			IGlobalResourceManager grm = (IGlobalResourceManager) context.getService(ref);
			if(grm.getClass().getName().contains("Dexter")) {
				// TODO implement
			}
		}
		
		return Response.ok().build();
	}

	protected void activate(ComponentContext ctx) {
		this.context = ctx.getBundleContext();
	}

}
