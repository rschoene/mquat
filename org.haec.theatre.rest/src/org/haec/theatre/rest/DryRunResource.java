/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import java.io.IOException;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.haec.theatre.rest.util.AbstractGemResource;
import org.haec.theatre.utils.FileUtils;

/**
 * Resource for extended API used in dry-runs/reviews.
 * @author René Schöne
 */
@Path("dry-run")
public class DryRunResource extends AbstractGemResource {

	private static final String appName = "org.haec.app.videoTranscodingServer";
	private static final String typeName = "Scaling";
	private IGlobalEnergyManager gem;

	public DryRunResource() {
		super("dry-run");
	}

	@Path("updateContract")
	@PUT
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response updateContract(@QueryParam("path") String pathname) {
		String contract;
		try {
			contract = FileUtils.readAsString(pathname);
		} catch (IOException e) {
			e.printStackTrace();
			return Response.serverError().entity(e.getClass().getName() + ":" + e.getMessage()).build();
		}
		gem.updateContract(appName, typeName, contract);
		return Response.ok(appName + " - " + typeName + " = " + contract).build();
	}
	
	protected void setGem(IGlobalEnergyManager gem) {
		this.gem = gem;
	}
	
	protected void unsetGem(IGlobalEnergyManager gem) {
		this.gem = null;
	}
	
}
