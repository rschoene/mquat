/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.variant.Component;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.haec.theatre.persistator.SimpleHardwareModel;
import org.haec.theatre.persistator.StandardPersistator;
import org.haec.theatre.utils.Cache;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.Function2;

/**
 * Resource providing information about the GRM as XML and JSON.
 * @author René Schöne
 */
@Path("grm")
public class GRMResource {
	
	private static Logger log = Logger.getLogger(GRMResource.class);

	@GET()
	@Path("sm")
	@Produces( {MediaType.TEXT_XML} )
	public Object sm() {
		return grm.getInfrastructureTypes();
	}

	@GET()
	@Path("vm")
	@Produces( {MediaType.TEXT_XML} )
	public Object vm() {
		log.debug("entry");
		return grm.getInfrastructure();
	}
	
	private static Function2<VariantModel, VariantModel, Boolean> lastChangedChecker =
			new Function2<VariantModel, VariantModel, Boolean>() {

		@Override
		public Boolean apply(VariantModel s1, VariantModel s2) {
			Component root1 = s1.getRoot();
			Component root2 = s2.getRoot();
			if(root1 instanceof Resource && root2 instanceof Resource) {
				Long lastChanged1 = ((Resource) root1).getLastChanged();
				Long lastChanged2 = ((Resource) root2).getLastChanged();
				return lastChanged1 != null ? !lastChanged1.equals(lastChanged2) : true;
			}
			return true;
		}
	};
	private static Function<VariantModel, SimpleHardwareModel> model2Json = new Function<VariantModel, SimpleHardwareModel>() {

		@Override
		public SimpleHardwareModel apply(VariantModel s) {
			return StandardPersistator.parse(s);
		}
	};
	private Cache<VariantModel, SimpleHardwareModel> jsonModelCache = new Cache<>(model2Json, lastChangedChecker, false);
	
	@GET()
	@Path("vm-json")
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response vmJson() {
		log.debug("entry");
		VariantModel vm = grm.getInfrastructureAsModel();
		SimpleHardwareModel shm = jsonModelCache.get(vm);
		return Response.ok(shm, MediaType.APPLICATION_JSON).header("Access-Control-Allow-Origin", "*").build();
	}
	
//	private boolean useRosgi = false;
	private IGlobalResourceManager grm;
	
//	protected IGlobalResourceManager getGRM() {
//		return ServiceWorkaround.currentInstance.getGrm();
//		BundleContext context = GemServletActivator.getContext();
//		if(useRosgi) {
//			RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
//			RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(remote,
//					AppUtil.getGRMUri(),
//					IGlobalResourceManager.class.getName());
//			if(srefs == null || srefs.length == 0) {
//				return null;
//			}
//			IGlobalResourceManager grm = (IGlobalResourceManager) remote.getRemoteService(srefs[0]);
//			return grm;
//		} else {
//			ServiceReference<IGlobalResourceManager> ref = context.getServiceReference(IGlobalResourceManager.class);
//			return context.getService(ref);
//		}
//	}
	
	
	protected void setGrm(IGlobalResourceManager grm) {
		this.grm = grm;
	}

	protected void unsetGrm(IGlobalResourceManager grm) {
		this.grm = null;
	}

	
}
