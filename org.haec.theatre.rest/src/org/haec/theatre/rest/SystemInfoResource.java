/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.TheatreCommandProvider;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.haec.theatre.utils.settings.Setting;
import org.haec.theatre.utils.settings.SettingHolder;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

@XmlRootElement
class JavaProperties {
	@XmlElement
	Map<String, String> properties;
	public JavaProperties() { }
	public JavaProperties(Map<String, String> systemProperties) {
		properties = systemProperties;
	}
}

@XmlRootElement
class AvailableCommands {
	@XmlElement List<AvailableCommand> commands;
	Set<String> seenCommandNames;
	public AvailableCommands() {
		commands = new ArrayList<>();
		seenCommandNames = new HashSet<>();
	}
	public void add(AvailableCommand ac) { if(seenCommandNames.add(ac.name)) { commands.add(ac); }}
}

class AvailableCommand {
	@XmlElement String name;
	@XmlElement String description;
	public AvailableCommand() { }
	public AvailableCommand(String name, String description) {
		this.name = name;
		this.description = description;
	}
}

@XmlRootElement
class CommandInvocationResults {
	@XmlElement List<CommandInvocationResult> results;
	@XmlElement String startTime;
	public CommandInvocationResults() {
		results = new ArrayList<>();
		startTime = new SimpleDateFormat().format(Calendar.getInstance().getTime());
	}
	public boolean add(CommandInvocationResult cir) { return results.add(cir); }
}

class CommandInvocationResult {
	@XmlElement String host;
	@XmlElement String result;
	public CommandInvocationResult() { }
	public CommandInvocationResult(String host, String result) {
		this.host = host;
		this.result = result;
	}
	
}

/**
 * General resource for the system status.
 * @author René Schöne
 */
@Path("system")
public class SystemInfoResource {

	private BundleContext context;
	private IGlobalEnergyManager gem;
	private TheatreSettings settings;
	private static Map<String, String> systemProperties;

	static {
		systemProperties = new HashMap<>();
		for(Entry<Object, Object> x : System.getProperties().entrySet()) {
			systemProperties.put(x.getKey().toString(), StringUtils.cap(x.getValue(), 500));
		}
	}
	
	private static Logger log = Logger.getLogger(SystemInfoResource.class);

	@Path("pr")
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getJavaProperties() {
		return Response.ok(new JavaProperties(systemProperties)).build();
	}

	@Path("cmd/{cmd}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
	public Response sendCommandToLems(@PathParam("cmd") String command, @QueryParam("host") String host,
			@QueryParam("param") final List<String> params) {
//		if(!checkValidCommand(command)) { return makeAvailableCommands(); }

		String filter = "(" + TheatreCommandProvider.PROPERTY_COMMAND_NAME + "=*" + command + "*)";
		log.debug("Filter= " + filter);
		List<TheatreCommandProvider> tcpl = new ArrayList<>();
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
		Filter filterFilter;
		try {
			if(command.equals("help")) { filterFilter = null; }
			else { filterFilter = context.createFilter(filter); }
		} catch (InvalidSyntaxException e) {
			return Response.status(Status.BAD_REQUEST).entity("InvalidFilter:" + e.getMessage()).build();
		}
		if(host != null) {
			// use r-osgi to constrain by uri
			// check if hostname found
			String ip = PlatformUtils.getIpOf(host);
			if(ip == null) { ip = host; }
			RemoteServiceReference[] refs = RemoteOsgiUtil.getRemoteServiceReferences(remote,
					ip, TheatreCommandProvider.class.getName(), filterFilter);
			if(refs == null) {
				return filterFilter == null ?
						Response.status(Status.SERVICE_UNAVAILABLE).entity("No command providers found at " + ip).build() :
						Response.status(Status.BAD_REQUEST).entity("No provider found for "+command+" at " + ip).build();
			}
			for(RemoteServiceReference ref : refs) {
				TheatreCommandProvider tcProv = (TheatreCommandProvider) remote.getRemoteService(ref);
				if(tcProv != null) { tcpl.add(tcProv); }
			}
		}
		else {
			List<java.net.URI> uriList = gem.getLocalMgrs();
			// add gem to search space
			uriList.add(URI.create(gem.getUri()));
			for(java.net.URI uri : uriList) {
				RemoteServiceReference[] refs = RemoteOsgiUtil.getRemoteServiceReferences(
						remote, uri.toString(), TheatreCommandProvider.class.getName());
				if(refs == null) {
					return filterFilter == null ?
							Response.status(Status.SERVICE_UNAVAILABLE).entity("No command providers found").build() :
							Response.status(Status.BAD_REQUEST).entity("No provider found for "+command).build();
				}
				log.debug("Got " + refs.length + " refs at " + uri);
				for(RemoteServiceReference ref : refs) {
					TheatreCommandProvider tcProv = (TheatreCommandProvider) remote.getRemoteService(ref);
					if(tcProv != null) { tcpl.add(tcProv); }
				}
			}
			log.debug("Got " + tcpl.size() + " providers.");
//			Collection<ServiceReference<TheatreCommandProvider>> refs;
//			try { refs = context.getServiceReferences(TheatreCommandProvider.class, filter);
//			} catch (InvalidSyntaxException e) {
//				return Response.status(Status.BAD_REQUEST).entity("InvalidFilter:" + e.getMessage()).build();
//			}
//			for(ServiceReference<TheatreCommandProvider> ref : refs) {
//				TheatreCommandProvider tcProv = context.getService(ref);
//				if(tcProv != null) { tcpl.add(tcProv); }
//			}
		}

		if(command.equals("help")) {
			AvailableCommands acs = new AvailableCommands();
			acs.add(new AvailableCommand("help", "shows this help and exists."));
			for(TheatreCommandProvider tcProv : tcpl) {
				acs.add(new AvailableCommand(tcProv.getCommandName(), tcProv.getDescription()));
			}
			return Response.ok(acs).build();
		}
		ExecutorService pool = Executors.newCachedThreadPool();
		CommandInvocationResults result = new CommandInvocationResults();
		final String finalCommand = command;
		final String[] parameters = params.toArray(new String[params.size()]);
		List<Callable<String>> callables = 
		CollectionsUtil.map(tcpl, new Function<TheatreCommandProvider, Callable<String>>() {
			@Override
			public Callable<String> apply(final TheatreCommandProvider s) {
				return new Callable<String>() { @Override public String call() throws Exception {
					return s.execute(finalCommand, parameters); }
				};
			}
		});
		List<Future<String>> futures;
		try {
			futures = pool.invokeAll(callables);
		} catch (InterruptedException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}
		for(Future<String> future : futures) {
			String resultDescription = null;
			try {
				resultDescription = future.get();
			} catch (InterruptedException | ExecutionException e) {
				result.add(new CommandInvocationResult("unknown host", e.getMessage()));
			}
			if(resultDescription == null) {
				result.add(new CommandInvocationResult("nullResult", "null"));
			}
			else {
				int hostDelimIndex = resultDescription.indexOf(TheatreCommandProvider.HOST_DELIMITER);
				if(hostDelimIndex > -1) {
					result.add(new CommandInvocationResult(resultDescription.substring(0, hostDelimIndex),
							resultDescription.substring(hostDelimIndex+1)));
				}
				else {
					result.add(new CommandInvocationResult("invalid format", resultDescription));
				}
			}
		}
		return Response.ok(result).build();
	}
	
	class SimpleSetting {
		String key;
		String currentVal;
		boolean isDefault;
		public SimpleSetting(String key, String currentVal, boolean isDefault) {
			super();
			this.key = key;
			this.currentVal = currentVal;
			this.isDefault = isDefault;
		}
	}
	
	@Path("settings")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response settings() {
		List<Setting<?>> list = SettingHolder.getSettings((SettingHolder) settings);
		// mapping is necessary to avoid circular references while serializing
		Function<Setting<?>, SimpleSetting> f = new Function<Setting<?>, SystemInfoResource.SimpleSetting>() {

			@Override
			public SimpleSetting apply(Setting<?> s) {
				return new SimpleSetting(s.getName(), s.get().toString(), s.isDefault());
			}
		};
		List<SimpleSetting> simpleList = CollectionsUtil.map(list, f);
		return Response.ok(simpleList).build();
	}

	protected void setTheatreSettings(TheatreSettings ts) {
		this.settings = ts;
	}

	protected void unsetTheatreSettings(TheatreSettings ts) {
		this.settings = null;
	}
	
	protected void setGem(IGlobalEnergyManager gem) {
		this.gem = gem;
	}
	
	protected void unsetGem(IGlobalEnergyManager gem) {
		this.gem = null;
	}
	
	protected void activate(ComponentContext ctx) {
		this.context = ctx.getBundleContext();
	}

}
