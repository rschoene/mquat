/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest.constants;

/**
 * Constants for REST services (paths and key names).
 * @author René Schöne
 */
public interface PathConstants {

	public static final String KEY_SOLVER = "solver";
	public static final String KEY_APP_NAME = "app";
	public static final String KEY_COMP_NAME = "comp";
	public static final String KEY_PORT_NAME = "port";
	public static final String KEY_NFR_NAMES = "nfrName";
	public static final String KEY_NFR_TYPES = "nfrType";
	public static final String KEY_NFR_VALUES = "nfrValue";
	public static final String KEY_MP_NAMES = "mpName";
	public static final String KEY_MP_VALUES = "mpValue";
	public static final String KEY_PARAM = "p";
	public static final String KEY_OPTIMIZE = "optimize";
	public static final String KEY_TASK_ID = "taskId";
	public static final String KEY_JOB_ID = "jobId";
	public static final String KEY_URI = "slaveUri";
	public static final String KEY_HOSTNAME = "slaveHostname";

	public static final String PREFIX_ERROR = "ERROR:";
	public static final String PREFIX_SUCCESS = "SUCCESS:";
	
	public static final String pathGem = "gem";
	
}
