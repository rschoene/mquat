/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest.util;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.haec.theatre.utils.UtilsSettings;

/**
 * Utility class providing methods concerning REST responses.
 * @author René Schöne
 */
public class RestUtils {

	private static Logger log = Logger.getLogger(UtilsSettings.LOGGER_NAME);
	
	public static <T> T getResponse(Builder b, GenericType<T> genericType) {
		return getResponse(b, genericType, log);
	}

	public static <T> T getResponse(Builder b, GenericType<T> genericType, Logger log) {
//		try {
			return b.get(genericType);
//		} catch (UniformInterfaceException e) {
//			if(log.isDebugEnabled()) {
//				ClientResponse resp = e.getResponse();
//				if(resp.hasEntity()) {
//					log.debug("Got "+resp.getStatus()+":"+StringUtils.cap(resp.getEntity(String.class), 400));
//				}
//			}
//			throw e;
//		}
	}

	public static <T> T putResponse(Builder b, Entity<?> e, Class<T> clazz) {
		return putResponse(b, e, clazz, log);
	}

	public static <T> T putResponse(Builder b, Entity<?> e, Class<T> clazz, Logger log) {
//		try {
			return b.put(e, clazz);
//		} catch (UniformInterfaceException e) {
//			if(log.isDebugEnabled()) {
//				ClientResponse resp = e.getResponse();
//				if(resp.hasEntity()) {
//					log.debug("Got "+resp.getStatus()+":"+StringUtils.cap(resp.getEntity(String.class), 400));
//				}
//			}
//			throw e;
//		}
	}
	
	/**
	 * Build the entity for the given type with some advantageous headers.
	 * @param entity the object to pack
	 * @param type the type to pack the object with
	 */
	public static Response build(Object entity, String type) {
		return Response.ok(entity, type).header("Access-Control-Allow-Origin", "*").build();
	}

}
