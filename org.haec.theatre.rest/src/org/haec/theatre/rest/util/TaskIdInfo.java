package org.haec.theatre.rest.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TaskIdInfo {
	@XmlElement(name="taskId")
	long taskId;
	
	public TaskIdInfo() { } // needed by JAXB
	public TaskIdInfo(long taskId) {
		this.taskId = taskId;
	}
	
}