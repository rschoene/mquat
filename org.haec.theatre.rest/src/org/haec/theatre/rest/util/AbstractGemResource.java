/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest.util;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.haec.theatre.utils.StringUtils;

/**
 * Abstract resource providing access to common functionality for resources accessing the GEM.
 * @author René Schöne
 */
public abstract class AbstractGemResource {
	
	protected String title;
	protected final String script = "<script language=\"javascript\">\n"
			+ "<!--\nfunction toggle(control)\n{\n"
			+ "var elem = document.getElementById(control);\n"
			+ "if(elem.style.display == \"none\")\n"
			+ "{\nelem.style.display = \"block\";\n"
			+ "}\nelse\n{\nelem.style.display = \"none\";\n}\n}\n//-->\n</script>\n";

	public AbstractGemResource(String title) {
		this.title = title;
	}

	protected Response exit(StringBuilder sb) {
		printHttpFooter(sb);
		return Response.status(200).entity(sb.toString()).build();
	}

	protected void printHttpHeader(StringBuilder sb) {
		sb.append(script);
		sb.append("<http><head><title>"+title+"</title></head><body>");
	}
	
	protected String createToggleLink(String control, String text) {
		return "<p><a href=\"javascript:toggle('"+control+"')\">"+text+"</a></p>";
	}
	
	protected String startToggle(String control, boolean initiallyHidden) {
		return "<div id=\""+control+"\""+ (initiallyHidden ? "style=\"DISPLAY: none\"" : "") +">";
	}
	
	protected String endToggle() {
		return "</div>";
	}
	
	/**
	 * @param r the Response to inspect
	 * @return <code>r.getStatus() + ": " + StringUtils.cap(r.getEntity(), 200)</code>
	 */
	protected String loggingContent(Response r) {
		return r.getStatus() + ": " + StringUtils.cap(r.getEntity(), 200);
	}

//	protected IGlobalEnergyManager getGem() {
//		return ServiceWorkaround.currentInstance.getGem();
//		BundleContext context = GemServletActivator.getContext();
//		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
//		RemoteServiceReference[] srefs = RemoteOsgiUtil.getRemoteServiceReferences(remote,
//				AppUtil.getGEMUri(),
//				IGlobalEnergyManager.class.getName());
//		if(srefs == null || srefs.length == 0) {
//			getLogger().error("Could not find GEM.");
//			return null;
//		}
//		IGlobalEnergyManager gem = (IGlobalEnergyManager) remote.getRemoteService(srefs[0]);
//		return gem;
//	}

	private void printHttpFooter(StringBuilder sb) {
		sb.append("</body></http>");
	}
	
//	protected int getConnectionIndexToDexter() {
//		return GemServletActivator.getConnectionIndex();
//	}

//	protected Connection getConnection() {
//		return SQLUtils.getConnection(getConnectionIndexToDexter());
//	}

	protected Response build(Object info) {
		return RestUtils.build(info, MediaType.APPLICATION_JSON);
	}

}
