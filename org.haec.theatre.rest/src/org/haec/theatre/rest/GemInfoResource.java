/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.haec.theatre.rest.GemInfoResource.AppList.App;
import org.haec.theatre.rest.GemInfoResource.AppList.App.Comp;
import org.haec.theatre.rest.GemInfoResource.AppList.App.Comp.Port;
import org.haec.theatre.rest.GemInfoResource.AppList.App.Comp.Port.Param;
import org.haec.theatre.rest.GemInfoResource.LemList.Lem;
import org.haec.theatre.rest.GemInfoResource.LemList.Lem.QueueSlot;
import org.haec.theatre.rest.constants.PathConstants;
import org.haec.theatre.rest.util.AbstractGemResource;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;
import ch.ethz.iks.r_osgi.URI;

/**
 * Resource providing information about the GEM as HTML.
 * TODO information should actually be provided as JSON and rendered at client-side.
 * @author René Schöne
 */
@Path(PathConstants.pathGem)
public class GemInfoResource extends AbstractGemResource {

	private static final char[] targets = new char[]{' ', '\n', '<', '>', '"'};
	private static final String[] replacements = new String[]{"&nbsp;", "<br>", "&lt;", "&gt;", "\\\""};
	private static final String nl = "\n";
	private Logger log = Logger.getLogger(GemInfoResource.class);
	private BundleContext context;
	private IGlobalEnergyManager gem;
	private static ExecutorService pool = Executors.newFixedThreadPool(10);

	
	public GemInfoResource() {
		super("Console for the THEATRE GEM");
	}
	
	@Path("info")
	@GET
	@Produces({MediaType.TEXT_HTML,MediaType.TEXT_PLAIN})
	public Response info() {
		log.debug("entry");
		StringBuilder out = new StringBuilder();
//			addTestTable(out);
		printDate(out);
		printHttpHeader(out);
		AppList list = printGemAppInfos(out, gem);
		printLemInfos(out, gem, list);
		printIpCacheInfos(out);
		return exit(out);
	}

	protected void printDate(StringBuilder out) {
		out.append("<p>").append(new SimpleDateFormat().format(Calendar.getInstance().getTime())).append("</p>");
	}

	protected AppList printGemAppInfos(StringBuilder out, IGlobalEnergyManager gem) {
		log.debug("Getting gem info.");
		log.debug("before getRegisteredApplications");
		String control = "appList";
		out.append(createToggleLink(control, "Show/Hide list of apps"));
		Set<String> apps = gem.getRegisteredApplications().keySet();
		AppList list = new AppList();
		out.append(startToggle(control, true));
		// table header
		for(String appName : apps) {
			out.append("<table class=\"tablesorter nicetable\" border=\"1\">"
					+ "<tr><th>AppName</th><th>CompName</th><th>PortName</th><th>PortParamName</th></tr>").append(nl);
			App a = list.addApp(appName);
			log.debug("before getComponentsOfApp " + appName);
			List<String> components = gem.getComponentsOfApp(appName);
			for(String compId : components) {
				Comp c = a.addComp(compId);
				log.debug("before getComponentPorts " + compId);
				List<String> ports = gem.getComponentPorts(appName, compId);
				for(String portName : ports) {
					Port p = c.addPort(portName);
					log.debug("before getPortParameters" + portName);
					List<String> params = gem.getPortParameters(appName, compId, portName);
					for(String paramName : params) {
						p.addParam(paramName);
					}
					if(params.isEmpty()) { p.setNoChilds(); }
				}
				if(ports.isEmpty()) { c.setNoChilds(); }
			}
			if(components.isEmpty()) { a.setNoChilds(); }
			out.append("<tr>" + a.getTd());
			boolean firstComp = true;
			for(Comp c : a) {
				if(firstComp) { firstComp = false; }
				else { out.append("<tr>"); }
				out.append(c.getTd());
				boolean firstPort = true;
				for(Port p : c) {
					if(firstPort) { firstPort = false; }
					else { out.append("<tr>"); }
					out.append(p.getTd());
					boolean firstParam = true;
					for(Param pp : p) {
						if(firstParam) { firstParam = false; }
						else { out.append("<tr>"); }
						out.append("<td>" + pp.paramName + "</td>");
						out.append("</tr>").append(nl);
					}
					if(firstParam) { /* no param was found */ out.append("</tr>").append(nl); }
				}
				if(firstPort) { /* no port was found */ out.append("</tr>").append(nl); }
			}
			if(firstComp) { /* no component was found */ out.append("</tr>").append(nl); }
			out.append("</table>").append(nl);
			String variantModel = gem.getCurrentSystemConfiguration(appName);
			out.append("<br><p>VariantModel:</p><p>").append(variantModel).append("</p>").append(nl);
		}
		out.append(endToggle());
		return list;
	}
	
	interface ContentHolder {
		public String getContent();
	}
	abstract class ListHolder<S> implements Iterable<S>, ContentHolder {
		int rowSpan = 0;
		int inheritedRowSpan = 0;
		List<S> list = new ArrayList<S>();
		ListHolder<?> parent;
		public ListHolder(ListHolder<?> parent) {
			this.parent = parent;
		}
		protected S r(S e) {
			list.add(e);
			incRowSpan();
			return e;
		}
		protected void incRowSpan() {
			rowSpan += 1;
			if(parent != null) {
				parent.incInheritedRowSpan();
			}
		}
		protected void incInheritedRowSpan() {
			inheritedRowSpan += 1;
			if(parent != null) {
				parent.incInheritedRowSpan();
			}
		}
		public Iterator<S> iterator() {
			return list.iterator();
		}
		public String getTd() {
			return "<td rowspan=\""+getRowspan()+"\">" + getContent()+ "</td>";
		}
		protected int getRowspan() {
			return Math.max(Math.max(rowSpan, inheritedRowSpan), 1);
		}
		public String getContent() {
			return "";
		}
		public S add(S newChild) {
			return r(newChild);
		}
		public void printTo(PrintWriter out) {
			//TODO generic print method
		}
		public void setNoChilds() {
			if(!list.isEmpty()) {
				log.warn("Childs list is not empty.");
			}
			if(rowSpan != 0) {
				log.warn("rowSpan is not zero, but " + rowSpan);
			}
			incRowSpan();
		}
	}
	
	class AppList extends ListHolder<App> {
		class App extends ListHolder<Comp> {
			class Comp extends ListHolder<Port> {
				class Port extends ListHolder<Param> {
					class Param {
						String paramName;
						public Param(String paramName) { this.paramName = paramName; }
						String getPort() { return portName; }
					}
					String portName;
					public Port(String portName) { super(Comp.this); this.portName = portName; }
					Param addParam(String paramName) { return r(new Param(paramName)); }
					@Override
					public String getContent() {
						return portName;
					}
				}
				String compName;
				public Comp(String compName) { super(App.this); this.compName = compName; }
				Port addPort(String portName) { return r(new Port(portName)); }
				@Override
				public String getContent() {
					return compName;
				}
			}
			String appName;
			public App(String appName) { super(AppList.this); this.appName = appName; }
			Comp addComp(String compName) { return r(new Comp(compName)); }
			@Override
			public String getContent() {
				return appName;
			}
		}
		public AppList() { super(null); }
		App addApp(String appName) { return r(new App(appName)); }
	}

	

//	public <S> S r(List<S> list, S e) {
//		list.add(e);
//		return e;
//	}
	
	/**
	 * Update helper class with information of real LEM. Reports error with return value.
	 * @param lemHelper the helper to update
	 * @param lemUri the URI to the LEM
	 * @return <code>null</code> on success, or a error message
	 */
	private String updateLemHelper(Lem lemHelper, java.net.URI lemUri) {
		ILocalEnergyManager lem;
		try {
			lem = getLem(lemUri);
		} catch (Exception e) {
			return ("Error while connecting to " + lemUri + ": " + e.getMessage());
		}
		// queue information
		log.debug("before getCallIdsInQueue for " + lemUri);
		long[] jobIds = lem.getJobIdsInQueue();
		for(long jobId : jobIds) {
			QueueSlot slot = lemHelper.new QueueSlot();
			slot.jobId = jobId;
			slot.finished = lem.isFinished(jobId);
			slot.implName = lem.getImplName(jobId);
			slot.result = lem.getResult(jobId);
			lemHelper.queue.add(slot);
		}
		return null;
	}

	protected void printLemInfos(StringBuilder out, IGlobalEnergyManager gem, AppList appList) {
		log.debug("Getting lem infos.");
		log.debug("before getLocalMgrs");
		List<java.net.URI> listOfLems = gem.getLocalMgrs();
		LemList list = new LemList();
		final List<String> errors = new ArrayList<String>();
		String control = "lemInfos";
		out.append(createToggleLink(control, "Show/Hide list of LEMs"));
		out.append(startToggle(control, false)).append("<table class=\"nicetable\" border=\"1\"><tr>")
			.append("<th>LemUri</th><th>Hostname</th><th>Queue</th></tr>").append(nl); // table header
		
		List<Future<?>> futures = new ArrayList<>();
		// start asynchronous information retrival
		for(final java.net.URI lemUri : listOfLems) {
			final Lem lemHelper = list.addLem(lemUri.toString());
			futures.add(pool.submit(new Runnable(){ public void run() {
				String e = updateLemHelper(lemHelper, lemUri);
				if(e != null) { errors.add(e); }
			}}));
		}
		// wait for threads to finish
		for (int i = 0; i < futures.size(); i++) {
			try {
				futures.get(i).get();
				java.net.URI lemUri = listOfLems.get(i);
				// print information
				Lem lemHelper = list.getLem(lemUri.toString());
//				lemHelper.rowSpan = Math.max(lemHelper.rowSpan, lemHelper.queue.size());
				out.append("<tr><td>").append(lemHelper.getContent()).append("</td>");
				out.append("<td>").append(getHostname(lemHelper.lemUri)).append("</td>");
				out.append("<td><ul>");
				for(QueueSlot slot : lemHelper.queue) {
					out.append("<li>").append(slot.implName).append("(").append(slot.jobId);
					if(slot.finished) {
						out.append(") finished with ").append(slot.result);
					}
					else {
						out.append(") executing");
					}
					out.append("</li>");
				}
				out.append("</ul></td></tr>").append(nl);
			} catch (InterruptedException e) {
				log.debug("Interrupted while getting information on " + listOfLems.get(i), e);
			} catch (ExecutionException e) {
				log.debug("Error while getting information on " + listOfLems.get(i), e);
			}
		}
		out.append("</table><br>").append(nl);
		for(String e : errors) {
			if(e != null) { out.append("<p>").append(asError(e)).append("</p>").append(nl); }
		}
		out.append(endToggle());
	}
	
	private String getHostname(String lemUri) {
		String ip = RemoteOsgiUtil.stripRosgiParts(lemUri);
		return PlatformUtils.getHostNameOf(ip);
	}

	private String asError(String message) {
		return "<font color=\"#FF0000\">" + encode(message) + "</font>";
	}
	
	class LemList extends ListHolder<Lem> {
		class Lem implements ContentHolder {
			class QueueSlot {
				String implName;
				long jobId;
				boolean finished;
				Object result;
			}
			String lemUri;
			List<QueueSlot> queue = new ArrayList<QueueSlot>();
			public Lem(String lemUri) { super(); this.lemUri = lemUri; }
			@Override
			public String getContent() {
				return lemUri.toString();
			}
		}
		public LemList() { super(null); }
		public Lem addLem(String lemUri) { return r(new Lem(lemUri)); }
		public Lem getLem(String lemUri) { 
			for(Lem lem : list) { if(lem.lemUri.equals(lemUri)) { return lem; } }
			return null;
		}
	}

	private String encode(String string) {
		return StringUtils.replaceMultiple(string, targets, replacements);
	}
	
	<I> I getService(Class<I> clazz, String uriAsString) throws Exception {
		URI uri = new URI(uriAsString);
		return getService(clazz, uri);
	}
	
	<I> I getService(Class<I> clazz, URI uri) throws Exception {
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(context);
		final RemoteServiceReference[] srefs = remote.getRemoteServiceReferences(uri, clazz.getName(), null);
		if(srefs.length > 0) {
			return clazz.cast(remote.getRemoteService(srefs[0]));
		}
		else {
			log.warn(String.format("No %s found at uri %s", clazz.getName(), uri));
			return null;
		}
	}

	protected ILocalEnergyManager getLem(java.net.URI lemUri) throws Exception {
//		URI uri = RemoteOsgiUtil.createRemoteOsgiUri(lemUri.toString());
		URI uri = URI.create(lemUri.toString());
		return getService(ILocalEnergyManager.class, uri);
	}

	protected void printIpCacheInfos(StringBuilder out) {
		String cacheAsString = RemoteOsgiUtil.getCacheAsString();
		String control = "ipCache";
		out.append(createToggleLink(control, "Show/Hide cached IPs on the master"));
		out.append(startToggle(control, true));
		// process them. assume pattern <name>":"(<ip>|"null")
		out.append("<table class=\"nicetable\" border=\"1\"><tr><th>Name</th><th>IP</th></tr>").append(nl);
		for(String line : StringUtils.tokenize(cacheAsString, "\n")) {
			if(line.isEmpty()) {
				continue;
			}
			out.append("<tr>").append(nl);
			for(String tok : StringUtils.tokenize(line, ":")) {
				out.append("<td>" + tok + "</td>");
			}
			out.append("</tr>").append(nl);
		}
		out.append("</table><br>").append(nl).append(endToggle());
	}
	
	protected void setGem(IGlobalEnergyManager gem) {
		this.gem = gem;
	}
	
	protected void unsetGem(IGlobalEnergyManager gem) {
		this.gem = null;
	}
	
	class SimpleSlave {
		String name = "unknown";
		java.net.URI uri;
		public SimpleSlave(java.net.URI uri) {
			super();
			this.uri = uri;
		}
	}

	@Path("connectedSlaves")
	@GET
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response connectedSlaves() {
		List<SimpleSlave> result = new ArrayList<>();
		for(java.net.URI uri : gem.getLocalMgrs()) {
			SimpleSlave slave = new SimpleSlave(uri);
			String ip = RemoteOsgiUtil.stripRosgiParts(uri.toString());
			String name = PlatformUtils.getHostNameOf(ip);
			if(name != null) { 
				slave.name = name;
			}
			result.add(slave);
		}
		return Response.ok(result).build();
	}

	@Path("sm")
	@GET
	@Produces( {MediaType.TEXT_XML} )
	public Response sm(@QueryParam(PathConstants.KEY_APP_NAME) String appName) {
		String sm_ser = gem.getApplicationModel(appName);
		return Response.ok(sm_ser).build();
	}

	@Path("vm")
	@GET
	@Produces( {MediaType.TEXT_XML} )
	public Response vm(@QueryParam(PathConstants.KEY_APP_NAME) String appName) {
		String vm_ser = gem.getCurrentSystemConfiguration(appName);
		return Response.ok(vm_ser).build();
	}
	
	protected void activate(ComponentContext ctx) {
		this.context = ctx.getBundleContext();
	}
	
	private String wrapHtml(String content) {
		return "<html><body>"+content+"</body></html>";
	}

	// apps
	
	@Path("json/apps")
	@GET
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response appsAsJson() {
		Map<String, String> models = gem.getRegisteredApplications();
		Response r = Response.ok(models.keySet()).build();
		log.debug(loggingContent(r));
		return r;
	}
	
	@Path("apps")
	@GET
	@Produces( {MediaType.TEXT_HTML} )
	public Response apps() {
		Map<String, String> models = gem.getRegisteredApplications();
		String content;
		if(models.isEmpty()) {
			content = "<p>No apps registered</p>";
		} else {
			StringBuilder sb = new StringBuilder("<ul><li>");
			Function<String, String> makeLink = new Function<String, String>() {
				@Override
				public String apply(String s) {
					return "<a href=\"" + s + "\">" + s + "</a>";
				}
			};
			sb.append(StringUtils.joinIterable("</li><li>",
					CollectionsUtil.map(models.keySet(), makeLink)));
			sb.append("</li></ul>");
			content = sb.toString();
		}
		Response r = Response.ok(wrapHtml(content)).build();
		log.debug(loggingContent(r));
		return r;
	}
	
	// components
	
	@Path("json/{"+PathConstants.KEY_APP_NAME + "}/components")
	@GET
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response componentsAsJson(@PathParam(PathConstants.KEY_APP_NAME) String appName) {
		List<String> components = gem.getComponentsOfApp(appName);
		Response r = Response.ok(components).build();
		log.debug(loggingContent(r));
		return r;
	}
	
	@Path("{"+PathConstants.KEY_APP_NAME + "}")
	@GET
	@Produces( {MediaType.TEXT_HTML} )
	public Response components(@PathParam(PathConstants.KEY_APP_NAME) final String appName) {
		String content;
		List<String> components = gem.getComponentsOfApp(appName);
		if(components == null || components.isEmpty()) {
			content = "<p>No components found</p>";
		} else {
			Function<String, String> makeLink = new Function<String, String>() {
				@Override
				public String apply(String s) {
					return "<a href=\"" + appName + "/" + s + "\">" + s + "</a>";
				}
			};
			StringBuilder sb = new StringBuilder("<ul><li>");
			sb.append(StringUtils.joinIterable("</li><li>",
					CollectionsUtil.map(components, makeLink)));
			sb.append("</li></ul>");
			content = sb.toString();
		}
		Response r = Response.ok(wrapHtml(content)).build();
		log.debug(loggingContent(r));
		return r;
	}
	
	@Path("json/{"+PathConstants.KEY_APP_NAME + "}/{comp}/contract")
	@GET
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response contractAsJson(@PathParam(PathConstants.KEY_APP_NAME) String appName,
			@PathParam("comp") String compName) {
		Map<String, String> contracts = gem.getContractsForComponent(appName, compName);
		Response r = Response.ok(contracts).build();
		log.debug(loggingContent(r));
		return r;
	}
	
	@Path("{"+PathConstants.KEY_APP_NAME + "}/{comp}")
	@GET
	@Produces( {MediaType.TEXT_HTML} )
	public Response contract(@PathParam(PathConstants.KEY_APP_NAME) String appName,
			@PathParam("comp") String compName) {
		Map<String, String> contracts = gem.getContractsForComponent(appName, compName);
		String content;
		if(contracts == null || contracts.isEmpty()) {
			content = "<p>No contracts found for" + compName + "</p>";
		} else {
			StringBuilder sb = new StringBuilder("<ul><li>");
			Function<Entry<String, String>, String> makeContract =
					new Function<Entry<String, String>, String>() {
				@Override
				public String apply(Entry<String, String> s) {
					return s.getKey() + ": <pre>" + s.getValue() + "</pre>";
				}
			};
			sb.append(StringUtils.joinIterable("</li><li>",
					CollectionsUtil.map(contracts.entrySet(), makeContract)));
			sb.append("</li></ul>");
			content = sb.toString();
		}
		Response r = Response.ok(wrapHtml(content)).build();
		log.debug(loggingContent(r));
		return r;
	}
	
	@Path("json/{"+PathConstants.KEY_APP_NAME + "}/{comp}/ports")
	@GET
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response portsAsJson(@PathParam(PathConstants.KEY_APP_NAME) String appName,
			@PathParam("comp") String compName) {
		List<String> components = gem.getComponentPorts(appName, compName);
		Response r = Response.ok(components).build();
		log.debug(loggingContent(r));
		return r;
	}

}
