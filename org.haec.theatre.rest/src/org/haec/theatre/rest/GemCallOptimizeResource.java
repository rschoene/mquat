/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.util.AsyncExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ErrorResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResultSerializer;
import org.coolsoftware.theatre.energymanager.util.SyncExecuteResult;
import org.haec.theatre.rest.constants.PathConstants;
import org.haec.theatre.rest.util.AbstractGemResource;
import org.haec.theatre.rest.util.TaskIdInfo;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.StringUtils;

@XmlRootElement
class AsyncResponseInfo {
	@XmlElement(name="taskId")
	long taskId;
	@XmlElement(name="jobId")
	long jobId;
	@XmlElement(name="uri")
	String uri;
	public AsyncResponseInfo() { } // needed by JAXB
	public AsyncResponseInfo(long taskId, long jobId, String uri) {
		this.taskId = taskId;
		this.jobId = jobId;
		this.uri = uri;
	}
	
}

@XmlRootElement
class SyncResponseInfo {
	@XmlElement(name="filename")
	String filename;
	@XmlElement(name="uri")
	String uri;
	public SyncResponseInfo() { } // needed by JAXB
	public SyncResponseInfo(String filename, String uri) {
		this.filename = filename;
		this.uri = uri;
	}
	
}

/**
 * Resource providing the main functionality of the GEM (its optimize method).
 * Services subclassing this must set {@link IGlobalEnergyManager} as a reference using
 * <ul>
 * <li> bind = {@link #setGem(IGlobalEnergyManager)}</li>
 * <li> unbind = {@link #unsetGem(IGlobalEnergyManager)}</li>
 * </ul>
 * @author René Schöne
 */
@Path(PathConstants.pathGem)
public class GemCallOptimizeResource extends AbstractGemResource {

	private final Logger log = Logger.getLogger(GemCallOptimizeResource.class);
	private IGlobalEnergyManager gem;

	public GemCallOptimizeResource() {
		super("Call of gem.optimize");
	}

	@Path( "optimize" )
	@POST
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response optimize(
			@Context UriInfo uriInfo,
			@DefaultValue("ilp") @QueryParam(PathConstants.KEY_SOLVER) String solver,
			@QueryParam(PathConstants.KEY_APP_NAME) String appName,
			@QueryParam(PathConstants.KEY_COMP_NAME) String compName,
			@QueryParam(PathConstants.KEY_PORT_NAME) String portName,
			@QueryParam(PathConstants.KEY_NFR_NAMES) List<String> nfrNames,
			@QueryParam(PathConstants.KEY_NFR_TYPES) List<String> nfrTypes,
			@QueryParam(PathConstants.KEY_NFR_VALUES) List<String> nfrValues,
			@QueryParam(PathConstants.KEY_MP_NAMES) List<String> mpNames,
			@QueryParam(PathConstants.KEY_MP_VALUES) List<String> mpValues,
			@QueryParam(PathConstants.KEY_PARAM) List<String> params,
			@DefaultValue("true") @QueryParam(PathConstants.KEY_OPTIMIZE) boolean optimize
			) {
		// create request
		String request = makeRequest(solver, appName, compName, portName,
				nfrNames, nfrTypes, nfrValues, mpNames, mpValues);
		log.debug("request=" + request);
		// extract parameters
		Object[] userParams = makeUserParams(params);
		if(userParams == null) {
			Response r = Response.status(Status.BAD_REQUEST).entity("Could not parse parameters").build();
			log.debug(loggingContent(r));
			return r;
		}
		log.debug("params=" + Arrays.toString(userParams));
		return fire(request, true, optimize, userParams);
	}

	protected Response fire(String request, boolean returnTaskId, boolean optimize, Object[] userParams) {
		// connect to gem
		if(gem == null) {
			Response r = Response.status(Status.SERVICE_UNAVAILABLE).entity("Could not connect to GEM.").build();
			log.debug(loggingContent(r));
			return r;
		}
		
		// call optimize method
		Serializable s;
		try {
			s = gem.run(request, returnTaskId, optimize, userParams);
		}
		catch(Exception e) {
			s = null;
		}
		if(s == null) {
			Response r = Response.status(Status.INTERNAL_SERVER_ERROR).entity("Got null as optimization result.").build();
			log.debug(loggingContent(r));
			return r;
		}
		if(returnTaskId) {
			long taskId = (Long) s;
			return Response.ok(new TaskIdInfo(taskId),
					MediaType.APPLICATION_JSON).build();
		} else {
			// expected result as SyncExecuteResult
			// return number of workers selected in last ILP
			ExecuteResult ee = ExecuteResultSerializer.deserialize(s);
			int tries = 5;
			while(tries-- > 0 && !ee.isResultReady()) {
				try { Thread.sleep(1000);
				} catch (InterruptedException e) {
					break;
				}
			}
			Serializable result = ee.getResult();
			return processResult(request, userParams, result);
		}
	}

	/**
	 * Method called by {@link #fire(String, boolean, boolean, Object[])} if valid result and
	 * returnTaskId was false, i.e. the result is the result of the call
	 * @param request the original request
	 * @param userParams the parameter of the request
	 * @param result the result of the request
	 * @return a response to be sent to the client
	 */
	protected Response processResult(String request, Object[] userParams,
			Serializable result) {
		Response r = Response.ok(StringUtils.cap(result, 400)).build();
		log.debug(loggingContent(r));
		return r;
	}

	protected Response handleResult(ExecuteResult er) {
		if(er instanceof ErrorResult) {
			Throwable t = ((ErrorResult) er).getError();
			Response r = Response.status(Status.INTERNAL_SERVER_ERROR).entity(
					"Theatre threw an exception: " + t.getMessage()).build();
			log.debug(loggingContent(r));
			return r;
		}
		else if(er instanceof AsyncExecuteResult) {
			AsyncExecuteResult aer = (AsyncExecuteResult) er;
			long taskId = aer.getTaskId();
			long jobId = aer.getJobId();
			String lemUri = aer.getLemURI();
			Response r = Response.ok(new AsyncResponseInfo(taskId, jobId, lemUri)).build();
			log.debug(loggingContent(r));
			return r;
		}
		else if(er instanceof SyncExecuteResult) {
			Serializable result = er.getResult();
			if(result instanceof FileProxy) {
				FileProxy proxy = (FileProxy) result;
				File f = proxy.getFile();
				URI lemUri = proxy.getLemUri();
				Response r = Response.ok(new SyncResponseInfo(f.getAbsolutePath(), lemUri.toString()),
						MediaType.APPLICATION_JSON).build();
				log.debug(loggingContent(r));
				return r;
			}
			else {
				Response r = Response.status(Status.INTERNAL_SERVER_ERROR).entity(
						"Got non-FileProxy as result:" + result).build();
				log.debug(loggingContent(r));
				return r;
			}
		}
		else {
			Response r = Response.status(Status.INTERNAL_SERVER_ERROR).entity(
					"Got unknown execute result").build();
			log.debug(loggingContent(r));
			return r;
		}
	}

	private Object[] makeUserParams(List<String> params) {
		if(params == null || params.size() == 0) {
			log.debug("No user params found");
			return new Object[0];
		}
		try {
			Object[] result = new Object[params.size()];
			for (int i = 0; i < result.length; i++) {
				result[i] = StringUtils.parseStringParam(params.get(i));
			}
			return result;
		} catch(Exception e) {
			return null;
		}
	}

	public String makeRequest(String solver, String appName, String compName,
			String portName, List<String> nfrNames, List<String> nfrTypes,
			List<String> nfrValues, List<String> mpNames, List<String> mpValues) {
		return makeRequest(solver, appName, compName, portName, nfrNames, nfrTypes, nfrValues, mpNames, mpValues, null);
	}

	public String makeRequest(String solver, String appName, String compName,
			String portName, List<String> nfrNames, List<String> nfrTypes,
			List<String> nfrValues, List<String> mpNames, List<String> mpValues, MultivaluedMap<String, String> options) {
		class NfrConcatHandler implements ConcatHandler {
			@Override
			public void concate(StringBuilder sb, String key, List<String> values) {
				sb.append(key).append(" ").append(values.get(0)).append(": ").append(values.get(1));
			}
		}
		class MetaparamConcatHandler implements ConcatHandler {
			@Override
			public void concate(StringBuilder sb, String key, List<String> values) {
				sb.append(key).append("=").append(values.get(0));
			}
		}
		// prepare nfrs and mps
		Map<String, List<String>> nfrs = getMappedParam(nfrNames, nfrTypes, nfrValues);
		Map<String, List<String>> mps = getMappedParam(mpNames, null, mpValues);

		// concat to final result
		final String delim = "#";
		final String nfrDelim = "|";
		StringBuilder sb = new StringBuilder();
		sb.append(solver).append(delim)
			.append(appName).append(delim)
			.append(compName).append(delim)
			.append(portName).append(delim);
		appendMap(sb, nfrs, nfrDelim, new NfrConcatHandler());
		sb.append(delim);
		appendMap(sb, mps, nfrDelim, new MetaparamConcatHandler());
		sb.append(delim);
		appendMap(sb, options, nfrDelim, new MetaparamConcatHandler());
		// valid examplary request:
		//"ilp#org.haec.app.videoTranscodingServer#Controller#pipScaled#response_time max: 80000#max_video_length=5357568#";
		return sb.toString();
	}

	static interface ConcatHandler {
		void concate(StringBuilder sb, String key, List<String> list);
	}

	protected static void appendMap(StringBuilder sb, Map<String, List<String>> map,
			final String valueDelim, ConcatHandler handler) {
		if(map == null) { return; }
		boolean first;
		first = true;
		for(Entry<String, List<String>> entry : map.entrySet()) {
			if(first) { first = false; } else {
				sb.append(valueDelim);
			}
			handler.concate(sb, entry.getKey(), entry.getValue());
		}
	}

	private Map<String, List<String>> getMappedParam(List<String> names,
			List<String> types, List<String> values) {
		//TODO Check
		int namesLength = names.size();
		if(types != null) {
			if(types.size() > namesLength) {
				log.warn("Parameter names count do not match types count. Discarding superfluous values.");
			}
			if(types.size() < namesLength) {
				log.error("Not enough types for " + namesLength +" parameters. (Just " +
						types.size() + " given. Exiting");
				return Collections.emptyMap();
			}
		}
		if(values.size() > namesLength) {
			log.warn("Parameter names count do not match values count. Discarding superfluous values.");
		}
		if(values.size() < namesLength) {
			log.error("Not enough values for " + namesLength +" parameters. (Just " + values.size() + " given. Exiting");
			return Collections.emptyMap();
		}
		Map<String, List<String>> result = new HashMap<String, List<String>>(namesLength);
		for (int i = 0; i < namesLength; i++) {
			List<String> valuesForKey = new ArrayList<String>(types != null ? 2 : 1);
			if(types != null) {
				valuesForKey.add(types.get(i));
			}
			valuesForKey.add(values.get(i));
			result.put(names.get(i), valuesForKey);
		}
		return result;
	}
	
	protected IGlobalEnergyManager getGem() {
		return this.gem;
	}
	
	protected void setGem(IGlobalEnergyManager gem) {
		this.gem = gem;
	}
	
	protected void unsetGem(IGlobalEnergyManager gem) {
		this.gem = null;
	}
}
