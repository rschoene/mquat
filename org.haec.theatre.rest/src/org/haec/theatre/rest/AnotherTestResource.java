/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Test Resource.
 * @author René Schöne
 */
@Path("/gem1")
public class AnotherTestResource {

	@GET
	@Produces({MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
	@Path("/contest")
	public Response conTest(@QueryParam("id") String id,
			@QueryParam("sc") int sc) {
		String content = "this was a request with the id=" + id + "<a href=\"link\">text</a>";
		Status status = Status.fromStatusCode(sc);
		if(status == null) {
			status = Status.BAD_REQUEST;
		}
		return Response.status(status).entity(content).build();
	}
	
}
