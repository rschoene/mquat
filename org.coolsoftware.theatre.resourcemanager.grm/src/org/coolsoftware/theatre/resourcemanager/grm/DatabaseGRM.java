/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.resourcemanager.grm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Level;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.theatre.TheatreStartupHook;
import org.haec.theatre.dao.CpuCoreInfo;
import org.haec.theatre.dao.LoadInfo;
import org.haec.theatre.dao.RamInfo;
import org.haec.theatre.dao.ResourceDAO;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;

/**
 * This GRM cooperates with a database extending the {@link StandardGRM}.
 * It assumes a structural model including the types server, cpu and ram.
 * Currently only the following attributes are updated:
 * <ul>
 * <li>Server: load1, load5, load15</li>
 * <li>CPU: frequency</li>
 * <li>RAM: free</li>
 * </ul>
 * If a property notification does not change the model, i.e.
 * super.{@link #notifyPropertyChanged(double, String, String, String, ResourceType) notifyPropertyChange}
 * returns <code>false</code>, a new server resource is created and the notification is fired again.<br>
 * @author René Schöne
 */
public class DatabaseGRM extends StandardGRM {
	
	abstract class ConnectingTimerTask extends TimerTask {
		
		@Override
		public void run() {
			long before = System.currentTimeMillis();
			try {
				doWork();
			} catch(Exception e) {
				log.error(getErrorMessage(), e);
			} finally {
				computationTime.addAndGet(System.currentTimeMillis() - before);
			}
		}

		abstract protected String getErrorMessage();
		
		abstract protected void doWork() throws Exception;
	}
	
	private Timer timer;
	
	private AtomicInteger notificationCount = new AtomicInteger();
	private AtomicInteger activeServerCount = new AtomicInteger();
	private AtomicInteger serverCount = new AtomicInteger();
	private AtomicLong computationTime = new AtomicLong();

	public DatabaseGRM() {
		super();
		timer = new Timer();
	}
	
	@Override
	public void sayHello() {
		log.info("DexterGRM says Hello.");
	}
	
	private ConnectingTimerTask observeCPU() {
		ConnectingTimerTask cpuObserver = new ConnectingTimerTask() {
			private Map<String, List<CpuCoreInfo>> coresMap = new HashMap<>();
			@Override
			protected String getErrorMessage() {
				return "Exception while updating CPU properties";
			}
			@Override
			public void doWork() throws Exception {
				int nCount = resourceDAO.updateCpuCores(coresMap);
				if(nCount == ResourceDAO.RESOURCE_UPDATE_FAILED || nCount == 0) {
					// either error or no changes
					return;
				}
				int i=0;
				for(Entry<String, List<CpuCoreInfo>> e : coresMap.entrySet()) {
					String ip = e.getKey();
					for(CpuCoreInfo core : e.getValue()) {
						if(core.changed) {
							notifyPropertyChanged(core.frequency, ip, "core"+core.coreId, "frequency", cpuType);
							i++;
						}
					}
				}
				if(i != nCount) {
					log.debug("Updated only " + i + " of " + nCount + " cpu-core updates");
				}
				activeServerCount.set(coresMap.size());
				notificationCount.addAndGet(i);
			}
		};
		return cpuObserver;
	}

	private ConnectingTimerTask observeLoad() {
		ConnectingTimerTask loadObserver = new ConnectingTimerTask() {
			private Map<String, LoadInfo> loadMap = new HashMap<>();
			protected String getErrorMessage() {
				return "Exception while updating server load";
			}
			@Override
			public void doWork() throws Exception {
				int nCount = resourceDAO.updateLoad(loadMap);
				if(nCount == ResourceDAO.RESOURCE_UPDATE_FAILED || nCount == 0) {
					// either error or no changes
					return;
				}
				for(Entry<String, LoadInfo> e : loadMap.entrySet()) {
					String ip = e.getKey();
					LoadInfo load = e.getValue();
					if(load.changed) {
						notifyPropertyChanged(load.load1,  ip, ip, "load1",  serverType);
						notifyPropertyChanged(load.load5,  ip, ip, "load5",  serverType);
						notifyPropertyChanged(load.load15, ip, ip, "load15", serverType);
					}
				}
				notificationCount.addAndGet(nCount);
			}
		};
		return loadObserver;
	}

	private ConnectingTimerTask observeRAM() {
		ConnectingTimerTask ramObserver = new ConnectingTimerTask() {
			private Map<String, RamInfo> ramMap = new HashMap<>();
			@Override
			protected String getErrorMessage() {
				return "Exception while updating RAM";
			}
			@Override
			public void doWork() throws Exception {
				int nCount = resourceDAO.updateRAM(ramMap);
				if(nCount == ResourceDAO.RESOURCE_UPDATE_FAILED || nCount == 0) {
					// either error or no changes
					return;
				}
				int i = 0;
				for(Entry<String, RamInfo> e : ramMap.entrySet()) {
					String ip = e.getKey();
					RamInfo ram = e.getValue();
					if(ram.changed) {
						notifyPropertyChanged(ram.free, ip, ram.name, "free", ramType);
						i++;
					}
				}
				if(i != nCount) {
					log.debug("Updated only " + i + " of " + nCount + " ram updates");
				}
				notificationCount.addAndGet(i);
			}
		};
		return ramObserver;
	}
	
	private void observeNotificationCount() {
		TimerTask counter = new TimerTask() {
			
			@Override
			public void run() {
				int count = notificationCount.getAndSet(0);
				long comp = computationTime.getAndSet(0);
				if(count > 0) {
					log.info("n=" + count + ", servers: " + activeServerCount.get() + "/" + serverCount.get()
							+ " in " + comp + "ms");
				}
			}
		};
		timer.schedule(counter, 0, 5000);
	}
	
	@Override
	public void shutdown() {
		super.shutdown();
		timer.cancel();
	}
	
	@Override
	public boolean notifyPropertyChanged(double value, String ip, String rname, String pname, ResourceType type) {
		boolean change = super.notifyPropertyChanged(value, ip, rname, pname, type);
		if(!change) {
			Resource server = null;
			// get server
			for(Resource r : ((Resource)vm.getRoot()).getSubresources()) {
				if(r.getName().equals(ip)) {
					server = r;
				}
			}
			if(server == null) {
				((Resource)vm.getRoot()).getSubresources().add(createNewServerResource(ip));
				// process notification again
				log.debug("Second notification");
				change = super.notifyPropertyChanged(value, ip, rname, pname, type);
				if(!change) {
					if(log.isEnabledFor(Level.WARN))
						log.warn(String.format("2nd notification failed. (%s.%s = %s)", rname, pname, value));
				}
			} else {
				// server is found, but StandardGRM had no change. It's a new resource! But of which type? :|
				server.getSubresources().add(createResourcePrototype(type, rname, false));
				change = super.notifyPropertyChanged(value, ip, rname, pname, type);
				if(!change) {
					if(log.isEnabledFor(Level.WARN))
						log.warn(String.format("2nd notification failed. (%s.%s = %s)", rname, pname, value));
				}
			}
		}
		return change;
	}

	/**
	 * Creates a new resource of the server type, comprising cpu (2 cores), ram, network and hdd.
	 * Sets the name to ip.
	 * @param ip the ip of the new server, will be the name of the created resource
	 * @return the created resource
	 */
	private Resource createNewServerResource(String ip) {
		if(serverType == null) {
			log.error("Could not find 'server' subtype in structural model");
			return null;
		}
		Resource ret = createResourcePrototype(serverType, ip, true);
		log.info("New resource with name " + ip);

		// create sub resources
		createSubResourcePrototype(ret, cpuType /*, makeLocalCoreName(0), makeLocalCoreName(1)*/);
		createSubResourcePrototype(ret, ramType);
		createSubResourcePrototype(ret, networkType);
		createSubResourcePrototype(ret, hddType);
		serverCount.incrementAndGet();
		return ret;
	}

	/**
	 * Creates a number of new sub resources.
	 * The created resources will be a subresource of the resource ret, of resourcetype type
	 * and have either a name out of rnames or, if rnames is empty the name of the type is used as a name.
	 * @param ret the super resource
	 * @param type the type of the resources
	 * @param rnames the names of the new resources, may be <code>null</code> or empty
	 *  to set the name to the name of the type
	 */
	private void createSubResourcePrototype(Resource ret, ResourceType type, String... rnames) {
//		log.debug(ret + ", type: " + type);
		if(rnames == null || rnames.length == 0) {
			// create one resource with the name equal to the name of the type
			rnames = new String[]{type.getName()};
		}
		for(String rname : rnames) {
			ret.getSubresources().add(createResourcePrototype(type, rname, false));
		}
	}

	/**
	 * Creates a new resource.
	 * It will use the type as specification and initializes variante property binding
	 * according to the properties of the type.
	 * @param type the specification of the resource
	 * @param rname the name of the resource
	 * @return the created resource
	 */
	private Resource createResourcePrototype(ResourceType type, String rname, boolean isContainer) {
//		log.debug(type + ", rname: " + rname);
		Resource r = isContainer ? VariantFactory.eINSTANCE.createContainerProvider()
				: VariantFactory.eINSTANCE.createResource();
		r.setSpecification(type);
		r.setName(rname);
		if(type == null) {
			log.debug("Got null resource type");
			return null;
		}
		else {
			for(Property p : type.getProperties()) {
//				if(log.isDebugEnabled()) {
//					String name = p.getDeclaredVariable() != null ? p.getDeclaredVariable().getName() : "unknown";
//					log.debug("Adding property binding for property " + p + ", p.name = " + name);
//				}
				VariantPropertyBinding binding = VariantFactory.eINSTANCE.createVariantPropertyBinding();
				binding.setProperty(p);
				r.getPropertyBinding().add(binding);
			}
		}
		return r;
	}
	
	private ResourceDAO resourceDAO;

	private TheatreStartupHook startupHook;
	
	protected void setResourceDAO(ResourceDAO rd) {
		this.resourceDAO = rd;
	}
	
	protected void unsetResourceDAO(ResourceDAO rd) {
		this.resourceDAO = null;
	}
	
	protected void setStartupHook(TheatreStartupHook startupHook) {
		this.startupHook = startupHook;
	}
	
	protected void unsetStartupHook(TheatreStartupHook startupHook) {
		this.startupHook = null;
	}
	
	class Init implements Runnable {
		private BundleContext bundleContext;

		public Init(BundleContext bundleContext) {
			this.bundleContext = bundleContext;
		}

		@Override
		public void run() {
			if(!resourceDAO.isReady()) {
				log.warn("Resource DAO is not ready");
			}
			if(startupHook != null) {
				startupHook.runSetup();
			}
			DatabaseGRM.super.init(bundleContext);
			// delayed setup
			for(ConnectingTimerTask task : new ConnectingTimerTask[]{observeCPU(), observeLoad(), observeRAM()}) {
				timer.scheduleAtFixedRate(task, 0, settings.getDatabaseUpdaterate().get());
			}
			observeNotificationCount();
		}
	}

	@Override
	protected void activate(final ComponentContext ctx) {
		new Thread(new Init(ctx.getBundleContext())).start();
	}
}
