/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.resourcemanager.grm;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.settings.BooleanSetting;
import org.haec.theatre.utils.settings.Setting;
import org.haec.theatre.utils.settings.SettingHolder;
import org.haec.theatre.utils.settings.StringSetting;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

/**
 * Activator to start the GRM (either Database or Standard).
 * @author Sebastian Götz
 * @author René Schöne
 * @deprecated
 */
public class GRMActivator extends SettingHolder implements BundleActivator, ServiceListener {

	/** Uri of the GRM (default: "192.168.0.201")*/
	public static Setting<String> uri = new StringSetting("theatre.grm.uri", "192.168.0.201");
	/** Whether to log each hw change notification. False to log only counts. (default: false)*/
	public static Setting<Boolean> logSingleNotification = new BooleanSetting("theatre.grm.log.singleNotification", false);
	/** Whether to log ip mismatches (Attention: will floods logs) (default: false)*/
	public static Setting<Boolean> logIpMismatch = new BooleanSetting("theatre.grm.log.ipMismatch", false);

	private static final Logger log = Logger.getLogger(GRMActivator.class);
	private static BundleContext context;
	private IGlobalResourceManager grmService;
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		log.debug("Starting Global Resource Manager...");
		GRMActivator.context = context;
		grmService = createGRM();
		
		log.info("Global Resource Manager is up and running.");
		
		// really needed?
		RemoteOsgiUtil.getRemoteOSGiService(context).connect(RemoteOsgiUtil.createRemoteOsgiUri("127.0.0.1"));
		
		RemoteOsgiUtil.register(context, IGlobalResourceManager.class, grmService);
		log.debug("GRM Service registered (for remote access)");
	}

	protected IGlobalResourceManager createGRM() {
//		if(DexterActivator.use.get()) {
//			return new DatabaseGRM();
//		}
//		// else
//		return new StandardGRM();
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		grmService.shutdown();
		log.info("Global Resource Manager is down.");
		GRMActivator.context = null;
	}
	
	public static BundleContext getContext() {
		return context;
	}
	
	public void serviceChanged(ServiceEvent ev) {
		
	}


}
