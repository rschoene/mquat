/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.resourcemanager.grm;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.Job;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.Schedule;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.ccm.variant.impl.VariantFactoryImpl;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.BundleUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.theatre.utils.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.ComponentContext;

/**
 * @author Sebastian G�tz
 * 
 */
public class StandardGRM implements IGlobalResourceManager {
	protected static final String HDD_TYPE = "hdd";
	protected static final String NETWORK_TYPE = "network";
	protected static final String RAM_TYPE = "ram";
	protected static final String CPU_TYPE = "cpu";
	protected static final String SERVER_TYPE = "server";
	protected static final String LOCALHOST_PREFIX = "127";

	protected final Logger log = Logger.getLogger(StandardGRM.class);

	protected VariantModel vm;
	private static String serializedStructModel;
	private String serializedVarModel;
	private Resource infra;

	ResourceType serverType = null,
			cpuType = null,
			ramType = null,
			networkType = null,
			hddType = null;

	protected TheatreSettings settings;
	private BundleContext context;

	/**
	 * @param loadedStruct
	 */
	private void setTypes(StructuralModel loadedStruct) {
		for (ResourceType sub : ((ResourceType) loadedStruct.getRoot())
				.getSubtypes()) {
			if (sub.getName().toLowerCase().equals(SERVER_TYPE)) {
				serverType = sub;
				break;
			}
		}
		if(serverType == null) {
			log.error("Could not find 'server' subtype in structural model");
			return;
		}
		for (ResourceType sub : serverType.getSubtypes()) {
			switch(sub.getName().toLowerCase()) {
			case CPU_TYPE: cpuType = sub; break;
			case RAM_TYPE: ramType = sub; break;
			case NETWORK_TYPE: networkType = sub; break;
			case HDD_TYPE: hddType = sub; break;
			}
		}
	}
	
	@Override
	public ResourceType getType(String name) {
		switch(name.toLowerCase()) {
		case SERVER_TYPE: return serverType;
		case CPU_TYPE: return cpuType;
		case RAM_TYPE: return ramType;
		case NETWORK_TYPE: return networkType;
		case HDD_TYPE: return hddType;
		}
		log.debug("Invalid type: " + name);
		return null;
	}

	private Bundle getBundle(String symbolicName) {
		Bundle result = Platform.getBundle(symbolicName);
		if(result == null) {
			result = BundleUtils.getBundle(context, symbolicName);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.coolsoftware.theatre.grm.GRM#sayHello()
	 */
	@Override
	public void sayHello() {
		log.info("greetings from your GRM");
	}

	/**
	 * Deprecated Use
	 * {@link CCMUtil#serializeResource(org.eclipse.emf.ecore.resource.Resource)}
	 * instead.
	 * 
	 * @throws IOException
	 */
	@Deprecated
	public void loadInfrastructure() throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		vm.eResource().save(out, null);
		serializedVarModel = out.toString();
	}

	public void loadInfrastructure(URI variantModelFileURI, URI structureModelFileURI) throws IOException {
		
		File fs = new File(structureModelFileURI);
		BufferedReader fr = new BufferedReader(new FileReader(fs));
		String line2 = "";
		serializedStructModel = "";

		while ((line2 = fr.readLine()) != null) {
			if (line2 != null)
				serializedStructModel += line2;
		}
		fr.close();

		if (variantModelFileURI != null) {
			log.debug("loading infra from " + variantModelFileURI);

			File f = new File(variantModelFileURI.toString());
			fr = new BufferedReader(new FileReader(f));
			String line = "";
			serializedVarModel = "";

			while ((line = fr.readLine()) != null) {
				if (line != null)
					serializedVarModel += line;
			}
			fr.close();

			EObject result = null;
			org.eclipse.emf.common.util.URI ccmURI = org.eclipse.emf.common.util.URI.createFileURI(f.getAbsolutePath());
			org.eclipse.emf.common.util.URI sURI = org.eclipse.emf.common.util.URI.createFileURI(fs.getAbsolutePath());

			ResourceSet rs = CCMUtil.createResourceSet();
			org.eclipse.emf.ecore.resource.Resource resource = rs.getResource(ccmURI, true);
			org.eclipse.emf.ecore.resource.Resource resource2 = rs.getResource(sURI, true);

			/* Check if the resource has already been created. */
			if (resource == null) {
				resource = rs.createResource(ccmURI);
			}
			// no else.

			if (!resource.isLoaded())
				resource.load(null);
			// no else.

			if (!resource2.isLoaded())
				resource2.load(null);
			// no else.

			if (resource.getContents().size() > 0) {
				result = resource.getContents().get(0);
			}

			vm = (VariantModel) result;
		}

	}

	@Override
	public String getInfrastructure() {
		try {
			serializedVarModel = CCMUtil.serializeResource(vm.eResource());
		} catch (IOException e) {
			log.error("Could not serialize vm", e);
		}

		return serializedVarModel;
	}
	
	@Override
	public VariantModel getInfrastructureAsModel() {
		return vm;
	}

	public String getInfrastructureTypes() {
		return serializedStructModel;
	}

	/**
	 * Register a new (server-level) resource.
	 * 
	 * @param uri
	 *            the URI of the corresponding local resource manager.
	 */
	public void registerResource(String resource) {
		log.debug("retrieved: " + resource);

		try {
			VariantModel vmx = CCMUtil.deserializePartialHWVariantModel(serializedStructModel, resource, false);
			Resource server = (Resource) vmx.getRoot();
			((Resource) vm.getRoot()).getSubresources().add(server);
			CCMUtil.persistResource(vm.eResource());

			// persistInfrastructure();

		} catch (IOException e) {
			log.warn("Could not register resource " + StringUtils.cap(resource, 200), e);
		}
	}

//	/**
//	 * Use
//	 * {@link CCMUtil#persistResource(org.eclipse.emf.ecore.resource.Resource)}
//	 * instead
//	 * 
//	 * @deprecated
//	 */
//	private void persistInfrastructure() {
//		try {
//			org.eclipse.emf.common.util.URI fileURI = org.eclipse.emf.common.util.URI.createPlatformResourceURI(
//					CCMUtil.CURRENT_INFRASTRUCTURE_VARIANT_PATH, false);
//			org.eclipse.emf.ecore.resource.Resource poResource = new XMLResourceFactoryImpl().createResource(fileURI);
//			poResource.getContents().add(vm);
//			poResource.save(null);
//		} catch (IOException ioe) {
//			ioe.printStackTrace();
//		}
//	}

	public void unregisterResource(String name) {
		Resource rem = null;
		for (Resource r : ((Resource) vm.getRoot()).getSubresources()) {
			if (r.getName().equals(name)) {
				log.debug("removing " + r);
				rem = r;
			}
		}
		if (rem != null)
			((Resource) vm.getRoot()).getSubresources().remove(rem);
	}

//	/**
//	 * For deserialization of {@link VariantModel}s use either
//	 * {@link CCMUtil#deserializePartialHWVariantModel(String, String, boolean)}
//	 * or
//	 * {@link CCMUtil#deserializeGlobalHWVariantModel(String, String, boolean)}
//	 * 
//	 * @deprecated
//	 */
//	private static VariantModel deserializeVarModel(String serializedVarModel, String serializedStructModel)
//			throws IOException {
//		log.debug("deserializeVarModel: ");
//		log.debug("\t" + serializedVarModel);
//		log.debug("\t" + serializedStructModel);
//
//		// dump received model to disk
//		// File tmp = dumpFile("SentServerData.variant", serializedVarModel);
//		// File tmp2 = dumpFile("ServerD.structure", serializedStructModel);
//		ByteArrayInputStream bVar = new ByteArrayInputStream(serializedVarModel.getBytes());
//		ByteArrayInputStream bStruct = new ByteArrayInputStream(serializedVarModel.getBytes());
//
//		// load it using emf + emftext factory
//		EObject result = null;
//		org.eclipse.emf.common.util.URI structURI = org.eclipse.emf.common.util.URI.createPlatformResourceURI(
//				CCMUtil.RESOURCESTRUCTURE_PATH, false);
//		// .createFileURI(tmp2.getAbsolutePath());
//		org.eclipse.emf.common.util.URI ccmURI = org.eclipse.emf.common.util.URI.createPlatformResourceURI(
//				CCMUtil.SENT_SERVER_DATA_VARIANT_PATH, false);
//		ResourceSet rs = CCMUtil.createResourceSet();
//		org.eclipse.emf.ecore.resource.Resource resource_struct = rs.getResource(structURI, false);
//		org.eclipse.emf.ecore.resource.Resource resource = rs.getResource(ccmURI, false);
//
//		/* Check if the resource has already been created. */
//		if (resource_struct == null) {
//			resource_struct = rs.createResource(structURI);
//
//		}
//		// no else.
//
//		if (!resource_struct.isLoaded())
//			resource_struct.load(bStruct, null);
//
//		// no else.
//
//		/* Check if the resource has already been created. */
//		if (resource == null) {
//			resource = rs.createResource(ccmURI);
//		}
//		// no else.
//
//		if (!resource.isLoaded())
//			resource.load(bVar, null);
//
//		// no else.
//
//		if (resource.getContents().size() > 0) {
//			result = resource.getContents().get(0);
//		}
//
//		return (VariantModel) result;
//	}

//	@Deprecated
//	private static File dumpFile(String fileName, String content) throws IOException {
//		File tmp = new File(fileName);
//		log.debug(tmp.getAbsolutePath());
//		if (!tmp.exists())
//			tmp.createNewFile();
//		FileWriter fw = new FileWriter(tmp, false);
//		fw.write(content);
//		fw.close();
//		return tmp;
//	}

	@Override
	public boolean notifyPropertyChanged(double value, String ip, String rname, String pname, ResourceType type) {
		//TODO check if a derived property is effected by this change and recompute it
//		log.debug(""+rname+"."+pname+" = "+value);
//		log.debug(String.format("%s:%s.%s = %s", serverName, rname, pname, value));
		boolean change = false;
		String desc = ip + "$" + rname + "." + pname;
		if(ip.startsWith(LOCALHOST_PREFIX)) {
			if(settings.getGrmLogSingleNotification().get()) {
				log.debug("Would drop " + desc);
			}
			return false;
		}
		for(Resource r : ((Resource)vm.getRoot()).getSubresources()) {
			if(r.getName().equals(ip)) {
//				log.debug("Matched ip for: " + r.getName() + "=" + desc);
			}
			else {
				if(settings.getGrmLogIpMismatch().get()) {
					log.debug("Ip mismatch: " + r.getName() + "!=" + desc);
				}
				continue;
			}
			change |= handleResource(r, ip, rname, pname, value);
			for(Resource ir : r.getSubresources()) {
				if(ir == null) {
					log.warn("Got null resource within " + r.getName());
					continue;
				}
				if(ir.getName() == null) {
					continue;
				}
				change |= handleResource(ir, ip, rname, pname, value);
			}
		}
		return change;
	}
	
	private boolean handleResource(Resource r, String ip, String rname, String pname, double value) {
		boolean change = false;
		if(r.getName().equals(rname)) {
			for(VariantPropertyBinding b : r.getPropertyBinding()) {
				if(b.getProperty().getDeclaredVariable().getName().equals(pname)) {
					RealLiteralExpression stre = LiteralsFactory.eINSTANCE
							.createRealLiteralExpression();
					stre.setValue(value);
					if(settings.getGrmLogSingleNotification().get()) {
						log.debug(ip + "$" + rname + "." + pname + "=" + value);
					}
					b.setValueExpression(stre);
					change = true;
					updateLastChanged(r, System.currentTimeMillis());
				}
			}
		}
		return change;
	}
	
	private void updateLastChanged(Resource r, long millis) {
		r.setLastChanged(millis);
		EObject container = r.eContainer();
		if(container != null && container instanceof Resource) {
			updateLastChanged((Resource) container, millis);
		}
	}

	@Override
	public void updateWithModel(String serializeModel) {
		if(log.isDebugEnabled()) {
			log.debug(StringUtils.cap(serializeModel, 500));
		}
		VariantModel changed;
		try {
			changed = CCMUtil.deserializeGlobalHWVariantModel(serializedStructModel, serializeModel, false);
		} catch (IOException e) {
			log.debug("Could not deserialize model to apply changes.");
			return;
		}
		Resource root = (Resource) changed.getRoot();
		List<Resource> toSearch = new ArrayList<>();
		toSearch.add(root);
		int totalJobsAdded = 0;
		int totalJobsUpdated = 0;
		while(!toSearch.isEmpty()) {
			Resource r = toSearch.remove(0);
			int[] newAndUpdated = checkForNewJobs(r);
			totalJobsAdded = newAndUpdated[0];
			totalJobsUpdated = newAndUpdated[1];
			toSearch.addAll(r.getSubresources());
		}
		log.info(String.format("%d new jobs, %d updated jobs.", totalJobsAdded, totalJobsUpdated));
	}

	private static int[] nothingChanged = new int[]{0,0};
	private int[] checkForNewJobs(Resource r) {
		int nrOfNewJobs = 0;
		int nrOfUpdatedJobs = 0;
		log.debug("Checking " + r);
		if(r instanceof ContainerProvider) {
			Schedule schedule = ((ContainerProvider) r).getSchedule();
			if(schedule == null || schedule.getJobs().isEmpty()) {
				log.debug("No or empty schedule in " + r);
				return nothingChanged;
			}
			Resource localResource = matchResource(r);
			if(localResource == null) {
				log.debug("Could not match resource " + r);
				return nothingChanged;
			}
			if(localResource instanceof ContainerProvider) {
				ContainerProvider cp = (ContainerProvider) localResource;
				Schedule localSchedule = cp.getSchedule();
				if(localSchedule == null) {
					localSchedule = VariantFactory.eINSTANCE.createSchedule();
					cp.setSchedule(localSchedule);
					localSchedule.getJobs().addAll(schedule.getJobs());
					nrOfNewJobs = localSchedule.getJobs().size();
				}
				else {
					EList<Job> existingJobs = localSchedule.getJobs();
					List<Job> jobsToAdd = new ArrayList<>(schedule.getJobs());
					for(Job existingJob : existingJobs) {
						Long jobId = existingJob.getJobId();
						if(jobId == null) { continue; }
						for(Job newJob : schedule.getJobs()) {
							if(jobId.equals(newJob.getJobId())) {
								updateJob(existingJob, newJob);
								nrOfUpdatedJobs += 1;
								jobsToAdd.remove(newJob);
							}
						}
					}
					nrOfNewJobs += jobsToAdd.size();
					existingJobs.addAll(jobsToAdd);
				}
			}
			else {
				log.warn("Found non-ContainerProvider for resource " + r);
			}
		}
		else {
			log.debug("Not a ContainerProvider: " + r);
		}
		return new int[]{nrOfNewJobs,nrOfUpdatedJobs};
	}

	/**
	 * @param existingJob
	 * @param newJob
	 */
	private void updateJob(Job existingJob, Job newJob) {
		if(newJob.getHost() != null) { existingJob.setHost(newJob.getHost()); }
		if(newJob.getRawResult() != null) { existingJob.setRawResult(newJob.getRawResult()); }
		if(newJob.getEndTime() != null) { existingJob.setEndTime(newJob.getEndTime()); }
		if(newJob.getStartTime() != null) { existingJob.setStartTime(newJob.getStartTime()); }
		if(newJob.getStatus() != null) { existingJob.setStatus(newJob.getStatus()); }
	}

	/** Matches the resource with the local variant model and returns a resource of this model. */
	protected Resource matchResource(Resource res) {
		Resource localRoot = (Resource) vm.getRoot();
		List<Resource> toSearch = new ArrayList<>();
		toSearch.add(localRoot);
		while(!toSearch.isEmpty()) {
			Resource localRes = toSearch.remove(0);
			if(res.getName().equals(localRes.getName())) {
				return localRes;
			}
			toSearch.addAll(localRes.getSubresources());
		}
		return null;
	}

	@Override
	public void shutdown() {
		// nothing to do here
	}

	@Override
	public boolean reset(boolean full) {
		// nothing to do here (maybe reload structural model)
		return true;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.Resettable#getName()
	 */
	@Override
	public String getName() {
		return getClass().getName();
	}
	
	protected void setTheatreSettings(TheatreSettings ts) {
		this.settings = ts;
		log.info("*** New Settings: " + ts + " ***");
	}
	
	protected void unsetTheatreSettings(TheatreSettings ts) {
		log.info("*** Unset settings: " + ts + " ***");
		this.settings = null;
	}
	
	protected void activate(final ComponentContext ctx) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				init(ctx.getBundleContext());
			}
		}).start();
	}
	
	protected void init(BundleContext bundleContext) {
		context = bundleContext;
		log.debug("creating variant model of infrastructure");
		
		StructuralModel loadedStruct = loadStruct();
		
		if (loadedStruct!=null){
			setTypes(loadedStruct);
			// create the currentVariant resource
//			org.eclipse.emf.common.util.URI varUri = org.eclipse.emf.common.util.URI.createPlatformResourceURI(
//					correctUriPath(CCMUtil.CURRENT_INFRASTRUCTURE_VARIANT_PATH), false);
			org.eclipse.emf.common.util.URI varUri = EcorePlugin.resolvePlatformResourcePath(
					CCMUtil.CURRENT_INFRASTRUCTURE_VARIANT_PATH);
			org.eclipse.emf.ecore.resource.Resource varRes = loadedStruct.eResource().getResourceSet().createResource(varUri);
			
			vm = VariantFactoryImpl.eINSTANCE.createVariantModel();
			infra = VariantFactoryImpl.eINSTANCE.createResource();
			infra.setName("Infrastructure");
			vm.setRoot(infra);
			varRes.getContents().add(vm);
			try {
				CCMUtil.persistResource(varRes);
			} catch (IOException e) {
				log.warn("Could not persist vm", e);
			}
			log.debug("test infrastructure initiated successfully: "
					+ vm);
		}
		else {
			log.error("Structural model was not loaded!");
		}
		
		// publish iglobalresourcemanager
		BundleContext bc = FrameworkUtil.getBundle(StandardGRM.class).getBundleContext();
		RemoteOsgiUtil.register(bc, IGlobalResourceManager.class, this);
	}

//	/**
//	 * Remove leading '/' from the path, if any.
//	 * @param path
//	 * @return the corrected path
//	 */
//	private String correctUriPath(String path) {
//		if(path.charAt(0) == '/') {
//			return path.substring(1);
//		}
//		return path;
//	}

//	/**
//	 * Workaround for bug with runtime URI loading
//	 * @return the loaded hardware structure model
//	 */
//	private StructuralModel loadStruct2() {
//		try {
//			String path = correctUriPath(CCMUtil.RESOURCESTRUCTURE_PATH);
//			String sw_sm_string = FileUtils.readAsString(path);
//			StructuralModel sw_sm = CCMUtil.deserializeHWStructModel(sw_sm_string, false);
//			try {
//				serializedStructModel = CCMUtil.serializeEObject(sw_sm);
//			} catch (IOException e) {
//				log.warn("Could not serialize structural model from " + path, e);
//				// use just read plain text
//				serializedStructModel = sw_sm_string;
//			}
//			return sw_sm;
//		} catch (IOException e) {
//			log.error("Could not load hardware structure model", e);
//			return null;
//		}
//	}

	/**
	 * Old load method using runtime URI loading
	 * @return the loaded hardware structure model
	 */
	protected StructuralModel loadStruct() {
		org.eclipse.emf.common.util.URI structUri = EcorePlugin.resolvePlatformResourcePath(CCMUtil.RESOURCESTRUCTURE_PATH);
		log.debug("structUri= " + structUri);
		StructuralModel loadedStruct = null;
//		log.debug("URI-Handlers:");
//		try {
//			for(URIHandler handler : URIConverter.INSTANCE.getURIHandlers()) {
//				boolean canHandle = handler.canHandle(structUri);
//				Class<? extends URIHandler> clazz = handler.getClass();
//				log.debug(String.format("%s (%s): can handle=%s",
//						handler.toString(), clazz.toString(), canHandle));
//			}
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
		
		// try to load the structure model via the Runtime URI
		if(URIConverter.INSTANCE.exists(structUri, null)){
			log.debug("URI " + structUri +" exists.");
			// TODO: Handle error during dynamic resource loading
			org.eclipse.emf.ecore.resource.Resource structRes = CCMUtil.createResourceSet().getResource(structUri, true);
			log.debug("structRes= " + structRes);
			EObject content = structRes.getContents().get(0);
			log.debug("content= " + content);
			if(content instanceof StructuralModel){
				loadedStruct = (StructuralModel) content;
				try {
					serializedStructModel = CCMUtil.serializeEObject(content);
				} catch (IOException e) {
					log.warn("StructuralModel can't be loaded. URI:" + structUri.toString(), e);
				}
			}
			// no else.
		} else{
			// load the backup model which is contained in org.coolsoftware.theatre bundle
			try{		
				Bundle b = getBundle("org.coolsoftware.theatre");
				log.debug("theatre-bundle= " + b);
				URL fileURL = b.getEntry("models/Server.structure");
				log.debug("server.structure-fileURL= " + fileURL);
				URI uri = FileLocator.resolve(fileURL).toURI();
				log.debug("server.structure-resolvedURL= " + fileURL);
//				File f = new File(uri);
				loadInfrastructure(null, uri);	
				loadedStruct = CCMUtil.deserializeHWStructModel(serializedStructModel, true);
			} catch (Exception e) {
				log.error("Could not load backup model", e);
			}
		}
		return loadedStruct;
	}
	
}
