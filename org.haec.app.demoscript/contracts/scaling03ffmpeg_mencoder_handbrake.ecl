import ccm [../Server.structure]
import ccm [../VideoTranscoding.structure]

contract org_haec_app_scaling_ffmpeg_ScalingFfmpeg implements software Scaling.scale {
	mode scaleFfmpeg1 {
		requires resource CPU {
			frequency min: 503.3
			cpu_time <f_cpu_time(max_video_length)>
			energyRate: 5.33}
		requires resource RAM {free min: 53.3 energyRate: 5.33}
		provides response_time <f_wtime_fast(max_video_length)>
	}
}

contract org_haec_app_scaling_mencoder_ScalingMencoder implements software Scaling.scale {
	mode scaleMencoder1 {
		requires resource CPU {
			frequency min: 503.2
			cpu_time <f_cpu_time(max_video_length)>
			energyRate: 5.32}
		requires resource RAM {free min: 53.2 energyRate: 5.32}
		provides response_time <f_wtime_slow(max_video_length)>
	}
}

contract org_haec_app_scaling_handbrake_ScalingHandbrake implements software Scaling.scale {
	mode scaleHandbrake1 {
		requires resource CPU {
			frequency min: 503.1
			cpu_time <f_cpu_time(max_video_length)>
			energyRate: 5.31}
		requires resource RAM {free min: 53.1 energyRate: 5.31}
		provides response_time <f_wtime_slow(max_video_length)>
	}
}