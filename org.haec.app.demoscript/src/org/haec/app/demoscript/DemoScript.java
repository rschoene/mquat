/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.demoscript;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.ILocalEnergyManager;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResultSerializer;
import org.eclipse.core.runtime.FileLocator;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.app.videotranscodingserver.ui.BundleNames;
import org.haec.apps.util.monitor.LocalCompositeMonitor;
import org.haec.apps.util.monitor.LocalEnergyMonitor;
import org.haec.apps.util.monitor.LocalMonitor;
import org.haec.apps.util.monitor.LocalTimeMonitor;
import org.haec.theatre.utils.BundleUtils;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.videos.Video;
import org.haec.videoprovider.IVideoProviderFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

import ch.ethz.iks.r_osgi.RemoteOSGiService;
import ch.ethz.iks.r_osgi.RemoteServiceReference;

/**
 * Showcase script to demonstrate the runtime variability using the VideoTranscodingServer.
 * There are three different encoders (in the order of responsibility): ffmpeg, mencoder, handbrake.
 * In the script, three requests will be sent, each with more implmentations available, such that
 * for each request the next (better) implementation is chosen.
 * @author René Schöne
 */
public class DemoScript implements Runnable {

	protected static final int MAX_EXECUTE_COUNT = 5;
	private BundleContext context;
	private boolean failed;
	
	private static final String controllerCompName = "Controller";
	private static final String controllerPortname = "pipScaled";
	private static final int responseTimeMax = 80000;

	final String appName = "org.haec.app.videoTranscodingServer";
	final String typeName = "Scaling";
	Video video1 = null;
	Video video2 = null;
	final File fileVideo1;
	final File fileVideo2;
	final int w = -2;
	final int h = -2;
	final boolean mult = true;
	final int pos = PipDoPip.POS_CENTER;
	final String req = "ilp#" +		// optimizer
			appName + "#" +	// appName
			controllerCompName+"#" +		// compName
			controllerPortname+"#" +		// portName
			"response_time max: "+responseTimeMax+"#" +		// nfr's
			"max_video_length=" + computeMaxVideoLength(video1, video2);
	final Map<String, Bundle> resolvedBundles;
	final IGlobalEnergyManager gem;
	private static int lastStepNumber;
	
	private List<Step> steps = new LinkedList<DemoScript.Step>();
	private BufferedWriter bw;
	
	final LocalMonitor monitor;

	public DemoScript(BundleContext context) {
		pds("Last step number: " + lastStepNumber);
		IVideoProviderFactory fac = context.getService(
				context.getServiceReference(IVideoProviderFactory.class));
		List<Video> list = fac.getCurrentVideoProvider().getOneVideoOfEachLength();
		if(list.size() > 0) { video1 = list.get(0); }
		if(list.size() > 1) { video2 = list.get(1); }
		if(video1 == null || video2 == null) {
			throw new IllegalStateException("Could not find two videos to operate with. Exiting.");
		}
		this.fileVideo1 = video1.file;
		this.fileVideo2 = video2.file;
		this.context = context;
		this.failed = true;
		this.gem = getGEM();
		LocalMonitor m = null;
		boolean useOnlyTime = false;
		try {
			// testing energy monitor
			m = new LocalEnergyMonitor(context);
			m.before();
			Thread.sleep(300);
			m.after();
			if(m.getDiff().startsWith("0")) {
				// weak check, but here we assume, energy monitor is not working
				useOnlyTime = true;
			}
			else {
				m = new LocalCompositeMonitor(";", new LocalTimeMonitor(context), new LocalEnergyMonitor(context));
			}
		}
		catch(Throwable e) {
			// exception just thrown by local energy monitor
			// so don't use it
			e.printStackTrace();
			System.err.println("Using just time measurement.");
			useOnlyTime  = true;
		}
		if(useOnlyTime) {
			m = new LocalTimeMonitor(context);
		}
		this.monitor = m;
		resolvedBundles = BundleUtils.resolveBundles(context, BundleNames.getAllBundleNames());
		// initialize log
		if(bw == null) {
			try {
				File f = new File("steps"+getLogfileIdentifier()+".txt");
				FileWriter fw = new FileWriter(f);
				bw = new BufferedWriter(fw);
				bw.write("stepnr;" + monitor.getParameterName());
				bw.newLine();
				System.out.println("Create writer at :" + f);
			} catch (IOException e) {
				throw new RuntimeException("Could not create writer. Exiting.", e);
			}
		}
		// create steps
		steps.add(createPreparationStep());
		/**
		 * First step: Request to controller.pipScaled
		 *  (expecting scaling.ffmpeg chosen as it is the only impl)
		 */
		addExecutionSteps(steps);
		steps.add(createMencoderStep());
		/**
		 * Third step: Second Request to controller.pipScaled
		 *  (expecting scaling.mencoder chosen as it has better response_time)
		 */
		addExecutionSteps(steps);
		steps.add(createHandbrakeStep());
		/**
		 * Fifth step: Third Request to controller.pipScaled
		 *  (expecting scaling.handbrake chosen as it has the best response_time)
		 */
		addExecutionSteps(steps);
		
		// Step 6: Show different measured energy values
		//TODO rs: check and compare energy values
	}
	
	private String getLogfileIdentifier() {
//		return ""+System.currentTimeMillis();
		return new SimpleDateFormat("yyMMdd_HHmmss").format(new GregorianCalendar().getTime());
	}

	@Override
	protected void finalize() throws Throwable {
		if(bw != null) try { bw.close(); } catch (IOException ignore) { }
	}

	@Override
	public void run() {
		failed = true;
		// execute steps
		for(Step step : steps) {
			if(!step.runStep()) {
				System.err.println("Failure in " + step.toString());
				if(step.failure != null)
					step.failure.printStackTrace();
				else
					System.err.println("No exception given.");
				throw new RuntimeException();
			}
		}
		
		try {
			bw.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		pds("Finished with demo script. Exiting.");
		failed = false;
	}

	/**
	 * Preparation step: Start plugins by name, prepare ffmpeg-only scaling contract file
	 */
	protected Step createPreparationStep() {
		return new Step() {
			
			@Override
			protected boolean run() throws IOException {
				InputStream is;
				// copy ffmpeg-only scale
				URL tmp = resolvedBundles.get(BundleNames.getServer()).
						getEntry("/models/contracts/scaling.ecl");
//				URL scalingEclDestination = resolve(tmp);
				
				tmp = context.getBundle().getEntry("/contracts/scaling01ffmpeg.ecl");
				URL scalingOnlyFfmpeg = resolve(tmp);
//				copyContents(scalingOnlyFfmpeg, scalingEclDestination);
				
				try {
					// start all plugins of first level
					for(String name : BundleNames.getPreparation()) {
						pds("Starting " + name);
						resolvedBundles.get(name).start();
					}
					String name = BundleNames.getFirstScalingApp();
					pds("Starting " + name);
					resolvedBundles.get(name).start();
				} catch (BundleException e) {
					pds("Got exception while starting bundle. Exiting.");
					e.printStackTrace();
					return false;
				}
				waitAfterContractUpdate();
						
				try {
					is = scalingOnlyFfmpeg.openStream();
				} catch (IOException e) {
					pds("Can't open initial ecl file, error: " + e.getMessage());
					return false;
				}
				gem.updateContract(appName, typeName, FileUtils.readAsString(is));
				try { is.close(); } catch (IOException ignore) {}
				waitAfterContractUpdate();
				return true;
			}
			@Override
			protected boolean after() {
				sleep(1000); // wait for storage of file
				return true;
			}
		};
	}
	
	private void addExecutionSteps(List<Step> steps) {
		class ExecuteStep extends Step {
			URI lemUri;
			public ExecuteStep(URI lemUri) {
				super();
				this.lemUri = lemUri;
			}
			@Override
			protected boolean run() {
				ExecuteResult ee = ExecuteResultSerializer.deserialize(
						gem.executeAtLem(lemUri, null, controllerPortname, appName, controllerCompName,
						fileVideo1, fileVideo2, w, h, mult, pos));
				Serializable result = getResult(ee);
				pds("Got result (execute): " + result);
				if(result != null && !(result instanceof Exception))
					return false;
				return true;
			}
			
		}
		class OptimizeStep extends Step {
			
			@Override
			protected boolean run() {
				//TODO rs: measure energy
				ExecuteResult ee = ExecuteResultSerializer.deserialize(
						gem.optimize(req, false, fileVideo1, fileVideo2, w, h, mult, pos));
				Serializable result = getResult(ee);
				pds("Got result: " + result);
				if(result != null && !(result instanceof Exception))
					return false;
				return true;
			}
		};
		steps.add(new OptimizeStep());
		// grab first uri
		URI lemUri = gem.getLocalMgrs().get(0);
		Step exStep = new ExecuteStep(lemUri);
		for (int i = 0; i < MAX_EXECUTE_COUNT; i++) {
			steps.add(exStep);
		}
	}
	
	/* Copied from OsgiGem */
	public ILocalEnergyManager getLocalEnergyManager(URI container) {
		RemoteOSGiService remote = getRemoteOSGiService();

		final RemoteServiceReference[] srefs = remote
				.getRemoteServiceReferences(new ch.ethz.iks.r_osgi.URI(container+""),
						ILocalEnergyManager.class.getName(), null);

		ILocalEnergyManager lem = (ILocalEnergyManager) remote
				.getRemoteService(srefs[0]);
		return lem;
	}

	private Serializable getResult(ExecuteResult ee) {
		return ee.getResult();
	}

	/**
	 * Second step: Adding MEncoder implementation and update energy managers.
	 * <ol>
	 * <li>Add contract to scaling component</li>
	 * <li>Update contract in lem, ultimatly without benchmarking ffmpeg again</li>
	 * <li>Start of plugin scaling.mencoder (second level)</li>
	 * </ol>
	 */
	protected Step createMencoderStep() {
		return new Step() {
			
			@Override
			protected boolean run() throws IOException {
				InputStream is;
				// Add contract
				URL tmp = context.getBundle().getEntry("/contracts/scaling02ffmpeg_mencoder.ecl");
				URL scalingFfmpegAndMencoder = resolve(tmp);
				
				// Update contract in lem
				try {
					is = scalingFfmpegAndMencoder.openStream();
				} catch (IOException e) {
					pds("Can't open level 02 ecl file, error: " + e.getMessage());
					return false;
				}
				gem.updateContract(appName, typeName, FileUtils.readAsString(is));
				try { is.close(); } catch (IOException ignore) {}
				waitAfterContractUpdate();
				sleep(200); // let file be stored
				
				// Start plugin
				// Benchmark of scaling.mencoder has to start automatically (set theatre.gem.contracts to true)
				try {
					// start scaling.mencoder
					String name = BundleNames.getSecondScalingApp();
					pds("Starting " + name);
					resolvedBundles.get(name).start();
				} catch (BundleException e) {
					pds("Got exception. Exiting.");
					e.printStackTrace();
					return false;
				}
				waitAfterContractUpdate();
				return true;
			}
			@Override
			protected boolean after() {
				sleep(1000); // wait for storage of file
				return true;
			}
		};
	}

	/**
	 * Fourth step: Adding Handbrake implementation and update energy managers.
	 * <ol>
	 * <li>Add contract to scaling component</li>
	 * <li>Update contract in lem, ultimatly without benchmarking ffmpeg and mencoder again</li>
	 * <li>Start of plugin scaling.handbrake (third level)</li>
	 * </ol>
	 */
	protected Step createHandbrakeStep() {
		return new Step() {
			
			@Override
			protected boolean run() throws IOException {
				InputStream is;
				// Add contract
				URL tmp = context.getBundle().getEntry("/contracts/scaling03ffmpeg_mencoder_handbrake.ecl");
				URL scalingAll = resolve(tmp);
				
				// Update contract in lem
				try {
					is = scalingAll.openStream();
				} catch (IOException e) {
					pds("Can't open level 03 ecl file, error: " + e.getMessage());
					return false;
				}
				gem.updateContract(appName, typeName, FileUtils.readAsString(is));
				try { is.close(); } catch (IOException ignore) {}
				waitAfterContractUpdate();
				sleep(200); // let file be stored
				
				// Start plugin
				// Benchmark of scaling.handbrake has to start automatically (set theatre.gem.contracts to true)
				try {
					// start scaling.handbrake
					String name = BundleNames.getThirdScalingApp();
					pds("Starting " + name);
					resolvedBundles.get(name).start();
				} catch (BundleException e) {
					pds("Got exception. Exiting.");
					e.printStackTrace();
					return false;
				}
				waitAfterContractUpdate();
				return true;
			}
			@Override
			protected boolean after() {
				sleep(1000); // wait for storage of file
				return true;
			}
		};
	}
	
	/** <u>P</u>rint <u>s</u>tep with given stepNumber. */
	private void ps(int stepNumber) {
		lastStepNumber  = stepNumber;
		pds("*** Step "+stepNumber+" ***");
	}
	
	/** <u>P</u>rint message with preceding <u>DS</u>. */
	private static void pds(String message) {
		System.out.println("[DS] " + message);
	}
	
//	/** <u>P</u>rint<u>f</u> message with preceding <u>DS</u>. */
//	private static void pdsf(String message, Object... args) {
//		System.out.printf("[DS] " + message, args);
//	}

	private void waitAfterContractUpdate() {
		pds("Waiting for benchmarks.");
		// This should also work, if no contract has to be computed (just wait for fetching contracts)
		gem.waitForCurrentBenchmarksToFinish();
	}

	private void sleep(long millis) {
		pds("Waiting " + millis + "ms.");
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ignore) { }
	}

	public boolean hasFailed() {
		return failed;
	}

//	private String findEclUri(String appName, String compId, String compName) {
//		String appModel = getGEM().getApplicationModel(appName);
//		try {
//			StructuralModel swModel = CCMUtil.deserializeSWStructModel(appName, appModel, false);
//			SWComponentType root = (SWComponentType) swModel.getRoot();
//			List<SWComponentType> toSearch = new LinkedList<SWComponentType>();
//			toSearch.add(root);
//			while(!toSearch.isEmpty()) {
//				SWComponentType current = toSearch.remove(0);
//				if((compId != null && compId.equals(current.getId()) ||
//						(compName != null && compName.equals(current.getName())))) {
//					return current.getEclUri();
//				}
//				toSearch.addAll(root.getSubtypes());
//			}
//			pdsf("No component found with id=%s or name=%s%n", compId, compName);
//			return null;
//		} catch (IOException e) {
//			pds("Could not retrieve scaling uri, error: " + e.getMessage());
//			return null;
//		}
//	}

//	private boolean copyContents(URL source, URL destination) {
//		URI destUri, srcUri;
//		try {
//			srcUri  = source.toURI();
//			destUri = destination.toURI();
//		} catch (URISyntaxException e) {
//			pds("Could create URI. Error: " + e.getMessage());
//			return false;
//		}
//		File srcFile  = new File(srcUri);
//		File destFile = new File(destUri);
//		Writer fw = null;
//		try {
//			fw = new FileWriter(destFile);
//		} catch (IOException e) {
//			pds("Could not create writer for " + destFile + ", error: " + e.getMessage());
//			return false;
//		}
//		BufferedWriter bw = new BufferedWriter(fw);
//		FileReader fr = null;
//		try {
//			fr = new FileReader(srcFile);
//		} catch (FileNotFoundException e) {
//			pds("Could not find source file, error: " + e.getMessage());
//		}
//		char[] cbuf = new char[1024];
//		try {
//			while(fr.read(cbuf) != -1) {
//				bw.write(cbuf);
//			}
//		} catch (IOException e) {
//			pds("Error while copying: " + e.getMessage());
//		}
//		if(fr != null) try { fr.close(); } catch(IOException ignore) { }
//		if(fw != null) try { fw.close(); } catch(IOException ignore) { }
//		try { bw.close(); } catch(IOException ignore) { }
//		return true;
//	}

	private URL resolve(URL tmp) {
		try {
			return FileLocator.resolve(tmp);
		} catch (IOException e) {
			pds("Error while resolving " + tmp + ": " + e.getMessage());
			return null;
		}
	}

	private int computeMaxVideoLength(Video video1, Video video2) {
		return TranscodingServerUtil.getMetaparameterValue(
				video1.length, video2.length,
				video1.resX, video1.resY, video2.resX, video2.resY);
	}

	private IGlobalEnergyManager getGEM() {
		IGlobalEnergyManager ret = null;
		RemoteOSGiService remote = getRemoteOSGiService();
		
		String ip = "127.0.0.1";
		final RemoteServiceReference[] srefs =
			remote.getRemoteServiceReferences(RemoteOsgiUtil.createRemoteOsgiUri(ip),
				IGlobalEnergyManager.class.getName(), null);
		
		ret = (IGlobalEnergyManager)remote.getRemoteService(srefs[0]);
		return ret;
	}
	
	public RemoteOSGiService getRemoteOSGiService() {
		BundleContext context = FrameworkUtil.getBundle(this.getClass())
				.getBundleContext();
		ServiceReference<?> remoteRef = context
				.getServiceReference(RemoteOSGiService.class.getName());
		if (remoteRef == null) {
			pds("Error: R-OSGi not found!");
		}
		RemoteOSGiService remote = (RemoteOSGiService) context
				.getService(remoteRef);
		return remote;
	}

	
	int stepCounter = 0;
	abstract class Step {
		Exception failure;
		private final int nr;
		public Step() {
			this(stepCounter++);
		}
		protected Step(int stepnr) {
			this.nr = stepnr;
		}
		/**
		 * Runs the step performing the following actions:
		 * <ol>
		 * <li>Call before()</li>
		 * <li>Call run(), measuring the call with regard to the property of the monitor</li>
		 * <li>Call after()</li>
		 * <li>Return whether to proceed with the next step,
		 *  i.e. whether this step has not failed</li>
		 * </ol>
		 * @return success and move on?
		 */
		public boolean runStep() {
			ps(nr);
			boolean success = false;
			if(before()) {
				monitor.before();
				try {
					success = run();
				} catch (Exception e) {
					success = false;
				}
				monitor.after();
				try {
					writeDiff(nr, monitor.getDiff());
				} catch (IOException e) {
					failure = e;
					return false;
				}
				if(success)
					success = after();
			}
			return success;
		}
		/**
		 * Default: return true
		 * @return move on?
		 */
		protected boolean before() {
			return true;
		}
		/**
		 * @return move on?
		 */
		abstract protected boolean run() throws Exception;
		/**
		 * Default: return true
		 * @return move on?
		 */
		protected boolean after() {
			return true;
		}
		@Override
		public String toString() {
			return "Step"+nr;
		}
		public int getNr() {
			return nr;
		}
	}

	public void writeDiff(int stepnr, String diff) throws IOException {
		bw.write(stepnr + ";" + diff);
		bw.newLine();
	}

}
