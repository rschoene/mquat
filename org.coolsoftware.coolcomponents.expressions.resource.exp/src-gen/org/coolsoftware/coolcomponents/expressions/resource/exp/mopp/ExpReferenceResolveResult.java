/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;

/**
 * A basic implementation of the
 * org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolveRes
 * ult interface that collects mappings in a list.
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class ExpReferenceResolveResult<ReferenceType> implements org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolveResult<ReferenceType> {
	
	private java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private java.util.Set<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix> quickFixes;
	
	public ExpReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix>();
		}
		return java.util.Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new java.util.LinkedHashSet<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, org.eclipse.emf.common.util.URI uri, String warning) {
		if (mappings == null) {
			mappings = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
