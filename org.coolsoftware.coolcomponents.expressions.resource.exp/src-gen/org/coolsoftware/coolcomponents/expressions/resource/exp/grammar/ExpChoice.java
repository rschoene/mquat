/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.grammar;

public class ExpChoice extends org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpSyntaxElement {
	
	public ExpChoice(org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpCardinality cardinality, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpStringUtil.explode(getChildren(), "|");
	}
	
}
