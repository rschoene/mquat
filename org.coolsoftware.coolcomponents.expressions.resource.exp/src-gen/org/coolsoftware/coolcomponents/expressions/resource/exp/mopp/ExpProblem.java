/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;

public class ExpProblem implements org.coolsoftware.coolcomponents.expressions.resource.exp.IExpProblem {
	
	private String message;
	private org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType type;
	private org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity severity;
	private java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix> quickFixes;
	
	public ExpProblem(String message, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType type, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix>emptySet());
	}
	
	public ExpProblem(String message, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType type, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity severity, org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public ExpProblem(String message, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType type, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity severity, java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType getType() {
		return type;
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
