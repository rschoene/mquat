/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;

public class ExpMetaInformation implements org.coolsoftware.coolcomponents.expressions.resource.exp.IExpMetaInformation {
	
	public String getSyntaxName() {
		return "exp";
	}
	
	public String getURI() {
		return "http://www.cool-software.org/dimm/expressions";
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextScanner createLexer() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpAntlrScanner(new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpLexer());
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpParser().createInstance(inputStream, encoding);
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextPrinter createPrinter(java.io.OutputStream outputStream, org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolverSwitch getReferenceResolverSwitch() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpReferenceResolverSwitch();
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolverFactory getTokenResolverFactory() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "org.coolsoftware.coolcomponents.expressions/metamodel/expressions.cs";
	}
	
	public String[] getTokenNames() {
		return org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpParser.tokenNames;
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenStyle getDefaultTokenStyle(String tokenName) {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpBracketPair> getBracketPairs() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResourceFactory();
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpNewFileContentProvider getNewFileContentProvider() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "org.coolsoftware.coolcomponents.expressions.resource.exp.ui.launchConfigurationType";
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpNameProvider createNameProvider() {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.analysis.ExpDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpAntlrTokenHelper tokenHelper = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
