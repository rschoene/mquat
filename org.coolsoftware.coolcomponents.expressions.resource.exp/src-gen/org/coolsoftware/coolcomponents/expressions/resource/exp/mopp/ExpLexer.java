/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 3.4

	package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class ExpLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int ILT=4;
    public static final int LINEBREAK=5;
    public static final int ML_COMMENT=6;
    public static final int QUOTED_34_34=7;
    public static final int REAL_LITERAL=8;
    public static final int SL_COMMENT=9;
    public static final int TEXT=10;
    public static final int TYPE_LITERAL=11;
    public static final int WHITESPACE=12;

    	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
    	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
    	
    	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
    		lexerExceptions.add(e);
    		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
    	}


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public ExpLexer() {} 
    public ExpLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public ExpLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "Exp.g"; }

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:15:7: ( '!' )
            // Exp.g:15:9: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:16:7: ( '!=' )
            // Exp.g:16:9: '!='
            {
            match("!="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:17:7: ( '(' )
            // Exp.g:17:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:18:7: ( ')' )
            // Exp.g:18:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:19:7: ( '*' )
            // Exp.g:19:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:20:7: ( '+' )
            // Exp.g:20:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:21:7: ( '++' )
            // Exp.g:21:9: '++'
            {
            match("++"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:22:7: ( '+=' )
            // Exp.g:22:9: '+='
            {
            match("+="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:23:7: ( '-' )
            // Exp.g:23:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:24:7: ( '--' )
            // Exp.g:24:9: '--'
            {
            match("--"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:25:7: ( '-=' )
            // Exp.g:25:9: '-='
            {
            match("-="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:26:7: ( '/' )
            // Exp.g:26:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:27:7: ( ':' )
            // Exp.g:27:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:28:7: ( ';' )
            // Exp.g:28:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:29:7: ( '<' )
            // Exp.g:29:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:30:7: ( '<=' )
            // Exp.g:30:9: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:31:7: ( '=' )
            // Exp.g:31:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:32:7: ( '==' )
            // Exp.g:32:9: '=='
            {
            match("=="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:33:7: ( '>' )
            // Exp.g:33:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:34:7: ( '>=' )
            // Exp.g:34:9: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:35:7: ( '^' )
            // Exp.g:35:9: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:36:7: ( 'and' )
            // Exp.g:36:9: 'and'
            {
            match("and"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:37:7: ( 'cos' )
            // Exp.g:37:9: 'cos'
            {
            match("cos"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:38:7: ( 'f' )
            // Exp.g:38:9: 'f'
            {
            match('f'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:39:7: ( 'false' )
            // Exp.g:39:9: 'false'
            {
            match("false"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:40:7: ( 'implies' )
            // Exp.g:40:9: 'implies'
            {
            match("implies"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:41:7: ( 'not' )
            // Exp.g:41:9: 'not'
            {
            match("not"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:42:7: ( 'or' )
            // Exp.g:42:9: 'or'
            {
            match("or"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:43:7: ( 'sin' )
            // Exp.g:43:9: 'sin'
            {
            match("sin"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:44:7: ( 'true' )
            // Exp.g:44:9: 'true'
            {
            match("true"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:45:7: ( 'var' )
            // Exp.g:45:9: 'var'
            {
            match("var"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:46:7: ( '{' )
            // Exp.g:46:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:47:7: ( '}' )
            // Exp.g:47:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "SL_COMMENT"
    public final void mSL_COMMENT() throws RecognitionException {
        try {
            int _type = SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3246:11: ( ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* ) )
            // Exp.g:3247:3: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            {
            // Exp.g:3247:3: ( '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )* )
            // Exp.g:3247:3: '//' (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            {
            match("//"); 



            // Exp.g:3247:7: (~ ( '\\n' | '\\r' | '\\uffff' ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= '\u0000' && LA1_0 <= '\t')||(LA1_0 >= '\u000B' && LA1_0 <= '\f')||(LA1_0 >= '\u000E' && LA1_0 <= '\uFFFE')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Exp.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFE') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SL_COMMENT"

    // $ANTLR start "ML_COMMENT"
    public final void mML_COMMENT() throws RecognitionException {
        try {
            int _type = ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3250:11: ( ( '/*' ( . )* '*/' ) )
            // Exp.g:3251:3: ( '/*' ( . )* '*/' )
            {
            // Exp.g:3251:3: ( '/*' ( . )* '*/' )
            // Exp.g:3251:3: '/*' ( . )* '*/'
            {
            match("/*"); 



            // Exp.g:3251:7: ( . )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='*') ) {
                    int LA2_1 = input.LA(2);

                    if ( (LA2_1=='/') ) {
                        alt2=2;
                    }
                    else if ( ((LA2_1 >= '\u0000' && LA2_1 <= '.')||(LA2_1 >= '0' && LA2_1 <= '\uFFFF')) ) {
                        alt2=1;
                    }


                }
                else if ( ((LA2_0 >= '\u0000' && LA2_0 <= ')')||(LA2_0 >= '+' && LA2_0 <= '\uFFFF')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Exp.g:3251:7: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            match("*/"); 



            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ML_COMMENT"

    // $ANTLR start "ILT"
    public final void mILT() throws RecognitionException {
        try {
            int _type = ILT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3254:4: ( ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? ) )
            // Exp.g:3255:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            {
            // Exp.g:3255:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            // Exp.g:3255:2: ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            {
            // Exp.g:3255:2: ( '-' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='-') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // Exp.g:3255:2: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Exp.g:3255:6: ( '0' .. '9' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // Exp.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            // Exp.g:3255:17: ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='E'||LA7_0=='e') ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // Exp.g:3255:18: ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Exp.g:3255:27: ( '-' )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0=='-') ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // Exp.g:3255:28: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // Exp.g:3255:33: ( '0' .. '9' )+
                    int cnt6=0;
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // Exp.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt6 >= 1 ) break loop6;
                                EarlyExitException eee =
                                    new EarlyExitException(6, input);
                                throw eee;
                        }
                        cnt6++;
                    } while (true);


                    }
                    break;

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ILT"

    // $ANTLR start "REAL_LITERAL"
    public final void mREAL_LITERAL() throws RecognitionException {
        try {
            int _type = REAL_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3257:13: ( ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? ) )
            // Exp.g:3258:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            {
            // Exp.g:3258:2: ( ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? )
            // Exp.g:3258:2: ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )? '.' ( '-' )? ( '0' .. '9' )+ ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            {
            // Exp.g:3258:2: ( '-' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='-') ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // Exp.g:3258:2: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Exp.g:3258:6: ( '0' .. '9' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0 >= '0' && LA9_0 <= '9')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // Exp.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            // Exp.g:3258:17: ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='E'||LA12_0=='e') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // Exp.g:3258:18: ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Exp.g:3258:27: ( '-' )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0=='-') ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // Exp.g:3258:28: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // Exp.g:3258:33: ( '0' .. '9' )+
                    int cnt11=0;
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( ((LA11_0 >= '0' && LA11_0 <= '9')) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // Exp.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt11 >= 1 ) break loop11;
                                EarlyExitException eee =
                                    new EarlyExitException(11, input);
                                throw eee;
                        }
                        cnt11++;
                    } while (true);


                    }
                    break;

            }


            match('.'); 

            // Exp.g:3258:49: ( '-' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='-') ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // Exp.g:3258:49: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // Exp.g:3258:53: ( '0' .. '9' )+
            int cnt14=0;
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0 >= '0' && LA14_0 <= '9')) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // Exp.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);


            // Exp.g:3258:64: ( ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+ )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0=='E'||LA17_0=='e') ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // Exp.g:3258:65: ( 'e' | 'E' ) ( '-' )? ( '0' .. '9' )+
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    // Exp.g:3258:74: ( '-' )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0=='-') ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // Exp.g:3258:75: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // Exp.g:3258:80: ( '0' .. '9' )+
                    int cnt16=0;
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( ((LA16_0 >= '0' && LA16_0 <= '9')) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // Exp.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt16 >= 1 ) break loop16;
                                EarlyExitException eee =
                                    new EarlyExitException(16, input);
                                throw eee;
                        }
                        cnt16++;
                    } while (true);


                    }
                    break;

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "REAL_LITERAL"

    // $ANTLR start "TYPE_LITERAL"
    public final void mTYPE_LITERAL() throws RecognitionException {
        try {
            int _type = TYPE_LITERAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3260:13: ( ( ( 'Boolean' | 'Integer' | 'Real' | 'String' ) ) )
            // Exp.g:3261:2: ( ( 'Boolean' | 'Integer' | 'Real' | 'String' ) )
            {
            // Exp.g:3261:2: ( ( 'Boolean' | 'Integer' | 'Real' | 'String' ) )
            // Exp.g:3261:2: ( 'Boolean' | 'Integer' | 'Real' | 'String' )
            {
            // Exp.g:3261:2: ( 'Boolean' | 'Integer' | 'Real' | 'String' )
            int alt18=4;
            switch ( input.LA(1) ) {
            case 'B':
                {
                alt18=1;
                }
                break;
            case 'I':
                {
                alt18=2;
                }
                break;
            case 'R':
                {
                alt18=3;
                }
                break;
            case 'S':
                {
                alt18=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;

            }

            switch (alt18) {
                case 1 :
                    // Exp.g:3261:3: 'Boolean'
                    {
                    match("Boolean"); 



                    }
                    break;
                case 2 :
                    // Exp.g:3261:15: 'Integer'
                    {
                    match("Integer"); 



                    }
                    break;
                case 3 :
                    // Exp.g:3261:27: 'Real'
                    {
                    match("Real"); 



                    }
                    break;
                case 4 :
                    // Exp.g:3261:36: 'String'
                    {
                    match("String"); 



                    }
                    break;

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TYPE_LITERAL"

    // $ANTLR start "TEXT"
    public final void mTEXT() throws RecognitionException {
        try {
            int _type = TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3263:5: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ ) )
            // Exp.g:3264:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ )
            {
            // Exp.g:3264:2: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+ )
            // Exp.g:3264:2: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+
            {
            // Exp.g:3264:2: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' | '-' )+
            int cnt19=0;
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0=='-'||(LA19_0 >= '0' && LA19_0 <= '9')||(LA19_0 >= 'A' && LA19_0 <= 'Z')||LA19_0=='_'||(LA19_0 >= 'a' && LA19_0 <= 'z')) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // Exp.g:
            	    {
            	    if ( input.LA(1)=='-'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt19 >= 1 ) break loop19;
                        EarlyExitException eee =
                            new EarlyExitException(19, input);
                        throw eee;
                }
                cnt19++;
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TEXT"

    // $ANTLR start "WHITESPACE"
    public final void mWHITESPACE() throws RecognitionException {
        try {
            int _type = WHITESPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3266:11: ( ( ( ' ' | '\\t' | '\\f' ) ) )
            // Exp.g:3267:2: ( ( ' ' | '\\t' | '\\f' ) )
            {
            if ( input.LA(1)=='\t'||input.LA(1)=='\f'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "LINEBREAK"
    public final void mLINEBREAK() throws RecognitionException {
        try {
            int _type = LINEBREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3270:10: ( ( ( '\\r\\n' | '\\r' | '\\n' ) ) )
            // Exp.g:3271:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            {
            // Exp.g:3271:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            // Exp.g:3271:2: ( '\\r\\n' | '\\r' | '\\n' )
            {
            // Exp.g:3271:2: ( '\\r\\n' | '\\r' | '\\n' )
            int alt20=3;
            int LA20_0 = input.LA(1);

            if ( (LA20_0=='\r') ) {
                int LA20_1 = input.LA(2);

                if ( (LA20_1=='\n') ) {
                    alt20=1;
                }
                else {
                    alt20=2;
                }
            }
            else if ( (LA20_0=='\n') ) {
                alt20=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;

            }
            switch (alt20) {
                case 1 :
                    // Exp.g:3271:3: '\\r\\n'
                    {
                    match("\r\n"); 



                    }
                    break;
                case 2 :
                    // Exp.g:3271:12: '\\r'
                    {
                    match('\r'); 

                    }
                    break;
                case 3 :
                    // Exp.g:3271:19: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINEBREAK"

    // $ANTLR start "QUOTED_34_34"
    public final void mQUOTED_34_34() throws RecognitionException {
        try {
            int _type = QUOTED_34_34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Exp.g:3274:13: ( ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) ) )
            // Exp.g:3275:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            {
            // Exp.g:3275:2: ( ( '\"' ) (~ ( '\"' ) )* ( '\"' ) )
            // Exp.g:3275:2: ( '\"' ) (~ ( '\"' ) )* ( '\"' )
            {
            // Exp.g:3275:2: ( '\"' )
            // Exp.g:3275:3: '\"'
            {
            match('\"'); 

            }


            // Exp.g:3275:7: (~ ( '\"' ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0 >= '\u0000' && LA21_0 <= '!')||(LA21_0 >= '#' && LA21_0 <= '\uFFFF')) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // Exp.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            // Exp.g:3275:16: ( '\"' )
            // Exp.g:3275:17: '\"'
            {
            match('\"'); 

            }


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "QUOTED_34_34"

    public void mTokens() throws RecognitionException {
        // Exp.g:1:8: ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | SL_COMMENT | ML_COMMENT | ILT | REAL_LITERAL | TYPE_LITERAL | TEXT | WHITESPACE | LINEBREAK | QUOTED_34_34 )
        int alt22=42;
        alt22 = dfa22.predict(input);
        switch (alt22) {
            case 1 :
                // Exp.g:1:10: T__13
                {
                mT__13(); 


                }
                break;
            case 2 :
                // Exp.g:1:16: T__14
                {
                mT__14(); 


                }
                break;
            case 3 :
                // Exp.g:1:22: T__15
                {
                mT__15(); 


                }
                break;
            case 4 :
                // Exp.g:1:28: T__16
                {
                mT__16(); 


                }
                break;
            case 5 :
                // Exp.g:1:34: T__17
                {
                mT__17(); 


                }
                break;
            case 6 :
                // Exp.g:1:40: T__18
                {
                mT__18(); 


                }
                break;
            case 7 :
                // Exp.g:1:46: T__19
                {
                mT__19(); 


                }
                break;
            case 8 :
                // Exp.g:1:52: T__20
                {
                mT__20(); 


                }
                break;
            case 9 :
                // Exp.g:1:58: T__21
                {
                mT__21(); 


                }
                break;
            case 10 :
                // Exp.g:1:64: T__22
                {
                mT__22(); 


                }
                break;
            case 11 :
                // Exp.g:1:70: T__23
                {
                mT__23(); 


                }
                break;
            case 12 :
                // Exp.g:1:76: T__24
                {
                mT__24(); 


                }
                break;
            case 13 :
                // Exp.g:1:82: T__25
                {
                mT__25(); 


                }
                break;
            case 14 :
                // Exp.g:1:88: T__26
                {
                mT__26(); 


                }
                break;
            case 15 :
                // Exp.g:1:94: T__27
                {
                mT__27(); 


                }
                break;
            case 16 :
                // Exp.g:1:100: T__28
                {
                mT__28(); 


                }
                break;
            case 17 :
                // Exp.g:1:106: T__29
                {
                mT__29(); 


                }
                break;
            case 18 :
                // Exp.g:1:112: T__30
                {
                mT__30(); 


                }
                break;
            case 19 :
                // Exp.g:1:118: T__31
                {
                mT__31(); 


                }
                break;
            case 20 :
                // Exp.g:1:124: T__32
                {
                mT__32(); 


                }
                break;
            case 21 :
                // Exp.g:1:130: T__33
                {
                mT__33(); 


                }
                break;
            case 22 :
                // Exp.g:1:136: T__34
                {
                mT__34(); 


                }
                break;
            case 23 :
                // Exp.g:1:142: T__35
                {
                mT__35(); 


                }
                break;
            case 24 :
                // Exp.g:1:148: T__36
                {
                mT__36(); 


                }
                break;
            case 25 :
                // Exp.g:1:154: T__37
                {
                mT__37(); 


                }
                break;
            case 26 :
                // Exp.g:1:160: T__38
                {
                mT__38(); 


                }
                break;
            case 27 :
                // Exp.g:1:166: T__39
                {
                mT__39(); 


                }
                break;
            case 28 :
                // Exp.g:1:172: T__40
                {
                mT__40(); 


                }
                break;
            case 29 :
                // Exp.g:1:178: T__41
                {
                mT__41(); 


                }
                break;
            case 30 :
                // Exp.g:1:184: T__42
                {
                mT__42(); 


                }
                break;
            case 31 :
                // Exp.g:1:190: T__43
                {
                mT__43(); 


                }
                break;
            case 32 :
                // Exp.g:1:196: T__44
                {
                mT__44(); 


                }
                break;
            case 33 :
                // Exp.g:1:202: T__45
                {
                mT__45(); 


                }
                break;
            case 34 :
                // Exp.g:1:208: SL_COMMENT
                {
                mSL_COMMENT(); 


                }
                break;
            case 35 :
                // Exp.g:1:219: ML_COMMENT
                {
                mML_COMMENT(); 


                }
                break;
            case 36 :
                // Exp.g:1:230: ILT
                {
                mILT(); 


                }
                break;
            case 37 :
                // Exp.g:1:234: REAL_LITERAL
                {
                mREAL_LITERAL(); 


                }
                break;
            case 38 :
                // Exp.g:1:247: TYPE_LITERAL
                {
                mTYPE_LITERAL(); 


                }
                break;
            case 39 :
                // Exp.g:1:260: TEXT
                {
                mTEXT(); 


                }
                break;
            case 40 :
                // Exp.g:1:265: WHITESPACE
                {
                mWHITESPACE(); 


                }
                break;
            case 41 :
                // Exp.g:1:276: LINEBREAK
                {
                mLINEBREAK(); 


                }
                break;
            case 42 :
                // Exp.g:1:286: QUOTED_34_34
                {
                mQUOTED_34_34(); 


                }
                break;

        }

    }


    protected DFA22 dfa22 = new DFA22(this);
    static final String DFA22_eotS =
        "\1\uffff\1\43\3\uffff\1\46\1\51\1\54\2\uffff\1\56\1\60\1\62\1\uffff"+
        "\2\36\1\66\6\36\2\uffff\1\76\4\36\11\uffff\1\104\13\uffff\3\36\1"+
        "\uffff\2\36\1\112\4\36\2\uffff\4\36\1\uffff\1\124\1\125\2\36\1\130"+
        "\1\uffff\1\131\1\36\1\133\1\36\1\76\4\36\2\uffff\2\36\2\uffff\1"+
        "\142\1\uffff\2\36\1\145\1\36\1\147\1\36\1\uffff\2\36\1\uffff\1\36"+
        "\1\uffff\3\36\1\145\1\157\2\145\1\uffff";
    static final String DFA22_eofS =
        "\160\uffff";
    static final String DFA22_minS =
        "\1\11\1\75\3\uffff\1\53\1\55\1\52\2\uffff\3\75\1\uffff\1\156\1\157"+
        "\1\55\1\155\1\157\1\162\1\151\1\162\1\141\2\uffff\1\55\1\157\1\156"+
        "\1\145\1\164\11\uffff\1\55\13\uffff\1\144\1\163\1\154\1\uffff\1"+
        "\160\1\164\1\55\1\156\1\165\1\162\1\55\2\uffff\1\157\1\164\1\141"+
        "\1\162\1\uffff\2\55\1\163\1\154\1\55\1\uffff\1\55\1\145\1\55\1\60"+
        "\1\55\1\154\1\145\1\154\1\151\2\uffff\1\145\1\151\2\uffff\1\55\1"+
        "\uffff\1\145\1\147\1\55\1\156\1\55\1\145\1\uffff\1\141\1\145\1\uffff"+
        "\1\147\1\uffff\1\163\1\156\1\162\4\55\1\uffff";
    static final String DFA22_maxS =
        "\1\175\1\75\3\uffff\1\75\1\172\1\57\2\uffff\3\75\1\uffff\1\156\1"+
        "\157\1\172\1\155\1\157\1\162\1\151\1\162\1\141\2\uffff\1\172\1\157"+
        "\1\156\1\145\1\164\11\uffff\1\172\13\uffff\1\144\1\163\1\154\1\uffff"+
        "\1\160\1\164\1\172\1\156\1\165\1\162\1\71\2\uffff\1\157\1\164\1"+
        "\141\1\162\1\uffff\2\172\1\163\1\154\1\172\1\uffff\1\172\1\145\1"+
        "\172\1\71\1\172\1\154\1\145\1\154\1\151\2\uffff\1\145\1\151\2\uffff"+
        "\1\172\1\uffff\1\145\1\147\1\172\1\156\1\172\1\145\1\uffff\1\141"+
        "\1\145\1\uffff\1\147\1\uffff\1\163\1\156\1\162\4\172\1\uffff";
    static final String DFA22_acceptS =
        "\2\uffff\1\3\1\4\1\5\3\uffff\1\15\1\16\3\uffff\1\25\11\uffff\1\40"+
        "\1\41\5\uffff\1\47\1\50\1\51\1\52\1\2\1\1\1\7\1\10\1\6\1\uffff\1"+
        "\13\1\11\1\42\1\43\1\14\1\20\1\17\1\22\1\21\1\24\1\23\3\uffff\1"+
        "\30\7\uffff\1\44\1\45\4\uffff\1\12\5\uffff\1\34\11\uffff\1\26\1"+
        "\27\2\uffff\1\33\1\35\1\uffff\1\37\6\uffff\1\36\2\uffff\1\46\1\uffff"+
        "\1\31\7\uffff\1\32";
    static final String DFA22_specialS =
        "\160\uffff}>";
    static final String[] DFA22_transitionS = {
            "\1\37\1\40\1\uffff\1\37\1\40\22\uffff\1\37\1\1\1\41\5\uffff"+
            "\1\2\1\3\1\4\1\5\1\uffff\1\6\1\uffff\1\7\12\31\1\10\1\11\1\12"+
            "\1\13\1\14\2\uffff\1\36\1\32\6\36\1\33\10\36\1\34\1\35\7\36"+
            "\3\uffff\1\15\1\36\1\uffff\1\16\1\36\1\17\2\36\1\20\2\36\1\21"+
            "\4\36\1\22\1\23\3\36\1\24\1\25\1\36\1\26\4\36\1\27\1\uffff\1"+
            "\30",
            "\1\42",
            "",
            "",
            "",
            "\1\44\21\uffff\1\45",
            "\1\47\2\uffff\12\31\3\uffff\1\50\3\uffff\32\36\4\uffff\1\36"+
            "\1\uffff\32\36",
            "\1\53\4\uffff\1\52",
            "",
            "",
            "\1\55",
            "\1\57",
            "\1\61",
            "",
            "\1\63",
            "\1\64",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\1\65"+
            "\31\36",
            "\1\67",
            "\1\70",
            "\1\71",
            "\1\72",
            "\1\73",
            "\1\74",
            "",
            "",
            "\1\36\1\77\1\uffff\12\31\7\uffff\4\36\1\75\25\36\4\uffff\1"+
            "\36\1\uffff\4\36\1\75\25\36",
            "\1\100",
            "\1\101",
            "\1\102",
            "\1\103",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\105",
            "\1\106",
            "\1\107",
            "",
            "\1\110",
            "\1\111",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\113",
            "\1\114",
            "\1\115",
            "\1\116\2\uffff\12\117",
            "",
            "",
            "\1\120",
            "\1\121",
            "\1\122",
            "\1\123",
            "",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\126",
            "\1\127",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\132",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\12\117",
            "\1\36\1\77\1\uffff\12\117\7\uffff\32\36\4\uffff\1\36\1\uffff"+
            "\32\36",
            "\1\134",
            "\1\135",
            "\1\136",
            "\1\137",
            "",
            "",
            "\1\140",
            "\1\141",
            "",
            "",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\143",
            "\1\144",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\146",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\150",
            "",
            "\1\151",
            "\1\152",
            "",
            "\1\153",
            "",
            "\1\154",
            "\1\155",
            "\1\156",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\36\2\uffff\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            ""
    };

    static final short[] DFA22_eot = DFA.unpackEncodedString(DFA22_eotS);
    static final short[] DFA22_eof = DFA.unpackEncodedString(DFA22_eofS);
    static final char[] DFA22_min = DFA.unpackEncodedStringToUnsignedChars(DFA22_minS);
    static final char[] DFA22_max = DFA.unpackEncodedStringToUnsignedChars(DFA22_maxS);
    static final short[] DFA22_accept = DFA.unpackEncodedString(DFA22_acceptS);
    static final short[] DFA22_special = DFA.unpackEncodedString(DFA22_specialS);
    static final short[][] DFA22_transition;

    static {
        int numStates = DFA22_transitionS.length;
        DFA22_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA22_transition[i] = DFA.unpackEncodedString(DFA22_transitionS[i]);
        }
    }

    class DFA22 extends DFA {

        public DFA22(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 22;
            this.eot = DFA22_eot;
            this.eof = DFA22_eof;
            this.min = DFA22_min;
            this.max = DFA22_max;
            this.accept = DFA22_accept;
            this.special = DFA22_special;
            this.transition = DFA22_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | SL_COMMENT | ML_COMMENT | ILT | REAL_LITERAL | TYPE_LITERAL | TEXT | WHITESPACE | LINEBREAK | QUOTED_34_34 );";
        }
    }
 

}