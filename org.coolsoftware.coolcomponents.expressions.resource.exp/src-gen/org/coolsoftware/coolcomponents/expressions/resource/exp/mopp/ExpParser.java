/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// $ANTLR 3.4

	package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class ExpParser extends ExpANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ILT", "LINEBREAK", "ML_COMMENT", "QUOTED_34_34", "REAL_LITERAL", "SL_COMMENT", "TEXT", "TYPE_LITERAL", "WHITESPACE", "'!'", "'!='", "'('", "')'", "'*'", "'+'", "'++'", "'+='", "'-'", "'--'", "'-='", "'/'", "':'", "';'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'^'", "'and'", "'cos'", "'f'", "'false'", "'implies'", "'not'", "'or'", "'sin'", "'true'", "'var'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int ILT=4;
    public static final int LINEBREAK=5;
    public static final int ML_COMMENT=6;
    public static final int QUOTED_34_34=7;
    public static final int REAL_LITERAL=8;
    public static final int SL_COMMENT=9;
    public static final int TEXT=10;
    public static final int TYPE_LITERAL=11;
    public static final int WHITESPACE=12;

    // delegates
    public ExpANTLRParserBase[] getDelegates() {
        return new ExpANTLRParserBase[] {};
    }

    // delegators


    public ExpParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public ExpParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(69 + 1);
         

    }

    public String[] getTokenNames() { return ExpParser.tokenNames; }
    public String getGrammarFileName() { return "Exp.g"; }


    	private org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolverFactory tokenResolverFactory = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
    			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpProblem() {
    					public org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity getSeverity() {
    						return org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity.ERROR;
    					}
    					public org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType getType() {
    						return org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement terminal = org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpFollowSetProvider.TERMINALS[terminalID];
    		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[] containmentFeatures = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpFollowSetProvider.LINKS[ids[i]];
    		}
    		org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace containmentTrace = new org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace(eClass, containmentFeatures);
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal expectedElement = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
    			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
    				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
    			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
    				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
    			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
    				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new ExpParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ExpLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new ExpParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ExpLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public ExpParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((ExpLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((ExpLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.Block.class) {
    				return parse_org_coolsoftware_coolcomponents_expressions_Block();
    			}
    			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.variables.Variable.class) {
    				return parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();
    			}
    		}
    		throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>>();
    		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpParseResult parseResult = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		org.coolsoftware.coolcomponents.expressions.resource.exp.IExpParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal>();
    		java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 48;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpPair<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpPair<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[]> newFollowerPair : newFollowers) {
    							org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace containmentTrace = new org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace(null, newFollowerPair.getRight());
    							org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal newFollowTerminal = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Exp.g:502:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_org_coolsoftware_coolcomponents_expressions_Expression |c1= parse_org_coolsoftware_coolcomponents_expressions_Block ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        org.coolsoftware.coolcomponents.expressions.Expression c0 =null;

        org.coolsoftware.coolcomponents.expressions.Block c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Exp.g:503:2: ( (c0= parse_org_coolsoftware_coolcomponents_expressions_Expression |c1= parse_org_coolsoftware_coolcomponents_expressions_Block ) EOF )
            // Exp.g:504:2: (c0= parse_org_coolsoftware_coolcomponents_expressions_Expression |c1= parse_org_coolsoftware_coolcomponents_expressions_Block ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[0]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[1]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[2]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[3]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[4]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[5]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[6]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[7]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[8]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[9]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[10]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[11]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[12]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[13]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[14]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[15]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[16]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[17]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[18]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[19]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[20]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[21]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[22]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[23]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[24]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[25]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[26]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[27]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[28]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[29]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[30]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[31]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[32]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[33]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[34]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[35]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[36]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[37]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[38]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[39]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[40]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[41]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[42]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[43]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[44]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[45]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[46]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[47]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[48]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[49]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[50]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[51]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[52]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[53]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[54]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[55]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[56]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[57]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[58]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[59]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[60]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[61]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[62]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[63]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[64]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[65]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[66]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[67]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[68]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[69]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[70]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[71]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[72]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[73]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[74]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[75]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[76]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[77]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[78]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[79]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[80]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[81]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[82]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[83]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[84]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[85]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[86]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[87]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[88]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[89]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[90]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[91]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[92]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[93]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[94]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[95]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[96]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[97]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[98]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[99]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[100]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[101]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[102]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[103]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[104]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[105]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[106]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[107]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[108]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[109]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[110]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[111]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[112]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[113]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[114]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[115]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[116]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[117]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[118]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[119]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[120]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[121]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[122]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[123]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[124]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[125]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[126]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[127]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[128]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[129]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[130]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[131]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[132]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[133]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[134]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[135]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[136]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[137]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[138]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[139]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[140]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[141]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[142]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[143]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[144]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[145]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[146]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[147]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[148]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[149]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[150]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[151]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[152]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[153]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[154]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[155]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[156]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[157]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[158]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[159]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[160]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[161]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[162]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[163]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[164]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[165]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[166]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[167]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[168]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[169]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[170]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[171]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[172]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[173]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[174]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[175]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[176]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[177]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[178]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[179]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[180]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[181]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[182]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[183]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[184]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[185]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[186]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[187]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[188]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[189]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[190]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[191]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[192]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[193]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[194]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[195]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[196]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[197]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[198]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[199]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[200]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[201]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[202]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[203]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[204]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[205]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[206]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[207]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[208]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[209]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[210]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[211]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[212]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[213]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[214]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[215]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[216]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[217]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[218]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[219]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[220]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[221]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[222]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[223]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[224]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[225]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[226]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[227]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[228]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[229]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[230]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[231]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[232]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[233]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[234]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[235]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[236]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[237]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[238]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[239]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[240]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Exp.g:749:2: (c0= parse_org_coolsoftware_coolcomponents_expressions_Expression |c1= parse_org_coolsoftware_coolcomponents_expressions_Block )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==ILT||(LA1_0 >= QUOTED_34_34 && LA1_0 <= REAL_LITERAL)||LA1_0==TEXT||LA1_0==13||LA1_0==15||(LA1_0 >= 35 && LA1_0 <= 37)||LA1_0==39||(LA1_0 >= 41 && LA1_0 <= 43)) ) {
                alt1=1;
            }
            else if ( (LA1_0==44) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }
            switch (alt1) {
                case 1 :
                    // Exp.g:750:3: c0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_start82);
                    c0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; }

                    }
                    break;
                case 2 :
                    // Exp.g:751:8: c1= parse_org_coolsoftware_coolcomponents_expressions_Block
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Block_in_start96);
                    c1=parse_org_coolsoftware_coolcomponents_expressions_Block();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; }

                    }
                    break;

            }


            match(input,EOF,FOLLOW_EOF_in_start103); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_Block"
    // Exp.g:759:1: parse_org_coolsoftware_coolcomponents_expressions_Block returns [org.coolsoftware.coolcomponents.expressions.Block element = null] : a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}' ;
    public final org.coolsoftware.coolcomponents.expressions.Block parse_org_coolsoftware_coolcomponents_expressions_Block() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Block element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_Block_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        Token a4=null;
        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;

        org.coolsoftware.coolcomponents.expressions.Expression a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Exp.g:762:2: (a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}' )
            // Exp.g:763:2: a0= '{' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )* a4= '}'
            {
            a0=(Token)match(input,44,FOLLOW_44_in_parse_org_coolsoftware_coolcomponents_expressions_Block129); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[241]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[242]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[243]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[244]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[245]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[246]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[247]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[248]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[249]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[250]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[251]);
            		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[252]);
            	}

            // Exp.g:788:2: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            // Exp.g:789:3: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block147);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            				startIncompleteElement(element);
            			}
            			if (a1_0 != null) {
            				if (a1_0 != null) {
            					Object value = a1_0;
            					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_2, a1_0, true);
            				copyLocalizationInfos(a1_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[253]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[254]);
            	}

            // Exp.g:815:2: ( (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==26) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Exp.g:816:3: (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
            	    {
            	    // Exp.g:816:3: (a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
            	    // Exp.g:817:4: a2= ';' (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            	    {
            	    a2=(Token)match(input,26,FOLLOW_26_in_parse_org_coolsoftware_coolcomponents_expressions_Block174); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_4_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[255]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[256]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[257]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[258]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[259]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[260]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[261]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[262]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[263]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[264]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[265]);
            	    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[266]);
            	    			}

            	    // Exp.g:842:4: (a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            	    // Exp.g:843:5: a3_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            	    {
            	    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block200);
            	    a3_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_4_0_0_2, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[267]);
            	    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[268]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[269]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[270]);
            	}

            a4=(Token)match(input,45,FOLLOW_45_in_parse_org_coolsoftware_coolcomponents_expressions_Block241); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_org_coolsoftware_coolcomponents_expressions_Block_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_Block"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_Variable"
    // Exp.g:892:1: parse_org_coolsoftware_coolcomponents_expressions_variables_Variable returns [org.coolsoftware.coolcomponents.expressions.variables.Variable element = null] : (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? ;
    public final org.coolsoftware.coolcomponents.expressions.variables.Variable parse_org_coolsoftware_coolcomponents_expressions_variables_Variable() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.Variable element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Exp.g:895:2: ( (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )? )
            // Exp.g:896:2: (a0= TEXT ) ( (a1= ':' (a2= TYPE_LITERAL ) ) )? ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            {
            // Exp.g:896:2: (a0= TEXT )
            // Exp.g:897:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable274); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[271]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[272]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[273]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[274]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[275]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[276]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[277]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[278]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[279]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[280]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[281]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[282]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[283]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[284]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[285]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[286]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[287]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[288]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[289]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[290]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[291]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[292]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[293]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[294]);
            	}

            // Exp.g:955:2: ( (a1= ':' (a2= TYPE_LITERAL ) ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==25) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // Exp.g:956:3: (a1= ':' (a2= TYPE_LITERAL ) )
                    {
                    // Exp.g:956:3: (a1= ':' (a2= TYPE_LITERAL ) )
                    // Exp.g:957:4: a1= ':' (a2= TYPE_LITERAL )
                    {
                    a1=(Token)match(input,25,FOLLOW_25_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable304); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_1_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[295]);
                    			}

                    // Exp.g:971:4: (a2= TYPE_LITERAL )
                    // Exp.g:972:5: a2= TYPE_LITERAL
                    {
                    a2=(Token)match(input,TYPE_LITERAL,FOLLOW_TYPE_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable330); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                    						startIncompleteElement(element);
                    					}
                    					if (a2 != null) {
                    						org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TYPE_LITERAL");
                    						tokenResolver.setOptions(getOptions());
                    						org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
                    						}
                    						org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral resolved = (org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_1_0_0_3, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[296]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[297]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[298]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[299]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[300]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[301]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[302]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[303]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[304]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[305]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[306]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[307]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[308]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[309]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[310]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[311]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[312]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[313]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[314]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[315]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[316]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[317]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[318]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[319]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[320]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[321]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[322]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[323]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[324]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[325]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[326]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[327]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[328]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[329]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[330]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[331]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[332]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[333]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[334]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[335]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[336]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[337]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[338]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[339]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[340]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[341]);
            	}

            // Exp.g:1058:2: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==29) ) {
                int LA4_1 = input.LA(2);

                if ( (synpred4_Exp()) ) {
                    alt4=1;
                }
            }
            switch (alt4) {
                case 1 :
                    // Exp.g:1059:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    {
                    // Exp.g:1059:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
                    // Exp.g:1060:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    {
                    a3=(Token)match(input,29,FOLLOW_29_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable385); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_2_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[342]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[343]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[344]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[345]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[346]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[347]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[348]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[349]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[350]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[351]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[352]);
                    				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[353]);
                    			}

                    // Exp.g:1085:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
                    // Exp.g:1086:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable411);
                    a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
                    						startIncompleteElement(element);
                    					}
                    					if (a4_0 != null) {
                    						if (a4_0 != null) {
                    							Object value = a4_0;
                    							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_2_0_0_3, a4_0, true);
                    						copyLocalizationInfos(a4_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[354]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[355]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[356]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[357]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[358]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[359]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[360]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[361]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[362]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[363]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[364]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[365]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[366]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[367]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[368]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[369]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[370]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[371]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[372]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[373]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[374]);
                    				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[375]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[376]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[377]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[378]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[379]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[380]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[381]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[382]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[383]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[384]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[385]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[386]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[387]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[388]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[389]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[390]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[391]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[392]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[393]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[394]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[395]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[396]);
            		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[397]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_Variable"



    // $ANTLR start "parseop_Expression_level_03"
    // Exp.g:1162:1: parseop_Expression_level_03 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_03() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_03_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Exp.g:1165:2: (leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |) )
            // Exp.g:1166:2: leftArg= parseop_Expression_level_04 ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_03467);
            leftArg=parseop_Expression_level_04();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:1166:40: ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ |)
            int alt6=2;
            switch ( input.LA(1) ) {
            case 40:
                {
                int LA6_1 = input.LA(2);

                if ( (synpred7_Exp()) ) {
                    alt6=1;
                }
                else if ( (true) ) {
                    alt6=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;

                }
                }
                break;
            case 34:
                {
                int LA6_2 = input.LA(2);

                if ( (synpred7_Exp()) ) {
                    alt6=1;
                }
                else if ( (true) ) {
                    alt6=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 38:
            case 45:
                {
                alt6=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }

            switch (alt6) {
                case 1 :
                    // Exp.g:1166:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
                    {
                    // Exp.g:1166:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
                    int cnt5=0;
                    loop5:
                    do {
                        int alt5=3;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==40) ) {
                            int LA5_2 = input.LA(2);

                            if ( (synpred5_Exp()) ) {
                                alt5=1;
                            }


                        }
                        else if ( (LA5_0==34) ) {
                            int LA5_3 = input.LA(2);

                            if ( (synpred6_Exp()) ) {
                                alt5=2;
                            }


                        }


                        switch (alt5) {
                    	case 1 :
                    	    // Exp.g:1167:3: () a0= 'or' rightArg= parseop_Expression_level_04
                    	    {
                    	    // Exp.g:1167:3: ()
                    	    // Exp.g:1167:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,40,FOLLOW_40_in_parseop_Expression_level_03487); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_25_0_0_2, null, true);
                    	    			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[398]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[399]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[400]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[401]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[402]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[403]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[404]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[405]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[406]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[407]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[408]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[409]);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_03504);
                    	    rightArg=parseop_Expression_level_04();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_25_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_25_0_0_4, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Exp.g:1234:3: () a0= 'and' rightArg= parseop_Expression_level_04
                    	    {
                    	    // Exp.g:1234:3: ()
                    	    // Exp.g:1234:4: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,34,FOLLOW_34_in_parseop_Expression_level_03538); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_26_0_0_2, null, true);
                    	    			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			// expected elements (follow set)
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[410]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[411]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[412]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[413]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[414]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[415]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[416]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[417]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[418]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[419]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[420]);
                    	    			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[421]);
                    	    		}

                    	    pushFollow(FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_03555);
                    	    rightArg=parseop_Expression_level_04();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (leftArg != null) {
                    	    				if (leftArg != null) {
                    	    					Object value = leftArg;
                    	    					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_26_0_0_0, leftArg, true);
                    	    				copyLocalizationInfos(leftArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) {
                    	    			if (terminateParsing) {
                    	    				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    			}
                    	    			if (element == null) {
                    	    				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
                    	    				startIncompleteElement(element);
                    	    			}
                    	    			if (rightArg != null) {
                    	    				if (rightArg != null) {
                    	    					Object value = rightArg;
                    	    					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    					completedElement(value, true);
                    	    				}
                    	    				collectHiddenTokens(element);
                    	    				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_26_0_0_4, rightArg, true);
                    	    				copyLocalizationInfos(rightArg, element);
                    	    			}
                    	    		}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt5 >= 1 ) break loop5;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:1300:21: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parseop_Expression_level_03_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_03"



    // $ANTLR start "parseop_Expression_level_04"
    // Exp.g:1305:1: parseop_Expression_level_04 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_04() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_04_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Exp.g:1308:9: (leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |) )
            // Exp.g:1309:9: leftArg= parseop_Expression_level_5 ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_04601);
            leftArg=parseop_Expression_level_5();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:1309:37: ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ |)
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==38) ) {
                int LA8_1 = input.LA(2);

                if ( (synpred9_Exp()) ) {
                    alt8=1;
                }
                else if ( (true) ) {
                    alt8=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA8_0==EOF||LA8_0==14||(LA8_0 >= 16 && LA8_0 <= 24)||(LA8_0 >= 26 && LA8_0 <= 34)||LA8_0==40||LA8_0==45) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // Exp.g:1309:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
                    {
                    // Exp.g:1309:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==38) ) {
                            int LA7_2 = input.LA(2);

                            if ( (synpred8_Exp()) ) {
                                alt7=1;
                            }


                        }


                        switch (alt7) {
                    	case 1 :
                    	    // Exp.g:1310:2: () a0= 'implies' rightArg= parseop_Expression_level_5
                    	    {
                    	    // Exp.g:1310:2: ()
                    	    // Exp.g:1310:3: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,38,FOLLOW_38_in_parseop_Expression_level_04617); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_27_0_0_2, null, true);
                    	    		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		// expected elements (follow set)
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[422]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[423]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[424]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[425]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[426]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[427]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[428]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[429]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[430]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[431]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[432]);
                    	    		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[433]);
                    	    	}

                    	    pushFollow(FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_04631);
                    	    rightArg=parseop_Expression_level_5();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (leftArg != null) {
                    	    			if (leftArg != null) {
                    	    				Object value = leftArg;
                    	    				element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_27_0_0_0, leftArg, true);
                    	    			copyLocalizationInfos(leftArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) {
                    	    		if (terminateParsing) {
                    	    			throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    		}
                    	    		if (element == null) {
                    	    			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
                    	    			startIncompleteElement(element);
                    	    		}
                    	    		if (rightArg != null) {
                    	    			if (rightArg != null) {
                    	    				Object value = rightArg;
                    	    				addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    				completedElement(value, true);
                    	    			}
                    	    			collectHiddenTokens(element);
                    	    			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_27_0_0_4, rightArg, true);
                    	    			copyLocalizationInfos(rightArg, element);
                    	    		}
                    	    	}

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:1376:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parseop_Expression_level_04_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_04"



    // $ANTLR start "parseop_Expression_level_5"
    // Exp.g:1381:1: parseop_Expression_level_5 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'f' arg= parseop_Expression_level_6 |a0= '!' arg= parseop_Expression_level_6 |arg= parseop_Expression_level_6 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_5() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_5_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Exp.g:1384:0: (a0= 'f' arg= parseop_Expression_level_6 |a0= '!' arg= parseop_Expression_level_6 |arg= parseop_Expression_level_6 )
            int alt9=3;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt9=1;
                }
                break;
            case 13:
                {
                alt9=2;
                }
                break;
            case ILT:
            case QUOTED_34_34:
            case REAL_LITERAL:
            case TEXT:
            case 15:
            case 35:
            case 37:
            case 39:
            case 41:
            case 42:
            case 43:
                {
                alt9=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }

            switch (alt9) {
                case 1 :
                    // Exp.g:1385:0: a0= 'f' arg= parseop_Expression_level_6
                    {
                    a0=(Token)match(input,36,FOLLOW_36_in_parseop_Expression_level_5672); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_11_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[434]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[435]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[436]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[437]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[438]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[439]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[440]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[441]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[442]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[443]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[444]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[445]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_5683);
                    arg=parseop_Expression_level_6();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    }
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
                    	startIncompleteElement(element);
                    }
                    if (arg != null) {
                    	if (arg != null) {
                    		Object value = arg;
                    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP), value);
                    		completedElement(value, true);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_11_0_0_2, arg, true);
                    	copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Exp.g:1430:0: a0= '!' arg= parseop_Expression_level_6
                    {
                    a0=(Token)match(input,13,FOLLOW_13_in_parseop_Expression_level_5692); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
                    	startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_28_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[446]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[447]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[448]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[449]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[450]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[451]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[452]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[453]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[454]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[455]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[456]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[457]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_5703);
                    arg=parseop_Expression_level_6();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    }
                    if (element == null) {
                    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
                    	startIncompleteElement(element);
                    }
                    if (arg != null) {
                    	if (arg != null) {
                    		Object value = arg;
                    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    		completedElement(value, true);
                    	}
                    	collectHiddenTokens(element);
                    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_28_0_0_2, arg, true);
                    	copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Exp.g:1476:5: arg= parseop_Expression_level_6
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_5713);
                    arg=parseop_Expression_level_6();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parseop_Expression_level_5_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_5"



    // $ANTLR start "parseop_Expression_level_6"
    // Exp.g:1479:1: parseop_Expression_level_6 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_11 ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_6() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_6_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Exp.g:1482:9: (leftArg= parseop_Expression_level_11 ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |) )
            // Exp.g:1483:9: leftArg= parseop_Expression_level_11 ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_6735);
            leftArg=parseop_Expression_level_11();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:1483:38: ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ |)
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==29) ) {
                int LA11_1 = input.LA(2);

                if ( (synpred13_Exp()) ) {
                    alt11=1;
                }
                else if ( (true) ) {
                    alt11=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA11_0==EOF||LA11_0==14||(LA11_0 >= 16 && LA11_0 <= 24)||(LA11_0 >= 26 && LA11_0 <= 28)||(LA11_0 >= 30 && LA11_0 <= 34)||LA11_0==38||LA11_0==40||LA11_0==45) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // Exp.g:1483:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
                    {
                    // Exp.g:1483:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
                    int cnt10=0;
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==29) ) {
                            int LA10_2 = input.LA(2);

                            if ( (synpred12_Exp()) ) {
                                alt10=1;
                            }


                        }


                        switch (alt10) {
                    	case 1 :
                    	    // Exp.g:1484:0: () a0= '=' rightArg= parseop_Expression_level_11
                    	    {
                    	    // Exp.g:1484:2: ()
                    	    // Exp.g:1484:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,29,FOLLOW_29_in_parseop_Expression_level_6748); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_32_0_0_2, null, true);
                    	    	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	// expected elements (follow set)
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[458]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[459]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[460]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[461]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[462]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[463]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[464]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[465]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[466]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[467]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[468]);
                    	    	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[469]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_6759);
                    	    rightArg=parseop_Expression_level_11();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (leftArg != null) {
                    	    		if (leftArg != null) {
                    	    			Object value = leftArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_32_0_0_0, leftArg, true);
                    	    		copyLocalizationInfos(leftArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    	if (terminateParsing) {
                    	    		throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    	}
                    	    	if (element == null) {
                    	    		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
                    	    		startIncompleteElement(element);
                    	    	}
                    	    	if (rightArg != null) {
                    	    		if (rightArg != null) {
                    	    			Object value = rightArg;
                    	    			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION), value);
                    	    			completedElement(value, true);
                    	    		}
                    	    		collectHiddenTokens(element);
                    	    		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_32_0_0_4, rightArg, true);
                    	    		copyLocalizationInfos(rightArg, element);
                    	    	}
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt10 >= 1 ) break loop10;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(10, input);
                                throw eee;
                        }
                        cnt10++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:1550:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parseop_Expression_level_6_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_6"



    // $ANTLR start "parseop_Expression_level_11"
    // Exp.g:1555:1: parseop_Expression_level_11 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_11() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_11_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Exp.g:1558:9: (leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |) )
            // Exp.g:1559:9: leftArg= parseop_Expression_level_12 ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11797);
            leftArg=parseop_Expression_level_12();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:1559:38: ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ |)
            int alt13=2;
            switch ( input.LA(1) ) {
            case 31:
                {
                int LA13_1 = input.LA(2);

                if ( (synpred18_Exp()) ) {
                    alt13=1;
                }
                else if ( (true) ) {
                    alt13=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 1, input);

                    throw nvae;

                }
                }
                break;
            case 32:
                {
                int LA13_2 = input.LA(2);

                if ( (synpred18_Exp()) ) {
                    alt13=1;
                }
                else if ( (true) ) {
                    alt13=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 2, input);

                    throw nvae;

                }
                }
                break;
            case 27:
                {
                int LA13_3 = input.LA(2);

                if ( (synpred18_Exp()) ) {
                    alt13=1;
                }
                else if ( (true) ) {
                    alt13=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 3, input);

                    throw nvae;

                }
                }
                break;
            case 28:
                {
                int LA13_4 = input.LA(2);

                if ( (synpred18_Exp()) ) {
                    alt13=1;
                }
                else if ( (true) ) {
                    alt13=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 4, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 26:
            case 29:
            case 30:
            case 33:
            case 34:
            case 38:
            case 40:
            case 45:
                {
                alt13=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }

            switch (alt13) {
                case 1 :
                    // Exp.g:1559:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
                    {
                    // Exp.g:1559:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
                    int cnt12=0;
                    loop12:
                    do {
                        int alt12=5;
                        switch ( input.LA(1) ) {
                        case 31:
                            {
                            int LA12_2 = input.LA(2);

                            if ( (synpred14_Exp()) ) {
                                alt12=1;
                            }


                            }
                            break;
                        case 32:
                            {
                            int LA12_3 = input.LA(2);

                            if ( (synpred15_Exp()) ) {
                                alt12=2;
                            }


                            }
                            break;
                        case 27:
                            {
                            int LA12_4 = input.LA(2);

                            if ( (synpred16_Exp()) ) {
                                alt12=3;
                            }


                            }
                            break;
                        case 28:
                            {
                            int LA12_5 = input.LA(2);

                            if ( (synpred17_Exp()) ) {
                                alt12=4;
                            }


                            }
                            break;

                        }

                        switch (alt12) {
                    	case 1 :
                    	    // Exp.g:1560:0: () a0= '>' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Exp.g:1560:2: ()
                    	    // Exp.g:1560:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,31,FOLLOW_31_in_parseop_Expression_level_11810); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_19_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[470]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[471]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[472]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[473]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[474]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[475]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[476]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[477]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[478]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[479]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[480]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[481]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11821);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_19_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_19_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Exp.g:1627:0: () a0= '>=' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Exp.g:1627:2: ()
                    	    // Exp.g:1627:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,32,FOLLOW_32_in_parseop_Expression_level_11839); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_20_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[482]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[483]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[484]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[485]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[486]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[487]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[488]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[489]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[490]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[491]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[492]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[493]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11850);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_20_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_20_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 3 :
                    	    // Exp.g:1694:0: () a0= '<' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Exp.g:1694:2: ()
                    	    // Exp.g:1694:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,27,FOLLOW_27_in_parseop_Expression_level_11868); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_21_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[494]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[495]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[496]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[497]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[498]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[499]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[500]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[501]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[502]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[503]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[504]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[505]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11879);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_21_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_21_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 4 :
                    	    // Exp.g:1761:0: () a0= '<=' rightArg= parseop_Expression_level_12
                    	    {
                    	    // Exp.g:1761:2: ()
                    	    // Exp.g:1761:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,28,FOLLOW_28_in_parseop_Expression_level_11897); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_22_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[506]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[507]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[508]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[509]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[510]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[511]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[512]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[513]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[514]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[515]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[516]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[517]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11908);
                    	    rightArg=parseop_Expression_level_12();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    	if (leftArg != null) {
                    	    		Object value = leftArg;
                    	    		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_22_0_0_0, leftArg, true);
                    	    	copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
                    	    	startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    	if (rightArg != null) {
                    	    		Object value = rightArg;
                    	    		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    		completedElement(value, true);
                    	    	}
                    	    	collectHiddenTokens(element);
                    	    	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_22_0_0_4, rightArg, true);
                    	    	copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt12 >= 1 ) break loop12;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(12, input);
                                throw eee;
                        }
                        cnt12++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:1827:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parseop_Expression_level_11_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_11"



    // $ANTLR start "parseop_Expression_level_12"
    // Exp.g:1832:1: parseop_Expression_level_12 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_12() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_12_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Exp.g:1835:9: (leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |) )
            // Exp.g:1836:9: leftArg= parseop_Expression_level_19 ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_12946);
            leftArg=parseop_Expression_level_19();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:1836:38: ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ |)
            int alt15=2;
            switch ( input.LA(1) ) {
            case 30:
                {
                int LA15_1 = input.LA(2);

                if ( (synpred21_Exp()) ) {
                    alt15=1;
                }
                else if ( (true) ) {
                    alt15=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 15, 1, input);

                    throw nvae;

                }
                }
                break;
            case 14:
                {
                int LA15_2 = input.LA(2);

                if ( (synpred21_Exp()) ) {
                    alt15=1;
                }
                else if ( (true) ) {
                    alt15=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 15, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 26:
            case 27:
            case 28:
            case 29:
            case 31:
            case 32:
            case 33:
            case 34:
            case 38:
            case 40:
            case 45:
                {
                alt15=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;

            }

            switch (alt15) {
                case 1 :
                    // Exp.g:1836:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
                    {
                    // Exp.g:1836:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
                    int cnt14=0;
                    loop14:
                    do {
                        int alt14=3;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==30) ) {
                            int LA14_2 = input.LA(2);

                            if ( (synpred19_Exp()) ) {
                                alt14=1;
                            }


                        }
                        else if ( (LA14_0==14) ) {
                            int LA14_3 = input.LA(2);

                            if ( (synpred20_Exp()) ) {
                                alt14=2;
                            }


                        }


                        switch (alt14) {
                    	case 1 :
                    	    // Exp.g:1837:0: () a0= '==' rightArg= parseop_Expression_level_19
                    	    {
                    	    // Exp.g:1837:2: ()
                    	    // Exp.g:1837:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,30,FOLLOW_30_in_parseop_Expression_level_12959); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_23_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[518]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[519]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[520]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[521]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[522]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[523]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[524]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[525]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[526]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[527]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[528]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[529]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_12970);
                    	    rightArg=parseop_Expression_level_19();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    	Object value = leftArg;
                    	    	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_23_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    	Object value = rightArg;
                    	    	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_23_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Exp.g:1904:0: () a0= '!=' rightArg= parseop_Expression_level_19
                    	    {
                    	    // Exp.g:1904:2: ()
                    	    // Exp.g:1904:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,14,FOLLOW_14_in_parseop_Expression_level_12988); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_24_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[530]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[531]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[532]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[533]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[534]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[535]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[536]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[537]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[538]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[539]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[540]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[541]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_12999);
                    	    rightArg=parseop_Expression_level_19();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    	Object value = leftArg;
                    	    	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_24_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    	Object value = rightArg;
                    	    	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    	completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_24_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt14 >= 1 ) break loop14;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(14, input);
                                throw eee;
                        }
                        cnt14++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:1970:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parseop_Expression_level_12_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_12"



    // $ANTLR start "parseop_Expression_level_19"
    // Exp.g:1975:1: parseop_Expression_level_19 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_19() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_19_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Exp.g:1978:9: (leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |) )
            // Exp.g:1979:9: leftArg= parseop_Expression_level_21 ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_191037);
            leftArg=parseop_Expression_level_21();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:1979:38: ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ |)
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==33) ) {
                int LA17_1 = input.LA(2);

                if ( (synpred23_Exp()) ) {
                    alt17=1;
                }
                else if ( (true) ) {
                    alt17=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 17, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA17_0==EOF||LA17_0==14||(LA17_0 >= 16 && LA17_0 <= 24)||(LA17_0 >= 26 && LA17_0 <= 32)||LA17_0==34||LA17_0==38||LA17_0==40||LA17_0==45) ) {
                alt17=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;

            }
            switch (alt17) {
                case 1 :
                    // Exp.g:1979:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
                    {
                    // Exp.g:1979:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
                    int cnt16=0;
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==33) ) {
                            int LA16_2 = input.LA(2);

                            if ( (synpred22_Exp()) ) {
                                alt16=1;
                            }


                        }


                        switch (alt16) {
                    	case 1 :
                    	    // Exp.g:1980:0: () a0= '^' rightArg= parseop_Expression_level_21
                    	    {
                    	    // Exp.g:1980:2: ()
                    	    // Exp.g:1980:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,33,FOLLOW_33_in_parseop_Expression_level_191050); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_18_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[542]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[543]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[544]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[545]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[546]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[547]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[548]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[549]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[550]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[551]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[552]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[553]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_191061);
                    	    rightArg=parseop_Expression_level_21();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_18_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_18_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt16 >= 1 ) break loop16;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(16, input);
                                throw eee;
                        }
                        cnt16++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:2046:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parseop_Expression_level_19_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_19"



    // $ANTLR start "parseop_Expression_level_21"
    // Exp.g:2051:1: parseop_Expression_level_21 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_21() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_21_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Exp.g:2054:9: (leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |) )
            // Exp.g:2055:9: leftArg= parseop_Expression_level_22 ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_211099);
            leftArg=parseop_Expression_level_22();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:2055:38: ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ |)
            int alt19=2;
            switch ( input.LA(1) ) {
            case 18:
                {
                int LA19_1 = input.LA(2);

                if ( (synpred26_Exp()) ) {
                    alt19=1;
                }
                else if ( (true) ) {
                    alt19=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 1, input);

                    throw nvae;

                }
                }
                break;
            case 21:
                {
                int LA19_2 = input.LA(2);

                if ( (synpred26_Exp()) ) {
                    alt19=1;
                }
                else if ( (true) ) {
                    alt19=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case 14:
            case 16:
            case 17:
            case 19:
            case 20:
            case 22:
            case 23:
            case 24:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 38:
            case 40:
            case 45:
                {
                alt19=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;

            }

            switch (alt19) {
                case 1 :
                    // Exp.g:2055:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
                    {
                    // Exp.g:2055:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
                    int cnt18=0;
                    loop18:
                    do {
                        int alt18=3;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==18) ) {
                            int LA18_2 = input.LA(2);

                            if ( (synpred24_Exp()) ) {
                                alt18=1;
                            }


                        }
                        else if ( (LA18_0==21) ) {
                            int LA18_3 = input.LA(2);

                            if ( (synpred25_Exp()) ) {
                                alt18=2;
                            }


                        }


                        switch (alt18) {
                    	case 1 :
                    	    // Exp.g:2056:0: () a0= '+' rightArg= parseop_Expression_level_22
                    	    {
                    	    // Exp.g:2056:2: ()
                    	    // Exp.g:2056:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,18,FOLLOW_18_in_parseop_Expression_level_211112); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_12_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[554]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[555]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[556]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[557]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[558]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[559]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[560]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[561]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[562]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[563]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[564]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[565]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_211123);
                    	    rightArg=parseop_Expression_level_22();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_12_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_12_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Exp.g:2123:0: () a0= '-' rightArg= parseop_Expression_level_22
                    	    {
                    	    // Exp.g:2123:2: ()
                    	    // Exp.g:2123:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,21,FOLLOW_21_in_parseop_Expression_level_211141); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_13_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[566]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[567]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[568]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[569]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[570]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[571]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[572]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[573]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[574]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[575]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[576]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[577]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_211152);
                    	    rightArg=parseop_Expression_level_22();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_13_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_13_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt18 >= 1 ) break loop18;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(18, input);
                                throw eee;
                        }
                        cnt18++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:2189:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parseop_Expression_level_21_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_21"



    // $ANTLR start "parseop_Expression_level_22"
    // Exp.g:2194:1: parseop_Expression_level_22 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_22() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_22_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Exp.g:2197:9: (leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |) )
            // Exp.g:2198:9: leftArg= parseop_Expression_level_80 ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_221190);
            leftArg=parseop_Expression_level_80();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:2198:38: ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ |)
            int alt21=2;
            switch ( input.LA(1) ) {
            case 17:
                {
                int LA21_1 = input.LA(2);

                if ( (synpred29_Exp()) ) {
                    alt21=1;
                }
                else if ( (true) ) {
                    alt21=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 21, 1, input);

                    throw nvae;

                }
                }
                break;
            case 24:
                {
                int LA21_2 = input.LA(2);

                if ( (synpred29_Exp()) ) {
                    alt21=1;
                }
                else if ( (true) ) {
                    alt21=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 21, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case 14:
            case 16:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 38:
            case 40:
            case 45:
                {
                alt21=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;

            }

            switch (alt21) {
                case 1 :
                    // Exp.g:2198:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
                    {
                    // Exp.g:2198:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
                    int cnt20=0;
                    loop20:
                    do {
                        int alt20=3;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==17) ) {
                            int LA20_2 = input.LA(2);

                            if ( (synpred27_Exp()) ) {
                                alt20=1;
                            }


                        }
                        else if ( (LA20_0==24) ) {
                            int LA20_3 = input.LA(2);

                            if ( (synpred28_Exp()) ) {
                                alt20=2;
                            }


                        }


                        switch (alt20) {
                    	case 1 :
                    	    // Exp.g:2199:0: () a0= '*' rightArg= parseop_Expression_level_80
                    	    {
                    	    // Exp.g:2199:2: ()
                    	    // Exp.g:2199:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,17,FOLLOW_17_in_parseop_Expression_level_221203); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_16_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[578]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[579]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[580]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[581]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[582]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[583]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[584]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[585]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[586]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[587]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[588]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[589]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_221214);
                    	    rightArg=parseop_Expression_level_80();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_16_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_16_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Exp.g:2266:0: () a0= '/' rightArg= parseop_Expression_level_80
                    	    {
                    	    // Exp.g:2266:2: ()
                    	    // Exp.g:2266:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,24,FOLLOW_24_in_parseop_Expression_level_221232); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_17_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[590]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[591]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[592]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[593]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[594]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[595]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[596]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[597]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[598]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[599]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[600]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[601]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_221243);
                    	    rightArg=parseop_Expression_level_80();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_17_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_17_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt20 >= 1 ) break loop20;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(20, input);
                                throw eee;
                        }
                        cnt20++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:2332:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parseop_Expression_level_22_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_22"



    // $ANTLR start "parseop_Expression_level_80"
    // Exp.g:2337:1: parseop_Expression_level_80 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'sin' arg= parseop_Expression_level_87 |a0= 'cos' arg= parseop_Expression_level_87 |arg= parseop_Expression_level_87 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_80() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_80_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Exp.g:2340:0: (a0= 'sin' arg= parseop_Expression_level_87 |a0= 'cos' arg= parseop_Expression_level_87 |arg= parseop_Expression_level_87 )
            int alt22=3;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt22=1;
                }
                break;
            case 35:
                {
                alt22=2;
                }
                break;
            case ILT:
            case QUOTED_34_34:
            case REAL_LITERAL:
            case TEXT:
            case 15:
            case 37:
            case 39:
            case 42:
            case 43:
                {
                alt22=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;

            }

            switch (alt22) {
                case 1 :
                    // Exp.g:2341:0: a0= 'sin' arg= parseop_Expression_level_87
                    {
                    a0=(Token)match(input,41,FOLLOW_41_in_parseop_Expression_level_801281); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_9_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[602]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[603]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[604]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[605]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[606]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[607]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[608]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[609]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[610]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[611]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[612]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[613]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_801292);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_9_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Exp.g:2386:0: a0= 'cos' arg= parseop_Expression_level_87
                    {
                    a0=(Token)match(input,35,FOLLOW_35_in_parseop_Expression_level_801301); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_10_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[614]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[615]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[616]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[617]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[618]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[619]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[620]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[621]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[622]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[623]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[624]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[625]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_801312);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_10_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Exp.g:2432:5: arg= parseop_Expression_level_87
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_801322);
                    arg=parseop_Expression_level_87();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parseop_Expression_level_80_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_80"



    // $ANTLR start "parseop_Expression_level_87"
    // Exp.g:2435:1: parseop_Expression_level_87 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (a0= 'not' arg= parseop_Expression_level_88 |arg= parseop_Expression_level_88 );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_87() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_87_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Exp.g:2438:0: (a0= 'not' arg= parseop_Expression_level_88 |arg= parseop_Expression_level_88 )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==39) ) {
                alt23=1;
            }
            else if ( (LA23_0==ILT||(LA23_0 >= QUOTED_34_34 && LA23_0 <= REAL_LITERAL)||LA23_0==TEXT||LA23_0==15||LA23_0==37||(LA23_0 >= 42 && LA23_0 <= 43)) ) {
                alt23=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;

            }
            switch (alt23) {
                case 1 :
                    // Exp.g:2439:0: a0= 'not' arg= parseop_Expression_level_88
                    {
                    a0=(Token)match(input,39,FOLLOW_39_in_parseop_Expression_level_871344); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_8_0_0_0, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[626]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[627]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[628]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[629]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[630]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[631]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[632]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[633]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[634]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[635]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[636]);
                    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[637]);
                    }

                    pushFollow(FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_871355);
                    arg=parseop_Expression_level_88();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_8_0_0_2, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Exp.g:2485:5: arg= parseop_Expression_level_88
                    {
                    pushFollow(FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_871365);
                    arg=parseop_Expression_level_88();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parseop_Expression_level_87_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_87"



    // $ANTLR start "parseop_Expression_level_88"
    // Exp.g:2488:1: parseop_Expression_level_88 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_88() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_88_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression arg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return element; }

            // Exp.g:2491:5: (arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |) )
            // Exp.g:2492:5: arg= parseop_Expression_level_90 (a0= '++' |a0= '--' |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_90_in_parseop_Expression_level_881387);
            arg=parseop_Expression_level_90();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:2492:34: (a0= '++' |a0= '--' |)
            int alt24=3;
            switch ( input.LA(1) ) {
            case 19:
                {
                int LA24_1 = input.LA(2);

                if ( (synpred33_Exp()) ) {
                    alt24=1;
                }
                else if ( (true) ) {
                    alt24=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 24, 1, input);

                    throw nvae;

                }
                }
                break;
            case 22:
                {
                int LA24_2 = input.LA(2);

                if ( (synpred34_Exp()) ) {
                    alt24=2;
                }
                else if ( (true) ) {
                    alt24=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 24, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case 14:
            case 16:
            case 17:
            case 18:
            case 20:
            case 21:
            case 23:
            case 24:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 38:
            case 40:
            case 45:
                {
                alt24=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;

            }

            switch (alt24) {
                case 1 :
                    // Exp.g:2493:0: a0= '++'
                    {
                    a0=(Token)match(input,19,FOLLOW_19_in_parseop_Expression_level_881394); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_6_0_0_2, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[638]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[639]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[640]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[641]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[642]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[643]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[644]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[645]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[646]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[647]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[648]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[649]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[650]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[651]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[652]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[653]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[654]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[655]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[656]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[657]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[658]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[659]);
                    }

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_6_0_0_0, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 2 :
                    // Exp.g:2548:0: a0= '--'
                    {
                    a0=(Token)match(input,22,FOLLOW_22_in_parseop_Expression_level_881409); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_7_0_0_2, null, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    }

                    if ( state.backtracking==0 ) {
                    // expected elements (follow set)
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[660]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[661]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[662]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[663]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[664]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[665]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[666]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[667]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[668]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[669]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[670]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[671]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[672]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[673]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[674]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[675]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[676]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[677]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[678]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[679]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[680]);
                    addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[681]);
                    }

                    if ( state.backtracking==0 ) {
                    if (terminateParsing) {
                    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    }
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
                    startIncompleteElement(element);
                    }
                    if (arg != null) {
                    if (arg != null) {
                    Object value = arg;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
                    completedElement(value, true);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_7_0_0_0, arg, true);
                    copyLocalizationInfos(arg, element);
                    }
                    }

                    }
                    break;
                case 3 :
                    // Exp.g:2603:15: 
                    {
                    if ( state.backtracking==0 ) { element = arg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 15, parseop_Expression_level_88_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_88"



    // $ANTLR start "parseop_Expression_level_90"
    // Exp.g:2607:1: parseop_Expression_level_90 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |) ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_90() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_90_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression leftArg =null;

        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return element; }

            // Exp.g:2610:9: (leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |) )
            // Exp.g:2611:9: leftArg= parseop_Expression_level_91 ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |)
            {
            pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_901446);
            leftArg=parseop_Expression_level_91();

            state._fsp--;
            if (state.failed) return element;

            // Exp.g:2611:38: ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ |)
            int alt26=2;
            switch ( input.LA(1) ) {
            case 20:
                {
                int LA26_1 = input.LA(2);

                if ( (synpred37_Exp()) ) {
                    alt26=1;
                }
                else if ( (true) ) {
                    alt26=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 26, 1, input);

                    throw nvae;

                }
                }
                break;
            case 23:
                {
                int LA26_2 = input.LA(2);

                if ( (synpred37_Exp()) ) {
                    alt26=1;
                }
                else if ( (true) ) {
                    alt26=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 26, 2, input);

                    throw nvae;

                }
                }
                break;
            case EOF:
            case 14:
            case 16:
            case 17:
            case 18:
            case 19:
            case 21:
            case 22:
            case 24:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 38:
            case 40:
            case 45:
                {
                alt26=2;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;

            }

            switch (alt26) {
                case 1 :
                    // Exp.g:2611:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
                    {
                    // Exp.g:2611:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
                    int cnt25=0;
                    loop25:
                    do {
                        int alt25=3;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==20) ) {
                            int LA25_2 = input.LA(2);

                            if ( (synpred35_Exp()) ) {
                                alt25=1;
                            }


                        }
                        else if ( (LA25_0==23) ) {
                            int LA25_3 = input.LA(2);

                            if ( (synpred36_Exp()) ) {
                                alt25=2;
                            }


                        }


                        switch (alt25) {
                    	case 1 :
                    	    // Exp.g:2612:0: () a0= '+=' rightArg= parseop_Expression_level_91
                    	    {
                    	    // Exp.g:2612:2: ()
                    	    // Exp.g:2612:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,20,FOLLOW_20_in_parseop_Expression_level_901459); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_14_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[682]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[683]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[684]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[685]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[686]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[687]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[688]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[689]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[690]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[691]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[692]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[693]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_901470);
                    	    rightArg=parseop_Expression_level_91();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_14_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_14_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;
                    	case 2 :
                    	    // Exp.g:2679:0: () a0= '-=' rightArg= parseop_Expression_level_91
                    	    {
                    	    // Exp.g:2679:2: ()
                    	    // Exp.g:2679:2: 
                    	    {
                    	    }


                    	    if ( state.backtracking==0 ) { element = null; }

                    	    a0=(Token)match(input,23,FOLLOW_23_in_parseop_Expression_level_901488); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_15_0_0_2, null, true);
                    	    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    // expected elements (follow set)
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[694]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[695]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[696]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[697]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[698]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[699]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[700]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[701]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[702]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[703]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[704]);
                    	    addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[705]);
                    	    }

                    	    pushFollow(FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_901499);
                    	    rightArg=parseop_Expression_level_91();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (leftArg != null) {
                    	    if (leftArg != null) {
                    	    Object value = leftArg;
                    	    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_15_0_0_0, leftArg, true);
                    	    copyLocalizationInfos(leftArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) {
                    	    if (terminateParsing) {
                    	    throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
                    	    }
                    	    if (element == null) {
                    	    element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
                    	    startIncompleteElement(element);
                    	    }
                    	    if (rightArg != null) {
                    	    if (rightArg != null) {
                    	    Object value = rightArg;
                    	    addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
                    	    completedElement(value, true);
                    	    }
                    	    collectHiddenTokens(element);
                    	    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_15_0_0_4, rightArg, true);
                    	    copyLocalizationInfos(rightArg, element);
                    	    }
                    	    }

                    	    if ( state.backtracking==0 ) { leftArg = element; /* this may become an argument in the next iteration */ }

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt25 >= 1 ) break loop25;
                    	    if (state.backtracking>0) {state.failed=true; return element;}
                                EarlyExitException eee =
                                    new EarlyExitException(25, input);
                                throw eee;
                        }
                        cnt25++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // Exp.g:2745:20: 
                    {
                    if ( state.backtracking==0 ) { element = leftArg; }

                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 16, parseop_Expression_level_90_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_90"



    // $ANTLR start "parseop_Expression_level_91"
    // Exp.g:2750:1: parseop_Expression_level_91 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : (c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression |c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression |c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression |c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression |c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression |c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration |c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression );
    public final org.coolsoftware.coolcomponents.expressions.Expression parseop_Expression_level_91() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parseop_Expression_level_91_StartIndex = input.index();

        org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression c0 =null;

        org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression c1 =null;

        org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression c2 =null;

        org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression c3 =null;

        org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression c4 =null;

        org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration c5 =null;

        org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression c6 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return element; }

            // Exp.g:2753:0: (c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression |c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression |c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression |c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression |c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression |c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration |c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression )
            int alt27=7;
            switch ( input.LA(1) ) {
            case ILT:
                {
                alt27=1;
                }
                break;
            case REAL_LITERAL:
                {
                alt27=2;
                }
                break;
            case 37:
            case 42:
                {
                alt27=3;
                }
                break;
            case QUOTED_34_34:
                {
                alt27=4;
                }
                break;
            case 15:
                {
                alt27=5;
                }
                break;
            case 43:
                {
                alt27=6;
                }
                break;
            case TEXT:
                {
                alt27=7;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;

            }

            switch (alt27) {
                case 1 :
                    // Exp.g:2754:0: c0= parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_in_parseop_Expression_level_911537);
                    c0=parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Exp.g:2755:2: c1= parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_in_parseop_Expression_level_911545);
                    c1=parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Exp.g:2756:2: c2= parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_in_parseop_Expression_level_911553);
                    c2=parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Exp.g:2757:2: c3= parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_in_parseop_Expression_level_911561);
                    c3=parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Exp.g:2758:2: c4= parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_in_parseop_Expression_level_911569);
                    c4=parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 6 :
                    // Exp.g:2759:2: c5= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_in_parseop_Expression_level_911577);
                    c5=parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c5; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 7 :
                    // Exp.g:2760:2: c6= parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression
                    {
                    pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_in_parseop_Expression_level_911585);
                    c6=parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c6; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 17, parseop_Expression_level_91_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parseop_Expression_level_91"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression"
    // Exp.g:2763:1: parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element = null] : (a0= ILT ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return element; }

            // Exp.g:2766:4: ( (a0= ILT ) )
            // Exp.g:2767:4: (a0= ILT )
            {
            // Exp.g:2767:4: (a0= ILT )
            // Exp.g:2768:4: a0= ILT
            {
            a0=(Token)match(input,ILT,FOLLOW_ILT_in_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression1609); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createIntegerLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("ILT");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.Long resolved = (java.lang.Long) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_1_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[706]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[707]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[708]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[709]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[710]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[711]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[712]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[713]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[714]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[715]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[716]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[717]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[718]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[719]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[720]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[721]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[722]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[723]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[724]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[725]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[726]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[727]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 18, parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression"
    // Exp.g:2826:1: parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element = null] : (a0= REAL_LITERAL ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return element; }

            // Exp.g:2829:4: ( (a0= REAL_LITERAL ) )
            // Exp.g:2830:4: (a0= REAL_LITERAL )
            {
            // Exp.g:2830:4: (a0= REAL_LITERAL )
            // Exp.g:2831:4: a0= REAL_LITERAL
            {
            a0=(Token)match(input,REAL_LITERAL,FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression1639); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createRealLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.Double resolved = (java.lang.Double) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_2_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[728]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[729]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[730]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[731]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[732]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[733]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[734]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[735]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[736]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[737]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[738]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[739]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[740]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[741]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[742]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[743]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[744]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[745]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[746]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[747]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[748]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[749]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 19, parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression"
    // Exp.g:2889:1: parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element = null] : ( (a0= 'true' |a1= 'false' ) ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return element; }

            // Exp.g:2892:0: ( ( (a0= 'true' |a1= 'false' ) ) )
            // Exp.g:2893:0: ( (a0= 'true' |a1= 'false' ) )
            {
            // Exp.g:2893:0: ( (a0= 'true' |a1= 'false' ) )
            // Exp.g:2894:0: (a0= 'true' |a1= 'false' )
            {
            // Exp.g:2894:0: (a0= 'true' |a1= 'false' )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==42) ) {
                alt28=1;
            }
            else if ( (LA28_0==37) ) {
                alt28=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;

            }
            switch (alt28) {
                case 1 :
                    // Exp.g:2895:0: a0= 'true'
                    {
                    a0=(Token)match(input,42,FOLLOW_42_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression1671); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_3_0_0_0, true, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
                    // set value of boolean attribute
                    Object value = true;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;
                case 2 :
                    // Exp.g:2908:2: a1= 'false'
                    {
                    a1=(Token)match(input,37,FOLLOW_37_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression1680); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    if (element == null) {
                    element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
                    startIncompleteElement(element);
                    }
                    collectHiddenTokens(element);
                    retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_3_0_0_0, false, true);
                    copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    // set value of boolean attribute
                    Object value = false;
                    element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
                    completedElement(value, false);
                    }

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[750]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[751]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[752]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[753]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[754]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[755]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[756]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[757]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[758]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[759]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[760]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[761]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[762]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[763]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[764]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[765]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[766]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[767]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[768]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[769]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[770]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[771]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 20, parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression"
    // Exp.g:2951:1: parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element = null] : (a0= QUOTED_34_34 ) ;
    public final org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return element; }

            // Exp.g:2954:4: ( (a0= QUOTED_34_34 ) )
            // Exp.g:2955:4: (a0= QUOTED_34_34 )
            {
            // Exp.g:2955:4: (a0= QUOTED_34_34 )
            // Exp.g:2956:4: a0= QUOTED_34_34
            {
            a0=(Token)match(input,QUOTED_34_34,FOLLOW_QUOTED_34_34_in_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression1712); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createStringLiteralExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            java.lang.String resolved = (java.lang.String) resolvedObject;
            if (resolved != null) {
            Object value = resolved;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_4_0_0_0, resolved, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[772]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[773]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[774]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[775]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[776]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[777]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[778]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[779]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[780]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[781]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[782]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[783]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[784]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[785]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[786]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[787]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[788]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[789]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[790]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[791]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[792]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[793]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 21, parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression"
    // Exp.g:3014:1: parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression returns [org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element = null] : a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')' ;
    public final org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        org.coolsoftware.coolcomponents.expressions.Expression a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return element; }

            // Exp.g:3017:4: (a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')' )
            // Exp.g:3018:4: a0= '(' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) a2= ')'
            {
            a0=(Token)match(input,15,FOLLOW_15_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression1740); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_5_0_0_0, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[794]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[795]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[796]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[797]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[798]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[799]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[800]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[801]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[802]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[803]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[804]);
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[805]);
            }

            // Exp.g:3043:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
            // Exp.g:3044:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression1753);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            Object value = a1_0;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP), value);
            completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_5_0_0_2, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[806]);
            }

            a2=(Token)match(input,16,FOLLOW_16_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression1765); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_5_0_0_4, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[807]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[808]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[809]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[810]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[811]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[812]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[813]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[814]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[815]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[816]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[817]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[818]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[819]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[820]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[821]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[822]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[823]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[824]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[825]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[826]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[827]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[828]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 22, parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration"
    // Exp.g:3106:1: parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration returns [org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element = null] : a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable ) ;
    public final org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_StartIndex = input.index();

        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.variables.Variable a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return element; }

            // Exp.g:3109:4: (a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable ) )
            // Exp.g:3110:4: a0= 'var' (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable )
            {
            a0=(Token)match(input,43,FOLLOW_43_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration1791); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
            startIncompleteElement(element);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_29_0_0_0, null, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            }

            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableDeclaration(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[829]);
            }

            // Exp.g:3124:6: (a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable )
            // Exp.g:3125:6: a1_0= parse_org_coolsoftware_coolcomponents_expressions_variables_Variable
            {
            pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration1804);
            a1_0=parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
            startIncompleteElement(element);
            }
            if (a1_0 != null) {
            if (a1_0 != null) {
            Object value = a1_0;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE), value);
            completedElement(value, true);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_29_0_0_2, a1_0, true);
            copyLocalizationInfos(a1_0, element);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[830]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[831]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[832]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[833]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[834]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[835]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[836]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[837]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[838]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[839]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[840]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[841]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[842]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[843]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[844]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[845]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[846]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[847]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[848]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[849]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[850]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[851]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 23, parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression"
    // Exp.g:3173:1: parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression returns [org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element = null] : (a0= TEXT ) ;
    public final org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return element; }

            // Exp.g:3176:4: ( (a0= TEXT ) )
            // Exp.g:3177:4: (a0= TEXT )
            {
            // Exp.g:3177:4: (a0= TEXT )
            // Exp.g:3178:4: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression1833); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            if (terminateParsing) {
            throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
            }
            if (element == null) {
            element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableCallExpression();
            startIncompleteElement(element);
            }
            if (a0 != null) {
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            tokenResolver.setOptions(getOptions());
            org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
            tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), result);
            Object resolvedObject = result.getResolvedToken();
            if (resolvedObject == null) {
            addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            }
            String resolved = (String) resolvedObject;
            org.coolsoftware.coolcomponents.expressions.variables.Variable proxy = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
            collectHiddenTokens(element);
            registerContextDependentProxy(new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContextDependentURIFragmentFactory<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableCallExpressionReferredVariableReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), resolved, proxy);
            if (proxy != null) {
            Object value = proxy;
            element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), value);
            completedElement(value, false);
            }
            collectHiddenTokens(element);
            retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_31_0_0_0, proxy, true);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
            }
            }

            }


            if ( state.backtracking==0 ) {
            // expected elements (follow set)
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[852]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[853]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[854]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[855]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[856]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[857]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[858]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[859]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[860]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[861]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[862]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[863]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[864]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[865]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[866]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[867]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[868]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[869]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[870]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[871]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[872]);
            addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[873]);
            }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 24, parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression"



    // $ANTLR start "parse_org_coolsoftware_coolcomponents_expressions_Expression"
    // Exp.g:3240:1: parse_org_coolsoftware_coolcomponents_expressions_Expression returns [org.coolsoftware.coolcomponents.expressions.Expression element = null] : c= parseop_Expression_level_03 ;
    public final org.coolsoftware.coolcomponents.expressions.Expression parse_org_coolsoftware_coolcomponents_expressions_Expression() throws RecognitionException {
        org.coolsoftware.coolcomponents.expressions.Expression element =  null;

        int parse_org_coolsoftware_coolcomponents_expressions_Expression_StartIndex = input.index();

        org.coolsoftware.coolcomponents.expressions.Expression c =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return element; }

            // Exp.g:3241:3: (c= parseop_Expression_level_03 )
            // Exp.g:3242:3: c= parseop_Expression_level_03
            {
            pushFollow(FOLLOW_parseop_Expression_level_03_in_parse_org_coolsoftware_coolcomponents_expressions_Expression1857);
            c=parseop_Expression_level_03();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c; /* this rule is an expression root */ }

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 25, parse_org_coolsoftware_coolcomponents_expressions_Expression_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_coolsoftware_coolcomponents_expressions_Expression"

    // $ANTLR start synpred4_Exp
    public final void synpred4_Exp_fragment() throws RecognitionException {
        Token a3=null;
        org.coolsoftware.coolcomponents.expressions.Expression a4_0 =null;


        // Exp.g:1059:3: ( (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) ) )
        // Exp.g:1059:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
        {
        // Exp.g:1059:3: (a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression ) )
        // Exp.g:1060:4: a3= '=' (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        {
        a3=(Token)match(input,29,FOLLOW_29_in_synpred4_Exp385); if (state.failed) return ;

        // Exp.g:1085:4: (a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression )
        // Exp.g:1086:5: a4_0= parse_org_coolsoftware_coolcomponents_expressions_Expression
        {
        pushFollow(FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred4_Exp411);
        a4_0=parse_org_coolsoftware_coolcomponents_expressions_Expression();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }

    }
    // $ANTLR end synpred4_Exp

    // $ANTLR start synpred5_Exp
    public final void synpred5_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1167:3: ( () a0= 'or' rightArg= parseop_Expression_level_04 )
        // Exp.g:1167:3: () a0= 'or' rightArg= parseop_Expression_level_04
        {
        // Exp.g:1167:3: ()
        // Exp.g:1167:4: 
        {
        }


        a0=(Token)match(input,40,FOLLOW_40_in_synpred5_Exp487); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred5_Exp504);
        rightArg=parseop_Expression_level_04();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred5_Exp

    // $ANTLR start synpred6_Exp
    public final void synpred6_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1234:3: ( () a0= 'and' rightArg= parseop_Expression_level_04 )
        // Exp.g:1234:3: () a0= 'and' rightArg= parseop_Expression_level_04
        {
        // Exp.g:1234:3: ()
        // Exp.g:1234:4: 
        {
        }


        a0=(Token)match(input,34,FOLLOW_34_in_synpred6_Exp538); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred6_Exp555);
        rightArg=parseop_Expression_level_04();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred6_Exp

    // $ANTLR start synpred7_Exp
    public final void synpred7_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1166:41: ( ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+ )
        // Exp.g:1166:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
        {
        // Exp.g:1166:41: ( () a0= 'or' rightArg= parseop_Expression_level_04 | () a0= 'and' rightArg= parseop_Expression_level_04 )+
        int cnt29=0;
        loop29:
        do {
            int alt29=3;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==40) ) {
                alt29=1;
            }
            else if ( (LA29_0==34) ) {
                alt29=2;
            }


            switch (alt29) {
        	case 1 :
        	    // Exp.g:1167:3: () a0= 'or' rightArg= parseop_Expression_level_04
        	    {
        	    // Exp.g:1167:3: ()
        	    // Exp.g:1167:4: 
        	    {
        	    }


        	    a0=(Token)match(input,40,FOLLOW_40_in_synpred7_Exp487); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred7_Exp504);
        	    rightArg=parseop_Expression_level_04();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Exp.g:1234:3: () a0= 'and' rightArg= parseop_Expression_level_04
        	    {
        	    // Exp.g:1234:3: ()
        	    // Exp.g:1234:4: 
        	    {
        	    }


        	    a0=(Token)match(input,34,FOLLOW_34_in_synpred7_Exp538); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_04_in_synpred7_Exp555);
        	    rightArg=parseop_Expression_level_04();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt29 >= 1 ) break loop29;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(29, input);
                    throw eee;
            }
            cnt29++;
        } while (true);


        }

    }
    // $ANTLR end synpred7_Exp

    // $ANTLR start synpred8_Exp
    public final void synpred8_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1310:2: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )
        // Exp.g:1310:2: () a0= 'implies' rightArg= parseop_Expression_level_5
        {
        // Exp.g:1310:2: ()
        // Exp.g:1310:3: 
        {
        }


        a0=(Token)match(input,38,FOLLOW_38_in_synpred8_Exp617); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_5_in_synpred8_Exp631);
        rightArg=parseop_Expression_level_5();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred8_Exp

    // $ANTLR start synpred9_Exp
    public final void synpred9_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1309:38: ( ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+ )
        // Exp.g:1309:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
        {
        // Exp.g:1309:38: ( () a0= 'implies' rightArg= parseop_Expression_level_5 )+
        int cnt30=0;
        loop30:
        do {
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==38) ) {
                alt30=1;
            }


            switch (alt30) {
        	case 1 :
        	    // Exp.g:1310:2: () a0= 'implies' rightArg= parseop_Expression_level_5
        	    {
        	    // Exp.g:1310:2: ()
        	    // Exp.g:1310:3: 
        	    {
        	    }


        	    a0=(Token)match(input,38,FOLLOW_38_in_synpred9_Exp617); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_5_in_synpred9_Exp631);
        	    rightArg=parseop_Expression_level_5();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt30 >= 1 ) break loop30;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(30, input);
                    throw eee;
            }
            cnt30++;
        } while (true);


        }

    }
    // $ANTLR end synpred9_Exp

    // $ANTLR start synpred12_Exp
    public final void synpred12_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1484:2: ( () a0= '=' rightArg= parseop_Expression_level_11 )
        // Exp.g:1484:2: () a0= '=' rightArg= parseop_Expression_level_11
        {
        // Exp.g:1484:2: ()
        // Exp.g:1484:2: 
        {
        }


        a0=(Token)match(input,29,FOLLOW_29_in_synpred12_Exp748); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_11_in_synpred12_Exp759);
        rightArg=parseop_Expression_level_11();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred12_Exp

    // $ANTLR start synpred13_Exp
    public final void synpred13_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1483:39: ( ( () a0= '=' rightArg= parseop_Expression_level_11 )+ )
        // Exp.g:1483:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
        {
        // Exp.g:1483:39: ( () a0= '=' rightArg= parseop_Expression_level_11 )+
        int cnt31=0;
        loop31:
        do {
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==29) ) {
                alt31=1;
            }


            switch (alt31) {
        	case 1 :
        	    // Exp.g:1484:0: () a0= '=' rightArg= parseop_Expression_level_11
        	    {
        	    // Exp.g:1484:2: ()
        	    // Exp.g:1484:2: 
        	    {
        	    }


        	    a0=(Token)match(input,29,FOLLOW_29_in_synpred13_Exp748); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_11_in_synpred13_Exp759);
        	    rightArg=parseop_Expression_level_11();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt31 >= 1 ) break loop31;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(31, input);
                    throw eee;
            }
            cnt31++;
        } while (true);


        }

    }
    // $ANTLR end synpred13_Exp

    // $ANTLR start synpred14_Exp
    public final void synpred14_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1560:2: ( () a0= '>' rightArg= parseop_Expression_level_12 )
        // Exp.g:1560:2: () a0= '>' rightArg= parseop_Expression_level_12
        {
        // Exp.g:1560:2: ()
        // Exp.g:1560:2: 
        {
        }


        a0=(Token)match(input,31,FOLLOW_31_in_synpred14_Exp810); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred14_Exp821);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred14_Exp

    // $ANTLR start synpred15_Exp
    public final void synpred15_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1627:2: ( () a0= '>=' rightArg= parseop_Expression_level_12 )
        // Exp.g:1627:2: () a0= '>=' rightArg= parseop_Expression_level_12
        {
        // Exp.g:1627:2: ()
        // Exp.g:1627:2: 
        {
        }


        a0=(Token)match(input,32,FOLLOW_32_in_synpred15_Exp839); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred15_Exp850);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred15_Exp

    // $ANTLR start synpred16_Exp
    public final void synpred16_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1694:2: ( () a0= '<' rightArg= parseop_Expression_level_12 )
        // Exp.g:1694:2: () a0= '<' rightArg= parseop_Expression_level_12
        {
        // Exp.g:1694:2: ()
        // Exp.g:1694:2: 
        {
        }


        a0=(Token)match(input,27,FOLLOW_27_in_synpred16_Exp868); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred16_Exp879);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred16_Exp

    // $ANTLR start synpred17_Exp
    public final void synpred17_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1761:2: ( () a0= '<=' rightArg= parseop_Expression_level_12 )
        // Exp.g:1761:2: () a0= '<=' rightArg= parseop_Expression_level_12
        {
        // Exp.g:1761:2: ()
        // Exp.g:1761:2: 
        {
        }


        a0=(Token)match(input,28,FOLLOW_28_in_synpred17_Exp897); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred17_Exp908);
        rightArg=parseop_Expression_level_12();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred17_Exp

    // $ANTLR start synpred18_Exp
    public final void synpred18_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1559:39: ( ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+ )
        // Exp.g:1559:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
        {
        // Exp.g:1559:39: ( () a0= '>' rightArg= parseop_Expression_level_12 | () a0= '>=' rightArg= parseop_Expression_level_12 | () a0= '<' rightArg= parseop_Expression_level_12 | () a0= '<=' rightArg= parseop_Expression_level_12 )+
        int cnt32=0;
        loop32:
        do {
            int alt32=5;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt32=1;
                }
                break;
            case 32:
                {
                alt32=2;
                }
                break;
            case 27:
                {
                alt32=3;
                }
                break;
            case 28:
                {
                alt32=4;
                }
                break;

            }

            switch (alt32) {
        	case 1 :
        	    // Exp.g:1560:0: () a0= '>' rightArg= parseop_Expression_level_12
        	    {
        	    // Exp.g:1560:2: ()
        	    // Exp.g:1560:2: 
        	    {
        	    }


        	    a0=(Token)match(input,31,FOLLOW_31_in_synpred18_Exp810); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred18_Exp821);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Exp.g:1627:0: () a0= '>=' rightArg= parseop_Expression_level_12
        	    {
        	    // Exp.g:1627:2: ()
        	    // Exp.g:1627:2: 
        	    {
        	    }


        	    a0=(Token)match(input,32,FOLLOW_32_in_synpred18_Exp839); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred18_Exp850);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 3 :
        	    // Exp.g:1694:0: () a0= '<' rightArg= parseop_Expression_level_12
        	    {
        	    // Exp.g:1694:2: ()
        	    // Exp.g:1694:2: 
        	    {
        	    }


        	    a0=(Token)match(input,27,FOLLOW_27_in_synpred18_Exp868); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred18_Exp879);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 4 :
        	    // Exp.g:1761:0: () a0= '<=' rightArg= parseop_Expression_level_12
        	    {
        	    // Exp.g:1761:2: ()
        	    // Exp.g:1761:2: 
        	    {
        	    }


        	    a0=(Token)match(input,28,FOLLOW_28_in_synpred18_Exp897); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_12_in_synpred18_Exp908);
        	    rightArg=parseop_Expression_level_12();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt32 >= 1 ) break loop32;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(32, input);
                    throw eee;
            }
            cnt32++;
        } while (true);


        }

    }
    // $ANTLR end synpred18_Exp

    // $ANTLR start synpred19_Exp
    public final void synpred19_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1837:2: ( () a0= '==' rightArg= parseop_Expression_level_19 )
        // Exp.g:1837:2: () a0= '==' rightArg= parseop_Expression_level_19
        {
        // Exp.g:1837:2: ()
        // Exp.g:1837:2: 
        {
        }


        a0=(Token)match(input,30,FOLLOW_30_in_synpred19_Exp959); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred19_Exp970);
        rightArg=parseop_Expression_level_19();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred19_Exp

    // $ANTLR start synpred20_Exp
    public final void synpred20_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1904:2: ( () a0= '!=' rightArg= parseop_Expression_level_19 )
        // Exp.g:1904:2: () a0= '!=' rightArg= parseop_Expression_level_19
        {
        // Exp.g:1904:2: ()
        // Exp.g:1904:2: 
        {
        }


        a0=(Token)match(input,14,FOLLOW_14_in_synpred20_Exp988); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred20_Exp999);
        rightArg=parseop_Expression_level_19();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred20_Exp

    // $ANTLR start synpred21_Exp
    public final void synpred21_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1836:39: ( ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+ )
        // Exp.g:1836:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
        {
        // Exp.g:1836:39: ( () a0= '==' rightArg= parseop_Expression_level_19 | () a0= '!=' rightArg= parseop_Expression_level_19 )+
        int cnt33=0;
        loop33:
        do {
            int alt33=3;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==30) ) {
                alt33=1;
            }
            else if ( (LA33_0==14) ) {
                alt33=2;
            }


            switch (alt33) {
        	case 1 :
        	    // Exp.g:1837:0: () a0= '==' rightArg= parseop_Expression_level_19
        	    {
        	    // Exp.g:1837:2: ()
        	    // Exp.g:1837:2: 
        	    {
        	    }


        	    a0=(Token)match(input,30,FOLLOW_30_in_synpred21_Exp959); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred21_Exp970);
        	    rightArg=parseop_Expression_level_19();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Exp.g:1904:0: () a0= '!=' rightArg= parseop_Expression_level_19
        	    {
        	    // Exp.g:1904:2: ()
        	    // Exp.g:1904:2: 
        	    {
        	    }


        	    a0=(Token)match(input,14,FOLLOW_14_in_synpred21_Exp988); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_19_in_synpred21_Exp999);
        	    rightArg=parseop_Expression_level_19();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt33 >= 1 ) break loop33;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(33, input);
                    throw eee;
            }
            cnt33++;
        } while (true);


        }

    }
    // $ANTLR end synpred21_Exp

    // $ANTLR start synpred22_Exp
    public final void synpred22_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1980:2: ( () a0= '^' rightArg= parseop_Expression_level_21 )
        // Exp.g:1980:2: () a0= '^' rightArg= parseop_Expression_level_21
        {
        // Exp.g:1980:2: ()
        // Exp.g:1980:2: 
        {
        }


        a0=(Token)match(input,33,FOLLOW_33_in_synpred22_Exp1050); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_21_in_synpred22_Exp1061);
        rightArg=parseop_Expression_level_21();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred22_Exp

    // $ANTLR start synpred23_Exp
    public final void synpred23_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:1979:39: ( ( () a0= '^' rightArg= parseop_Expression_level_21 )+ )
        // Exp.g:1979:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
        {
        // Exp.g:1979:39: ( () a0= '^' rightArg= parseop_Expression_level_21 )+
        int cnt34=0;
        loop34:
        do {
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==33) ) {
                alt34=1;
            }


            switch (alt34) {
        	case 1 :
        	    // Exp.g:1980:0: () a0= '^' rightArg= parseop_Expression_level_21
        	    {
        	    // Exp.g:1980:2: ()
        	    // Exp.g:1980:2: 
        	    {
        	    }


        	    a0=(Token)match(input,33,FOLLOW_33_in_synpred23_Exp1050); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_21_in_synpred23_Exp1061);
        	    rightArg=parseop_Expression_level_21();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt34 >= 1 ) break loop34;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(34, input);
                    throw eee;
            }
            cnt34++;
        } while (true);


        }

    }
    // $ANTLR end synpred23_Exp

    // $ANTLR start synpred24_Exp
    public final void synpred24_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2056:2: ( () a0= '+' rightArg= parseop_Expression_level_22 )
        // Exp.g:2056:2: () a0= '+' rightArg= parseop_Expression_level_22
        {
        // Exp.g:2056:2: ()
        // Exp.g:2056:2: 
        {
        }


        a0=(Token)match(input,18,FOLLOW_18_in_synpred24_Exp1112); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred24_Exp1123);
        rightArg=parseop_Expression_level_22();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred24_Exp

    // $ANTLR start synpred25_Exp
    public final void synpred25_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2123:2: ( () a0= '-' rightArg= parseop_Expression_level_22 )
        // Exp.g:2123:2: () a0= '-' rightArg= parseop_Expression_level_22
        {
        // Exp.g:2123:2: ()
        // Exp.g:2123:2: 
        {
        }


        a0=(Token)match(input,21,FOLLOW_21_in_synpred25_Exp1141); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred25_Exp1152);
        rightArg=parseop_Expression_level_22();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred25_Exp

    // $ANTLR start synpred26_Exp
    public final void synpred26_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2055:39: ( ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+ )
        // Exp.g:2055:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
        {
        // Exp.g:2055:39: ( () a0= '+' rightArg= parseop_Expression_level_22 | () a0= '-' rightArg= parseop_Expression_level_22 )+
        int cnt35=0;
        loop35:
        do {
            int alt35=3;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==18) ) {
                alt35=1;
            }
            else if ( (LA35_0==21) ) {
                alt35=2;
            }


            switch (alt35) {
        	case 1 :
        	    // Exp.g:2056:0: () a0= '+' rightArg= parseop_Expression_level_22
        	    {
        	    // Exp.g:2056:2: ()
        	    // Exp.g:2056:2: 
        	    {
        	    }


        	    a0=(Token)match(input,18,FOLLOW_18_in_synpred26_Exp1112); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred26_Exp1123);
        	    rightArg=parseop_Expression_level_22();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Exp.g:2123:0: () a0= '-' rightArg= parseop_Expression_level_22
        	    {
        	    // Exp.g:2123:2: ()
        	    // Exp.g:2123:2: 
        	    {
        	    }


        	    a0=(Token)match(input,21,FOLLOW_21_in_synpred26_Exp1141); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_22_in_synpred26_Exp1152);
        	    rightArg=parseop_Expression_level_22();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt35 >= 1 ) break loop35;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(35, input);
                    throw eee;
            }
            cnt35++;
        } while (true);


        }

    }
    // $ANTLR end synpred26_Exp

    // $ANTLR start synpred27_Exp
    public final void synpred27_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2199:2: ( () a0= '*' rightArg= parseop_Expression_level_80 )
        // Exp.g:2199:2: () a0= '*' rightArg= parseop_Expression_level_80
        {
        // Exp.g:2199:2: ()
        // Exp.g:2199:2: 
        {
        }


        a0=(Token)match(input,17,FOLLOW_17_in_synpred27_Exp1203); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred27_Exp1214);
        rightArg=parseop_Expression_level_80();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred27_Exp

    // $ANTLR start synpred28_Exp
    public final void synpred28_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2266:2: ( () a0= '/' rightArg= parseop_Expression_level_80 )
        // Exp.g:2266:2: () a0= '/' rightArg= parseop_Expression_level_80
        {
        // Exp.g:2266:2: ()
        // Exp.g:2266:2: 
        {
        }


        a0=(Token)match(input,24,FOLLOW_24_in_synpred28_Exp1232); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred28_Exp1243);
        rightArg=parseop_Expression_level_80();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred28_Exp

    // $ANTLR start synpred29_Exp
    public final void synpred29_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2198:39: ( ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+ )
        // Exp.g:2198:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
        {
        // Exp.g:2198:39: ( () a0= '*' rightArg= parseop_Expression_level_80 | () a0= '/' rightArg= parseop_Expression_level_80 )+
        int cnt36=0;
        loop36:
        do {
            int alt36=3;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==17) ) {
                alt36=1;
            }
            else if ( (LA36_0==24) ) {
                alt36=2;
            }


            switch (alt36) {
        	case 1 :
        	    // Exp.g:2199:0: () a0= '*' rightArg= parseop_Expression_level_80
        	    {
        	    // Exp.g:2199:2: ()
        	    // Exp.g:2199:2: 
        	    {
        	    }


        	    a0=(Token)match(input,17,FOLLOW_17_in_synpred29_Exp1203); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred29_Exp1214);
        	    rightArg=parseop_Expression_level_80();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Exp.g:2266:0: () a0= '/' rightArg= parseop_Expression_level_80
        	    {
        	    // Exp.g:2266:2: ()
        	    // Exp.g:2266:2: 
        	    {
        	    }


        	    a0=(Token)match(input,24,FOLLOW_24_in_synpred29_Exp1232); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_80_in_synpred29_Exp1243);
        	    rightArg=parseop_Expression_level_80();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt36 >= 1 ) break loop36;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(36, input);
                    throw eee;
            }
            cnt36++;
        } while (true);


        }

    }
    // $ANTLR end synpred29_Exp

    // $ANTLR start synpred33_Exp
    public final void synpred33_Exp_fragment() throws RecognitionException {
        Token a0=null;

        // Exp.g:2493:4: (a0= '++' )
        // Exp.g:2493:4: a0= '++'
        {
        a0=(Token)match(input,19,FOLLOW_19_in_synpred33_Exp1394); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred33_Exp

    // $ANTLR start synpred34_Exp
    public final void synpred34_Exp_fragment() throws RecognitionException {
        Token a0=null;

        // Exp.g:2548:4: (a0= '--' )
        // Exp.g:2548:4: a0= '--'
        {
        a0=(Token)match(input,22,FOLLOW_22_in_synpred34_Exp1409); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred34_Exp

    // $ANTLR start synpred35_Exp
    public final void synpred35_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2612:2: ( () a0= '+=' rightArg= parseop_Expression_level_91 )
        // Exp.g:2612:2: () a0= '+=' rightArg= parseop_Expression_level_91
        {
        // Exp.g:2612:2: ()
        // Exp.g:2612:2: 
        {
        }


        a0=(Token)match(input,20,FOLLOW_20_in_synpred35_Exp1459); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred35_Exp1470);
        rightArg=parseop_Expression_level_91();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred35_Exp

    // $ANTLR start synpred36_Exp
    public final void synpred36_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2679:2: ( () a0= '-=' rightArg= parseop_Expression_level_91 )
        // Exp.g:2679:2: () a0= '-=' rightArg= parseop_Expression_level_91
        {
        // Exp.g:2679:2: ()
        // Exp.g:2679:2: 
        {
        }


        a0=(Token)match(input,23,FOLLOW_23_in_synpred36_Exp1488); if (state.failed) return ;

        pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred36_Exp1499);
        rightArg=parseop_Expression_level_91();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred36_Exp

    // $ANTLR start synpred37_Exp
    public final void synpred37_Exp_fragment() throws RecognitionException {
        Token a0=null;
        org.coolsoftware.coolcomponents.expressions.Expression rightArg =null;


        // Exp.g:2611:39: ( ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+ )
        // Exp.g:2611:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
        {
        // Exp.g:2611:39: ( () a0= '+=' rightArg= parseop_Expression_level_91 | () a0= '-=' rightArg= parseop_Expression_level_91 )+
        int cnt37=0;
        loop37:
        do {
            int alt37=3;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==20) ) {
                alt37=1;
            }
            else if ( (LA37_0==23) ) {
                alt37=2;
            }


            switch (alt37) {
        	case 1 :
        	    // Exp.g:2612:0: () a0= '+=' rightArg= parseop_Expression_level_91
        	    {
        	    // Exp.g:2612:2: ()
        	    // Exp.g:2612:2: 
        	    {
        	    }


        	    a0=(Token)match(input,20,FOLLOW_20_in_synpred37_Exp1459); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred37_Exp1470);
        	    rightArg=parseop_Expression_level_91();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;
        	case 2 :
        	    // Exp.g:2679:0: () a0= '-=' rightArg= parseop_Expression_level_91
        	    {
        	    // Exp.g:2679:2: ()
        	    // Exp.g:2679:2: 
        	    {
        	    }


        	    a0=(Token)match(input,23,FOLLOW_23_in_synpred37_Exp1488); if (state.failed) return ;

        	    pushFollow(FOLLOW_parseop_Expression_level_91_in_synpred37_Exp1499);
        	    rightArg=parseop_Expression_level_91();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt37 >= 1 ) break loop37;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(37, input);
                    throw eee;
            }
            cnt37++;
        } while (true);


        }

    }
    // $ANTLR end synpred37_Exp

    // Delegated rules

    public final boolean synpred18_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred18_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred12_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred12_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred36_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred36_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred27_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred27_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred34_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred34_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred21_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred21_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred28_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred28_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred16_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred16_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred9_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred9_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred19_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred19_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred22_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred22_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred25_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred25_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred7_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred13_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred13_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred20_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred20_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred37_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred37_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred17_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred17_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred4_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred24_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred24_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred15_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred15_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred14_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred14_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred23_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred23_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred5_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred35_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred35_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred33_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred33_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred6_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred6_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred29_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred29_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred26_Exp() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred26_Exp_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Block_in_start96 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_parse_org_coolsoftware_coolcomponents_expressions_Block129 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block147 = new BitSet(new long[]{0x0000200004000000L});
    public static final BitSet FOLLOW_26_in_parse_org_coolsoftware_coolcomponents_expressions_Block174 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_Block200 = new BitSet(new long[]{0x0000200004000000L});
    public static final BitSet FOLLOW_45_in_parse_org_coolsoftware_coolcomponents_expressions_Block241 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable274 = new BitSet(new long[]{0x0000000022000002L});
    public static final BitSet FOLLOW_25_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable304 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_TYPE_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable330 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_29_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable385 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_03467 = new BitSet(new long[]{0x0000010400000002L});
    public static final BitSet FOLLOW_40_in_parseop_Expression_level_03487 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_03504 = new BitSet(new long[]{0x0000010400000002L});
    public static final BitSet FOLLOW_34_in_parseop_Expression_level_03538 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_parseop_Expression_level_03555 = new BitSet(new long[]{0x0000010400000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_04601 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_38_in_parseop_Expression_level_04617 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_parseop_Expression_level_04631 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_36_in_parseop_Expression_level_5672 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_5683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_parseop_Expression_level_5692 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_5703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_6_in_parseop_Expression_level_5713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_6735 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_29_in_parseop_Expression_level_6748 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_parseop_Expression_level_6759 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11797 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_31_in_parseop_Expression_level_11810 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11821 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_32_in_parseop_Expression_level_11839 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11850 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_27_in_parseop_Expression_level_11868 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11879 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_28_in_parseop_Expression_level_11897 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_parseop_Expression_level_11908 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_12946 = new BitSet(new long[]{0x0000000040004002L});
    public static final BitSet FOLLOW_30_in_parseop_Expression_level_12959 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_12970 = new BitSet(new long[]{0x0000000040004002L});
    public static final BitSet FOLLOW_14_in_parseop_Expression_level_12988 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_parseop_Expression_level_12999 = new BitSet(new long[]{0x0000000040004002L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_191037 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_33_in_parseop_Expression_level_191050 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_parseop_Expression_level_191061 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_211099 = new BitSet(new long[]{0x0000000000240002L});
    public static final BitSet FOLLOW_18_in_parseop_Expression_level_211112 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_211123 = new BitSet(new long[]{0x0000000000240002L});
    public static final BitSet FOLLOW_21_in_parseop_Expression_level_211141 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_parseop_Expression_level_211152 = new BitSet(new long[]{0x0000000000240002L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_221190 = new BitSet(new long[]{0x0000000001020002L});
    public static final BitSet FOLLOW_17_in_parseop_Expression_level_221203 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_221214 = new BitSet(new long[]{0x0000000001020002L});
    public static final BitSet FOLLOW_24_in_parseop_Expression_level_221232 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_parseop_Expression_level_221243 = new BitSet(new long[]{0x0000000001020002L});
    public static final BitSet FOLLOW_41_in_parseop_Expression_level_801281 = new BitSet(new long[]{0x00000CA000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_801292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_parseop_Expression_level_801301 = new BitSet(new long[]{0x00000CA000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_801312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_87_in_parseop_Expression_level_801322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_parseop_Expression_level_871344 = new BitSet(new long[]{0x00000C2000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_871355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_88_in_parseop_Expression_level_871365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_90_in_parseop_Expression_level_881387 = new BitSet(new long[]{0x0000000000480002L});
    public static final BitSet FOLLOW_19_in_parseop_Expression_level_881394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_parseop_Expression_level_881409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_901446 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_20_in_parseop_Expression_level_901459 = new BitSet(new long[]{0x00000C2000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_901470 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_23_in_parseop_Expression_level_901488 = new BitSet(new long[]{0x00000C2000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_parseop_Expression_level_901499 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression_in_parseop_Expression_level_911537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression_in_parseop_Expression_level_911545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression_in_parseop_Expression_level_911553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression_in_parseop_Expression_level_911561 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression_in_parseop_Expression_level_911569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration_in_parseop_Expression_level_911577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression_in_parseop_Expression_level_911585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ILT_in_parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression1609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REAL_LITERAL_in_parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression1639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression1671 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression1680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_34_34_in_parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression1712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression1740 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression1753 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression1765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration1791 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_variables_Variable_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration1804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TEXT_in_parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression1833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parseop_Expression_level_03_in_parse_org_coolsoftware_coolcomponents_expressions_Expression1857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_synpred4_Exp385 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parse_org_coolsoftware_coolcomponents_expressions_Expression_in_synpred4_Exp411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_synpred5_Exp487 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred5_Exp504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_synpred6_Exp538 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred6_Exp555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_synpred7_Exp487 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred7_Exp504 = new BitSet(new long[]{0x0000010400000002L});
    public static final BitSet FOLLOW_34_in_synpred7_Exp538 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_04_in_synpred7_Exp555 = new BitSet(new long[]{0x0000010400000002L});
    public static final BitSet FOLLOW_38_in_synpred8_Exp617 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_synpred8_Exp631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_synpred9_Exp617 = new BitSet(new long[]{0x00000EB80000A590L});
    public static final BitSet FOLLOW_parseop_Expression_level_5_in_synpred9_Exp631 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_29_in_synpred12_Exp748 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_synpred12_Exp759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_synpred13_Exp748 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_11_in_synpred13_Exp759 = new BitSet(new long[]{0x0000000020000002L});
    public static final BitSet FOLLOW_31_in_synpred14_Exp810 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred14_Exp821 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_synpred15_Exp839 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred15_Exp850 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_synpred16_Exp868 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred16_Exp879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_synpred17_Exp897 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred17_Exp908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_synpred18_Exp810 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred18_Exp821 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_32_in_synpred18_Exp839 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred18_Exp850 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_27_in_synpred18_Exp868 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred18_Exp879 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_28_in_synpred18_Exp897 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_12_in_synpred18_Exp908 = new BitSet(new long[]{0x0000000198000002L});
    public static final BitSet FOLLOW_30_in_synpred19_Exp959 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred19_Exp970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_synpred20_Exp988 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred20_Exp999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred21_Exp959 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred21_Exp970 = new BitSet(new long[]{0x0000000040004002L});
    public static final BitSet FOLLOW_14_in_synpred21_Exp988 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_19_in_synpred21_Exp999 = new BitSet(new long[]{0x0000000040004002L});
    public static final BitSet FOLLOW_33_in_synpred22_Exp1050 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_synpred22_Exp1061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_synpred23_Exp1050 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_21_in_synpred23_Exp1061 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_18_in_synpred24_Exp1112 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred24_Exp1123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_synpred25_Exp1141 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred25_Exp1152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_synpred26_Exp1112 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred26_Exp1123 = new BitSet(new long[]{0x0000000000240002L});
    public static final BitSet FOLLOW_21_in_synpred26_Exp1141 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_22_in_synpred26_Exp1152 = new BitSet(new long[]{0x0000000000240002L});
    public static final BitSet FOLLOW_17_in_synpred27_Exp1203 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred27_Exp1214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_synpred28_Exp1232 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred28_Exp1243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_synpred29_Exp1203 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred29_Exp1214 = new BitSet(new long[]{0x0000000001020002L});
    public static final BitSet FOLLOW_24_in_synpred29_Exp1232 = new BitSet(new long[]{0x00000EA800008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_80_in_synpred29_Exp1243 = new BitSet(new long[]{0x0000000001020002L});
    public static final BitSet FOLLOW_19_in_synpred33_Exp1394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_synpred34_Exp1409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_synpred35_Exp1459 = new BitSet(new long[]{0x00000C2000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred35_Exp1470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_synpred36_Exp1488 = new BitSet(new long[]{0x00000C2000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred36_Exp1499 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_synpred37_Exp1459 = new BitSet(new long[]{0x00000C2000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred37_Exp1470 = new BitSet(new long[]{0x0000000000900002L});
    public static final BitSet FOLLOW_23_in_synpred37_Exp1488 = new BitSet(new long[]{0x00000C2000008590L});
    public static final BitSet FOLLOW_parseop_Expression_level_91_in_synpred37_Exp1499 = new BitSet(new long[]{0x0000000000900002L});

}