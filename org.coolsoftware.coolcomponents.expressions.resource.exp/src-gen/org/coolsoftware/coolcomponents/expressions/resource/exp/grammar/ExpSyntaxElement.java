/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.grammar;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class ExpSyntaxElement {
	
	private ExpSyntaxElement[] children;
	private ExpSyntaxElement parent;
	private org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpCardinality cardinality;
	
	public ExpSyntaxElement(org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpCardinality cardinality, ExpSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (ExpSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(ExpSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public ExpSyntaxElement getParent() {
		return parent;
	}
	
	public ExpSyntaxElement[] getChildren() {
		if (children == null) {
			return new ExpSyntaxElement[0];
		}
		return children;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpCardinality getCardinality() {
		return cardinality;
	}
	
}
