/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.util;

/**
 * Class ExpTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.
 */
public class ExpTextResourceUtil {
	
	/**
	 * Use
	 * org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.ge
	 * tResource() instead.
	 */
	@Deprecated	
	public static org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResource getResource(org.eclipse.core.resources.IFile file) {
		return new org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.ge
	 * tResource() instead.
	 */
	@Deprecated	
	public static org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.ge
	 * tResource() instead.
	 */
	@Deprecated	
	public static org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResource getResource(org.eclipse.emf.common.util.URI uri) {
		return org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.ge
	 * tResource() instead.
	 */
	@Deprecated	
	public static org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpResourceUtil.getResource(uri, options);
	}
	
}
