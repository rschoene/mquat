/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.util;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractExpInterpreter<ResultType, ContextType> {
	
	private java.util.Stack<org.eclipse.emf.ecore.EObject> interpretationStack = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	private java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpInterpreterListener> listeners = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpInterpreterListener>();
	private org.eclipse.emf.ecore.EObject nextObjectToInterprete;
	private Object currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		org.eclipse.emf.ecore.EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (java.util.EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(org.eclipse.emf.ecore.EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof org.coolsoftware.coolcomponents.expressions.Block) {
			result = interprete_org_coolsoftware_coolcomponents_expressions_Block((org.coolsoftware.coolcomponents.expressions.Block) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression) {
			result = interprete_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression((org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.coolcomponents.expressions.Expression) {
			result = interprete_org_coolsoftware_coolcomponents_expressions_Expression((org.coolsoftware.coolcomponents.expressions.Expression) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.coolsoftware.coolcomponents.expressions.Statement) {
			result = interprete_org_coolsoftware_coolcomponents_expressions_Statement((org.coolsoftware.coolcomponents.expressions.Statement) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_org_coolsoftware_coolcomponents_expressions_Statement(org.coolsoftware.coolcomponents.expressions.Statement statement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_coolcomponents_expressions_Expression(org.coolsoftware.coolcomponents.expressions.Expression expression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression(org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression parenthesisedExpression, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_coolsoftware_coolcomponents_expressions_Block(org.coolsoftware.coolcomponents.expressions.Block block, ContextType context) {
		return null;
	}
	
	private void notifyListeners(org.eclipse.emf.ecore.EObject element) {
		for (org.coolsoftware.coolcomponents.expressions.resource.exp.IExpInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(org.eclipse.emf.ecore.EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		for (org.eclipse.emf.ecore.EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		java.util.List<org.eclipse.emf.ecore.EObject> reverse = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>(objects.size());
		reverse.addAll(objects);
		java.util.Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(org.eclipse.emf.ecore.EObject root) {
		java.util.List<org.eclipse.emf.ecore.EObject> objects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
		objects.add(root);
		java.util.Iterator<org.eclipse.emf.ecore.EObject> it = root.eAllContents();
		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public org.eclipse.emf.ecore.EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public java.util.Stack<org.eclipse.emf.ecore.EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public Object getCurrentContext() {
		return currentContext;
	}
	
}
