/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;

public class ExpReferenceResolverSwitch implements org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected org.coolsoftware.coolcomponents.expressions.resource.exp.analysis.VariableCallExpressionReferredVariableReferenceResolver variableCallExpressionReferredVariableReferenceResolver = new org.coolsoftware.coolcomponents.expressions.resource.exp.analysis.VariableCallExpressionReferredVariableReferenceResolver();
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable> getVariableCallExpressionReferredVariableReferenceResolver() {
		return getResolverChain(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression_ReferredVariable(), variableCallExpressionReferredVariableReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		variableCallExpressionReferredVariableReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression().isInstance(container)) {
			ExpFuzzyResolveResult<org.coolsoftware.coolcomponents.expressions.variables.Variable> frr = new ExpFuzzyResolveResult<org.coolsoftware.coolcomponents.expressions.variables.Variable>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("referredVariable")) {
				variableCallExpressionReferredVariableReferenceResolver.resolve(identifier, (org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression_ReferredVariable()) {
			return getResolverChain(reference, variableCallExpressionReferredVariableReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpRuntimeUtil().logWarning("Found value with invalid type for option " + org.coolsoftware.coolcomponents.expressions.resource.exp.IExpOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver) {
			org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver replacingResolver = (org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver) resolverValue;
			if (replacingResolver instanceof org.coolsoftware.coolcomponents.expressions.resource.exp.IExpDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((org.coolsoftware.coolcomponents.expressions.resource.exp.IExpDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceCache) {
					org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver nextResolver = (org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver) next;
					if (nextResolver instanceof org.coolsoftware.coolcomponents.expressions.resource.exp.IExpDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((org.coolsoftware.coolcomponents.expressions.resource.exp.IExpDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.coolcomponents.expressions.resource.exp.IExpOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.coolcomponents.expressions.resource.exp.IExpDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpRuntimeUtil().logWarning("Found value with invalid type in value map for option " + org.coolsoftware.coolcomponents.expressions.resource.exp.IExpOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + org.coolsoftware.coolcomponents.expressions.resource.exp.IExpDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
