/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;

public class ExpPrinter implements org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextPrinter {
	
	protected org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolverFactory tokenResolverFactory = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTokenResolverFactory();
	
	protected java.io.OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource;
	
	private java.util.Map<?, ?> options;
	private String encoding = System.getProperty("file.encoding");
	
	public ExpPrinter(java.io.OutputStream outputStream, org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(java.util.Map<String, Integer> featureCounter, java.util.Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.io.PrintWriter out, String globaltab) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof org.coolsoftware.coolcomponents.expressions.Block) {
			print_org_coolsoftware_coolcomponents_expressions_Block((org.coolsoftware.coolcomponents.expressions.Block) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression) {
			print_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression((org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression) {
			print_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression((org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression) {
			print_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression((org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression) {
			print_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression((org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression) {
			print_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression((org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_IncrementOperatorExpression((org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_DecrementOperatorExpression((org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_NegativeOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_SinusOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_CosinusOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_FunctionExpression((org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_AdditiveOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_SubtractiveOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_AddToVarOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_SubtractFromVarOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_MultiplicativeOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_DivisionOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_PowerOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_GreaterThanOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_GreaterThanEqualsOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_LessThanOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_LessThanEqualsOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_EqualsOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_NotEqualsOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_DisjunctionOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_ConjunctionOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_ImplicationOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_operators_NegationOperationCallExpression((org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration) {
			print_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration((org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.Variable) {
			print_org_coolsoftware_coolcomponents_expressions_variables_Variable((org.coolsoftware.coolcomponents.expressions.variables.Variable) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression) {
			print_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression((org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression) element, globaltab, out);
			return;
		}
		if (element instanceof org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment) {
			print_org_coolsoftware_coolcomponents_expressions_variables_VariableAssignment((org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpReferenceResolverSwitch getReferenceResolverSwitch() {
		return (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpReferenceResolverSwitch) new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpProblem(errorMessage, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType.PRINT_PROBLEM, org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public void setEncoding(String encoding) {
		if (encoding != null) {
			this.encoding = encoding;
		}
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(org.eclipse.emf.ecore.EObject element) throws java.io.IOException {
		java.io.PrintWriter out = new java.io.PrintWriter(new java.io.OutputStreamWriter(new java.io.BufferedOutputStream(outputStream), encoding));
		doPrint(element, out, "");
		out.flush();
		out.close();
	}
	
	public void print_org_coolsoftware_coolcomponents_expressions_Block(org.coolsoftware.coolcomponents.expressions.Block element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS));
		printCountingMap.put("expressions", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("expressions");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("expressions", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_coolsoftware_coolcomponents_expressions_Block_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_org_coolsoftware_coolcomponents_expressions_Block_0(org.coolsoftware.coolcomponents.expressions.Block element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("expressions");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("expressions", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression(org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE));
		printCountingMap.put("value", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("value");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE));
			if (o != null) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver resolver = tokenResolverFactory.createTokenResolver("ILT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), element));
				out.print(" ");
			}
			printCountingMap.put("value", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression(org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE));
		printCountingMap.put("value", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("value");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE));
			if (o != null) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver resolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), element));
				out.print(" ");
			}
			printCountingMap.put("value", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression(org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE));
		printCountingMap.put("value", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (BooleanTerminal)
		count = printCountingMap.get("value");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE));
			if (o != null) {
			}
			printCountingMap.put("value", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression(org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE));
		printCountingMap.put("value", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("value");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE));
			if (o != null) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), element));
				out.print(" ");
			}
			printCountingMap.put("value", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression(org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_IncrementOperatorExpression(org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("++");
		out.print(" ");
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_DecrementOperatorExpression(org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("--");
		out.print(" ");
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_NegativeOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("not");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_SinusOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("sin");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_CosinusOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("cos");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_FunctionExpression(org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("f");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_AdditiveOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("+");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_SubtractiveOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("-");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_AddToVarOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("+=");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_SubtractFromVarOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("-=");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_MultiplicativeOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("*");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_DivisionOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("/");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_PowerOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("^");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_GreaterThanOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print(">");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_GreaterThanEqualsOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print(">=");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_LessThanOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("<");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_LessThanEqualsOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("<=");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_EqualsOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("==");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_NotEqualsOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("!=");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_DisjunctionOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("or");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_ConjunctionOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("and");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_ImplicationOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("implies");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("argumentExps");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("argumentExps", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_operators_NegationOperationCallExpression(org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
		printCountingMap.put("sourceExp", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__OPERATION_NAME));
		printCountingMap.put("operationName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS));
		printCountingMap.put("argumentExps", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("!");
		// DEFINITION PART BEGINS (WhiteSpaces)
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("sourceExp");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("sourceExp", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration(org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE));
		printCountingMap.put("declaredVariable", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("var");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("declaredVariable");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("declaredVariable", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_variables_Variable(org.coolsoftware.coolcomponents.expressions.variables.Variable element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION));
		printCountingMap.put("initialExpression", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME));
			if (o != null) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_coolsoftware_coolcomponents_expressions_variables_Variable_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_coolsoftware_coolcomponents_expressions_variables_Variable_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_org_coolsoftware_coolcomponents_expressions_variables_Variable_0(org.coolsoftware.coolcomponents.expressions.variables.Variable element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("type");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE));
			if (o != null) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver resolver = tokenResolverFactory.createTokenResolver("TYPE_LITERAL");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), element));
				out.print(" ");
			}
			printCountingMap.put("type", count - 1);
		}
	}
	
	public void print_org_coolsoftware_coolcomponents_expressions_variables_Variable_1(org.coolsoftware.coolcomponents.expressions.variables.Variable element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("initialExpression");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("initialExpression", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression(org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE));
		printCountingMap.put("referredVariable", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("referredVariable");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE));
			if (o != null) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver resolver = tokenResolverFactory.createTokenResolver("TEXT");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableCallExpressionReferredVariableReferenceResolver().deResolve((org.coolsoftware.coolcomponents.expressions.variables.Variable) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE)), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), element));
				out.print(" ");
			}
			printCountingMap.put("referredVariable", count - 1);
		}
	}
	
	
	public void print_org_coolsoftware_coolcomponents_expressions_variables_VariableAssignment(org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE));
		printCountingMap.put("referredVariable", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION));
		printCountingMap.put("valueExpression", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("referredVariable");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("referredVariable", count - 1);
		}
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("valueExpression");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("valueExpression", count - 1);
		}
	}
	
	
}
