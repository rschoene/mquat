/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;

/**
 * The ExpTaskItemBuilder is used to find task items in text documents. The
 * current implementation uses the generated lexer and the TaskItemDetector to
 * detect task items. This class is called by the BuilderAdapter, which runs both
 * this builder and the default builder that is intended to be customized.
 */
public class ExpTaskItemBuilder {
	
	public void build(org.eclipse.core.resources.IFile resource, org.eclipse.emf.ecore.resource.ResourceSet resourceSet, org.eclipse.core.runtime.IProgressMonitor monitor) {
		monitor.setTaskName("Searching for task items");
		new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpMarkerHelper().removeAllMarkers(resource, org.eclipse.core.resources.IMarker.TASK);
		if (isInBinFolder(resource)) {
			return;
		}
		java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTaskItem> taskItems = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTaskItem>();
		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTaskItemDetector taskItemDetector = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTaskItemDetector();
		try {
			java.io.InputStream inputStream = resource.getContents();
			String content = org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpStreamUtil.getContent(inputStream);
			org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextScanner lexer = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpMetaInformation().createLexer();
			lexer.setText(content);
			
			org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextToken nextToken = lexer.getNextToken();
			while (nextToken != null) {
				String text = nextToken.getText();
				taskItems.addAll(taskItemDetector.findTaskItems(text, nextToken.getLine(), nextToken.getOffset()));
				nextToken = lexer.getNextToken();
			}
		} catch (java.io.IOException e) {
			org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpPlugin.logError("Exception while searching for task items", e);
		} catch (org.eclipse.core.runtime.CoreException e) {
			org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpPlugin.logError("Exception while searching for task items", e);
		}
		
		for (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTaskItem taskItem : taskItems) {
			java.util.Map<String, Object> markerAttributes = new java.util.LinkedHashMap<String, Object>();
			markerAttributes.put(org.eclipse.core.resources.IMarker.USER_EDITABLE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.DONE, false);
			markerAttributes.put(org.eclipse.core.resources.IMarker.LINE_NUMBER, taskItem.getLine());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_START, taskItem.getCharStart());
			markerAttributes.put(org.eclipse.core.resources.IMarker.CHAR_END, taskItem.getCharEnd());
			markerAttributes.put(org.eclipse.core.resources.IMarker.MESSAGE, taskItem.getMessage());
			new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpMarkerHelper().createMarker(resource, org.eclipse.core.resources.IMarker.TASK, markerAttributes);
		}
	}
	
	public String getBuilderMarkerId() {
		return org.eclipse.core.resources.IMarker.TASK;
	}
	
	public boolean isInBinFolder(org.eclipse.core.resources.IFile resource) {
		org.eclipse.core.resources.IContainer parent = resource.getParent();
		while (parent != null) {
			if ("bin".equals(parent.getName())) {
				return true;
			}
			parent = parent.getParent();
		}
		return false;
	}
	
}
