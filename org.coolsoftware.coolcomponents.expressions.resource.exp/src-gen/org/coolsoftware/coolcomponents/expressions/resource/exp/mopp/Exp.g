grammar Exp;

options {
	superClass = ExpANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;
}

@members{
	private org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolverFactory tokenResolverFactory = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> expectedElements = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpProblem() {
					public org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity getSeverity() {
						return org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemSeverity.ERROR;
					}
					public org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType getType() {
						return org.coolsoftware.coolcomponents.expressions.resource.exp.ExpEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement terminal = org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpFollowSetProvider.TERMINALS[terminalID];
		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[] containmentFeatures = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpFollowSetProvider.LINKS[ids[i]];
		}
		org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace containmentTrace = new org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace(eClass, containmentFeatures);
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal expectedElement = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>() {
			public boolean execute(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource resource) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new ExpParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ExpLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new ExpParser(new org.antlr.runtime3_4_0.CommonTokenStream(new ExpLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public ExpParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((ExpLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((ExpLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.Block.class) {
				return parse_org_coolsoftware_coolcomponents_expressions_Block();
			}
			if (type.getInstanceClass() == org.coolsoftware.coolcomponents.expressions.variables.Variable.class) {
				return parse_org_coolsoftware_coolcomponents_expressions_variables_Variable();
			}
		}
		throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(org.coolsoftware.coolcomponents.expressions.resource.exp.IExpOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public org.coolsoftware.coolcomponents.expressions.resource.exp.IExpParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource>>();
		org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpParseResult parseResult = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		org.coolsoftware.coolcomponents.expressions.resource.exp.IExpParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (org.coolsoftware.coolcomponents.expressions.resource.exp.IExpCommand<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal>();
		java.util.List<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal> newFollowSet = new java.util.ArrayList<org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 48;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpPair<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (org.coolsoftware.coolcomponents.expressions.resource.exp.util.ExpPair<org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContainedFeature[]> newFollowerPair : newFollowers) {
							org.coolsoftware.coolcomponents.expressions.resource.exp.IExpExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace containmentTrace = new org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpContainmentTrace(null, newFollowerPair.getRight());
							org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal newFollowTerminal = new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[0]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[1]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[2]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[3]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[4]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[5]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[6]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[7]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[8]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[9]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[10]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[11]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[12]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[13]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[14]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[15]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[16]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[17]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[18]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[19]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[20]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[21]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[22]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[23]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[24]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[25]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[26]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[27]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[28]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[29]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[30]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[31]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[32]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[33]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[34]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[35]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[36]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[37]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[38]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[39]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[40]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[41]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[42]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[43]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[44]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[45]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[46]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[47]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[48]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[49]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[50]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[51]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[52]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[53]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[54]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[55]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[56]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[57]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[58]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[59]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[60]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[61]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[62]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[63]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[64]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[65]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[66]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[67]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[68]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[69]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[70]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[71]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[72]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[73]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[74]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[75]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[76]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[77]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[78]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[79]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[80]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[81]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[82]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[83]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[84]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[85]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[86]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[87]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[88]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[89]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[90]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[91]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[92]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[93]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[94]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[95]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[96]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[97]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[98]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[99]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[100]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[101]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[102]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[103]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[104]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[105]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[106]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[107]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[108]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[109]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[110]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[111]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[112]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[113]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[114]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[115]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[116]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[117]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[118]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[119]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[120]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[121]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[122]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[123]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[124]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[125]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[126]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[127]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[128]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[129]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[130]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[131]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[132]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[133]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[134]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[135]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[136]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[137]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[138]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[139]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[140]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[141]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[142]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[143]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[144]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[145]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[146]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[147]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[148]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[149]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[150]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[151]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[152]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[153]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[154]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[155]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[156]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[157]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[158]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[159]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[160]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[161]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[162]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[163]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[164]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[165]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[166]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[167]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[168]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[169]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[170]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[171]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[172]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[173]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[174]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[175]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[176]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[177]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[178]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[179]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[180]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[181]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[182]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[183]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[184]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[185]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[186]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[187]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[188]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[189]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[190]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[191]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[192]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[193]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[194]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[195]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[196]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[197]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[198]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[199]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[200]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[201]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[202]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[203]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[204]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[205]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[206]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[207]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[208]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[209]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[210]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[211]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[212]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[213]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[214]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[215]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[216]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[217]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[218]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[219]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[220]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[221]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[222]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[223]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[224]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[225]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[226]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[227]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[228]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[229]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[230]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[231]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[232]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[233]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[234]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[235]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[236]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[237]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[238]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[239]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[240]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_org_coolsoftware_coolcomponents_expressions_Expression{ element = c0; }
		|  		c1 = parse_org_coolsoftware_coolcomponents_expressions_Block{ element = c1; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_org_coolsoftware_coolcomponents_expressions_Block returns [org.coolsoftware.coolcomponents.expressions.Block element = null]
@init{
}
:
	a0 = '{' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[241]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[242]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[243]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[244]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[245]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[246]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[247]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[248]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[249]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[250]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[251]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[252]);
	}
	
	(
		a1_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression		{
			if (terminateParsing) {
				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
				startIncompleteElement(element);
			}
			if (a1_0 != null) {
				if (a1_0 != null) {
					Object value = a1_0;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_2, a1_0, true);
				copyLocalizationInfos(a1_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[253]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[254]);
	}
	
	(
		(
			a2 = ';' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_4_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[255]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[256]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[257]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[258]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[259]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[260]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[261]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[262]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[263]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[264]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[265]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[266]);
			}
			
			(
				a3_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.BLOCK__EXPRESSIONS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_4_0_0_2, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[267]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[268]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[269]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[270]);
	}
	
	a4 = '}' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createBlock();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_0_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
	}
	
;

parse_org_coolsoftware_coolcomponents_expressions_variables_Variable returns [org.coolsoftware.coolcomponents.expressions.variables.Variable element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[271]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[272]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[273]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[274]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[275]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[276]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[277]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[278]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[279]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[280]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[281]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[282]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[283]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[284]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[285]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[286]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[287]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[288]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[289]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[290]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[291]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[292]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[293]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[294]);
	}
	
	(
		(
			a1 = ':' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_1_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[295]);
			}
			
			(
				a2 = TYPE_LITERAL				
				{
					if (terminateParsing) {
						throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
						startIncompleteElement(element);
					}
					if (a2 != null) {
						org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TYPE_LITERAL");
						tokenResolver.setOptions(getOptions());
						org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
						}
						org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral resolved = (org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__TYPE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_1_0_0_3, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[296]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[297]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[298]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[299]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[300]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[301]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[302]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[303]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[304]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[305]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[306]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[307]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[308]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[309]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[310]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[311]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[312]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[313]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[314]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[315]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[316]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[317]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[318]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[319]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[320]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[321]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[322]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[323]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[324]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[325]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[326]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[327]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[328]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[329]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[330]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[331]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[332]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[333]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[334]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[335]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[336]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[337]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[338]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[339]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[340]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[341]);
	}
	
	(
		(
			a3 = '=' {
				if (element == null) {
					element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_2_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[342]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[343]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[344]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[345]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[346]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[347]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[348]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[349]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[350]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[351]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[352]);
				addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[353]);
			}
			
			(
				a4_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression				{
					if (terminateParsing) {
						throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
					}
					if (element == null) {
						element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
						startIncompleteElement(element);
					}
					if (a4_0 != null) {
						if (a4_0 != null) {
							Object value = a4_0;
							element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE__INITIAL_EXPRESSION), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_30_0_0_2_0_0_3, a4_0, true);
						copyLocalizationInfos(a4_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[354]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[355]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[356]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[357]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[358]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[359]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[360]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[361]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[362]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[363]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[364]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[365]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[366]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[367]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[368]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[369]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[370]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[371]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[372]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[373]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[374]);
				addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[375]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[376]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[377]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[378]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[379]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[380]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[381]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[382]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[383]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[384]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[385]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[386]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[387]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[388]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[389]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[390]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[391]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[392]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[393]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[394]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[395]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[396]);
		addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[397]);
	}
	
;

parseop_Expression_level_03 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
	leftArg = parseop_Expression_level_04	((
		()
		{ element = null; }
		a0 = 'or' {
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_25_0_0_2, null, true);
			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[398]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[399]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[400]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[401]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[402]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[403]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[404]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[405]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[406]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[407]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[408]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[409]);
		}
		
		rightArg = parseop_Expression_level_04		{
			if (terminateParsing) {
				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_25_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDisjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_25_0_0_4, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
		|		
		()
		{ element = null; }
		a0 = 'and' {
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_26_0_0_2, null, true);
			copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
		}
		{
			// expected elements (follow set)
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[410]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[411]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[412]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[413]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[414]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[415]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[416]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[417]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[418]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[419]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[420]);
			addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[421]);
		}
		
		rightArg = parseop_Expression_level_04		{
			if (terminateParsing) {
				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (leftArg != null) {
				if (leftArg != null) {
					Object value = leftArg;
					element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_26_0_0_0, leftArg, true);
				copyLocalizationInfos(leftArg, element);
			}
		}
		{
			if (terminateParsing) {
				throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
			}
			if (element == null) {
				element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createConjunctionOperationCallExpression();
				startIncompleteElement(element);
			}
			if (rightArg != null) {
				if (rightArg != null) {
					Object value = rightArg;
					addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_26_0_0_4, rightArg, true);
				copyLocalizationInfos(rightArg, element);
			}
		}
		{ leftArg = element; /* this may become an argument in the next iteration */ }
	)+ | /* epsilon */ { element = leftArg; }
	
)
;

parseop_Expression_level_04 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_5((
	()
	{ element = null; }
	a0 = 'implies' {
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_27_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[422]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[423]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[424]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[425]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[426]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[427]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[428]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[429]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[430]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[431]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[432]);
		addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[433]);
	}
	
	rightArg = parseop_Expression_level_5	{
		if (terminateParsing) {
			throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
		}
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		if (leftArg != null) {
			if (leftArg != null) {
				Object value = leftArg;
				element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_27_0_0_0, leftArg, true);
			copyLocalizationInfos(leftArg, element);
		}
	}
	{
		if (terminateParsing) {
			throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
		}
		if (element == null) {
			element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createImplicationOperationCallExpression();
			startIncompleteElement(element);
		}
		if (rightArg != null) {
			if (rightArg != null) {
				Object value = rightArg;
				addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
				completedElement(value, true);
			}
			collectHiddenTokens(element);
			retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_27_0_0_4, rightArg, true);
			copyLocalizationInfos(rightArg, element);
		}
	}
	{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_5 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'f' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_11_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[434]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[435]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[436]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[437]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[438]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[439]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[440]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[441]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[442]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[443]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[444]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[445]);
}

arg = parseop_Expression_level_6{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createFunctionExpression();
	startIncompleteElement(element);
}
if (arg != null) {
	if (arg != null) {
		Object value = arg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.FUNCTION_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_11_0_0_2, arg, true);
	copyLocalizationInfos(arg, element);
}
}
|
a0 = '!' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_28_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[446]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[447]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[448]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[449]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[450]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[451]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[452]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[453]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[454]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[455]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[456]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[457]);
}

arg = parseop_Expression_level_6{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegationOperationCallExpression();
	startIncompleteElement(element);
}
if (arg != null) {
	if (arg != null) {
		Object value = arg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_28_0_0_2, arg, true);
	copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_6{ element = arg; }
;

parseop_Expression_level_6 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_11((
()
{ element = null; }
a0 = '=' {
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
		startIncompleteElement(element);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_32_0_0_2, null, true);
	copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
	// expected elements (follow set)
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[458]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[459]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[460]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[461]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[462]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[463]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[464]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[465]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[466]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[467]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[468]);
	addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[469]);
}

rightArg = parseop_Expression_level_11{
	if (terminateParsing) {
		throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
		startIncompleteElement(element);
	}
	if (leftArg != null) {
		if (leftArg != null) {
			Object value = leftArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__REFERRED_VARIABLE), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_32_0_0_0, leftArg, true);
		copyLocalizationInfos(leftArg, element);
	}
}
{
	if (terminateParsing) {
		throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
	}
	if (element == null) {
		element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableAssignment();
		startIncompleteElement(element);
	}
	if (rightArg != null) {
		if (rightArg != null) {
			Object value = rightArg;
			element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_ASSIGNMENT__VALUE_EXPRESSION), value);
			completedElement(value, true);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_32_0_0_4, rightArg, true);
		copyLocalizationInfos(rightArg, element);
	}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_11 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_12((
()
{ element = null; }
a0 = '>' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_19_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[470]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[471]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[472]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[473]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[474]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[475]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[476]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[477]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[478]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[479]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[480]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[481]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_19_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_19_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '>=' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_20_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[482]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[483]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[484]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[485]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[486]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[487]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[488]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[489]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[490]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[491]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[492]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[493]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_20_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createGreaterThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_20_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '<' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_21_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[494]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[495]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[496]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[497]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[498]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[499]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[500]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[501]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[502]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[503]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[504]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[505]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_21_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_21_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '<=' {
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_22_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[506]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[507]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[508]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[509]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[510]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[511]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[512]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[513]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[514]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[515]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[516]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[517]);
}

rightArg = parseop_Expression_level_12{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (leftArg != null) {
	if (leftArg != null) {
		Object value = leftArg;
		element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_22_0_0_0, leftArg, true);
	copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
	throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
	element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createLessThanEqualsOperationCallExpression();
	startIncompleteElement(element);
}
if (rightArg != null) {
	if (rightArg != null) {
		Object value = rightArg;
		addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
		completedElement(value, true);
	}
	collectHiddenTokens(element);
	retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_22_0_0_4, rightArg, true);
	copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_12 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_19((
()
{ element = null; }
a0 = '==' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_23_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[518]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[519]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[520]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[521]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[522]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[523]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[524]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[525]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[526]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[527]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[528]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[529]);
}

rightArg = parseop_Expression_level_19{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
	Object value = leftArg;
	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_23_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
	Object value = rightArg;
	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_23_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '!=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_24_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[530]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[531]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[532]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[533]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[534]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[535]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[536]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[537]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[538]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[539]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[540]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[541]);
}

rightArg = parseop_Expression_level_19{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
	Object value = leftArg;
	element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_24_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNotEqualsOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
	Object value = rightArg;
	addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
	completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_24_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_19 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_21((
()
{ element = null; }
a0 = '^' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_18_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[542]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[543]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[544]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[545]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[546]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[547]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[548]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[549]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[550]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[551]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[552]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[553]);
}

rightArg = parseop_Expression_level_21{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_18_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createPowerOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_18_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_21 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_22((
()
{ element = null; }
a0 = '+' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_12_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[554]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[555]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[556]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[557]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[558]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[559]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[560]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[561]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[562]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[563]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[564]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[565]);
}

rightArg = parseop_Expression_level_22{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_12_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAdditiveOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_12_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '-' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_13_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[566]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[567]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[568]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[569]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[570]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[571]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[572]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[573]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[574]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[575]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[576]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[577]);
}

rightArg = parseop_Expression_level_22{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_13_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractiveOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_13_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_22 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_80((
()
{ element = null; }
a0 = '*' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_16_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[578]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[579]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[580]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[581]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[582]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[583]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[584]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[585]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[586]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[587]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[588]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[589]);
}

rightArg = parseop_Expression_level_80{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_16_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createMultiplicativeOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_16_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '/' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_17_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[590]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[591]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[592]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[593]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[594]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[595]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[596]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[597]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[598]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[599]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[600]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[601]);
}

rightArg = parseop_Expression_level_80{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_17_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDivisionOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_17_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_80 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'sin' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_9_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[602]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[603]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[604]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[605]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[606]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[607]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[608]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[609]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[610]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[611]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[612]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[613]);
}

arg = parseop_Expression_level_87{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSinusOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_9_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
a0 = 'cos' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_10_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[614]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[615]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[616]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[617]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[618]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[619]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[620]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[621]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[622]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[623]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[624]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[625]);
}

arg = parseop_Expression_level_87{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createCosinusOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_10_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_87{ element = arg; }
;

parseop_Expression_level_87 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
a0 = 'not' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_8_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[626]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[627]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[628]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[629]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[630]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[631]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[632]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[633]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[634]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[635]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[636]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[637]);
}

arg = parseop_Expression_level_88{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createNegativeOperationCallExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_8_0_0_2, arg, true);
copyLocalizationInfos(arg, element);
}
}
|

arg = parseop_Expression_level_88{ element = arg; }
;

parseop_Expression_level_88 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
arg = parseop_Expression_level_90(
a0 = '++' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_6_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[638]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[639]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[640]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[641]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[642]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[643]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[644]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[645]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[646]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[647]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[648]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[649]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[650]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[651]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[652]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[653]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[654]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[655]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[656]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[657]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[658]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[659]);
}

{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createIncrementOperatorExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_6_0_0_0, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
a0 = '--' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_7_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[660]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[661]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[662]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[663]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[664]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[665]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[666]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[667]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[668]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[669]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[670]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[671]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[672]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[673]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[674]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[675]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[676]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[677]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[678]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[679]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[680]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[681]);
}

{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createDecrementOperatorExpression();
startIncompleteElement(element);
}
if (arg != null) {
if (arg != null) {
Object value = arg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_7_0_0_0, arg, true);
copyLocalizationInfos(arg, element);
}
}
|
/* epsilon */ { element = arg; } 
)
;

parseop_Expression_level_90 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
leftArg = parseop_Expression_level_91((
()
{ element = null; }
a0 = '+=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_14_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[682]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[683]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[684]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[685]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[686]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[687]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[688]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[689]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[690]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[691]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[692]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[693]);
}

rightArg = parseop_Expression_level_91{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_14_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createAddToVarOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_14_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
|
()
{ element = null; }
a0 = '-=' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_15_0_0_2, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[694]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[695]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[696]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[697]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[698]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[699]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[700]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[701]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[702]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[703]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[704]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[705]);
}

rightArg = parseop_Expression_level_91{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
if (leftArg != null) {
if (leftArg != null) {
Object value = leftArg;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_15_0_0_0, leftArg, true);
copyLocalizationInfos(leftArg, element);
}
}
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory.eINSTANCE.createSubtractFromVarOperationCallExpression();
startIncompleteElement(element);
}
if (rightArg != null) {
if (rightArg != null) {
Object value = rightArg;
addObjectToList(element, org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS, value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_15_0_0_4, rightArg, true);
copyLocalizationInfos(rightArg, element);
}
}
{ leftArg = element; /* this may become an argument in the next iteration */ }
)+ | /* epsilon */ { element = leftArg; }

)
;

parseop_Expression_level_91 returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
@init{
}
:
c0 = parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression{ element = c0; /* this is a subclass or primitive expression choice */ }
|c1 = parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression{ element = c1; /* this is a subclass or primitive expression choice */ }
|c2 = parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression{ element = c2; /* this is a subclass or primitive expression choice */ }
|c3 = parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression{ element = c3; /* this is a subclass or primitive expression choice */ }
|c4 = parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression{ element = c4; /* this is a subclass or primitive expression choice */ }
|c5 = parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration{ element = c5; /* this is a subclass or primitive expression choice */ }
|c6 = parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression{ element = c6; /* this is a subclass or primitive expression choice */ }
;

parse_org_coolsoftware_coolcomponents_expressions_literals_IntegerLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression element = null]
@init{
}
:
(
a0 = ILT
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createIntegerLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("ILT");
tokenResolver.setOptions(getOptions());
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.Long resolved = (java.lang.Long) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.INTEGER_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_1_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[706]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[707]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[708]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[709]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[710]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[711]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[712]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[713]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[714]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[715]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[716]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[717]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[718]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[719]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[720]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[721]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[722]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[723]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[724]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[725]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[726]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[727]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_RealLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression element = null]
@init{
}
:
(
a0 = REAL_LITERAL
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createRealLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("REAL_LITERAL");
tokenResolver.setOptions(getOptions());
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.Double resolved = (java.lang.Double) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.REAL_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_2_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[728]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[729]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[730]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[731]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[732]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[733]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[734]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[735]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[736]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[737]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[738]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[739]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[740]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[741]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[742]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[743]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[744]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[745]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[746]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[747]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[748]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[749]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_BooleanLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression element = null]
@init{
}
:
(
(
a0 = 'true' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_3_0_0_0, true, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
// set value of boolean attribute
Object value = true;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
|a1 = 'false' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createBooleanLiteralExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_3_0_0_0, false, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
// set value of boolean attribute
Object value = false;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.BOOLEAN_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
)
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[750]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[751]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[752]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[753]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[754]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[755]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[756]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[757]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[758]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[759]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[760]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[761]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[762]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[763]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[764]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[765]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[766]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[767]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[768]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[769]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[770]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[771]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_literals_StringLiteralExpression returns [org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression element = null]
@init{
}
:
(
a0 = QUOTED_34_34
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory.eINSTANCE.createStringLiteralExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34");
tokenResolver.setOptions(getOptions());
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
java.lang.String resolved = (java.lang.String) resolvedObject;
if (resolved != null) {
Object value = resolved;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.STRING_LITERAL_EXPRESSION__VALUE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_4_0_0_0, resolved, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[772]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[773]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[774]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[775]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[776]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[777]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[778]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[779]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[780]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[781]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[782]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[783]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[784]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[785]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[786]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[787]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[788]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[789]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[790]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[791]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[792]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[793]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_ParenthesisedExpression returns [org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression element = null]
@init{
}
:
a0 = '(' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_5_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[794]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[795]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[796]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[797]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[798]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[799]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[800]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[801]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[802]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[803]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[804]);
addExpectedElement(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[805]);
}

(
a1_0 = parse_org_coolsoftware_coolcomponents_expressions_Expression{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
Object value = a1_0;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_5_0_0_2, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[806]);
}

a2 = ')' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.ExpressionsFactory.eINSTANCE.createParenthesisedExpression();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_5_0_0_4, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
}
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[807]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[808]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[809]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[810]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[811]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[812]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[813]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[814]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[815]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[816]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[817]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[818]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[819]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[820]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[821]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[822]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[823]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[824]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[825]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[826]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[827]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[828]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_variables_VariableDeclaration returns [org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration element = null]
@init{
}
:
a0 = 'var' {
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
startIncompleteElement(element);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_29_0_0_0, null, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
}
{
// expected elements (follow set)
addExpectedElement(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableDeclaration(), org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[829]);
}

(
a1_0 = parse_org_coolsoftware_coolcomponents_expressions_variables_Variable{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableDeclaration();
startIncompleteElement(element);
}
if (a1_0 != null) {
if (a1_0 != null) {
Object value = a1_0;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE), value);
completedElement(value, true);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_29_0_0_2, a1_0, true);
copyLocalizationInfos(a1_0, element);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[830]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[831]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[832]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[833]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[834]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[835]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[836]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[837]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[838]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[839]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[840]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[841]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[842]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[843]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[844]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[845]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[846]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[847]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[848]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[849]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[850]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[851]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_variables_VariableCallExpression returns [org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression element = null]
@init{
}
:
(
a0 = TEXT
{
if (terminateParsing) {
throw new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpTerminateParsingException();
}
if (element == null) {
element = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariableCallExpression();
startIncompleteElement(element);
}
if (a0 != null) {
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
tokenResolver.setOptions(getOptions());
org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result = getFreshTokenResolveResult();
tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), result);
Object resolvedObject = result.getResolvedToken();
if (resolvedObject == null) {
addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
}
String resolved = (String) resolvedObject;
org.coolsoftware.coolcomponents.expressions.variables.Variable proxy = org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory.eINSTANCE.createVariable();
collectHiddenTokens(element);
registerContextDependentProxy(new org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpContextDependentURIFragmentFactory<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableCallExpressionReferredVariableReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), resolved, proxy);
if (proxy != null) {
Object value = proxy;
element.eSet(element.eClass().getEStructuralFeature(org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.VARIABLE_CALL_EXPRESSION__REFERRED_VARIABLE), value);
completedElement(value, false);
}
collectHiddenTokens(element);
retrieveLayoutInformation(element, org.coolsoftware.coolcomponents.expressions.resource.exp.grammar.ExpGrammarInformationProvider.EXP_31_0_0_0, proxy, true);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, proxy);
}
}
)
{
// expected elements (follow set)
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[852]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[853]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[854]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[855]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[856]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[857]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[858]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[859]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[860]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[861]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[862]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[863]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[864]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[865]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[866]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[867]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[868]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[869]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[870]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[871]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[872]);
addExpectedElement(null, org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpExpectationConstants.EXPECTATIONS[873]);
}

;

parse_org_coolsoftware_coolcomponents_expressions_Expression returns [org.coolsoftware.coolcomponents.expressions.Expression element = null]
:
c = parseop_Expression_level_03{ element = c; /* this rule is an expression root */ }

;

SL_COMMENT:
( '//'(~('\n'|'\r'|'\uffff'))* )
{ _channel = 99; }
;
ML_COMMENT:
( '/*'.*'*/')
{ _channel = 99; }
;
ILT:
('-'?('0'..'9')+(('e'|'E')('-')?('0'..'9')+)?)
;
REAL_LITERAL:
('-'?('0'..'9')+(('e'|'E')('-')?('0'..'9')+)?'.''-'?('0'..'9')+(('e'|'E')('-')?('0'..'9')+)?)
;
TYPE_LITERAL:
(('Boolean' | 'Integer' | 'Real' | 'String'))
;
TEXT:
(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
;
WHITESPACE:
((' ' | '\t' | '\f'))
{ _channel = 99; }
;
LINEBREAK:
(('\r\n' | '\r' | '\n'))
{ _channel = 99; }
;
QUOTED_34_34:
(('"')(~('"'))*('"'))
;

