/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.mopp;

public class ExpSyntaxCoverageInformationProvider {
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.eclipse.emf.ecore.EClass[] {
			org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(),
			org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.eINSTANCE.getIntegerLiteralExpression(),
			org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.eINSTANCE.getRealLiteralExpression(),
			org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.eINSTANCE.getBooleanLiteralExpression(),
			org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage.eINSTANCE.getStringLiteralExpression(),
			org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getParenthesisedExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getIncrementOperatorExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDecrementOperatorExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegativeOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSinusOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getCosinusOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getFunctionExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAdditiveOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractiveOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getAddToVarOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getSubtractFromVarOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getMultiplicativeOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDivisionOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getPowerOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getGreaterThanEqualsOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getLessThanEqualsOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getEqualsOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNotEqualsOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getDisjunctionOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getConjunctionOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getImplicationOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage.eINSTANCE.getNegationOperationCallExpression(),
			org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableDeclaration(),
			org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariable(),
			org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableCallExpression(),
			org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage.eINSTANCE.getVariableAssignment(),
		};
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.eclipse.emf.ecore.EClass[] {
			org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getExpression(),
			org.coolsoftware.coolcomponents.expressions.ExpressionsPackage.eINSTANCE.getBlock(),
		};
	}
	
}
