/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.analysis;

import org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral;

public class ExpTYPE_LITERALTokenResolver
		implements
		org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolver {

	private org.coolsoftware.coolcomponents.expressions.resource.exp.analysis.ExpDefaultTokenResolver defaultTokenResolver = new org.coolsoftware.coolcomponents.expressions.resource.exp.analysis.ExpDefaultTokenResolver();

	public String deResolve(Object value,
			org.eclipse.emf.ecore.EStructuralFeature feature,
			org.eclipse.emf.ecore.EObject container) {
		String result = defaultTokenResolver.deResolve(value, feature,
				container);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.coolcomponents.expressions.expressions.resource.expression
	 * .IExpressionTokenResolver#resolve(java.lang.String,
	 * org.eclipse.emf.ecore.EStructuralFeature,
	 * org.coolsoftware.coolcomponents.
	 * expressions.expressions.resource.expression
	 * .IExpressionTokenResolveResult)
	 */
	public void resolve(
			String lexem,
			org.eclipse.emf.ecore.EStructuralFeature feature,
			org.coolsoftware.coolcomponents.expressions.resource.exp.IExpTokenResolveResult result) {
		if (lexem.equals(TypeLiteral.BOOLEAN.getName()))
			result.setResolvedToken(TypeLiteral.BOOLEAN);
		else if (lexem.equals(TypeLiteral.INTEGER.getName()))
			result.setResolvedToken(TypeLiteral.INTEGER);
		else if (lexem.equals(TypeLiteral.REAL.getName()))
			result.setResolvedToken(TypeLiteral.REAL);
		else if (lexem.equals(TypeLiteral.STRING.getName()))
			result.setResolvedToken(TypeLiteral.STRING);
		else
			result.setErrorMessage("Cannot resolve type '" + lexem + "'.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.coolcomponents.expressions.expressions.resource.expression
	 * .IExpressionConfigurable#setOptions(java.util.Map)
	 */
	public void setOptions(java.util.Map<?, ?> options) {
		defaultTokenResolver.setOptions(options);
	}
}
