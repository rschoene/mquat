/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.resource.exp;

import java.util.Collections;
import java.util.Set;

import org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResource;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.eclipse.emf.common.util.URI;

/**
 * An {@link ExpResourceWithContext} is an {@link ExpResource} that further can
 * have a context containing a {@link Set} of {@link Variable}s that are visible
 * within the {@link ExpResource}s context.
 * 
 * @author Claas Wilke
 */
public class ExpResourceWithContext extends ExpResource {

	private Set<Variable> visibleVariables = null;

	/**
	 * Creates an instance with the given URI.
	 * 
	 * @param uri
	 *            the URI.
	 */
	public ExpResourceWithContext(URI uri) {
		super(uri);
	}

	/**
	 * Returns the visible {@link Variable} set as context of this
	 * {@link ExpResource}.
	 * 
	 * @return The visible {@link Variable} set as context of this
	 *         {@link ExpResource}.
	 */
	public Set<Variable> getVisibleVariables() {
		if (visibleVariables == null)
			return Collections.emptySet();
		else
			return visibleVariables;
	}

	/**
	 * Sets the visible {@link Variable} set as context of this
	 * {@link ExpResource}.
	 * 
	 * @param visibleVariables
	 *            The visible {@link Variable} set as context of this
	 *            {@link ExpResource}.
	 */
	public void setVisibleVariables(Set<Variable> visibleVariables) {
		this.visibleVariables = visibleVariables;
	}
}
