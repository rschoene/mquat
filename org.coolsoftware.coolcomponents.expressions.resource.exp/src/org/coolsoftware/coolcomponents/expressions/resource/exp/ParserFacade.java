/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.resource.exp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpPrinter2;
import org.coolsoftware.coolcomponents.expressions.resource.exp.mopp.ExpResource;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Facade class to provide methods to parse {@link Expression}s with a given
 * context.
 * 
 * @author Claas Wilke
 */
public class ParserFacade {

	/**
	 * Parses a given {@link String} into an {@link ExpResource}. The
	 * {@link ExpResource} might contain errors if the given {@link String} does
	 * not represent a valid {@link Statement}.
	 * 
	 * <p>
	 * <strong>Note:</strong> Well-formedness rule checks are integrated via
	 * depency injection, thus, they will only be checked when executing this
	 * class as an Eclipse application.
	 * <p>
	 * 
	 * @param source
	 *            The {@link String} to be parsed.
	 * @param variableDeclarations
	 *            A {@link Set} of {@link Variable}s that are visible within the
	 *            context of the {@link Statement} to be parsed.
	 * @return The parsed {@link ExpResource}.
	 */
	public static ExpResource parseStatement(String source, Set<Variable> vars) {

		if (source == null)
			throw new IllegalArgumentException(
					"The source String to be parsed cannot be null");
		// no else.

		ResourceSet rs = new ResourceSetImpl();
		ExpResourceWithContext resource = new ExpResourceWithContext(
				URI.createFileURI("temp.exp"));

		if (vars != null)
			resource.setVisibleVariables(vars);
		// no else.

		rs.getResources().add(resource);

		try {
			resource.load(new ByteArrayInputStream(source.getBytes()), null);
			EcoreUtil.resolveAll(resource);
		}

		catch (IOException e) {
			throw new IllegalStateException(
					"Unexpected IOException during parsing of source '"
							+ source + "'.", e);
		}

		return resource;
	}

	/**
	 * Returns the printed {@link String} representation for a given
	 * {@link Statement}.
	 * 
	 * @param stmt
	 *            The {@link Statement} to be printed.
	 * @return The printext {@link Statement}.
	 */
	public static String printStatement(Statement stmt) {

		OutputStream os = new ByteArrayOutputStream();
		ExpPrinter2 printer;

		if (stmt.eResource() instanceof IExpTextResource)
			printer = new ExpPrinter2(os, (ExpResource) stmt.eResource());
		else
			printer = new ExpPrinter2(os, null);

		try {
			printer.print(stmt);
		}

		catch (IOException e) {
			throw new IllegalStateException(
					"Unexpected IOException during printing of statement '"
							+ stmt.toString() + "'.", e);
		}

		return os.toString();
	}
}
