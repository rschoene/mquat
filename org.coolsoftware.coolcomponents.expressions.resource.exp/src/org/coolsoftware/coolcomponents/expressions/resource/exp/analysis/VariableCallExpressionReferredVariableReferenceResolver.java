/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package org.coolsoftware.coolcomponents.expressions.resource.exp.analysis;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.coolsoftware.coolcomponents.expressions.Block;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.resource.exp.ExpResourceWithContext;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.eclipse.emf.ecore.EObject;

public class VariableCallExpressionReferredVariableReferenceResolver
		implements
		org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolver<org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression, org.coolsoftware.coolcomponents.expressions.variables.Variable> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.coolcomponents.expressions.expressions.resource.expression
	 * .IExpressionReferenceResolver#resolve(java.lang.String,
	 * org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference, int,
	 * boolean,
	 * org.coolsoftware.coolcomponents.expressions.expressions.resource.
	 * expression.IExpressionReferenceResolveResult)
	 */
	public void resolve(
			String identifier,
			org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression container,
			org.eclipse.emf.ecore.EReference reference,
			int position,
			boolean resolveFuzzy,
			final org.coolsoftware.coolcomponents.expressions.resource.exp.IExpReferenceResolveResult<org.coolsoftware.coolcomponents.expressions.variables.Variable> result) {

		result.setErrorMessage("Cannot find Variable " + identifier
				+ " within the scope of this expression.");

		List<Variable> definedVariables = computeReachableVariables(container,
				new HashSet<Statement>());

		/* Probably use context as well. */
		if (container.eResource() instanceof ExpResourceWithContext) {
			ExpResourceWithContext resource = (ExpResourceWithContext) container
					.eResource();
			definedVariables.addAll(resource.getVisibleVariables());
		}
		// no else.

		for (Variable var : definedVariables) {
			if (resolveFuzzy && var.getName().startsWith(identifier))
				result.addMapping(var.getName(), var);
			else if (var.getName().equals(identifier))
				result.addMapping(identifier, var);
		}
	}

	/**
	 * Computes the reachable {@link Variable}s for a given {@link Statement}.
	 * 
	 * @param stmt
	 *            The {@link Statement}.
	 * @return The reachable {@link Variable}s as a List
	 */
	private List<Variable> computeReachableVariables(Statement stmt,
			Set<Statement> visitedStatements) {

		List<Variable> result = new ArrayList<Variable>();

		if (stmt == null || visitedStatements.contains(stmt))
			return result;
		// no else.

		visitedStatements.add(stmt);

		if (stmt instanceof VariableDeclaration)
			result.add(((VariableDeclaration) stmt).getDeclaredVariable());

		else if (stmt instanceof Expression) {
			if(stmt.eContainer() == null) {
			} else
			if (stmt.eContainer() instanceof Statement) {
				result.addAll(computeReachableVariables(
						(Statement) stmt.eContainer(), visitedStatements));
			} else if (stmt
					.eContainer()
					.getClass()
					.getName()
					.equals("org.coolsoftware.ecl.impl.PropertyRequirementClauseImpl")) {
				EObject contract = stmt.eContainer().eContainer().eContainer()
						.eContainer();
				
				//ctr.getPort().getMetaparameter()
				try {
					Object p = contract.getClass()
							.getMethod("getPort", new Class[0])
							.invoke(contract, new Object[0]);
					Object o = p.getClass()
							.getMethod("getMetaparameter", new Class[0])
							.invoke(p, new Object[0]);
					int size = (Integer)(o.getClass().getMethod("size", new Class[0])
							.invoke(o, new Object[0]));
					for(int i = 0; i < size; i++) {
						Object metaparam = o.getClass().getMethod("get", int.class)
								.invoke(o, 0);
						Variable v = (Variable) metaparam;
						result.add(v);
					}
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}

			} else if (stmt
					.eContainer()
					.getClass()
					.getName()
					.equals("org.coolsoftware.ecl.impl.SWComponentRequirementClauseImpl")) {
				EObject contract = stmt.eContainer().eContainer().eContainer()
						.eContainer();
				try {
					Object p = contract.getClass()
							.getMethod("getPort", new Class[0])
							.invoke(contract, new Object[0]);
					Object o = p.getClass()
							.getMethod("getMetaparameter", new Class[0])
							.invoke(p, new Object[0]);
					int size = (Integer)(o.getClass().getMethod("size", new Class[0])
							.invoke(o, new Object[0]));
					for(int i = 0; i < size; i++) {
						Object metaparam = o.getClass().getMethod("get", int.class)
								.invoke(o, 0);
						Variable v = (Variable) metaparam;
						result.add(v);
					}
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}

			}
		 else if (stmt
				.eContainer()
				.getClass()
				.getName()
				.equals("org.coolsoftware.ecl.impl.ProvisionClauseImpl")) {
			EObject contract = stmt.eContainer().eContainer().eContainer();
			try {
				Object p = contract.getClass()
						.getMethod("getPort", new Class[0])
						.invoke(contract, new Object[0]);
				Object o = p.getClass()
						.getMethod("getMetaparameter", new Class[0])
						.invoke(p, new Object[0]);
				int size = (Integer)(o.getClass().getMethod("size", new Class[0])
						.invoke(o, new Object[0]));
				for(int i = 0; i < size; i++) {
					Object metaparam = o.getClass().getMethod("get", int.class)
							.invoke(o, 0);
					Variable v = (Variable) metaparam;
					result.add(v);
				}
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} 
		 }
		}
		else if (stmt instanceof Block) {
			for (Expression containedExp : ((Block) stmt).getExpressions()) {
				result.addAll(computeReachableVariables(containedExp,
						visitedStatements));
			}
			// end for.
		}
		

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.coolcomponents.expressions.expressions.resource.expression
	 * .IExpressionReferenceResolver#deResolve(org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference)
	 */
	public String deResolve(
			org.coolsoftware.coolcomponents.expressions.variables.Variable element,
			org.coolsoftware.coolcomponents.expressions.variables.VariableCallExpression container,
			org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.coolsoftware.coolcomponents.expressions.expressions.resource.expression
	 * .IExpressionConfigurable#setOptions(java.util.Map)
	 */
	public void setOptions(java.util.Map<?, ?> options) {
		// save options in a field or leave method empty if this resolver does
		// not depend
		// on any option
	}
}
