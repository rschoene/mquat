/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.copyFile;

import java.io.File;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.task.repository.AbstractTaskComponent;
import org.haec.theatre.utils.FileProxy;

/**
 * Abstract superclass of every CopyFile implementations providing a generic copy method.
 * @author René Schöne
 */
public abstract class AbstractCopyFile extends TaskBasedImpl {

	protected static Logger log = Logger.getLogger(AbstractCopyFile.class);

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return TranscodingServerUtil.APP_NAME;
	}
	
	/**
	 * The copy method to be invoked by subclasses.
	 * @param fp1 object, either {@link FileProxy}, {@link File} or {@link String}
	 * @return FileProxy with thisUri, or an undefined FileProxy if object has none of the above types
	 */
	protected FileProxy getFile(Object fp1) {
		log.debug("Got " + fp1 + ", which is of class " +
				(fp1 != null ? fp1.getClass().getName() : "null"));
		String name = null;
		if(fp1 instanceof FileProxy) {
			// read the name, TODO is name a full path?
			name = ((FileProxy) fp1).getName();
		}
		else if(fp1 instanceof File) {
			name = ((File) fp1).getAbsolutePath();
		}
		else if(fp1 instanceof String) {
			name = (String) fp1;
		}
		if(name != null) {
			log.debug("Using name=" + name);
			return new FileProxy(new File(name), thisUri);
		}
		// return "undefined" file-proxy
		return new FileProxy();
	}


}
