/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.benchmark;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.PropertyType;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;

/**
 * Synchronizer of similar benchmarks. Not finished nor used yet.
 * @author René Schöne
 */
public class BenchmarkSynchronizer {
	
	Logger log = Logger.getLogger(BenchmarkSynchronizer.class);
	
	/** Benchmarks has not been started, contract is not comuted yet. */
	public static final int CONTRACT_STATUS_NOT_COMPUTED = 1;
	/** One or more benchmarks are finished, contract is currently being comuted. It may be ready after computation. */
	public static final int CONTRACT_STATUS_COMPUTING = 2;
	/** One or more benchmarks are finished, contract is not finished yet (still contains some function templates). */
	public static final int CONTRACT_STATUS_COMPUTED_INCOMPLETE = 3;
	/** All benchmarks are finished, contract is completly computed, thus containing only concrete values. */
	public static final int CONTRACT_STATUS_COMPLETE = 4;
	
	public static class Contract {
		ComponentType type;
		String contract;
		int status;
		
		public Contract() {
			status = CONTRACT_STATUS_NOT_COMPUTED;
		}
	}
	
	public static class FuzzyVariantModel {
		private VariantModel vm;
		/** property - fuzzy value (0=Do not need to match, 1=Must match exactly) */
		final private Map<PropertyType, Float> fuzzyness;
		
		public FuzzyVariantModel() {
			fuzzyness = new HashMap<PropertyType, Float>();
		}

		public VariantModel getVm() {
			return vm;
		}

		public void setVm(VariantModel vm) {
			this.vm = vm;
		}

		public Map<PropertyType, Float> getFuzzyness() {
			return fuzzyness;
		}
		
		public void putFuzzyValue(PropertyType prop, float value) {
			fuzzyness.put(prop, value);
		}
	}

	/** appName - (fuzzy) variant model - contracts */
	Map<String, Map<FuzzyVariantModel, Set<Contract>>> benchmarks;
	
	public BenchmarkSynchronizer() {
		// TODO Auto-generated constructor stub
//		contracts = new HashSet<String>();
		benchmarks = new HashMap<String, Map<FuzzyVariantModel, Set<Contract>>>();
	}

	public Contract getContract(String appName, String compName) {
		//TODO stub
		return null;
	}
	
	public void putBenchData(String appName, FuzzyVariantModel vm, String compName, String benchData) {
		Map<FuzzyVariantModel, Set<Contract>> byVariantModel = benchmarks.get(appName);
		if(byVariantModel == null) {
			byVariantModel = new HashMap<BenchmarkSynchronizer.FuzzyVariantModel, Set<Contract>>();
			benchmarks.put(appName, byVariantModel);
			log.debug("Created benchmark set for app " + appName);
		}
		Set<Contract> contracts = byVariantModel.get(vm);
		if(contracts == null) {
			contracts = new HashSet<BenchmarkSynchronizer.Contract>();
			byVariantModel.put(vm, contracts);
			log.debug("Create contracts set for fuzzy variant model " + vm);
		}
		// search contracts for component with given compName
		boolean found = false;
		for(Contract c : contracts) {
			if(equalCompName(c.type, compName)) {
				updateContract(c, benchData);
				found = true;
				break;
			}
		}
		if(!found) {
			// no contract with given name
		}
	}

	private boolean equalCompName(ComponentType type, String compName) {
		return type.getName().equals(compName);
	}

	private void updateContract(Contract c, String benchData) {
		// TODO Auto-generated method stub
		
	}
	
}
