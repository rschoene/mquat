/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.benchmark;

import java.io.InputStream;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * BenchmarkUtil delegating the work to a proxy running at the GEM.
 * @author René Schöne
 */
public class BenchmarkUtilProxy implements IBenchmarkUtil {
	
	Logger log = Logger.getLogger(BenchmarkUtilProxy.class);
	
//	private final String methodSignatureIsEnergyMeasurementSupported =
//			"isEnergyMeasurementSupported()boolean;";
//	private final String methodSignatureGetExpressionsForFunctionTemplate =
//			"getExpressionsForFunctionTemplate(Ljava/io/File;org/haec/theatre/benchmark/LocalBenchmarkUtil/TemplateNfpName;"
//			+ "Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Set;)Lorg/coolsoftware/coolcomponents/expressions/Statement;";
//	private final String methodSignatureSetUseEureqa = "setUseEureqa(Ljava/lang/Boolean;);";
//	private final String methodSignatureIsUseEureqa = "isUseEureqa()Ljava/lang/Boolean;";
//	private final Object[] EMPTY_ARGS = new Object[0];
	
	private IBenchmarkUtil ibu;

	private TheatreSettings settings;

	public BenchmarkUtilProxy() {
	}
	
	@Override
	public boolean isEnergyMeasurementSupported() {
//		return (Boolean) syncExecute(methodSignatureIsEnergyMeasurementSupported, EMPTY_ARGS);
		return ibu.isEnergyMeasurementSupported();
	}

	@Override
	public String getExpressionsForFunctionTemplate(InputStream benchData, String nfpName, int index, int nrOfNfps,
			Collection<String> metaparams) {
//		return (Statement) syncExecute(methodSignatureGetExpressionsForFunctionTemplate,
//				new Object[]{benchData,template,index,nrOfNfps,metaparams});
		log.debug("("+nfpName+","+index+","+nrOfNfps+")");
		return ibu.getExpressionsForFunctionTemplate(benchData, nfpName, index, nrOfNfps, metaparams);
	}

	@Override
	public void setUseEureqa(boolean useEureqa) {
//		syncExecute(methodSignatureSetUseEureqa, new Object[]{useEureqa});
		ibu.setUseEureqa(useEureqa);
	}

	@Override
	public boolean isUseEureqa() {
//		return (Boolean) syncExecute(methodSignatureIsUseEureqa, EMPTY_ARGS);
		return ibu.isUseEureqa();
	}
	
	protected void setTheatreSettings(TheatreSettings s) {
		this.settings = s;
	}
	
	protected void unsetTheatreSettings(TheatreSettings s) {
		this.settings = null;
	}
	
	protected void activate(ComponentContext ctx) {
		String uri = settings.getGemUri().get();
		log.debug("New benchmark util proxy to " + uri);
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(ctx.getBundleContext());
		ibu = RemoteOsgiUtil.getRemoteService(remote, uri, IBenchmarkUtil.class);
	}
	
//	private static class Result {
//		Object result;
//		public void setResult(Object result) {
//			this.result = result;
//		}
//		public Object getResult() {
//			return result;
//		}
//	}
//	
//	/**
//	 * Executes a method synchronously, remotely. Thus, this method will wait for the result.
//	 * @param result an object to store the result it, mainly to get result class.
//	 *  May be <code>null</code>, if the method has <i>void</i> as return type or result is simply not needed.
//	 * @param methodSignature the signature of the method to execute
//	 * @param args the arguments to be passed to the method
//	 * @return the result of the method invocation as {@link Object}
//	 */
//	private Object syncExecute(String methodSignature, final Object[] args) {
//		final Result result = new Result();
//		System.out.println("#!#!# "+uri+"."+methodSignature);
//		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(Activator.getContext());
//		final RemoteServiceReference[] srefs = remote
//				.getRemoteServiceReferences(new ch.ethz.iks.r_osgi.URI(uri), IBenchmarkUtil.class.getName(), null);
//		// do a synchronous remote call, i.e. wait for the result
//		final Lock lock = new ReentrantLock();
//		final Condition finished = lock.newCondition();
//		lock.lock();
//		remote.asyncRemoteCall(srefs[0].getURI(), methodSignature,
//				args, new AsyncRemoteCallCallback() {
////					@SuppressWarnings("unchecked")
//					@Override
//					public void remoteCallResult(boolean success, Object o) {
//						System.out.println("[BenchUtilProxy] finished executing (success=" + success + ")");
//						lock.lock();
//						if(result != null) {
//							result.setResult(o);
//						}
//						finished.signal();
//						lock.unlock();
//						//System.err.println("Returned Object: " + o);
//					}
//				});
//		finished.awaitUninterruptibly();
//		lock.unlock();
//		return result.getResult();
//	}

}
