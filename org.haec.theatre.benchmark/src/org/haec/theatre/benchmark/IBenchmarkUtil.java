/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.benchmark;

import java.io.InputStream;
import java.util.Collection;

/**
 * Utility class for calculating formulas given benchmark results.
 * @author René Schöne
 */
public interface IBenchmarkUtil {
	
	String PROPERTY_TYPE = "type";
	String TYPE_LOCAL = "local";
	String TYPE_PROXY = "proxy";

	public abstract boolean isEnergyMeasurementSupported();

	/**
	 * Uses the benchmark data to compute an expression for the given template.
	 * @param benchData stream providing benchmark results
	 * @param nfpName the name of the template to evaluate
	 * @param index index of nfp
	 * @param nrOfNfps the number of nfp's
	 * @param metaparams the meta parameter names
	 * @return the expression as a string, or <code>null</code> on failure
	 */
	public abstract String getExpressionsForFunctionTemplate(InputStream benchData, String nfpName, int index, int nrOfNfps,
			Collection<String> metaparams);

	public abstract void setUseEureqa(boolean useEureqa);

	public abstract boolean isUseEureqa();

}
