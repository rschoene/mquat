/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.benchmark;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Analyzer of benchmark result.
 * @author Sebastian Götz
 */
public class Analyzer {
	
	private List<Tuple> tuples;
	
	private class Tuple {
		private List<Object> cells;
		public Tuple() {
			cells = new ArrayList<Object>();
		}
//		public void setCell(int i, Object value) {
//			cells.set(i, value);
//		}
		public void addCell(Object value) {
			cells.add(value);
		}
		public Object getCell(int i) {
			return cells.get(i);
		}
		public int getSize() {
			return cells.size();
		}
		
		@Override
		public String toString() {
			String ret = "";
			for(Object v : cells) {
				ret += v+";";
			}
			if(ret.length() > 0)
				ret = ret.substring(0,ret.length()-1);
			return ret;
		}
	}
	
	private Analyzer(Reader r) {
		tuples = new ArrayList<Analyzer.Tuple>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(r);
			String line;
			while((line = br.readLine()) != null) {
				Tuple t = new Tuple();
//				int i = 0;
				for(String cell : line.split(";")) {
					t.addCell(cell);
				}
				tuples.add(t);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(br != null) { try { br.close(); } catch(IOException ignore) {} }
		}
	}
	
	public Analyzer(File data) throws FileNotFoundException {
		this(new FileReader(data));
	}
	
	public Analyzer(InputStream benchData) {
		this(new InputStreamReader(benchData));
	}

	public Map<String,Double> sumUpColumn(int col, String filter, int nrOfNfps) {
		Map<String,Double> groupedSum = new HashMap<String, Double>();
		for(Tuple t : tuples) {
			boolean matchedFilter = false;
			for (int i = 0; i < nrOfNfps; i++) {
				matchedFilter |= t.getCell(2*i).toString().equals(filter);
			}
			if(!matchedFilter) continue;
			Tuple remainder = new Tuple();
			for(int i = 2*nrOfNfps; i < t.getSize(); i++) {
//				if(i != col) {
					remainder.addCell(t.getCell(i));
				{}
			}
			Double old = 0.0;
			if(groupedSum.get(remainder.toString()) != null) {
				old = groupedSum.get(remainder.toString());
			}
			old += Double.parseDouble(t.getCell(col).toString());
			if(old != 0) {
				old += Double.parseDouble(t.getCell(col).toString());
				old /= 2;
			}
			groupedSum.put(remainder.toString(),old);
		}
		
		return groupedSum;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		Analyzer ana = new Analyzer(new File("C:/dev/workspaces/runtime-coolsw/Quicksort.bench"));
		Map<String, Double> merger = ana.sumUpColumn(1,"wall_time",1);
		for(String fix : merger.keySet()) {
			System.out.println(fix+"\t"+merger.get(fix));
		}
	}
}
