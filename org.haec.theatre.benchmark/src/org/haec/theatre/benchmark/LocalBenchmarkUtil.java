/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.benchmark;

//Keep the imports to avoid ClassNotFoundException
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;
import org.haec.theatre.api.Monitor;
import org.haec.theatre.monitor.MonitorRepository;
import org.haec.theatre.settings.TheatreSettings;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentConstants;
import org.osgi.service.component.ComponentContext;

import rcaller.RCaller;
import rcaller.RCode;
import rcaller.exception.RCallerExecutionException;
import rcaller.exception.RCallerParseException;

/**
 * BenchmarkUtil actually computing the formulas either using Eureqa or GNU Rscript.
 * @author Sebastian Götz
 * @author René Schöne
 */
public class LocalBenchmarkUtil implements IBenchmarkUtil {
	
	private static Logger log = Logger.getLogger(LocalBenchmarkUtil.class);
	
	private boolean useEureqa = false;
	private ExpressionCalculator expCalculator;
	private Boolean energyMeasurementSupported = null;

	private TheatreSettings settings;

	public LocalBenchmarkUtil() {
		updateExpCalculator();
	}

	private void updateExpCalculator() {
		if(useEureqa && !(expCalculator instanceof EureqaExpressionCalculator)) {
			expCalculator = new EureqaExpressionCalculator();
		}
		if(!useEureqa && !(expCalculator instanceof RscriptExpressionCalculator)) {
			expCalculator = new RscriptExpressionCalculator();
		}
	}
	
	@Override
	public boolean isUseEureqa() {
		return useEureqa;
	}
	
	@Override
	public void setUseEureqa(boolean useEureqa) {
		this.useEureqa = useEureqa;
		updateExpCalculator();
	}
	
	abstract class ExpressionCalculator {
		abstract public String calculate(File benchData, String nfpName,
				String metaparamsAsString, double fixed_val);
	}
	
	class EureqaExpressionCalculator extends ExpressionCalculator {

		@Override
		public String calculate(File benchData, String nfpName,
				String metaparamsAsString, double fixed_val) {
			// start eureqa server if not already running
			String expression = null;
			try {
				ensureServerIsRunning();

				log.debug("f(" + metaparamsAsString + ")=" + nfpName);
				Process eureqa_client = new ProcessBuilder("eureqa_client.exe",
						benchData.getAbsolutePath(), "f(" + metaparamsAsString
								+ ")=" + nfpName).start();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						eureqa_client.getInputStream()));
				String line;
				while ((line = br.readLine()) != null) {
					expression = line;
					break;
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
				// expression retains its value
			}
			return expression;
		}

		private void ensureServerIsRunning() throws IOException {
			ProcessBuilder b = new ProcessBuilder("tasklist");
			Process tasklist = b.start();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					tasklist.getInputStream()));
			String line;
			boolean found = false;
			while ((line = br.readLine()) != null) {
				if (line.startsWith("eureqa_server.exe"))
					found = true;
			}
			br.close();
			tasklist.destroy();
//			Process eureqa_server = null;
			if (!found) {
//				eureqa_server = 
				new ProcessBuilder("eureqa_server.exe").start();
				log.info("eureqa server started");
			}
		}
		
	}
	
	class RscriptExpressionCalculator extends ExpressionCalculator {

		private class RegressionModel {
			private double[] coef;
			private double r2;
			
			public RegressionModel() {
			}
		
			public double[] getCoef() {
				return coef;
			}
		
			public void setCoef(double[] coef) {
				this.coef = coef;
			}
		
			public double getR2() {
				return r2;
			}
		
			public void setR2(double r2) {
				this.r2 = r2;
			}
		}

		private RegressionModel linearRegression(File benchData, String nfpName, String metaparamsAsString, int level) {
			RCaller caller = new RCaller();
		    caller.setRscriptExecutable(settings.getRExe().get().replaceAll("%20", " "));
		    RCode code = new RCode();
		    code.clear();
		
		    code.addRCode("benchData <- read.csv(\""+benchData.getAbsolutePath().replaceAll("\\\\", "/")+"\",sep=\"\\t\")");
		    code.addRCode("attach(benchData)");
		    if(level == 1) {
		    	code.addRCode("fit <- nls("+nfpName+" ~ a + b * "+metaparamsAsString+
		    			", lower=c(a = 0, b = 0),start=c(a=0,b=0), alg=\"port\")");
		    	//code.addRCode("fit <- lm(benchData$"+nfpName+" ~ benchata$"+metaparamsAsString+")");
		    } else {
		    	code.addRCode("if(dim(benchData)[1]<="+level+") { r.squared <- 0; x <- lm(1~1); "
		    			+ "stop(\"Not enough data to compute function\", dim(benchData), "+level+") }");
		    	//String str = "fit <- lm(benchData$"+nfpName+" ~ benchData$"+metaparamsAsString;
		    	String str = "fit <- nls("+nfpName+" ~ a + b1 * "+metaparamsAsString;
		    	for(int i = 2; i <= level; i++) {
		    		str += " + b"+i+" * I("+metaparamsAsString+"^"+i+")";
		    	}
		    	str += ",lower=c(b1=0";
		    	for(int i = 2; i <= level; i++) {
		    		str += ",b"+i+"=0";
		    	}
		    	str+=",a=0),start=c(b1=0";
		    	for(int i = 2; i <= level; i++) {
		    		str += ",b"+i+"=0";
		    	}	
		    	str += ",a=0),alg=\"port\")";
		    	code.addRCode(str);
		    }
		    code.addRCode("RSS<-sum(residuals(fit)^2)");
		    code.addRCode("TSS<-sum(("+nfpName+"-mean("+nfpName+"))^2)");
		    code.addRCode("r.squared<-1-(RSS/TSS)");
		    code.addRCode("x <- summary(fit)");
		    
		    caller.setRCode(code);
		    RegressionModel rm = new RegressionModel();;
			try {
				caller.runAndReturnResult("x");
				rm.setCoef(caller.getParser().getAsDoubleArray("coefficients"));
				caller.runAndReturnResult("r.squared");
				rm.setR2(caller.getParser().getAsDoubleArray("r.squared")[0]);
			} catch (RCallerExecutionException | RCallerParseException e) {
				log.error("Problems executing R code", e);
				rm.setCoef(new double[]{-1});
				rm.setR2(0.0);
			}
		    return rm;
		}

		@Override
		public String calculate(File benchData, String nfpName,
				String metaparamsAsString, double fixed_val) {
			String expression = null;
			if(!metaparamsAsString.trim().isEmpty()) {
				//use R via RCaller
				int level = 1;
				double r2 = 0; //search for a polynom of grade "level" with a precision of at least 70%
				while(r2 < 0.7 && level <= 5) {
					RegressionModel rm = linearRegression(benchData, nfpName, metaparamsAsString, level);
					//System.out.println(result.getXMLFileAsString());
		            double[] coef = rm.getCoef();
		            //r2 = result.getAsDoubleArray("r_squared")[0];
		            r2 = rm.getR2();
		            
		            expression = "f ("+metaparamsAsString+") = "+coef[0];
		            for(int coefIndex = 1; coefIndex <= level; coefIndex++) {
		            	if(coef[coefIndex] != 0)
		            		expression += " + "+coef[coefIndex]+"*("+metaparamsAsString+
		            			(coefIndex > 1 ? ("^"+coefIndex) : "")+")";
		            }
		            log.debug(nfpName+" = " + expression+" [R^2: "+r2+"]");		            
		            level++;
				}
			} else {
				log.debug("Metaparams is empty, using fixed value.");
				expression = String.format(Locale.US, "%f", fixed_val);
			}
			return expression;
		}
	}

	@Override
	public String getExpressionsForFunctionTemplate(InputStream benchData, String nfpName, int index, int nrOfNfps,
			Collection<String> metaparams) {
		log.debug("("+nfpName+","+index+","+nrOfNfps+")");
		if(nfpName.equals("cpu_energy") && !isEnergyMeasurementSupported()) {
			return "-1";
		}
		
		Analyzer analyzer = new Analyzer(benchData);
		
		String expression = "";
		double fixed_val = 0; //used if no metaparams are given (i.e., nfp does not depend on user input, only on server)
		try {
			// use a new file to write into
			//benchData.getCanonicalPath() + "_" + 
			File nfpData = File.createTempFile(nfpName, null);
			log.debug("Created tmp file: " + nfpData.getAbsolutePath());
			BufferedWriter bw = new BufferedWriter(new FileWriter(nfpData, false));

			String metaparamsAsString = " ";

			//XXX rs: implement analyzer for multiple mpv
			Map<String, Double> data = analyzer.sumUpColumn(2*index + 1, nfpName, nrOfNfps);
			if(metaparams.size() > 0) {
				Iterator<String> it = metaparams.iterator();
				while (it.hasNext()) {
					String name = it.next();
					bw.write(name + "\t");
					metaparamsAsString += name + ",";
				}
			}
			bw.write(nfpName + "\n");
			for (String str : data.keySet()) {
				bw.write(str + "\t" + data.get(str) + "\n");
				fixed_val = data.get(str);
			}
			bw.flush();
			bw.close();

			if(metaparams.size() > 0) {
				metaparamsAsString = metaparamsAsString.substring(0,
						metaparamsAsString.length() - 1);
				metaparamsAsString = metaparamsAsString.trim();
			}
			
			expression = expCalculator.calculate(nfpData, nfpName, metaparamsAsString, fixed_val);

		} catch (IOException e) {
			e.printStackTrace();
		}

		log.debug(expression);
		return expression;
	}
	
	@Override
	public boolean isEnergyMeasurementSupported() {
		if(energyMeasurementSupported == null) {
			// test once
			ServiceReference<MonitorRepository> ref = Activator.getContext().getServiceReference(MonitorRepository.class);
			if(ref == null) {
				log.warn("Monitor-Repository unavailable");
				return false;
			}
			MonitorRepository repo = Activator.getContext().getService(ref);
			Monitor toTest = repo.getMonitor("cpu_energy");
			if(toTest == null) {
				energyMeasurementSupported = Boolean.FALSE;
			}
			else {
				try {
					toTest.collectBefore();
					// random computation instead of Thread.sleep()
					Math.abs(new Random().nextGaussian() * new Random().nextGaussian());
					toTest.collectAfter(null, null);
					toTest.compute();
					// no problems -> assume monitor is working properly
					energyMeasurementSupported = Boolean.TRUE;
				}
				catch(Exception e) {
					// problems occured, so no support
					energyMeasurementSupported = Boolean.FALSE;
				}
			}
			log.debug("EnergyMeasurmentSupported: " + energyMeasurementSupported.booleanValue());
		}
		return energyMeasurementSupported.booleanValue();
	}

	
	protected void setTheatreSettings(TheatreSettings s) {
		this.settings = s;
	}
	
	protected void unsetTheatreSettings(TheatreSettings s) {
		this.settings = null;
	}
	
	protected void activate(ComponentContext ctx) {
		if(!settings.isMaster().get() && settings.getLemContractsAtGem().get()) {
			// slaves computing contracts at master don't need local bu
			ctx.disableComponent((String) ctx.getProperties().get(ComponentConstants.COMPONENT_NAME));
		}
	}

}
