/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.notifier.database;

import org.coolsoftware.theatre.energymanager.IJobPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.IJobResultNotifier;
import org.coolsoftware.theatre.energymanager.ITaskPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.ITaskResultNotifier;
import org.coolsoftware.theatre.energymanager.JobDescription;
import org.coolsoftware.theatre.energymanager.JobResultDescription;
import org.coolsoftware.theatre.energymanager.TaskDescription;
import org.coolsoftware.theatre.energymanager.TaskResultDescription;
import org.haec.theatre.dao.ImplementationDAO;
import org.haec.theatre.dao.JobDAO;
import org.haec.theatre.dao.JobInfo;
import org.haec.theatre.dao.TaskDAO;
import org.haec.theatre.dao.TaskInfo;

/**
 * Notifier taking care of updating the database.
 * @author René Schöne
 */
public class DatabaseNotifier implements IJobPreExecuteNotifier, IJobResultNotifier,
ITaskPreExecuteNotifier, ITaskResultNotifier {

	private JobDAO jobDAO;
	private TaskDAO taskDAO;
	private ImplementationDAO implementationDAO;

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.notifier.ITaskResultNotifier#afterTaskFinish(org.coolsoftware.theatre.notifier.TaskResultDescription)
	 */
	@Override
	public void afterTaskFinish(TaskResultDescription trd) {
		String status = trd.success ? TaskInfo.STATUS_FINISHED : TaskInfo.STATUS_FAILED;
		taskDAO.updateTask(trd.taskId, status);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.notifier.ITaskPreExecuteNotifier#beforeTaskExecute(org.coolsoftware.theatre.notifier.TaskDescription)
	 */
	@Override
	public void beforeTaskExecute(TaskDescription td) {
		taskDAO.updateTask(td.taskId, TaskInfo.STATUS_STARTED);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.notifier.IJobResultNotifier#afterJobFinish(org.coolsoftware.theatre.notifier.JobResultDescription)
	 */
	@Override
	public void afterJobFinish(JobResultDescription jrd) {
		String status = jrd.success ? JobInfo.STATUS_FINISHED : JobInfo.STATUS_FAILED;
		jobDAO.updateJob(jrd.jobId, null, null, null, null, null, jrd.endTime, status, null);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.notifier.IJobPreExecuteNotifier#beforeJobExecute(org.coolsoftware.theatre.notifier.JobDescription)
	 */
	@Override
	public void beforeJobExecute(JobDescription jd) {
		long implId = implementationDAO.getOrCreateImplementation(jd.implName);
		jobDAO.updateJob(jd.jobId, jd.taskId, implId, jd.containerName, jd.startTime,
				jd.predicted, null, JobInfo.STATUS_STARTED, null);
	}
	
	public void setJobDAO(JobDAO jd) {
		this.jobDAO = jd;
	}
	
	public void unsetJobDAO(JobDAO jd) {
		this.jobDAO = null;
	}

	public void setTaskDAO(TaskDAO td) {
		this.taskDAO = td;
	}

	public void unsetTaskDAO(TaskDAO td) {
		this.taskDAO = null;
	}

	public void setImplementationDAO(ImplementationDAO id) {
		this.implementationDAO = id;
	}

	public void unsetImplementationDAO(ImplementationDAO id) {
		this.implementationDAO = null;
	}

}
