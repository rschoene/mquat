/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.transcoder;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.utils.FileProxy;
import org.haec.videoprovider.IVideoProviderFactory;
import org.haec.videoprovider.VideoProvider;
import org.haec.videos.Video;

class TranscodingData implements BenchmarkData {

	FileProxy in;
	int mvl;

	public TranscodingData(FileProxy in, int mvl) {
		this.in = in;
		this.mvl = mvl;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mvl;
	}
	
}

class BenchmarkTranscoderTranscode extends TaskBasedImpl implements TranscoderTranscode {
	
	private AbstractTranscoder delegatee;

	public BenchmarkTranscoderTranscode(AbstractTranscoder delegatee) {
		this.delegatee = delegatee;
	}
	/* (non-Javadoc)
	 * @see org.haec.app.videotranscodingserver.TranscoderTranscode#transcode(java.lang.Object, java.lang.String, java.lang.String)
	 */
	@Override
	public FileProxy transcode(Object in, String outputVideoCodec,
			String outputAudioCodec) {
		File f = ((FileProxy) in).getFile();
		return wrap(delegatee.transcode(outputVideoCodec, outputAudioCodec, f));
	}
	public String getApplicationName() { return null; }
	public String getComponentType() { return null; }
	public String getVariantName() { return null; }
	public Benchmark getBenchmark() { return null; }
	public String getPluginName() { return null; }
}

/**
 * Abstract superclass of all Transcoder implementations providing a generic benchmark.
 * @author René Schöne
 */
public class AbstractTranscoderBenchmark implements Benchmark {

	public int MAX_BENCHMARK_COUNT = Integer.getInteger(TranscodingServerUtil.MAX_BENCH_TRANSCODING, 4);

	private static List<String> vCodecs = TranscodingServerUtil.getSupportedVideoCodecs();
	private static List<String> aCodecs = TranscodingServerUtil.getSupportedAudioCodecs();
	protected static Logger log = Logger.getLogger(AbstractTranscoderBenchmark.class);

	private TranscoderTranscode comp;
	FileProxy in;
	String outputVideoCodec;
	String outputAudioCodec;

	private IVideoProviderFactory fac;
	
	public AbstractTranscoderBenchmark(AbstractTranscoder comp, IVideoProviderFactory fac) {
		this.comp = new BenchmarkTranscoderTranscode(comp);
		this.fac = fac;
	}
	
	/**
	 * Default impl using first video of {@link VideoProvider#getOneVideoOfEachLength()}
	 * @return the list of videos to use
	 */
	protected List<Video> getList() {
		return fac.getCurrentVideoProvider().getOneVideoOfEachLength();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		List<BenchmarkData> result = new ArrayList<>();
		log.info("Starting");
		int benchCountRemaining = MAX_BENCHMARK_COUNT > 0 ? MAX_BENCHMARK_COUNT : Integer.MAX_VALUE;
		List<Video> list = getList();
		log.debug("Going to benchmark " + list.size() + " videos, but at most " + benchCountRemaining);
		// choose codecs randomly
		outputVideoCodec = vCodecs.get(0);
		outputAudioCodec = aCodecs.get(0);
		Collections.sort(list, new Comparator<Video>() {

			@Override
			public int compare(Video o1, Video o2) {
				return Integer.compare(o1.length, o2.length);
			}
		});
		for(Video video : list) {
			in = new FileProxy(video.file);
			int mpValue = TranscodingServerUtil.getMetaparameterValue(video.length, video.resX, video.resY);
			if(benchCountRemaining-- <= 0) { break; }
			result.add(new TranscodingData(in, mpValue));
		}
		log.info("[TRA Bench] Finished");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		TranscodingData d = (TranscodingData) data;
		log.debug("video.fileName=" + d.in.getFile().getName() + " -vc " + outputVideoCodec + " -ac " + outputAudioCodec);
		return comp.transcode(d.in, outputVideoCodec, outputAudioCodec);
	}


}
