/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.transcoder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.apps.util.VideoUtil;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.FileUtils;
import org.haec.videoprovider.IVideoProviderFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 * Abstract superclass of all Transcoder implementations providing a generic scale method.
 * @author René Schöne
 */
public abstract class AbstractTranscoder extends TaskBasedImpl implements TranscoderTranscode {

	protected final VideoUtil util;
	protected static Logger log = Logger.getLogger(AbstractTranscoder.class);
	
	protected final int defaultAudioBitrate = 128000;
	protected IVideoProviderFactory fac;
	
	/**
	 * Creates a new abstract scaling component
	 * @param util instance of util object to create processes
	 */
	public AbstractTranscoder(VideoUtil util) {
		this.util = util;
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return TranscodingServerUtil.APP_NAME;
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "Transcoder";
	}
	
	@Override
	public FileProxy transcode(Object fp1, String outputVideoCodec, String outputAudioCodec) {
		// this could actually be generated or handled by the framework
		FileProxy fp = invoke(FileProxy.class, "CopyTranscoderFile1", "getFile", fp1);
		File fileVideo = fp.getFileFor(thisUri);
		return wrap(transcode(outputVideoCodec, outputAudioCodec, fileVideo));
	}
	
	/** Provides template method for transcoding. Should not be overridden by subclasses. */
	protected File transcode(String outputVideoCodec, String outputAudioCodec, File fileVideo) {
		ErrorState state = util.createErrorState();
		if(util.shouldPass()) {
			log.debug("Pass. Returning " + fileVideo);
			return fileVideo;
		}
		if(outputVideoCodec == null && outputAudioCodec == null) {
			log.warn("Both video and audio codec were null. Exiting.");
		}
		File out;
		try {
			out = util.getTemporaryFile(getDefaultContainerForVideoCodec(fileVideo, outputVideoCodec));
		} catch (IOException e) {
			log.warn("Could not create temporary output. Exiting.", e);
			return fileVideo;
		}
		boolean doTranscode = true;
		while(doTranscode) {
			List<String> params = new ArrayList<String>();
			addArguments(params, fileVideo, outputVideoCodec, outputAudioCodec, out, state);
			try {
				int exitValue = handleProcess(util.createProcess(params));
				doTranscode = handleExitValue(exitValue, state);
			} catch (IOException e) {
				log.warn("Could not create process. Exiting.", e);
				doTranscode = false;
			} catch (InterruptedException e) {
				log.warn("Process was interrupted. Exiting.", e);
				doTranscode = false;
			}
		}
		// return result of temporary output
		log.debug("Return value: " + out);
		return out;
	}

	/**
	 * Searches for a service implementing {@link VideoResolutionCalculator}.
	 * Subclasses may override it to use a special calculator.
	 */
	protected VideoResolutionCalculator getResolutionCalculator() {
		BundleContext context = Activator.getContext();
		if(context == null) return null;
		ServiceReference<VideoResolutionCalculator> sref = context.getServiceReference(
				VideoResolutionCalculator.class);
		if(sref == null) return null;
		return context.getService(sref);
	}
	
	/**
	 * Analyse the exitvalue of the process and decide whether to start another
	 * process (with other parameters).
	 * Default implementation returns always <code>false</code>. Subclass may override it
	 * for different behaviour.
	 * @param exitValue the exit value of the formerly started process
	 * @param state state for handling multiple attempts
	 * @return <code>true</code> to start another process, <code>false</code> to return the result
	 */
	protected boolean handleExitValue(int exitValue, ErrorState state) {
		if(exitValue != 0) {
			log.warn("Got exitValue of " + exitValue);
		}
		return false;
	}
	
	/**
	 * @param file the input file, used only, if the given video codec is not in the list of
	 * default codecs, i.e. if there is no special extension required
	 * @param outputVideoCodec the given video codec
	 * @return the default container to be used for the given video codec, or the same container as
	 * the input file, if the given video codec does not require a special extension
	 */
	protected String getDefaultContainerForVideoCodec(File file, String outputVideoCodec) {
		//TODO
		switch(outputVideoCodec) {
		case "h264": return "mp4";
		}
		String ext = FileUtils.splitExt(file).extension;
		switch(ext) {
		case "": return "mp4";
		default: return ext;
		}
	}

	/**
	 * Given a started process, handles the processing, i.e. read output or
	 * wait for the process to terminate.
	 * @param startedProcess the started process
	 * @return the exitvalue of the process
	 * @throws InterruptedException if an Error occurs while waiting
	 */
	protected abstract int handleProcess(Process startedProcess) throws InterruptedException;

	/**
	 * Fills the list of parameters
	 * @param params a list of parameters, initially empty but never <code>null</code>
	 * @param fileVideo the video file to process
	 * @param outputVideoCodec the new video codec, may be <code>null</code> if outputAudioCodec is not
	 * @param outputAudioCodec the new audio codec, may be <code>null</code> if outputVideoCodec is not
	 * @param out the output file
	 * @param state state for handling multiple attempts
	 */
	protected abstract void addArguments(List<String> params, File fileVideo, String outputVideoCodec,
			String outputAudioCodec, File out, ErrorState state);

	protected void addOrModify(List<String> params, String key, String value,
			String delim) {
		int i = params.indexOf(key);
		if(i != -1) {
			// append to already existing options
			String options = params.get(i+1);
			options += delim + value;
			params.set(i+1, options);
		}
		else {
			params.add(key);
			params.add(value);
		}
	}
	
	protected void setVideoProviderFactory(IVideoProviderFactory fac) {
		this.fac = fac;
	}

}
