/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.resourcemanager;

import org.coolsoftware.theatre.Resettable;

/**
 * A local resource manager (LRM) is resonsible for
 * <ul>
 * <li> monitoring the resource layer for the container it is running on </li>
 * <li> reporting monitored information to the GRM </li>
 * </ul>
 * @author Sebastian Götz
 */
public interface ILocalResourceManager extends Resettable {

	void shutdown();

}
