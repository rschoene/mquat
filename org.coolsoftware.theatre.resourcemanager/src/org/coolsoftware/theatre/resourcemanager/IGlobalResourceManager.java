/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.resourcemanager;

import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.theatre.Resettable;

/**
 * A global resource manager (GRM) is resonsible to 
 * <ul>
 * <li> monitor the resource layer </li>
 * <li> provide access to the resource layer </li>
 * <li> manage all local resource managers</li>
 * </ul>
 * @author Sebastian Götz
 * @author René Schöne
 */
public interface IGlobalResourceManager extends Resettable {
	/**
	 * Greets the user.
	 */
	public void sayHello();

	/**
	 * @return VariantModel as String (serialized) of infrastructure
	 */
	public String getInfrastructure();

	/**
	 * @return VariantModel as Model of infrastructure
	 */
	public VariantModel getInfrastructureAsModel();

	/**
	 * 
	 * @return StructuralModel as String (serialized)
	 */
	public String getInfrastructureTypes();

	/**
	 * A new resource (server-level) has been added.
	 * @param resource the serialized variant model of the resource.
	 */
	public void registerResource(String resource);

	/**
	 * An existing resource (server-level) was removed.
	 * @param name the name of the resource
	 */
	public void unregisterResource(String name);
	
	/**
	 * The value of a property has changed.
	 * @param value the new value
	 * @param serverName the IP of the server
	 * @param rname the name of the resource
	 * @param pname the name of the property
	 * @return whether a change was made in the variant model
	 */
	public boolean notifyPropertyChanged(double value, String ip, String rname, String pname, ResourceType type);
	
	/**
	 * Shuts the GRM down.
	 */
	public void shutdown();
	
	/**
	 * Updates the local model with the changes found in the given model.
	 * <i>Currently only parses job lists.</i>
	 * @param serializeModel the variant model in serialized form
	 */
	public void updateWithModel(String serializeModel);

	/**
	 * @param name
	 * @return
	 */
	public ResourceType getType(String name);
}
