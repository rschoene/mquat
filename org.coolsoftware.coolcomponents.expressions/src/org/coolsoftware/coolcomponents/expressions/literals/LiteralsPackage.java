/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.literals;

import org.coolsoftware.coolcomponents.expressions.ExpressionsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory
 * @model kind="package"
 * @generated
 */
public interface LiteralsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "literals";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.cool-software.org/dimm/expressions/literals";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "literals";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LiteralsPackage eINSTANCE = org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getLiteralExpression()
	 * @generated
	 */
	int LITERAL_EXPRESSION = 0;

	/**
	 * The number of structural features of the '<em>Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION_FEATURE_COUNT = ExpressionsPackage.EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.BooleanLiteralExpressionImpl <em>Boolean Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.BooleanLiteralExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getBooleanLiteralExpression()
	 * @generated
	 */
	int BOOLEAN_LITERAL_EXPRESSION = 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_LITERAL_EXPRESSION__VALUE = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_LITERAL_EXPRESSION_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.IntegerLiteralExpressionImpl <em>Integer Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.IntegerLiteralExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getIntegerLiteralExpression()
	 * @generated
	 */
	int INTEGER_LITERAL_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LITERAL_EXPRESSION__VALUE = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_LITERAL_EXPRESSION_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.RealLiteralExpressionImpl <em>Real Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.RealLiteralExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getRealLiteralExpression()
	 * @generated
	 */
	int REAL_LITERAL_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_LITERAL_EXPRESSION__VALUE = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Real Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_LITERAL_EXPRESSION_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.StringLiteralExpressionImpl <em>String Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.StringLiteralExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getStringLiteralExpression()
	 * @generated
	 */
	int STRING_LITERAL_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LITERAL_EXPRESSION__VALUE = LITERAL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LITERAL_EXPRESSION_FEATURE_COUNT = LITERAL_EXPRESSION_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.literals.LiteralExpression <em>Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.LiteralExpression
	 * @generated
	 */
	EClass getLiteralExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression <em>Boolean Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Literal Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression
	 * @generated
	 */
	EClass getBooleanLiteralExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.BooleanLiteralExpression#isValue()
	 * @see #getBooleanLiteralExpression()
	 * @generated
	 */
	EAttribute getBooleanLiteralExpression_Value();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression <em>Integer Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Literal Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression
	 * @generated
	 */
	EClass getIntegerLiteralExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.IntegerLiteralExpression#getValue()
	 * @see #getIntegerLiteralExpression()
	 * @generated
	 */
	EAttribute getIntegerLiteralExpression_Value();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression <em>Real Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Literal Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression
	 * @generated
	 */
	EClass getRealLiteralExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression#getValue()
	 * @see #getRealLiteralExpression()
	 * @generated
	 */
	EAttribute getRealLiteralExpression_Value();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression <em>String Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Literal Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression
	 * @generated
	 */
	EClass getStringLiteralExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.literals.StringLiteralExpression#getValue()
	 * @see #getStringLiteralExpression()
	 * @generated
	 */
	EAttribute getStringLiteralExpression_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LiteralsFactory getLiteralsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getLiteralExpression()
		 * @generated
		 */
		EClass LITERAL_EXPRESSION = eINSTANCE.getLiteralExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.BooleanLiteralExpressionImpl <em>Boolean Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.BooleanLiteralExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getBooleanLiteralExpression()
		 * @generated
		 */
		EClass BOOLEAN_LITERAL_EXPRESSION = eINSTANCE.getBooleanLiteralExpression();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_LITERAL_EXPRESSION__VALUE = eINSTANCE.getBooleanLiteralExpression_Value();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.IntegerLiteralExpressionImpl <em>Integer Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.IntegerLiteralExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getIntegerLiteralExpression()
		 * @generated
		 */
		EClass INTEGER_LITERAL_EXPRESSION = eINSTANCE.getIntegerLiteralExpression();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_LITERAL_EXPRESSION__VALUE = eINSTANCE.getIntegerLiteralExpression_Value();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.RealLiteralExpressionImpl <em>Real Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.RealLiteralExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getRealLiteralExpression()
		 * @generated
		 */
		EClass REAL_LITERAL_EXPRESSION = eINSTANCE.getRealLiteralExpression();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_LITERAL_EXPRESSION__VALUE = eINSTANCE.getRealLiteralExpression_Value();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.literals.impl.StringLiteralExpressionImpl <em>String Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.StringLiteralExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl#getStringLiteralExpression()
		 * @generated
		 */
		EClass STRING_LITERAL_EXPRESSION = eINSTANCE.getStringLiteralExpression();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_LITERAL_EXPRESSION__VALUE = eINSTANCE.getStringLiteralExpression_Value();

	}

} //LiteralsPackage
