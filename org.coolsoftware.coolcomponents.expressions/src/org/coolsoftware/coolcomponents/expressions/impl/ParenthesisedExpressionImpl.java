/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.impl;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.ExpressionsPackage;
import org.coolsoftware.coolcomponents.expressions.ParenthesisedExpression;
import org.coolsoftware.coolcomponents.expressions.WellFormednessRuleException;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Parenthesised Expression</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.impl.ParenthesisedExpressionImpl#getSourceExp <em>Source Exp</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ParenthesisedExpressionImpl extends ExpressionImpl implements
		ParenthesisedExpression {
	/**
	 * The cached value of the '{@link #getSourceExp() <em>Source Exp</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSourceExp()
	 * @generated
	 * @ordered
	 */
	protected Expression sourceExp;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ParenthesisedExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.PARENTHESISED_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getSourceExp() {
		return sourceExp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceExp(Expression newSourceExp,
			NotificationChain msgs) {
		Expression oldSourceExp = sourceExp;
		sourceExp = newSourceExp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP, oldSourceExp, newSourceExp);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceExp(Expression newSourceExp) {
		if (newSourceExp != sourceExp) {
			NotificationChain msgs = null;
			if (sourceExp != null)
				msgs = ((InternalEObject)sourceExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP, null, msgs);
			if (newSourceExp != null)
				msgs = ((InternalEObject)newSourceExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP, null, msgs);
			msgs = basicSetSourceExp(newSourceExp, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP, newSourceExp, newSourceExp));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP:
				return basicSetSourceExp(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP:
				return getSourceExp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP:
				setSourceExp((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP:
				setSourceExp((Expression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExpressionsPackage.PARENTHESISED_EXPRESSION__SOURCE_EXP:
				return sourceExp != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.coolsoftware.coolcomponents.expressions.impl.ExpressionImpl#computeResultType()
	 * 
	 * @generated NOT
	 */
	@Override
	protected CcmType computeResultType() {
		return sourceExp.getResultType();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.coolsoftware.coolcomponents.expressions.impl.ExpressionImpl#checkMyWFRs()
	 * 
	 * @generated NOT
	 */
	@Override
	protected void checkMyWFRs() throws WellFormednessRuleException {
		/* No WFR checks required. */
	}

} // ParenthesisedExpressionImpl
