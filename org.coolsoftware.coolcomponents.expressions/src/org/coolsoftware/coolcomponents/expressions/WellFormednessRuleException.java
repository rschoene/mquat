/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions;

/**
 * Thrown by the {@link Expression}s, if WFRs are violated.
 */
public class WellFormednessRuleException extends Exception {

	/** ID used for serialization */
	private static final long serialVersionUID = 535570230202822137L;

	/** The {@link Expression} causing this {@link WellFormednessRuleException}. */
	protected Expression malFormedExpression;

	/**
	 * Creates a new {@link WellFormednessRuleException}.
	 * 
	 * @param msg
	 *            The message of the {@link Exception}.
	 * @param malFormedExpression
	 *            The {@link Expression} causing this
	 *            {@link WellFormednessRuleException}.
	 */
	public WellFormednessRuleException(String msg,
			Expression malFormedExpression) {
		super(msg);

		this.malFormedExpression = malFormedExpression;
	}

	/**
	 * Returns the {@link Expression} causing this
	 * {@link WellFormednessRuleException}.
	 * 
	 * @return The {@link Expression} causing this
	 *         {@link WellFormednessRuleException}.
	 */
	public Expression getMalFormedExpression() {
		return this.malFormedExpression;
	}
}