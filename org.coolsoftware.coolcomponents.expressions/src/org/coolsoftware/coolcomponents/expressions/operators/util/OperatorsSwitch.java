/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators.util;

import java.util.List;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.operators.*;
import org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage
 * @generated
 */
public class OperatorsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OperatorsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsSwitch() {
		if (modelPackage == null) {
			modelPackage = OperatorsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION: {
				AdditiveOperationCallExpression additiveOperationCallExpression = (AdditiveOperationCallExpression)theEObject;
				T result = caseAdditiveOperationCallExpression(additiveOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(additiveOperationCallExpression);
				if (result == null) result = caseExpression(additiveOperationCallExpression);
				if (result == null) result = caseStatement(additiveOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION: {
				AddToVarOperationCallExpression addToVarOperationCallExpression = (AddToVarOperationCallExpression)theEObject;
				T result = caseAddToVarOperationCallExpression(addToVarOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(addToVarOperationCallExpression);
				if (result == null) result = caseExpression(addToVarOperationCallExpression);
				if (result == null) result = caseStatement(addToVarOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION: {
				ConjunctionOperationCallExpression conjunctionOperationCallExpression = (ConjunctionOperationCallExpression)theEObject;
				T result = caseConjunctionOperationCallExpression(conjunctionOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(conjunctionOperationCallExpression);
				if (result == null) result = caseExpression(conjunctionOperationCallExpression);
				if (result == null) result = caseStatement(conjunctionOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION: {
				DecrementOperatorExpression decrementOperatorExpression = (DecrementOperatorExpression)theEObject;
				T result = caseDecrementOperatorExpression(decrementOperatorExpression);
				if (result == null) result = caseOperationCallExpression(decrementOperatorExpression);
				if (result == null) result = caseExpression(decrementOperatorExpression);
				if (result == null) result = caseStatement(decrementOperatorExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION: {
				DisjunctionOperationCallExpression disjunctionOperationCallExpression = (DisjunctionOperationCallExpression)theEObject;
				T result = caseDisjunctionOperationCallExpression(disjunctionOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(disjunctionOperationCallExpression);
				if (result == null) result = caseExpression(disjunctionOperationCallExpression);
				if (result == null) result = caseStatement(disjunctionOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION: {
				DivisionOperationCallExpression divisionOperationCallExpression = (DivisionOperationCallExpression)theEObject;
				T result = caseDivisionOperationCallExpression(divisionOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(divisionOperationCallExpression);
				if (result == null) result = caseExpression(divisionOperationCallExpression);
				if (result == null) result = caseStatement(divisionOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION: {
				EqualsOperationCallExpression equalsOperationCallExpression = (EqualsOperationCallExpression)theEObject;
				T result = caseEqualsOperationCallExpression(equalsOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(equalsOperationCallExpression);
				if (result == null) result = caseExpression(equalsOperationCallExpression);
				if (result == null) result = caseStatement(equalsOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION: {
				GreaterThanOperationCallExpression greaterThanOperationCallExpression = (GreaterThanOperationCallExpression)theEObject;
				T result = caseGreaterThanOperationCallExpression(greaterThanOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(greaterThanOperationCallExpression);
				if (result == null) result = caseExpression(greaterThanOperationCallExpression);
				if (result == null) result = caseStatement(greaterThanOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION: {
				GreaterThanEqualsOperationCallExpression greaterThanEqualsOperationCallExpression = (GreaterThanEqualsOperationCallExpression)theEObject;
				T result = caseGreaterThanEqualsOperationCallExpression(greaterThanEqualsOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(greaterThanEqualsOperationCallExpression);
				if (result == null) result = caseExpression(greaterThanEqualsOperationCallExpression);
				if (result == null) result = caseStatement(greaterThanEqualsOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION: {
				ImplicationOperationCallExpression implicationOperationCallExpression = (ImplicationOperationCallExpression)theEObject;
				T result = caseImplicationOperationCallExpression(implicationOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(implicationOperationCallExpression);
				if (result == null) result = caseExpression(implicationOperationCallExpression);
				if (result == null) result = caseStatement(implicationOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION: {
				IncrementOperatorExpression incrementOperatorExpression = (IncrementOperatorExpression)theEObject;
				T result = caseIncrementOperatorExpression(incrementOperatorExpression);
				if (result == null) result = caseOperationCallExpression(incrementOperatorExpression);
				if (result == null) result = caseExpression(incrementOperatorExpression);
				if (result == null) result = caseStatement(incrementOperatorExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION: {
				LessThanOperationCallExpression lessThanOperationCallExpression = (LessThanOperationCallExpression)theEObject;
				T result = caseLessThanOperationCallExpression(lessThanOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(lessThanOperationCallExpression);
				if (result == null) result = caseExpression(lessThanOperationCallExpression);
				if (result == null) result = caseStatement(lessThanOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION: {
				LessThanEqualsOperationCallExpression lessThanEqualsOperationCallExpression = (LessThanEqualsOperationCallExpression)theEObject;
				T result = caseLessThanEqualsOperationCallExpression(lessThanEqualsOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(lessThanEqualsOperationCallExpression);
				if (result == null) result = caseExpression(lessThanEqualsOperationCallExpression);
				if (result == null) result = caseStatement(lessThanEqualsOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION: {
				MultiplicativeOperationCallExpression multiplicativeOperationCallExpression = (MultiplicativeOperationCallExpression)theEObject;
				T result = caseMultiplicativeOperationCallExpression(multiplicativeOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(multiplicativeOperationCallExpression);
				if (result == null) result = caseExpression(multiplicativeOperationCallExpression);
				if (result == null) result = caseStatement(multiplicativeOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION: {
				NegativeOperationCallExpression negativeOperationCallExpression = (NegativeOperationCallExpression)theEObject;
				T result = caseNegativeOperationCallExpression(negativeOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(negativeOperationCallExpression);
				if (result == null) result = caseExpression(negativeOperationCallExpression);
				if (result == null) result = caseStatement(negativeOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION: {
				NegationOperationCallExpression negationOperationCallExpression = (NegationOperationCallExpression)theEObject;
				T result = caseNegationOperationCallExpression(negationOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(negationOperationCallExpression);
				if (result == null) result = caseExpression(negationOperationCallExpression);
				if (result == null) result = caseStatement(negationOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION: {
				NotEqualsOperationCallExpression notEqualsOperationCallExpression = (NotEqualsOperationCallExpression)theEObject;
				T result = caseNotEqualsOperationCallExpression(notEqualsOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(notEqualsOperationCallExpression);
				if (result == null) result = caseExpression(notEqualsOperationCallExpression);
				if (result == null) result = caseStatement(notEqualsOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.OPERATION_CALL_EXPRESSION: {
				OperationCallExpression operationCallExpression = (OperationCallExpression)theEObject;
				T result = caseOperationCallExpression(operationCallExpression);
				if (result == null) result = caseExpression(operationCallExpression);
				if (result == null) result = caseStatement(operationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION: {
				SubtractFromVarOperationCallExpression subtractFromVarOperationCallExpression = (SubtractFromVarOperationCallExpression)theEObject;
				T result = caseSubtractFromVarOperationCallExpression(subtractFromVarOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(subtractFromVarOperationCallExpression);
				if (result == null) result = caseExpression(subtractFromVarOperationCallExpression);
				if (result == null) result = caseStatement(subtractFromVarOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION: {
				SubtractiveOperationCallExpression subtractiveOperationCallExpression = (SubtractiveOperationCallExpression)theEObject;
				T result = caseSubtractiveOperationCallExpression(subtractiveOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(subtractiveOperationCallExpression);
				if (result == null) result = caseExpression(subtractiveOperationCallExpression);
				if (result == null) result = caseStatement(subtractiveOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION: {
				PowerOperationCallExpression powerOperationCallExpression = (PowerOperationCallExpression)theEObject;
				T result = casePowerOperationCallExpression(powerOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(powerOperationCallExpression);
				if (result == null) result = caseExpression(powerOperationCallExpression);
				if (result == null) result = caseStatement(powerOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION: {
				SinusOperationCallExpression sinusOperationCallExpression = (SinusOperationCallExpression)theEObject;
				T result = caseSinusOperationCallExpression(sinusOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(sinusOperationCallExpression);
				if (result == null) result = caseExpression(sinusOperationCallExpression);
				if (result == null) result = caseStatement(sinusOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION: {
				CosinusOperationCallExpression cosinusOperationCallExpression = (CosinusOperationCallExpression)theEObject;
				T result = caseCosinusOperationCallExpression(cosinusOperationCallExpression);
				if (result == null) result = caseOperationCallExpression(cosinusOperationCallExpression);
				if (result == null) result = caseExpression(cosinusOperationCallExpression);
				if (result == null) result = caseStatement(cosinusOperationCallExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.FUNCTION_EXPRESSION: {
				FunctionExpression functionExpression = (FunctionExpression)theEObject;
				T result = caseFunctionExpression(functionExpression);
				if (result == null) result = caseOperationCallExpression(functionExpression);
				if (result == null) result = caseExpression(functionExpression);
				if (result == null) result = caseStatement(functionExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Additive Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Additive Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdditiveOperationCallExpression(AdditiveOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Add To Var Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Add To Var Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddToVarOperationCallExpression(AddToVarOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conjunction Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conjunction Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConjunctionOperationCallExpression(ConjunctionOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decrement Operator Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decrement Operator Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecrementOperatorExpression(DecrementOperatorExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Disjunction Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Disjunction Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDisjunctionOperationCallExpression(DisjunctionOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Division Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Division Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDivisionOperationCallExpression(DivisionOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equals Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEqualsOperationCallExpression(EqualsOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Greater Than Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Greater Than Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGreaterThanOperationCallExpression(GreaterThanOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Greater Than Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Greater Than Equals Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGreaterThanEqualsOperationCallExpression(GreaterThanEqualsOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Implication Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Implication Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImplicationOperationCallExpression(ImplicationOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Less Than Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Less Than Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLessThanOperationCallExpression(LessThanOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Less Than Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Less Than Equals Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLessThanEqualsOperationCallExpression(LessThanEqualsOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multiplicative Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multiplicative Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiplicativeOperationCallExpression(MultiplicativeOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Negative Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Negative Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNegativeOperationCallExpression(NegativeOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Negation Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Negation Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNegationOperationCallExpression(NegationOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Equals Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotEqualsOperationCallExpression(NotEqualsOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationCallExpression(OperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subtract From Var Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subtract From Var Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubtractFromVarOperationCallExpression(SubtractFromVarOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subtractive Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subtractive Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubtractiveOperationCallExpression(SubtractiveOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Power Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Power Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePowerOperationCallExpression(PowerOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sinus Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sinus Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSinusOperationCallExpression(SinusOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cosinus Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cosinus Operation Call Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCosinusOperationCallExpression(CosinusOperationCallExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionExpression(FunctionExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatement(Statement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Increment Operator Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Increment Operator Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIncrementOperatorExpression(IncrementOperatorExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpression(Expression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OperatorsSwitch
