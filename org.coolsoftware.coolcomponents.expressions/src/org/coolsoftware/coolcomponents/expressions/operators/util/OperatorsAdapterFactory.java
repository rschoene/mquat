/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators.util;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.operators.*;
import org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage
 * @generated
 */
public class OperatorsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OperatorsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OperatorsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperatorsSwitch<Adapter> modelSwitch =
		new OperatorsSwitch<Adapter>() {
			@Override
			public Adapter caseAdditiveOperationCallExpression(AdditiveOperationCallExpression object) {
				return createAdditiveOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseAddToVarOperationCallExpression(AddToVarOperationCallExpression object) {
				return createAddToVarOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseConjunctionOperationCallExpression(ConjunctionOperationCallExpression object) {
				return createConjunctionOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseDecrementOperatorExpression(DecrementOperatorExpression object) {
				return createDecrementOperatorExpressionAdapter();
			}
			@Override
			public Adapter caseDisjunctionOperationCallExpression(DisjunctionOperationCallExpression object) {
				return createDisjunctionOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseDivisionOperationCallExpression(DivisionOperationCallExpression object) {
				return createDivisionOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseEqualsOperationCallExpression(EqualsOperationCallExpression object) {
				return createEqualsOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseGreaterThanOperationCallExpression(GreaterThanOperationCallExpression object) {
				return createGreaterThanOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseGreaterThanEqualsOperationCallExpression(GreaterThanEqualsOperationCallExpression object) {
				return createGreaterThanEqualsOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseImplicationOperationCallExpression(ImplicationOperationCallExpression object) {
				return createImplicationOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseIncrementOperatorExpression(IncrementOperatorExpression object) {
				return createIncrementOperatorExpressionAdapter();
			}
			@Override
			public Adapter caseLessThanOperationCallExpression(LessThanOperationCallExpression object) {
				return createLessThanOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseLessThanEqualsOperationCallExpression(LessThanEqualsOperationCallExpression object) {
				return createLessThanEqualsOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseMultiplicativeOperationCallExpression(MultiplicativeOperationCallExpression object) {
				return createMultiplicativeOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseNegativeOperationCallExpression(NegativeOperationCallExpression object) {
				return createNegativeOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseNegationOperationCallExpression(NegationOperationCallExpression object) {
				return createNegationOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseNotEqualsOperationCallExpression(NotEqualsOperationCallExpression object) {
				return createNotEqualsOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseOperationCallExpression(OperationCallExpression object) {
				return createOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseSubtractFromVarOperationCallExpression(SubtractFromVarOperationCallExpression object) {
				return createSubtractFromVarOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseSubtractiveOperationCallExpression(SubtractiveOperationCallExpression object) {
				return createSubtractiveOperationCallExpressionAdapter();
			}
			@Override
			public Adapter casePowerOperationCallExpression(PowerOperationCallExpression object) {
				return createPowerOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseSinusOperationCallExpression(SinusOperationCallExpression object) {
				return createSinusOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseCosinusOperationCallExpression(CosinusOperationCallExpression object) {
				return createCosinusOperationCallExpressionAdapter();
			}
			@Override
			public Adapter caseFunctionExpression(FunctionExpression object) {
				return createFunctionExpressionAdapter();
			}
			@Override
			public Adapter caseStatement(Statement object) {
				return createStatementAdapter();
			}
			@Override
			public Adapter caseExpression(Expression object) {
				return createExpressionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression <em>Additive Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression
	 * @generated
	 */
	public Adapter createAdditiveOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression <em>Add To Var Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression
	 * @generated
	 */
	public Adapter createAddToVarOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression <em>Conjunction Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression
	 * @generated
	 */
	public Adapter createConjunctionOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression <em>Decrement Operator Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression
	 * @generated
	 */
	public Adapter createDecrementOperatorExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression <em>Disjunction Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression
	 * @generated
	 */
	public Adapter createDisjunctionOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression <em>Division Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression
	 * @generated
	 */
	public Adapter createDivisionOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression <em>Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression
	 * @generated
	 */
	public Adapter createEqualsOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression <em>Greater Than Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression
	 * @generated
	 */
	public Adapter createGreaterThanOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression <em>Greater Than Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression
	 * @generated
	 */
	public Adapter createGreaterThanEqualsOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression <em>Implication Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression
	 * @generated
	 */
	public Adapter createImplicationOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression <em>Less Than Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression
	 * @generated
	 */
	public Adapter createLessThanOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression <em>Less Than Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression
	 * @generated
	 */
	public Adapter createLessThanEqualsOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression <em>Multiplicative Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression
	 * @generated
	 */
	public Adapter createMultiplicativeOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression <em>Negative Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression
	 * @generated
	 */
	public Adapter createNegativeOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression <em>Negation Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression
	 * @generated
	 */
	public Adapter createNegationOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression <em>Not Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression
	 * @generated
	 */
	public Adapter createNotEqualsOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression <em>Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression
	 * @generated
	 */
	public Adapter createOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression <em>Subtract From Var Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression
	 * @generated
	 */
	public Adapter createSubtractFromVarOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression <em>Subtractive Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression
	 * @generated
	 */
	public Adapter createSubtractiveOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression <em>Power Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression
	 * @generated
	 */
	public Adapter createPowerOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression <em>Sinus Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression
	 * @generated
	 */
	public Adapter createSinusOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression <em>Cosinus Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression
	 * @generated
	 */
	public Adapter createCosinusOperationCallExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression <em>Function Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression
	 * @generated
	 */
	public Adapter createFunctionExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.Statement
	 * @generated
	 */
	public Adapter createStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression <em>Increment Operator Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression
	 * @generated
	 */
	public Adapter createIncrementOperatorExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.expressions.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.coolsoftware.coolcomponents.expressions.Expression
	 * @generated
	 */
	public Adapter createExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OperatorsAdapterFactory
