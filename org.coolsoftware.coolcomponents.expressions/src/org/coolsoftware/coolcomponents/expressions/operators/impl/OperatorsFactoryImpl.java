/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators.impl;

import org.coolsoftware.coolcomponents.expressions.operators.*;
import org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperatorsFactoryImpl extends EFactoryImpl implements OperatorsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OperatorsFactory init() {
		try {
			OperatorsFactory theOperatorsFactory = (OperatorsFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.cool-software.org/dimm/expressions/operators"); 
			if (theOperatorsFactory != null) {
				return theOperatorsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OperatorsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OperatorsPackage.ADDITIVE_OPERATION_CALL_EXPRESSION: return createAdditiveOperationCallExpression();
			case OperatorsPackage.ADD_TO_VAR_OPERATION_CALL_EXPRESSION: return createAddToVarOperationCallExpression();
			case OperatorsPackage.CONJUNCTION_OPERATION_CALL_EXPRESSION: return createConjunctionOperationCallExpression();
			case OperatorsPackage.DECREMENT_OPERATOR_EXPRESSION: return createDecrementOperatorExpression();
			case OperatorsPackage.DISJUNCTION_OPERATION_CALL_EXPRESSION: return createDisjunctionOperationCallExpression();
			case OperatorsPackage.DIVISION_OPERATION_CALL_EXPRESSION: return createDivisionOperationCallExpression();
			case OperatorsPackage.EQUALS_OPERATION_CALL_EXPRESSION: return createEqualsOperationCallExpression();
			case OperatorsPackage.GREATER_THAN_OPERATION_CALL_EXPRESSION: return createGreaterThanOperationCallExpression();
			case OperatorsPackage.GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION: return createGreaterThanEqualsOperationCallExpression();
			case OperatorsPackage.IMPLICATION_OPERATION_CALL_EXPRESSION: return createImplicationOperationCallExpression();
			case OperatorsPackage.INCREMENT_OPERATOR_EXPRESSION: return createIncrementOperatorExpression();
			case OperatorsPackage.LESS_THAN_OPERATION_CALL_EXPRESSION: return createLessThanOperationCallExpression();
			case OperatorsPackage.LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION: return createLessThanEqualsOperationCallExpression();
			case OperatorsPackage.MULTIPLICATIVE_OPERATION_CALL_EXPRESSION: return createMultiplicativeOperationCallExpression();
			case OperatorsPackage.NEGATIVE_OPERATION_CALL_EXPRESSION: return createNegativeOperationCallExpression();
			case OperatorsPackage.NEGATION_OPERATION_CALL_EXPRESSION: return createNegationOperationCallExpression();
			case OperatorsPackage.NOT_EQUALS_OPERATION_CALL_EXPRESSION: return createNotEqualsOperationCallExpression();
			case OperatorsPackage.SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION: return createSubtractFromVarOperationCallExpression();
			case OperatorsPackage.SUBTRACTIVE_OPERATION_CALL_EXPRESSION: return createSubtractiveOperationCallExpression();
			case OperatorsPackage.POWER_OPERATION_CALL_EXPRESSION: return createPowerOperationCallExpression();
			case OperatorsPackage.SINUS_OPERATION_CALL_EXPRESSION: return createSinusOperationCallExpression();
			case OperatorsPackage.COSINUS_OPERATION_CALL_EXPRESSION: return createCosinusOperationCallExpression();
			case OperatorsPackage.FUNCTION_EXPRESSION: return createFunctionExpression();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdditiveOperationCallExpression createAdditiveOperationCallExpression() {
		AdditiveOperationCallExpressionImpl additiveOperationCallExpression = new AdditiveOperationCallExpressionImpl();
		return additiveOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddToVarOperationCallExpression createAddToVarOperationCallExpression() {
		AddToVarOperationCallExpressionImpl addToVarOperationCallExpression = new AddToVarOperationCallExpressionImpl();
		return addToVarOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConjunctionOperationCallExpression createConjunctionOperationCallExpression() {
		ConjunctionOperationCallExpressionImpl conjunctionOperationCallExpression = new ConjunctionOperationCallExpressionImpl();
		return conjunctionOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecrementOperatorExpression createDecrementOperatorExpression() {
		DecrementOperatorExpressionImpl decrementOperatorExpression = new DecrementOperatorExpressionImpl();
		return decrementOperatorExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisjunctionOperationCallExpression createDisjunctionOperationCallExpression() {
		DisjunctionOperationCallExpressionImpl disjunctionOperationCallExpression = new DisjunctionOperationCallExpressionImpl();
		return disjunctionOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DivisionOperationCallExpression createDivisionOperationCallExpression() {
		DivisionOperationCallExpressionImpl divisionOperationCallExpression = new DivisionOperationCallExpressionImpl();
		return divisionOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EqualsOperationCallExpression createEqualsOperationCallExpression() {
		EqualsOperationCallExpressionImpl equalsOperationCallExpression = new EqualsOperationCallExpressionImpl();
		return equalsOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GreaterThanOperationCallExpression createGreaterThanOperationCallExpression() {
		GreaterThanOperationCallExpressionImpl greaterThanOperationCallExpression = new GreaterThanOperationCallExpressionImpl();
		return greaterThanOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GreaterThanEqualsOperationCallExpression createGreaterThanEqualsOperationCallExpression() {
		GreaterThanEqualsOperationCallExpressionImpl greaterThanEqualsOperationCallExpression = new GreaterThanEqualsOperationCallExpressionImpl();
		return greaterThanEqualsOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplicationOperationCallExpression createImplicationOperationCallExpression() {
		ImplicationOperationCallExpressionImpl implicationOperationCallExpression = new ImplicationOperationCallExpressionImpl();
		return implicationOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LessThanOperationCallExpression createLessThanOperationCallExpression() {
		LessThanOperationCallExpressionImpl lessThanOperationCallExpression = new LessThanOperationCallExpressionImpl();
		return lessThanOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LessThanEqualsOperationCallExpression createLessThanEqualsOperationCallExpression() {
		LessThanEqualsOperationCallExpressionImpl lessThanEqualsOperationCallExpression = new LessThanEqualsOperationCallExpressionImpl();
		return lessThanEqualsOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicativeOperationCallExpression createMultiplicativeOperationCallExpression() {
		MultiplicativeOperationCallExpressionImpl multiplicativeOperationCallExpression = new MultiplicativeOperationCallExpressionImpl();
		return multiplicativeOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NegativeOperationCallExpression createNegativeOperationCallExpression() {
		NegativeOperationCallExpressionImpl negativeOperationCallExpression = new NegativeOperationCallExpressionImpl();
		return negativeOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NegationOperationCallExpression createNegationOperationCallExpression() {
		NegationOperationCallExpressionImpl negationOperationCallExpression = new NegationOperationCallExpressionImpl();
		return negationOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotEqualsOperationCallExpression createNotEqualsOperationCallExpression() {
		NotEqualsOperationCallExpressionImpl notEqualsOperationCallExpression = new NotEqualsOperationCallExpressionImpl();
		return notEqualsOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtractFromVarOperationCallExpression createSubtractFromVarOperationCallExpression() {
		SubtractFromVarOperationCallExpressionImpl subtractFromVarOperationCallExpression = new SubtractFromVarOperationCallExpressionImpl();
		return subtractFromVarOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtractiveOperationCallExpression createSubtractiveOperationCallExpression() {
		SubtractiveOperationCallExpressionImpl subtractiveOperationCallExpression = new SubtractiveOperationCallExpressionImpl();
		return subtractiveOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PowerOperationCallExpression createPowerOperationCallExpression() {
		PowerOperationCallExpressionImpl powerOperationCallExpression = new PowerOperationCallExpressionImpl();
		return powerOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SinusOperationCallExpression createSinusOperationCallExpression() {
		SinusOperationCallExpressionImpl sinusOperationCallExpression = new SinusOperationCallExpressionImpl();
		return sinusOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CosinusOperationCallExpression createCosinusOperationCallExpression() {
		CosinusOperationCallExpressionImpl cosinusOperationCallExpression = new CosinusOperationCallExpressionImpl();
		return cosinusOperationCallExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionExpression createFunctionExpression() {
		FunctionExpressionImpl functionExpression = new FunctionExpressionImpl();
		return functionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IncrementOperatorExpression createIncrementOperatorExpression() {
		IncrementOperatorExpressionImpl incrementOperatorExpression = new IncrementOperatorExpressionImpl();
		return incrementOperatorExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsPackage getOperatorsPackage() {
		return (OperatorsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OperatorsPackage getPackage() {
		return OperatorsPackage.eINSTANCE;
	}

} //OperatorsFactoryImpl
