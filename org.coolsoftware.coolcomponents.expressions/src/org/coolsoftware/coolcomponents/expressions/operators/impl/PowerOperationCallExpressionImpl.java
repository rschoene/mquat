/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators.impl;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.WellFormednessRuleException;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage;
import org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Power Operation Call Expression</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class PowerOperationCallExpressionImpl extends
		OperationCallExpressionImpl implements PowerOperationCallExpression {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	protected PowerOperationCallExpressionImpl() {
		super();
		
		this.operationName = "^";
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorsPackage.Literals.POWER_OPERATION_CALL_EXPRESSION;
	}

	/**
	 * @generated NOT
	 */
	@Override
	protected void checkMyWFRs() throws WellFormednessRuleException {
		/* Validate source. */
		if (this.sourceExp == null)
			throw new WellFormednessRuleException(
					"The source of an operator cannot be null.", this);
		// no else.

		this.sourceExp.checkWFRs();

		/* Validate argument. */
		if (this.argumentExps == null || this.argumentExps.size() != 1)
			throw new WellFormednessRuleException(
					"A binary operator requires exactly one argument.", this);
		// no else.

		Expression arg = argumentExps.get(0);
		arg.checkWFRs();

		CcmType realtType = CcmStandardLibraryTypeFactory.eINSTANCE
				.getRealType();
		CcmType stringType = CcmStandardLibraryTypeFactory.eINSTANCE
				.getStringType();

		CcmType sourceType = this.sourceExp.getResultType();

		if (!sourceType.conformsTo(realtType))
			throw new WellFormednessRuleException(
					"Power operator is only allowed on Integer and Real values.",
					this);
		// no else.

		CcmType argType = arg.getResultType();

		if (sourceType.conformsTo(realtType) && !argType.conformsTo(realtType))
			throw new WellFormednessRuleException(
					"Power operator on Real requires argument of type Real.",
					arg);
		// no else.
	}

	/**
	 * @generated NOT
	 */
	@Override
	protected CcmType computeResultType() {
		CcmType integerType = CcmStandardLibraryTypeFactory.eINSTANCE
				.getIntegerType();
		if (sourceExp.getResultType().conformsTo(integerType)
				&& argumentExps.size() == 1
				&& argumentExps.get(0).getResultType().conformsTo(integerType))
			return integerType;
		else
			return CcmStandardLibraryTypeFactory.eINSTANCE.getRealType();
	}

} // PowerOperationCallExpressionImpl
