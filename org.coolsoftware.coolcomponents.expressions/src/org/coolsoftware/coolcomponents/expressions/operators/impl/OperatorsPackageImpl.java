/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators.impl;

import org.coolsoftware.coolcomponents.expressions.ExpressionsPackage;
import org.coolsoftware.coolcomponents.expressions.impl.ExpressionsPackageImpl;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsPackage;
import org.coolsoftware.coolcomponents.expressions.literals.impl.LiteralsPackageImpl;
import org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage;
import org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.expressions.variables.impl.VariablesPackageImpl;
import org.coolsoftware.coolcomponents.types.DatatypesPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperatorsPackageImpl extends EPackageImpl implements OperatorsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass additiveOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addToVarOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conjunctionOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decrementOperatorExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass disjunctionOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass divisionOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass equalsOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass greaterThanOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass greaterThanEqualsOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass implicationOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lessThanOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lessThanEqualsOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplicativeOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negativeOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negationOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notEqualsOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subtractFromVarOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subtractiveOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass powerOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sinusOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cosinusOperationCallExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass incrementOperatorExpressionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OperatorsPackageImpl() {
		super(eNS_URI, OperatorsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OperatorsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OperatorsPackage init() {
		if (isInited) return (OperatorsPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);

		// Obtain or create and register package
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OperatorsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DatatypesPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) : ExpressionsPackage.eINSTANCE);
		LiteralsPackageImpl theLiteralsPackage = (LiteralsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LiteralsPackage.eNS_URI) instanceof LiteralsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LiteralsPackage.eNS_URI) : LiteralsPackage.eINSTANCE);
		VariablesPackageImpl theVariablesPackage = (VariablesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI) instanceof VariablesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(VariablesPackage.eNS_URI) : VariablesPackage.eINSTANCE);

		// Create package meta-data objects
		theOperatorsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theLiteralsPackage.createPackageContents();
		theVariablesPackage.createPackageContents();

		// Initialize created meta-data
		theOperatorsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theLiteralsPackage.initializePackageContents();
		theVariablesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOperatorsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OperatorsPackage.eNS_URI, theOperatorsPackage);
		return theOperatorsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdditiveOperationCallExpression() {
		return additiveOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAddToVarOperationCallExpression() {
		return addToVarOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConjunctionOperationCallExpression() {
		return conjunctionOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDecrementOperatorExpression() {
		return decrementOperatorExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDisjunctionOperationCallExpression() {
		return disjunctionOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDivisionOperationCallExpression() {
		return divisionOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEqualsOperationCallExpression() {
		return equalsOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGreaterThanOperationCallExpression() {
		return greaterThanOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGreaterThanEqualsOperationCallExpression() {
		return greaterThanEqualsOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImplicationOperationCallExpression() {
		return implicationOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLessThanOperationCallExpression() {
		return lessThanOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLessThanEqualsOperationCallExpression() {
		return lessThanEqualsOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplicativeOperationCallExpression() {
		return multiplicativeOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNegativeOperationCallExpression() {
		return negativeOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNegationOperationCallExpression() {
		return negationOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNotEqualsOperationCallExpression() {
		return notEqualsOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationCallExpression() {
		return operationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationCallExpression_SourceExp() {
		return (EReference)operationCallExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationCallExpression_OperationName() {
		return (EAttribute)operationCallExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationCallExpression_ArgumentExps() {
		return (EReference)operationCallExpressionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubtractFromVarOperationCallExpression() {
		return subtractFromVarOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubtractiveOperationCallExpression() {
		return subtractiveOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPowerOperationCallExpression() {
		return powerOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSinusOperationCallExpression() {
		return sinusOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCosinusOperationCallExpression() {
		return cosinusOperationCallExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionExpression() {
		return functionExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIncrementOperatorExpression() {
		return incrementOperatorExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsFactory getOperatorsFactory() {
		return (OperatorsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		additiveOperationCallExpressionEClass = createEClass(ADDITIVE_OPERATION_CALL_EXPRESSION);

		addToVarOperationCallExpressionEClass = createEClass(ADD_TO_VAR_OPERATION_CALL_EXPRESSION);

		conjunctionOperationCallExpressionEClass = createEClass(CONJUNCTION_OPERATION_CALL_EXPRESSION);

		decrementOperatorExpressionEClass = createEClass(DECREMENT_OPERATOR_EXPRESSION);

		disjunctionOperationCallExpressionEClass = createEClass(DISJUNCTION_OPERATION_CALL_EXPRESSION);

		divisionOperationCallExpressionEClass = createEClass(DIVISION_OPERATION_CALL_EXPRESSION);

		equalsOperationCallExpressionEClass = createEClass(EQUALS_OPERATION_CALL_EXPRESSION);

		greaterThanOperationCallExpressionEClass = createEClass(GREATER_THAN_OPERATION_CALL_EXPRESSION);

		greaterThanEqualsOperationCallExpressionEClass = createEClass(GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION);

		implicationOperationCallExpressionEClass = createEClass(IMPLICATION_OPERATION_CALL_EXPRESSION);

		incrementOperatorExpressionEClass = createEClass(INCREMENT_OPERATOR_EXPRESSION);

		lessThanOperationCallExpressionEClass = createEClass(LESS_THAN_OPERATION_CALL_EXPRESSION);

		lessThanEqualsOperationCallExpressionEClass = createEClass(LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION);

		multiplicativeOperationCallExpressionEClass = createEClass(MULTIPLICATIVE_OPERATION_CALL_EXPRESSION);

		negativeOperationCallExpressionEClass = createEClass(NEGATIVE_OPERATION_CALL_EXPRESSION);

		negationOperationCallExpressionEClass = createEClass(NEGATION_OPERATION_CALL_EXPRESSION);

		notEqualsOperationCallExpressionEClass = createEClass(NOT_EQUALS_OPERATION_CALL_EXPRESSION);

		operationCallExpressionEClass = createEClass(OPERATION_CALL_EXPRESSION);
		createEReference(operationCallExpressionEClass, OPERATION_CALL_EXPRESSION__SOURCE_EXP);
		createEAttribute(operationCallExpressionEClass, OPERATION_CALL_EXPRESSION__OPERATION_NAME);
		createEReference(operationCallExpressionEClass, OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS);

		subtractFromVarOperationCallExpressionEClass = createEClass(SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION);

		subtractiveOperationCallExpressionEClass = createEClass(SUBTRACTIVE_OPERATION_CALL_EXPRESSION);

		powerOperationCallExpressionEClass = createEClass(POWER_OPERATION_CALL_EXPRESSION);

		sinusOperationCallExpressionEClass = createEClass(SINUS_OPERATION_CALL_EXPRESSION);

		cosinusOperationCallExpressionEClass = createEClass(COSINUS_OPERATION_CALL_EXPRESSION);

		functionExpressionEClass = createEClass(FUNCTION_EXPRESSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		additiveOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		addToVarOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		conjunctionOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		decrementOperatorExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		disjunctionOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		divisionOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		equalsOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		greaterThanOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		greaterThanEqualsOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		implicationOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		incrementOperatorExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		lessThanOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		lessThanEqualsOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		multiplicativeOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		negativeOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		negationOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		notEqualsOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		operationCallExpressionEClass.getESuperTypes().add(theExpressionsPackage.getExpression());
		subtractFromVarOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		subtractiveOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		powerOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		sinusOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		cosinusOperationCallExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());
		functionExpressionEClass.getESuperTypes().add(this.getOperationCallExpression());

		// Initialize classes and features; add operations and parameters
		initEClass(additiveOperationCallExpressionEClass, AdditiveOperationCallExpression.class, "AdditiveOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(addToVarOperationCallExpressionEClass, AddToVarOperationCallExpression.class, "AddToVarOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conjunctionOperationCallExpressionEClass, ConjunctionOperationCallExpression.class, "ConjunctionOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(decrementOperatorExpressionEClass, DecrementOperatorExpression.class, "DecrementOperatorExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(disjunctionOperationCallExpressionEClass, DisjunctionOperationCallExpression.class, "DisjunctionOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(divisionOperationCallExpressionEClass, DivisionOperationCallExpression.class, "DivisionOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(equalsOperationCallExpressionEClass, EqualsOperationCallExpression.class, "EqualsOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(greaterThanOperationCallExpressionEClass, GreaterThanOperationCallExpression.class, "GreaterThanOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(greaterThanEqualsOperationCallExpressionEClass, GreaterThanEqualsOperationCallExpression.class, "GreaterThanEqualsOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(implicationOperationCallExpressionEClass, ImplicationOperationCallExpression.class, "ImplicationOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(incrementOperatorExpressionEClass, IncrementOperatorExpression.class, "IncrementOperatorExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(lessThanOperationCallExpressionEClass, LessThanOperationCallExpression.class, "LessThanOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(lessThanEqualsOperationCallExpressionEClass, LessThanEqualsOperationCallExpression.class, "LessThanEqualsOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(multiplicativeOperationCallExpressionEClass, MultiplicativeOperationCallExpression.class, "MultiplicativeOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(negativeOperationCallExpressionEClass, NegativeOperationCallExpression.class, "NegativeOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(negationOperationCallExpressionEClass, NegationOperationCallExpression.class, "NegationOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(notEqualsOperationCallExpressionEClass, NotEqualsOperationCallExpression.class, "NotEqualsOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(operationCallExpressionEClass, OperationCallExpression.class, "OperationCallExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationCallExpression_SourceExp(), theExpressionsPackage.getExpression(), null, "sourceExp", null, 1, 1, OperationCallExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationCallExpression_OperationName(), ecorePackage.getEString(), "operationName", null, 0, 1, OperationCallExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationCallExpression_ArgumentExps(), theExpressionsPackage.getExpression(), null, "argumentExps", null, 0, -1, OperationCallExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(subtractFromVarOperationCallExpressionEClass, SubtractFromVarOperationCallExpression.class, "SubtractFromVarOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(subtractiveOperationCallExpressionEClass, SubtractiveOperationCallExpression.class, "SubtractiveOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(powerOperationCallExpressionEClass, PowerOperationCallExpression.class, "PowerOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sinusOperationCallExpressionEClass, SinusOperationCallExpression.class, "SinusOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cosinusOperationCallExpressionEClass, CosinusOperationCallExpression.class, "CosinusOperationCallExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(functionExpressionEClass, FunctionExpression.class, "FunctionExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //OperatorsPackageImpl
