/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.impl.ExpressionImpl;
import org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression;
import org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Operation Call Expression</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.operators.impl.OperationCallExpressionImpl#getSourceExp <em>Source Exp</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.operators.impl.OperationCallExpressionImpl#getOperationName <em>Operation Name</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.operators.impl.OperationCallExpressionImpl#getArgumentExps <em>Argument Exps</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class OperationCallExpressionImpl extends ExpressionImpl
		implements OperationCallExpression {
	/**
	 * The cached value of the '{@link #getSourceExp() <em>Source Exp</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSourceExp()
	 * @generated
	 * @ordered
	 */
	protected Expression sourceExp;

	/**
	 * The default value of the '{@link #getOperationName() <em>Operation Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getOperationName()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATION_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperationName() <em>Operation Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getOperationName()
	 * @generated
	 * @ordered
	 */
	protected String operationName = OPERATION_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getArgumentExps() <em>Argument Exps</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getArgumentExps()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> argumentExps;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationCallExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorsPackage.Literals.OPERATION_CALL_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getSourceExp() {
		return sourceExp;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceExp(Expression newSourceExp,
			NotificationChain msgs) {
		Expression oldSourceExp = sourceExp;
		sourceExp = newSourceExp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP, oldSourceExp, newSourceExp);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceExp(Expression newSourceExp) {
		if (newSourceExp != sourceExp) {
			NotificationChain msgs = null;
			if (sourceExp != null)
				msgs = ((InternalEObject)sourceExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP, null, msgs);
			if (newSourceExp != null)
				msgs = ((InternalEObject)newSourceExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP, null, msgs);
			msgs = basicSetSourceExp(newSourceExp, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP, newSourceExp, newSourceExp));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getOperationName() {
		return operationName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationName(String newOperationName) {
		String oldOperationName = operationName;
		operationName = newOperationName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorsPackage.OPERATION_CALL_EXPRESSION__OPERATION_NAME, oldOperationName, operationName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expression> getArgumentExps() {
		if (argumentExps == null) {
			argumentExps = new EObjectContainmentEList<Expression>(Expression.class, this, OperatorsPackage.OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS);
		}
		return argumentExps;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP:
				return basicSetSourceExp(null, msgs);
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS:
				return ((InternalEList<?>)getArgumentExps()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP:
				return getSourceExp();
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__OPERATION_NAME:
				return getOperationName();
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS:
				return getArgumentExps();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP:
				setSourceExp((Expression)newValue);
				return;
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__OPERATION_NAME:
				setOperationName((String)newValue);
				return;
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS:
				getArgumentExps().clear();
				getArgumentExps().addAll((Collection<? extends Expression>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP:
				setSourceExp((Expression)null);
				return;
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__OPERATION_NAME:
				setOperationName(OPERATION_NAME_EDEFAULT);
				return;
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS:
				getArgumentExps().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__SOURCE_EXP:
				return sourceExp != null;
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__OPERATION_NAME:
				return OPERATION_NAME_EDEFAULT == null ? operationName != null : !OPERATION_NAME_EDEFAULT.equals(operationName);
			case OperatorsPackage.OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS:
				return argumentExps != null && !argumentExps.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operationName: ");
		result.append(operationName);
		result.append(')');
		return result.toString();
	}

} // OperationCallExpressionImpl
