/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Call Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getSourceExp <em>Source Exp</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getOperationName <em>Operation Name</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getArgumentExps <em>Argument Exps</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage#getOperationCallExpression()
 * @model abstract="true"
 * @generated
 */
public interface OperationCallExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Exp</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Exp</em>' containment reference.
	 * @see #setSourceExp(Expression)
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage#getOperationCallExpression_SourceExp()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getSourceExp();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getSourceExp <em>Source Exp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Exp</em>' containment reference.
	 * @see #getSourceExp()
	 * @generated
	 */
	void setSourceExp(Expression value);

	/**
	 * Returns the value of the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Name</em>' attribute.
	 * @see #setOperationName(String)
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage#getOperationCallExpression_OperationName()
	 * @model
	 * @generated
	 */
	String getOperationName();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getOperationName <em>Operation Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation Name</em>' attribute.
	 * @see #getOperationName()
	 * @generated
	 */
	void setOperationName(String value);

	/**
	 * Returns the value of the '<em><b>Argument Exps</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.coolcomponents.expressions.Expression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Argument Exps</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Argument Exps</em>' containment reference list.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage#getOperationCallExpression_ArgumentExps()
	 * @model containment="true"
	 * @generated
	 */
	EList<Expression> getArgumentExps();

} // OperationCallExpression
