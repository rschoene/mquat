/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators;

import org.coolsoftware.coolcomponents.expressions.ExpressionsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsFactory
 * @model kind="package"
 * @generated
 */
public interface OperatorsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operators";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.cool-software.org/dimm/expressions/operators";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operators";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperatorsPackage eINSTANCE = org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.OperationCallExpressionImpl <em>Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getOperationCallExpression()
	 * @generated
	 */
	int OPERATION_CALL_EXPRESSION = 17;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_CALL_EXPRESSION__SOURCE_EXP = ExpressionsPackage.EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_CALL_EXPRESSION__OPERATION_NAME = ExpressionsPackage.EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = ExpressionsPackage.EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_CALL_EXPRESSION_FEATURE_COUNT = ExpressionsPackage.EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.AdditiveOperationCallExpressionImpl <em>Additive Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.AdditiveOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getAdditiveOperationCallExpression()
	 * @generated
	 */
	int ADDITIVE_OPERATION_CALL_EXPRESSION = 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Additive Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.AddToVarOperationCallExpressionImpl <em>Add To Var Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.AddToVarOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getAddToVarOperationCallExpression()
	 * @generated
	 */
	int ADD_TO_VAR_OPERATION_CALL_EXPRESSION = 1;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_TO_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_TO_VAR_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_TO_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Add To Var Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_TO_VAR_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.ConjunctionOperationCallExpressionImpl <em>Conjunction Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.ConjunctionOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getConjunctionOperationCallExpression()
	 * @generated
	 */
	int CONJUNCTION_OPERATION_CALL_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Conjunction Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.DecrementOperatorExpressionImpl <em>Decrement Operator Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.DecrementOperatorExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getDecrementOperatorExpression()
	 * @generated
	 */
	int DECREMENT_OPERATOR_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECREMENT_OPERATOR_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECREMENT_OPERATOR_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECREMENT_OPERATOR_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Decrement Operator Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECREMENT_OPERATOR_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.DisjunctionOperationCallExpressionImpl <em>Disjunction Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.DisjunctionOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getDisjunctionOperationCallExpression()
	 * @generated
	 */
	int DISJUNCTION_OPERATION_CALL_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Disjunction Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.DivisionOperationCallExpressionImpl <em>Division Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.DivisionOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getDivisionOperationCallExpression()
	 * @generated
	 */
	int DIVISION_OPERATION_CALL_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISION_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISION_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Division Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIVISION_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.EqualsOperationCallExpressionImpl <em>Equals Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.EqualsOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getEqualsOperationCallExpression()
	 * @generated
	 */
	int EQUALS_OPERATION_CALL_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Equals Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQUALS_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanOperationCallExpressionImpl <em>Greater Than Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getGreaterThanOperationCallExpression()
	 * @generated
	 */
	int GREATER_THAN_OPERATION_CALL_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Greater Than Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanEqualsOperationCallExpressionImpl <em>Greater Than Equals Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanEqualsOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getGreaterThanEqualsOperationCallExpression()
	 * @generated
	 */
	int GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Greater Than Equals Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.ImplicationOperationCallExpressionImpl <em>Implication Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.ImplicationOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getImplicationOperationCallExpression()
	 * @generated
	 */
	int IMPLICATION_OPERATION_CALL_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Implication Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanOperationCallExpressionImpl <em>Less Than Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getLessThanOperationCallExpression()
	 * @generated
	 */
	int LESS_THAN_OPERATION_CALL_EXPRESSION = 11;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanEqualsOperationCallExpressionImpl <em>Less Than Equals Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanEqualsOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getLessThanEqualsOperationCallExpression()
	 * @generated
	 */
	int LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION = 12;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.MultiplicativeOperationCallExpressionImpl <em>Multiplicative Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.MultiplicativeOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getMultiplicativeOperationCallExpression()
	 * @generated
	 */
	int MULTIPLICATIVE_OPERATION_CALL_EXPRESSION = 13;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.NegativeOperationCallExpressionImpl <em>Negative Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.NegativeOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getNegativeOperationCallExpression()
	 * @generated
	 */
	int NEGATIVE_OPERATION_CALL_EXPRESSION = 14;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.NegationOperationCallExpressionImpl <em>Negation Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.NegationOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getNegationOperationCallExpression()
	 * @generated
	 */
	int NEGATION_OPERATION_CALL_EXPRESSION = 15;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.NotEqualsOperationCallExpressionImpl <em>Not Equals Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.NotEqualsOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getNotEqualsOperationCallExpression()
	 * @generated
	 */
	int NOT_EQUALS_OPERATION_CALL_EXPRESSION = 16;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractiveOperationCallExpressionImpl <em>Subtractive Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractiveOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getSubtractiveOperationCallExpression()
	 * @generated
	 */
	int SUBTRACTIVE_OPERATION_CALL_EXPRESSION = 19;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.IncrementOperatorExpressionImpl <em>Increment Operator Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.IncrementOperatorExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getIncrementOperatorExpression()
	 * @generated
	 */
	int INCREMENT_OPERATOR_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCREMENT_OPERATOR_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCREMENT_OPERATOR_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCREMENT_OPERATOR_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Increment Operator Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INCREMENT_OPERATOR_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Less Than Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Less Than Equals Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Multiplicative Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Negative Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATIVE_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Negation Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EQUALS_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EQUALS_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EQUALS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Not Equals Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EQUALS_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractFromVarOperationCallExpressionImpl <em>Subtract From Var Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractFromVarOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getSubtractFromVarOperationCallExpression()
	 * @generated
	 */
	int SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION = 18;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Subtract From Var Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACTIVE_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACTIVE_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACTIVE_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Subtractive Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBTRACTIVE_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.PowerOperationCallExpressionImpl <em>Power Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.PowerOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getPowerOperationCallExpression()
	 * @generated
	 */
	int POWER_OPERATION_CALL_EXPRESSION = 20;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Power Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POWER_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.SinusOperationCallExpressionImpl <em>Sinus Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.SinusOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getSinusOperationCallExpression()
	 * @generated
	 */
	int SINUS_OPERATION_CALL_EXPRESSION = 21;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINUS_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINUS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Sinus Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINUS_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.CosinusOperationCallExpressionImpl <em>Cosinus Operation Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.CosinusOperationCallExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getCosinusOperationCallExpression()
	 * @generated
	 */
	int COSINUS_OPERATION_CALL_EXPRESSION = 22;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COSINUS_OPERATION_CALL_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COSINUS_OPERATION_CALL_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COSINUS_OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Cosinus Operation Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COSINUS_OPERATION_CALL_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.FunctionExpressionImpl <em>Function Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.FunctionExpressionImpl
	 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getFunctionExpression()
	 * @generated
	 */
	int FUNCTION_EXPRESSION = 23;

	/**
	 * The feature id for the '<em><b>Source Exp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_EXPRESSION__SOURCE_EXP = OPERATION_CALL_EXPRESSION__SOURCE_EXP;

	/**
	 * The feature id for the '<em><b>Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_EXPRESSION__OPERATION_NAME = OPERATION_CALL_EXPRESSION__OPERATION_NAME;

	/**
	 * The feature id for the '<em><b>Argument Exps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_EXPRESSION__ARGUMENT_EXPS = OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS;

	/**
	 * The number of structural features of the '<em>Function Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_EXPRESSION_FEATURE_COUNT = OPERATION_CALL_EXPRESSION_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression <em>Additive Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Additive Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.AdditiveOperationCallExpression
	 * @generated
	 */
	EClass getAdditiveOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression <em>Add To Var Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Add To Var Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.AddToVarOperationCallExpression
	 * @generated
	 */
	EClass getAddToVarOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression <em>Conjunction Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conjunction Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.ConjunctionOperationCallExpression
	 * @generated
	 */
	EClass getConjunctionOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression <em>Decrement Operator Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decrement Operator Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.DecrementOperatorExpression
	 * @generated
	 */
	EClass getDecrementOperatorExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression <em>Disjunction Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjunction Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.DisjunctionOperationCallExpression
	 * @generated
	 */
	EClass getDisjunctionOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression <em>Division Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Division Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.DivisionOperationCallExpression
	 * @generated
	 */
	EClass getDivisionOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression <em>Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Equals Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.EqualsOperationCallExpression
	 * @generated
	 */
	EClass getEqualsOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression <em>Greater Than Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Greater Than Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.GreaterThanOperationCallExpression
	 * @generated
	 */
	EClass getGreaterThanOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression <em>Greater Than Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Greater Than Equals Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.GreaterThanEqualsOperationCallExpression
	 * @generated
	 */
	EClass getGreaterThanEqualsOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression <em>Implication Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Implication Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.ImplicationOperationCallExpression
	 * @generated
	 */
	EClass getImplicationOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression <em>Less Than Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Less Than Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.LessThanOperationCallExpression
	 * @generated
	 */
	EClass getLessThanOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression <em>Less Than Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Less Than Equals Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.LessThanEqualsOperationCallExpression
	 * @generated
	 */
	EClass getLessThanEqualsOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression <em>Multiplicative Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicative Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.MultiplicativeOperationCallExpression
	 * @generated
	 */
	EClass getMultiplicativeOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression <em>Negative Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negative Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.NegativeOperationCallExpression
	 * @generated
	 */
	EClass getNegativeOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression <em>Negation Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negation Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.NegationOperationCallExpression
	 * @generated
	 */
	EClass getNegationOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression <em>Not Equals Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not Equals Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.NotEqualsOperationCallExpression
	 * @generated
	 */
	EClass getNotEqualsOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression <em>Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression
	 * @generated
	 */
	EClass getOperationCallExpression();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getSourceExp <em>Source Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Exp</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getSourceExp()
	 * @see #getOperationCallExpression()
	 * @generated
	 */
	EReference getOperationCallExpression_SourceExp();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getOperationName <em>Operation Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation Name</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getOperationName()
	 * @see #getOperationCallExpression()
	 * @generated
	 */
	EAttribute getOperationCallExpression_OperationName();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getArgumentExps <em>Argument Exps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Argument Exps</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.OperationCallExpression#getArgumentExps()
	 * @see #getOperationCallExpression()
	 * @generated
	 */
	EReference getOperationCallExpression_ArgumentExps();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression <em>Subtract From Var Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subtract From Var Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.SubtractFromVarOperationCallExpression
	 * @generated
	 */
	EClass getSubtractFromVarOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression <em>Subtractive Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subtractive Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.SubtractiveOperationCallExpression
	 * @generated
	 */
	EClass getSubtractiveOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression <em>Power Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Power Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.PowerOperationCallExpression
	 * @generated
	 */
	EClass getPowerOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression <em>Sinus Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sinus Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.SinusOperationCallExpression
	 * @generated
	 */
	EClass getSinusOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression <em>Cosinus Operation Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cosinus Operation Call Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.CosinusOperationCallExpression
	 * @generated
	 */
	EClass getCosinusOperationCallExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression <em>Function Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression
	 * @generated
	 */
	EClass getFunctionExpression();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression <em>Increment Operator Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Increment Operator Expression</em>'.
	 * @see org.coolsoftware.coolcomponents.expressions.operators.IncrementOperatorExpression
	 * @generated
	 */
	EClass getIncrementOperatorExpression();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperatorsFactory getOperatorsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.AdditiveOperationCallExpressionImpl <em>Additive Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.AdditiveOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getAdditiveOperationCallExpression()
		 * @generated
		 */
		EClass ADDITIVE_OPERATION_CALL_EXPRESSION = eINSTANCE.getAdditiveOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.AddToVarOperationCallExpressionImpl <em>Add To Var Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.AddToVarOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getAddToVarOperationCallExpression()
		 * @generated
		 */
		EClass ADD_TO_VAR_OPERATION_CALL_EXPRESSION = eINSTANCE.getAddToVarOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.ConjunctionOperationCallExpressionImpl <em>Conjunction Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.ConjunctionOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getConjunctionOperationCallExpression()
		 * @generated
		 */
		EClass CONJUNCTION_OPERATION_CALL_EXPRESSION = eINSTANCE.getConjunctionOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.DecrementOperatorExpressionImpl <em>Decrement Operator Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.DecrementOperatorExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getDecrementOperatorExpression()
		 * @generated
		 */
		EClass DECREMENT_OPERATOR_EXPRESSION = eINSTANCE.getDecrementOperatorExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.DisjunctionOperationCallExpressionImpl <em>Disjunction Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.DisjunctionOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getDisjunctionOperationCallExpression()
		 * @generated
		 */
		EClass DISJUNCTION_OPERATION_CALL_EXPRESSION = eINSTANCE.getDisjunctionOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.DivisionOperationCallExpressionImpl <em>Division Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.DivisionOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getDivisionOperationCallExpression()
		 * @generated
		 */
		EClass DIVISION_OPERATION_CALL_EXPRESSION = eINSTANCE.getDivisionOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.EqualsOperationCallExpressionImpl <em>Equals Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.EqualsOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getEqualsOperationCallExpression()
		 * @generated
		 */
		EClass EQUALS_OPERATION_CALL_EXPRESSION = eINSTANCE.getEqualsOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanOperationCallExpressionImpl <em>Greater Than Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getGreaterThanOperationCallExpression()
		 * @generated
		 */
		EClass GREATER_THAN_OPERATION_CALL_EXPRESSION = eINSTANCE.getGreaterThanOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanEqualsOperationCallExpressionImpl <em>Greater Than Equals Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.GreaterThanEqualsOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getGreaterThanEqualsOperationCallExpression()
		 * @generated
		 */
		EClass GREATER_THAN_EQUALS_OPERATION_CALL_EXPRESSION = eINSTANCE.getGreaterThanEqualsOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.ImplicationOperationCallExpressionImpl <em>Implication Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.ImplicationOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getImplicationOperationCallExpression()
		 * @generated
		 */
		EClass IMPLICATION_OPERATION_CALL_EXPRESSION = eINSTANCE.getImplicationOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanOperationCallExpressionImpl <em>Less Than Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getLessThanOperationCallExpression()
		 * @generated
		 */
		EClass LESS_THAN_OPERATION_CALL_EXPRESSION = eINSTANCE.getLessThanOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanEqualsOperationCallExpressionImpl <em>Less Than Equals Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.LessThanEqualsOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getLessThanEqualsOperationCallExpression()
		 * @generated
		 */
		EClass LESS_THAN_EQUALS_OPERATION_CALL_EXPRESSION = eINSTANCE.getLessThanEqualsOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.MultiplicativeOperationCallExpressionImpl <em>Multiplicative Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.MultiplicativeOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getMultiplicativeOperationCallExpression()
		 * @generated
		 */
		EClass MULTIPLICATIVE_OPERATION_CALL_EXPRESSION = eINSTANCE.getMultiplicativeOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.NegativeOperationCallExpressionImpl <em>Negative Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.NegativeOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getNegativeOperationCallExpression()
		 * @generated
		 */
		EClass NEGATIVE_OPERATION_CALL_EXPRESSION = eINSTANCE.getNegativeOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.NegationOperationCallExpressionImpl <em>Negation Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.NegationOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getNegationOperationCallExpression()
		 * @generated
		 */
		EClass NEGATION_OPERATION_CALL_EXPRESSION = eINSTANCE.getNegationOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.NotEqualsOperationCallExpressionImpl <em>Not Equals Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.NotEqualsOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getNotEqualsOperationCallExpression()
		 * @generated
		 */
		EClass NOT_EQUALS_OPERATION_CALL_EXPRESSION = eINSTANCE.getNotEqualsOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.OperationCallExpressionImpl <em>Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getOperationCallExpression()
		 * @generated
		 */
		EClass OPERATION_CALL_EXPRESSION = eINSTANCE.getOperationCallExpression();

		/**
		 * The meta object literal for the '<em><b>Source Exp</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_CALL_EXPRESSION__SOURCE_EXP = eINSTANCE.getOperationCallExpression_SourceExp();

		/**
		 * The meta object literal for the '<em><b>Operation Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_CALL_EXPRESSION__OPERATION_NAME = eINSTANCE.getOperationCallExpression_OperationName();

		/**
		 * The meta object literal for the '<em><b>Argument Exps</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_CALL_EXPRESSION__ARGUMENT_EXPS = eINSTANCE.getOperationCallExpression_ArgumentExps();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractFromVarOperationCallExpressionImpl <em>Subtract From Var Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractFromVarOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getSubtractFromVarOperationCallExpression()
		 * @generated
		 */
		EClass SUBTRACT_FROM_VAR_OPERATION_CALL_EXPRESSION = eINSTANCE.getSubtractFromVarOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractiveOperationCallExpressionImpl <em>Subtractive Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.SubtractiveOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getSubtractiveOperationCallExpression()
		 * @generated
		 */
		EClass SUBTRACTIVE_OPERATION_CALL_EXPRESSION = eINSTANCE.getSubtractiveOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.PowerOperationCallExpressionImpl <em>Power Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.PowerOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getPowerOperationCallExpression()
		 * @generated
		 */
		EClass POWER_OPERATION_CALL_EXPRESSION = eINSTANCE.getPowerOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.SinusOperationCallExpressionImpl <em>Sinus Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.SinusOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getSinusOperationCallExpression()
		 * @generated
		 */
		EClass SINUS_OPERATION_CALL_EXPRESSION = eINSTANCE.getSinusOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.CosinusOperationCallExpressionImpl <em>Cosinus Operation Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.CosinusOperationCallExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getCosinusOperationCallExpression()
		 * @generated
		 */
		EClass COSINUS_OPERATION_CALL_EXPRESSION = eINSTANCE.getCosinusOperationCallExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.FunctionExpressionImpl <em>Function Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.FunctionExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getFunctionExpression()
		 * @generated
		 */
		EClass FUNCTION_EXPRESSION = eINSTANCE.getFunctionExpression();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.coolcomponents.expressions.operators.impl.IncrementOperatorExpressionImpl <em>Increment Operator Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.IncrementOperatorExpressionImpl
		 * @see org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsPackageImpl#getIncrementOperatorExpression()
		 * @generated
		 */
		EClass INCREMENT_OPERATOR_EXPRESSION = eINSTANCE.getIncrementOperatorExpression();

	}

} //OperatorsPackage
