/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.operators;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.expressions.operators.OperatorsPackage
 * @generated
 */
public interface OperatorsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperatorsFactory eINSTANCE = org.coolsoftware.coolcomponents.expressions.operators.impl.OperatorsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Additive Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Additive Operation Call Expression</em>'.
	 * @generated
	 */
	AdditiveOperationCallExpression createAdditiveOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Add To Var Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Add To Var Operation Call Expression</em>'.
	 * @generated
	 */
	AddToVarOperationCallExpression createAddToVarOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Conjunction Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conjunction Operation Call Expression</em>'.
	 * @generated
	 */
	ConjunctionOperationCallExpression createConjunctionOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Decrement Operator Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Decrement Operator Expression</em>'.
	 * @generated
	 */
	DecrementOperatorExpression createDecrementOperatorExpression();

	/**
	 * Returns a new object of class '<em>Disjunction Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Disjunction Operation Call Expression</em>'.
	 * @generated
	 */
	DisjunctionOperationCallExpression createDisjunctionOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Division Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Division Operation Call Expression</em>'.
	 * @generated
	 */
	DivisionOperationCallExpression createDivisionOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equals Operation Call Expression</em>'.
	 * @generated
	 */
	EqualsOperationCallExpression createEqualsOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Greater Than Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Greater Than Operation Call Expression</em>'.
	 * @generated
	 */
	GreaterThanOperationCallExpression createGreaterThanOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Greater Than Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Greater Than Equals Operation Call Expression</em>'.
	 * @generated
	 */
	GreaterThanEqualsOperationCallExpression createGreaterThanEqualsOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Implication Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Implication Operation Call Expression</em>'.
	 * @generated
	 */
	ImplicationOperationCallExpression createImplicationOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Less Than Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Less Than Operation Call Expression</em>'.
	 * @generated
	 */
	LessThanOperationCallExpression createLessThanOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Less Than Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Less Than Equals Operation Call Expression</em>'.
	 * @generated
	 */
	LessThanEqualsOperationCallExpression createLessThanEqualsOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Multiplicative Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplicative Operation Call Expression</em>'.
	 * @generated
	 */
	MultiplicativeOperationCallExpression createMultiplicativeOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Negative Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Negative Operation Call Expression</em>'.
	 * @generated
	 */
	NegativeOperationCallExpression createNegativeOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Negation Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Negation Operation Call Expression</em>'.
	 * @generated
	 */
	NegationOperationCallExpression createNegationOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Not Equals Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Equals Operation Call Expression</em>'.
	 * @generated
	 */
	NotEqualsOperationCallExpression createNotEqualsOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Subtract From Var Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subtract From Var Operation Call Expression</em>'.
	 * @generated
	 */
	SubtractFromVarOperationCallExpression createSubtractFromVarOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Subtractive Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subtractive Operation Call Expression</em>'.
	 * @generated
	 */
	SubtractiveOperationCallExpression createSubtractiveOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Power Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Power Operation Call Expression</em>'.
	 * @generated
	 */
	PowerOperationCallExpression createPowerOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Sinus Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sinus Operation Call Expression</em>'.
	 * @generated
	 */
	SinusOperationCallExpression createSinusOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Cosinus Operation Call Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cosinus Operation Call Expression</em>'.
	 * @generated
	 */
	CosinusOperationCallExpression createCosinusOperationCallExpression();

	/**
	 * Returns a new object of class '<em>Function Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Expression</em>'.
	 * @generated
	 */
	FunctionExpression createFunctionExpression();

	/**
	 * Returns a new object of class '<em>Increment Operator Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Increment Operator Expression</em>'.
	 * @generated
	 */
	IncrementOperatorExpression createIncrementOperatorExpression();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OperatorsPackage getOperatorsPackage();

} //OperatorsFactory
