/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.variables.impl;

import org.coolsoftware.coolcomponents.expressions.WellFormednessRuleException;
import org.coolsoftware.coolcomponents.expressions.impl.ExpressionImpl;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableDeclaration;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Variable Declaration</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.impl.VariableDeclarationImpl#getDeclaredVariable <em>Declared Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VariableDeclarationImpl extends ExpressionImpl implements
		VariableDeclaration {
	/**
	 * The cached value of the '{@link #getDeclaredVariable() <em>Declared Variable</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getDeclaredVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable declaredVariable;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariablesPackage.Literals.VARIABLE_DECLARATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getDeclaredVariable() {
		return declaredVariable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeclaredVariable(
			Variable newDeclaredVariable, NotificationChain msgs) {
		Variable oldDeclaredVariable = declaredVariable;
		declaredVariable = newDeclaredVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE, oldDeclaredVariable, newDeclaredVariable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeclaredVariable(Variable newDeclaredVariable) {
		if (newDeclaredVariable != declaredVariable) {
			NotificationChain msgs = null;
			if (declaredVariable != null)
				msgs = ((InternalEObject)declaredVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE, null, msgs);
			if (newDeclaredVariable != null)
				msgs = ((InternalEObject)newDeclaredVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE, null, msgs);
			msgs = basicSetDeclaredVariable(newDeclaredVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE, newDeclaredVariable, newDeclaredVariable));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE:
				return basicSetDeclaredVariable(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE:
				return getDeclaredVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE:
				setDeclaredVariable((Variable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE:
				setDeclaredVariable((Variable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariablesPackage.VARIABLE_DECLARATION__DECLARED_VARIABLE:
				return declaredVariable != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.coolsoftware.coolcomponents.expressions.impl.ExpressionImpl#computeResultType()
	 * 
	 * @generated NOT
	 */
	@Override
	protected CcmType computeResultType() {
		return declaredVariable.getResultType();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.coolsoftware.coolcomponents.expressions.impl.ExpressionImpl#checkMyWFRs()
	 * 
	 * @generated NOT
	 */
	@Override
	protected void checkMyWFRs() throws WellFormednessRuleException {

		/* Validate variable. */
		if (this.declaredVariable == null)
			throw new WellFormednessRuleException(
					"The declared Variable of a variable declaration cannot be null.",
					this);
		// no else.

		/* Validate initial Expression. */
		if (declaredVariable.getInitialExpression() != null) {
			declaredVariable.getInitialExpression().checkWFRs();
			CcmType varType = declaredVariable.getResultType();
			CcmType initType = declaredVariable.getInitialExpression()
					.getResultType();

			if (!initType.conformsTo(varType))
				throw new WellFormednessRuleException(
						"The type of the initial expression does not conform to the variable's type.",
						declaredVariable.getInitialExpression());
			// no else.
		}
		// no else.

		if (declaredVariable.getInitialExpression() == null
				&& declaredVariable.getType() == TypeLiteral.VALUE)
			throw new WellFormednessRuleException(
					"Either the type or an initial expression for the variable must be given.",
					this);
		// no else.
	}
} // VariableDeclarationImpl
