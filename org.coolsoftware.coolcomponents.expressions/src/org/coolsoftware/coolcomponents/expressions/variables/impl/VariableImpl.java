/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.variables.impl;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Variable</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.impl.VariableImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.impl.VariableImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.impl.VariableImpl#getInitialExpression <em>Initial Expression</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.impl.VariableImpl#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VariableImpl extends EObjectImpl implements Variable {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final TypeLiteral TYPE_EDEFAULT = TypeLiteral.VALUE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected TypeLiteral type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInitialExpression() <em>Initial Expression</em>}' containment reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getInitialExpression()
	 * @generated
	 * @ordered
	 */
	protected Expression initialExpression;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected CcmValue value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariablesPackage.Literals.VARIABLE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariablesPackage.VARIABLE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Expression getInitialExpression() {
		return initialExpression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public NotificationChain basicSetInitialExpression(
			Expression newInitialExpression, NotificationChain msgs) {
		Expression oldInitialExpression = initialExpression;
		initialExpression = newInitialExpression;

		/*
		 * Probably update the variables type (but only if not stated
		 * explicitly.
		 */
		if (this.type == TypeLiteral.VALUE) {
			if (initialExpression.getResultType() instanceof CcmBoolean)
				this.setType(TypeLiteral.BOOLEAN);
			else if (initialExpression.getResultType() instanceof CcmInteger)
				this.setType(TypeLiteral.INTEGER);
			else if (initialExpression.getResultType() instanceof CcmReal)
				this.setType(TypeLiteral.REAL);
			else if (initialExpression.getResultType() instanceof CcmString)
				this.setType(TypeLiteral.STRING);
			else
				this.setType(TypeLiteral.VALUE);
		}
		// no else.

		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					VariablesPackage.VARIABLE__INITIAL_EXPRESSION,
					oldInitialExpression, newInitialExpression);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialExpression(Expression newInitialExpression) {
		if (newInitialExpression != initialExpression) {
			NotificationChain msgs = null;
			if (initialExpression != null)
				msgs = ((InternalEObject)initialExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VariablesPackage.VARIABLE__INITIAL_EXPRESSION, null, msgs);
			if (newInitialExpression != null)
				msgs = ((InternalEObject)newInitialExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VariablesPackage.VARIABLE__INITIAL_EXPRESSION, null, msgs);
			msgs = basicSetInitialExpression(newInitialExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariablesPackage.VARIABLE__INITIAL_EXPRESSION, newInitialExpression, newInitialExpression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public TypeLiteral getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(TypeLiteral newType) {
		TypeLiteral oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariablesPackage.VARIABLE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CcmValue getValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject)value;
			value = (CcmValue)eResolveProxy(oldValue);
			if (value != oldValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariablesPackage.VARIABLE__VALUE, oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public CcmValue basicGetValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(CcmValue newValue) {
		CcmValue oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariablesPackage.VARIABLE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public CcmType getResultType() {
		if (initialExpression != null && type == TypeLiteral.VALUE)
			return initialExpression.getResultType();
		else {
			switch (type) {
			case BOOLEAN:
				return CcmStandardLibraryTypeFactory.eINSTANCE.getBooleanType();
			case INTEGER:
				return CcmStandardLibraryTypeFactory.eINSTANCE.getIntegerType();
			case REAL:
				return CcmStandardLibraryTypeFactory.eINSTANCE.getRealType();
			case STRING:
				return CcmStandardLibraryTypeFactory.eINSTANCE.getStringType();
			default:
				return CcmStandardLibraryTypeFactory.eINSTANCE.getValueType();
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariablesPackage.VARIABLE__INITIAL_EXPRESSION:
				return basicSetInitialExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariablesPackage.VARIABLE__NAME:
				return getName();
			case VariablesPackage.VARIABLE__TYPE:
				return getType();
			case VariablesPackage.VARIABLE__INITIAL_EXPRESSION:
				return getInitialExpression();
			case VariablesPackage.VARIABLE__VALUE:
				if (resolve) return getValue();
				return basicGetValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariablesPackage.VARIABLE__NAME:
				setName((String)newValue);
				return;
			case VariablesPackage.VARIABLE__TYPE:
				setType((TypeLiteral)newValue);
				return;
			case VariablesPackage.VARIABLE__INITIAL_EXPRESSION:
				setInitialExpression((Expression)newValue);
				return;
			case VariablesPackage.VARIABLE__VALUE:
				setValue((CcmValue)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariablesPackage.VARIABLE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case VariablesPackage.VARIABLE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case VariablesPackage.VARIABLE__INITIAL_EXPRESSION:
				setInitialExpression((Expression)null);
				return;
			case VariablesPackage.VARIABLE__VALUE:
				setValue((CcmValue)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariablesPackage.VARIABLE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case VariablesPackage.VARIABLE__TYPE:
				return type != TYPE_EDEFAULT;
			case VariablesPackage.VARIABLE__INITIAL_EXPRESSION:
				return initialExpression != null;
			case VariablesPackage.VARIABLE__VALUE:
				return value != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} // VariableImpl
