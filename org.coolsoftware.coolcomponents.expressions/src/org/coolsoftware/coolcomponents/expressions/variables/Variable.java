/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.expressions.variables;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getName <em>Name</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getType <em>Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getInitialExpression <em>Initial Expression</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage#getVariable()
 * @model
 * @generated
 */
public interface Variable extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage#getVariable_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Initial Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Expression</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Expression</em>' containment reference.
	 * @see #setInitialExpression(Expression)
	 * @see org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage#getVariable_InitialExpression()
	 * @model containment="true"
	 * @generated
	 */
	Expression getInitialExpression();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getInitialExpression <em>Initial Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Expression</em>' containment reference.
	 * @see #getInitialExpression()
	 * @generated
	 */
	void setInitialExpression(Expression value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral
	 * @see #setType(TypeLiteral)
	 * @see org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage#getVariable_Type()
	 * @model required="true"
	 * @generated
	 */
	TypeLiteral getType();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.coolsoftware.coolcomponents.types.stdlib.TypeLiteral
	 * @see #getType()
	 * @generated
	 */
	void setType(TypeLiteral value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(CcmValue)
	 * @see org.coolsoftware.coolcomponents.expressions.variables.VariablesPackage#getVariable_Value()
	 * @model required="true" derived="true"
	 * @generated
	 */
	CcmValue getValue();

	/**
	 * Sets the value of the '{@link org.coolsoftware.coolcomponents.expressions.variables.Variable#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(CcmValue value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	CcmType getResultType();

} // Variable
