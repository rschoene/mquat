SYNTAXDEF exp
FOR <http://www.cool-software.org/dimm/expressions> <expressions.genmodel>
START Expression, Block

OPTIONS {
	reloadGeneratorModel = "true";
	disableDebugSupport = "true";
	disableLaunchSupport = "true";
}

TOKENS {
	DEFINE SL_COMMENT
		$ '//'(~('\n'|'\r'|'\uffff'))* $;
	DEFINE ML_COMMENT
		$ '/*'.*'*/'$;

	// Literals
	DEFINE ILT $'-'?('0'..'9')+(('e'|'E')('-')?('0'..'9')+)?$;
	DEFINE REAL_LITERAL ILT + $'.'$ + ILT;
	DEFINE TYPE_LITERAL $('Boolean' | 'Integer' | 'Real' | 'String')$;
}

TOKENSTYLES {
	"SL_COMMENT" COLOR #008000, ITALIC;
	"ML_COMMENT" COLOR #008000, ITALIC;
}

RULES {

	Block ::= "{" #1 expressions #0 (";" #1 expressions)* #1 "}";
	
	// Literals (highest precedence)
	@Operator(type="primitive", weight="91", superclass="Expression")
	Literals.IntegerLiteralExpression ::= 
		value[ILT];
	
	@Operator(type="primitive", weight="91", superclass="Expression")
	Literals.RealLiteralExpression ::= 
		value[REAL_LITERAL];
	
	@Operator(type="primitive", weight="91", superclass="Expression")
	Literals.BooleanLiteralExpression ::= 
		value["true" : "false"];
	
	@Operator(type="primitive", weight="91", superclass="Expression")
	Literals.StringLiteralExpression	::= 
		value['"', '"'];	

	@Operator(type="primitive", weight="91", superclass="Expression")
	ParenthesisedExpression	::= 
		"(" #1 sourceExp #1 ")";


	// increment, decrement, negation
	@Operator(type="unary_postfix", weight="88", superclass="Expression")
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.IncrementOperatorExpression ::= 
		sourceExp #1 "++";

	@Operator(type="unary_postfix", weight="88", superclass="Expression")
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.DecrementOperatorExpression ::= 
		sourceExp #1 "--";

	@Operator(type="unary_prefix", weight="87", superclass="Expression")
	@SuppressWarnings(featureWithoutSyntax)
	Operators.NegativeOperationCallExpression ::= 
		"not" #1 sourceExp;
		
	@Operator(type="unary_prefix", weight="80", superclass="Expression")
	@SuppressWarnings(featureWithoutSyntax)
	Operators.SinusOperationCallExpression ::= 
		"sin" #1 sourceExp;	
		
	@Operator(type="unary_prefix", weight="80", superclass="Expression")
	@SuppressWarnings(featureWithoutSyntax)
	Operators.CosinusOperationCallExpression ::= 
		"cos" #1 sourceExp;		

	@Operator(type="unary_prefix", weight="5", superclass="Expression")
	@SuppressWarnings(featureWithoutSyntax)
	Operators.FunctionExpression ::= 
		"f" #0 sourceExp;

	// arithmetic operations
	@Operator(type="binary_left_associative", weight="21", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.AdditiveOperationCallExpression ::= 
		sourceExp #1 "+" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="21", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.SubtractiveOperationCallExpression ::= 
		sourceExp #1 "-" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="90", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.AddToVarOperationCallExpression ::= 
		sourceExp #1 "+=" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="90", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.SubtractFromVarOperationCallExpression ::= 
		sourceExp #1 "-=" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="22", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.MultiplicativeOperationCallExpression ::= 
		sourceExp #1 "*" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="22", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.DivisionOperationCallExpression ::= 
		sourceExp #1 "/" #1 argumentExps;
		
	@Operator(type="binary_left_associative", weight="19", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.PowerOperationCallExpression ::= 
		sourceExp #1 "^" #1 argumentExps;	

	// relational operations
	@Operator(type="binary_left_associative", weight="11", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.GreaterThanOperationCallExpression ::= 
		sourceExp #1 ">" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="11", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.GreaterThanEqualsOperationCallExpression ::= 
		sourceExp #1 ">=" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="11", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.LessThanOperationCallExpression ::= 
		sourceExp #1 "<" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="11", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.LessThanEqualsOperationCallExpression ::= 
		sourceExp #1 "<=" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="12", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.EqualsOperationCallExpression ::= 
		sourceExp #1 "==" #1 argumentExps;

	@Operator(type="binary_left_associative", weight="12", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.NotEqualsOperationCallExpression ::= 
		sourceExp #1 "!=" #1 argumentExps;


	// boolean operations
	@Operator(type="binary_left_associative", weight="03", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.DisjunctionOperationCallExpression ::= 
		sourceExp #1 "or" #1 argumentExps;
		
	@Operator(type="binary_left_associative", weight="03", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.ConjunctionOperationCallExpression ::= 
		sourceExp #1 "and" #1 argumentExps;
	
	@Operator(type="binary_left_associative", weight="04", superclass="Expression")
	@SuppressWarnings(minOccurenceMismatch)
	@SuppressWarnings(featureWithoutSyntax)
	@SuppressWarnings(leftRecursiveRule)
	Operators.ImplicationOperationCallExpression ::= 
		sourceExp #1 "implies" #1 argumentExps;
		
	@Operator(type="unary_prefix", weight="05", superclass="Expression")
	@SuppressWarnings(featureWithoutSyntax)
	Operators.NegationOperationCallExpression	::= 
		"!" #0 sourceExp;

		
	




		
	@Operator(type="primitive", weight="91", superclass="Expression")
	Variables.VariableDeclaration ::=
		"var" #1 declaredVariable #0;
		
	@SuppressWarnings(minOccurenceMismatch)
	Variables.Variable ::=
		name[TEXT] (#1 ":" #1 type[TYPE_LITERAL])? (#1 "=" #1 initialExpression)?;
		
	@Operator(type="primitive", weight="91", superclass="Expression")
	Variables.VariableCallExpression ::=
		referredVariable[TEXT];
		
	@Operator(type="binary_left_associative", weight="6", superclass="Expression")
	@SuppressWarnings(leftRecursiveRule)
	@SuppressWarnings(minOccurenceMismatch)
	Variables.VariableAssignment ::=
		referredVariable #1 "=" #1 valueExpression;
}