/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.scaling.handbrake;

import java.io.File;
import java.util.List;

import org.haec.app.scaling.AbstractScaling;
import org.haec.app.videotranscodingserver.ScalingScale;
import org.haec.apps.util.ErrorState;
import org.haec.apps.util.VideoResolution;
import org.haec.apps.util.VideoResolutionCalculator;
import org.haec.apps.util.VideoUtil;
import org.haec.handbrake.HandbrakeUtil;
import org.haec.theatre.api.Benchmark;

/**
 * Implementation of org.haec.app.scaling.handbrake.ScalingHandbrake
 * @author René Schöne
 */
public class ScalingHandbrake extends AbstractScaling implements ScalingScale {

	public ScalingHandbrake() {
		super(HandbrakeUtil.sharedInstance);
	}

	@Override
	protected void addArguments(List<String> params, File fileVideo, int w,
			int h, boolean mult, File out, ErrorState ignore) {
		params.add("-i");
		params.add(fileVideo.getAbsolutePath());
		params.add("-o");
		params.add(out.getAbsolutePath());
		addScaleArguments(params, fileVideo, w, h, mult);
		addOtherArguments(params);
	}

	private void addScaleArguments(List<String> params, File fileVideo, int w, int h,
			boolean mult) {
		VideoResolution source = null;
		if(mult) {
			VideoResolutionCalculator calc = getResolutionCalculator();
			if(calc == null) {
				log.warn("No resolution calculator found, thus no source resolution available, option mult ignored.");
				return;
			}
			source = calc.getVideoResolution(fileVideo);
			if(source.equals(VideoResolution.UNKNOWN)) {
				log.warn("Unknown source resolution calculated, option mult ignored.");
				return;
			}
		}
		VideoResolution target = convert(source, w, h, mult);
		params.add("--width");
		params.add(Integer.toString(target.width));
		params.add("--height");
		params.add(Integer.toString(target.height));
	}
	
	private void addOtherArguments(List<String> params) {
		// just make one preview image
		params.add("--previews");
		params.add("1:0");
		if(util.shouldEncodeNoSound()) {
			params.add("-a");
			params.add("none");
		}
	}

	@Override
	protected int handleProcess(Process startedProcess, VideoUtil util) {
		return util.waitForever(startedProcess, true);
	}
	
	@Override
	protected VideoResolutionCalculator getResolutionCalculator() {
		// use handbrake
		return HandbrakeUtil.sharedInstance;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.scaling.handbrake.ScalingHandbrake";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new ScalingHandbrakeBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.scaling.handbrake";
	}

}