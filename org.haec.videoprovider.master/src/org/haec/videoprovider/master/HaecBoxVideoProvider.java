/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoprovider.master;

import java.io.File;
import java.util.List;

import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.videoplatform.resolver.IVideoPlatformResolver;
import org.haec.videos.Video;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.haec.videoprovider.ExampleVideoProvider;
import org.haec.videoprovider.VideoProvider;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * Video provider using the videoplatform of the HAEC-Box prototype to provide suitable videos.
 * TODO split into master and slave part?
 * @author René Schöne
 */
public class HaecBoxVideoProvider extends ExampleVideoProvider implements VideoProvider {

	public static final String NAME = "haec-box";
	public static List<org.haec.videoplatform.resolver.Video> vpVideoList;
	private BundleContext bundleContext;
	
	public HaecBoxVideoProvider() {
		super();
	}
	
	@Override
	protected void initVideoByLength() {
		log.debug("Getting video list");
		ensureVpVideoList();
		if(vpVideoList == null) {
			log.error("Could not get videoList.");
			return;
		}
		log.debug("Got " + vpVideoList.size() + " videos." );
		for(org.haec.videoplatform.resolver.Video vpVideo : vpVideoList) {
			String extension = FileUtils.splitExt(vpVideo.fname).extension;
			Video v = new Video(new File(settings.getResourceDir().get(), vpVideo.fname),
					(int) vpVideo.duration, vpVideo.width, vpVideo.height, extension);
			for(String hostname : vpVideo.hosts) {
				if(hostname.trim().isEmpty()) { /*log.debug("Skipping " + v);*/ continue; }
				addVideo(v, hostname);
			}
		}
	}

	private void ensureVpVideoList() {
		if(vpVideoList == null) {
			IVideoPlatformResolver resolver = getResolver();
			if(resolver == null) {
				log.fatal("Could not find resolver. Expect errors.");
				return;
			}
			vpVideoList = resolver.getVideoList("*");
		}
	}

	private IVideoPlatformResolver getResolver() {
		try {
			ServiceReference<IVideoPlatformResolver> ref;
			// try local first
			ref = bundleContext.getServiceReference(IVideoPlatformResolver.class);
			if(ref != null) {
				return bundleContext.getService(ref);
			}
			// try remotely next
			RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(bundleContext);
			return RemoteOsgiUtil.getRemoteService(remote, settings.getGemUri().get(), IVideoPlatformResolver.class);
		} catch (Exception e) {
			log.warn("Got exception while retrieving resolver");
		}
		return null;
	}

	@Override
	public String getName() {
		return NAME;
	}
	
	protected void activate(ComponentContext ctx) {
		this.bundleContext = ctx.getBundleContext();
	}


}
