/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.notifier.justlog;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.energymanager.IJobPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.IJobResultNotifier;
import org.coolsoftware.theatre.energymanager.ITaskPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.ITaskResultNotifier;
import org.coolsoftware.theatre.energymanager.JobDescription;
import org.coolsoftware.theatre.energymanager.JobResultDescription;
import org.coolsoftware.theatre.energymanager.TaskDescription;
import org.coolsoftware.theatre.energymanager.TaskResultDescription;

/**
 * Notifier implementing every Notifier interface and logs each notification.
 * @author René Schöne
 */
public class JustLog implements IJobPreExecuteNotifier, IJobResultNotifier,
	ITaskPreExecuteNotifier, ITaskResultNotifier {
	
	private Logger log = Logger.getLogger(JustLog.class);

	@Override
	public void afterTaskFinish(TaskResultDescription trd) {
		log.debug("Task-finished: " + trd);
	}

	@Override
	public void beforeTaskExecute(TaskDescription td) {
		log.debug("task-started: " + td);
	}

	@Override
	public void afterJobFinish(JobResultDescription jrd) {
		log.debug("Job-finished: " + jrd);
	}

	@Override
	public void beforeJobExecute(JobDescription jd) {
		log.debug("job-started: " + jd);
	}
	

}
