/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.theatre.connector;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.net.URI;
import java.security.ProtectionDomain;
import java.util.Map;
import java.util.Properties;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

/**
 * 
 * @author Sebastian Götz
 */
public class ActivatorTransformer implements ClassFileTransformer {

	@Override
	public byte[] transform(ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {
		
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(new File("lib.properties")));
			String white = prop.get("whitelist").toString();
			String osgi = prop.get("osgi").toString();
			String rosgi = prop.get("rosgi").toString();
			String gem = prop.get("gem").toString();
		
		if (className.contains(white) && className.contains("activator")) {
			System.out.println(className);
			System.out.println(classfileBuffer);			
			
			try {
				ClassPool cp = ClassPool.getDefault();
				cp.appendClassPath(osgi);
				cp.appendClassPath(rosgi);
				cp.appendClassPath(gem);
				
				CtClass c = cp.makeClass(new ByteArrayInputStream(classfileBuffer));
				System.out.println("## "+c.getName());
				for(CtClass i : c.getInterfaces()) {
					if(i.getName().equals("org.osgi.framework.BundleActivator")) {
						System.out.println("this one is mine! ("+c+")");
						for(CtMethod m : c.getMethods()) {
							if(m.getName().equals("start")) {
								
								String registrationCode = 
									"org.osgi.framework.ServiceReference remoteRef = context.getServiceReference(ch.ethz.iks.r_osgi.RemoteOSGiService.class.getName());"
+	"if(remoteRef == null) {"
+	"	 System.out.println(\"Error: R-OSGi not found!\");"
+	"}"
+	"ch.ethz.iks.r_osgi.RemoteOSGiService remote = (ch.ethz.iks.r_osgi.RemoteOSGiService)context.getService(remoteRef);"
+	"final ch.ethz.iks.r_osgi.RemoteServiceReference[] srefs = "
+	"	remote.getRemoteServiceReferences(new ch.ethz.iks.r_osgi.URI(\"r-osgi://127.0.0.1:9278\"),"
+	"	org.coolsoftware.theatre.energymanager.IGlobalEnergyManager.class.getName(), null);"
	
+	"org.coolsoftware.theatre.energymanager.IGlobalEnergyManager gem = (org.coolsoftware.theatre.energymanager.IGlobalEnergyManager)remote.getRemoteService(srefs[0]);"
//TODO gem.registerApp(String appName, URI containerURI, String structureModel, Map<String, String> contracts);
+	"System.out.println(\"[App] connection to GEM established.\");";
								
								
								m.insertAfter(registrationCode);
							}
						}
					}
				}
				return c.toBytecode();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RuntimeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CannotCompileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(":: ");
			
		
			
		} 
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}
	
		

}
