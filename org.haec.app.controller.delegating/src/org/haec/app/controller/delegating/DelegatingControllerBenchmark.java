/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.controller.delegating;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.PipDoPip;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.BenchmarkData;
import org.haec.videos.Video;
import org.haec.videoprovider.IVideoProviderFactory;
import org.haec.videoprovider.VideoProviderFactory;

class ControllerData implements BenchmarkData {
	File in1;
	File in2;
	int mvl;
	
	public ControllerData(File in1, File in2, int mvl) {
		super();
		this.in1 = in1;
		this.in2 = in2;
		this.mvl = mvl;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.BenchmarkData#getMetaparamValue()
	 */
	@Override
	public int getMetaparamValue() {
		return mvl;
	}
	
}

/**
 * Benchmark for org.haec.app.controller.delegating.DelegatingController
 * @author René Schöne
 */
public class DelegatingControllerBenchmark implements Benchmark {

	DelegatingController comp;
	private int w2;
	private int h2;
	private boolean mult;
	private int pos;
	protected Logger log = Logger.getLogger(DelegatingControllerBenchmark.class);
	private IVideoProviderFactory fac;
	
	public DelegatingControllerBenchmark(IVideoProviderFactory fac) {
		this.comp = new DelegatingController();
		this.fac = fac;
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#getData()
	 */
	@Override
	public List<BenchmarkData> getData() {
		w2 = -2;
		h2 = -2;
		mult = true;
		pos = PipDoPip.POS_CENTER;
		log.info("[CON Bench] Starting");
		List<BenchmarkData> result = new ArrayList<>();
		for(Video[] vids : fac.getCurrentVideoProvider().getTwoVideos(true, false, true, true)) {
			Video video1 = vids[0], video2 = vids[1];
			int mpValue = TranscodingServerUtil.getMetaparameterValue(video1.length, video2.length,
					video1.resX, video1.resY, video2.resX, video2.resY);
			result.add(new ControllerData(video1.file, video2.file, mpValue));
		}
		log.info("[CON Bench] Finished");
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Benchmark#iteration(org.haec.theatre.api.BenchmarkData)
	 */
	@Override
	public Object iteration(BenchmarkData data) {
		ControllerData d = (ControllerData) data;
		log.debug("[CON Bench] - vid1: "+d.in1.getName()+", vid2:"+d.in2.getName());
		return comp.pipScaled(d.in1, d.in2, w2, h2, mult, pos);
	}

}
