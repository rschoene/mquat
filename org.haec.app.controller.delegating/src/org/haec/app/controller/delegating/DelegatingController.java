/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.controller.delegating;

import java.io.File;

import org.apache.log4j.Logger;
import org.haec.app.videotranscodingserver.ControllerPipScaled;
import org.haec.app.videotranscodingserver.TranscodingServerUtil;
import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.TaskBasedImpl;
import org.haec.theatre.utils.FileProxy;
import org.haec.videoprovider.IVideoProviderFactory;

/**
 * Implementation of org.haec.app.controller.delegating.DelegatingController
 * @author René Schöne
 */
public class DelegatingController extends TaskBasedImpl implements ControllerPipScaled {
	
	public final Logger log = Logger.getLogger(DelegatingController.class);
	private IVideoProviderFactory fac;

	public File pipScaled(File fileVideo1, File fileVideo2, int w2, int h2, boolean mult,
			int pos) {
		log.debug("starting");
//		updateDelegatees();
		// scale 2nd file down according to parameters w2,h2,mult.
		FileProxy video2Scaled = scaleNew(fileVideo2, w2, h2, mult);
		// do pip with 1st file and the scaled video according to parameters pos
		FileProxy result = pipNew(fileVideo1, video2Scaled, pos);
		log.debug("[CON] Return value: " + result);
		return result.getFileFor(thisUri);
	}

	protected FileProxy pipNew(File fileVideo1, FileProxy video2Scaled, int pos) {
		String pipName = "PiP";
		String pipPort = "doPip";
		return invoke(FileProxy.class, pipName, pipPort, fileVideo1, video2Scaled, pos);
	}
	
	protected FileProxy scaleNew(File fileVideo2, int w2, int h2, boolean mult) {
		String scaleName = "Scaling";
		String scalePort = "scale";
		return invoke(FileProxy.class, scaleName, scalePort, fileVideo2, w2, h2, mult);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return TranscodingServerUtil.APP_NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "Controller";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.controller.delegating.DelegatingController";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new DelegatingControllerBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.controller.delegating";
	}
	
	protected void setVideoProviderFactory(IVideoProviderFactory fac) {
		this.fac = fac;
	}

// -------------- not used currently --------------

//	private PipDoPip pdp;
//	private ScalingScale scs;
//	private CensorCensor cec;
//	private TranscoderTranscode trt;
//	protected IGlobalEnergyManager gem;

//	protected File pip(File fileVideo1, int pos, File video2Scaled) {
//		return pdp.doPip(fileVideo1, video2Scaled, pos);
//	}
//
//	protected File scale(File fileVideo2, int w2, int h2, boolean mult) {
//		return scs.scale(fileVideo2, w2, h2, mult);
//	}
	
//	protected void updateDelegatees() {
//	Runnable runnable = new Runnable() {
//		
//		@Override
//		public void run() {
//	RemoteOSGiService r = getRemoteOSGiService();
//	String ip = AppUtil.getGEMUri();
//	URI remoteOsgiUri = RemoteOsgiUtil.createRemoteOsgiUri(ip);
//	gem = getFirstMatch(r, remoteOsgiUri, IGlobalEnergyManager.class);
//	if(gem == null) {
//		throw new IllegalStateException("GEM not found.");
//	}
//	List<java.net.URI> listOfLems = gem.getLocalMgrs();
//	X-XX rs: consider to take the right lem, instead of the first
//	URI lemUri = new URI(listOfLems.get(0).toString());
//	ILocalEnergyManager lem = getFirstMatch(r, lemUri, ILocalEnergyManager.class);
//	if(lem == null) {
//		throw new IllegalStateException("LEM not found.");
//	}
//	
//	X-XX rs: Assumption A = only one deployed component at a time
//	try {
//		T-ODO rs: avoid hardcoding names of componentType
//		List<String> pips = lem.getDeployedComponents("PiP");
//		String pipImplName = pips.get(0);
//		PipDoPip implPip = getFirstRemoteImpl(r, lemUri, PipDoPip.class, pipImplName);
//		setDoPip(implPip);
//	}
//	catch (Exception e) {
//		e.printStackTrace();
//		setDoPip(getFirstMatch(r, remoteOsgiUri, PipDoPip.class));
//	}
//	
//	try {
//		T-ODO rs: avoid hardcoding names of componentType
//		List<String> scas = lem.getDeployedComponents("Scaling");
//		String scaImplName = scas.get(0);
//		ScalingScale implSca = getFirstRemoteImpl(r, lemUri, ScalingScale.class, scaImplName);
//		setScale(implSca);
//	}
//	catch (Exception e) {
//		e.printStackTrace();
//		setScale(getFirstMatch(r, remoteOsgiUri, ScalingScale.class));
//	}
//	
////	// using first
////	setCensor(getFirstMatch(r, remoteOsgiUri, CensorCensor.class));
////	setTranscode(getFirstMatch(r, remoteOsgiUri, TranscoderTranscode.class));
//		}
//	};
//	Thread t = new Thread(runnable);
//	t.start();
//	 // wait some time to finish
//	try {
//		int secondsToWait = 40;
//		System.out.println("[CON] Waiting at most " + secondsToWait + " secs to update delegatees.");
//		t.join(secondsToWait * 1000);
//		System.out.println("[CON] Delegatees updated");
//	} catch (InterruptedException ignore) { }
//}
//
//private <T> T getFirstRemoteImpl(RemoteOSGiService r, URI remoteOsgiUri,
//		Class<T> clazz, String expectedImplName) {
//	if(expectedImplName != null) {
//		RemoteServiceReference[] matches = getMatches(r, remoteOsgiUri, clazz.getName());
//		for(RemoteServiceReference sref : matches) {
//			String actualImplName = (String) sref.getProperty(EnhancedImplActivator.IMPL_NAME);
//			if(expectedImplName.equals(actualImplName)) {
//				@SuppressWarnings("unchecked")
//				T result = (T) r.getRemoteService(sref);
//				return result;
//			}
//		}
//	}
//	log.error("No service found for " + clazz.getName() + ", matching " + expectedImplName);
//	return null;
//}
//
//@SuppressWarnings("unchecked")
//protected <T> T getFirstMatch(RemoteOSGiService r,
//		URI remoteOsgiUri, Class<T> clazz) {
//	RemoteServiceReference[] matches = getMatches(r, remoteOsgiUri, clazz.getName());
//	if(matches == null)
//		return null;
//	if(matches.length > 1) {
//		log.warn("Got multiple services for " + clazz.getName() + ". Using first.");
//	}
//	return (T) r.getRemoteService(matches[0]);
//}
//
//@SuppressWarnings("unchecked")
//protected <T> List<T> getAllMatches(RemoteOSGiService r,
//		URI remoteOsgiUri, String className) {
//	RemoteServiceReference[] matches = getMatches(r, remoteOsgiUri, className);
//	List<T> result = new ArrayList<T>(matches.length);
//	for(RemoteServiceReference sref : matches) {
//		result.add((T) r.getRemoteService(sref));
//	}
//	return result;
//}
//
//private RemoteServiceReference[] getMatches(RemoteOSGiService r,
//		URI remoteOsgiUri, String className) {
//	RemoteServiceReference[] srefs = r.getRemoteServiceReferences(remoteOsgiUri,
//			className, null);
//	if(srefs == null) {
//		log.error("No service found for " + className);
//		return null;
//	}
//	return srefs;
//}
//
//private RemoteOSGiService getRemoteOSGiService() {
//	BundleContext context = FrameworkUtil.getBundle(this.getClass())
//			.getBundleContext();
//	ServiceReference<?> remoteRef = context
//			.getServiceReference(RemoteOSGiService.class.getName());
//	if (remoteRef == null) {
//		log.warn("Error: R-OSGi not found!");
//	}
//	RemoteOSGiService remote = (RemoteOSGiService) context
//			.getService(remoteRef);
//	return remote;
//}

//	public synchronized void setDoPip(PipDoPip service) {
//		log.debug("[CON] New doPip: " + service);
//		this.pdp = service;
//	}
//	
//	public synchronized void unsetDoPip(PipDoPip service) {
//		if(this.pdp == service)
//			this.pdp = null;
//	}
//
//	public synchronized void setScale(ScalingScale service) {
//		log.debug("[CON] New scale: " + service);
//		this.scs = service;
//	}
//	
//	public synchronized void unsetScale(ScalingScale service) {
//		if(this.scs == service)
//			this.scs = null;
//	}
//
//	public synchronized void setCensor(CensorCensor service) {
//		log.debug("[CON] New censor: " + service);
//		this.cec = service;
//	}
//	
//	public synchronized void unsetCensor(CensorCensor service) {
//		if(this.cec == service)
//			this.cec = null;
//	}
//
//	public synchronized void setTranscode(TranscoderTranscode service) {
//		log.debug("[CON] New transcode: " + service);
//		this.trt = service;
//	}
//	
//	public synchronized void unsetTranscode(TranscoderTranscode service) {
//		if(this.trt == service)
//			this.trt = null;
//	}

}