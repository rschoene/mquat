/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.ilp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.structure.ComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.CopyFileType;
import org.coolsoftware.coolcomponents.ccm.structure.Order;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.Property;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.SWConnectorType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.ContainerProvider;
import org.coolsoftware.coolcomponents.ccm.variant.Job;
import org.coolsoftware.coolcomponents.ccm.variant.JobStatus;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.SWComponent;
import org.coolsoftware.coolcomponents.ccm.variant.Schedule;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.CcmBoolean;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.coolcomponents.types.stdlib.StdlibFactory;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.Optimizer;
import org.coolsoftware.theatre.resourcemanager.IGlobalResourceManager;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.GeneratorUtil;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.GlpkException;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_cpxcp;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;
import org.haec.optimizer.ilp.GLPK_ILPGenerator.ProblemBuffer.StartTimeConstraint;
import org.haec.optimizer.ilp.GLPK_ILPGenerator.ProblemBuffer.StartTimeConstraint.EndTime;
import org.haec.optimizer.ilp.GLPK_ILPGenerator.ProblemBuffer.StartTimeConstraint.QueueLengthPerImpl;
import org.haec.optimizer.ilp.GLPK_ILPGenerator.ProblemBuffer.StartTimeConstraint.ResponseTimeReqs;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.Cache;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.FileUtils;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.Function2;
import org.haec.theatre.utils.StringUtils;

/**
 * The planner component responsible for scheduling implementations on containers.
 * This planner uses the GNU GLPK implementation to solve the ILP problem.
 * @author René Schöne
 * @see <a href="http://www.gnu.org/software/glpk/">http://www.gnu.org/software/glpk/<a>
 */
public class GLPK_ILPGenerator implements Optimizer {
	
	private Map<String, List<String>> debugging = new TreeMap<>();
	
	class UsageVarAndValue {
		String usageVar;
		CcmValue val;
		public UsageVarAndValue(String usageVar, CcmValue val) {
			this.usageVar = usageVar; this.val = val;
		}
	}

	class ProblemBuffer {
		/** {@link SWComponentType} + {@link String} */
		class CompTypeString {
			SWComponentType s1;
			String s2;
			@Override
			public String toString() {
				return "CompTypeString [s1=" + s1 + ", s2=" + s2 + "]";
			}
		}
		/** ComponentName + [ {@link CompTypeReq} ] */
		class CompTypeReq {
			public CompTypeReq(String compName) {
				this.compName = compName;
			}
			String compName;
			List<CompTypeReq> reqs = new ArrayList<>();
			@Override
			public String toString() {
				return "CTR [compName=" + compName + ", reqs=" + reqs
						+ "]";
			}
		}
		/** column of variable + upper bound */
		class VarnameAndUpperBound {
			int varcol;
			CcmValue upperBound;
			public VarnameAndUpperBound(String varname, CcmValue upperBound) {
				super();
				this.varcol = toCol(varname, true);
				this.upperBound = upperBound;
			}
		}
		/** column of variable + upper bound + kind (equals, less, greater) */
		class SingleConstraint {
			int varcol;
			CcmValue val;
			int kind;
			public SingleConstraint(String varname, CcmValue val, int kind) {
				this.varcol = toCol(varname, true);
				this.val = val; this.kind = kind;
			}
			@Override
			public String toString() {
				return "SingleConstraint [varname=" + toName(varcol) + ", val=" + val
						+ ", kind=" + kind + "]";
			}
		}
		/** column of variable + List of (variable,value) + kind (equals, less, greater) */
		class ListConstraint {
			int varcol;
			List<VarnameAndWeight> reqList;
			int kind;
			public ListConstraint(String varname,
					List<UsageVarAndValue> reqList, int kind) {
				this.varcol = toCol(varname, true);
				this.reqList = parse(reqList);
				this.kind = kind;
			}
			private List<VarnameAndWeight> parse(List<UsageVarAndValue> list) {
				List<VarnameAndWeight> result = new ArrayList<VarnameAndWeight>(list.size());
				for(UsageVarAndValue uvav : list) {
					result.add(new VarnameAndWeight(uvav.usageVar, CCMUtil.getRealValue(uvav.val)));
				}
				return result;
			}
			@Override
			public String toString() {
				return "ListConstraint [" + toName(varcol) + kindtoString(kind)
						+ reqList + "]";
			}
			private String kindtoString(int kind) {
				switch(kind) {
				case CONSTRAINT_EQUAL: return "=";
				case CONSTRAINT_GREATER_THAN_REQS: return ">=";
				case CONSTRAINT_LESS_THAN_REQS: return "<=";
				default: return "?";
				}
			}
		}
		/** Two Lists of variables (for each side of the equality) + constant value */
		class EqualsConstraint {
			List<Integer> positives;
			List<Integer> negatives;
			int val;
			public EqualsConstraint(List<String> positives,
					Set<String> negatives) {
				this.positives = mapToCol(positives);
				this.negatives = mapToCol(negatives);
				this.val = 0;
			}
			public EqualsConstraint(List<String> positives, int val) {
				this.positives = mapToCol(positives);
				this.negatives = Collections.emptyList();
				this.val = val;
			}
			@Override
			public String toString() {
				return "EqualsConstraint [positives=" + mapToName(positives)
						+ ", negatives=" + mapToName(negatives) + ", val=" + val + "]";
			}
			private String mapToName(List<Integer> list) {
				StringBuilder sb = new StringBuilder("[");
				boolean first = true;
				for(Integer i : list) {
					if(first) { first = false; }
					else { sb.append(","); }
					sb.append(toName(i));
				}
				return sb.append("]").toString();
			}
		}
		/** Name of startTime-variable + componentType + List of {@link QueueLengthPerImpl} <br>
		 *  + { reqCompName -> {@link ResponseTimeReqs} }  */
		class StartTimeConstraint {
			/** QueueLength + binaryVar */
			class QueueLengthPerImpl {
				double queueLengthOfServer;
				String bvn;
				@Override
				public String toString() {
					return "QLPI [queueLengthOfServer="
							+ queueLengthOfServer + ", bvn=" + bvn + "]";
				}
			}
			/** reqComponent-startTimeVar + [ {@link EndTime} ] */
			class ResponseTimeReqs {
				String reqCompStartTime;
				List<EndTime> endTimes = new ArrayList<>();
				@Override
				public String toString() {
					return "RTR [reqCompStartTime="
							+ reqCompStartTime + ", endTimes=" + endTimes
							+ "]";
				}
			}
			/** predictedRunTime + binaryVar */
			class EndTime {
				double predictedRunTimeOfImplOnServer;
				String bvn;
				@Override
				public String toString() {
					return "ET [predictedRunTimeOfImplOnServer="
							+ predictedRunTimeOfImplOnServer + ", bvn="
							+ bvn + "]";
				}
			}
			final String startTimeName;
			final SWComponentType compType;
			boolean validQueueTimes = false;
			boolean validEndTimes = false;
			List<QueueLengthPerImpl> qlpis = new ArrayList<>();
			Map<String, ResponseTimeReqs> rtrs = new HashMap<>();
			
			public StartTimeConstraint(SWComponentType compType) {
				this.compType = compType;
				this.startTimeName = startProp(compType.getName());
				ensureEclVariable(startTimeName);
			}
			@Override
			public String toString() {
				return "StartTimeConstraint [startTimeName="
						+ startTimeName + ", qlpis=" + qlpis + ", rtrs="
						+ rtrs + "]";
			}
			public void addQueueLength(double queueLength, String bvn) {
				if(existingQLPI(bvn, queueLength)) {
					return;
				}
				QueueLengthPerImpl qlpi = new QueueLengthPerImpl();
				qlpi.queueLengthOfServer = queueLength;
				qlpi.bvn = bvn;
				qlpis.add(qlpi);
				if(!validQueueTimes) {
					ProblemBuffer.this.necessaryStcRows++;
				}
				validQueueTimes = true;
			}
			public void addStartTimeOfReq(String reqCompTypeName) {
				ResponseTimeReqs rtr = getRtr(reqCompTypeName);
				// overriding existing value (should be the same each time)
				rtr.reqCompStartTime = startProp(reqCompTypeName);
			}
			/** Remember to also invoke {@link #addStartTimeOfReq(String)} */
			public void addEndTimeOfReq(String reqCompTypeName, double predictedRunTimeOfImplOnServer, String bvn) {
				ResponseTimeReqs rtr = getRtr(reqCompTypeName);
				// check for existing value with same bvn
				if(existingEndTime(rtr, bvn, predictedRunTimeOfImplOnServer)) {
					// ignore this one
					return;
				}
				EndTime et = new EndTime();
				et.bvn = bvn;
				et.predictedRunTimeOfImplOnServer = predictedRunTimeOfImplOnServer;
				rtr.endTimes.add(et);
				if(!validEndTimes) {
					ProblemBuffer.this.necessaryStcRows++;
				}
				validEndTimes = true;
			}
			/** Check if endtime tuple with same bvn already exists. Log warning if different values.
			 * @return <code>true</code> if already existing */
			private boolean existingEndTime(ResponseTimeReqs rtr, String bvn,
					double predictedRunTimeOfImplOnServer) {
				for(EndTime et : rtr.endTimes) {
					if(et.bvn.equals(bvn)) {
						// Already known, check if same value
						if(et.predictedRunTimeOfImplOnServer != predictedRunTimeOfImplOnServer) {
							log.warn(String.format("Unequal predicted runtime: %f != %f",
									et.predictedRunTimeOfImplOnServer, predictedRunTimeOfImplOnServer));
						}
						return true;
					}
				}
				return false;
			}
			/** Check if queue tuple with same bvn already exists. Log warning if different values.
			 * @return <code>true</code> if already existing */
			private boolean existingQLPI(String bvn, double queueLengthOfServer) {
				for(QueueLengthPerImpl qlpi : qlpis) {
					if(qlpi.bvn.equals(bvn)) {
						// Already known, check if same value
						if(qlpi.queueLengthOfServer != queueLengthOfServer) {
							log.warn(String.format("Unequal queue length: %f != %f",
									qlpi.queueLengthOfServer, queueLengthOfServer));
						}
						return true;
					}
				}
				return false;
			}
			private ResponseTimeReqs getRtr(String reqCompTypeName) {
				ResponseTimeReqs rtr = rtrs.get(reqCompTypeName);
				if(rtr == null) {
					rtr = new ResponseTimeReqs();
					rtrs.put(reqCompTypeName, rtr);
				}
				return rtr;
			}
		}
		class VarnameAndWeight {
			int varcol;
			double weight;
			public VarnameAndWeight(String varname, double weight) {
				this.varcol = toCol(varname, true);
				this.weight = weight;
			}
			/** Attention: Variable must already exist. */
			public VarnameAndWeight(int varcol, double weight) {
				this.varcol = varcol;
				this.weight = weight;
			}
			@Override
			public String toString() {
				return toName(varcol) + "="	+ weight + "]";
			}
		}
		/** { name of variable -> column in ilp } */
		final Map<String, Integer> varnameToCol;
		/** { column in ilp -> name of variable } */
		final List<String> varnames;
		/** [ (name of componentType -> column in ilp } */
		final List<CompTypeString> proxies;
		/** boolean varname */
		final List<Integer> binaryVars;
		/** 0 &lt;= varname &lt;= upperBound */
		final List<VarnameAndUpperBound> usageVars;
		/**  */
		final List<Integer> eclVars;
		/** varname (op) val */
		final List<SingleConstraint> singleConstraints;
		/** varname (op) weighted_sum{reqList} */
		final List<ListConstraint> listConstraints;
		/** sum{vars} = sum{depBvn} <br><i>or</i><br>
	 	sum{vars} = val */
		final List<EqualsConstraint> equalsConstraints;
		/** objective = min: weighted_sum{[varname,weight]}*/
		final List<VarnameAndWeight> objective;
		/** [ bvn ] */
		final List<Integer> ignoredBvns;
		/** startTime >= queueLength && startTime >= sum{reqComp.responseTime} */
		final Map<String, StartTimeConstraint> startTimesConstraintsByCompName;
		final List<CompTypeReq> reqs;
		int necessaryStcRows = 0;
		boolean reloadedLast;
		public ProblemBuffer() {
			binaryVars = new ArrayList<>();
			usageVars = new ArrayList<>();
			eclVars = new ArrayList<>();
			singleConstraints = new ArrayList<>();
			listConstraints = new ArrayList<>();
			equalsConstraints = new ArrayList<>();
			objective = new ArrayList<>();
			startTimesConstraintsByCompName = new HashMap<>();

			varnames = new ArrayList<>();
			varnameToCol = new HashMap<>();
			proxies = new ArrayList<>();
			reqs = new ArrayList<>();
			ignoredBvns = new ArrayList<>();
		}
		public void ensureEclVariable(String propName) {
			eclVars.add(toCol(propName, true));
		}
		public int getRowCount() {
			return singleConstraints.size() + listConstraints.size() +
					equalsConstraints.size() + necessaryStcRows + startTimesConstraintsByCompName.size();
		}
		public int getColCount() {
			return varnames.size();
		}
		public List<Integer> mapToCol(Collection<String> collection) {
			List<Integer> result = new ArrayList<Integer>(collection.size());
			for(String varname : collection) {
				result.add(toCol(varname, true));
			}
			return result;
		}
		public int toCol(String varname, boolean createNonexistent) {
			Integer col = varnameToCol.get(varname);
			if(col == null) {
				// varname not seen before, append it to varnames list
				if(createNonexistent) {
					varnames.add(varname);
					col = new Integer(varnames.size());
					varnameToCol.put(varname, col);
				}
				else {
					log.error("No var with name " + varname);
					col = Integer.valueOf(0);
				}
			}
			return col.intValue();
		}
		public String toName(int varcol) {
			return varnames.get(varcol-1);
		}
		/** varname (op) weighted_sum{reqList} */
		public void addListConstraint(String varname,
				List<UsageVarAndValue> reqList, int kind) {
			listConstraints.add(new ListConstraint(varname, reqList, kind));
		}
		/** varname (op) val */
		public void addSingleConstraint(String varname, CcmValue val, int kind) {
			//TODO: maybe add varname to usagevars
			singleConstraints.add(new SingleConstraint(varname, val, kind));
		}
		/** boolean varname */
		public void addBinaryVar(String varname) {
			binaryVars.add(toCol(varname, true));
		}
		/** 0 &lt;= varname &lt;= upperBound */
		public void addUsageVar(String varname, CcmValue upperBound) {
			if(settings.getILPlogUsageVar().get()) {
				log.debug("Usage var:" + varname + "<=" + upperBound);
			}
			usageVars.add(new VarnameAndUpperBound(varname, upperBound));
		}
		/** sum{vars} = sum{depBvn} */
		public void addBinaryVarsEqualsConstraint(List<String> vars,
				Set<String> depBvn) {
			equalsConstraints.add(new EqualsConstraint(vars, depBvn));
		}
		/** sum{vars} = val */
		public void addBinaryVarsConstraint(List<String> vars, int val) {
			equalsConstraints.add(new EqualsConstraint(vars, val));
		}
		/** objective = min: weighted_sum{[varname,weight]}*/
		public void addObjective(String varname, double weight) {
			objective.add(new VarnameAndWeight(varname, weight));
		}
		public StartTimeConstraint getOrCreateStartTimeConstraint(SWComponentType compType) {
			String compName = compType.getName();
			StartTimeConstraint stc = startTimesConstraintsByCompName.get(compName);
			if(stc == null) {
				stc = new StartTimeConstraint(compType);
				startTimesConstraintsByCompName.put(compName, stc);
				log.debug("stc created for " + compName);
			}
			return stc;
		}
		public void eliminateDuplicates() {
			// test for exact match of two EqualsConstraints. May be extended to set match
			Set<EqualsConstraint> toRemove = new HashSet<>(); // check for identity is sufficient
			for (int i = 0; i < equalsConstraints.size(); i++) {
				for (int j = i+1; j < equalsConstraints.size(); j++) {
					EqualsConstraint first = equalsConstraints.get(i);
					EqualsConstraint second = equalsConstraints.get(j);
					if(first.positives.equals(second.positives) &&
							first.negatives.equals(second.negatives)
							&& first.val == second.val) {
						toRemove.add(first);
					}
				}
			}
			for(EqualsConstraint ec : toRemove) {
				if(settings.getILPlogDuplicateElimination().get()) {
					log.debug("Removing duplicate " + ec);
				}
				equalsConstraints.remove(ec);
			}
		}
		public void addRequirement(String compName, String reqCompName) {
			CompTypeReq ctr = getReq(reqs, compName, true);
			if(ctr == null) {
				ctr = createCompTypeReq(compName, reqs);
			}
			CompTypeReq reqCtr = getReq(ctr.reqs, reqCompName, false);
			if(reqCtr == null) {
				createCompTypeReq(reqCompName, ctr.reqs);
			}
			log.debug(String.format("%s is requiring %s.", compName, reqCompName));
		}
		private CompTypeReq createCompTypeReq(String compName,
				List<CompTypeReq> reqList) {
			CompTypeReq ctr = new CompTypeReq(compName);
			if(reqList != null) {
				reqList.add(ctr);
			}
			return ctr;
		}
		private CompTypeReq getReq(List<CompTypeReq> reqList, String compName, boolean recursive) {
			List<CompTypeReq> toSearchIn = new ArrayList<>(reqList);
			while(!toSearchIn.isEmpty()) {
				CompTypeReq ctr = toSearchIn.remove(0);
				if(ctr.compName.equals(compName)) {
					return ctr;
				}
				if(recursive) {
					toSearchIn.addAll(ctr.reqs);
				}
			}
			return null;
		}
		public void writeBuffer(glp_prob ilp) {
			// add response_time to variables
			for(String compName : compNameToBvn.keySet()) {
				String response = responseProp(compName);
				pb.ensureEclVariable(response);
				if(settings.getILPobjectiveIncludeResponseTime().get()) {
					addObjective(response, 1.0);
				}
			}
			
			/*	set_col_bnds
			  	GLP_FR	−∞ < x < +∞		Free (unbounded) variable
				GLP_LO	lb ≤ x < +∞		Variable with lower bound
				GLP_UP	−∞ < x ≤ ub		Variable with upper bound
				GLP_DB	lb ≤ x ≤ ub		Double-bounded variable
				GLP_FX	lb = x = ub		Fixed variable (only lb is used, ub is ignored)
			 */
			/*	set_col_kind
				GLP_CV	continuous variable;
				GLP_IV	integer variable;
				GLP_BV	binary variable.
					Setting a column to GLP_BV has the same effect as if it were set to GLP_IV,
					its lower bound were set 0, and its upper bound were set to 1.
			 */
			int colCount = getColCount();
			if(settings.getILPfeaturePenalties().get() && !reloadedLast) {
				final int resCount = resourceShortCuts.size();
				// every combination of two servers = sum of first resCount numbers
				final int incPerReq = resCount * (resCount - 1);
				int treeCount = getTreeCount(reqs);
				int totalInc = treeCount * incPerReq;
				colCount += totalInc;
				if(log.isDebugEnabled()) {
					log.debug(String.format("adding %d (comp) * %d * %d (server) = %d variables",
							treeCount, resCount, resCount -1, totalInc));
				}
				for (int i = 0; i < totalInc; i++) {
					String indicatorVarName = "b"+i;
					addBinaryVar(indicatorVarName);
				}
			}
//			log.debug(varnames);
//			log.debug(varnameToCol);
			GLPK.glp_add_cols(ilp, colCount);
			// create binary variables
			StringBuilder sb = new StringBuilder("binaryVars (x = 0|1): ");
			binaryVars.removeAll(ignoredBvns);
			for(int col : binaryVars) {
				GLPK.glp_set_col_name(ilp, col, toName(col));
				GLPK.glp_set_col_kind(ilp, col, GLPKConstants.GLP_BV);
//				GLPK.glp_set_col_bnds(ilp, col, GLPKConstants.GLP_DB, 0, 1);
				sb.append(toName(col)).append(",");
			}
			// create usage variables
			log.debug(StringUtils.cap(sb.substring(0, sb.length() - 1), 400));
			sb = new StringBuilder("usageVars (0<=x<=C): ");
			for(VarnameAndUpperBound vup : usageVars) {
				GLPK.glp_set_col_name(ilp, vup.varcol, toName(vup.varcol));
				GLPK.glp_set_col_kind(ilp, vup.varcol, GLPKConstants.GLP_CV);
				int kind;
				double up;
				if(vup.upperBound == null) {
					kind = GLPKConstants.GLP_LO;
					up = 0; // will be ignored
				}
				else {
					up = CCMUtil.getRealValue(vup.upperBound);
					if(up == 0) {
						if(settings.getILPfeatureZeroIsUnknown().get()) {
							// results in var >= 0 which is always satisfied.
							// at least for positive values. obviously.
							kind = GLPKConstants.GLP_LO;
						}
						else {
							// here lower bound == upper bound == 0
							// therefore use fixed variable (up is ignored then)
							// and the variable is fixed to zero
							kind = GLPKConstants.GLP_FX;
						}
					}
					else {
						kind = GLPKConstants.GLP_DB;
					}
				}
				GLPK.glp_set_col_bnds(ilp, vup.varcol, kind, 0, up);
				sb.append(toName(vup.varcol)).append(",");
			}
			// create ecl variables
			log.debug(StringUtils.cap(sb.substring(0, sb.length() - 1), 400));
			sb = new StringBuilder("eclVars (0<=x): ");
			for(int col : eclVars) {
				GLPK.glp_set_col_name(ilp, col, toName(col));
				GLPK.glp_set_col_kind(ilp, col, GLPKConstants.GLP_CV);
				GLPK.glp_set_col_bnds(ilp, col, GLPKConstants.GLP_LO, 0, 0);
				sb.append(toName(col)).append(",");
			}
			log.debug(StringUtils.cap(sb.substring(0, sb.length() - 1), 400));
	        
	        // create rows
//			final int rowCount = getRowCount();
//			GLPK.glp_add_rows(ilp, rowCount);

			// remove ignored bvns from every source
			// sources include: compNameToBvn, reqs, binaryVars, varnames?, varnameToCol?, (single|list|equals|startTime)Constraints, objective  
			for(int bvn : ignoredBvns) {
				// try soft version first: set bvn to 0
				SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
				SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
				int rowCounter = GLPK.glp_add_rows(ilp, 1);
				GLPK.intArray_setitem(ind, 1, bvn);
				GLPK.doubleArray_setitem(val, 1, 1);
				GLPK.glp_set_row_bnds(ilp, rowCounter, GLPKConstants.GLP_FX, 0, 0);
				GLPK.glp_set_row_name(ilp, rowCounter, "ignored"+rowCounter);
				GLPK.glp_set_mat_row(ilp, rowCounter, 1, ind, val);
				GLPK.delete_intArray(ind);
				GLPK.delete_doubleArray(val);

			}

			// single constraints
			for(SingleConstraint sc : singleConstraints) {
		        SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
		        SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
				int rowCounter = GLPK.glp_add_rows(ilp, 1);
				if(settings.getILPlogConstraints().get()) {
					log.debug(String.format("%4d: %s", rowCounter, sc));
				}
				int glpk_kind;
				double lb, ub;
				lb = ub = CCMUtil.getRealValue(sc.val);
				switch(sc.kind) {
				case CONSTRAINT_EQUAL:
					glpk_kind = GLPKConstants.GLP_FX;
					break;
				case CONSTRAINT_LESS_THAN_REQS:
					glpk_kind = GLPKConstants.GLP_UP;
					lb = 0;
					break;
				case CONSTRAINT_GREATER_THAN_REQS:
					glpk_kind = GLPKConstants.GLP_LO;
					ub = 0;
					break;
				default:
					// should not happen
					log.warn("Unknown kind: " + sc.kind);
					glpk_kind = 0; //Maybe this will result in an error
				}
				GLPK.glp_set_row_bnds(ilp, rowCounter, glpk_kind, lb, ub);
				GLPK.glp_set_row_name(ilp, rowCounter, "single"+rowCounter);
				GLPK.intArray_setitem(ind, 1, sc.varcol);
				GLPK.doubleArray_setitem(val, 1, 1);
				GLPK.glp_set_mat_row(ilp, rowCounter, 1, ind, val);
				GLPK.delete_intArray(ind);
//				GLPK.delete_doubleArray(val);
			}
			
			// list constraints
			for(ListConstraint lc : listConstraints) {
		        SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
		        SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
				int rowCounter = GLPK.glp_add_rows(ilp, 1);
				if(settings.getILPlogConstraints().get()) {
					log.debug(String.format("%4d: %s", rowCounter, lc));
				}
				//    varname (op) weighted_sum{reqList}
				// => weighted_sum{reqList} - varname !(op) 0
				// !(<=) is >=, !(>=) is <=, !(=) stays =
				int glpk_kind;
				double lb, ub;
				lb = ub = 0;
				switch(lc.kind) {
				case CONSTRAINT_EQUAL:
					glpk_kind = GLPKConstants.GLP_FX;
					break;
				case CONSTRAINT_LESS_THAN_REQS:
					glpk_kind = GLPKConstants.GLP_LO;
					break;
				case CONSTRAINT_GREATER_THAN_REQS:
					glpk_kind = GLPKConstants.GLP_UP;
					break;
				default:
					// should not happen
					log.warn("Unknown kind: " + lc.kind);
					glpk_kind = 0; //Maybe this will result in an error
				}
				GLPK.glp_set_row_bnds(ilp, rowCounter, glpk_kind, lb, ub);
				GLPK.glp_set_row_name(ilp, rowCounter, "list"+rowCounter);
				GLPK.intArray_setitem(ind, 1, lc.varcol);
				GLPK.doubleArray_setitem(val, 1, -1); // minus varname
				int colCounter = 2; // begin at 2, because 1 is already used for varname
				for(VarnameAndWeight vaw : lc.reqList) {
					GLPK.intArray_setitem(ind, colCounter, vaw.varcol);
					GLPK.doubleArray_setitem(val, colCounter, vaw.weight);
					colCounter++;
				}
				int len = lc.reqList.size() + 1;
				GLPK.glp_set_mat_row(ilp, rowCounter, len, ind, val);
				GLPK.delete_intArray(ind);
				GLPK.delete_doubleArray(val);
			}
			
			// equality constraints
			for(EqualsConstraint ec : equalsConstraints) {
		        SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
		        SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
				int rowCounter = GLPK.glp_add_rows(ilp, 1);
				if(settings.getILPlogConstraints().get()) {
					log.debug(String.format("%4d: %s", rowCounter, ec));
				}
				double lb = ec.negatives.isEmpty() ? ec.val : 0;
				int len = ec.positives.size() + ec.negatives.size();
				GLPK.glp_set_row_bnds(ilp, rowCounter, GLPKConstants.GLP_FX, lb, 0);
				GLPK.glp_set_row_name(ilp, rowCounter, "eq"+rowCounter);
				int colCounter = 1; // this time begin at 1
				for(Integer posCol : ec.positives) {
					GLPK.intArray_setitem(ind, colCounter, posCol);
					GLPK.doubleArray_setitem(val, colCounter, 1);
					colCounter++;
				}
				for(Integer negCol : ec.negatives) {
					GLPK.intArray_setitem(ind, colCounter, negCol);
					GLPK.doubleArray_setitem(val, colCounter, -1);
					colCounter++;
				}
				GLPK.glp_set_mat_row(ilp, rowCounter, len, ind, val);
				GLPK.delete_intArray(ind);
				GLPK.delete_doubleArray(val);
			}
			
			// start times and required components
			for(Entry<String, StartTimeConstraint> e : startTimesConstraintsByCompName.entrySet()) {
				int glpk_kind, colCounter, len;
				String compName = e.getKey();
				StartTimeConstraint stc = e.getValue();
//				if(!stc.validQueueTimes && !stc.validEndTimes) {
//					log.debug(String.format("Ignore %s", stc));
//				}
				
				if(stc.compType instanceof SWConnectorType) {
//					log.debug(String.format("%s is a connector. skipping startTime constraints.", stc.compType.getName()));
					
					CompTypeReq ctr = getReq(reqs, compName, true);
					log.debug(String.format("Create sequential order for required components of %s.", compName));
					for (int i = 0; i < ctr.reqs.size(); i++) {
				        SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
				        SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
						CompTypeReq reqComp = ctr.reqs.get(i);
						// response time of connector is greater or equal max(response_time of reqComps) + connector#run_time
						int rowCounter = GLPK.glp_add_rows(ilp, 1);
						if(settings.getILPlogConstraints().get()) {
							log.debug(String.format("%4d: %s >= %s", rowCounter, compName,
								reqComp.compName));
						}
						glpk_kind = GLPKConstants.GLP_UP;
						GLPK.glp_set_row_bnds(ilp, rowCounter, glpk_kind, 0, 0);
						GLPK.glp_set_row_name(ilp, rowCounter, "stc_con_responseTime"+compName+rowCounter);

						// connector response time
						GLPK.intArray_setitem(ind, 1, toCol(responseProp(compName), false));
						GLPK.doubleArray_setitem(val, 1, -1); // minus varname
						// reqComponent responseTime
						GLPK.intArray_setitem(ind, 2, toCol(responseProp(reqComp.compName), false));
						GLPK.doubleArray_setitem(val, 2, 1);
						
						GLPK.glp_set_mat_row(ilp, rowCounter, 2, ind, val);
						GLPK.delete_intArray(ind);
						GLPK.delete_doubleArray(val);
						
						// if component C is connector, execute required components sequentially
						// by defining: reqComp_i.startTime >= reqComp_{i-1}.responseTime
						// do not add rows recursively, as they will be added if new connector is found in reqComps
						if (i == 0) {
							// start with second reqComp
							continue;
						}
						if(settings.getILPfeatureSequentialConstraint().get()) {
					        ind = GLPK.new_intArray(colCount);
					        val = GLPK.new_doubleArray(colCount);
							rowCounter = GLPK.glp_add_rows(ilp, 1);
							CompTypeReq prevReqComp = ctr.reqs.get(i-1);
							if(settings.getILPlogConstraints().get()) {
								log.debug(String.format("%4d: %s >= %s", rowCounter, reqComp.compName,
									prevReqComp.compName));
							}
							glpk_kind = GLPKConstants.GLP_UP;
							GLPK.glp_set_row_bnds(ilp, rowCounter, glpk_kind, 0, 0);
							GLPK.glp_set_row_name(ilp, rowCounter, "stc_con_req"+compName+rowCounter);
							// reqComp startTime
							GLPK.intArray_setitem(ind, 1, toCol(startProp(reqComp.compName), false));
							GLPK.doubleArray_setitem(val, 1, -1); // minus varname
							// prevReqComponent responseTime
							GLPK.intArray_setitem(ind, 2, toCol(responseProp(prevReqComp.compName), false));
							GLPK.doubleArray_setitem(val, 2, 1);
							GLPK.glp_set_mat_row(ilp, rowCounter, 2, ind, val);
							GLPK.delete_intArray(ind);
							GLPK.delete_doubleArray(val);
						}
						else if(logOnce(settings.getILPfeatureSequentialConstraint())) {
							log.warn("Got Connector "+compName+", but sequential constraints disabled.");
						}
					}
					
					
				}
				else {
					if(stc.validQueueTimes) {
				        SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
				        SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
						int rowCounter = GLPK.glp_add_rows(ilp, 1);
						if(settings.getILPlogConstraints().get()) {
							log.debug(String.format("%4d: QLPI %s", rowCounter, stc));
						}
						// startTime >= queueLength
						// using form: queueLength - startTime <= 0
						glpk_kind = GLPKConstants.GLP_UP;
						GLPK.glp_set_row_bnds(ilp, rowCounter, glpk_kind, 0, 0);
						GLPK.glp_set_row_name(ilp, rowCounter, "stc_queue"+compName+rowCounter);
						GLPK.intArray_setitem(ind, 1, toCol(stc.startTimeName, false));
						GLPK.doubleArray_setitem(val, 1, -1); // minus varname
						colCounter = 2; // begin at 2, because 1 is already used for startTimeName
						for(QueueLengthPerImpl qlpi : stc.qlpis) {
							GLPK.intArray_setitem(ind, colCounter, toCol(qlpi.bvn, false));
							GLPK.doubleArray_setitem(val, colCounter, qlpi.queueLengthOfServer);
							colCounter++;
						}
						len = stc.qlpis.size() + 1;
						GLPK.glp_set_mat_row(ilp, rowCounter, len, ind, val);
						GLPK.delete_intArray(ind);
						GLPK.delete_doubleArray(val);
					}
					
					if(stc.validEndTimes) {
						// startTime >= reqComponentResponseTime
						// using form: reqComponentResponseTime - startTime <= 0
						for(Entry<String, ResponseTimeReqs> entry : stc.rtrs.entrySet()) {
					        SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
					        SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
							int rowCounter = GLPK.glp_add_rows(ilp, 1);
							if(settings.getILPlogConstraints().get()) {
								log.debug(String.format("%4d: RTR %s", rowCounter, stc));
							}
							// add a row for each entry
							ResponseTimeReqs rtr = entry.getValue();
							glpk_kind = GLPKConstants.GLP_UP;
							GLPK.glp_set_row_bnds(ilp, rowCounter, glpk_kind, 0, 0);
							GLPK.glp_set_row_name(ilp, rowCounter, "stc_reqs"+compName+rowCounter);
							// connector startTime
							GLPK.intArray_setitem(ind, 1, toCol(stc.startTimeName, false));
							GLPK.doubleArray_setitem(val, 1, -1); // minus varname
							// reqComponent startTime
							String startTimeVar = rtr.reqCompStartTime;
							GLPK.intArray_setitem(ind, 2, toCol(startTimeVar, false));
							GLPK.doubleArray_setitem(val, 2, 1);
							colCounter = 3; // begin at 3, because 1 and 2 are already used for startTimes
							for(EndTime et : rtr.endTimes) {
								GLPK.intArray_setitem(ind, colCounter, toCol(et.bvn, false));
								double runtime = et.predictedRunTimeOfImplOnServer;
								GLPK.doubleArray_setitem(val, colCounter, runtime );
								colCounter++;
							}
							GLPK.glp_set_mat_row(ilp, rowCounter, colCounter - 1, ind, val);
							GLPK.delete_intArray(ind);
							GLPK.delete_doubleArray(val);
						}
					}

					// add response of component C as sum of startTime of C and runTime of required components of C
			        SWIGTYPE_p_int ind = GLPK.new_intArray(colCount);
			        SWIGTYPE_p_double val = GLPK.new_doubleArray(colCount);
					int rowCounter = GLPK.glp_add_rows(ilp, 1);
					String response = responseProp(stc.compType.getName());
					String start = stc.startTimeName;
					if(settings.getILPlogConstraints().get()) {
						log.debug(String.format("%4d: %s = %s + %s", rowCounter,
								response, start, compNameToBvn.get(compName)));
					}
					glpk_kind = GLPKConstants.GLP_FX;
					GLPK.glp_set_row_bnds(ilp, rowCounter, glpk_kind, 0, 0);
					GLPK.glp_set_row_name(ilp, rowCounter, "stc_responseTime"+compName+rowCounter);
					GLPK.intArray_setitem(ind, 1, toCol(response, false));
					GLPK.doubleArray_setitem(val, 1, -1); // minus response
					GLPK.intArray_setitem(ind, 2, toCol(start, false));
					GLPK.doubleArray_setitem(val, 2, 1); // + start
					colCounter = 3; // begin at 3, because response and start got 1 and 2
					for(String bvn : compNameToBvn.get(compName)) {
						GLPK.intArray_setitem(ind, colCounter, toCol(bvn, false));
						double runtime = getRuntimeOrHugeValue(bvn);
						GLPK.doubleArray_setitem(val, colCounter, runtime);
						colCounter++;
					}
					GLPK.glp_set_mat_row(ilp, rowCounter, colCounter-1, ind, val);
					GLPK.delete_intArray(ind);
					GLPK.delete_doubleArray(val);
				}
			}
			
			if(settings.getILPfeaturePenalties().get()) {
				int indicatorVarCounter = 0;
				for(Entry<String, Set<String>> e : compNameToBvn.entrySet()) {
					// a component compName
					String compName = e.getKey();
					CompTypeReq ctr = getReq(reqs, compName, true);
					if(log.isDebugEnabled()) {
						log.debug("Comp:" + compName + ", ctr=" + ctr + ", bvns=" +
								StringUtils.cap(e.getValue(), 200));
					}
					Map<String, Set<String>> bvnsPerServer = getBvnPerServer(e.getValue());
					if(ctr != null && ctr.reqs != null) for(CompTypeReq reqCtr : ctr.reqs) {
						// requirement of component compName
						String reqCompName = reqCtr.compName;
						Set<String> reqBvns = compNameToBvn.get(reqCompName); // reqBvns == null?
						for(Entry<String, Set<String>> bvnsPerServerEntry : bvnsPerServer.entrySet()) {
							Set<String> addedReqBvns = new HashSet<>();
							// server -> {bvn(compName, server)}
							String compServer = bvnsPerServerEntry.getKey();
							if(log.isDebugEnabled()) {
								log.debug("reqComp:" + reqCompName + ", reqCtr=" + reqCtr + ", bvns=" +
										StringUtils.cap(reqBvns, 200));
							}

							for(Entry<String, Set<String>> reqBvnsPerServerEntry :
								getBvnPerServer(reqBvns).entrySet()) {
								// reqServer -> {bvn(reqComp, reqServer)}
								String reqCompServer = reqBvnsPerServerEntry.getKey();
								if(compServer.equals(reqCompServer)) {
									// same server -> no penalty
									continue;
								}
								// here we use one indicator variable
								String indicatorVarName = "b"+ (indicatorVarCounter++);
								addObjective(indicatorVarName, getPenaltyWeight(compServer,
										reqCompServer, reqCompName));
								boolean first = true;
								int row1 = GLPK.glp_add_rows(ilp, 3);
								int row2 = row1 + 1;
								int row3 = row1 + 2;
								int colCounter1 = 2, colCounter2 = 2, colCounter3 = 2;
						        SWIGTYPE_p_int ind1 = GLPK.new_intArray(colCount);
						        SWIGTYPE_p_int ind2 = GLPK.new_intArray(colCount);
						        SWIGTYPE_p_int ind3 = GLPK.new_intArray(colCount);
						        SWIGTYPE_p_double val1 = GLPK.new_doubleArray(colCount);
						        SWIGTYPE_p_double val2 = GLPK.new_doubleArray(colCount);
						        SWIGTYPE_p_double val3 = GLPK.new_doubleArray(colCount);
								GLPK.glp_set_row_bnds(ilp, row1, GLPKConstants.GLP_UP, 0, 0);
								GLPK.glp_set_row_bnds(ilp, row2, GLPKConstants.GLP_UP, 0, 0);
								GLPK.glp_set_row_bnds(ilp, row3, GLPKConstants.GLP_LO, -1, -1);
								String namePrefix = "penalty_"+indicatorVarName+"_";
								GLPK.glp_set_row_name(ilp, row1, namePrefix+compName+row1);
								GLPK.glp_set_row_name(ilp, row2, namePrefix+reqCompName+row2);
								GLPK.glp_set_row_name(ilp, row3, namePrefix+compName+"_"+reqCompName+row3);
								
								if(log.isDebugEnabled() && settings.getILPlogConstraints().get() &&
										settings.getILPlogSingleIndicator().get()) {
									log.debug(String.format("%4d-%d: New indicator variable %s for %s@%s->%s@%s",
											row1, row3, indicatorVarName, compName, compServer,
											reqCompName, reqCompServer));
								}
								
								GLPK.intArray_setitem(ind1, 1, toCol(indicatorVarName, false));
								GLPK.doubleArray_setitem(val1, 1, 1);
								GLPK.intArray_setitem(ind2, 1, toCol(indicatorVarName, false));
								GLPK.doubleArray_setitem(val2, 1, 1);
								GLPK.intArray_setitem(ind3, 1, toCol(indicatorVarName, false));
								GLPK.doubleArray_setitem(val3, 1, 1);
								if(settings.getILPlogSingleIndicator().get()) {
									CollectionsUtil.ensureArrayList(debugging, row1+"ind1:").add("+"+indicatorVarName);
									CollectionsUtil.ensureArrayList(debugging, row2+"ind2:").add("+"+indicatorVarName);
									CollectionsUtil.ensureArrayList(debugging, row3+"ind3:").add("+"+indicatorVarName);
								}
								for(String bvn : bvnsPerServerEntry.getValue()) {
									// add to first statement for bvns
									GLPK.intArray_setitem(ind1, colCounter1, toCol(bvn, false));
									GLPK.doubleArray_setitem(val1, colCounter1, -1);
									colCounter1++;
									// add to third statement for combination
									GLPK.intArray_setitem(ind3, colCounter3, toCol(bvn, false));
									GLPK.doubleArray_setitem(val3, colCounter3, -1);
									if(settings.getILPlogSingleIndicator().get()) {
										CollectionsUtil.ensureArrayList(debugging, row1+"ind1:").add("-"+bvn);
										CollectionsUtil.ensureArrayList(debugging, row3+"ind3:").add("-"+bvn);
									}
									colCounter3++;
									for(String reqBvn : reqBvnsPerServerEntry.getValue()) {
										if(!addedReqBvns.contains(reqBvn)) {
											// add to third statement for combination
											GLPK.intArray_setitem(ind3, colCounter3, toCol(reqBvn, false));
											GLPK.doubleArray_setitem(val3, colCounter3, -1);
											if(settings.getILPlogSingleIndicator().get()) {
												CollectionsUtil.ensureArrayList(debugging, row3+"ind3:").add("-"+reqBvn);
											}
											colCounter3++;
											addedReqBvns.add(reqBvn);
										}

										if(first) {
											// add to second statement for reqBvns BUT ONLY ONCE
											GLPK.intArray_setitem(ind2, colCounter2, toCol(reqBvn, false));
											GLPK.doubleArray_setitem(val2, colCounter2, -1);
											if(settings.getILPlogSingleIndicator().get()) {
												CollectionsUtil.ensureArrayList(debugging, row2+"ind2:").add("-"+reqBvn);
											}
											colCounter2++;
										}
									}
									first = false;
								}
								GLPK.glp_set_mat_row(ilp, row1, colCounter1-1, ind1, val1);
								GLPK.glp_set_mat_row(ilp, row2, colCounter2-1, ind2, val2);
								GLPK.glp_set_mat_row(ilp, row3, colCounter3-1, ind3, val3);
								GLPK.delete_intArray(ind1);
								GLPK.delete_intArray(ind2);
								GLPK.delete_intArray(ind3);
								GLPK.delete_doubleArray(val1);
								GLPK.delete_doubleArray(val2);
								GLPK.delete_doubleArray(val3);
							}
						}
					}
				}
			}
			
			// objective
			GLPK.glp_set_obj_name(ilp, "objective");
			GLPK.glp_set_obj_dir(ilp, GLPKConstants.GLP_MIN);
			GLPK.glp_set_obj_coef(ilp, 0, 0); // absolute value, may be superflous to set to 0
			if(settings.getILPlogObjectiveSettings().get()) {
				log.debug("objective:");
			}
			boolean[] variablesNotUsed = new boolean[colCount+1]; // leave index 0 empty, cols are counter from 1 on
			Arrays.fill(variablesNotUsed, true);
			for(VarnameAndWeight vaw : objective) {
				if(settings.getILPlogObjectiveSettings().get()) {
					log.debug(String.format("%s= %f", toName(vaw.varcol), vaw.weight));
				}
				GLPK.glp_set_obj_coef(ilp, vaw.varcol, vaw.weight);
				variablesNotUsed[vaw.varcol] = false;
				// other columns are not set -> assumption: values of other columns are equal 0
			}
			for (int i = 1; i < variablesNotUsed.length; i++) {
				if(variablesNotUsed[i]) {
					if(settings.getILPlogObjectiveSettings().get()) {
						log.debug(String.format("%s set to 0", toName(i)));
					}
					GLPK.glp_set_obj_coef(ilp, i, 0);
				}
			}
			String pathname = FileUtils.getWorkspaceFile(problemName+".lp");
			try { GLPK.glp_write_lp(ilp, null, FileUtils.ensureFileExisting(pathname, true, true)); }
			catch (IOException e) { log.warn("Could not create file " + pathname, e); }
		}
		private int getTreeCount(List<CompTypeReq> reqList) {
			int count = 0;
			for(CompTypeReq ctr : reqList) {
				count += ctr.reqs.size();
				count += getTreeCount(ctr.reqs);
			}
			return count;
		}
		private Map<String, Set<String>> getBvnPerServer(Set<String> bvns) {
			Map<String, Set<String>> result = new HashMap<>();
			for(ShortCut shortCut : resourceShortCuts.values()) {
				if(!shortCut.used) {
					continue;
				}
				String serverName = shortCut.shortName;
				result.put(serverName, new HashSet<String>());
			}
			for(String bvn : bvns) {
				int lastHash = bvn.lastIndexOf('#');
				result.get(bvn.substring(lastHash+1)).add(bvn);
			}
			return result;
		}
		/** Stores a value into a proxy for endTime */
		public void putProxyValue(SWComponentType compType, String bvn, double realValue) {
			boolean found = false;
			// search for [proxies].s2 == compName
			String compName = compType.getName();
			for(CompTypeString line : proxies) {
				if(compTypeEquals(line.s1, compType)) {
					// already there
					found = true;
					break;
				}
				if(line.s2.equals(compName)) {
					// add end time to stc
					getOrCreateStartTimeConstraint(line.s1).addEndTimeOfReq(compName, realValue, bvn);
					found = true;
				}
			}
			if(!found) {
				log.warn("No registred proxy for " + compName + ". proxies=" + proxies);
			}
		}
		/** dirty way of saving bvn - compName relation */
		Map<String, Set<String>> compNameToBvn = new HashMap<>();
		protected void registerBvnToCompName(String compName, String bvn) {
			if(CollectionsUtil.ensureHashSet(compNameToBvn, compName).add(bvn)) {
				log.debug(compName + ":" + bvn);
			}
		}
		/** Register a new proxy for the endTimes */
		public void registerProxy(SWComponentType stcCompType, String proxyCompName) {
			for(CompTypeString line : proxies) {
				if(compTypeEquals(line.s1, stcCompType) && line.s2.equals(proxyCompName)) {
					// already registered
					return;
				}
			}
			CompTypeString line = new CompTypeString();
			line.s1 = stcCompType;
			line.s2 = proxyCompName;
			proxies.add(line);
			addRequirement(stcCompType.getName(), proxyCompName);
		}
		private boolean compTypeEquals(SWComponentType s1,
				SWComponentType s2) {
			return s1 == null || s2 == null ? false : stringEquals(s1.getId(), s2.getId()) || stringEquals(s1.getName(), s2.getName());
		}
		private boolean stringEquals(String s1, String s2) {
			return s1 == null || s2 == null ? false : s1.equals(s2);
		}
		public void addIgnored(String bv) {
			ignoredBvns.add(toCol(bv, false));
		}
		/** @return an copy of this buffer */
		public ProblemBuffer copy() {
			ProblemBuffer copy = new ProblemBuffer();
			copy.binaryVars.addAll(this.binaryVars);
			copy.compNameToBvn.putAll(this.compNameToBvn);
			copy.eclVars.addAll(this.eclVars);
			copy.equalsConstraints.addAll(this.equalsConstraints);
			copy.ignoredBvns.addAll(this.ignoredBvns);
			copy.listConstraints.addAll(this.listConstraints);
			copy.necessaryStcRows = this.necessaryStcRows;
			copy.objective.addAll(this.objective);
			copy.proxies.addAll(this.proxies);
			copy.reqs.addAll(this.reqs);
			copy.singleConstraints.addAll(this.singleConstraints);
			copy.startTimesConstraintsByCompName.putAll(this.startTimesConstraintsByCompName);
			copy.usageVars.addAll(this.usageVars);
			copy.varnames.addAll(this.varnames);
			copy.varnameToCol.putAll(this.varnameToCol);
			return copy;
		}
	}
	
	class PropertyAndMap {
		public final Property property;
		public final Map<String, CcmValue> map;
		public PropertyAndMap(Property property) {
			this.property = property;
			this.map = new HashMap<String, CcmValue>();
		}
	}

	/** Value that is returned, if no runtime was found for a component on a particular server.
	 * This should be sufficiently high, such that this server is <i>never</i> be chosen
	 * as target of this component. Other possiblities: Set bvn to <code>0</code>. */
	private static final double RUNTIME_HUGE_VALUE = 9e20;
	
	
//	static {
//		String jlp = System.getProperty("java.library.path");
//		if(!jlp.endsWith(File.pathSeparator))
//			jlp += File.pathSeparator;
//		jlp += "/usr/local/lib/jni";
//		System.setProperty("java.library.path", jlp);
//	}
	
//	static {
//		GlpkTerminalListener listener = new GlpkTerminalListener() {
//			
//			@Override
//			public boolean output(String str) {
//				log.debug(str.trim());
//				return false;
//			}
//		};
//		GlpkTerminal.addListener(listener);
//	}

	public static final String startTimePropName = "##" + "startTime";
	private static String startProp(String compName) {
		return compName + startTimePropName;
	}
	private final Map<Object, Boolean> loggedOnce = new HashMap<>();
	/**
	 * Test, if a log message should be logged.
	 * @param o an identifier to represent the message (tested with equals)
	 * @return <code>true</code> if the message should be logged, <code>false</code> otherwise
	 */
	public boolean logOnce(Object o) {
		Boolean alreadyLogged = loggedOnce.get(o);
		if(alreadyLogged == null) {
			loggedOnce.put(o, Boolean.TRUE);
			return true;
		}
		return false;
	}
	/** Get weight for penalty using compName at serverReqComp and transfer result to serverComp 
	 * @param serverComp The server on which the component should run
	 * @param serverReqComp The server on which the required component should run
	 * @param reqCompName the component name of the required component
	 * @param <i>metaParamValues</i> map, mapping metaParamNames to their values. Currently missing.
	 * @return the computed weight. Currently static value of 42.
	 * */
	public double getPenaltyWeight(String serverComp, String serverReqComp, String reqCompName) {
		return 42;
	}
	public static final String responseTimePropName = "#" + "response_time";
	private static String responseProp(String compName) {
		return compName + responseTimePropName;
	}
	public static final String rawRunTimePropName = "run_time";

	private static final Function2<String, String, Boolean> hashDifferentCheck = new Function2<String, String, Boolean>() {

		@Override
		public Boolean apply(String s1, String s2) {
			if(s1 == null) {
				return s2 != null;
			}
			if(s2 == null) { return true; }
			return s1.hashCode() != s2.hashCode();
		}
		
	};

	public String problemName;
	public String profile;

	private final Set<String> binaryVars;
	private final Map<String, ShortCut> resourceShortCuts;
	private final Map<String, Set<HWComponentRequirementClause>> resourceReqs;
	private final Map<String, Double> responseTimes;
	private final Map<String, Double> runTimes;
	/** {propName -> {bv -> val}} with
	 * <ul>
	 * <li>propName is e.g. Scaling#response_time,</li>
	 * <li>bv is e.g. b#org_haec_app_scaling_mencoder_ScalingMencoder#scaleMencoder1#R1,</li>
	 * <li>val would be response_time of ScalingMencoder on R1</li>
	 * </ul>
	 */
	private final Map<String, Map<String, CcmValue>> providedProperties;
	private final Map<String, PropertyAndMap> requiredProperties;
	private final AtomicBoolean stopInit;
	private String errorMessage;
	
	class EclFileCache {
		/** compName -> containerName -> contract */
		Map<String, Map<String, StringAndEclfile>> c = new HashMap<>();
		public void init(Map<String, Map<String, String>> fromGem) {
			for(Entry<String, Map<String, String>> compEntry : fromGem.entrySet()) {
				Map<String, StringAndEclfile> contractsMap = CollectionsUtil.ensureHashMap(c, compEntry.getKey());
				for(Entry<String, String> containerEntry : compEntry.getValue().entrySet()) {
					if(stopInit.get()) { return; }
					// store proxy, deserialize later, only if needed
					contractsMap.put(containerEntry.getKey(), new StringAndEclfile(containerEntry.getValue()));
					if(log.isDebugEnabled()) {
						log.debug(compEntry.getKey() + " -> " + containerEntry.getKey()
								+ " -> " + StringUtils.cap(containerEntry.getValue(), 10));
					}
				}
			}
		}
		public Map<String, EclFile> getEclFiles(String appName, String compName) {
			Map<String, StringAndEclfile> cached = c.get(compName);
			Map<String, EclFile> result = new HashMap<>();
			for(Entry<String, StringAndEclfile> e : cached.entrySet()) {
				// ensure deserialized contract, supply with appName and compName
				e.getValue().setEclFile(appName, compName);
				// store in result map
				result.put(e.getKey(), e.getValue().getEclFile());
			}
			return result;
		}
	}
	class StringAndEclfile {
		String serializedForm;
		boolean deserialized;
		EclFile eclFile;

		public StringAndEclfile(String serializedForm) {
			this.serializedForm = serializedForm;
			deserialized = false;
			eclFile = null;
		}
		
		public void setEclFile(String appName, String compName) {
			if(!deserialized) {
				deserialized = true; // deserialization is only tried once per contract
				try {
					eclFile = CCMUtil.deserializeECLContract(appModel, hwTypes, appName, compName, serializedForm, false);
				} catch (IOException e) {
					log.warn(String.format("Could not deserialize contract %s:%s", appName, compName), e);
					eclFile = null;
				}
			}
		}
		
		public EclFile getEclFile() {
			return eclFile;
		}
	}
	class ShortCut {
		String shortName;
		boolean used;
		public ShortCut(String shortName, boolean used) {
			this.shortName = shortName;
			this.used = used;
		}
		@Override
		public String toString() {
			return shortName + "(used=" + used + ")";
		}
	}
	
	private static Cache<String, StructuralModel> hwTypesCache = new Cache<>(new Function<String, StructuralModel>() {

		@Override
		public StructuralModel apply(String s) {
			log.debug("Recomputing hw-sm model");
			try {
				return CCMUtil.deserializeHWStructModel(s, false);
			} catch (IOException e) {
				log.warn("Could not deserialize HW-SM", e);
			} catch (NullPointerException e) {
				log.warn("No HW-SM found", e);
			}
			return null;
		}
		
	}, hashDifferentCheck, false);
	private StructuralModel hwTypes;
	private ProblemBuffer pb;
	private static ProblemBuffer lastPb;

	private final EclFileCache eclFileCache;
	private class AppNameAndModel {
		String name = "";
		String model = "";
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
//			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((model == null) ? 0 : model.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AppNameAndModel other = (AppNameAndModel) obj;
//			if (!getOuterType().equals(other.getOuterType()))
//				return false;
			if (model == null) {
				if (other.model != null)
					return false;
			} else if (!model.equals(other.model))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
//		private GLPK_ILPGenerator getOuterType() {
//			return GLPK_ILPGenerator.this;
//		}
		
	}
	private static Cache<AppNameAndModel, StructuralModel> appModelCache = new Cache<>(new Function<AppNameAndModel, StructuralModel>() {
		
		@Override
		public StructuralModel apply(AppNameAndModel a) {
			log.debug("Recomputing app model");
			try {
				return CCMUtil.deserializeSWStructModel(a.name, a.model, false);
			} catch (IOException e) {
				log.error("Can not deserialize appModel for " + a.name, e);
			}
			return null;
		}
	}, false);
	private StructuralModel appModel;
	private static Set<String> lastContainers = new HashSet<>();
	private static Request lastRequest;

	private VariantModel sw_vm;


	private long targetTime;

	private final static CcmExpressionInterpreter interpreter = new CcmExpressionInterpreter();

	private static Logger log = Logger.getLogger(GLPK_ILPGenerator.class);
	
	private static final int CONSTRAINT_EQUAL = 1;
	private static final int CONSTRAINT_LESS_THAN_REQS = 2;
	private static final int CONSTRAINT_GREATER_THAN_REQS = 3;

	public GLPK_ILPGenerator() {
		binaryVars = new HashSet<>();
		resourceShortCuts = new HashMap<>();
		resourceReqs = new HashMap<>();
		responseTimes = new HashMap<>();
		runTimes = new HashMap<>();
		providedProperties = new HashMap<>();
		requiredProperties = new HashMap<>();
		pb = new ProblemBuffer();
		eclFileCache = new EclFileCache();
		stopInit = new AtomicBoolean(false);
		reset();
	}
	
	private void reset() {
		binaryVars.clear();
		resourceShortCuts.clear();
		resourceReqs.clear();
		responseTimes.clear();
		runTimes.clear();
		providedProperties.clear();
		requiredProperties.clear();
		pb = new ProblemBuffer();
		stopInit.set(false);
		errorMessage = null;
	}

	public Mapping runILP(Request req, Map<String,EclFile> eclFiles, glp_prob ilp) {
		File reqFile = new File("tmp");
		return runILP(req, eclFiles, reqFile, ilp);
	}

	public Mapping runILP(Request req,
			Map<String,EclFile> eclFiles, File reqFile, glp_prob ilp) {


		Mapping ret = new Mapping();
		try {
			EclFile ecl = eclFiles.values().iterator().next(); //TODO check if all contracts are required here, too
			//persist contract for debugging
			File cinst = new File(reqFile.getAbsolutePath()+"-instance.ecl");
			ecl.eResource().save(new FileOutputStream(cinst), new HashMap<Object,Object>());
		} catch (Exception e) {
			log.error("Cannot save ecl file", e);
		}

//			log.debug("written ilp into file: "+lpFile.getAbsolutePath()+"/"+lpFile.getName());
//			log.debug("written contract instance into file: "+cinst.getAbsolutePath()+"/"+cinst.getName());

		try {
//			// try to patch the Java-Library-Path
//			IWorkspace ws = null;
//			try {
//				ws = ResourcesPlugin.getWorkspace();
//			} catch (IllegalStateException ise) {
//				// ok, do not use the workspace (it is closed)
//			}
//			if(ws != null) {
//				String jlp = System.getProperty("java.library.path");
//				String workspaceLocation = ws.getRoot().getLocationURI().getPath();
//				if(!workspaceLocation.isEmpty()) {
////						workspaceLocation = workspaceLocation.replaceAll("/", "\\\\"); //Windows
//					if(workspaceLocation.startsWith("\\")) workspaceLocation = workspaceLocation.substring(1);
//					if(!jlp.endsWith(File.pathSeparator)) jlp += File.pathSeparator;
//					jlp += workspaceLocation+File.separator;
//				}				
//				System.setProperty("java.library.path", jlp);
//			}
//				log.debug("JLP: "+System.getProperty("java.library.path")
//						.replace(';', '\n')	//Windows
//						.replace(':', '\n')); //Unix
			
			String fname = req.eResource().getURI().trimSegments(1).appendSegment("gip.glpk").toFileString();
			glp_cpxcp parm_write = new glp_cpxcp();
			int result = GLPK.glp_write_lp(ilp, parm_write, fname);
			log.info("Save lp at " + fname + ". Result = " + result + ".");

			long start = System.nanoTime();
			// Solve model
			glp_smcp parm = new glp_smcp();
			if(timeout > 0) {
				parm.setIt_lim(timeout); //set timeout
			}
			GLPK.glp_init_smcp(parm);
			int returnCode;
			if(settings.getILPsolverExact().get()) {
				returnCode = GLPK.glp_exact(ilp, parm);
			}
			else {
				returnCode = GLPK.glp_simplex(ilp, parm);
			}
		
			long duration = System.nanoTime()-start;
			log.info("Simplex took: "+duration/1000/1000+" ms");
			
			// Retrieve solution
			if (returnCode == 0) {
				Set<String> optimalQualityPath = new HashSet<String>();
				log.debug("Value of objective function: " + GLPK.glp_get_obj_val(ilp) +
						(GLPK.glp_mip_obj_val(ilp) != 0 ? " (mip: "+GLPK.glp_mip_obj_val(ilp)+")"
								: "" ));
//					double[] var = solver.getPtrVariables();
				int n = GLPK.glp_get_num_cols(ilp);
				FileUtils.ensureDirectories(true, "logs/csv");
				String valuesLogName = "logs/csv/ilp_"+FileUtils.getFileNameTimestamp()+".csv";

				// schema: solver, timestamp, rows, cols, duration
				String fileName = req.eResource().getURI().trimSegments(1).appendSegment("sol.csv").toFileString();
				FileUtils.writeToCsv(fileName, "GLPK", timestamp(), GLPK.glp_get_num_rows(ilp), GLPK.glp_get_num_cols(ilp), duration);
				log.info("Stats saved to " + fileName);

				FileWriter fw;
				try {
					fw = new FileWriter(valuesLogName);
				} catch (IOException e) {
					log.warn("Can not access " + valuesLogName + ". Thus, not logging values.");
					fw = null;
				}
				for(int i=1; i <= n; i++) {
					String name = GLPK.glp_get_col_name(ilp, i);
					double val  = GLPK.glp_get_col_prim(ilp, i);
					if(fw != null) {
						try {
							FileUtils.writeToCsv(fw, i, name, val);
						} catch (IOException e) {
							// don't write to this file anymore, as further exceptions are likely.
							log.warn("Stop writing to " + valuesLogName, e);
							fw = null;
						}
					}
					if(name != null) {
						if(name.startsWith("b#")) {
							if(val > 0) { log.debug("Value of var[" + i + "] ("+name+") = " + val); }
							if(val == 1 || (settings.getILPfeatureFuzzy().get() && val > 0.5)) {
								optimalQualityPath.add(name);
							}
						}
						else {
							int index;
							if((index = name.indexOf(startTimePropName))!= -1) {
								String compName = name.substring(0, index);
								ret.setStartTime(compName, Math.round(val));
								log.debug(compName + ".startTime="+val);
							}
							if((index = name.indexOf("#"+rawRunTimePropName))!= -1) {
								String compName = name.substring(0, index);
								ret.setRunTime(compName, val);
								log.debug(compName + ".runTime="+val);
							}
							if((index = name.indexOf(responseTimePropName))!= -1) {
								String compName = name.substring(0, index);
								ret.setResponseTime(compName, val);
								log.debug(compName + ".responseTime="+val);
							}
						}
					}
				}
				if(fw != null) {
					try { fw.close();
						log.debug("Values written to " + valuesLogName);
					} catch(IOException ignore) { }
				}
				StringBuilder sb = new StringBuilder("Optimal Mapping:\n");
				for(String str : optimalQualityPath) {
					String[] parts = str.split("#");
					String impl = parts[1];
					String mode = parts[2];
					String server = parts[3];
					for(String x : resourceShortCuts.keySet()) {
						if(resourceShortCuts.get(x).shortName.equals(server)) {
							server = x;
							break;
						}
					}
					ret.put(impl.replaceAll("_", "."),server);
					if(log.isDebugEnabled()) {
						sb.append(impl).append("(").append(mode).append(") => ").append(server).append("\n");
					}
				}
				if(log.isDebugEnabled()) { log.debug(sb.toString()); }
			} else {
				log.warn("The problem could not be solved (returncode = " + returnCode + ")");
			}

			// Free memory
			GLPK.glp_delete_prob(ilp);


		} catch (GlpkException e) {
			log.error("GLPK threw an exception", e);
//			GLPK.glp_term_hook(null, null);
			return null;
		}

		return ret;
	}

	private String timestamp() {
		Calendar cal = Calendar.getInstance();
		Date currentTime = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
		return dateFormat.format(currentTime);
	}
	
	protected double getRuntimeOrHugeValue(String bvn) {
		Double val = runTimes.get(bvn);
		if(val != null) {
			return val;
		}
		return RUNTIME_HUGE_VALUE;
	}
	
	private void checkTimeout() throws TimeoutException {
		if(targetTime > 0 && System.nanoTime() > targetTime) {
			System.out.println(TimeUnit.NANOSECONDS.toMillis(System.nanoTime()) + " > "
					+ TimeUnit.NANOSECONDS.toMillis(targetTime));
			// write error to csv
			Long[] durations = new Long[]{-1L, -1L, -1L, -1L, -1L, (System.nanoTime() - targetTime)/1000000 + timeout * 1000};
			try { storeDurationsInCsv(new SimpleDateFormat().format(GregorianCalendar.getInstance().getTime()), durations); }
			catch (IOException ignore) { }

			throw new TimeoutException();
		}
	}
	
	private void initTimeout(long start) {
		targetTime = timeout < 0 ? -1 : start + TimeUnit.SECONDS.toNanos(timeout);
	}
	
//	ExecutorService pool = Executors.newCachedThreadPool();
	public glp_prob generateILPforRequest(final Request req,
			Map<String, EclFile> eclFiles, boolean verbose,
			final IGlobalEnergyManager gem, final String appName,
			final Map<String, List<String>> fileNameMap)
			throws TimeoutException {
		long start = System.nanoTime();
		initTimeout(start);
		final AtomicBoolean newAppModel = new AtomicBoolean(false);
		
		Thread inits = new Thread(new Runnable() {
			
			@Override
			public void run() {
				initModels();
			}

			private void initModels() {
				// hardware structure model
				String newTypes = ((IGlobalResourceManager) gem.getGlobalResourceManager()).getInfrastructureTypes();
				hwTypesCache.set(newTypes);
				hwTypes = hwTypesCache.get();
				if(hwTypes == null) {
					// fall back to old behaviour
					hwTypes = req.getImport().getModel();
				}
				
				// init application structure model
				AppNameAndModel a = new AppNameAndModel();
				a.name = appName;
				a.model = gem.getApplicationModel(appName);
				// update cache, and store, if new cache value was actually recomputed
				newAppModel.set(appModelCache.set(a));
				appModel = appModelCache.get();
				
				// init application variant model
				Map<String, VariantModel> confs = gem.getCurrentSystemConfigurations();
				if(confs != null) {
					sw_vm = confs.get(appName);
				}
				if(sw_vm == null) {
					// old behaviour
					log.warn("sw_vm==null, fallback to old behavior.");
					String sw_vm_ser = gem.getCurrentSystemConfiguration(appName);
					if(sw_vm_ser != null) {
						ResourceSet rs = CCMUtil.createResourceSet();
						try {
							sw_vm = CCMUtil.deserializeSWVariantModel(rs, appName, sw_vm_ser, false);
						} catch (IOException e) {
							log.warn("SW Variantmodel will not be used.", e);
							// assume no running jobs
							sw_vm = null;
						}
					}
				}

				// ecl files
				eclFileCache.init(gem.getContractsForApp(appName, fileNameMap));
			}
		});
		inits.start();

		glp_prob ilp = GLPK.glp_create_prob();
		GLPK.glp_set_prob_name(ilp, req.getComponent().getName() + "." + req.getTarget().getName());

		Platform platform = req.getHardware();
		VariantModel vm = platform.getHwmodel();
		Resource root = (Resource) vm.getRoot();
		Map<String, Resource> containers = new HashMap<String, Resource>();
		// get all servers
		int rcount = 1;
		long now = System.currentTimeMillis();
		for (Resource r : root.getSubresources()) {
			Long lastChanged = r.getLastChanged();
			if(lastChanged != null && now - lastChanged.longValue() > settings.getILPresourceOfflineTimeout().get()) {
				log.info("Consider resource " + r.getName() + " as offline.");
				if(log.isDebugEnabled()) {
					log.debug("because " + now + " - " + lastChanged.longValue() + "  > " +
							settings.getILPresourceOfflineTimeout().get());
				}
				continue;
			}
			containers.put(r.getName(), r);
			// create initially unused resource
			resourceShortCuts.put(r.getName(), new ShortCut("R"+rcount++, false));
		}
		if(resourceShortCuts.size() == 0) {
			// no containers found, fail fast to avoid errors within glpk
			return setAndWarnError("No containers found. Exiting.");
		}

		// wait to be initialized
		try {
			inits.join(settings.getILPsetupInitTimeout().get() * 1000);
		} catch (InterruptedException e) {
			return setAndWarnError("Could not initialize models. Exiting.", e);
		}
		if(inits.isAlive()) {
			stopInit.set(true);
			return setAndWarnError("Could not initialize models within "+settings.getILPsetupInitTimeout().get()
					+" seconds. Exiting.");
		}
		long startBinvar = System.nanoTime();
		if(settings.getILPfeatureOnlyUpdate().get() && !newAppModel.get() && lastContainers.equals(containers.keySet())
				&& lastPb != null && requestMatch(lastRequest, req)) {
			log.info("** Experimental: Skipping SW updates.");
			pb = lastPb;
			for(ShortCut sc : resourceShortCuts.values()) {
				sc.used = true;
			}
			pb.reloadedLast = true;
		}
		else {
			log.info("starting to generate binary vars");
			log.debug("containers:" + resourceShortCuts);
			generateDecisionVarsAndCache(eclFiles, containers, req, gem, appName, null, 0);
			eliminateDuplicates();
			log.info("binary vars generated");
			lastContainers = containers.keySet();
			pb.reloadedLast = false;
			lastRequest = req;
		}
		// now go for resource constraints
		// for each subresource of containers introduce rules of the following
		// kind
		// usage#container#resource#property <= resource-capacity && >= 0
		long startUsageConstraint = System.nanoTime();
		log.info("Resource usage constraint generation started...");
		for (Resource r : root.getSubresources()) {
			if(settings.getILPlogResourceProcessing().get()) {
				log.debug("processing resource "+r.getName());
			}
			boolean moveOn = true;
			String rname = GeneratorUtil.cleanString(r.getName());
			// check for properties of top resource
			if(settings.getILPfeatureIncludeTopLevelProperties().get()) {
				moveOn = processResource(rname, r);
			}
			if(!moveOn) { continue; }
			for (Resource sr : r.getSubresources()) {
				moveOn = processResource(rname, sr);
				if(!moveOn) { break; }
			}
			if(!moveOn) { continue; }
		}
		checkTimeout();

		for(String propName : providedProperties.keySet()) {
			List<UsageVarAndValue> provList = new ArrayList<UsageVarAndValue>();
			Map<String,CcmValue> provProp = providedProperties.get(propName);
			for(String binaryVar : provProp.keySet()) {
				CcmValue val = provProp.get(binaryVar);
				provList.add(new UsageVarAndValue(binaryVar, val));
			}
			pb.ensureEclVariable(propName);
			pb.addListConstraint(propName, provList, CONSTRAINT_EQUAL);
		}
//		for(String bvn : responseTimes.keySet()) {
//			Double value = responseTimes.get(bvn);
//			CcmReal val = StdlibFactory.eINSTANCE.createCcmReal();
//			val.setRealValue(value);
//			provList.add(new UsageVarAndValue(binaryVar, val));
//			pb.ensureEclVariable(propName);
//			pb.addListConstraint(propName, provList, CONSTRAINT_EQUAL);
//		}

		long startPropertyConstraint = System.nanoTime();
		log.info("Required property constraint generation started...");
		//required properties constraints
		for(Entry<String, PropertyAndMap> e : requiredProperties.entrySet()) {
			String propName = e.getKey();
			List<UsageVarAndValue> reqList = new ArrayList<UsageVarAndValue>();
			Map<String,CcmValue> reqProp = e.getValue().map;
			for(String binaryVar : reqProp.keySet()) {
				CcmValue val = reqProp.get(binaryVar);
				reqList.add(new UsageVarAndValue(binaryVar, val));
			}
			int kind = (isIncreasing(e.getValue().property) ? CONSTRAINT_GREATER_THAN_REQS : CONSTRAINT_LESS_THAN_REQS);
			pb.ensureEclVariable(propName);
			pb.addListConstraint(propName, reqList, kind);
		}
		checkTimeout();

		//request requirements
		if(req.getReqs() != null) {
			for(PropertyRequirementClause prc : req.getReqs()) {
				String name = req.getComponent().getName()+"#"+prc.getRequiredProperty().getDeclaredVariable().getName();
				CcmValue val;
				int kind;
				if(isIncreasing(prc.getRequiredProperty())) {
					val = interpreter.interpret(prc.getMinValue());
					kind = CONSTRAINT_GREATER_THAN_REQS;
				}
				else {
					val = interpreter.interpret(prc.getMaxValue());
					kind = CONSTRAINT_LESS_THAN_REQS;
				}
				pb.ensureEclVariable(name);
				pb.addSingleConstraint(name, val, kind);
			}
		}

		for (String bvn : binaryVars) {
			pb.addBinaryVar(bvn);
		}

		/* TODO change objective function according to profile. make better nuances of utility, e.g. "0.5" for normal, "1" for utility
		 * This will need adaptions in the contracts */
		switch(profile) {
		case PROFILE_ENERGY_SAVING: generateMinEnergyObjectiveFunction(0); break;
		case PROFILE_NORMAL: 
		case PROFILE_UTILITY: 
		default: generateMinEnergyObjectiveFunction(1);
		}
		long startBuffer = System.nanoTime();
		pb.writeBuffer(ilp);
		if(settings.getILPfeatureOnlyUpdate().get()) { lastPb = pb.copy(); }
		/* We have the following times:
		 * start
		 * binVar
		 * usageConstraint
		 * propertyConstraint
		 * buffer
		 */
		long end = System.nanoTime();
		long durationSetup = startBinvar - start;
		long durationBinVar = startUsageConstraint - startBinvar;
		long durationUsage = startPropertyConstraint - startUsageConstraint;
		long durationProperty = startBuffer - startPropertyConstraint;
		long durationBuffer = end - startBuffer;
		long durationTotal = end - start;
		
		Long[] durations = new Long[]{durationSetup, durationBinVar, durationUsage,
				durationProperty, durationBuffer, durationTotal};
		for (int i = 0; i < durations.length; i++) {
			durations[i] = durations[i]/1000000;
		}

		log.info(String.format("ILP generation done: setup=%s ms, binVar=%s ms, usage=%s ms,"
				+ " property=%s ms, buffer=%s ms, total=%s ms", (Object[])durations));
		log.debug(binaryVars.size()+" binary vars");
		try {
			storeDurationsInCsv(new SimpleDateFormat().format(GregorianCalendar.getInstance().getTime()), durations);
		} catch (IOException ignore) { }
		return ilp;
	}

	/**
	 * @param lastRequest2
	 * @param req
	 * @return
	 */
	private boolean requestMatch(Request lastRequest, Request req) {
		if(lastRequest == null) { return false; }
		boolean match = true;
		match &= safeEquals(lastRequest.getTarget().getName(), req.getTarget().getName());
		match &= safeEquals(lastRequest.getComponent().getName(), req.getComponent().getName());
		EList<MetaParamValue> lastMvpValues = lastRequest.getMetaParamValues();
		EList<MetaParamValue> currentMvpValues = req.getMetaParamValues();
		if(lastMvpValues.size() != currentMvpValues.size()) { return false; }
		for (int i = 0; i < lastMvpValues.size(); i++) {
			MetaParamValue lastMvp = lastMvpValues.get(i);
			MetaParamValue currentMvp = currentMvpValues.get(i);
			match &= safeEquals(lastMvp.getMetaparam().getName(), currentMvp.getMetaparam().getName());
			match &= lastMvp.getValue() == currentMvp.getValue();
		}
		return match;
	}
	private boolean safeEquals(Object o1, Object o2) {
		if(o1 == null) { return o2 == null; }
		return o1.equals(o2);
	}
	private glp_prob setAndWarnError(String message) {
		return setAndWarnError(message, null);
	}

	private glp_prob setAndWarnError(String message, Throwable t) {
		log.warn(message, t);
		errorMessage = message;
		return null;
	}
	
	/**
	 * Create usage variables with value from property bindings.
	 * Affects
	 * <ul>
	 * <li>{@link ProblemBuffer#usageVars usageVars}</li>
	 * <li>{@link ProblemBuffer#listConstraints listConstraints}</li>
	 * </ul>
	 * Honors
	 * <ul>
	 * <li>ignoreUnknownProperties</li>
	 * <li>logModeServerMismatch</li>
	 * </ul>
	 * @param rname the name of the parent resource
	 * @param r the resource to process
	 * @return <code>true</code> if sucessfully processed, or <code>false</code> if resource should be skipped
	 */
	protected boolean processResource(String rname, Resource r) {
		String srname = GeneratorUtil.cleanString(r.getName());
		ShortCut currentResShortCut = resourceShortCuts.get(rname);
		if(currentResShortCut == null) {
			// resource is offline 
			return false;
		}
		if(!currentResShortCut.used) {
			// resource is not used
			log.debug("Resource " + rname + "(" + currentResShortCut.shortName + ") not used. Skipping.");
			return false;
		}
		String tname = r.getSpecification().getName();
		for (VariantPropertyBinding vpb : r.getPropertyBinding()) {
			String pname = GeneratorUtil.cleanString(vpb.getProperty()
					.getDeclaredVariable().getName());
			Expression ve = vpb.getValueExpression();
			CcmValue val;
			String usageVar = "usage#" + currentResShortCut.shortName + "#" + srname + "#"
					+ pname;
			if(ve != null) {
				val = interpreter.interpret(ve);
			}
			else {
				// ve is null, construct a default val
				if(settings.getILPfeatureIgnoreUnknownProperties().get()) {
					// create a unbound usage var, i.e. an ecl var
//							pb.ensureEclVariable(usageVar);
					pb.addUsageVar(usageVar, null);
					continue;
				}
				val = getDefaultValueFor(vpb.getProperty(), srname);
			}

			// as of now, ignore strings (difficult to compare them within an ILP)
			if(val instanceof CcmString) continue;

			pb.addUsageVar(usageVar, val);
			// add resource reqs per impl if they exist
			boolean reqExists = false;
			List<UsageVarAndValue> reqList = new ArrayList<UsageVarAndValue>();
			for (String mode : resourceReqs.keySet()) {
				// check container coded into mode and compare with current container
				String modeContainer = mode.substring(mode.lastIndexOf('#')+1);
				if(!currentResShortCut.shortName.equals(modeContainer)) {
					// mode not for current server
					if(settings.getILPlogModeServerMismatch().get()) {
						log.debug("Mode mismatch " + currentResShortCut.shortName + " - " + mode);
					}
					continue;
				}
				List<String> processedProperties = new ArrayList<String>();
				Set<HWComponentRequirementClause> hrcs = resourceReqs
						.get(mode);
				for (HWComponentRequirementClause hrc : hrcs) {
					for (PropertyRequirementClause prc : hrc
							.getRequiredProperties()) {
						//need to identify those relevant for current usage var
						//will have the form: usage#container#resource#property = val*binaryvar
						//the binaryvar contains the mode, the val is expressed as res req in this mode
						if(prc.getRequiredProperty().getDeclaredVariable() != null &&
								hrc.getRequiredResourceType().getName() != null)
							if(hrc.getRequiredResourceType().getName().equals(tname) &&
									prc.getRequiredProperty().getDeclaredVariable().getName().equals(pname)) {
								if(processedProperties.contains(prc.getRequiredProperty().getDeclaredVariable().getName()))
									continue;
								Statement stmt = prc.getMinValue();
								if(stmt == null) {
									stmt = prc.getMaxValue();
								}
								CcmValue reqVal;
								if(stmt != null) {
									reqVal = interpreter.interpret(stmt);
								}
								else {
									// skip prc
									continue;
									// stmt still null. not good. use default value.
//											reqVal = getDefaultValueFor(prc.getRequiredProperty(), srname);
								}
								reqList.add(new UsageVarAndValue("b#"+mode, reqVal));
								reqExists = true;
								processedProperties.add(prc.getRequiredProperty().getDeclaredVariable().getName());
							}
					}
				}
			}
			if(reqExists) {
				pb.addListConstraint(usageVar, reqList, CONSTRAINT_EQUAL);
			}
		}
		return true;
	}
	
	private void storeDurationsInCsv(String currentDate, Long[] durations) throws IOException {
		FileWriter fw = new FileWriter("logs/durations.csv", true);
		int usedResources = 0;
		for(ShortCut sc : resourceShortCuts.values()) { if(sc.used) { usedResources++; } }
		fw.append(String.format("%s,%s,%s,%s,%s\n", problemName, currentDate, toCommaSeperatedValue(durations),
				resourceShortCuts.size(), usedResources));
		fw.close();
	}
	
	private String toCommaSeperatedValue(Long[] durations) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(Long d : durations) {
			if(first) { first = false; } else { sb.append(","); }
			sb.append(d);
		}
		return sb.toString();
	}
	private CcmValue getDefaultValueFor(Property p, String srname) {
		CcmValue val;
		if(log.isDebugEnabled()) {
			log.debug("Construct default value for " +srname + "." + p.getDeclaredVariable().getName());
		}
		CcmType type = p.getResultType();
		CcmStandardLibraryTypeFactory typeFac = CcmStandardLibraryTypeFactory.eINSTANCE;
		StdlibFactory valueFac = StdlibFactory.eINSTANCE;
		if(type.conformsTo(typeFac.getBooleanType())) {
			val = valueFac.createCcmBoolean();
			((CcmBoolean) val).setBooleanValue(false);
		}
		else if(type.conformsTo(typeFac.getRealType())) {
			val = valueFac.createCcmReal();
			((CcmReal) val).setRealValue(0f);
		}
		else if(type.conformsTo(typeFac.getIntegerType())) {
			val = valueFac.createCcmInteger();
			((CcmInteger) val).setIntegerValue(0);
		}
		else if(type.conformsTo(typeFac.getStringType())) {
			val = valueFac.createCcmString();
			((CcmString) val).setStringValue("");
		}
		else {
			log.error("Property does not have suitable type to deduce default value.");
			val = null;
		}
		return val;
	}

	private static boolean isIncreasing(Property property) {
		return property.getValOrder().equals(Order.INCREASING);
	}

	/**
	 * This method drops duplicate syntactic/selection constraints, which occur due to the double nature of
	 * {@link #generateDecisionVarsAndCache(Map, Map, Request, OsgiGem, String, Set)}
	 *  (i.e., selection constraints + filling cache).
	 */
	private void eliminateDuplicates() {
		pb.eliminateDuplicates();
	}

//	/**
//	 * @param usageVars
//	 * @return
//	 */
//	private String generateMinUsageObjectiveFunction(Set<String> usageVars) {
//		String objective = "min: ";
//		for(String uv : usageVars) {
//			objective += uv + " + ";
//		}
//		objective = objective.substring(0,objective.length()-3);
//		objective += ";\n\n";
//		return objective;
//	}

	/**
	 * Generate the objective.
	 * Currently, if there is no utility specified, the contract will also be considered.
	 * The alternative would be to let utility default to 0 (and therefor ignore the contract,
	 *  if requiredUtility is greater than 0).
	 * @param requiredUtility minimum utility required.
	 */
	/* former parameter: List<VarnameAndUpperBound> usageVars, Request req */
	private void generateMinEnergyObjectiveFunction(double requiredUtility) {
		Map<String, Double> objective = new HashMap<>();
		final double IGNORE = 9e5;
		for(Entry<String, Map<String, CcmValue>> eProp : providedProperties.entrySet()) {
			String propName = eProp.getKey();
			if(isUtility(propName)) {
				// check utility values
				for(Entry<String, CcmValue> eBinary : eProp.getValue().entrySet()) {
					double utility = CCMUtil.getRealValue(eBinary.getValue());
					if(utility < requiredUtility) {
						// put a flag for the according bv
						String bv = eBinary.getKey();
						objective.put(bv, IGNORE);
						log.debug("Ignoring " + bv + " because utility = " + utility + " < " + requiredUtility);
						pb.addIgnored(bv);
					}
				}
			}
			else {
				for(Entry<String, CcmValue> eBinary : eProp.getValue().entrySet()) {
					String bv = eBinary.getKey();
					double val = CCMUtil.getRealValue(eBinary.getValue());
					double energyRateSum = getEnergyRateSum(bv);
					double bvEnergySum = CollectionsUtil.get(objective, bv, 0.0);
					if(bvEnergySum == IGNORE) { continue; }
					objective.put(bv, bvEnergySum + (val * energyRateSum));
				}
			}
		}
		for(Entry<String, Double> e : objective.entrySet()) {
			if(e.getValue() == IGNORE) { continue; }
			pb.addObjective(e.getKey(), e.getValue());
		}
//		for(String bv : binaryVars) {
//			pb.addObjective(bv, getWeightForMappingVar(bv, new CcmExpressionInterpreter(), req));
//		}
	}

	/**
	 * @param propName
	 * @return
	 */
	private boolean isUtility(String propName) {
		return propName.endsWith("#utility");
	}
	
	private double getEnergyRateSum(String bv) {
		String reqsByImplMode = bv.substring(2);
		Set<HWComponentRequirementClause> reqs = resourceReqs
				.get(reqsByImplMode);
		// for components with no hardware requirements, return weight of 0
		if(reqs == null)
			return 0;
		double result = 0;
		for (HWComponentRequirementClause hrc : reqs) {
			double energyRate = hrc.getEnergyRate();
			result += energyRate;
		}
		return result;
	}

//	/**
//	 * Computes the weight of a mapping variable (b#VLC#highQ#Server1) by
//	 * interpreting its resource requirements. Currently, the sum of all NFRs is
//	 * used. Later, energy should be used to normalize the values.
//	 * 
//	 * @param bv
//	 *            name of the mapping variable
//	 * @return double - weight of variable in terms of resource requirements
//	 */
//	private double getWeightForMappingVar(String bv, Request r) {
//		// read from resourceReqs (bv without b# and #container)
//		double weight = 0;
//
//		String reqsByImplMode = bv.substring(2);
//		Set<HWComponentRequirementClause> reqs = resourceReqs
//				.get(reqsByImplMode);
//		// for components with no hardware requirements, return weight of 0
//		if(reqs == null)
//			return 0;
//		for (HWComponentRequirementClause hrc : reqs) {
//			double energyRate = hrc.getEnergyRate();
//			if(objectiveSimpleWeight.get()) {
//				// get response time, multiply with energyRate, add to weight
//				double responseTime = responseTimes.get(bv);
//				weight += energyRate * responseTime;
//				continue;
//			}
//			ResourceType resType = hrc.getRequiredResourceType();
//			for (PropertyRequirementClause prc : hrc.getRequiredProperties()) {
//				if(prc.getMinValue() == null && prc.getMaxValue() == null && prc.getFormula() != null) {
//					weight = 0;
//					log.debug("The Formula "+prc.getFormula().getName()+" for "+bv+" has not been evaluated..");
//					return weight;
//				}
////				Property prop = prc.getRequiredProperty(); Variable propVar = prop.getDeclaredVariable();
////				log.debug("Property " + (propVar != null ? propVar.getName() : "?") + " : " + prop);
////				if(prc.getRequiredProperty().getValOrder().equals(Order.INCREASING)) {
//				if(prc.getMinValue() != null) {
//					if(prc.getMinValue() instanceof FunctionExpression) {
//						Expression e = ((VariableAssignment)((FunctionExpression)prc.getMinValue()).getSourceExp()).getValueExpression();
//						weight = CCMUtil.getRealValue(interpreter.interpret(e));
//					} else {
//						weight = CCMUtil.getRealValue(interpreter.interpret(prc.getMinValue()));
//					}
//				} else {
//					if(prc.getMaxValue() instanceof FunctionExpression) {
//						Expression e = ((VariableAssignment)((FunctionExpression)prc.getMaxValue()).getSourceExp()).getValueExpression();
//						weight = CCMUtil.getRealValue(interpreter.interpret(e));
//					} else {
//						weight = CCMUtil.getRealValue(interpreter.interpret(prc.getMaxValue()));
//					}
//				}
//				//the time is to be multiplied with the power implied by the hardware setup specified in the quality mode
//				//-> simulate the resource in that mode using the computed workload (e.g., cpu_time)
//				//TODO overcome assumption: only one resource of a type per server
//				String sName = bv.substring(bv.lastIndexOf("#")+1);
//				for(String longName : resourceShortCuts.keySet())
//					if(resourceShortCuts.get(longName).equals(sName)) sName = longName;
//				//now access variant model of infrastructure...
//				Resource root = (Resource)r.getHardware().getHwmodel().getRoot();
//				for(Resource server : root.getSubresources()) {
//					if(server.getName().equals(sName)) {
//						for(Resource res : server.getSubresources()) {
//							if(res.getSpecification().getName().equals(resType.getName())) {
//								Behavior behavior = res.getBehavior();
//								if(behavior != null) {
//									if(behavior.getTemplate() instanceof StateMachine) {
//										StateMachine sm = (StateMachine)behavior.getTemplate();
//										//now set the mode for the resource and evaluate the behavior using the workload from above
//										Workload w = BehaviorFactory.eINSTANCE.createWorkload();
//										Occurrence o = BehaviorFactory.eINSTANCE.createOccurrence();
//										o.setTime(0);
//										o.setLoad((int)weight);
//										//TODO get correct pin (currently only works if there's just one pin)
//										Pin pin = sm.getPins().iterator().next();
//										o.setPin(pin);
//										w.getItems().add(o);
//										w.setResource(res);
////										Simulator sim = new Simulator();
//										List<CostParameterBinding> bindings = new ArrayList<CostParameterBinding>();
//										for(State state : sm.getStates()) {
//											CostParameter cp = state.getParameter().iterator().next(); //there should be always only one
//											CostParameterBinding cpb = VariantFactory.eINSTANCE.createCostParameterBinding();
//											cpb.setParameter(cp);
//											Variable v = VariablesFactory.eINSTANCE.createVariable();
//											v.setName(cp.getName());
//											RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
//											rle.setValue(energyRate);
//											v.setInitialExpression(rle);
//											cpb.setDeclaredVariable(v);
//											bindings.add(cpb);
//										}
//										// reset timer and power
//										sm.reset();
////										sm = sim.simulate(sm, w, bindings);
//										CcmReal energy = ((CcmReal)sm.getPower().getDeclaredVariable().getValue());
//										if(energy != null) {
//											weight = energy.getRealValue();
//											CcmInteger timer = ((CcmInteger)sm.getTimer().getDeclaredVariable().getValue());
//											log.debug("simulated ("+res.getName()+" | "+bv+"): "+weight+" in "
//											+timer.getIntegerValue());
//											return weight;
//										}
//									}
//								} else {
//									log.debug("Behavior of "+res.getName()
//											+" should be set to compute the induced energy consumption.");
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//
//		return weight;
//	}

	private final Map<SWContractMode, Set<String>> processedServersByMode = new HashMap<>();
	private TheatreSettings settings;
	public int timeout;

	private void generateDecisionVarsAndCache(Map<String, EclFile> eclFiles,
			Map<String, Resource> containers, Request req,
			IGlobalEnergyManager gem, String appName, Set<String> depBvn,
			int depth) throws TimeoutException {
		checkTimeout();
		if(log.isDebugEnabled()) {
			log.debug("eclFiles.keySet="+eclFiles.keySet());
			log.debug("containers.keySet="+containers.keySet());
		}
		//deptype,reftype,container,contract
		Map<String, Map<String, Map<String, EclFile>>> deps = new HashMap<String, Map<String, Map<String, EclFile>>>();

		List<String> vars = new ArrayList<String>();
		for (String sName : containers.keySet()) {
			if(depth == 0) { log.debug(sName); }
			EclFile ecl = eclFiles.get(sName);
			if(ecl == null) {
				if(settings.getILPlogMissingEcl().get()) {
					log.debug("Skipping " + sName);
				}
				continue;
			}
			if(log.isDebugEnabled()) {
				org.eclipse.emf.ecore.resource.Resource res = ecl != null ? ecl.eResource() : null;
				String uri = res != null ? ""+res.getURI() : "null";
				log.debug(String.format("(%d) generate vars for %s on %s", depth, uri, sName));
			}

			for (EclContract ctr : ecl.getContracts()) {
				if (ctr instanceof SWComponentContract) {
					SWComponentContract swc = (SWComponentContract) ctr;
					//set metaparameter values from request 
					setMetaparameters(req, ctr);
					String implName = GeneratorUtil.cleanString(swc.getName());
					for (SWContractMode swm : swc.getModes()) {
						String modeName = GeneratorUtil.cleanString(swm.getName());
						// generate binary vars (impl/mode mapped to server =
						// true/false)

						ShortCut shortCut = resourceShortCuts.get(sName);
						String bvn = "b#"+implName + "#" + modeName + "#"
								+ shortCut.shortName;
						shortCut.used = true;
						vars.add(bvn);
						binaryVars.add(bvn);
						pb.registerBvnToCompName(swc.getComponentType().getName(), bvn);
						
						Set<String> processedServers = CollectionsUtil.ensureHashSet(processedServersByMode, swm);
						if(processedServers.add(sName)) {
							//provided property per mode
							for(SWContractClause cl : swm.getClauses()) {
								if(cl instanceof ProvisionClause) {
									handleProvisions(req, bvn, (ProvisionClause) cl, swc.getComponentType(),
											sName, sw_vm, containers.get(sName));
								} else if (cl instanceof SWComponentRequirementClause) {
									handleSWRequirements(req, deps, bvn, (SWComponentRequirementClause) cl,
											gem, appName);
								} else if (cl instanceof HWComponentRequirementClause) {
									handleHWRequirements(req, implName, modeName,
											(HWComponentRequirementClause) cl,
											resourceShortCuts.get(sName).shortName);
								}
							}
						}
						else {
							if(logOnce(swm)) {
								log.debug("Already processed " + swm.getName() + " on " + sName);
							}
//								if(swm.getName().equals("simpleGetFile")) {
//									System.out.println("continue " + swm);
//								continue;
//								}
						}
					}
				}
				else {
					log.warn("SWComponentContract required, but got " + ctr);
				}
			}
		}
		log.debug("deps: " + deps);

		if(depBvn != null && depBvn.size() > 0) {
			pb.addBinaryVarsEqualsConstraint(vars, depBvn);
		} else {
			pb.addBinaryVarsConstraint(vars, 1);
		}

		//recursively follow dependencies
		int newDepth = ++depth;
		for (String newDepBvn : deps.keySet()) {
			Map<String, Map<String, EclFile>> depFilesPerContainerByDependentType = deps.get(newDepBvn);
			if(depFilesPerContainerByDependentType == null) {
				throw new RuntimeException("Unresolved ECL contract dependency.");
			}
			for(String refType : depFilesPerContainerByDependentType.keySet()) {
				Map<String, EclFile> depFilesPerContainer = depFilesPerContainerByDependentType.get(refType);
				generateDecisionVarsAndCache(depFilesPerContainer, containers, req, gem, appName, deps.keySet(), newDepth);
				depFilesPerContainer = null;
			}
		}
		deps = null;
	}

//	private static String toRosgi(String ip) {
//		return "r-osgi://"+ip+":9278";
//	}
	/**
	 * @param req
	 * @param implName
	 * @param modeName
	 * @param cl
	 */
	private void handleHWRequirements(Request req, String implName,
			String modeName, HWComponentRequirementClause hrc, String server) {
		Set<HWComponentRequirementClause> reqs = resourceReqs
				.get(implName + "#" + modeName + "#" + server);
		if (reqs == null) {
			reqs = new HashSet<HWComponentRequirementClause>();
		}
		reqs.add(hrc);
		resourceReqs.put(implName + "#" + modeName + "#" + server, reqs);

		for(PropertyRequirementClause prc : hrc.getRequiredProperties()) {
			CcmReal val;
			boolean min = true;
			Statement minmaxval = prc.getMinValue();
			if(minmaxval == null) {
				minmaxval = prc.getMaxValue();
				min = false;
			}
			if(minmaxval instanceof FunctionExpression) {
				val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
				rle.setValue(val.getRealValue());
				if(min)
					prc.setMinValue(rle);
				else
					prc.setMaxValue(rle);
			} 
		}
	}

	/*	public static RemoteOSGiService getRemoteOSGiService() {
		BundleContext context = FrameworkUtil.getBundle(ILPGenerator.class)
				.getBundleContext();
		ServiceReference remoteRef = context
				.getServiceReference(RemoteOSGiService.class.getName());
		if (remoteRef == null) {
			System.err.println("Error: R-OSGi not found!");
		}
		RemoteOSGiService remote = (RemoteOSGiService) context
				.getService(remoteRef);
		return remote;
	}
	 */
	/**
	 * @param req
	 * @param deps
	 * @param bvn
	 * @param cl
	 */
	private void handleSWRequirements(Request req, Map<String, Map<String, Map<String, EclFile>>> deps,
			String bvn, SWComponentRequirementClause swcrc, IGlobalEnergyManager gem, String appName) {
		SWComponentType t = swcrc.getRequiredComponentType();
		// add requirements for startTimeConstraint
		EObject mode = swcrc.eContainer();
		if(mode != null && mode instanceof SWContractMode) {
			EObject contract = mode.eContainer();
			if(contract != null && contract instanceof SWComponentContract) {
				SWComponentContract swcc = (SWComponentContract) contract;
				StartTimeConstraint stc = pb.getOrCreateStartTimeConstraint(swcc.getComponentType());
				// add required component start time
				stc.addStartTimeOfReq(t.getName());
				// endTimes will be computed later, just register a proxy value here
				pb.registerProxy(swcc.getComponentType(), t.getName());
			}
		}
		//load EclFiles of dependent component type per container
		Map<String, EclFile> referrencedContractByContainer = eclFileCache.getEclFiles(appName, t.getName());
		CollectionsUtil.ensureHashMap(deps, bvn).put(t.getName(), referrencedContractByContainer);

		for(PropertyRequirementClause prc : swcrc.getRequiredProperties()) {
			boolean min = true;
			Statement minmaxval = prc.getMinValue();
			if(minmaxval == null) {
				minmaxval = prc.getMaxValue();
				min = false;
			}
			CcmReal val;
			if(minmaxval instanceof FunctionExpression) {
				val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
				rle.setValue(val.getRealValue());
				if(min)	prc.setMinValue(rle);
				else prc.setMaxValue(rle);
			} else if (prc.getFormula() != null) {
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
				rle.setValue(0);
				val = (CcmReal) interpreter.interpret(rle);
			}	else {
				val = (CcmReal) interpreter.interpret(minmaxval);
			}
			if(min) val.setAscending(false); else val.setAscending(true);
			Property reqProp = prc.getRequiredProperty();
			String propName = swcrc.getRequiredComponentType().getName() + "#"
					+ reqProp.getDeclaredVariable().getName();
			PropertyAndMap reqVal = requiredProperties.get(propName);
			if(reqVal == null) {
				reqVal = new PropertyAndMap(reqProp);
				requiredProperties.put(propName, reqVal);
			}
			reqVal.map.put(bvn, val);
		}
	}
	
	/**
	 * @param req request
	 * @param bvn binary variable name
	 * @param pc provision clause in the contract
	 * @param sName serverName (containerName). Part of duplicate information with sw_vm for resource.
	 * @param sw_vm software VariantModel. Part of duplicate information with sName for resource.
	 * @param resource container of the provision. Duplicate information for (sw_vm,sName)
	 */
	private void handleProvisions(Request req, String bvn,
			ProvisionClause pc, SWComponentType t, String sName, VariantModel sw_vm, Resource resource) {
		CcmReal val = null;
		boolean isMinPropery = true;
		Statement minmaxval = pc.getMinValue();
		if(minmaxval == null) {
			minmaxval = pc.getMaxValue();
			isMinPropery = false;
		}
		if(minmaxval instanceof FunctionExpression) {
			val = (CcmReal)GeneratorUtil.evalFormula(minmaxval, req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if(isMinPropery)
				pc.setMinValue(rle);
			else 
				pc.setMaxValue(rle);
		} else if (pc.getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmReal) interpreter.interpret(rle);
		} else {
			val = (CcmReal) interpreter.interpret(minmaxval);
		}
		val.setAscending(isMinPropery);
		String propVarName = pc.getProvidedProperty().getDeclaredVariable().getName();
		String propName = t.getName() + "#" + propVarName;
		Map<String, CcmValue> p = getOrCreateProperties(propName);
		if(isRunTime(propVarName, pc)) {
			runTimes.put(bvn, val.getRealValue());
			// get schedule of container and add predicted remaining times
			// if started, add (startTime - currentTime) to predictedExecTime. Else use predictedExecTime
			Schedule schedule = getSchedule(sw_vm, sName, resource);
			double predictedQueueTime = 0;
			if(schedule != null) {
				for(Job job : schedule.getJobs()) {
					if(!shouldIgnore(job)) {
						double plus = 0;
						if(job.getStartTime() != null) {
							// job was already started.
							// Attention (unit-clash): currentTimeMillis = milliseconds,
							// jobStartTime = milliseconds, jobPredictedRuntime = seconds
							 plus = -currentTimeMillis() + job.getStartTime();
						}
						plus += job.getPredictedRuntime()*1000;
						if(plus > 0) {
							// only consider jobs currently running or scheduled
							predictedQueueTime += plus;
						}
					}
				}
			}
			StartTimeConstraint stc = pb.getOrCreateStartTimeConstraint(t);
			if(predictedQueueTime != 0) {
				double runTime = val.getRealValue();
				double responseTime = predictedQueueTime + runTime;
				log.debug(String.format("Adding %s to get response-time of %s for %s",
						predictedQueueTime, bvn, sName));
				stc.addQueueLength(predictedQueueTime, bvn);
				responseTimes.put(bvn, responseTime);
			}
			pb.putProxyValue(t, bvn, val.getRealValue());
		}
		p.put(bvn, val);
	}
	
	/**
	 * Ignore a job, if
	 * <ul>
	 * <li>its impl is a ConnectorType</li>
	 * <li>its impl is a copyFileType</li>
	 * <li>its result is not empty, i.e. it is finished</li>
	 * </ul>
	 * @param job the job
	 * @return whether to ignore the given job
	 */
	private boolean shouldIgnore(Job job) {
		if(job == null) { return true; }
		ComponentType spec = job.getImpl().getSpecification();
		boolean running = JobStatus.STARTED.equals(job.getStatus());
		return spec instanceof SWConnectorType || spec instanceof CopyFileType ||
				job.getRawResult() != null || !running;
	}
	protected long currentTimeMillis() {
		return System.currentTimeMillis();
	}

	private Map<String, CcmValue> getOrCreateProperties(String propName) {
		Map<String, CcmValue> p = providedProperties.get(propName);
		if(p == null) {
			p = new HashMap<>();
			providedProperties.put(propName, p);
		}
		return p;
	}

	/** propVarName.equals("run_time") 
	 * @param pc currently unused */
	private static boolean isRunTime(String propVarName, ProvisionClause pc) {
		return propVarName.equals(rawRunTimePropName);
	}

	private static Schedule getSchedule(VariantModel sw_vm, String sName, Resource resource) {
		if(resource instanceof ContainerProvider) {
			Schedule schedule = ((ContainerProvider) resource).getSchedule();
			if(schedule != null) {
				return schedule;
			}
		}
		// search through model to find a job with a schedule contained in a ContainerProvider with the given name
		SWComponent root = (SWComponent) sw_vm.getRoot();
		for(SWComponent sub : root.getSubtypes()) {
			for(Job job : sub.getJobs()) {
				EObject scheduleObject = job.eContainer();
				if(scheduleObject != null && scheduleObject instanceof Schedule) {
					Schedule schedule = (Schedule) scheduleObject;
					EObject container = schedule.eContainer();
					if(container != null && container instanceof ContainerProvider) {
						String containerName = ((ContainerProvider) container).getName();
						if(containerName.equals(sName)) {
							return schedule;
						}
					}
				}
			}
		}
		// Assumption: No schedule found for a container with the given sName
		return null;
	}

	/**
	 * @param req
	 * @param ctr
	 */
	private static void setMetaparameters(Request req, EclContract ctr) {
		if(((SWComponentContract) ctr).getPort() != null) {
			for(Parameter mp : ((SWComponentContract) ctr).getPort().getMetaparameter()) {
				for(MetaParamValue mpv : req.getMetaParamValues()) {
					if(mpv.getMetaparam().getName().equals(mp.getName())) {
						RealLiteralExpression ile = LiteralsFactory.eINSTANCE.createRealLiteralExpression();
						ile.setValue(mpv.getValue());
						mp.setValue(interpreter.interpret(ile));
					}
				}
			}
		}
	}
	
	@Override
	public Mapping optimize(Object[] args) {
//		Activator.reloadProperties();
		try {
			log.debug("Starting optimization");
			Request req = (Request)args[0];
			@SuppressWarnings("unchecked") Map<String, EclFile> eclFiles = (Map<String, EclFile>)args[1];
			IGlobalEnergyManager gem = (IGlobalEnergyManager)args[2];
			@SuppressWarnings("unchecked") Map<String, Object> options = (Map<String, Object>) args[3];
			Boolean verbose = CollectionsUtil.get(options, OPTION_VERBOSE, false);
			String appName = (String) options.get(OPTION_APPNAME);
			Map<String, List<String>> fileNameMap = CollectionsUtil.get(
					options, OPTION_RESOURCENAMES, new HashMap<String, List<String>>());
			problemName = CollectionsUtil.get(options, OPTION_PROBLEMNAME, appName.replace(',', '_'));
			profile = CollectionsUtil.get(options, OPTION_PROFILE, PROFILE_NORMAL);
			timeout = CollectionsUtil.get(options, OPTION_TIMEOUT, -1);
			if(settings.getILPcleanStart().get()) {
				pb = new ProblemBuffer();
				reset();
			}
			glp_prob ilp = generateILPforRequest(req, eclFiles, verbose, gem, appName, fileNameMap);
			if(ilp == null) { return new Mapping(errorMessage); }
			return runILP(req, eclFiles, ilp);
		} catch(Exception e) {
			setAndWarnError("Exception on optimize", e);
		}
		finally {
			if(log.isDebugEnabled() && !debugging.isEmpty()) {
				for(String indVarname : debugging.keySet()) {
					log.debug(indVarname + "=" + debugging.get(indVarname));
				}
			}
		}
		return null;
	}
	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.Optimizer#getId()
	 */
	@Override
	public String getId() {
		return "org.haec.optimizer.ilp";
	}
	
	public void setTheatreSettings(TheatreSettings s) {
		this.settings = s;
	}
}
