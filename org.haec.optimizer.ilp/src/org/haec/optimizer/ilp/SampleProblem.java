/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.ilp;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.GlpkException;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;

/**
 * A simple problem with a known solution. Used to check if GLPK works and is configured correctly.
 * @author René Schöne
 */
public class SampleProblem implements Runnable {
	
	private static Logger log = Logger.getLogger(SampleProblem.class);
	/** Write problem after solving */
	static boolean writeProblemAtFinish = true;
	/** if <code>true</code> use write_lp. Otherwise use write_prob */
	static boolean useLp = false;
	final static String fname = "easy.lp";
	
    //	Minimize z = -.5 * x1 + .5 * x2 - x3 + 1
    //
    //	subject to
    //	0.0 <= x1 - .5 * x2 <= 0.2
    //  -x2 + x3 <= 0.4
    //	where,
    //	0.0 <= x1 <= 0.5
    //	0.0 <= x2 <= 0.5
    //  0.0 <= x3 <= 0.5

	@Override
    public void run() {
        glp_prob lp;
        glp_smcp parm;
        SWIGTYPE_p_int ind;
        SWIGTYPE_p_double val;
        int ret;

        try {
            // Create problem
            lp = GLPK.glp_create_prob();
            log.debug("Problem creation");
            GLPK.glp_set_prob_name(lp, "myProblem");

            // Define columns
            GLPK.glp_add_cols(lp, 3);
            GLPK.glp_set_col_name(lp, 1, "x1");
            GLPK.glp_set_col_kind(lp, 1, GLPKConstants.GLP_CV);
            GLPK.glp_set_col_bnds(lp, 1, GLPKConstants.GLP_DB, 0, .5);
            GLPK.glp_set_col_name(lp, 2, "x2");
            GLPK.glp_set_col_kind(lp, 2, GLPKConstants.GLP_CV);
            GLPK.glp_set_col_bnds(lp, 2, GLPKConstants.GLP_DB, 0, .5);
            GLPK.glp_set_col_name(lp, 3, "x3");
            GLPK.glp_set_col_kind(lp, 3, GLPKConstants.GLP_CV);
            GLPK.glp_set_col_bnds(lp, 3, GLPKConstants.GLP_DB, 0, .5);

            // Create constraints

            // Allocate memory
            ind = GLPK.new_intArray(3);
            val = GLPK.new_doubleArray(3);

            // Create rows
            GLPK.glp_add_rows(lp, 2);

            // Set row details
            GLPK.glp_set_row_name(lp, 1, "c1");
            GLPK.glp_set_row_bnds(lp, 1, GLPKConstants.GLP_DB, 0, 0.2);
            GLPK.intArray_setitem(ind, 1, 1);
            GLPK.intArray_setitem(ind, 2, 2);
            GLPK.doubleArray_setitem(val, 1, 1.);
            GLPK.doubleArray_setitem(val, 2, -.5);
            GLPK.glp_set_mat_row(lp, 1, 2, ind, val);

            GLPK.glp_set_row_name(lp, 2, "c2");
            GLPK.glp_set_row_bnds(lp, 2, GLPKConstants.GLP_UP, 0, 0.4);
            GLPK.intArray_setitem(ind, 1, 2);
            GLPK.intArray_setitem(ind, 2, 3);
            GLPK.doubleArray_setitem(val, 1, -1.);
            GLPK.doubleArray_setitem(val, 2, 1.);
            GLPK.glp_set_mat_row(lp, 2, 2, ind, val);

            // Free memory
            GLPK.delete_intArray(ind);
            GLPK.delete_doubleArray(val);

            // Define objective
            GLPK.glp_set_obj_name(lp, "z");
            GLPK.glp_set_obj_dir(lp, GLPKConstants.GLP_MIN);
            GLPK.glp_set_obj_coef(lp, 0, 1.);
            GLPK.glp_set_obj_coef(lp, 1, -.5);
            GLPK.glp_set_obj_coef(lp, 2, .5);
            GLPK.glp_set_obj_coef(lp, 3, -1);
            
            if(writeProblemAtFinish) {
	            // Write model to file
            	if(useLp) {
            		GLPK.glp_write_lp(lp, null, fname);
            	}
            	else {
            		GLPK.glp_write_prob(lp, 0, fname);
            	}
            }

            // Solve model
            parm = new glp_smcp();
            GLPK.glp_init_smcp(parm);
            ret = GLPK.glp_simplex(lp, parm);

            AtomicBoolean success = new AtomicBoolean(true);
            // Retrieve solution
            if (ret == 0) {
                write_lp_solution(lp, success);
            } else {
                log.debug("The problem could not be solved");
                logVarAndVal("retcode", ret);
                success.set(false);
            }
            if(!success.get()) {
            	log.warn("Sample Problem could not be solved successfully.");
            }

            // Free memory
            GLPK.glp_delete_prob(lp);
        } catch (GlpkException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * write simplex solution
     * @param lp problem
     * @param success 
     */
    protected static void write_lp_solution(glp_prob lp, AtomicBoolean success) {
        int i;
        int n;
        String name;
        double val;

        name = GLPK.glp_get_obj_name(lp);
        val = GLPK.glp_get_obj_val(lp);
        logVarAndVal(name, val);
        n = GLPK.glp_get_num_cols(lp);
        for (i = 1; i <= n; i++) {
            name = GLPK.glp_get_col_name(lp, i);
            val = GLPK.glp_get_col_prim(lp, i);
            switch(name) {
            case "z":  assertEquals(name, success, val, 0.425);break;
            case "x1": assertEquals(name, success, val, 0.25);break;
            case "x2": assertEquals(name, success, val, 0.1);break;
            case "x3": assertEquals(name, success, val, 0.5);break;
            }
            logVarAndVal(name, val);
        }
    }
    
    public static double delta = 0.0001;
    private static void assertEquals(String name, AtomicBoolean success, double actual, double expected) {
		if(Math.abs(expected - actual) > delta) {
			log.warn(String.format("Wrong value for %s: expected %f, actual %f, diff=%f",
					name, expected, actual, expected - actual));
			success.set(false);
		}
	}

	private static void logVarAndVal(String name, double val) {
    	log.debug(String.format("%s = %f", name, val));
    }
}
