/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.ilp;

import java.io.File;

import org.apache.log4j.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator fixing the java library path and trying the solve a {@link SampleProblem}.
 * @author René Schöne
 */
public class Activator implements BundleActivator {

	private static final String JAVA_LIBRARY_PATH = "java.library.path";
	public static final String KEY_GLPK_JAVA_LIB = "org.haec.optimizer.lib.glpk_java_lib";
	public static final String KEY_GLPK_JAVA_JAR = "org.haec.optimizer.lib.glpk_java_jar";
	private static Logger log = Logger.getLogger(Activator.class);
	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
//		System.out.println(new File(fileName).getAbsolutePath());
		String jlp = System.getProperty(JAVA_LIBRARY_PATH);
		String glpk_java_lib = System.getProperty(KEY_GLPK_JAVA_LIB);
		String glpk_java_jar = System.getProperty(KEY_GLPK_JAVA_JAR);
		if(!jlp.endsWith(File.pathSeparator)) {
			jlp += File.pathSeparator;
		}
		if(glpk_java_lib != null) {
			jlp += glpk_java_lib + File.pathSeparator;
		}
		if(glpk_java_jar != null) {
			jlp += glpk_java_jar + File.pathSeparator;
		}
		System.setProperty(JAVA_LIBRARY_PATH, jlp);
		log.debug("jlp in activator: " +System.getProperty(JAVA_LIBRARY_PATH));
		
		solveSampleProblem();
	}

	private void solveSampleProblem() {
		new SampleProblem().run();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}
