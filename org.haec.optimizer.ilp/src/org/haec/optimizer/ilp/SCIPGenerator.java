/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.ilp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.coolsoftware.coolcomponents.ccm.behavior.BehaviorFactory;
import org.coolsoftware.coolcomponents.ccm.behavior.CostParameter;
import org.coolsoftware.coolcomponents.ccm.behavior.Occurrence;
import org.coolsoftware.coolcomponents.ccm.behavior.Pin;
import org.coolsoftware.coolcomponents.ccm.behavior.State;
import org.coolsoftware.coolcomponents.ccm.behavior.StateMachine;
import org.coolsoftware.coolcomponents.ccm.behavior.Workload;
import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.coolcomponents.ccm.structure.ResourceType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.coolcomponents.ccm.variant.Behavior;
import org.coolsoftware.coolcomponents.ccm.variant.CostParameterBinding;
import org.coolsoftware.coolcomponents.ccm.variant.Resource;
import org.coolsoftware.coolcomponents.ccm.variant.VariantFactory;
import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;
import org.coolsoftware.coolcomponents.ccm.variant.VariantPropertyBinding;
import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.expressions.Statement;
import org.coolsoftware.coolcomponents.expressions.interpreter.CcmExpressionInterpreter;
import org.coolsoftware.coolcomponents.expressions.literals.LiteralsFactory;
import org.coolsoftware.coolcomponents.expressions.literals.RealLiteralExpression;
import org.coolsoftware.coolcomponents.expressions.operators.FunctionExpression;
import org.coolsoftware.coolcomponents.expressions.variables.Variable;
import org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment;
import org.coolsoftware.coolcomponents.expressions.variables.VariablesFactory;
import org.coolsoftware.coolcomponents.types.stdlib.CcmInteger;
import org.coolsoftware.coolcomponents.types.stdlib.CcmReal;
import org.coolsoftware.coolcomponents.types.stdlib.CcmString;
import org.coolsoftware.coolcomponents.types.stdlib.CcmValue;
import org.coolsoftware.ecl.EclContract;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.ecl.FormulaTemplate;
import org.coolsoftware.ecl.HWComponentRequirementClause;
import org.coolsoftware.ecl.PropertyRequirementClause;
import org.coolsoftware.ecl.ProvisionClause;
import org.coolsoftware.ecl.SWComponentContract;
import org.coolsoftware.ecl.SWComponentRequirementClause;
import org.coolsoftware.ecl.SWContractClause;
import org.coolsoftware.ecl.SWContractMode;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
//import org.coolsoftware.simulation.workload.resource.workload.Simulator;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.Optimizer;
import org.coolsoftware.theatre.util.CCMUtil;
import org.coolsoftware.theatre.util.GeneratorUtil;

/**
 * The planner component responsible for computing a reconfiguration, i.e. a mapping of implementations on containers.
 * This planner uses the scip implementation to solve the ILP problem. 
 * @author Sebastian Götz
 * @see <a href="http://scip.zib.de/">http://scip.zib.de/</a>
 */
public class SCIPGenerator implements Optimizer {

	private static Set<String> binaryVars = new HashSet<String>();
	private static Map<String, String> resourceShortCuts = new HashMap<String, String>();
	private static Map<String, Set<HWComponentRequirementClause>> resourceReqs = new HashMap<String, Set<HWComponentRequirementClause>>();
	private static Map<String, Map<String, CcmValue>> providedProperties = new HashMap<String, Map<String, CcmValue>>();
	private static Map<String, Map<String, CcmValue>> requiredProperties = new HashMap<String, Map<String, CcmValue>>();
	private static StructuralModel hwTypes;

	private static Logger log = Logger.getLogger("ILPGenerator");
	static {
		Handler h = new ConsoleHandler();
		h.setLevel(Level.OFF); // set to ALL if needed

		log.addHandler(h);
	}

	public static Mapping runILP(Request req,
			Map<String, EclFile> eclFiles, String ilp) {
		File reqFile = new File("tmp");
		return runILP(req, eclFiles, reqFile, ilp);
	}

	public static Mapping runILP(Request req,
			Map<String, EclFile> eclFiles, File reqFile, String ilp) {

		EclFile ecl = eclFiles.values().iterator().next();

		Mapping ret = new Mapping();
		File lpFile = new File(reqFile.getAbsolutePath() + ".lp");
		System.out.println("lpFile: " + lpFile.getAbsolutePath());
		try {
			if (!lpFile.exists())
				if (!lpFile.createNewFile())
					System.out.println("cannot create lp file");

			FileWriter fw = new FileWriter(lpFile, false);
			fw.write(ilp);
			fw.close();
			// persist contract for debugging
			File cinst = new File(reqFile.getAbsolutePath() + "-instance.ecl");
			ecl.eResource().save(new FileOutputStream(cinst),
					new HashMap<Object, Object>());
			fw.close();

			System.out.println("written ilp into file: "
					+ lpFile.getAbsolutePath() + "/" + lpFile.getName());
			System.out.println("written contract instance into file: "
					+ cinst.getAbsolutePath() + "/" + cinst.getName());

			long start = System.nanoTime();

			String cmd = "/home/cosh/dev/scip-3.0.2/bin/scip -q -f "+lpFile.getAbsolutePath()+" -l result.txt";
			System.out.println("exec: "+cmd);
			Process p = Runtime.getRuntime().exec(cmd);
			
			long duration = System.nanoTime() - start;

			System.out.println("\n====took: " + duration / 1000 / 1000
					+ " ms====");
			
			BufferedReader br = new BufferedReader(new FileReader(new File("./result.txt")));
			String line = "";
			while((line = br.readLine()) != null) {
				System.out.println(line);
			}

			Set<String> optimalQualityPath = new HashSet<String>();
			// System.out.println("Value of objective function: " +
			// solver.getObjective());

			// to free memory
			// solver.deleteLp();

			String msg = "Optimal Mapping:\n\n";
			for (String str : optimalQualityPath) {
				String[] parts = str.split("#");
				String impl = parts[1];
				String mode = parts[2];
				String server = parts[3];
				for (String x : resourceShortCuts.keySet()) {
					if (resourceShortCuts.get(x).equals(server)) {
						server = x;
						break;
					}
				}
				msg += impl + "(" + mode + ") => " + server + "\n";
				ret.put(impl.replaceAll("_", "."), server);
			}

			System.out.println(msg);

		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return ret;
	}

	public static String generateILPforRequest(Request req,
			Map<String, EclFile> eclFiles, boolean verbose,
			IGlobalEnergyManager gem, String appName) {
		long start = System.nanoTime();
		binaryVars = new HashSet<String>();
		resourceReqs = new HashMap<String, Set<HWComponentRequirementClause>>();
		providedProperties = new HashMap<String, Map<String, CcmValue>>();
		requiredProperties = new HashMap<String, Map<String, CcmValue>>();

		Set<String> usageVars = new HashSet<String>();

		String ilp = "";

		hwTypes = req.getImport().getModel();

		Platform platform = req.getHardware();
		VariantModel vm = platform.getHwmodel();
		Resource root = (Resource) vm.getRoot();
		Map<String, Resource> containers = new HashMap<String, Resource>();
		// get all servers
		int rcount = 1;
		for (Resource r : root.getSubresources()) {
			containers.put(r.getName(), r);
			resourceShortCuts.put(r.getName(), "R" + rcount++);
		}

		log.setLevel(Level.FINEST);

		log.info("starting to generate binary vars");
		ilp += generateDecisionVarsAndCache(eclFiles, containers, req, gem,
				appName, null, 0);
		ilp = eliminateDuplicates(ilp);
		log.info("binary vars generated");
		ilp += "\n";
		// now go for resource constraints
		// for each subresource of containers introduce rules of the following
		// kind
		// usage#container#resource#property <= resource-capacity && >= 0
		log.info("Resource usage constraint generation started...");
		for (Resource r : root.getSubresources()) {
			log.info("processing resource " + r.getName());
			String rname = GeneratorUtil.cleanString(r.getName());
			for (Resource sr : r.getSubresources()) {
				log.info("\tprocessing " + sr.getName());
				String srname = GeneratorUtil.cleanString(sr.getName());
				String tname = sr.getSpecification().getName();
				for (VariantPropertyBinding vpb : sr.getPropertyBinding()) {
					String pname = GeneratorUtil.cleanString(vpb.getProperty()
							.getDeclaredVariable().getName());
					CcmExpressionInterpreter interpreter = new CcmExpressionInterpreter();
					CcmValue val = interpreter.interpret(vpb
							.getValueExpression());

					if (val instanceof CcmString)
						continue;

					String usageVar = "usage#" + resourceShortCuts.get(rname)
							+ "#" + srname + "#" + pname;
					usageVars.add(usageVar);
					ilp += usageVar + " < " + GeneratorUtil.formatCcmValue(val)
							+ "\n";
					ilp += usageVar + " > 0\n";
					// add resource reqs per impl if they exist
					int numElems = 0;
					String oldILP = ilp;
					ilp += usageVar + " - ";
					for (String mode : resourceReqs.keySet()) {
						List<String> processedProperties = new ArrayList<String>();
						Set<HWComponentRequirementClause> hrcs = resourceReqs
								.get(mode);
						for (HWComponentRequirementClause hrc : hrcs) {
							for (PropertyRequirementClause prc : hrc
									.getRequiredProperties()) {
								// need to identify those relevant for current
								// usage var
								// will have the form:
								// usage#container#resource#property =
								// val*binaryvar
								// the binaryvar contains the mode, the val is
								// expressed as res req in this mode
								if (prc.getRequiredProperty()
										.getDeclaredVariable() != null
										&& hrc.getRequiredResourceType()
												.getName() != null)
									if (hrc.getRequiredResourceType().getName()
											.equals(tname)
											&& prc.getRequiredProperty()
													.getDeclaredVariable()
													.getName().equals(pname)) {
										if (processedProperties.contains(prc
												.getRequiredProperty()
												.getDeclaredVariable()
												.getName()))
											continue;
										ilp += GeneratorUtil
												.formatCcmValue(interpreter
														.interpret(prc
																.getMinValue()));
										ilp += " b#" + mode + " - ";
										numElems++;
										processedProperties.add(prc
												.getRequiredProperty()
												.getDeclaredVariable()
												.getName());
									}
							}
						}
					}
					if (numElems == 0)
						ilp = oldILP;
					else
						ilp = ilp.substring(0, ilp.length() - 3) + " = 0\n";

				}
			}
		}

		log.info("Provided property constraint generation started...");
		// provided properties constraints
		for (String propName : providedProperties.keySet()) {
			String strProv = propName + " - ";
			Map<String, CcmValue> provProp = providedProperties.get(propName);
			for (String binaryVar : provProp.keySet()) {
				CcmValue val = provProp.get(binaryVar);
				strProv += GeneratorUtil.formatCcmValue(val) + " " + binaryVar
						+ " - ";
			}
			strProv = strProv.substring(0, strProv.length() - 3);
			ilp += strProv + " = 0\n";
		}

		log.info("Required property constraint generation started...");
		// required properties constraints
		for (String propName : requiredProperties.keySet()) {
			String strReq = propName + " - ";
			Map<String, CcmValue> reqProp = requiredProperties.get(propName);
			for (String binaryVar : reqProp.keySet()) {
				CcmValue val = reqProp.get(binaryVar);
				strReq += GeneratorUtil.formatCcmValue(val) + " " + binaryVar
						+ " - ";
			}
			strReq = strReq.substring(0, strReq.length() - 3);
			ilp += strReq + " > 0\n";
		}

		// request requirements
		if (req.getReqs() != null) {
			for (PropertyRequirementClause prc : req.getReqs()) {
				if (prc.getMinValue() != null)
					ilp += "\n"
							+ req.getComponent().getName()
							+ "#"
							+ prc.getRequiredProperty().getDeclaredVariable()
									.getName()
							+ " > "
							+ GeneratorUtil
									.formatCcmValue(new CcmExpressionInterpreter()
											.interpret(prc.getMinValue()))
							+ "\n";
				else
					ilp += "\n"
							+ req.getComponent().getName()
							+ "#"
							+ prc.getRequiredProperty().getDeclaredVariable()
									.getName()
							+ " < "
							+ GeneratorUtil
									.formatCcmValue(new CcmExpressionInterpreter()
											.interpret(prc.getMaxValue()))
							+ "\n";

			}
		}

		ilp += "\nBinaries ";
		for (String bvn : binaryVars) {
			ilp += bvn + " ";
		}

		ilp = ilp.substring(0, ilp.length() - 1);

		String objective = generateMinUsageObjectiveFunction(usageVars);

		ilp = objective + "Subject to\n" + ilp;

		if (verbose)
			System.out.println(ilp);

		log.info("ILP generation done");
		long duration = System.nanoTime() - start;
		log.info("duration: " + duration / 1000 / 1000 + " ms");
		log.info(binaryVars.size() + " binary vars");
		return ilp;
	}

	/**
	 * This method drops duplicate syntactic/selection constraints, which occur
	 * due to the double nature of
	 * {@link #generateDecisionVarsAndCache(Map, Map, Request, OsgiGem, String, Set)}
	 * (i.e., selection constraints + filling cache).
	 * 
	 * @param ilp
	 *            The generated ILP including duplicates
	 * @return The ILP without duplicates
	 */
	private static String eliminateDuplicates(String ilp) {
		String[] lines = ilp.split("\n");
		Set<String> noDuplicates = new HashSet<String>();
		for (String line : lines) {
			noDuplicates.add(line);
		}
		String ret = "";
		for (String line : noDuplicates) {
			ret += line + "\n";
		}
		return ret;
	}

	/**
	 * @param usageVars
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String generateMinUsageObjectiveFunction(
			Set<String> usageVars) {
		String objective = "Minimize\nObj: ";
		for (String uv : usageVars) {
			objective += uv + " + ";
		}
		objective = objective.substring(0, objective.length() - 3);
		objective += "\n\n";
		return objective;
	}

	/**
	 * @param usageVars
	 * @return
	 */
	private static String generateMinEnergyObjectiveFunction(
			@SuppressWarnings("unused") Set<String> usageVars, Request req) {
		String objective = "Minimize\nObj: ";
		for (String bv : binaryVars) {
			objective += getWeightForMappingVar(bv,
					new CcmExpressionInterpreter(), req) + " " + bv + " + ";
		}
		objective = objective.substring(0, objective.length() - 3);
		objective += "\n\n";
		return objective;
	}

	/**
	 * Computes the weight of a mapping variable (b#VLC#highQ#Server1) by
	 * interpreting its resource requirements. Currently, the sum of all NFRs is
	 * used. Later, energy should be used to normalize the values.
	 * 
	 * @param bv
	 *            name of the mapping variable
	 * @return int - weight of variable in terms of resource requirements
	 */
	private static double getWeightForMappingVar(String bv,
			CcmExpressionInterpreter interpreter, Request req) {
		// read from resourceReqs (bv without b# and #container)
		double weight = 0;

		String reqsByImplMode = bv.substring(2);
		Set<HWComponentRequirementClause> reqs = resourceReqs
				.get(reqsByImplMode);
		if (reqs == null)
			return weight;
		for (HWComponentRequirementClause hrc : reqs) {
			double energyRate = hrc.getEnergyRate();
			ResourceType resType = hrc.getRequiredResourceType();
			for (PropertyRequirementClause prc : hrc.getRequiredProperties()) {
				if (prc.getMinValue() == null && prc.getMaxValue() == null
						&& prc.getFormula() != null) {
					weight = 0;
					System.out.println("The Formula "
							+ prc.getFormula().getName() + " for " + bv
							+ " has not been evaluated..");
					return weight;
				}
				if (prc.getMinValue() != null) {
					if (prc.getMinValue() instanceof FunctionExpression) {
						Expression e = ((VariableAssignment) ((FunctionExpression) prc
								.getMinValue()).getSourceExp())
								.getValueExpression();
						weight = ((CcmReal) interpreter.interpret(e))
								.getRealValue();
					} else {
						weight = ((CcmReal) interpreter.interpret(prc
								.getMinValue())).getRealValue();
					}
				} else {
					if (prc.getMaxValue() instanceof FunctionExpression) {
						Expression e = ((VariableAssignment) ((FunctionExpression) prc
								.getMaxValue()).getSourceExp())
								.getValueExpression();
						weight = ((CcmReal) interpreter.interpret(e))
								.getRealValue();
					} else {
						weight = ((CcmReal) interpreter.interpret(prc
								.getMaxValue())).getRealValue();
					}
				}
				// the time is to be multiplied with the power implied by the
				// hardware setup specified in the quality mode
				// -> simulate the resource in that mode using the computed
				// workload (e.g., cpu_time)
				// TODO overcome assumption: only one resource of a type per
				// server
				String sName = bv.substring(bv.lastIndexOf("#") + 1);
				for (String longName : resourceShortCuts.keySet())
					if (resourceShortCuts.get(longName).equals(sName))
						sName = longName;
				// now access variant model of infrastructure...
				Resource root = (Resource) req.getHardware().getHwmodel()
						.getRoot();
				for (Resource server : root.getSubresources()) {
					if (server.getName().equals(sName)) {
						for (Resource res : server.getSubresources()) {
							if (res.getSpecification().getName()
									.equals(resType.getName())) {
								Behavior behavior = res.getBehavior();
								if (behavior != null) {
									if (behavior.getTemplate() instanceof StateMachine) {
										StateMachine sm = (StateMachine) behavior
												.getTemplate();
										// now set the mode for the resource and
										// evaluate the behavior using the
										// workload from above
										Workload w = BehaviorFactory.eINSTANCE
												.createWorkload();
										Occurrence o = BehaviorFactory.eINSTANCE
												.createOccurrence();
										o.setTime(0);
										o.setLoad((int) weight);
										// TODO get correct pin (currently only
										// works if there's just one pin)
										Pin pin = sm.getPins().iterator()
												.next();
										o.setPin(pin);
										w.getItems().add(o);
										w.setResource(res);
//										Simulator sim = new Simulator();
										List<CostParameterBinding> bindings = new ArrayList<CostParameterBinding>();
										for (State state : sm.getStates()) {
											CostParameter cp = state
													.getParameter().iterator()
													.next(); // there should be
																// always only
																// one
											CostParameterBinding cpb = VariantFactory.eINSTANCE
													.createCostParameterBinding();
											cpb.setParameter(cp);
											Variable v = VariablesFactory.eINSTANCE
													.createVariable();
											v.setName(cp.getName());
											RealLiteralExpression rle = LiteralsFactory.eINSTANCE
													.createRealLiteralExpression();
											rle.setValue(energyRate);
											v.setInitialExpression(rle);
											cpb.setDeclaredVariable(v);
											bindings.add(cpb);
										}
//										sm = sim.simulate(sm, w, bindings);
										CcmReal energy = ((CcmReal) sm
												.getPower()
												.getDeclaredVariable()
												.getValue());
										if (energy != null) {
											weight = energy.getRealValue();
											System.out
													.println("simulated ("
															+ res.getName()
															+ " | "
															+ bv
															+ "): "
															+ weight
															+ " in "
															+ ((CcmInteger) sm
																	.getTimer()
																	.getDeclaredVariable()
																	.getValue())
																	.getIntegerValue());
										}
										sm.reset();
									}
								} else {
									System.out
											.println("Behavior of "
													+ res.getName()
													+ " should be set to compute the induced energy consumption.");
								}
							}
						}
					}
				}
			}
		}

		return weight;
	}

	private static String generateDecisionVarsAndCache(
			Map<String, EclFile> eclFiles, Map<String, Resource> containers,
			Request req, IGlobalEnergyManager gem, String appName,
			Set<String> depBvn, int depth) {

		// deptype,reftype,container,contract
		Map<String, Map<String, Map<String, EclFile>>> deps = new HashMap<String, Map<String, Map<String, EclFile>>>();

		String ret = "";
		for (String sName : containers.keySet()) {
			EclFile ecl = eclFiles.get(sName);
			log.info("(" + depth + ") generate vars for " + ecl.eResource()
					+ " on " + sName);

			for (EclContract ctr : ecl.getContracts()) {
				if (ctr instanceof SWComponentContract) {
					SWComponentContract swc = (SWComponentContract) ctr;
					// set metaparameter values from request
					setMetaparameters(req, ctr);
					String implName = GeneratorUtil.cleanString(swc.getName());
					for (SWContractMode swm : swc.getModes()) {

						String modeName = GeneratorUtil.cleanString(swm
								.getName());
						// generate binary vars (impl/mode mapped to server =
						// true/false)

						String bvn = "b#" + implName + "#" + modeName + "#"
								+ resourceShortCuts.get(sName);
						ret += bvn + " + ";
						binaryVars.add(bvn);

						// provided property per mode
						for (SWContractClause cl : swm.getClauses()) {
							if (cl instanceof ProvisionClause) {
								handleProvisions(req, bvn, cl,
										swc.getComponentType());
							} else if (cl instanceof SWComponentRequirementClause) {
								handleSWRequirements(req, deps, bvn, cl, gem,
										appName);
							} else if (cl instanceof HWComponentRequirementClause) {
								handleHWRequirements(req, implName, modeName,
										cl, resourceShortCuts.get(sName));
							}
						}
					}
				}
			}
		}

		ret = ret.substring(0, ret.length() - 3);
		if (depBvn != null && depBvn.size() > 0) {
			ret += " - ";
			for (String db : depBvn) {
				ret += db + " - ";
			}
			ret = ret.substring(0, ret.length() - 3);
			ret += "= 0\n";
		} else {
			ret += "= 1\n";
		}

		// recursively follow dependencies
		int newDepth = ++depth;
		for (String newDepBvn : deps.keySet()) {
			Map<String, Map<String, EclFile>> depFilesPerContainerByDependentType = deps
					.get(newDepBvn);
			if (depFilesPerContainerByDependentType == null) {
				throw new RuntimeException(
						"Unresolved ECL contract dependency.");
			} else {
				for (String refType : depFilesPerContainerByDependentType
						.keySet()) {
					Map<String, EclFile> depFilesPerContainer = depFilesPerContainerByDependentType
							.get(refType);
					String depRet = generateDecisionVarsAndCache(
							depFilesPerContainer, containers, req, gem,
							appName, deps.keySet(), newDepth);
					ret += depRet;
					depFilesPerContainer = null;
				}
			}
		}
		deps = null;
		return ret;
	}

	/**
	 * @param req
	 * @param implName
	 * @param modeName
	 * @param cl
	 */
	private static void handleHWRequirements(Request req, String implName,
			String modeName, SWContractClause cl, String server) {
		HWComponentRequirementClause hrc = (HWComponentRequirementClause) cl;
		Set<HWComponentRequirementClause> reqs = resourceReqs.get(implName
				+ "#" + modeName + "#" + server);
		if (reqs == null) {
			reqs = new HashSet<HWComponentRequirementClause>();
		}
		reqs.add(hrc);
		resourceReqs.put(implName + "#" + modeName + "#" + server, reqs);

		for (PropertyRequirementClause prc : ((HWComponentRequirementClause) cl)
				.getRequiredProperties()) {
			CcmReal val;
			boolean min = true;
			Statement minmaxval = prc.getMinValue();
			if (minmaxval == null) {
				minmaxval = prc.getMaxValue();
				min = false;
			}
			if (minmaxval instanceof FunctionExpression) {
				val = (CcmReal) GeneratorUtil.evalFormula(minmaxval,
						req.getMetaParamValues());
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE
						.createRealLiteralExpression();
				rle.setValue(val.getRealValue());
				if (min)
					prc.setMinValue(rle);
				else
					prc.setMaxValue(rle);
			}
		}
	}

	/*
	 * public static RemoteOSGiService getRemoteOSGiService() { BundleContext
	 * context = FrameworkUtil.getBundle(ILPGenerator.class)
	 * .getBundleContext(); ServiceReference remoteRef = context
	 * .getServiceReference(RemoteOSGiService.class.getName()); if (remoteRef ==
	 * null) { System.err.println("Error: R-OSGi not found!"); }
	 * RemoteOSGiService remote = (RemoteOSGiService) context
	 * .getService(remoteRef); return remote; }
	 */
	/**
	 * @param req
	 * @param deps
	 * @param bvn
	 * @param cl
	 */
	private static void handleSWRequirements(Request req,
			Map<String, Map<String, Map<String, EclFile>>> deps, String bvn,
			SWContractClause cl, IGlobalEnergyManager gem, String appName) {
		SWComponentType t = ((SWComponentRequirementClause) cl)
				.getRequiredComponentType();
		// load EclFiles of dependent component type per container
		try {
			Map<String, String> serializedContractByContainer = gem
					.getContractsForComponent(appName, t.getName());
			Map<String, EclFile> referrencedContractByContainer = new HashMap<String, EclFile>();
			StructuralModel appModel = CCMUtil.deserializeSWStructModel(
					appName, gem.getApplicationModel(appName), false);
			for (String container : serializedContractByContainer.keySet()) {
				String serialContract = serializedContractByContainer
						.get(container);
				EclFile f = CCMUtil.deserializeECLContract(appModel, hwTypes,
						appName, t.getName(), serialContract, false);
				referrencedContractByContainer.put(container, f);
			}
			Map<String, Map<String, EclFile>> dep = deps.get(bvn);
			if (dep == null) {
				dep = new HashMap<String, Map<String, EclFile>>();
				deps.put(bvn, dep);
			}
			dep.put(t.getName(), referrencedContractByContainer);
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}

		for (PropertyRequirementClause prc : ((SWComponentRequirementClause) cl)
				.getRequiredProperties()) {
			boolean min = true;
			Statement minmaxval = prc.getMinValue();
			if (minmaxval == null) {
				minmaxval = prc.getMaxValue();
				min = false;
			}
			CcmReal val;
			if (minmaxval instanceof FunctionExpression) {
				val = (CcmReal) GeneratorUtil.evalFormula(minmaxval,
						req.getMetaParamValues());
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE
						.createRealLiteralExpression();
				rle.setValue(val.getRealValue());
				if (min)
					prc.setMinValue(rle);
				else
					prc.setMaxValue(rle);
			} else if (prc.getFormula() instanceof FormulaTemplate) {
				RealLiteralExpression rle = LiteralsFactory.eINSTANCE
						.createRealLiteralExpression();
				rle.setValue(0);
				val = (CcmReal) new CcmExpressionInterpreter().interpret(rle);
			} else {
				val = (CcmReal) new CcmExpressionInterpreter()
						.interpret(minmaxval);
			}
			if (min)
				val.setAscending(false);
			else
				val.setAscending(true);
			String propName = ((SWComponentRequirementClause) cl)
					.getRequiredComponentType().getName()
					+ "#"
					+ prc.getRequiredProperty().getDeclaredVariable().getName();
			Map<String, CcmValue> reqVal = requiredProperties.get(propName);
			if (reqVal == null)
				reqVal = new HashMap<String, CcmValue>();
			reqVal.put(bvn, val);
			requiredProperties.put(propName, reqVal);
		}
	}

	/**
	 * @param req
	 * @param bvn
	 * @param cl
	 */
	private static void handleProvisions(Request req, String bvn,
			SWContractClause cl, SWComponentType t) {
		CcmReal val = null;
		boolean min = true;
		Statement minmaxval = ((ProvisionClause) cl).getMinValue();
		if (minmaxval == null) {
			minmaxval = ((ProvisionClause) cl).getMaxValue();
			min = false;
		}
		if (minmaxval instanceof FunctionExpression) {
			val = (CcmReal) GeneratorUtil.evalFormula(minmaxval,
					req.getMetaParamValues());
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE
					.createRealLiteralExpression();
			rle.setValue(val.getRealValue());
			if (min)
				((ProvisionClause) cl).setMinValue(rle);
			else
				((ProvisionClause) cl).setMaxValue(rle);
		} else if (((ProvisionClause) cl).getFormula() instanceof FormulaTemplate) {
			RealLiteralExpression rle = LiteralsFactory.eINSTANCE
					.createRealLiteralExpression();
			rle.setValue(0);
			val = (CcmReal) new CcmExpressionInterpreter().interpret(rle);
		} else {
			val = (CcmReal) new CcmExpressionInterpreter().interpret(minmaxval);
		}
		if (min)
			val.setAscending(true);
		else
			val.setAscending(false);
		String propName = t.getName()
				+ "#"
				+ ((ProvisionClause) cl).getProvidedProperty()
						.getDeclaredVariable().getName();
		Map<String, CcmValue> propVal = providedProperties.get(propName);
		if (propVal == null)
			propVal = new HashMap<String, CcmValue>();
		propVal.put(bvn, val);
		providedProperties.put(propName, propVal);
	}

	/**
	 * @param req
	 * @param ctr
	 */
	private static void setMetaparameters(Request req, EclContract ctr) {
		if (((SWComponentContract) ctr).getPort() != null)
			for (Parameter mp : ((SWComponentContract) ctr).getPort()
					.getMetaparameter()) {
				for (MetaParamValue mpv : req.getMetaParamValues()) {
					if (mpv.getMetaparam().getName().equals(mp.getName())) {
						RealLiteralExpression ile = LiteralsFactory.eINSTANCE
								.createRealLiteralExpression();
						ile.setValue(mpv.getValue());
						mp.setValue(new CcmExpressionInterpreter()
								.interpret(ile));
					}
				}
			}
	}

	@Override
	public Mapping optimize(Object[] args) {
		try {
			Request req = (Request) args[0];
			Map<String, EclFile> eclFiles = (Map<String, EclFile>) args[1];
			Boolean verbose = (Boolean) args[2];
			IGlobalEnergyManager gem = (IGlobalEnergyManager) args[3];
			String appName = (String) args[4];
			String ilp = generateILPforRequest(req, eclFiles, verbose, gem,
					appName);
			return runILP(req, eclFiles, ilp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.Optimizer#getId()
	 */
	@Override
	public String getId() {
		return "org.haec.optimizer.ilp";
	}
}
