#!/bin/bash
# $1 path to copy result jar to

res_dir=`pwd`
ilp_dir="`dirname \"$0\"`"
ilp_dir="`( cd \"$ilp_dir\" && pwd )`"
jar_file=$ilp_dir/org.haec.optimizer.ilp_1.0.0.`date +%Y%m%d%H%M`.jar

cd $ilp_dir
jar vcfm $jar_file META-INF/MANIFEST.MF `ls lib/*.jar` plugin.xml > /dev/null
cd bin
jar vuf $jar_file `find . -name "*.class" -or -name "*.properties"` > /dev/null

cd $res_dir
cp $jar_file $1