package org.haec.videoprovider;

public interface IVideoProviderFactory {

	public abstract VideoProvider getCurrentVideoProvider();

	public abstract VideoProvider getCurrentVideoProvider(String hostname);

}
