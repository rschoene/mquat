/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoprovider;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.haec.apps.util.VideoSettings;
import org.haec.theatre.utils.PlatformUtils;

/**
 * Factory providing the {@link #getCurrentVideoProvider() current} {@link VideoProvider}.
 * The provider to use can be specified with {@link TheatrePropertyKeys#VIDEO_PROVIDER}.
 * @author René Schöne
 */
public class VideoProviderFactory implements IVideoProviderFactory {
	
	public static final String DEFAULT_PROVIDER = NoneVideoProvider.NAME;
	
	private Logger log = Logger.getLogger(VideoProviderFactory.class);
	private final Map<String, VideoProvider> providers = new HashMap<>();
	private VideoProvider current;

	private VideoSettings settings;
	
	public VideoProviderFactory() {
		// empty
	}

	@Override
	public VideoProvider getCurrentVideoProvider(String hostname) {
		return current.setHostname(hostname);
	}

	@Override
	public VideoProvider getCurrentVideoProvider() {
		return getCurrentVideoProvider(PlatformUtils.getHostName());
	}
	
	protected void activate() {
		String toUse = settings.getVideoProviderName().get();
		VideoProvider current = providers.get(toUse);
		if(current == null) {
			log.warn("Could not find provider with name " + toUse + ". Using default ('" + DEFAULT_PROVIDER + "')");
			current = providers.get(DEFAULT_PROVIDER);
		} else {
			log.info("Using VideoProvider " + current.getName());
		}
	}
	
	protected void addVideoProvider(VideoProvider vp) {
		providers.put(vp.getName(), vp);
	}
	
	protected void setVideoSettings(VideoSettings vs) {
		this.settings = vs;
	}

	protected void unsetVideoSettings(VideoSettings vs) {
		this.settings = null;
	}


}
