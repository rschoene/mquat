/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoprovider;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.haec.apps.util.VideoSettings;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.videos.Video;

/**
 * Standard provider of videos searching a directory with sub-directories specifying the length of the videos
 * in this sub-directory. Further, filenames have to match the {@link Video#pattern} and hostname is ignored.
 * @author René Schöne
 */
public class ExampleVideoProvider implements VideoProvider {
	
	public static final String NAME = "example";
	private Map<String, int[]> availableLengths;
	protected Logger log = Logger.getLogger(VideoProvider.class);

	private Map<String, SortedMap<Integer, List<Video>>> videosByHostnameAndLength;
	protected VideoSettings settings;
	private String me;
	
	public ExampleVideoProvider() {
		me = PlatformUtils.getHostName();
	}
	
	protected ExampleVideoProvider lazyInit() {
		if(videosByHostnameAndLength == null) {
			availableLengths = new HashMap<>();
			videosByHostnameAndLength = new HashMap<>();
			initVideoByLength();
			updateAvailableLengths();
			if(log.isEnabledFor(Level.INFO)) {
				// calculate amount of videos
				Set<Entry<String, SortedMap<Integer, List<Video>>>> entrySet = videosByHostnameAndLength.entrySet();
				int totalVideos = 0;
				int totalHosts  = entrySet.size();
				for(Entry<String, SortedMap<Integer, List<Video>>> e : entrySet) {
					for(Integer length : e.getValue().keySet()) {
						totalVideos += e.getValue().get(length).size();
					}
				}
				log.info("Got " + totalVideos + " video(s) for " + totalHosts + " different host(s).");
			}
		}
		return this;
	}

	protected void initVideoByLength() {
		int[] available = new int[] {
				3, 4, 6, 10, //20, 30, 40, 50, 60, 70 //XXX rs: just for testing
		};
		SortedMap<Integer, List<Video>> lonelyMap = new TreeMap<>();
		videosByHostnameAndLength.put(me, lonelyMap);
		for(int length : available) {
			File dir = getDir(padLength(length, 2));
			if(dir == null) {
				lonelyMap.put(length, Collections.<Video>emptyList());
				continue;
			}
			File[] files = dir.listFiles();
			if(files != null) { for(File videoFile : files) {
				addVideo(new Video(videoFile, length), me);
			}}
		}
	}
	
	protected void addVideo(Video v, String hostname) {
		Map<Integer, List<Video>> videosByLength = CollectionsUtil.ensureTreeMap(videosByHostnameAndLength, hostname);
		CollectionsUtil.ensureArrayList(videosByLength, Integer.valueOf(v.length)).add(v);
	}
	
	protected void updateAvailableLengths() {
		for(Entry<String, SortedMap<Integer, List<Video>>> e : videosByHostnameAndLength.entrySet()) {
			List<Integer> availableLengthsList = new ArrayList<Integer>(e.getValue().keySet());
			Collections.sort(availableLengthsList);
			int[] availableLengthsForHost = new int[availableLengthsList.size()];
			for (int i = 0; i < availableLengthsForHost.length; i++) {
				availableLengthsForHost[i] = availableLengthsList.get(i);
			}
			availableLengths.put(e.getKey(), availableLengthsForHost);
		}
	}

	private File getDir(String paddedLength) {
		return new File(settings.getResourceDir().get(), paddedLength);
	}

	private String padLength(int number, int size) {
		return String.format("%0"+size+"d", number);
	}

	@Override
	public int[] getAvailableLengths() {
		return lazyInit().getAvailableLengths(me);
	}

	@Override
	public int[] getAvailableLengths(String hostname) {
		return lazyInit().availableLengths.get(hostname);
	}

	@Override
	public List<Video> getVideos(int length, String hostname) {
		return lazyInit().videosByHostnameAndLength.get(hostname).get(length);
	}
	
//	@Override
//	public List<Video[]> getTwoVideos(boolean onePerLength, String hostname) {
//		return lazyInit().getTwoVideos(onePerLength, false, true, true, hostname);
//	}
	
	@Override
	public List<Video[]> getTwoVideos(boolean oneForFirstLength, boolean oneForSecondLength,
			boolean longer, boolean higherResolution, String hostname) {
		lazyInit();
		List<Video[]> result = new ArrayList<Video[]>();
		boolean gotOneForI, gotOneForJ;
		int[] availableLengthsForHost = availableLengths.get(hostname);
		for (int i = 0; i < availableLengthsForHost.length; i++) {
			gotOneForI = false;
			int videoLength1 = availableLengthsForHost[i];
			for(Video video1 : getVideos(videoLength1, hostname)) {
				if(gotOneForI && oneForFirstLength)
					break;
				for (int j = 0; j < (longer ? i : availableLengthsForHost.length); j++) {
					if(gotOneForI && oneForFirstLength)
						break;
					gotOneForJ = false;
					int videoLength2 = availableLengthsForHost[j];
					for(Video video2 : getVideos(videoLength2, hostname)) {
						if(gotOneForJ && oneForSecondLength)
							break;
						if(!higherResolution || (video1.resX > video2.resX && video1.resY > video2.resY)) {
							// take the pair
							result.add(new Video[]{video1, video2});
							gotOneForJ = true;
							gotOneForI = true;
						}
					}// for(video2)
				}// for j (index of videolength2)
			}// for(video1)
		}// for i (index of videolength1)
		return result;
	}
	
	@Override
	public List<Video> getOneVideoOfEachLength(String hostname) {
		lazyInit();
		Collection<List<Video>> v = videosByHostnameAndLength.get(hostname).values();
		List<Video> result = new ArrayList<>();
		for(List<Video> perLength : v) {
			if(perLength != null && !perLength.isEmpty()) { result.add(perLength.get(0)); }
		}
		return result;
	}
	
	@Override
	public String getName() {
		return NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getTwoVideos(boolean, boolean, boolean, boolean)
	 */
	@Override
	public List<Video[]> getTwoVideos(boolean oneForFirstLength,
			boolean oneForSecondLength, boolean longer, boolean higherResolution) {
		return getTwoVideos(oneForFirstLength, oneForSecondLength, longer, higherResolution, me);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getVideos(int)
	 */
	@Override
	public List<Video> getVideos(int length) {
		return getVideos(length, me);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getOneVideoOfEachLength()
	 */
	@Override
	public List<Video> getOneVideoOfEachLength() {
		return getOneVideoOfEachLength(me); 
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#setHostname(java.lang.String)
	 */
	@Override
	public VideoProvider setHostname(String hostname) {
		return this;
	}

	protected void setVideoSettings(VideoSettings vs) {
		this.settings = vs;
	}

	protected void unsetVideoSettings(VideoSettings vs) {
		this.settings = null;
	}

}
