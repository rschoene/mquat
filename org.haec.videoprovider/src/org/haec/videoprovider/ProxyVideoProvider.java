/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoprovider;

import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.util.Reconnectable;
import org.haec.theatre.settings.TheatreSettings;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.haec.videos.Video;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;

import ch.ethz.iks.r_osgi.RemoteOSGiService;

/**
 * Video provider using another provider hosted at the master.
 * @author René Schöne
 */
public class ProxyVideoProvider implements VideoProvider, Reconnectable {
	
	public static final String NAME = "proxy";
	private static Logger log = Logger.getLogger(ProxyVideoProvider.class);
	private VideoProvider delegatee;
	private String hostname;
	private BundleContext bundleContext;
	private TheatreSettings settings;
	
	public ProxyVideoProvider() {
		reconnect();
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.util.Reconnectable#reconnect()
	 */
	@Override
	public void reconnect() {
		delegatee = getProxy();
		if(delegatee == null) {
			log.warn("Got null delegatee");
		}
		if(delegatee == this) {
			log.warn("Looks like a loop. Setting delegatee to null.");
			delegatee = null;
		}
	}

	/**
	 * @return the proxy
	 */
	private VideoProvider getProxy() {
		RemoteOSGiService remote = RemoteOsgiUtil.getRemoteOSGiService(bundleContext);
		return RemoteOsgiUtil.getRemoteService(remote, settings.getGemUri().get(), VideoProvider.class);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getTwoVideos(boolean, boolean, boolean, boolean)
	 */
	@Override
	public List<Video[]> getTwoVideos(boolean oneForFirstLength,
			boolean oneForSecondLength, boolean longer, boolean higherResolution) {
		return delegatee.getTwoVideos(oneForFirstLength, oneForSecondLength, longer, higherResolution, this.hostname);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getVideos(int)
	 */
	@Override
	public List<Video> getVideos(int length) {
		return delegatee.getVideos(length, this.hostname);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getAvailableLengths()
	 */
	@Override
	public int[] getAvailableLengths() {
		return getAvailableLengths();
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getName()
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getOneVideoOfEachLength()
	 */
	@Override
	public List<Video> getOneVideoOfEachLength() {
		return delegatee.getOneVideoOfEachLength(this.hostname);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#setHostname(java.lang.String)
	 */
	@Override
	public VideoProvider setHostname(String hostname) {
		this.hostname = hostname;
		return this;
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getTwoVideos(boolean, boolean, boolean, boolean, java.lang.String)
	 */
	@Override
	public List<Video[]> getTwoVideos(boolean oneForFirstLength,
			boolean oneForSecondLength, boolean longer,
			boolean higherResolution, String hostname) {
		return delegatee.getTwoVideos(oneForFirstLength, oneForSecondLength, longer, higherResolution, hostname);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getVideos(int, java.lang.String)
	 */
	@Override
	public List<Video> getVideos(int length, String hostname) {
		return delegatee.getVideos(length, hostname);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getOneVideoOfEachLength(java.lang.String)
	 */
	@Override
	public List<Video> getOneVideoOfEachLength(String hostname) {
		return delegatee.getOneVideoOfEachLength(hostname);
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getAvailableLengths(java.lang.String)
	 */
	@Override
	public int[] getAvailableLengths(String hostname) {
		return delegatee.getAvailableLengths(hostname);
	}
	
	protected void setSettings(TheatreSettings s) {
		this.settings = s;
	}
	
	protected void activate(ComponentContext ctx) {
		this.bundleContext = ctx.getBundleContext();
	}

}
