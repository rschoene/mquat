/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoprovider;

import java.util.List;

import org.haec.videos.Video;

/**
 * Provider of videos.
 * @author René Schöne
 */
public interface VideoProvider {

	/**
	 * Computes a list of pairs of videos. The first video will be longer, if the value
	 * of <b>longer</b> is <code>true</code> and of higher
	 * resolution, if the value of <b>higherResolution</b> is <code>true</code>,
	 * each compared to the second video. All possible combinations are used, except if
	 * <b>onePerLength</b> set to <code>true</code>. In this case only one video per length is
	 * used (the first video found) and combined with the other video. This applies to the 
	 * selection of both, first and second video.
	 * @param oneForFirstLength use only one second video
	 * @param oneForSecondLength use only one second video
	 * @param longer use only video pairs in which the first video is longer than the second
	 * @param higherResolution use only video pairs in which the first video is of
	 *  higher resolution than the second
	 * @return the list of pairs of videos
	 */
	public abstract List<Video[]> getTwoVideos(boolean oneForFirstLength, boolean oneForSecondLength, boolean longer,
			boolean higherResolution);

	public abstract List<Video[]> getTwoVideos(boolean oneForFirstLength, boolean oneForSecondLength, boolean longer,
			boolean higherResolution, String hostname);

//	/**
//	 * Computes a list of pairs of videos. The first video will always be longer and of higher
//	 * resolution than the second video. All possible combinations are used, except if
//	 * <b>onePerLength</b> set to <code>true</code>. In this case only one video per length is
//	 * used (the first video found) and combined with the other video. This applies to the 
//	 * selection of both, first and second video.
//	 * @param onePerLength use only one video per length given
//	 * @return the list of pairs of videos
//	 */
//	public abstract List<Video[]> getTwoVideos(boolean onePerLength);

	public abstract List<Video> getVideos(int length);

	public abstract List<Video> getVideos(int length, String hostname);

	public abstract int[] getAvailableLengths(String hostname);

	public abstract int[] getAvailableLengths();
	
	public abstract String getName();

//	public abstract Iterable<List<Video>> getVideos();

	public abstract List<Video> getOneVideoOfEachLength();

	public abstract List<Video> getOneVideoOfEachLength(String hostname);

	/** Sets the hostname to use and returns <code>this</code> for chaining. */
	public abstract VideoProvider setHostname(String hostname);

}
