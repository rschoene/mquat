/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.videoprovider;

import java.util.Collections;
import java.util.List;

import org.haec.videos.Video;

/**
 * Video provider returning always empty lists if asked.
 * @author René Schöne
 */
public class NoneVideoProvider implements VideoProvider {

	public static final String NAME = "none";
	private static final int[] availableLengths = new int[0];

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getTwoVideos(boolean, boolean, boolean, boolean)
	 */
	@Override
	public List<Video[]> getTwoVideos(boolean oneForFirstLength,
			boolean oneForSecondLength, boolean longer, boolean higherResolution, String hostname) {
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getVideos(int)
	 */
	@Override
	public List<Video> getVideos(int length, String hostname) {
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getAvailableLengths()
	 */
	@Override
	public int[] getAvailableLengths() {
		return availableLengths;
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getName()
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getOneVideoOfEachLength()
	 */
	@Override
	public List<Video> getOneVideoOfEachLength(String hostname) {
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getTwoVideos(boolean, boolean, boolean, boolean)
	 */
	@Override
	public List<Video[]> getTwoVideos(boolean oneForFirstLength,
			boolean oneForSecondLength, boolean longer, boolean higherResolution) {
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getVideos(int)
	 */
	@Override
	public List<Video> getVideos(int length) {
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getOneVideoOfEachLength()
	 */
	@Override
	public List<Video> getOneVideoOfEachLength() {
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#setHostname(java.lang.String)
	 */
	@Override
	public VideoProvider setHostname(String hostname) {
		return this;
	}

	/* (non-Javadoc)
	 * @see org.haec.videoprovider.VideoProvider#getAvailableLengths(java.lang.String)
	 */
	@Override
	public int[] getAvailableLengths(String hostname) {
		return availableLengths;
	}

}
