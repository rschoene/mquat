/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;
import org.coolsoftware.theatre.energymanager.IJobPreExecuteNotifier;
import org.coolsoftware.theatre.energymanager.ITaskResultNotifier;
import org.coolsoftware.theatre.energymanager.JobDescription;
import org.coolsoftware.theatre.energymanager.TaskResultDescription;
import org.eclipse.osgi.framework.console.CommandInterpreter;
import org.eclipse.osgi.framework.console.CommandProvider;
import org.haec.theatre.utils.PlatformUtils;
import org.haec.theatre.utils.sql.SQLUtils;

/**
 * CommandProvider providing the commands:
 * <ul>
 * <li>after: Call result notifier</li>
 * <li>before: Call preExecute notifier</li>
 * <li>silence: Disable log output of grm</li>
 * <li>dexter: Reconnect all connections to new DexterServer</li>
 * <li>host2ip: Get ip for hostname</li>
 * <li>ip2host: Get hostname for ip</li>
 * </ul>
 * @author René Schöne
 */
public class TheatreCmd implements CommandProvider {

	private static final boolean success_default = true;
	private static final long taskId_default = 1;
	private static final long jobId_default = 2;
	private static final double predictedFinishTime_default = 15;
	private static final long startTime_default = 3;
	
	protected List<IJobPreExecuteNotifier> jobPreExecutors = new ArrayList<>();
	protected List<ITaskResultNotifier> taskResultors = new ArrayList<>();

	@Override
	public String getHelp() {
		return "after \t\t Call result notifier. Syntax: after [success="+success_default+"] [filePath|hostname=null] [taskId="+taskId_default+"]\n"
			 + "before \t Call preExecute notifier with predefined parameters. Syntax: before\n"
			 + "silence \t Disable log output of grm.\n"
			 + "dexter \t Reconnect all connections to new DexterServer instance.\n"
			 + "host2ip \t Get ip for hostname. Syntax: host2ip <hostname>\n"
			 + "ip2host \t Get hostname for ip. Syntax: ip2host <ip>\n";
	}
	
	public void _aftertask(CommandInterpreter ci) {
		List<String> arguments = new ArrayList<String>();
		String arg;
		while((arg = ci.nextArgument()) != null) {
			arguments.add(arg);
		}
		callTaskResultNotifiers(arguments);
	}
	
	public void _before(CommandInterpreter ci) {
		JobDescription parameters = new JobDescription();
		parameters.containerName = "192.168.0.218";
		parameters.implName = "org.haec.app.scaling.handbrake.ScalingHandbrake";
		parameters.jobId = jobId_default;
		parameters.predicted = predictedFinishTime_default;
		parameters.startTime = startTime_default;
		parameters.taskId = taskId_default;
		callPreExecuteNotifiers(parameters);
	}
	
	public void _host2ip(CommandInterpreter ci) {
		String hostname = ci.nextArgument();
		System.out.println(PlatformUtils.getIpOf(hostname));
	}
	
	public void _ip2host(CommandInterpreter ci) {
		String ip = ci.nextArgument();
		System.out.println(PlatformUtils.getHostNameOf(ip));
	}
	
	public void _silence(CommandInterpreter ci) {
		Appender appender = Logger.getRootLogger().getAppender("stdout");
		if(appender == null) {
			appender = Logger.getRootLogger().getAppender("default");
		}
		if(appender == null || !(appender instanceof ConsoleAppender)) {
			System.err.println("Did not find adequate appender to silence. Exiting.");
			return;
		}
		Filter f = appender.getFilter();
		if(f == null) {
			System.out.println("Silencing GRM.");
			appender.addFilter(new Filter() {
				
				@Override
				public int decide(LoggingEvent event) {
					return event.getLocationInformation().getClassName().contains(
							"DexterGRM") ? Filter.DENY : Filter.ACCEPT;
				}
			});
		}
		else {
			System.out.println("Removing all filters.");
			appender.clearFilters();
		}
	}
	
	public void _dexter(CommandInterpreter ci) {
		boolean success = SQLUtils.recreateAllConnections();
		System.out.println((success ? "S" : "Uns") + "uccessful recreated connections.");
	}
	
	protected void callTaskResultNotifiers(List<String> arguments) {
		TaskResultDescription trd = new TaskResultDescription();
		for(String arg : arguments) {
			String[] parts = arg.split("=");
			String name = parts[0].trim();
			String value = parts[0].trim();
			switch(name) {
			case "success": trd.success = Boolean.parseBoolean(value); break;
			case "taskId": trd.taskId = Long.parseLong(value); break;
			case "filePath": trd.filePath = value; break;
			case "hostname": trd.hostname = value; break;
			default: System.out.println("Unknown property:" + name);
			}
		}
		for(ITaskResultNotifier notifier : taskResultors) {
			System.out.println("Calling " + notifier);
			notifier.afterTaskFinish(trd);
		}
	}
	
	protected void callPreExecuteNotifiers(JobDescription jd) {
		for(IJobPreExecuteNotifier notifier : jobPreExecutors) {
			System.out.println("Calling " + notifier);
			notifier.beforeJobExecute(jd);
		}
	}
	
	protected void addJobPreExecute(IJobPreExecuteNotifier jpen) {
		jobPreExecutors.add(jpen);
	}
	
	protected void addTaskResult(ITaskResultNotifier trn) {
		taskResultors.add(trn);
	}

}
