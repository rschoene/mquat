/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.commands;

import org.apache.log4j.Logger;
import org.coolsoftware.theatre.Resettable;
import org.coolsoftware.theatre.TheatreCommandProvider;
import org.haec.theatre.utils.AbstractTheatreCommandProvider;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;

/**
 * 
 * @author René Schöne
 */
public class ResetCommand extends AbstractTheatreCommandProvider {
	
	private Logger log = Logger.getLogger(ResetCommand.class);
	private BundleContext context;
	
	public ResetCommand() {
		super("reset");
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.TheatreCommandProvider#getDescription()
	 */
	@Override
	public String getDescription() {
		return "[Fully] Resets all resettable components.";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.utils.AbstractTheatreCommandProvider#execute0(java.lang.String[])
	 */
	@Override
	protected String execute0(String[] params) {
		ServiceReference<?>[] refs = null;
		try {
			refs = context.getServiceReferences(Resettable.class.getName(), null);
		} catch (InvalidSyntaxException e) {
			return "weird: " + e.getMessage();
		}
		boolean full = params.length > 0 && Boolean.parseBoolean(params[0]);
		StringBuilder sb = new StringBuilder();
		if(refs != null) { for(ServiceReference<?> ref : refs) {
			Resettable instance = (Resettable) context.getService(ref);
			if(instance != null) {
				log.debug("Resetting instance " + instance);
				sb.append(instance).append(":").append(instance.reset(full));
			}
			else { log.warn("Instance for " + ref + " is null."); }
		}}
		return sb.toString();
	}
	
	protected void activate(ComponentContext ctx) {
		this.context = ctx.getBundleContext();
		RemoteOsgiUtil.register(context, TheatreCommandProvider.class, this);
	}

}
