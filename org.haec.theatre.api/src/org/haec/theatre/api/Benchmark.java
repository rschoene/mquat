/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.api;

import java.util.List;

/**
 * The benchmark to be implemented by the client.
 * @param <T> the type to describe the input for one execution of the implementation
 * @author René Schöne
 */
public interface Benchmark {

	/**
	 * @return the data to iterate over
	 */
	public abstract List<BenchmarkData> getData();
	
	/**
	 * Executes the implementation for the given data. Most likely this data will have to be cast to
	 * the special type created using {@link #getData()}.
	 * @return the output of the iteration
	 */
	public abstract Object iteration(BenchmarkData data);

}
