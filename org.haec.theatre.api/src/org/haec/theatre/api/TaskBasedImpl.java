/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.api;

import java.io.File;
import java.net.URI;

import org.apache.log4j.Logger;
import org.haec.theatre.task.ITask;
import org.haec.theatre.task.repository.ITaskRepository;
import org.haec.theatre.utils.Cache;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.Function;
import org.haec.theatre.utils.RemoteOsgiUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

/**
 * 
 * @author René Schöne
 */
public abstract class TaskBasedImpl implements Impl {

	protected long taskId;
	private Cache<Long, ITask> taskCache;
	private Logger log = Logger.getLogger(TaskBasedImpl.class);
	protected final URI thisUri;
	
	public TaskBasedImpl() {
		thisUri = URI.create(RemoteOsgiUtil.getLemIp());
		Function<Long, ITask> getTaskFromRepo = new Function<Long, ITask>() {

			@Override
			public ITask apply(Long taskId) {
				BundleContext context = getContext();
				ServiceReference<ITaskRepository> ref = context.getServiceReference(ITaskRepository.class);
				if(ref == null) {
					log.warn("TaskRepository was not found. Returning null.");
					return null;
				}
				ITaskRepository repo = context.getService(ref);
				// fetch task in serialized form with this' id
				log.debug("TaskRepo fetched");
				ITask task = repo.getTask(taskId);
				return task;
			}
		};
		taskCache = new Cache<Long, ITask>(getTaskFromRepo, false);
	}
	
	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}
	
	/**
	 * @return the taskId
	 */
	public long getTaskId() {
		return taskId;
	}
	
	protected <S> S invoke(Class<S> clazz, String compName, String portName,
			Object... params) {
		ITask task = taskCache.get(getTaskId());
		log.debug("Task fetched, id=" + task.getTaskId());
		return task.execute(clazz, getApplicationName(), compName, portName, params);
	}

	protected BundleContext getContext() {
		return FrameworkUtil.getBundle(getClass()).getBundleContext();
	}
	
	protected FileProxy wrap(File f) {
		return new FileProxy(f, thisUri);
	}

}
