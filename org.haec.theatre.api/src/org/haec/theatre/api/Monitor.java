/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.api;

/**
 * The description of a monitor for certain properties. The protocol for a measurement is:
 * <ol>
 * <li> {@link #collectBefore()} </li>
 * <li> Run the iteration </li>
 * <li> {@link #collectAfter()} </li>
 * <li> {@link #compute()} </li>
 * </ol>
 * After these steps, the measured value must not change, and therefor, {@link #propertyAndValue()}
 * must return the same value.
 * @author Sebastian Götz
 */
public interface Monitor {
	
	public static final String PROPERTY_DELIMITER = ";";
	
	/**
	 * Called right before the iteration is started.
	 * Should not make any computations, i.e. just store needed information. 
	 */
	public void collectBefore();
	
	/**
	 * Called right after the iteration is finished.
	 * Should not make any computations, i.e. just store needed information.
	 * @param input the input of the iteration
	 * @param output the output of the iteration
	 */
	public void collectAfter(BenchmarkData input, Object output);
	
	/**
	 * Analyze the stored data, and compute the values for the measured properties.
	 */
	public void compute();
	
	/**
	 * @return the names of the properties which can be monitored
	 */
	public String[] getMeasuredPropertyNames();

	/**
	 * @return <code>true</code> if this can be used for monitoring and is correctly set up
	 */
	public boolean isAvailable();
	
	/**
	 * Provides both, property name and current value.
	 * @return propery {@value #PROPERTY_DELIMITER} value
	 */
	public String propertyAndValue();
	
}
