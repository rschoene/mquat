/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.api;

/**
 * Tagging interface for an application
 * @author René Schöne
 */
public interface App {
	
	/**
	 * @return the name of the app
	 */
	public String getName();
	
	/**
	 * @return the path to the model file
	 */
	public String getApplicationModelPath();
	
	/**
	 * @return the path to the contract directory (used together with the eclUris within the model)
	 */
	public String getContractsPath();

	/**
	 * @return the symbolic name of the app bundle
	 */
	public String getBundleSymbolicName();

}
