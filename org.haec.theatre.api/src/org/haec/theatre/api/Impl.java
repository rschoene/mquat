/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.theatre.api;

/**
 * Tagging interface for an implementation.
 * @author René Schöne
 */
public interface Impl {

	/**
	 * @return the name of the app
	 */
	public String getApplicationName();
	
	/**
	 * @return the name of the component type implemented
	 */
	public String getComponentType();
	
	/**
	 * @return the name of concrete variant
	 */
	public String getVariantName();
	
	/**
	 * @return the benchmark class
	 */
	public Benchmark getBenchmark();
	
	/**
	 * @return the name of the plugin containing the code
	 */
	public String getPluginName();

}
