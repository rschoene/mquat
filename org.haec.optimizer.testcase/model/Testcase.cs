SYNTAXDEF testcase
FOR <http://testcase/1.0>
START TestCases


TOKENS {
	DEFINE COMMENT $'//'(~('\n'|'\r'|'\uffff'))*$;
	DEFINE INTEGER $('-')?('1'..'9')('0'..'9')*|'0'$;
	DEFINE FLOAT $('-')?(('1'..'9') ('0'..'9')* | '0') '.' ('0'..'9')+ $;
//	DEFINE NAME $('a'..'z'|'.'|'-'|'_')+$;
}


TOKENSTYLES {
	"COMMENT" COLOR #008000, ITALIC;

	"TestCase","request","situation","expectedOutcome",":" COLOR #7F0055, BOLD;
	"compName","portName","reqPropName","reqPropValue","mpName","mpValue","container","impl","estimated","since","->" COLOR #7F0055;
}


RULES {
	TestCases ::= settings testcases+;
	Settings ::= "Settings" ":" ("pattern" pattern['"', '"'])? onlyFirst["first only" : ""];
	TestCase ::= "TestCase" name['"','"'] !1 request !1 situation !1 expectedOutcome;
	Request ::= "request" ":" ("variant" hardwareVariantmodelFile['"','"'])? ("compName" componentName['"','"'])? ("portName" portName['"','"'])? ("reqPropName" reqPropertyName['"','"'])?
	 ("reqPropValue" reqPropertyValue[FLOAT])? ("mpName" metaparamName['"','"'])? ("mpValue" metaparamValue[INTEGER])?
//	 ("profile" profile[EnergyEfficient : "energy", Utility : "utility", Normal : "normal"])?;
	 ("profile" profile['"', '"'])?;
	Situation ::= "situation" ":" schedules* #1 copyFilePatterns*;
	CopyFilePattern ::= "resource" ("contains" containsString['"','"'])? ("exact" exactString['"','"'])?;
	ExpectedOutcome ::= "expectedOutcome" ":" (mappings)*;
	ContainerSchedule ::= "container" #1 containerName['"','"'] ":" #1 "(" #1 (jobs)* #1 ")";
	JobDescription ::= implName['"','"'] #1 "estimated" #1 predictedRuntime[FLOAT] (#1 "since" #1 startTime[INTEGER])?;
	ImplDontCare ::= "impl" implName['"','"'] #1 "*";
	ImplToContainer ::= "impl" implName['"','"'] #1 "->" #1 containerName['"','"'] ("@" startAtMillis[INTEGER])?;
}