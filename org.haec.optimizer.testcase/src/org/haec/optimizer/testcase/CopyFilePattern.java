/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Copy File Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.CopyFilePattern#getContainsString <em>Contains String</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.CopyFilePattern#getExactString <em>Exact String</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.haec.optimizer.testcase.TestcasePackage#getCopyFilePattern()
 * @model
 * @generated
 */
public interface CopyFilePattern extends EObject {
	/**
	 * Returns the value of the '<em><b>Contains String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contains String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contains String</em>' attribute.
	 * @see #setContainsString(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getCopyFilePattern_ContainsString()
	 * @model
	 * @generated
	 */
	String getContainsString();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.CopyFilePattern#getContainsString <em>Contains String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contains String</em>' attribute.
	 * @see #getContainsString()
	 * @generated
	 */
	void setContainsString(String value);

	/**
	 * Returns the value of the '<em><b>Exact String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exact String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exact String</em>' attribute.
	 * @see #setExactString(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getCopyFilePattern_ExactString()
	 * @model
	 * @generated
	 */
	String getExactString();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.CopyFilePattern#getExactString <em>Exact String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exact String</em>' attribute.
	 * @see #getExactString()
	 * @generated
	 */
	void setExactString(String value);

} // CopyFilePattern
