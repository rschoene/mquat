/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container Schedule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.ContainerSchedule#getContainerName <em>Container Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.ContainerSchedule#getJobs <em>Jobs</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.haec.optimizer.testcase.TestcasePackage#getContainerSchedule()
 * @model
 * @generated
 */
public interface ContainerSchedule extends EObject {
	/**
	 * Returns the value of the '<em><b>Container Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Name</em>' attribute.
	 * @see #setContainerName(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getContainerSchedule_ContainerName()
	 * @model required="true"
	 * @generated
	 */
	String getContainerName();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.ContainerSchedule#getContainerName <em>Container Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Name</em>' attribute.
	 * @see #getContainerName()
	 * @generated
	 */
	void setContainerName(String value);

	/**
	 * Returns the value of the '<em><b>Jobs</b></em>' containment reference list.
	 * The list contents are of type {@link org.haec.optimizer.testcase.JobDescription}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Jobs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jobs</em>' containment reference list.
	 * @see org.haec.optimizer.testcase.TestcasePackage#getContainerSchedule_Jobs()
	 * @model containment="true"
	 * @generated
	 */
	EList<JobDescription> getJobs();

} // ContainerSchedule
