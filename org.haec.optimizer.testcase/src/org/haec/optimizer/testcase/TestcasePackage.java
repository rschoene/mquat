/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.haec.optimizer.testcase.TestcaseFactory
 * @model kind="package"
 * @generated
 */
public interface TestcasePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "testcase";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://testcase/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "testcase";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestcasePackage eINSTANCE = org.haec.optimizer.testcase.impl.TestcasePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.TestCaseImpl <em>Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.TestCaseImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getTestCase()
	 * @generated
	 */
	int TEST_CASE = 0;

	/**
	 * The feature id for the '<em><b>Situation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__SITUATION = 0;

	/**
	 * The feature id for the '<em><b>Expected Outcome</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__EXPECTED_OUTCOME = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__NAME = 2;

	/**
	 * The feature id for the '<em><b>Request</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__REQUEST = 3;

	/**
	 * The number of structural features of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.SituationImpl <em>Situation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.SituationImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getSituation()
	 * @generated
	 */
	int SITUATION = 1;

	/**
	 * The feature id for the '<em><b>Schedules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITUATION__SCHEDULES = 0;

	/**
	 * The feature id for the '<em><b>Copy File Patterns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITUATION__COPY_FILE_PATTERNS = 1;

	/**
	 * The number of structural features of the '<em>Situation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITUATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Situation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SITUATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.ExpectedOutcomeImpl <em>Expected Outcome</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.ExpectedOutcomeImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getExpectedOutcome()
	 * @generated
	 */
	int EXPECTED_OUTCOME = 2;

	/**
	 * The feature id for the '<em><b>Mappings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTED_OUTCOME__MAPPINGS = 0;

	/**
	 * The number of structural features of the '<em>Expected Outcome</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTED_OUTCOME_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Expected Outcome</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTED_OUTCOME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.ContainerScheduleImpl <em>Container Schedule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.ContainerScheduleImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getContainerSchedule()
	 * @generated
	 */
	int CONTAINER_SCHEDULE = 3;

	/**
	 * The feature id for the '<em><b>Container Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SCHEDULE__CONTAINER_NAME = 0;

	/**
	 * The feature id for the '<em><b>Jobs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SCHEDULE__JOBS = 1;

	/**
	 * The number of structural features of the '<em>Container Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SCHEDULE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Container Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SCHEDULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.JobDescriptionImpl <em>Job Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.JobDescriptionImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getJobDescription()
	 * @generated
	 */
	int JOB_DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Impl Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_DESCRIPTION__IMPL_NAME = 0;

	/**
	 * The feature id for the '<em><b>Predicted Runtime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_DESCRIPTION__PREDICTED_RUNTIME = 1;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_DESCRIPTION__START_TIME = 2;

	/**
	 * The number of structural features of the '<em>Job Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_DESCRIPTION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Job Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOB_DESCRIPTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.ImplMappingImpl <em>Impl Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.ImplMappingImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getImplMapping()
	 * @generated
	 */
	int IMPL_MAPPING = 7;

	/**
	 * The feature id for the '<em><b>Impl Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_MAPPING__IMPL_NAME = 0;

	/**
	 * The number of structural features of the '<em>Impl Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_MAPPING_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Impl Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_MAPPING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.ImplToContainerImpl <em>Impl To Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.ImplToContainerImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getImplToContainer()
	 * @generated
	 */
	int IMPL_TO_CONTAINER = 5;

	/**
	 * The feature id for the '<em><b>Impl Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_TO_CONTAINER__IMPL_NAME = IMPL_MAPPING__IMPL_NAME;

	/**
	 * The feature id for the '<em><b>Container Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_TO_CONTAINER__CONTAINER_NAME = IMPL_MAPPING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start At Millis</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_TO_CONTAINER__START_AT_MILLIS = IMPL_MAPPING_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Impl To Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_TO_CONTAINER_FEATURE_COUNT = IMPL_MAPPING_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Impl To Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_TO_CONTAINER_OPERATION_COUNT = IMPL_MAPPING_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.TestCasesImpl <em>Test Cases</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.TestCasesImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getTestCases()
	 * @generated
	 */
	int TEST_CASES = 6;

	/**
	 * The feature id for the '<em><b>Testcases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES__TESTCASES = 0;

	/**
	 * The feature id for the '<em><b>Settings</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES__SETTINGS = 1;

	/**
	 * The number of structural features of the '<em>Test Cases</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Test Cases</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.ImplDontCareImpl <em>Impl Dont Care</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.ImplDontCareImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getImplDontCare()
	 * @generated
	 */
	int IMPL_DONT_CARE = 8;

	/**
	 * The feature id for the '<em><b>Impl Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_DONT_CARE__IMPL_NAME = IMPL_MAPPING__IMPL_NAME;

	/**
	 * The number of structural features of the '<em>Impl Dont Care</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_DONT_CARE_FEATURE_COUNT = IMPL_MAPPING_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Impl Dont Care</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPL_DONT_CARE_OPERATION_COUNT = IMPL_MAPPING_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.RequestImpl <em>Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.RequestImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getRequest()
	 * @generated
	 */
	int REQUEST = 9;

	/**
	 * The feature id for the '<em><b>Component Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__COMPONENT_NAME = 0;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__PORT_NAME = 1;

	/**
	 * The feature id for the '<em><b>Req Property Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__REQ_PROPERTY_NAME = 2;

	/**
	 * The feature id for the '<em><b>Req Property Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__REQ_PROPERTY_VALUE = 3;

	/**
	 * The feature id for the '<em><b>Metaparam Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__METAPARAM_NAME = 4;

	/**
	 * The feature id for the '<em><b>Metaparam Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__METAPARAM_VALUE = 5;

	/**
	 * The feature id for the '<em><b>Hardware Variantmodel File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__HARDWARE_VARIANTMODEL_FILE = 6;

	/**
	 * The feature id for the '<em><b>Profile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__PROFILE = 7;

	/**
	 * The number of structural features of the '<em>Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.CopyFilePatternImpl <em>Copy File Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.CopyFilePatternImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getCopyFilePattern()
	 * @generated
	 */
	int COPY_FILE_PATTERN = 10;

	/**
	 * The feature id for the '<em><b>Contains String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_PATTERN__CONTAINS_STRING = 0;

	/**
	 * The feature id for the '<em><b>Exact String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_PATTERN__EXACT_STRING = 1;

	/**
	 * The number of structural features of the '<em>Copy File Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_PATTERN_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Copy File Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_FILE_PATTERN_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.impl.SettingsImpl <em>Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.impl.SettingsImpl
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getSettings()
	 * @generated
	 */
	int SETTINGS = 11;

	/**
	 * The feature id for the '<em><b>Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__PATTERN = 0;

	/**
	 * The feature id for the '<em><b>Only First</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS__ONLY_FIRST = 1;

	/**
	 * The number of structural features of the '<em>Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SETTINGS_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link org.haec.optimizer.testcase.Profile <em>Profile</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.haec.optimizer.testcase.Profile
	 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getProfile()
	 * @generated
	 */
	int PROFILE = 12;


	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case</em>'.
	 * @see org.haec.optimizer.testcase.TestCase
	 * @generated
	 */
	EClass getTestCase();

	/**
	 * Returns the meta object for the containment reference '{@link org.haec.optimizer.testcase.TestCase#getSituation <em>Situation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Situation</em>'.
	 * @see org.haec.optimizer.testcase.TestCase#getSituation()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_Situation();

	/**
	 * Returns the meta object for the containment reference '{@link org.haec.optimizer.testcase.TestCase#getExpectedOutcome <em>Expected Outcome</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expected Outcome</em>'.
	 * @see org.haec.optimizer.testcase.TestCase#getExpectedOutcome()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_ExpectedOutcome();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.TestCase#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.haec.optimizer.testcase.TestCase#getName()
	 * @see #getTestCase()
	 * @generated
	 */
	EAttribute getTestCase_Name();

	/**
	 * Returns the meta object for the containment reference '{@link org.haec.optimizer.testcase.TestCase#getRequest <em>Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Request</em>'.
	 * @see org.haec.optimizer.testcase.TestCase#getRequest()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_Request();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.Situation <em>Situation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Situation</em>'.
	 * @see org.haec.optimizer.testcase.Situation
	 * @generated
	 */
	EClass getSituation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.haec.optimizer.testcase.Situation#getSchedules <em>Schedules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Schedules</em>'.
	 * @see org.haec.optimizer.testcase.Situation#getSchedules()
	 * @see #getSituation()
	 * @generated
	 */
	EReference getSituation_Schedules();

	/**
	 * Returns the meta object for the containment reference list '{@link org.haec.optimizer.testcase.Situation#getCopyFilePatterns <em>Copy File Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Copy File Patterns</em>'.
	 * @see org.haec.optimizer.testcase.Situation#getCopyFilePatterns()
	 * @see #getSituation()
	 * @generated
	 */
	EReference getSituation_CopyFilePatterns();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.ExpectedOutcome <em>Expected Outcome</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expected Outcome</em>'.
	 * @see org.haec.optimizer.testcase.ExpectedOutcome
	 * @generated
	 */
	EClass getExpectedOutcome();

	/**
	 * Returns the meta object for the containment reference list '{@link org.haec.optimizer.testcase.ExpectedOutcome#getMappings <em>Mappings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Mappings</em>'.
	 * @see org.haec.optimizer.testcase.ExpectedOutcome#getMappings()
	 * @see #getExpectedOutcome()
	 * @generated
	 */
	EReference getExpectedOutcome_Mappings();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.ContainerSchedule <em>Container Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container Schedule</em>'.
	 * @see org.haec.optimizer.testcase.ContainerSchedule
	 * @generated
	 */
	EClass getContainerSchedule();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.ContainerSchedule#getContainerName <em>Container Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Container Name</em>'.
	 * @see org.haec.optimizer.testcase.ContainerSchedule#getContainerName()
	 * @see #getContainerSchedule()
	 * @generated
	 */
	EAttribute getContainerSchedule_ContainerName();

	/**
	 * Returns the meta object for the containment reference list '{@link org.haec.optimizer.testcase.ContainerSchedule#getJobs <em>Jobs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Jobs</em>'.
	 * @see org.haec.optimizer.testcase.ContainerSchedule#getJobs()
	 * @see #getContainerSchedule()
	 * @generated
	 */
	EReference getContainerSchedule_Jobs();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.JobDescription <em>Job Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Job Description</em>'.
	 * @see org.haec.optimizer.testcase.JobDescription
	 * @generated
	 */
	EClass getJobDescription();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.JobDescription#getImplName <em>Impl Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impl Name</em>'.
	 * @see org.haec.optimizer.testcase.JobDescription#getImplName()
	 * @see #getJobDescription()
	 * @generated
	 */
	EAttribute getJobDescription_ImplName();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.JobDescription#getPredictedRuntime <em>Predicted Runtime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Predicted Runtime</em>'.
	 * @see org.haec.optimizer.testcase.JobDescription#getPredictedRuntime()
	 * @see #getJobDescription()
	 * @generated
	 */
	EAttribute getJobDescription_PredictedRuntime();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.JobDescription#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see org.haec.optimizer.testcase.JobDescription#getStartTime()
	 * @see #getJobDescription()
	 * @generated
	 */
	EAttribute getJobDescription_StartTime();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.ImplToContainer <em>Impl To Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Impl To Container</em>'.
	 * @see org.haec.optimizer.testcase.ImplToContainer
	 * @generated
	 */
	EClass getImplToContainer();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.ImplToContainer#getContainerName <em>Container Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Container Name</em>'.
	 * @see org.haec.optimizer.testcase.ImplToContainer#getContainerName()
	 * @see #getImplToContainer()
	 * @generated
	 */
	EAttribute getImplToContainer_ContainerName();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.ImplToContainer#getStartAtMillis <em>Start At Millis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start At Millis</em>'.
	 * @see org.haec.optimizer.testcase.ImplToContainer#getStartAtMillis()
	 * @see #getImplToContainer()
	 * @generated
	 */
	EAttribute getImplToContainer_StartAtMillis();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.TestCases <em>Test Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Cases</em>'.
	 * @see org.haec.optimizer.testcase.TestCases
	 * @generated
	 */
	EClass getTestCases();

	/**
	 * Returns the meta object for the containment reference list '{@link org.haec.optimizer.testcase.TestCases#getTestcases <em>Testcases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Testcases</em>'.
	 * @see org.haec.optimizer.testcase.TestCases#getTestcases()
	 * @see #getTestCases()
	 * @generated
	 */
	EReference getTestCases_Testcases();

	/**
	 * Returns the meta object for the containment reference '{@link org.haec.optimizer.testcase.TestCases#getSettings <em>Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Settings</em>'.
	 * @see org.haec.optimizer.testcase.TestCases#getSettings()
	 * @see #getTestCases()
	 * @generated
	 */
	EReference getTestCases_Settings();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.ImplMapping <em>Impl Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Impl Mapping</em>'.
	 * @see org.haec.optimizer.testcase.ImplMapping
	 * @generated
	 */
	EClass getImplMapping();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.ImplMapping#getImplName <em>Impl Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Impl Name</em>'.
	 * @see org.haec.optimizer.testcase.ImplMapping#getImplName()
	 * @see #getImplMapping()
	 * @generated
	 */
	EAttribute getImplMapping_ImplName();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.ImplDontCare <em>Impl Dont Care</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Impl Dont Care</em>'.
	 * @see org.haec.optimizer.testcase.ImplDontCare
	 * @generated
	 */
	EClass getImplDontCare();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.Request <em>Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Request</em>'.
	 * @see org.haec.optimizer.testcase.Request
	 * @generated
	 */
	EClass getRequest();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getComponentName <em>Component Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Component Name</em>'.
	 * @see org.haec.optimizer.testcase.Request#getComponentName()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_ComponentName();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getPortName <em>Port Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Name</em>'.
	 * @see org.haec.optimizer.testcase.Request#getPortName()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_PortName();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getReqPropertyName <em>Req Property Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Req Property Name</em>'.
	 * @see org.haec.optimizer.testcase.Request#getReqPropertyName()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_ReqPropertyName();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getReqPropertyValue <em>Req Property Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Req Property Value</em>'.
	 * @see org.haec.optimizer.testcase.Request#getReqPropertyValue()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_ReqPropertyValue();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getMetaparamValue <em>Metaparam Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metaparam Value</em>'.
	 * @see org.haec.optimizer.testcase.Request#getMetaparamValue()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_MetaparamValue();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getHardwareVariantmodelFile <em>Hardware Variantmodel File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hardware Variantmodel File</em>'.
	 * @see org.haec.optimizer.testcase.Request#getHardwareVariantmodelFile()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_HardwareVariantmodelFile();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getProfile <em>Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Profile</em>'.
	 * @see org.haec.optimizer.testcase.Request#getProfile()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_Profile();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.CopyFilePattern <em>Copy File Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy File Pattern</em>'.
	 * @see org.haec.optimizer.testcase.CopyFilePattern
	 * @generated
	 */
	EClass getCopyFilePattern();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.CopyFilePattern#getContainsString <em>Contains String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Contains String</em>'.
	 * @see org.haec.optimizer.testcase.CopyFilePattern#getContainsString()
	 * @see #getCopyFilePattern()
	 * @generated
	 */
	EAttribute getCopyFilePattern_ContainsString();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.CopyFilePattern#getExactString <em>Exact String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exact String</em>'.
	 * @see org.haec.optimizer.testcase.CopyFilePattern#getExactString()
	 * @see #getCopyFilePattern()
	 * @generated
	 */
	EAttribute getCopyFilePattern_ExactString();

	/**
	 * Returns the meta object for class '{@link org.haec.optimizer.testcase.Settings <em>Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Settings</em>'.
	 * @see org.haec.optimizer.testcase.Settings
	 * @generated
	 */
	EClass getSettings();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Settings#getPattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pattern</em>'.
	 * @see org.haec.optimizer.testcase.Settings#getPattern()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_Pattern();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Settings#isOnlyFirst <em>Only First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Only First</em>'.
	 * @see org.haec.optimizer.testcase.Settings#isOnlyFirst()
	 * @see #getSettings()
	 * @generated
	 */
	EAttribute getSettings_OnlyFirst();

	/**
	 * Returns the meta object for enum '{@link org.haec.optimizer.testcase.Profile <em>Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Profile</em>'.
	 * @see org.haec.optimizer.testcase.Profile
	 * @generated
	 */
	EEnum getProfile();

	/**
	 * Returns the meta object for the attribute '{@link org.haec.optimizer.testcase.Request#getMetaparamName <em>Metaparam Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metaparam Name</em>'.
	 * @see org.haec.optimizer.testcase.Request#getMetaparamName()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_MetaparamName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TestcaseFactory getTestcaseFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.TestCaseImpl <em>Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.TestCaseImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getTestCase()
		 * @generated
		 */
		EClass TEST_CASE = eINSTANCE.getTestCase();

		/**
		 * The meta object literal for the '<em><b>Situation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__SITUATION = eINSTANCE.getTestCase_Situation();

		/**
		 * The meta object literal for the '<em><b>Expected Outcome</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__EXPECTED_OUTCOME = eINSTANCE.getTestCase_ExpectedOutcome();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASE__NAME = eINSTANCE.getTestCase_Name();

		/**
		 * The meta object literal for the '<em><b>Request</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__REQUEST = eINSTANCE.getTestCase_Request();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.SituationImpl <em>Situation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.SituationImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getSituation()
		 * @generated
		 */
		EClass SITUATION = eINSTANCE.getSituation();

		/**
		 * The meta object literal for the '<em><b>Schedules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SITUATION__SCHEDULES = eINSTANCE.getSituation_Schedules();

		/**
		 * The meta object literal for the '<em><b>Copy File Patterns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SITUATION__COPY_FILE_PATTERNS = eINSTANCE.getSituation_CopyFilePatterns();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.ExpectedOutcomeImpl <em>Expected Outcome</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.ExpectedOutcomeImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getExpectedOutcome()
		 * @generated
		 */
		EClass EXPECTED_OUTCOME = eINSTANCE.getExpectedOutcome();

		/**
		 * The meta object literal for the '<em><b>Mappings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPECTED_OUTCOME__MAPPINGS = eINSTANCE.getExpectedOutcome_Mappings();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.ContainerScheduleImpl <em>Container Schedule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.ContainerScheduleImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getContainerSchedule()
		 * @generated
		 */
		EClass CONTAINER_SCHEDULE = eINSTANCE.getContainerSchedule();

		/**
		 * The meta object literal for the '<em><b>Container Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTAINER_SCHEDULE__CONTAINER_NAME = eINSTANCE.getContainerSchedule_ContainerName();

		/**
		 * The meta object literal for the '<em><b>Jobs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER_SCHEDULE__JOBS = eINSTANCE.getContainerSchedule_Jobs();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.JobDescriptionImpl <em>Job Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.JobDescriptionImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getJobDescription()
		 * @generated
		 */
		EClass JOB_DESCRIPTION = eINSTANCE.getJobDescription();

		/**
		 * The meta object literal for the '<em><b>Impl Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB_DESCRIPTION__IMPL_NAME = eINSTANCE.getJobDescription_ImplName();

		/**
		 * The meta object literal for the '<em><b>Predicted Runtime</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB_DESCRIPTION__PREDICTED_RUNTIME = eINSTANCE.getJobDescription_PredictedRuntime();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOB_DESCRIPTION__START_TIME = eINSTANCE.getJobDescription_StartTime();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.ImplToContainerImpl <em>Impl To Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.ImplToContainerImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getImplToContainer()
		 * @generated
		 */
		EClass IMPL_TO_CONTAINER = eINSTANCE.getImplToContainer();

		/**
		 * The meta object literal for the '<em><b>Container Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPL_TO_CONTAINER__CONTAINER_NAME = eINSTANCE.getImplToContainer_ContainerName();

		/**
		 * The meta object literal for the '<em><b>Start At Millis</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPL_TO_CONTAINER__START_AT_MILLIS = eINSTANCE.getImplToContainer_StartAtMillis();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.TestCasesImpl <em>Test Cases</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.TestCasesImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getTestCases()
		 * @generated
		 */
		EClass TEST_CASES = eINSTANCE.getTestCases();

		/**
		 * The meta object literal for the '<em><b>Testcases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASES__TESTCASES = eINSTANCE.getTestCases_Testcases();

		/**
		 * The meta object literal for the '<em><b>Settings</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASES__SETTINGS = eINSTANCE.getTestCases_Settings();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.ImplMappingImpl <em>Impl Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.ImplMappingImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getImplMapping()
		 * @generated
		 */
		EClass IMPL_MAPPING = eINSTANCE.getImplMapping();

		/**
		 * The meta object literal for the '<em><b>Impl Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPL_MAPPING__IMPL_NAME = eINSTANCE.getImplMapping_ImplName();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.ImplDontCareImpl <em>Impl Dont Care</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.ImplDontCareImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getImplDontCare()
		 * @generated
		 */
		EClass IMPL_DONT_CARE = eINSTANCE.getImplDontCare();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.RequestImpl <em>Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.RequestImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getRequest()
		 * @generated
		 */
		EClass REQUEST = eINSTANCE.getRequest();

		/**
		 * The meta object literal for the '<em><b>Component Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__COMPONENT_NAME = eINSTANCE.getRequest_ComponentName();

		/**
		 * The meta object literal for the '<em><b>Port Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__PORT_NAME = eINSTANCE.getRequest_PortName();

		/**
		 * The meta object literal for the '<em><b>Req Property Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__REQ_PROPERTY_NAME = eINSTANCE.getRequest_ReqPropertyName();

		/**
		 * The meta object literal for the '<em><b>Req Property Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__REQ_PROPERTY_VALUE = eINSTANCE.getRequest_ReqPropertyValue();

		/**
		 * The meta object literal for the '<em><b>Metaparam Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__METAPARAM_VALUE = eINSTANCE.getRequest_MetaparamValue();

		/**
		 * The meta object literal for the '<em><b>Hardware Variantmodel File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__HARDWARE_VARIANTMODEL_FILE = eINSTANCE.getRequest_HardwareVariantmodelFile();

		/**
		 * The meta object literal for the '<em><b>Profile</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__PROFILE = eINSTANCE.getRequest_Profile();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.CopyFilePatternImpl <em>Copy File Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.CopyFilePatternImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getCopyFilePattern()
		 * @generated
		 */
		EClass COPY_FILE_PATTERN = eINSTANCE.getCopyFilePattern();

		/**
		 * The meta object literal for the '<em><b>Contains String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_FILE_PATTERN__CONTAINS_STRING = eINSTANCE.getCopyFilePattern_ContainsString();

		/**
		 * The meta object literal for the '<em><b>Exact String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COPY_FILE_PATTERN__EXACT_STRING = eINSTANCE.getCopyFilePattern_ExactString();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.impl.SettingsImpl <em>Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.impl.SettingsImpl
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getSettings()
		 * @generated
		 */
		EClass SETTINGS = eINSTANCE.getSettings();

		/**
		 * The meta object literal for the '<em><b>Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__PATTERN = eINSTANCE.getSettings_Pattern();

		/**
		 * The meta object literal for the '<em><b>Only First</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SETTINGS__ONLY_FIRST = eINSTANCE.getSettings_OnlyFirst();

		/**
		 * The meta object literal for the '{@link org.haec.optimizer.testcase.Profile <em>Profile</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.haec.optimizer.testcase.Profile
		 * @see org.haec.optimizer.testcase.impl.TestcasePackageImpl#getProfile()
		 * @generated
		 */
		EEnum PROFILE = eINSTANCE.getProfile();

		/**
		 * The meta object literal for the '<em><b>Metaparam Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__METAPARAM_NAME = eINSTANCE.getRequest_MetaparamName();

	}

} //TestcasePackage
