/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Impl To Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.ImplToContainer#getContainerName <em>Container Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.ImplToContainer#getStartAtMillis <em>Start At Millis</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.haec.optimizer.testcase.TestcasePackage#getImplToContainer()
 * @model
 * @generated
 */
public interface ImplToContainer extends ImplMapping {
	/**
	 * Returns the value of the '<em><b>Container Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Name</em>' attribute.
	 * @see #setContainerName(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getImplToContainer_ContainerName()
	 * @model required="true"
	 * @generated
	 */
	String getContainerName();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.ImplToContainer#getContainerName <em>Container Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Name</em>' attribute.
	 * @see #getContainerName()
	 * @generated
	 */
	void setContainerName(String value);

	/**
	 * Returns the value of the '<em><b>Start At Millis</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start At Millis</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start At Millis</em>' attribute.
	 * @see #setStartAtMillis(Long)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getImplToContainer_StartAtMillis()
	 * @model
	 * @generated
	 */
	Long getStartAtMillis();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.ImplToContainer#getStartAtMillis <em>Start At Millis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start At Millis</em>' attribute.
	 * @see #getStartAtMillis()
	 * @generated
	 */
	void setStartAtMillis(Long value);

} // ImplToContainer
