/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.Request#getComponentName <em>Component Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Request#getPortName <em>Port Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Request#getReqPropertyName <em>Req Property Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Request#getReqPropertyValue <em>Req Property Value</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Request#getMetaparamName <em>Metaparam Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Request#getMetaparamValue <em>Metaparam Value</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Request#getHardwareVariantmodelFile <em>Hardware Variantmodel File</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Request#getProfile <em>Profile</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest()
 * @model
 * @generated
 */
public interface Request extends EObject {
	/**
	 * Returns the value of the '<em><b>Component Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Name</em>' attribute.
	 * @see #setComponentName(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_ComponentName()
	 * @model
	 * @generated
	 */
	String getComponentName();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getComponentName <em>Component Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Name</em>' attribute.
	 * @see #getComponentName()
	 * @generated
	 */
	void setComponentName(String value);

	/**
	 * Returns the value of the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Name</em>' attribute.
	 * @see #setPortName(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_PortName()
	 * @model
	 * @generated
	 */
	String getPortName();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getPortName <em>Port Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Name</em>' attribute.
	 * @see #getPortName()
	 * @generated
	 */
	void setPortName(String value);

	/**
	 * Returns the value of the '<em><b>Req Property Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Req Property Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Req Property Name</em>' attribute.
	 * @see #setReqPropertyName(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_ReqPropertyName()
	 * @model
	 * @generated
	 */
	String getReqPropertyName();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getReqPropertyName <em>Req Property Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Req Property Name</em>' attribute.
	 * @see #getReqPropertyName()
	 * @generated
	 */
	void setReqPropertyName(String value);

	/**
	 * Returns the value of the '<em><b>Req Property Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Req Property Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Req Property Value</em>' attribute.
	 * @see #setReqPropertyValue(Double)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_ReqPropertyValue()
	 * @model
	 * @generated
	 */
	Double getReqPropertyValue();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getReqPropertyValue <em>Req Property Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Req Property Value</em>' attribute.
	 * @see #getReqPropertyValue()
	 * @generated
	 */
	void setReqPropertyValue(Double value);

	/**
	 * Returns the value of the '<em><b>Metaparam Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metaparam Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metaparam Value</em>' attribute.
	 * @see #setMetaparamValue(Integer)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_MetaparamValue()
	 * @model
	 * @generated
	 */
	Integer getMetaparamValue();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getMetaparamValue <em>Metaparam Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metaparam Value</em>' attribute.
	 * @see #getMetaparamValue()
	 * @generated
	 */
	void setMetaparamValue(Integer value);

	/**
	 * Returns the value of the '<em><b>Hardware Variantmodel File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hardware Variantmodel File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hardware Variantmodel File</em>' attribute.
	 * @see #setHardwareVariantmodelFile(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_HardwareVariantmodelFile()
	 * @model
	 * @generated
	 */
	String getHardwareVariantmodelFile();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getHardwareVariantmodelFile <em>Hardware Variantmodel File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hardware Variantmodel File</em>' attribute.
	 * @see #getHardwareVariantmodelFile()
	 * @generated
	 */
	void setHardwareVariantmodelFile(String value);

	/**
	 * Returns the value of the '<em><b>Profile</b></em>' attribute.
	 * The default value is <code>"normal"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile</em>' attribute.
	 * @see #setProfile(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_Profile()
	 * @model default="normal"
	 * @generated
	 */
	String getProfile();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getProfile <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile</em>' attribute.
	 * @see #getProfile()
	 * @generated
	 */
	void setProfile(String value);

	/**
	 * Returns the value of the '<em><b>Metaparam Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metaparam Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metaparam Name</em>' attribute.
	 * @see #setMetaparamName(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getRequest_MetaparamName()
	 * @model
	 * @generated
	 */
	String getMetaparamName();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.Request#getMetaparamName <em>Metaparam Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metaparam Name</em>' attribute.
	 * @see #getMetaparamName()
	 * @generated
	 */
	void setMetaparamName(String value);

} // Request
