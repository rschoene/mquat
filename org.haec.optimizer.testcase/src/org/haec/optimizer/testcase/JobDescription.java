/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Job Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.JobDescription#getImplName <em>Impl Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.JobDescription#getPredictedRuntime <em>Predicted Runtime</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.JobDescription#getStartTime <em>Start Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.haec.optimizer.testcase.TestcasePackage#getJobDescription()
 * @model
 * @generated
 */
public interface JobDescription extends EObject {
	/**
	 * Returns the value of the '<em><b>Impl Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Impl Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Impl Name</em>' attribute.
	 * @see #setImplName(String)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getJobDescription_ImplName()
	 * @model required="true"
	 * @generated
	 */
	String getImplName();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.JobDescription#getImplName <em>Impl Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Impl Name</em>' attribute.
	 * @see #getImplName()
	 * @generated
	 */
	void setImplName(String value);

	/**
	 * Returns the value of the '<em><b>Predicted Runtime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predicted Runtime</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicted Runtime</em>' attribute.
	 * @see #setPredictedRuntime(double)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getJobDescription_PredictedRuntime()
	 * @model required="true"
	 * @generated
	 */
	double getPredictedRuntime();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.JobDescription#getPredictedRuntime <em>Predicted Runtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predicted Runtime</em>' attribute.
	 * @see #getPredictedRuntime()
	 * @generated
	 */
	void setPredictedRuntime(double value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Long)
	 * @see org.haec.optimizer.testcase.TestcasePackage#getJobDescription_StartTime()
	 * @model
	 * @generated
	 */
	Long getStartTime();

	/**
	 * Sets the value of the '{@link org.haec.optimizer.testcase.JobDescription#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Long value);

} // JobDescription
