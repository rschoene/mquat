/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Situation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.Situation#getSchedules <em>Schedules</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.Situation#getCopyFilePatterns <em>Copy File Patterns</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.haec.optimizer.testcase.TestcasePackage#getSituation()
 * @model
 * @generated
 */
public interface Situation extends EObject {
	/**
	 * Returns the value of the '<em><b>Schedules</b></em>' containment reference list.
	 * The list contents are of type {@link org.haec.optimizer.testcase.ContainerSchedule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedules</em>' containment reference list.
	 * @see org.haec.optimizer.testcase.TestcasePackage#getSituation_Schedules()
	 * @model containment="true"
	 * @generated
	 */
	EList<ContainerSchedule> getSchedules();

	/**
	 * Returns the value of the '<em><b>Copy File Patterns</b></em>' containment reference list.
	 * The list contents are of type {@link org.haec.optimizer.testcase.CopyFilePattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copy File Patterns</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copy File Patterns</em>' containment reference list.
	 * @see org.haec.optimizer.testcase.TestcasePackage#getSituation_CopyFilePatterns()
	 * @model containment="true"
	 * @generated
	 */
	EList<CopyFilePattern> getCopyFilePatterns();

} // Situation
