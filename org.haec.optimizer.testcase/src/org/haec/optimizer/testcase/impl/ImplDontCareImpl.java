/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase.impl;

import org.eclipse.emf.ecore.EClass;

import org.haec.optimizer.testcase.ImplDontCare;
import org.haec.optimizer.testcase.TestcasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Impl Dont Care</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ImplDontCareImpl extends ImplMappingImpl implements ImplDontCare {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImplDontCareImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestcasePackage.Literals.IMPL_DONT_CARE;
	}

} //ImplDontCareImpl
