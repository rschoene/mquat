/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.haec.optimizer.testcase.Request;
import org.haec.optimizer.testcase.TestcasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Request</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getComponentName <em>Component Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getPortName <em>Port Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getReqPropertyName <em>Req Property Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getReqPropertyValue <em>Req Property Value</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getMetaparamName <em>Metaparam Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getMetaparamValue <em>Metaparam Value</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getHardwareVariantmodelFile <em>Hardware Variantmodel File</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.RequestImpl#getProfile <em>Profile</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RequestImpl extends MinimalEObjectImpl.Container implements Request {
	/**
	 * The default value of the '{@link #getComponentName() <em>Component Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentName()
	 * @generated
	 * @ordered
	 */
	protected static final String COMPONENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getComponentName() <em>Component Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentName()
	 * @generated
	 * @ordered
	 */
	protected String componentName = COMPONENT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPortName() <em>Port Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortName()
	 * @generated
	 * @ordered
	 */
	protected static final String PORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPortName() <em>Port Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortName()
	 * @generated
	 * @ordered
	 */
	protected String portName = PORT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getReqPropertyName() <em>Req Property Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPropertyName()
	 * @generated
	 * @ordered
	 */
	protected static final String REQ_PROPERTY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReqPropertyName() <em>Req Property Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPropertyName()
	 * @generated
	 * @ordered
	 */
	protected String reqPropertyName = REQ_PROPERTY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getReqPropertyValue() <em>Req Property Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPropertyValue()
	 * @generated
	 * @ordered
	 */
	protected static final Double REQ_PROPERTY_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReqPropertyValue() <em>Req Property Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPropertyValue()
	 * @generated
	 * @ordered
	 */
	protected Double reqPropertyValue = REQ_PROPERTY_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMetaparamName() <em>Metaparam Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaparamName()
	 * @generated
	 * @ordered
	 */
	protected static final String METAPARAM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMetaparamName() <em>Metaparam Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaparamName()
	 * @generated
	 * @ordered
	 */
	protected String metaparamName = METAPARAM_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMetaparamValue() <em>Metaparam Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaparamValue()
	 * @generated
	 * @ordered
	 */
	protected static final Integer METAPARAM_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMetaparamValue() <em>Metaparam Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaparamValue()
	 * @generated
	 * @ordered
	 */
	protected Integer metaparamValue = METAPARAM_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getHardwareVariantmodelFile() <em>Hardware Variantmodel File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHardwareVariantmodelFile()
	 * @generated
	 * @ordered
	 */
	protected static final String HARDWARE_VARIANTMODEL_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHardwareVariantmodelFile() <em>Hardware Variantmodel File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHardwareVariantmodelFile()
	 * @generated
	 * @ordered
	 */
	protected String hardwareVariantmodelFile = HARDWARE_VARIANTMODEL_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getProfile() <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfile()
	 * @generated
	 * @ordered
	 */
	protected static final String PROFILE_EDEFAULT = "normal";

	/**
	 * The cached value of the '{@link #getProfile() <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfile()
	 * @generated
	 * @ordered
	 */
	protected String profile = PROFILE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestcasePackage.Literals.REQUEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComponentName() {
		return componentName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentName(String newComponentName) {
		String oldComponentName = componentName;
		componentName = newComponentName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__COMPONENT_NAME, oldComponentName, componentName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPortName() {
		return portName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortName(String newPortName) {
		String oldPortName = portName;
		portName = newPortName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__PORT_NAME, oldPortName, portName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReqPropertyName() {
		return reqPropertyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReqPropertyName(String newReqPropertyName) {
		String oldReqPropertyName = reqPropertyName;
		reqPropertyName = newReqPropertyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__REQ_PROPERTY_NAME, oldReqPropertyName, reqPropertyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getReqPropertyValue() {
		return reqPropertyValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReqPropertyValue(Double newReqPropertyValue) {
		Double oldReqPropertyValue = reqPropertyValue;
		reqPropertyValue = newReqPropertyValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__REQ_PROPERTY_VALUE, oldReqPropertyValue, reqPropertyValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getMetaparamValue() {
		return metaparamValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetaparamValue(Integer newMetaparamValue) {
		Integer oldMetaparamValue = metaparamValue;
		metaparamValue = newMetaparamValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__METAPARAM_VALUE, oldMetaparamValue, metaparamValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHardwareVariantmodelFile() {
		return hardwareVariantmodelFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHardwareVariantmodelFile(String newHardwareVariantmodelFile) {
		String oldHardwareVariantmodelFile = hardwareVariantmodelFile;
		hardwareVariantmodelFile = newHardwareVariantmodelFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__HARDWARE_VARIANTMODEL_FILE, oldHardwareVariantmodelFile, hardwareVariantmodelFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getProfile() {
		return profile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfile(String newProfile) {
		String oldProfile = profile;
		profile = newProfile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__PROFILE, oldProfile, profile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMetaparamName() {
		return metaparamName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetaparamName(String newMetaparamName) {
		String oldMetaparamName = metaparamName;
		metaparamName = newMetaparamName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.REQUEST__METAPARAM_NAME, oldMetaparamName, metaparamName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestcasePackage.REQUEST__COMPONENT_NAME:
				return getComponentName();
			case TestcasePackage.REQUEST__PORT_NAME:
				return getPortName();
			case TestcasePackage.REQUEST__REQ_PROPERTY_NAME:
				return getReqPropertyName();
			case TestcasePackage.REQUEST__REQ_PROPERTY_VALUE:
				return getReqPropertyValue();
			case TestcasePackage.REQUEST__METAPARAM_NAME:
				return getMetaparamName();
			case TestcasePackage.REQUEST__METAPARAM_VALUE:
				return getMetaparamValue();
			case TestcasePackage.REQUEST__HARDWARE_VARIANTMODEL_FILE:
				return getHardwareVariantmodelFile();
			case TestcasePackage.REQUEST__PROFILE:
				return getProfile();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestcasePackage.REQUEST__COMPONENT_NAME:
				setComponentName((String)newValue);
				return;
			case TestcasePackage.REQUEST__PORT_NAME:
				setPortName((String)newValue);
				return;
			case TestcasePackage.REQUEST__REQ_PROPERTY_NAME:
				setReqPropertyName((String)newValue);
				return;
			case TestcasePackage.REQUEST__REQ_PROPERTY_VALUE:
				setReqPropertyValue((Double)newValue);
				return;
			case TestcasePackage.REQUEST__METAPARAM_NAME:
				setMetaparamName((String)newValue);
				return;
			case TestcasePackage.REQUEST__METAPARAM_VALUE:
				setMetaparamValue((Integer)newValue);
				return;
			case TestcasePackage.REQUEST__HARDWARE_VARIANTMODEL_FILE:
				setHardwareVariantmodelFile((String)newValue);
				return;
			case TestcasePackage.REQUEST__PROFILE:
				setProfile((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestcasePackage.REQUEST__COMPONENT_NAME:
				setComponentName(COMPONENT_NAME_EDEFAULT);
				return;
			case TestcasePackage.REQUEST__PORT_NAME:
				setPortName(PORT_NAME_EDEFAULT);
				return;
			case TestcasePackage.REQUEST__REQ_PROPERTY_NAME:
				setReqPropertyName(REQ_PROPERTY_NAME_EDEFAULT);
				return;
			case TestcasePackage.REQUEST__REQ_PROPERTY_VALUE:
				setReqPropertyValue(REQ_PROPERTY_VALUE_EDEFAULT);
				return;
			case TestcasePackage.REQUEST__METAPARAM_NAME:
				setMetaparamName(METAPARAM_NAME_EDEFAULT);
				return;
			case TestcasePackage.REQUEST__METAPARAM_VALUE:
				setMetaparamValue(METAPARAM_VALUE_EDEFAULT);
				return;
			case TestcasePackage.REQUEST__HARDWARE_VARIANTMODEL_FILE:
				setHardwareVariantmodelFile(HARDWARE_VARIANTMODEL_FILE_EDEFAULT);
				return;
			case TestcasePackage.REQUEST__PROFILE:
				setProfile(PROFILE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestcasePackage.REQUEST__COMPONENT_NAME:
				return COMPONENT_NAME_EDEFAULT == null ? componentName != null : !COMPONENT_NAME_EDEFAULT.equals(componentName);
			case TestcasePackage.REQUEST__PORT_NAME:
				return PORT_NAME_EDEFAULT == null ? portName != null : !PORT_NAME_EDEFAULT.equals(portName);
			case TestcasePackage.REQUEST__REQ_PROPERTY_NAME:
				return REQ_PROPERTY_NAME_EDEFAULT == null ? reqPropertyName != null : !REQ_PROPERTY_NAME_EDEFAULT.equals(reqPropertyName);
			case TestcasePackage.REQUEST__REQ_PROPERTY_VALUE:
				return REQ_PROPERTY_VALUE_EDEFAULT == null ? reqPropertyValue != null : !REQ_PROPERTY_VALUE_EDEFAULT.equals(reqPropertyValue);
			case TestcasePackage.REQUEST__METAPARAM_NAME:
				return METAPARAM_NAME_EDEFAULT == null ? metaparamName != null : !METAPARAM_NAME_EDEFAULT.equals(metaparamName);
			case TestcasePackage.REQUEST__METAPARAM_VALUE:
				return METAPARAM_VALUE_EDEFAULT == null ? metaparamValue != null : !METAPARAM_VALUE_EDEFAULT.equals(metaparamValue);
			case TestcasePackage.REQUEST__HARDWARE_VARIANTMODEL_FILE:
				return HARDWARE_VARIANTMODEL_FILE_EDEFAULT == null ? hardwareVariantmodelFile != null : !HARDWARE_VARIANTMODEL_FILE_EDEFAULT.equals(hardwareVariantmodelFile);
			case TestcasePackage.REQUEST__PROFILE:
				return PROFILE_EDEFAULT == null ? profile != null : !PROFILE_EDEFAULT.equals(profile);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (componentName: ");
		result.append(componentName);
		result.append(", portName: ");
		result.append(portName);
		result.append(", reqPropertyName: ");
		result.append(reqPropertyName);
		result.append(", reqPropertyValue: ");
		result.append(reqPropertyValue);
		result.append(", metaparamName: ");
		result.append(metaparamName);
		result.append(", metaparamValue: ");
		result.append(metaparamValue);
		result.append(", hardwareVariantmodelFile: ");
		result.append(hardwareVariantmodelFile);
		result.append(", profile: ");
		result.append(profile);
		result.append(')');
		return result.toString();
	}

} //RequestImpl
