/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.haec.optimizer.testcase.JobDescription;
import org.haec.optimizer.testcase.TestcasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Job Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.impl.JobDescriptionImpl#getImplName <em>Impl Name</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.JobDescriptionImpl#getPredictedRuntime <em>Predicted Runtime</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.JobDescriptionImpl#getStartTime <em>Start Time</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class JobDescriptionImpl extends MinimalEObjectImpl.Container implements JobDescription {
	/**
	 * The default value of the '{@link #getImplName() <em>Impl Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplName()
	 * @generated
	 * @ordered
	 */
	protected static final String IMPL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImplName() <em>Impl Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplName()
	 * @generated
	 * @ordered
	 */
	protected String implName = IMPL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPredictedRuntime() <em>Predicted Runtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredictedRuntime()
	 * @generated
	 * @ordered
	 */
	protected static final double PREDICTED_RUNTIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPredictedRuntime() <em>Predicted Runtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredictedRuntime()
	 * @generated
	 * @ordered
	 */
	protected double predictedRuntime = PREDICTED_RUNTIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final Long START_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected Long startTime = START_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JobDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestcasePackage.Literals.JOB_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImplName() {
		return implName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplName(String newImplName) {
		String oldImplName = implName;
		implName = newImplName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.JOB_DESCRIPTION__IMPL_NAME, oldImplName, implName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPredictedRuntime() {
		return predictedRuntime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredictedRuntime(double newPredictedRuntime) {
		double oldPredictedRuntime = predictedRuntime;
		predictedRuntime = newPredictedRuntime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.JOB_DESCRIPTION__PREDICTED_RUNTIME, oldPredictedRuntime, predictedRuntime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Long getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Long newStartTime) {
		Long oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.JOB_DESCRIPTION__START_TIME, oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestcasePackage.JOB_DESCRIPTION__IMPL_NAME:
				return getImplName();
			case TestcasePackage.JOB_DESCRIPTION__PREDICTED_RUNTIME:
				return getPredictedRuntime();
			case TestcasePackage.JOB_DESCRIPTION__START_TIME:
				return getStartTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestcasePackage.JOB_DESCRIPTION__IMPL_NAME:
				setImplName((String)newValue);
				return;
			case TestcasePackage.JOB_DESCRIPTION__PREDICTED_RUNTIME:
				setPredictedRuntime((Double)newValue);
				return;
			case TestcasePackage.JOB_DESCRIPTION__START_TIME:
				setStartTime((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestcasePackage.JOB_DESCRIPTION__IMPL_NAME:
				setImplName(IMPL_NAME_EDEFAULT);
				return;
			case TestcasePackage.JOB_DESCRIPTION__PREDICTED_RUNTIME:
				setPredictedRuntime(PREDICTED_RUNTIME_EDEFAULT);
				return;
			case TestcasePackage.JOB_DESCRIPTION__START_TIME:
				setStartTime(START_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestcasePackage.JOB_DESCRIPTION__IMPL_NAME:
				return IMPL_NAME_EDEFAULT == null ? implName != null : !IMPL_NAME_EDEFAULT.equals(implName);
			case TestcasePackage.JOB_DESCRIPTION__PREDICTED_RUNTIME:
				return predictedRuntime != PREDICTED_RUNTIME_EDEFAULT;
			case TestcasePackage.JOB_DESCRIPTION__START_TIME:
				return START_TIME_EDEFAULT == null ? startTime != null : !START_TIME_EDEFAULT.equals(startTime);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (implName: ");
		result.append(implName);
		result.append(", predictedRuntime: ");
		result.append(predictedRuntime);
		result.append(", startTime: ");
		result.append(startTime);
		result.append(')');
		return result.toString();
	}

} //JobDescriptionImpl
