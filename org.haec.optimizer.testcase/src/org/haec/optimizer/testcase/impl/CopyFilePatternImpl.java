/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.haec.optimizer.testcase.CopyFilePattern;
import org.haec.optimizer.testcase.TestcasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Copy File Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.impl.CopyFilePatternImpl#getContainsString <em>Contains String</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.CopyFilePatternImpl#getExactString <em>Exact String</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CopyFilePatternImpl extends MinimalEObjectImpl.Container implements CopyFilePattern {
	/**
	 * The default value of the '{@link #getContainsString() <em>Contains String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsString()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTAINS_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContainsString() <em>Contains String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainsString()
	 * @generated
	 * @ordered
	 */
	protected String containsString = CONTAINS_STRING_EDEFAULT;

	/**
	 * The default value of the '{@link #getExactString() <em>Exact String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExactString()
	 * @generated
	 * @ordered
	 */
	protected static final String EXACT_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExactString() <em>Exact String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExactString()
	 * @generated
	 * @ordered
	 */
	protected String exactString = EXACT_STRING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CopyFilePatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestcasePackage.Literals.COPY_FILE_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContainsString() {
		return containsString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainsString(String newContainsString) {
		String oldContainsString = containsString;
		containsString = newContainsString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.COPY_FILE_PATTERN__CONTAINS_STRING, oldContainsString, containsString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExactString() {
		return exactString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExactString(String newExactString) {
		String oldExactString = exactString;
		exactString = newExactString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.COPY_FILE_PATTERN__EXACT_STRING, oldExactString, exactString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestcasePackage.COPY_FILE_PATTERN__CONTAINS_STRING:
				return getContainsString();
			case TestcasePackage.COPY_FILE_PATTERN__EXACT_STRING:
				return getExactString();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestcasePackage.COPY_FILE_PATTERN__CONTAINS_STRING:
				setContainsString((String)newValue);
				return;
			case TestcasePackage.COPY_FILE_PATTERN__EXACT_STRING:
				setExactString((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestcasePackage.COPY_FILE_PATTERN__CONTAINS_STRING:
				setContainsString(CONTAINS_STRING_EDEFAULT);
				return;
			case TestcasePackage.COPY_FILE_PATTERN__EXACT_STRING:
				setExactString(EXACT_STRING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestcasePackage.COPY_FILE_PATTERN__CONTAINS_STRING:
				return CONTAINS_STRING_EDEFAULT == null ? containsString != null : !CONTAINS_STRING_EDEFAULT.equals(containsString);
			case TestcasePackage.COPY_FILE_PATTERN__EXACT_STRING:
				return EXACT_STRING_EDEFAULT == null ? exactString != null : !EXACT_STRING_EDEFAULT.equals(exactString);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (containsString: ");
		result.append(containsString);
		result.append(", exactString: ");
		result.append(exactString);
		result.append(')');
		return result.toString();
	}

} //CopyFilePatternImpl
