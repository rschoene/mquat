/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 */
package org.haec.optimizer.testcase.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.haec.optimizer.testcase.ContainerSchedule;
import org.haec.optimizer.testcase.CopyFilePattern;
import org.haec.optimizer.testcase.Situation;
import org.haec.optimizer.testcase.TestcasePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Situation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.haec.optimizer.testcase.impl.SituationImpl#getSchedules <em>Schedules</em>}</li>
 *   <li>{@link org.haec.optimizer.testcase.impl.SituationImpl#getCopyFilePatterns <em>Copy File Patterns</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SituationImpl extends MinimalEObjectImpl.Container implements Situation {
	/**
	 * The cached value of the '{@link #getSchedules() <em>Schedules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedules()
	 * @generated
	 * @ordered
	 */
	protected EList<ContainerSchedule> schedules;

	/**
	 * The cached value of the '{@link #getCopyFilePatterns() <em>Copy File Patterns</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopyFilePatterns()
	 * @generated
	 * @ordered
	 */
	protected EList<CopyFilePattern> copyFilePatterns;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SituationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestcasePackage.Literals.SITUATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ContainerSchedule> getSchedules() {
		if (schedules == null) {
			schedules = new EObjectContainmentEList<ContainerSchedule>(ContainerSchedule.class, this, TestcasePackage.SITUATION__SCHEDULES);
		}
		return schedules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CopyFilePattern> getCopyFilePatterns() {
		if (copyFilePatterns == null) {
			copyFilePatterns = new EObjectContainmentEList<CopyFilePattern>(CopyFilePattern.class, this, TestcasePackage.SITUATION__COPY_FILE_PATTERNS);
		}
		return copyFilePatterns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestcasePackage.SITUATION__SCHEDULES:
				return ((InternalEList<?>)getSchedules()).basicRemove(otherEnd, msgs);
			case TestcasePackage.SITUATION__COPY_FILE_PATTERNS:
				return ((InternalEList<?>)getCopyFilePatterns()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestcasePackage.SITUATION__SCHEDULES:
				return getSchedules();
			case TestcasePackage.SITUATION__COPY_FILE_PATTERNS:
				return getCopyFilePatterns();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestcasePackage.SITUATION__SCHEDULES:
				getSchedules().clear();
				getSchedules().addAll((Collection<? extends ContainerSchedule>)newValue);
				return;
			case TestcasePackage.SITUATION__COPY_FILE_PATTERNS:
				getCopyFilePatterns().clear();
				getCopyFilePatterns().addAll((Collection<? extends CopyFilePattern>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestcasePackage.SITUATION__SCHEDULES:
				getSchedules().clear();
				return;
			case TestcasePackage.SITUATION__COPY_FILE_PATTERNS:
				getCopyFilePatterns().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestcasePackage.SITUATION__SCHEDULES:
				return schedules != null && !schedules.isEmpty();
			case TestcasePackage.SITUATION__COPY_FILE_PATTERNS:
				return copyFilePatterns != null && !copyFilePatterns.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SituationImpl
