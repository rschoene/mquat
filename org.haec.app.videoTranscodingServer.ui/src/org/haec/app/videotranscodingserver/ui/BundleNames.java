/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.ui;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class storing the symbolic names of all bundles of org.haec.app.videoTranscodingServer
 * @author René Schöne
 */
public class BundleNames {
	
	private static final String server = "org.haec.app.videoTranscodingServer";
	public static final String[] preparation = new String[] {
		"org.haec.ffmpeg",
		"org.haec.mencoder",
		"org.haec.handbrake",
		"org.haec.app.pip.ffmpeg",
		"org.haec.app.controller.delegating",
	};
	public static final int PREPARATION_FFMPEG = 0;
	public static final int PREPARATION_MENCODER = 1;
	public static final int PREPARATION_HANDBRAKE = 2;
	public static final int PREPARATION_PIP_FFMPEG = 3;
	public static final int PREPARATION_CONTROLLER_DELEGATING = 4;

	public static final String[] scalingAppsByLevel = new String[] {
		"org.haec.app.scaling.ffmpeg",
		"org.haec.app.scaling.mencoder",
		"org.haec.app.scaling.handbrake",
	};
	public static final int SCALING_APP_FFMPEG = 0;
	public static final int SCALING_APP_MENCODER = 1;
	public static final int SCALING_APP_HANDBRAKE = 2;
	/**server*/
	public static String getServer() {
		return server;
	}
	/**server+preparation*/
	public static List<String> getPreparation() {
		List<String> tmp = new ArrayList<String>(1 + preparation.length);
		tmp.add(getServer());
		for(String bName : preparation)
			tmp.add(bName);
		return tmp;
	}
	/**ffmpeg*/
	public static String getFirstScalingApp() {
		return scalingAppsByLevel[SCALING_APP_FFMPEG];
	}
	/**mencoder*/
	public static String getSecondScalingApp() {
		return scalingAppsByLevel[SCALING_APP_MENCODER];
	}
	/**handbrake*/
	public static String getThirdScalingApp() {
		return scalingAppsByLevel[SCALING_APP_HANDBRAKE];
	}
	/**all (server+preparation+scalingApps)*/
	public static List<String> getAllBundleNames() {
		List<String> tmp = new ArrayList<String>(1 + preparation.length + scalingAppsByLevel.length);
		tmp.add(getServer());
		for(String bName : preparation)
			tmp.add(bName);
		for(String bName : scalingAppsByLevel)
			tmp.add(bName);
		return tmp;
	}

}