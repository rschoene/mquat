/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.videotranscodingserver.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.Direction;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructuralModel;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.util.ExecuteResult;
import org.coolsoftware.theatre.energymanager.util.ExecuteResultSerializer;
import org.coolsoftware.theatre.util.CCMUtil;
import org.osgi.framework.BundleContext;

/**
 * Abstract superclass for every request producing starter.
 * @author René Schöne
 */
public abstract class RequestStarter {
	
	Logger log = Logger.getLogger(RequestStarter.class);
	
	private static final String DEBUG_KEY = "org.haec.app.videotranscodingserver.ui.debug";
	private static final String USE_FRAMEWORK_KEY = "org.haec.app.videotranscodingserver.ui.useframework";
	static boolean shallDebug = Boolean.parseBoolean(System.getProperty(DEBUG_KEY, "false"));
	static boolean shallUseFramework = Boolean.parseBoolean(System.getProperty(USE_FRAMEWORK_KEY, "false"));
	protected final BundleContext context;
	
	protected void debug(Object... messages) {
		log.debug(zip(messages));
	}
	
	private String zip(Object[] messages) {
		if(messages.length == 1)
			return String.valueOf(messages[0]);
		StringBuilder sb = new StringBuilder();
		for(Object o : messages) {
			sb.append(o);
		}
		return sb.toString();
	}

	public RequestStarter(BundleContext context) {
		this.context = context;
	}

	protected abstract List<MetaparameterLine> getMetaparams();

	protected abstract List<NfrLine> getNfrs();

	protected abstract List<ParameterLine> getParams();

	protected abstract String getSelectedPortName();

	protected abstract String getCompName();

	protected abstract String getAppName();

	protected abstract String getSelectedOptimizer();

	protected abstract IGlobalEnergyManager getGEM();

	protected String flattenNfr(NfrLine item) {
	//		prop+" "+minMax+": "+boundary
			return item.getPropertyName() + " " + (item.isMin() ? "min" : "max") + ": " + item.getValue();
		}

	protected String flattenMetaparam(MetaparameterLine item) {
		return item.getMetaparameterName() + "=" + item.getValue();
	}

	public ExecuteResult startRequest() {
		// gather request information
		String req = getSelectedOptimizer()+"#"+getAppName()+"#"+getCompName()+"#"+getSelectedPortName()+"#";
		
		// flatten non-functional reqs
		boolean needSep = false;
		for(NfrLine item : getNfrs()) {
			if(needSep) {
				req += "|";
			}
			if(item.isUsed()) {
				req += flattenNfr(item);
				needSep = true;
			}
		}
		req += "#";
		
		// flatten metaparameters
		needSep = false;
		for(MetaparameterLine item : getMetaparams()) {
			if(needSep) {
				req += "|"; //XXX rs: multiple metaparameters not supported yet. Seperation char unknown.
			}
			req += flattenMetaparam(item);
			needSep = true;
		}
		//req = req.substring(0,req.length()-1); //XXX rs: is this line needed?
		
		// extract parameter values
		Object[] paramValues = new Object[getParams().size()];
		for (int i = 0; i < paramValues.length; i++) {
			paramValues[i] = getParams().get(i).getValue();
		}
		// start the optimizer and let it run!
		debug("start optimize of gem");
		ExecuteResult ee = ExecuteResultSerializer.deserialize(getGEM().optimize(req, false, paramValues));
		return ee;
	}
	
	/**
	 * Computes a list of the names of provided ports of the given type in the given app. Only
	 * one of typeName or typeId need to be specified. Id has priority over name, if both are
	 * given.
	 * @param gem the global energy manager to use
	 * @param appName the name of the application
	 * @param typeName the name of the component type, may be <code>null</code> to use typeId
	 * @param typeId the id of the component type, may be <code>null</code> to use typeName
	 * @return a list of names of all ports which are provided by the type
	 */
	public static List<String> getProvidedPorts(IGlobalEnergyManager gem, String appName, String typeName, String typeId) {
		String serializedStructModel = gem.getApplicationModel(appName);
		List<String> result = new ArrayList<String>();
		boolean useId = (typeId != null);
		try {
			StructuralModel swModel = CCMUtil.deserializeSWStructModel(appName, serializedStructModel, false);
			List<SWComponentType> toInspect = new LinkedList<SWComponentType>();
			toInspect.add((SWComponentType) swModel.getRoot());
			while(!toInspect.isEmpty()) {
				SWComponentType currentType = toInspect.remove(0);
				if((!useId || typeId.equals(currentType.getId())) &&
						(useId || typeName.equals(currentType.getName()))) {
					// currentType matched criteria (id or name)
					for(PortType portType : currentType.getPorttypes()) {
						if(portType.getDirection().equals(Direction.PROVIDED)) {
							result.add(portType.getName());
						}
					}
				}
				toInspect.addAll(currentType.getSubtypes());
			}
		} catch (IOException e) {
			System.err.println("Using all ports, because of error: " + e.getMessage());
			return gem.getComponentPorts(appName, typeName);
		}
		return result;
	}

	public interface ValueLine {
		public Object getValue();
		public void setValue(Object value);
	}

	public class ParameterLine implements ValueLine {
		String parameterName;
		String parameterType;
		Object value;
		public ParameterLine(String parameterName, String parameterType) {
			this(parameterName, parameterType, "");
		}
		public ParameterLine(String parameterName, String parameterType, Object value) {
			setParameterName(parameterName);
			setParameterType(parameterType);
			setValue(value);
		}
		public String getParameterName() {
			return parameterName;
		}
		public void setParameterName(String parameterName) {
			this.parameterName = parameterName;
		}
		public String getParameterType() {
			return parameterType;
		}
		public void setParameterType(String parameterType) {
			this.parameterType = parameterType;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		@Override
		public String toString() {
			return getParameterName() + ":" + getParameterType() + " = " + getValue();
		}
	}

	public class MetaparameterLine implements ValueLine {
		String metaparameterName;
		Object value = "";
		public MetaparameterLine(String metaparameterName) {
			setMetaparameterName(metaparameterName);
		}
		public String getMetaparameterName() {
			return metaparameterName;
		}
		public void setMetaparameterName(String metaparameterName) {
			this.metaparameterName = metaparameterName;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		@Override
		public String toString() {
			return getMetaparameterName() + " = " + getValue();
		}
	}

	public class NfrLine implements ValueLine {
		String propertyName;
		boolean min;
		boolean used;
		Object value;
		public NfrLine(String propertyName) {
			this(propertyName, "");
		}
		public NfrLine(String propertyName, Object value) {
			setPropertyName(propertyName);
			setMin(false);
			setUsed(true);
			setValue(value);
		}
		public String getPropertyName() {
			return propertyName;
		}
		public void setPropertyName(String propertyName) {
			this.propertyName = propertyName;
		}
		public boolean isMin() {
			return min;
		}
		public void setMin(boolean min) {
			this.min = min;
		}
		public boolean isUsed() {
			return used;
		}
		public void setUsed(boolean used) {
			this.used = used;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
	}

}
