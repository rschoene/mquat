/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.transcoder.mencoder;

import java.io.File;
import java.util.List;

import org.haec.app.transcoder.AbstractTranscoder;
import org.haec.app.videotranscodingserver.TranscoderTranscode;
import org.haec.apps.util.ErrorState;
import org.haec.mencoder.MencoderUtil;
import org.haec.theatre.api.Benchmark;

/**
 * Implementation of org.haec.app.transcoder.mencoder.TranscoderMencoder
 * @author René Schöne
 */
public class TranscoderMencoder extends AbstractTranscoder implements TranscoderTranscode {

	public TranscoderMencoder() {
		super(MencoderUtil.sharedInstance);
	}

	@Override
	protected int handleProcess(Process startedProcess)
			throws InterruptedException {
		util.printStream(startedProcess.getInputStream());
		return startedProcess.waitFor();
	}

	@Override
	protected void addArguments(List<String> params, File fileVideo,
			String outputVideoCodec, String outputAudioCodec, File out, ErrorState state) {
		params.add(fileVideo.getAbsolutePath());
		state.addHandlingParams(params);
		params.add("-o");
		params.add(out.getAbsolutePath());
		if(outputVideoCodec != null) {
			outputVideoCodec = handleSpecialVideoCodec(params, outputVideoCodec);
		} else {
			outputVideoCodec = "lavc";
		}
		params.add("-ovc");
		params.add(outputVideoCodec);
		if(outputAudioCodec != null) {
			outputAudioCodec = handleSpecialAudioCodec(params, outputAudioCodec);
		} else {
			outputAudioCodec = "copy";
		}
		// ac accepts a list (maybe helpful)
		params.add("-oac");
		params.add(outputAudioCodec);
	}

	private String handleSpecialVideoCodec(List<String> params,
			String outputVideoCodec) {
		switch(outputVideoCodec) {
		case "theora":
			addOrModifyLavcopts(params, "vcodec=libtheora");
			return "lavc";
		case "h264":
			return "x264";
		case "mpeg4":
			addOrModifyLavcopts(params, "vcodec=mpeg4");
			return "lavc";
		}
		return outputVideoCodec;
	}

	private String handleSpecialAudioCodec(List<String> params,
			String outputAudioCodec) {
		switch(outputAudioCodec) {
		case "mp3":
			// if this does not work, we need to ignore it
			return "mp3lame";
		case "ac3":
		case "aac":
			addOrModifyLavcopts(params, "acodec="+outputAudioCodec);
			return "lavc";
		}
		return outputAudioCodec;
	}

	protected void addOrModifyLavcopts(List<String> params, String value) {
		addOrModify(params, "-lavcopts", value, ":");
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.transcoder.mencoder.TranscoderMencoder";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new TranscoderMencoderBenchmark(fac);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.transcoder.mencoder";
	}

}
