/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.monitor.cambrionix;

import org.apache.log4j.Logger;
import org.haec.theatre.api.BenchmarkData;
import org.haec.theatre.monitor.AbstractMonitor;
import org.haec.theatre.monitor.RemoteMonitor;
import org.haec.theatre.powermanager.cambrionix.CambrionixPowerManager;
import org.osgi.service.component.ComponentContext;

import cambrionix.api.CambrionixException;
import cambrionix.api.CambrionixPort;

/**
 * Monitor using the cambrionix api to measure consumed energy of a host.
 * @author René Schöne
 */
public class ServerEnergyMonitor extends AbstractMonitor implements RemoteMonitor {
	
	public static final String SERVER_ENERGY = "server_energy";
	private CambrionixPowerManager cpm;
	
	protected Logger log = Logger.getLogger(ServerEnergyMonitor.class);
	protected Collector collector;

	private String hostIp;
	protected double result = 0;
	
	class ServerEnergyCollector implements Collector {

		private CambrionixPort port;
		private long totalCurrent = 0;
		protected boolean running = true;
		private long startMillis;
		private long stopMillis;
		
		public ServerEnergyCollector(CambrionixPort port) {
			if(port == null) {
				throw new NullPointerException("Got null for port.");
			}
			this.port = port;
		}
		
		/* (non-Javadoc)
		 * @see org.haec.monitor.cambrionix.Collector#getTotalCurrent()
		 */
		@Override
		public long getTotalCurrent() {
			return totalCurrent;
		}
		
		/* (non-Javadoc)
		 * @see org.haec.monitor.cambrionix.Collector#getTotalMillis()
		 */
		@Override
		public long getTotalMillis() {
			return stopMillis - startMillis;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			startMillis = System.currentTimeMillis();
			try {
				while(running) {
					totalCurrent += port.getDrawnCurrent();
					Thread.sleep(100);
				}
			} catch (InterruptedException e) {
				log.error("Was interupted.");
			}
		}
		
		/* (non-Javadoc)
		 * @see org.haec.monitor.cambrionix.Collector#stop()
		 */
		@Override
		public void stop() {
			running = false;
			stopMillis = System.currentTimeMillis();
		}
		
	}
	
	public ServerEnergyMonitor() {
		this(SERVER_ENERGY, true);
	}

	public ServerEnergyMonitor(String propertyName, boolean registerRemotely) {
		super(propertyName, registerRemotely);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#collectBefore()
	 */
	@Override
	public void collectBefore() {
		if(cpm == null) {
			log.error("Cambrionix not available. Maybe not connected?");
			return;
		}
		collector = getCollector(cpm);
		new Thread(collector).start();
	}

	protected Collector getCollector(CambrionixPowerManager cpm) {
		CambrionixPort port = cpm.getPort(hostIp);
		return new ServerEnergyCollector(port);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#collectAfter()
	 */
	@Override
	public void collectAfter(BenchmarkData input, Object output) {
		if(collector == null) {
			log.error("Collector and Cambrionix not available. Maybe not connected?");
			return;
		}
		collector.stop();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#compute()
	 */
	@Override
	public void compute() {
		result = 0;
		if(collector == null) {
			log.error("Collector and Cambrionix not available. Maybe not connected?");
			return;
		}
		// 100 := 1000 (milliseconds to seconds) / 10 (10 measurements per second)
		result = collector.getTotalCurrent() * 1.0 / (collector.getTotalMillis() / 100.0);
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.Monitor#isAvailable()
	 */
	@Override
	public boolean isAvailable() {
		return cpm != null;
	}
	
	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.AbstractMonitor#getValue()
	 */
	@Override
	protected Object getValue() {
		return result;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.monitor.RemoteMonitor#setHostIp(java.lang.String)
	 */
	@Override
	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}
	
	protected void activate(ComponentContext ctx) {
		super.activate(ctx);
		try {
			cpm = new CambrionixPowerManager();
		} catch (CambrionixException e) {
			log.fatal("Could not create cambrionix power manager", e);
			cpm = null;
		}
	};

}
