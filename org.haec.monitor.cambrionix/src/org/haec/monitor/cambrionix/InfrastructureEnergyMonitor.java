/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.monitor.cambrionix;

import org.haec.theatre.powermanager.cambrionix.CambrionixPowerManager;

import cambrionix.api.CambrionixException;
import cambrionix.api.CambrionixPort;

/**
 * 
 * @author René Schöne
 */
public class InfrastructureEnergyMonitor extends ServerEnergyMonitor {

	public static final String INFRASTRUCTURE_ENERGY = "infrastructure_energy";

	class InfrastructureEnergyCollector implements Collector {

		private long startMillis;
		private boolean running = true;
		private long totalCurrent;
		private CambrionixPowerManager cpm;
		private long stopMillis;
		

		public InfrastructureEnergyCollector(CambrionixPowerManager cpm) {
			this.cpm = cpm;
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			startMillis = System.currentTimeMillis();
			try {
				while(running) {
					// assume cambrionix with ports from 1 to 8
					for (int portId = 1; portId <= 8; portId++) {
						CambrionixPort port = cpm.getPort(portId);
						if(port != null) {
							totalCurrent += port.getDrawnCurrent();
						}
					}
					Thread.sleep(100);
				}
			} catch (InterruptedException | CambrionixException e) {
				log.error("Strange error.", e);
			}
		}

		/* (non-Javadoc)
		 * @see org.haec.monitor.cambrionix.Collector#getTotalCurrent()
		 */
		@Override
		public long getTotalCurrent() {
			System.out.println("current:" + totalCurrent);
			return totalCurrent;
		}

		/* (non-Javadoc)
		 * @see org.haec.monitor.cambrionix.Collector#getTotalMillis()
		 */
		@Override
		public long getTotalMillis() {
			System.out.println("millis:" + (stopMillis - startMillis));
			return stopMillis - startMillis;
		}

		/* (non-Javadoc)
		 * @see org.haec.monitor.cambrionix.Collector#stop()
		 */
		@Override
		public void stop() {
			stopMillis = System.currentTimeMillis();
			running = false;
		}
		
	}
	
	public InfrastructureEnergyMonitor() {
		super(INFRASTRUCTURE_ENERGY, true);
	}

	/* (non-Javadoc)
	 * @see org.haec.monitor.cambrionix.ServerEnergyMonitor#getCollector(org.haec.theatre.powermanager.cambrionix.CambrionixPowerManager)
	 */
	@Override
	protected Collector getCollector(CambrionixPowerManager cpm) {
		return new InfrastructureEnergyCollector(cpm);
	}
	
}
