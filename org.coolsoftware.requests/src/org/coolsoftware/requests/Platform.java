/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests;

import org.coolsoftware.coolcomponents.ccm.variant.VariantModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Platform</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.requests.Platform#getHwmodel <em>Hwmodel</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.requests.RequestsPackage#getPlatform()
 * @model
 * @generated
 */
public interface Platform extends EObject {
	/**
	 * Returns the value of the '<em><b>Hwmodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hwmodel</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hwmodel</em>' reference.
	 * @see #setHwmodel(VariantModel)
	 * @see org.coolsoftware.requests.RequestsPackage#getPlatform_Hwmodel()
	 * @model required="true"
	 * @generated
	 */
	VariantModel getHwmodel();

	/**
	 * Sets the value of the '{@link org.coolsoftware.requests.Platform#getHwmodel <em>Hwmodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hwmodel</em>' reference.
	 * @see #getHwmodel()
	 * @generated
	 */
	void setHwmodel(VariantModel value);

} // Platform
