/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests;

import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;

import org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment;
import org.coolsoftware.ecl.PropertyRequirementClause;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.requests.Request#getTarget <em>Target</em>}</li>
 *   <li>{@link org.coolsoftware.requests.Request#getComponent <em>Component</em>}</li>
 *   <li>{@link org.coolsoftware.requests.Request#getImport <em>Import</em>}</li>
 *   <li>{@link org.coolsoftware.requests.Request#getReqs <em>Reqs</em>}</li>
 *   <li>{@link org.coolsoftware.requests.Request#getHardware <em>Hardware</em>}</li>
 *   <li>{@link org.coolsoftware.requests.Request#getMetaParamValues <em>Meta Param Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.requests.RequestsPackage#getRequest()
 * @model
 * @generated
 */
public interface Request extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(PortType)
	 * @see org.coolsoftware.requests.RequestsPackage#getRequest_Target()
	 * @model required="true"
	 * @generated
	 */
	PortType getTarget();

	/**
	 * Sets the value of the '{@link org.coolsoftware.requests.Request#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(PortType value);

	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(SWComponentType)
	 * @see org.coolsoftware.requests.RequestsPackage#getRequest_Component()
	 * @model required="true"
	 * @generated
	 */
	SWComponentType getComponent();

	/**
	 * Sets the value of the '{@link org.coolsoftware.requests.Request#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(SWComponentType value);

	/**
	 * Returns the value of the '<em><b>Import</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import</em>' containment reference.
	 * @see #setImport(Import)
	 * @see org.coolsoftware.requests.RequestsPackage#getRequest_Import()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Import getImport();

	/**
	 * Sets the value of the '{@link org.coolsoftware.requests.Request#getImport <em>Import</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import</em>' containment reference.
	 * @see #getImport()
	 * @generated
	 */
	void setImport(Import value);

	/**
	 * Returns the value of the '<em><b>Reqs</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.ecl.PropertyRequirementClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reqs</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reqs</em>' containment reference list.
	 * @see org.coolsoftware.requests.RequestsPackage#getRequest_Reqs()
	 * @model containment="true"
	 * @generated
	 */
	EList<PropertyRequirementClause> getReqs();

	/**
	 * Returns the value of the '<em><b>Hardware</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hardware</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hardware</em>' containment reference.
	 * @see #setHardware(Platform)
	 * @see org.coolsoftware.requests.RequestsPackage#getRequest_Hardware()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Platform getHardware();

	/**
	 * Sets the value of the '{@link org.coolsoftware.requests.Request#getHardware <em>Hardware</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hardware</em>' containment reference.
	 * @see #getHardware()
	 * @generated
	 */
	void setHardware(Platform value);

	/**
	 * Returns the value of the '<em><b>Meta Param Values</b></em>' containment reference list.
	 * The list contents are of type {@link org.coolsoftware.requests.MetaParamValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Meta Param Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Meta Param Values</em>' containment reference list.
	 * @see org.coolsoftware.requests.RequestsPackage#getRequest_MetaParamValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetaParamValue> getMetaParamValues();

} // Request
