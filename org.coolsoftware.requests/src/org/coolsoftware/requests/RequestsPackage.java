/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.requests.RequestsFactory
 * @model kind="package"
 * @generated
 */
public interface RequestsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "requests";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.cool-software.org/requests";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.coolsoftware";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RequestsPackage eINSTANCE = org.coolsoftware.requests.impl.RequestsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.coolsoftware.requests.impl.RequestImpl <em>Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.requests.impl.RequestImpl
	 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getRequest()
	 * @generated
	 */
	int REQUEST = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Import</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__IMPORT = 2;

	/**
	 * The feature id for the '<em><b>Reqs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__REQS = 3;

	/**
	 * The feature id for the '<em><b>Hardware</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__HARDWARE = 4;

	/**
	 * The feature id for the '<em><b>Meta Param Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__META_PARAM_VALUES = 5;

	/**
	 * The number of structural features of the '<em>Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.coolsoftware.requests.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.requests.impl.ImportImpl
	 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 1;

	/**
	 * The feature id for the '<em><b>Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__MODEL = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 1;


	/**
	 * The meta object id for the '{@link org.coolsoftware.requests.impl.PlatformImpl <em>Platform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.requests.impl.PlatformImpl
	 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getPlatform()
	 * @generated
	 */
	int PLATFORM = 2;

	/**
	 * The feature id for the '<em><b>Hwmodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM__HWMODEL = 0;

	/**
	 * The number of structural features of the '<em>Platform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_FEATURE_COUNT = 1;


	/**
	 * The meta object id for the '{@link org.coolsoftware.requests.impl.MetaParamValueImpl <em>Meta Param Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.coolsoftware.requests.impl.MetaParamValueImpl
	 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getMetaParamValue()
	 * @generated
	 */
	int META_PARAM_VALUE = 3;

	/**
	 * The feature id for the '<em><b>Metaparam</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_PARAM_VALUE__METAPARAM = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_PARAM_VALUE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Meta Param Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_PARAM_VALUE_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link org.coolsoftware.requests.Request <em>Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Request</em>'.
	 * @see org.coolsoftware.requests.Request
	 * @generated
	 */
	EClass getRequest();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.requests.Request#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.coolsoftware.requests.Request#getTarget()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Target();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.requests.Request#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see org.coolsoftware.requests.Request#getComponent()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Component();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.requests.Request#getImport <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import</em>'.
	 * @see org.coolsoftware.requests.Request#getImport()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Import();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.requests.Request#getReqs <em>Reqs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reqs</em>'.
	 * @see org.coolsoftware.requests.Request#getReqs()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Reqs();

	/**
	 * Returns the meta object for the containment reference '{@link org.coolsoftware.requests.Request#getHardware <em>Hardware</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Hardware</em>'.
	 * @see org.coolsoftware.requests.Request#getHardware()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Hardware();

	/**
	 * Returns the meta object for the containment reference list '{@link org.coolsoftware.requests.Request#getMetaParamValues <em>Meta Param Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Meta Param Values</em>'.
	 * @see org.coolsoftware.requests.Request#getMetaParamValues()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_MetaParamValues();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.requests.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see org.coolsoftware.requests.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.requests.Import#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Model</em>'.
	 * @see org.coolsoftware.requests.Import#getModel()
	 * @see #getImport()
	 * @generated
	 */
	EReference getImport_Model();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.requests.Platform <em>Platform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Platform</em>'.
	 * @see org.coolsoftware.requests.Platform
	 * @generated
	 */
	EClass getPlatform();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.requests.Platform#getHwmodel <em>Hwmodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hwmodel</em>'.
	 * @see org.coolsoftware.requests.Platform#getHwmodel()
	 * @see #getPlatform()
	 * @generated
	 */
	EReference getPlatform_Hwmodel();

	/**
	 * Returns the meta object for class '{@link org.coolsoftware.requests.MetaParamValue <em>Meta Param Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Param Value</em>'.
	 * @see org.coolsoftware.requests.MetaParamValue
	 * @generated
	 */
	EClass getMetaParamValue();

	/**
	 * Returns the meta object for the reference '{@link org.coolsoftware.requests.MetaParamValue#getMetaparam <em>Metaparam</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metaparam</em>'.
	 * @see org.coolsoftware.requests.MetaParamValue#getMetaparam()
	 * @see #getMetaParamValue()
	 * @generated
	 */
	EReference getMetaParamValue_Metaparam();

	/**
	 * Returns the meta object for the attribute '{@link org.coolsoftware.requests.MetaParamValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.coolsoftware.requests.MetaParamValue#getValue()
	 * @see #getMetaParamValue()
	 * @generated
	 */
	EAttribute getMetaParamValue_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RequestsFactory getRequestsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.coolsoftware.requests.impl.RequestImpl <em>Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.requests.impl.RequestImpl
		 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getRequest()
		 * @generated
		 */
		EClass REQUEST = eINSTANCE.getRequest();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__TARGET = eINSTANCE.getRequest_Target();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__COMPONENT = eINSTANCE.getRequest_Component();

		/**
		 * The meta object literal for the '<em><b>Import</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__IMPORT = eINSTANCE.getRequest_Import();

		/**
		 * The meta object literal for the '<em><b>Reqs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__REQS = eINSTANCE.getRequest_Reqs();

		/**
		 * The meta object literal for the '<em><b>Hardware</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__HARDWARE = eINSTANCE.getRequest_Hardware();

		/**
		 * The meta object literal for the '<em><b>Meta Param Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__META_PARAM_VALUES = eINSTANCE.getRequest_MetaParamValues();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.requests.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.requests.impl.ImportImpl
		 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMPORT__MODEL = eINSTANCE.getImport_Model();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.requests.impl.PlatformImpl <em>Platform</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.requests.impl.PlatformImpl
		 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getPlatform()
		 * @generated
		 */
		EClass PLATFORM = eINSTANCE.getPlatform();

		/**
		 * The meta object literal for the '<em><b>Hwmodel</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLATFORM__HWMODEL = eINSTANCE.getPlatform_Hwmodel();

		/**
		 * The meta object literal for the '{@link org.coolsoftware.requests.impl.MetaParamValueImpl <em>Meta Param Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.coolsoftware.requests.impl.MetaParamValueImpl
		 * @see org.coolsoftware.requests.impl.RequestsPackageImpl#getMetaParamValue()
		 * @generated
		 */
		EClass META_PARAM_VALUE = eINSTANCE.getMetaParamValue();

		/**
		 * The meta object literal for the '<em><b>Metaparam</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_PARAM_VALUE__METAPARAM = eINSTANCE.getMetaParamValue_Metaparam();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute META_PARAM_VALUE__VALUE = eINSTANCE.getMetaParamValue_Value();

	}

} //RequestsPackage
