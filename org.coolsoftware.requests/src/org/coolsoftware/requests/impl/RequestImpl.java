/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests.impl;

import java.util.Collection;
import org.coolsoftware.coolcomponents.ccm.structure.PortType;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;

import org.coolsoftware.coolcomponents.expressions.variables.VariableAssignment;
import org.coolsoftware.ecl.PropertyRequirementClause;

import org.coolsoftware.requests.Import;
import org.coolsoftware.requests.MetaParamValue;
import org.coolsoftware.requests.Platform;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.RequestsPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Request</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.requests.impl.RequestImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.coolsoftware.requests.impl.RequestImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link org.coolsoftware.requests.impl.RequestImpl#getImport <em>Import</em>}</li>
 *   <li>{@link org.coolsoftware.requests.impl.RequestImpl#getReqs <em>Reqs</em>}</li>
 *   <li>{@link org.coolsoftware.requests.impl.RequestImpl#getHardware <em>Hardware</em>}</li>
 *   <li>{@link org.coolsoftware.requests.impl.RequestImpl#getMetaParamValues <em>Meta Param Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RequestImpl extends EObjectImpl implements Request {
	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected PortType target;

	/**
	 * The cached value of the '{@link #getComponent() <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponent()
	 * @generated
	 * @ordered
	 */
	protected SWComponentType component;

	/**
	 * The cached value of the '{@link #getImport() <em>Import</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImport()
	 * @generated
	 * @ordered
	 */
	protected Import import_;

	/**
	 * The cached value of the '{@link #getReqs() <em>Reqs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqs()
	 * @generated
	 * @ordered
	 */
	protected EList<PropertyRequirementClause> reqs;

	/**
	 * The cached value of the '{@link #getHardware() <em>Hardware</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHardware()
	 * @generated
	 * @ordered
	 */
	protected Platform hardware;

	/**
	 * The cached value of the '{@link #getMetaParamValues() <em>Meta Param Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaParamValues()
	 * @generated
	 * @ordered
	 */
	protected EList<MetaParamValue> metaParamValues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequestsPackage.Literals.REQUEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortType getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (PortType)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RequestsPackage.REQUEST__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortType basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(PortType newTarget) {
		PortType oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequestsPackage.REQUEST__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponentType getComponent() {
		if (component != null && component.eIsProxy()) {
			InternalEObject oldComponent = (InternalEObject)component;
			component = (SWComponentType)eResolveProxy(oldComponent);
			if (component != oldComponent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RequestsPackage.REQUEST__COMPONENT, oldComponent, component));
			}
		}
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SWComponentType basicGetComponent() {
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent(SWComponentType newComponent) {
		SWComponentType oldComponent = component;
		component = newComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequestsPackage.REQUEST__COMPONENT, oldComponent, component));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import getImport() {
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImport(Import newImport, NotificationChain msgs) {
		Import oldImport = import_;
		import_ = newImport;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RequestsPackage.REQUEST__IMPORT, oldImport, newImport);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImport(Import newImport) {
		if (newImport != import_) {
			NotificationChain msgs = null;
			if (import_ != null)
				msgs = ((InternalEObject)import_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RequestsPackage.REQUEST__IMPORT, null, msgs);
			if (newImport != null)
				msgs = ((InternalEObject)newImport).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RequestsPackage.REQUEST__IMPORT, null, msgs);
			msgs = basicSetImport(newImport, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequestsPackage.REQUEST__IMPORT, newImport, newImport));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PropertyRequirementClause> getReqs() {
		if (reqs == null) {
			reqs = new EObjectContainmentEList<PropertyRequirementClause>(PropertyRequirementClause.class, this, RequestsPackage.REQUEST__REQS);
		}
		return reqs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Platform getHardware() {
		return hardware;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHardware(Platform newHardware, NotificationChain msgs) {
		Platform oldHardware = hardware;
		hardware = newHardware;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RequestsPackage.REQUEST__HARDWARE, oldHardware, newHardware);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHardware(Platform newHardware) {
		if (newHardware != hardware) {
			NotificationChain msgs = null;
			if (hardware != null)
				msgs = ((InternalEObject)hardware).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RequestsPackage.REQUEST__HARDWARE, null, msgs);
			if (newHardware != null)
				msgs = ((InternalEObject)newHardware).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RequestsPackage.REQUEST__HARDWARE, null, msgs);
			msgs = basicSetHardware(newHardware, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RequestsPackage.REQUEST__HARDWARE, newHardware, newHardware));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MetaParamValue> getMetaParamValues() {
		if (metaParamValues == null) {
			metaParamValues = new EObjectContainmentEList<MetaParamValue>(MetaParamValue.class, this, RequestsPackage.REQUEST__META_PARAM_VALUES);
		}
		return metaParamValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case RequestsPackage.REQUEST__IMPORT:
				return basicSetImport(null, msgs);
			case RequestsPackage.REQUEST__REQS:
				return ((InternalEList<?>)getReqs()).basicRemove(otherEnd, msgs);
			case RequestsPackage.REQUEST__HARDWARE:
				return basicSetHardware(null, msgs);
			case RequestsPackage.REQUEST__META_PARAM_VALUES:
				return ((InternalEList<?>)getMetaParamValues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RequestsPackage.REQUEST__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case RequestsPackage.REQUEST__COMPONENT:
				if (resolve) return getComponent();
				return basicGetComponent();
			case RequestsPackage.REQUEST__IMPORT:
				return getImport();
			case RequestsPackage.REQUEST__REQS:
				return getReqs();
			case RequestsPackage.REQUEST__HARDWARE:
				return getHardware();
			case RequestsPackage.REQUEST__META_PARAM_VALUES:
				return getMetaParamValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RequestsPackage.REQUEST__TARGET:
				setTarget((PortType)newValue);
				return;
			case RequestsPackage.REQUEST__COMPONENT:
				setComponent((SWComponentType)newValue);
				return;
			case RequestsPackage.REQUEST__IMPORT:
				setImport((Import)newValue);
				return;
			case RequestsPackage.REQUEST__REQS:
				getReqs().clear();
				getReqs().addAll((Collection<? extends PropertyRequirementClause>)newValue);
				return;
			case RequestsPackage.REQUEST__HARDWARE:
				setHardware((Platform)newValue);
				return;
			case RequestsPackage.REQUEST__META_PARAM_VALUES:
				getMetaParamValues().clear();
				getMetaParamValues().addAll((Collection<? extends MetaParamValue>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RequestsPackage.REQUEST__TARGET:
				setTarget((PortType)null);
				return;
			case RequestsPackage.REQUEST__COMPONENT:
				setComponent((SWComponentType)null);
				return;
			case RequestsPackage.REQUEST__IMPORT:
				setImport((Import)null);
				return;
			case RequestsPackage.REQUEST__REQS:
				getReqs().clear();
				return;
			case RequestsPackage.REQUEST__HARDWARE:
				setHardware((Platform)null);
				return;
			case RequestsPackage.REQUEST__META_PARAM_VALUES:
				getMetaParamValues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RequestsPackage.REQUEST__TARGET:
				return target != null;
			case RequestsPackage.REQUEST__COMPONENT:
				return component != null;
			case RequestsPackage.REQUEST__IMPORT:
				return import_ != null;
			case RequestsPackage.REQUEST__REQS:
				return reqs != null && !reqs.isEmpty();
			case RequestsPackage.REQUEST__HARDWARE:
				return hardware != null;
			case RequestsPackage.REQUEST__META_PARAM_VALUES:
				return metaParamValues != null && !metaParamValues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RequestImpl
