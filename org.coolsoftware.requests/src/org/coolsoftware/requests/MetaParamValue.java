/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.requests;

import org.coolsoftware.coolcomponents.ccm.structure.Parameter;
import org.coolsoftware.ecl.Metaparameter;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Param Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.requests.MetaParamValue#getMetaparam <em>Metaparam</em>}</li>
 *   <li>{@link org.coolsoftware.requests.MetaParamValue#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.requests.RequestsPackage#getMetaParamValue()
 * @model
 * @generated
 */
public interface MetaParamValue extends EObject {
	/**
	 * Returns the value of the '<em><b>Metaparam</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metaparam</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metaparam</em>' reference.
	 * @see #setMetaparam(Parameter)
	 * @see org.coolsoftware.requests.RequestsPackage#getMetaParamValue_Metaparam()
	 * @model required="true"
	 * @generated
	 */
	Parameter getMetaparam();

	/**
	 * Sets the value of the '{@link org.coolsoftware.requests.MetaParamValue#getMetaparam <em>Metaparam</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metaparam</em>' reference.
	 * @see #getMetaparam()
	 * @generated
	 */
	void setMetaparam(Parameter value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see org.coolsoftware.requests.RequestsPackage#getMetaParamValue_Value()
	 * @model required="true"
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link org.coolsoftware.requests.MetaParamValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

} // MetaParamValue
