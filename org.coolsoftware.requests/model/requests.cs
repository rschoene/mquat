SYNTAXDEF request
FOR <http://www.cool-software.org/requests>
START Request

IMPORTS {
	ecl : <http://www.cool-software.org/ecl> 
		<org.coolsoftware.ecl/metamodel/ecl.genmodel>
		WITH SYNTAX ecl <../../org.coolsoftware.ecl/metamodel/ecl.cs>		
}

OPTIONS {
	reloadGeneratorModel = "true";
	//overrideClasspath = "false";
	//overrideManifest = "false";
	//overridePluginXML = "false";
	additionalLibraries = "lib/lpsolve55j.jar,lib/sat4j-pb.jar,./bin";
	additionalDependencies = "org.coolsoftware.coolcomponents.expressions.interpreter,org.eclipse.jface,org.eclipse.emf.ecore.xmi";
	additionalExports = "org.coolsoftware.requests.resource.request.analysis.util";
	disableDebugSupport = "true";
	disableLaunchSupport = "true";
}

TOKENS {
	DEFINE SL_COMMENT				$ '--'(~('\n'|'\r'|'\uffff'))* $ COLLECT IN comments;
	DEFINE ML_COMMENT				$ '/*'.*'*/'$ COLLECT IN comments;
	
	DEFINE FILE_NAME
		$ '[' (('A' .. 'Z' | 'a'..'z') ':' '/')? ('.' | '/') ('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' | '/' | '.' | ' ')+ ']' $;
		
	DEFINE NUMBER $ '0' | ('1'..'9')('0'..'9')+ $;
}

TOKENSTYLES {
	"ML_COMMENT" COLOR #008000, ITALIC;
	"SL_COMMENT" COLOR #008000, ITALIC;
}

RULES {
	// syntax definition for class 'StartMetaClass'
	Request   ::= import !0 hardware !0 "call" #1 component[]"."target[] "expecting" #1 "{" !1 metaParamValues* !1 reqs* !0 "}";
	
	MetaParamValue ::= metaparam[TEXT] "=" value[NUMBER];
	
	Import    ::= "import" #0 "ccm" model[FILE_NAME];
	
	Platform  ::= "target" #0 "platform" #0 hwmodel[FILE_NAME];
	
}