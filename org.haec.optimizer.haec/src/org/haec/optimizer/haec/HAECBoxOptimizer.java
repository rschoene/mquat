/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.optimizer.haec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.coolsoftware.coolcomponents.ccm.structure.SWComponentType;
import org.coolsoftware.coolcomponents.ccm.structure.StructureFactory;
import org.coolsoftware.ecl.EclFile;
import org.coolsoftware.requests.Request;
import org.coolsoftware.requests.RequestsFactory;
import org.coolsoftware.theatre.energymanager.IGlobalEnergyManager;
import org.coolsoftware.theatre.energymanager.Optimizer;
import org.haec.theatre.utils.CollectionsUtil;
import org.haec.theatre.utils.FileProxy;
import org.haec.theatre.utils.ResourceResolver;

/**
 * Hard-coded implementation of an optimizer for the HAEC-Box.
 * @author René Schöne
 */
public class HAECBoxOptimizer implements Optimizer {
	
	private static final String cpScalingFileComp = "CopyScalingFile1";
	private static final String cpScalingFileImpl = "org.haec.app.copyFile.scaling.CopyFileScalingImpl";
	
	private static final String cpTranscoderFileComp = "CopyTranscoderFile1";
	private static final String cpTranscoderFileImpl = "org.haec.app.copyFile.transcoder.CopyFileTranscoderImpl";

	private static final String scalingComp = "Scaling";
	private static final String scalingImpl = "org.haec.app.scaling.handbrake.ScalingHandbrake";

	private static final String transcoderComp = "Transcoder";
	private static final String transcoderImpl = "org.haec.app.transcoder.handbrake.TranscoderHandbrake";

	private static final List<String> copyFileTypes = new ArrayList<String>() {
		{ add(cpScalingFileComp); add(cpTranscoderFileComp); }
	};

	private Logger log = Logger.getLogger(HAECBoxOptimizer.class);
	private String problemName;
	private String profile;
	private ResourceResolver resolver;
	
	public HAECBoxOptimizer() {
		
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.Optimizer#optimize(java.lang.Object[])
	 */
	@Override
	public Mapping optimize(Object[] args) {
		try {
			log.debug("Starting simple optimization");
			Request req = (Request)args[0];
			@SuppressWarnings("unchecked") Map<String, EclFile> eclFiles = (Map<String, EclFile>)args[1];
			IGlobalEnergyManager gem = (IGlobalEnergyManager)args[2];
			@SuppressWarnings("unchecked") Map<String, Object> options = (Map<String, Object>) args[3];
			Boolean verbose = CollectionsUtil.get(options, OPTION_VERBOSE, false);
			String appName = (String) options.get(OPTION_APPNAME);
			Map<String, List<String>> fileNameMap = CollectionsUtil.get(
					options, OPTION_RESOURCENAMES, new HashMap<String, List<String>>());
			problemName = CollectionsUtil.get(options, OPTION_PROBLEMNAME, appName.replace(',', '_'));
			profile = CollectionsUtil.get(options, OPTION_PROFILE, PROFILE_NORMAL);
			return getMapping(gem, appName, req, eclFiles, fileNameMap);
		} catch(Exception e) {
			log.warn("Exception on optimize", e);
		}
		return null;
	}

	/**
	 * @param gem 
	 * @param appName 
	 * @param req
	 * @param eclFiles
	 * @param fileNameMap 
	 * @return
	 */
	private Mapping getMapping(IGlobalEnergyManager gem, String appName, Request req,
			Map<String, EclFile> eclFiles, Map<String, List<String>> fileNameMap) {
//		Map<String, Map<String, String>> map = gem.getContractsForApp(appName, fileNameMap);
//		logMap(map);
//		Set<String> fileHosts = new HashSet<>();
		boolean scaling = false;
//		// check requested component
		switch(req.getComponent().getName()) {
		case scalingComp: scaling = true; break;
		case transcoderComp: scaling = false; break;
		default: return error("Only scaling and transcoder are supported.");
		}
//		for(Entry<String, Map<String, String>> e : map.entrySet()) {
//			String compName = e.getKey();
//			Set<String> valueKeySet = e.getValue().keySet();
//			log.debug(compName + " -> " + valueKeySet);
//			if((scaling && compName.equals(cpScalingFileComp)) ||
//					(!scaling && compName.equals(cpTranscoderFileComp))) {
//				fileHosts.addAll(valueKeySet);
//			}
//		}
		String filename = fileNameMap.values().iterator().next().get(0);
		List<String> fileHosts = getResolvedUris(filename);
		// take one host (random, because of set)
		String host;
		if(!fileHosts.isEmpty()) {
			host = fileHosts.iterator().next();
		} else {
			return error("No providing hosts found.");
		}
		Mapping result = new Mapping();
		if(scaling) {
			result.put(cpScalingFileImpl, host);
			result.put(scalingImpl, host);
		} else {
			result.put(cpTranscoderFileImpl, host);
			result.put(transcoderImpl, host);
		}
		return result;
	}

	private void logMap(Map<String, Map<String, String>> map) {
		log.debug(map);
	}

	protected Mapping error(String message) {
		return new Mapping(message);
	}
	
	protected List<String> getResolvedUris(String filename) {
		List<FileProxy> list = resolveResource(filename);
		List<String> result = new ArrayList<>();
		for(FileProxy fp : list) {
			if(fp == null) { continue; }
			String host = fp.getLemUri().getHost();
			result.add(host != null ? host : fp.getLemUri().getPath());
		}
		log.debug("Recomputed list of hosts for " + filename + " and got:" + result);
		return result;
	}

	private List<FileProxy> resolveResource(String name) {
		try {
			List<FileProxy> list = resolver.resolve(name);
			log.debug("got: " + list);
			return list;
		} catch (Exception e) {
			log.warn(resolver.getClass() + " threw an exception.", e);
		}
		log.error("The resource resolver returned no suitable result. Returning unresolved FileProxy.");
		return Arrays.asList(new FileProxy(name));
	}
	
	public static void main(String[] args) {
		BasicConfigurator.configure();
		HAECBoxOptimizer hbo = new HAECBoxOptimizer() {
			/* (non-Javadoc)
			 * @see org.haec.optimizer.haec.HAECBoxOptimizer#getResolvedUris(java.lang.String)
			 */
			@Override
			protected List<String> getResolvedUris(String filename) {
				System.out.println("gru (" + filename + ")");
				return Arrays.asList("192.168.0.237", "192.168.0.238");
			}
		};
		SWComponentType comp = StructureFactory.eINSTANCE.createSWComponentType();
		comp.setName(scalingComp);
		Request req = RequestsFactory.eINSTANCE.createRequest();
		req.setComponent(comp);
		Map<String, List<String>> fileNameMap = new HashMap<>();
		List<String> fileList = new ArrayList<>();
		fileList.add("name_of_file");
		fileNameMap.put(cpScalingFileComp, fileList);
		Mapping result = hbo.getMapping(null, "app", req, null, fileNameMap);
		System.out.println(result);
	}

	/* (non-Javadoc)
	 * @see org.coolsoftware.theatre.energymanager.Optimizer#getId()
	 */
	@Override
	public String getId() {
		return "org.haec.optimizer.haec";
	}
	
	protected void setResourceResolver(ResourceResolver rr) {
		this.resolver = rr;
	}
	
	protected void unsetResourceResolver(ResourceResolver rr) {
		this.resolver = null;
	}

}
