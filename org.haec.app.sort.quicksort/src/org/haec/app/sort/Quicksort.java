/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.haec.app.sort;

import org.haec.theatre.api.Benchmark;
import org.haec.theatre.api.TaskBasedImpl;

/**
 * Implementation of Quicksort
 * @author Sebastian Götz
 */
public class Quicksort extends TaskBasedImpl {
	
	public Object sort(Object o) {
		Integer[] list;
		if(o instanceof Integer) {
			// we got invoked by the LEM. create list first.
			list = invoke(Integer[].class, "ListGen", "generate", (Integer) o);
		}
		else {
			list = (Integer[]) o;
		}
		return sort0(list);
	}
	
	// metaparameter: list_size = in.length
	public <T extends Comparable<T>> T[] sort0(T[] in) {
		T[] a = quicksort(in, 0, in.length - 1);
		return a;
	}

	public <T extends Comparable<T>> T[] quicksort(T[] a, int lo, int hi) {
		// lo is the lower index, hi is the upper index
		// of the region of array a that is to be sorted
		int i = lo, j = hi;
		T h;

		// comparison element x
		T x = a[(lo + hi) / 2];

		// partition
		do {
			while (a[i].compareTo(x) > 0)
				i++;
			while (a[j].compareTo(x) < 0)
				j--;
			if (i <= j) {
				h = a[i];
				a[i] = a[j];
				a[j] = h;
				i++;
				j--;
			}
		} while (i <= j);

		// recursion
		if (lo < j)
			quicksort(a, lo, j);
		if (i < hi)
			quicksort(a, i, hi);

		return a;
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getApplicationName()
	 */
	@Override
	public String getApplicationName() {
		return "org.haec.app.composedsort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getComponentType()
	 */
	@Override
	public String getComponentType() {
		return "Sort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getVariantName()
	 */
	@Override
	public String getVariantName() {
		return "org.haec.app.sort.Quicksort";
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getBenchmark()
	 */
	@Override
	public Benchmark getBenchmark() {
		return new QuicksortBenchmark();
	}

	/* (non-Javadoc)
	 * @see org.haec.theatre.api.Impl#getPluginName()
	 */
	@Override
	public String getPluginName() {
		return "org.haec.app.sort.quicksort";
	}
}
