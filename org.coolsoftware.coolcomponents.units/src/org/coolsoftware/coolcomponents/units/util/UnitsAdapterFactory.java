/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.units.util;

import org.coolsoftware.coolcomponents.units.*;
import org.coolsoftware.coolcomponents.units.ComplexUnit;
import org.coolsoftware.coolcomponents.units.Factor;
import org.coolsoftware.coolcomponents.units.Part;
import org.coolsoftware.coolcomponents.units.SimpleUnit;
import org.coolsoftware.coolcomponents.units.Unit;
import org.coolsoftware.coolcomponents.units.UnitLibrary;
import org.coolsoftware.coolcomponents.units.UnitsPackage;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.units.UnitsPackage
 * @generated
 */
public class UnitsAdapterFactory extends AdapterFactoryImpl {
	/**
   * The cached model package.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected static UnitsPackage modelPackage;

	/**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public UnitsAdapterFactory() {
    if (modelPackage == null)
    {
      modelPackage = UnitsPackage.eINSTANCE;
    }
  }

	/**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
	@Override
	public boolean isFactoryForType(Object object) {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

	/**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	protected UnitsSwitch<Adapter> modelSwitch =
		new UnitsSwitch<Adapter>()
    {
      @Override
      public Adapter caseUnitLibrary(UnitLibrary object)
      {
        return createUnitLibraryAdapter();
      }
      @Override
      public Adapter casePart(Part object)
      {
        return createPartAdapter();
      }
      @Override
      public Adapter caseUnit(Unit object)
      {
        return createUnitAdapter();
      }
      @Override
      public Adapter caseSimpleUnit(SimpleUnit object)
      {
        return createSimpleUnitAdapter();
      }
      @Override
      public Adapter caseComplexUnit(ComplexUnit object)
      {
        return createComplexUnitAdapter();
      }
      @Override
      public Adapter caseFactor(Factor object)
      {
        return createFactorAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

	/**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
	@Override
	public Adapter createAdapter(Notifier target) {
    return modelSwitch.doSwitch((EObject)target);
  }


	/**
   * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.units.Unit <em>Unit</em>}'.
   * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.coolsoftware.coolcomponents.units.Unit
   * @generated
   */
	public Adapter createUnitAdapter() {
    return null;
  }

	/**
   * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.units.SimpleUnit <em>Simple Unit</em>}'.
   * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.coolsoftware.coolcomponents.units.SimpleUnit
   * @generated
   */
	public Adapter createSimpleUnitAdapter() {
    return null;
  }

	/**
   * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.units.ComplexUnit <em>Complex Unit</em>}'.
   * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.coolsoftware.coolcomponents.units.ComplexUnit
   * @generated
   */
	public Adapter createComplexUnitAdapter() {
    return null;
  }

	/**
   * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.units.Factor <em>Factor</em>}'.
   * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.coolsoftware.coolcomponents.units.Factor
   * @generated
   */
	public Adapter createFactorAdapter() {
    return null;
  }

	/**
   * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.units.UnitLibrary <em>Unit Library</em>}'.
   * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.coolsoftware.coolcomponents.units.UnitLibrary
   * @generated
   */
	public Adapter createUnitLibraryAdapter() {
    return null;
  }

	/**
   * Creates a new adapter for an object of class '{@link org.coolsoftware.coolcomponents.units.Part <em>Part</em>}'.
   * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
   * @return the new adapter.
   * @see org.coolsoftware.coolcomponents.units.Part
   * @generated
   */
	public Adapter createPartAdapter() {
    return null;
  }

	/**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
	public Adapter createEObjectAdapter() {
    return null;
  }

} //UnitsAdapterFactory
