/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.units;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.units.UnitsFactory
 * @model kind="package"
 * @generated
 */
public interface UnitsPackage extends EPackage {
	/**
   * The package name.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNAME = "units";

	/**
   * The package namespace URI.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNS_URI = "http://www.cool-software.org/dimm/units";

	/**
   * The package namespace name.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	String eNS_PREFIX = "units";

	/**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	UnitsPackage eINSTANCE = org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl.init();

	/**
   * The meta object id for the '{@link org.coolsoftware.coolcomponents.units.impl.UnitImpl <em>Unit</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.coolcomponents.units.impl.UnitImpl
   * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getUnit()
   * @generated
   */
	int UNIT = 2;

	/**
   * The meta object id for the '{@link org.coolsoftware.coolcomponents.units.impl.SimpleUnitImpl <em>Simple Unit</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.coolcomponents.units.impl.SimpleUnitImpl
   * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getSimpleUnit()
   * @generated
   */
	int SIMPLE_UNIT = 3;

	/**
   * The meta object id for the '{@link org.coolsoftware.coolcomponents.units.impl.ComplexUnitImpl <em>Complex Unit</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.coolcomponents.units.impl.ComplexUnitImpl
   * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getComplexUnit()
   * @generated
   */
	int COMPLEX_UNIT = 4;

	/**
   * The meta object id for the '{@link org.coolsoftware.coolcomponents.units.impl.UnitLibraryImpl <em>Unit Library</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.coolcomponents.units.impl.UnitLibraryImpl
   * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getUnitLibrary()
   * @generated
   */
	int UNIT_LIBRARY = 0;

	/**
   * The feature id for the '<em><b>Parts</b></em>' containment reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UNIT_LIBRARY__PARTS = 0;

	/**
   * The number of structural features of the '<em>Unit Library</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UNIT_LIBRARY_FEATURE_COUNT = 1;

	/**
   * The meta object id for the '{@link org.coolsoftware.coolcomponents.units.impl.PartImpl <em>Part</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.coolcomponents.units.impl.PartImpl
   * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getPart()
   * @generated
   */
	int PART = 1;

	/**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int PART__TYPE = 0;

	/**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int PART__NAME = 1;

	/**
   * The number of structural features of the '<em>Part</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int PART_FEATURE_COUNT = 2;

	/**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UNIT__TYPE = PART__TYPE;

	/**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UNIT__NAME = PART__NAME;

	/**
   * The number of structural features of the '<em>Unit</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int UNIT_FEATURE_COUNT = PART_FEATURE_COUNT + 0;

	/**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int SIMPLE_UNIT__TYPE = UNIT__TYPE;

	/**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int SIMPLE_UNIT__NAME = UNIT__NAME;

	/**
   * The number of structural features of the '<em>Simple Unit</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int SIMPLE_UNIT_FEATURE_COUNT = UNIT_FEATURE_COUNT + 0;

	/**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int COMPLEX_UNIT__TYPE = UNIT__TYPE;

	/**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int COMPLEX_UNIT__NAME = UNIT__NAME;

	/**
   * The feature id for the '<em><b>Factor</b></em>' containment reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int COMPLEX_UNIT__FACTOR = UNIT_FEATURE_COUNT + 0;

	/**
   * The feature id for the '<em><b>Parts</b></em>' reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int COMPLEX_UNIT__PARTS = UNIT_FEATURE_COUNT + 1;

	/**
   * The feature id for the '<em><b>Per Parts</b></em>' reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int COMPLEX_UNIT__PER_PARTS = UNIT_FEATURE_COUNT + 2;

	/**
   * The number of structural features of the '<em>Complex Unit</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int COMPLEX_UNIT_FEATURE_COUNT = UNIT_FEATURE_COUNT + 3;


	/**
   * The meta object id for the '{@link org.coolsoftware.coolcomponents.units.impl.FactorImpl <em>Factor</em>}' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see org.coolsoftware.coolcomponents.units.impl.FactorImpl
   * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getFactor()
   * @generated
   */
	int FACTOR = 5;

	/**
   * The feature id for the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int FACTOR__TYPE = PART__TYPE;

	/**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int FACTOR__NAME = PART__NAME;

	/**
   * The feature id for the '<em><b>Factor</b></em>' containment reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int FACTOR__FACTOR = PART_FEATURE_COUNT + 0;

	/**
   * The number of structural features of the '<em>Factor</em>' class.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
	int FACTOR_FEATURE_COUNT = PART_FEATURE_COUNT + 1;


	/**
   * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.units.Unit <em>Unit</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unit</em>'.
   * @see org.coolsoftware.coolcomponents.units.Unit
   * @generated
   */
	EClass getUnit();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.units.SimpleUnit <em>Simple Unit</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Unit</em>'.
   * @see org.coolsoftware.coolcomponents.units.SimpleUnit
   * @generated
   */
	EClass getSimpleUnit();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.units.ComplexUnit <em>Complex Unit</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Complex Unit</em>'.
   * @see org.coolsoftware.coolcomponents.units.ComplexUnit
   * @generated
   */
	EClass getComplexUnit();

	/**
   * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.units.ComplexUnit#getFactor <em>Factor</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Factor</em>'.
   * @see org.coolsoftware.coolcomponents.units.ComplexUnit#getFactor()
   * @see #getComplexUnit()
   * @generated
   */
	EReference getComplexUnit_Factor();

	/**
   * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.units.ComplexUnit#getParts <em>Parts</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Parts</em>'.
   * @see org.coolsoftware.coolcomponents.units.ComplexUnit#getParts()
   * @see #getComplexUnit()
   * @generated
   */
	EReference getComplexUnit_Parts();

	/**
   * Returns the meta object for the reference list '{@link org.coolsoftware.coolcomponents.units.ComplexUnit#getPerParts <em>Per Parts</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Per Parts</em>'.
   * @see org.coolsoftware.coolcomponents.units.ComplexUnit#getPerParts()
   * @see #getComplexUnit()
   * @generated
   */
	EReference getComplexUnit_PerParts();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.units.Factor <em>Factor</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Factor</em>'.
   * @see org.coolsoftware.coolcomponents.units.Factor
   * @generated
   */
	EClass getFactor();

	/**
   * Returns the meta object for the containment reference '{@link org.coolsoftware.coolcomponents.units.Factor#getFactor <em>Factor</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Factor</em>'.
   * @see org.coolsoftware.coolcomponents.units.Factor#getFactor()
   * @see #getFactor()
   * @generated
   */
	EReference getFactor_Factor();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.units.UnitLibrary <em>Unit Library</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Unit Library</em>'.
   * @see org.coolsoftware.coolcomponents.units.UnitLibrary
   * @generated
   */
	EClass getUnitLibrary();

	/**
   * Returns the meta object for the containment reference list '{@link org.coolsoftware.coolcomponents.units.UnitLibrary#getParts <em>Parts</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parts</em>'.
   * @see org.coolsoftware.coolcomponents.units.UnitLibrary#getParts()
   * @see #getUnitLibrary()
   * @generated
   */
	EReference getUnitLibrary_Parts();

	/**
   * Returns the meta object for class '{@link org.coolsoftware.coolcomponents.units.Part <em>Part</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for class '<em>Part</em>'.
   * @see org.coolsoftware.coolcomponents.units.Part
   * @generated
   */
	EClass getPart();

	/**
   * Returns the meta object for the reference '{@link org.coolsoftware.coolcomponents.units.Part#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Type</em>'.
   * @see org.coolsoftware.coolcomponents.units.Part#getType()
   * @see #getPart()
   * @generated
   */
	EReference getPart_Type();

	/**
   * Returns the meta object for the attribute '{@link org.coolsoftware.coolcomponents.units.Part#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.coolsoftware.coolcomponents.units.Part#getName()
   * @see #getPart()
   * @generated
   */
	EAttribute getPart_Name();

	/**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
	UnitsFactory getUnitsFactory();

	/**
   * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
   * @generated
   */
	interface Literals {
		/**
     * The meta object literal for the '{@link org.coolsoftware.coolcomponents.units.impl.UnitImpl <em>Unit</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.coolcomponents.units.impl.UnitImpl
     * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getUnit()
     * @generated
     */
		EClass UNIT = eINSTANCE.getUnit();

		/**
     * The meta object literal for the '{@link org.coolsoftware.coolcomponents.units.impl.SimpleUnitImpl <em>Simple Unit</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.coolcomponents.units.impl.SimpleUnitImpl
     * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getSimpleUnit()
     * @generated
     */
		EClass SIMPLE_UNIT = eINSTANCE.getSimpleUnit();

		/**
     * The meta object literal for the '{@link org.coolsoftware.coolcomponents.units.impl.ComplexUnitImpl <em>Complex Unit</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.coolcomponents.units.impl.ComplexUnitImpl
     * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getComplexUnit()
     * @generated
     */
		EClass COMPLEX_UNIT = eINSTANCE.getComplexUnit();

		/**
     * The meta object literal for the '<em><b>Factor</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference COMPLEX_UNIT__FACTOR = eINSTANCE.getComplexUnit_Factor();

		/**
     * The meta object literal for the '<em><b>Parts</b></em>' reference list feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference COMPLEX_UNIT__PARTS = eINSTANCE.getComplexUnit_Parts();

		/**
     * The meta object literal for the '<em><b>Per Parts</b></em>' reference list feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference COMPLEX_UNIT__PER_PARTS = eINSTANCE.getComplexUnit_PerParts();

		/**
     * The meta object literal for the '{@link org.coolsoftware.coolcomponents.units.impl.FactorImpl <em>Factor</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.coolcomponents.units.impl.FactorImpl
     * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getFactor()
     * @generated
     */
		EClass FACTOR = eINSTANCE.getFactor();

		/**
     * The meta object literal for the '<em><b>Factor</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference FACTOR__FACTOR = eINSTANCE.getFactor_Factor();

		/**
     * The meta object literal for the '{@link org.coolsoftware.coolcomponents.units.impl.UnitLibraryImpl <em>Unit Library</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.coolcomponents.units.impl.UnitLibraryImpl
     * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getUnitLibrary()
     * @generated
     */
		EClass UNIT_LIBRARY = eINSTANCE.getUnitLibrary();

		/**
     * The meta object literal for the '<em><b>Parts</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference UNIT_LIBRARY__PARTS = eINSTANCE.getUnitLibrary_Parts();

		/**
     * The meta object literal for the '{@link org.coolsoftware.coolcomponents.units.impl.PartImpl <em>Part</em>}' class.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @see org.coolsoftware.coolcomponents.units.impl.PartImpl
     * @see org.coolsoftware.coolcomponents.units.impl.UnitsPackageImpl#getPart()
     * @generated
     */
		EClass PART = eINSTANCE.getPart();

		/**
     * The meta object literal for the '<em><b>Type</b></em>' reference feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EReference PART__TYPE = eINSTANCE.getPart_Type();

		/**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
     * @generated
     */
		EAttribute PART__NAME = eINSTANCE.getPart_Name();

	}

} //UnitsPackage
