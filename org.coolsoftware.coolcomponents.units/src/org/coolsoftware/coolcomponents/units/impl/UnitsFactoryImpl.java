/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.units.impl;

import org.coolsoftware.coolcomponents.units.*;
import org.coolsoftware.coolcomponents.units.ComplexUnit;
import org.coolsoftware.coolcomponents.units.Factor;
import org.coolsoftware.coolcomponents.units.SimpleUnit;
import org.coolsoftware.coolcomponents.units.UnitLibrary;
import org.coolsoftware.coolcomponents.units.UnitsFactory;
import org.coolsoftware.coolcomponents.units.UnitsPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UnitsFactoryImpl extends EFactoryImpl implements UnitsFactory {
	/**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public static UnitsFactory init() {
    try
    {
      UnitsFactory theUnitsFactory = (UnitsFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.cool-software.org/dimm/units"); 
      if (theUnitsFactory != null)
      {
        return theUnitsFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new UnitsFactoryImpl();
  }

	/**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public UnitsFactoryImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public EObject create(EClass eClass) {
    switch (eClass.getClassifierID())
    {
      case UnitsPackage.UNIT_LIBRARY: return createUnitLibrary();
      case UnitsPackage.SIMPLE_UNIT: return createSimpleUnit();
      case UnitsPackage.COMPLEX_UNIT: return createComplexUnit();
      case UnitsPackage.FACTOR: return createFactor();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public SimpleUnit createSimpleUnit() {
    SimpleUnitImpl simpleUnit = new SimpleUnitImpl();
    return simpleUnit;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public ComplexUnit createComplexUnit() {
    ComplexUnitImpl complexUnit = new ComplexUnitImpl();
    return complexUnit;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public Factor createFactor() {
    FactorImpl factor = new FactorImpl();
    return factor;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public UnitLibrary createUnitLibrary() {
    UnitLibraryImpl unitLibrary = new UnitLibraryImpl();
    return unitLibrary;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public UnitsPackage getUnitsPackage() {
    return (UnitsPackage)getEPackage();
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
	@Deprecated
	public static UnitsPackage getPackage() {
    return UnitsPackage.eINSTANCE;
  }

} //UnitsFactoryImpl
