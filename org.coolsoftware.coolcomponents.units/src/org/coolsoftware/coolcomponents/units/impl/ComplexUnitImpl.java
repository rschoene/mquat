/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.units.impl;

import java.util.Collection;

import org.coolsoftware.coolcomponents.expressions.Expression;
import org.coolsoftware.coolcomponents.types.CcmType;
import org.coolsoftware.coolcomponents.types.stdlib.CcmStandardLibraryTypeFactory;
import org.coolsoftware.coolcomponents.units.ComplexUnit;
import org.coolsoftware.coolcomponents.units.Part;
import org.coolsoftware.coolcomponents.units.UnitsPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Complex Unit</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.units.impl.ComplexUnitImpl#getFactor <em>Factor</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.units.impl.ComplexUnitImpl#getParts <em>Parts</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.units.impl.ComplexUnitImpl#getPerParts <em>Per Parts</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComplexUnitImpl extends UnitImpl implements ComplexUnit {
	/**
   * The cached value of the '{@link #getFactor() <em>Factor</em>}' containment reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getFactor()
   * @generated
   * @ordered
   */
	protected Expression factor;

	/**
   * The cached value of the '{@link #getParts() <em>Parts</em>}' reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getParts()
   * @generated
   * @ordered
   */
	protected EList<Part> parts;

	/**
   * The cached value of the '{@link #getPerParts() <em>Per Parts</em>}' reference list.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @see #getPerParts()
   * @generated
   * @ordered
   */
	protected EList<Part> perParts;

	/**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
	protected ComplexUnitImpl() {
    super();
  }

	/**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
	@Override
	protected EClass eStaticClass() {
    return UnitsPackage.Literals.COMPLEX_UNIT;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public Expression getFactor() {
    return factor;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public NotificationChain basicSetFactor(Expression newFactor, NotificationChain msgs) {
    Expression oldFactor = factor;
    factor = newFactor;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UnitsPackage.COMPLEX_UNIT__FACTOR, oldFactor, newFactor);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public void setFactor(Expression newFactor) {
    if (newFactor != factor)
    {
      NotificationChain msgs = null;
      if (factor != null)
        msgs = ((InternalEObject)factor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UnitsPackage.COMPLEX_UNIT__FACTOR, null, msgs);
      if (newFactor != null)
        msgs = ((InternalEObject)newFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UnitsPackage.COMPLEX_UNIT__FACTOR, null, msgs);
      msgs = basicSetFactor(newFactor, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, UnitsPackage.COMPLEX_UNIT__FACTOR, newFactor, newFactor));
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EList<Part> getParts() {
    if (parts == null)
    {
      parts = new EObjectResolvingEList<Part>(Part.class, this, UnitsPackage.COMPLEX_UNIT__PARTS);
    }
    return parts;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	public EList<Part> getPerParts() {
    if (perParts == null)
    {
      perParts = new EObjectResolvingEList<Part>(Part.class, this, UnitsPackage.COMPLEX_UNIT__PER_PARTS);
    }
    return perParts;
  }

	/**
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID)
    {
      case UnitsPackage.COMPLEX_UNIT__FACTOR:
        return basicSetFactor(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

	/**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID)
    {
      case UnitsPackage.COMPLEX_UNIT__FACTOR:
        return getFactor();
      case UnitsPackage.COMPLEX_UNIT__PARTS:
        return getParts();
      case UnitsPackage.COMPLEX_UNIT__PER_PARTS:
        return getPerParts();
    }
    return super.eGet(featureID, resolve, coreType);
  }

	/**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
    switch (featureID)
    {
      case UnitsPackage.COMPLEX_UNIT__FACTOR:
        setFactor((Expression)newValue);
        return;
      case UnitsPackage.COMPLEX_UNIT__PARTS:
        getParts().clear();
        getParts().addAll((Collection<? extends Part>)newValue);
        return;
      case UnitsPackage.COMPLEX_UNIT__PER_PARTS:
        getPerParts().clear();
        getPerParts().addAll((Collection<? extends Part>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

	/**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
	@Override
	public void eUnset(int featureID) {
    switch (featureID)
    {
      case UnitsPackage.COMPLEX_UNIT__FACTOR:
        setFactor((Expression)null);
        return;
      case UnitsPackage.COMPLEX_UNIT__PARTS:
        getParts().clear();
        return;
      case UnitsPackage.COMPLEX_UNIT__PER_PARTS:
        getPerParts().clear();
        return;
    }
    super.eUnset(featureID);
  }

	/**
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * @generated
   */
	@Override
	public boolean eIsSet(int featureID) {
    switch (featureID)
    {
      case UnitsPackage.COMPLEX_UNIT__FACTOR:
        return factor != null;
      case UnitsPackage.COMPLEX_UNIT__PARTS:
        return parts != null && !parts.isEmpty();
      case UnitsPackage.COMPLEX_UNIT__PER_PARTS:
        return perParts != null && !perParts.isEmpty();
    }
    return super.eIsSet(featureID);
  }

	/**
	 * (non-Javadoc)
	 * 
	 * @see org.coolsoftware.coolcomponents.units.impl.UnitImpl#getType()
	 * 
	 * @generated NOT
	 */
	@Override
	public CcmType getType() {

		// TODO WFRs for non-numeric units.

		CcmType realType = CcmStandardLibraryTypeFactory.eINSTANCE
				.getRealType();
		CcmType integerType = CcmStandardLibraryTypeFactory.eINSTANCE
				.getIntegerType();

		/* Any division results in real. */
		if (this.getPerParts().size() > 0)
			return CcmStandardLibraryTypeFactory.eINSTANCE.getRealType();

		/* A simple renaming results in the same time. */
		if (this.getParts().size() == 1 && this.perParts.size() == 0)
			return this.getParts().get(0).getType();

		/* Check for non-integer types. */
		for (Part elem : this.getParts()) {
			if (!elem.getType().conformsTo(integerType)
					&& elem.getType().conformsTo(realType))
				return realType;
		}
		// end for.

		return integerType;
	}
} // ComplexUnitImpl
