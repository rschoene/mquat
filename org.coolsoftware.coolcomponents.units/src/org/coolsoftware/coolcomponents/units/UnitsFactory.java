/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.units;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.coolsoftware.coolcomponents.units.UnitsPackage
 * @generated
 */
public interface UnitsFactory extends EFactory {
	/**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @generated
   */
	UnitsFactory eINSTANCE = org.coolsoftware.coolcomponents.units.impl.UnitsFactoryImpl.init();

	/**
   * Returns a new object of class '<em>Simple Unit</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Unit</em>'.
   * @generated
   */
	SimpleUnit createSimpleUnit();

	/**
   * Returns a new object of class '<em>Complex Unit</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Complex Unit</em>'.
   * @generated
   */
	ComplexUnit createComplexUnit();

	/**
   * Returns a new object of class '<em>Factor</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Factor</em>'.
   * @generated
   */
	Factor createFactor();

	/**
   * Returns a new object of class '<em>Unit Library</em>'.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return a new object of class '<em>Unit Library</em>'.
   * @generated
   */
	UnitLibrary createUnitLibrary();

	/**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
	UnitsPackage getUnitsPackage();

} //UnitsFactory
