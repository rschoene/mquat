/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.units;

import org.coolsoftware.coolcomponents.expressions.Expression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Factor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.units.Factor#getFactor <em>Factor</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.units.UnitsPackage#getFactor()
 * @model
 * @generated
 */
public interface Factor extends Part {
	/**
   * Returns the value of the '<em><b>Factor</b></em>' containment reference.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Factor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Factor</em>' containment reference.
   * @see #setFactor(Expression)
   * @see org.coolsoftware.coolcomponents.units.UnitsPackage#getFactor_Factor()
   * @model containment="true" required="true"
   * @generated
   */
	Expression getFactor();

	/**
   * Sets the value of the '{@link org.coolsoftware.coolcomponents.units.Factor#getFactor <em>Factor</em>}' containment reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Factor</em>' containment reference.
   * @see #getFactor()
   * @generated
   */
	void setFactor(Expression value);

} // Factor
