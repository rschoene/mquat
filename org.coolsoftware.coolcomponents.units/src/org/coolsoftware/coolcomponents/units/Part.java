/**
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package org.coolsoftware.coolcomponents.units;

import org.coolsoftware.coolcomponents.types.CcmType;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.coolsoftware.coolcomponents.units.Part#getType <em>Type</em>}</li>
 *   <li>{@link org.coolsoftware.coolcomponents.units.Part#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.coolsoftware.coolcomponents.units.UnitsPackage#getPart()
 * @model abstract="true"
 * @generated
 */
public interface Part extends EObject {
	/**
   * Returns the value of the '<em><b>Type</b></em>' reference.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' reference.
   * @see #setType(CcmType)
   * @see org.coolsoftware.coolcomponents.units.UnitsPackage#getPart_Type()
   * @model required="true"
   * @generated
   */
	CcmType getType();

	/**
   * Sets the value of the '{@link org.coolsoftware.coolcomponents.units.Part#getType <em>Type</em>}' reference.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' reference.
   * @see #getType()
   * @generated
   */
	void setType(CcmType value);

	/**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.coolsoftware.coolcomponents.units.UnitsPackage#getPart_Name()
   * @model id="true" required="true"
   * @generated
   */
	String getName();

	/**
   * Sets the value of the '{@link org.coolsoftware.coolcomponents.units.Part#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
	void setName(String value);

} // Part
