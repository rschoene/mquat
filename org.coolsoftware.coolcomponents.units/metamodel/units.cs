SYNTAXDEF unit
FOR <http://www.cool-software.org/dimm/units> <units.genmodel>
START UnitLibrary

IMPORTS {
	epx : <http://www.cool-software.org/dimm/expressions> 
		<org.coolsoftware.coolcomponents.expressions/metamodel/expressions.genmodel>
		WITH SYNTAX exp <../../org.coolsoftware.coolcomponents.expressions/metamodel/expressions.cs>
}

OPTIONS {
	reloadGeneratorModel = "true";
	disableDebugSupport = "true";
	disableLaunchSupport = "true";
}

RULES {
	UnitLibrary ::= 
		"library" #1 "{" !1
			(parts !0)* 
		"}";
		
	@SuppressWarnings(featureWithoutSyntax)
	Factor ::= 
		"factor" #1 name[TEXT] #1 "=" #1 factor #0 ";";
	
	SimpleUnit ::= 
		"simple" #1 "unit" #1 name[TEXT] #1 ":" #1 type[TYPE_LITERAL] #0 ";";
		
	@SuppressWarnings(featureWithoutSyntax)
	ComplexUnit ::=
		"complex" #1 "unit" #1 name[TEXT] #1 "=" 
			(#1 "[" #0 factor #0 "]")?
			#1 parts[TEXT](#1 parts[TEXT])* 
			(#1 "per" #1 perParts[TEXT] (#1 perParts[TEXT])*)? #0 ";";
}